package test;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.text.SimpleDateFormat;


public class JcmsSSLClient {

    String destURL;
    String acceptDataFormat;
    String contentType;
    String httpMethod;
    int connectionTimeoutInSecond;
    int readTimeoutInSecond;
    String authUser;
    String authPwd;
    String authToken;
    boolean useAuthTokenInBasicAuth;
    String hostName;
    boolean useHostNameInHeader;
    String requestData;
    boolean useBasicAuthentication;
    String certificateKeystorePath;
    String trustStorePassword;
    JcmsSSLClientResponse urlResponse;
     public JcmsSSLClient(){
            this.setDestURL("NA");
            this.setAcceptDataFormat("application/xml");
            this.setUseBasicAuthentication(false);
            this.setContentType("application/xml");
            this.setHttpMethod("GET");
            this.setConnectionTimeoutInSecond(10);
            this.setReadTimeoutInSecond(10);
            this.setCertificateKeystorePath("NA");
            urlResponse=new JcmsSSLClientResponse();
            this.setRequestData("NA");
            this.setAuthPwd("NA");
            this.setAuthUser("NA");
            this.setTrustStorePassword("NA");
    }
    
public static void main(String[] args) throws Exception{
	String keystorePath=args[0];
	String keystorePwd=args[1];
	String destURL=args[2];
	String msisdn=args[3];
	String service=args[4];
	String classVal=args[5];
	String transId=args[6];
	SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
	String data="Msisdn="+msisdn+"&Service="+service+"&Class="+classVal+"&Mode=WAP_D2C&RequestID="+transId+"&action=CAN&org_id=DIGIVIVE&loginid=divive12&password=diG23Vive&Timestamp="+sdf.format(new java.util.Date(System.currentTimeMillis()));
	 JcmsSSLClient sslClient=new JcmsSSLClient();
	sslClient.setDestURL(destURL);
	sslClient.setCertificateKeystorePath(keystorePath);
	sslClient.setTrustStorePassword(keystorePwd);
        sslClient.setUseBasicAuthentication(false);
        sslClient.setAcceptDataFormat("text/html");
	sslClient.setContentType("application/x-www-url-encoded");
        sslClient.setHttpMethod("POST");
	sslClient.setRequestData(data);
	sslClient.setConnectionTimeoutInSecond(30);
	sslClient.setReadTimeoutInSecond(30);
        JcmsSSLClientResponse resp=sslClient.connectURL();
	 System.out.println("Response code "+resp.getResponseCode());
	System.out.println("Response Data "+resp.getResponseData());
        System.out.println("Response Error "+resp.getErrorMessage());

}

public JcmsSSLClientResponse connectURL(){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else if(this.getCertificateKeystorePath().equalsIgnoreCase("NA") || this.getTrustStorePassword().equalsIgnoreCase("NA")){
            //Invalid Keystore path
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Certificate file path or truststore password");
        }else{
            try{
                  Properties systemProps = System.getProperties();
                  systemProps.put( "javax.net.debug", "ssl,keymanager");
                  System.setProperty("javax.net.ssl.trustStore", this.getCertificateKeystorePath());
                  System.setProperty("javax.net.ssl.trustStorePassword", this.getTrustStorePassword());
                  System.setProperties(systemProps);
                  
                  URL url = new URL( this.getDestURL() );
                  HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                  con.setRequestProperty( "Connection", "close" );
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setUseCaches(false);
                    con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                    con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                    con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                    con.setRequestProperty( "Content-Type", this.getContentType() );
                    con.setRequestProperty("Accept", this.getAcceptDataFormat());
                    if(this.isUseHostNameInHeader())
                        con.setRequestProperty("HOST", this.getHostName());
                   
                    if(this.isUseAuthTokenInBasicAuth()){
                        con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                    }else if(this.isUseBasicAuthentication()){
                       //String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                        //String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                        //con.setRequestProperty("Authorization", "Basic " + encoding);  
                    }
                    if(this.getRequestData().length()>0){
                        OutputStream outputStream = con.getOutputStream();
                        outputStream.write(this.getRequestData().getBytes());
                        outputStream.close();
                    }
                    int responseCode = con.getResponseCode();
                    InputStream inputStream;
                    if (responseCode == HttpURLConnection.HTTP_OK) 
                      inputStream = con.getInputStream();
                    else
                      inputStream = con.getErrorStream();
                
                    BufferedReader reader;
                    String line = null;
                    StringBuilder sb=new StringBuilder();
                    reader = new BufferedReader( new InputStreamReader( inputStream ) );
                    while( ( line = reader.readLine() ) != null ){
                      sb.append(line).append("\n\r");
                    }
                    
                    inputStream.close();
                    this.urlResponse.setResponseCode(responseCode);
                    this.urlResponse.setResponseData(sb.toString());
            }catch(Exception e){
                this.urlResponse.setResponseCode(-102);
                this.urlResponse.setErrorMessage("Exception "+e.getMessage());
            }
          
        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }

 public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public boolean isUseHostNameInHeader() {
        return useHostNameInHeader;
    }

    public void setUseHostNameInHeader(boolean useHostNameInHeader) {
        this.useHostNameInHeader = useHostNameInHeader;
    }

    public String getTrustStorePassword() {
        return trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }

    public boolean isUseAuthTokenInBasicAuth() {
        return useAuthTokenInBasicAuth;
    }

    public void setUseAuthTokenInBasicAuth(boolean useAuthTokenInBasicAuth) {
        this.useAuthTokenInBasicAuth = useAuthTokenInBasicAuth;
    }
    
    
    
    
    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    
    public JcmsSSLClientResponse getUrlResponse() {
        return urlResponse;
    }

    public void setUrlResponse(JcmsSSLClientResponse urlResponse) {
        this.urlResponse = urlResponse;
    }

    
    
    public String getDestURL() {
        return destURL;
    }

    public void setDestURL(String destURL) {
        this.destURL = destURL;
    }

    public String getAcceptDataFormat() {
        return acceptDataFormat;
    }

    public void setAcceptDataFormat(String acceptDataFormat) {
        this.acceptDataFormat = acceptDataFormat;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getConnectionTimeoutInSecond() {
        return connectionTimeoutInSecond;
    }

    public void setConnectionTimeoutInSecond(int connectionTimeoutInSecond) {
        this.connectionTimeoutInSecond = connectionTimeoutInSecond;
    }

    public int getReadTimeoutInSecond() {
        return readTimeoutInSecond;
    }

    public void setReadTimeoutInSecond(int readTimeoutInSecond) {
        this.readTimeoutInSecond = readTimeoutInSecond;
    }

    public String getAuthUser() {
        return authUser;
    }

    public void setAuthUser(String authUser) {
        this.authUser = authUser;
    }

    public String getAuthPwd() {
        return authPwd;
    }

    public void setAuthPwd(String authPwd) {
        this.authPwd = authPwd;
    }

    public boolean isUseBasicAuthentication() {
        return useBasicAuthentication;
    }

    public void setUseBasicAuthentication(boolean useBasicAuthentication) {
        this.useBasicAuthentication = useBasicAuthentication;
    }

    public String getCertificateKeystorePath() {
        return certificateKeystorePath;
    }

    public void setCertificateKeystorePath(String certificateKeystorePath) {
        this.certificateKeystorePath = certificateKeystorePath;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    
    public class JcmsSSLClientResponse{
        int responseCode;
        String responseData;
        String errorMessage;
        int responseTime;//In Milli Second
        public JcmsSSLClientResponse(){}

        public int getResponseTime() {
            return responseTime;
        }

        public void setResponseTime(int responseTime) {
            this.responseTime = responseTime;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public String getResponseData() {
            return responseData;
        }

        public void setResponseData(String responseData) {
            this.responseData = responseData;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
        
    }
}
