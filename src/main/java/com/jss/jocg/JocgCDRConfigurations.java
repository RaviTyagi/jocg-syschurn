package com.jss.jocg;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource; 


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
  entityManagerFactoryRef = "jocgcdrEntityManagerFactory",
  transactionManagerRef = "jocgcdrTransactionManager",
  basePackages = { "com.jss.jocg.cdr.dao" }
)
public class JocgCDRConfigurations  {

	@Bean(name = "jocgcdrDataSource")
	  @ConfigurationProperties(prefix = "jocgcdr.datasource")
	  public DataSource dataSource() {
	    return DataSourceBuilder.create().build();
	  }

	@Bean(name = "jocgcdrEntityManagerFactory")
	  public LocalContainerEntityManagerFactoryBean 
	  jocgcdrEntityManagerFactory(
	    EntityManagerFactoryBuilder builder,
	    @Qualifier("jocgcdrDataSource") DataSource dataSource
	  ) {
	    return
	      builder
	        .dataSource(dataSource)
	        .packages("com.jss.jocg.cdr.entities")
	        .persistenceUnit("jocgcdr")
	        .build();
	  }
	
	@Bean(name = "jocgcdrTransactionManager")
	  public PlatformTransactionManager jocgcdrTransactionManager(
	    @Qualifier("jocgcdrEntityManagerFactory") EntityManagerFactory
	    jocgcdrEntityManagerFactory
	  ) {
	    return new JpaTransactionManager(jocgcdrEntityManagerFactory);
	  }

}
