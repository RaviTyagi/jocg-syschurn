package com.jss.jocg.callback.bo;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.bo.airtel.ActivationCallback;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class AirtelIndiaSDPCallbackReceiver_onHold {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelIndiaSDPCallbackReceiver_onHold.class);
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");

	/**
	 Subscriber Status Codes:
	Pending/NewSub-0
	CGSUCCESS=1
	PARKING=2
	GRACE=3
	ACTIVE=5
	CGFAILED=-1
	SUSPENDEDFROMPARKING=-2
	SUSPENDEDFROMGRACE=-3
	UNSUB=-5

	 */		

	//@JmsListener(destination = "callback.airtelincg", containerFactory = "myFactory")
	public void processCGCallback(String suntechURL) {
		//Push Callback to Suntech
		
		try{
			
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setConnectionTimeoutInSecond(5);
			sslClient.setReadTimeoutInSecond(10);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL("AIRTEL-REFWD-SUNTECH",suntechURL);
			logger.debug(" Airtel-SUNTECH-URL:"+suntechURL+"|Resp="+resp.getResponseData()+"|Error="+resp.getErrorMessage()+"|RespTime="+resp.getResponseTime());
		}catch(Exception e){}
		
		
	}
	
	/** Queue which sends airtel callback to suntec url*/
	//@JmsListener(destination = "callback.tbmssdp", containerFactory = "myFactory")
    public void receiveMessage(String callbackData) {
		try{
			
			String responseString = "";
			String outputString = "";
			int modeVal=(int)(System.currentTimeMillis()%4);
			String wsURL =(modeVal<=0)?"http://172.31.19.172:8082/tbms/services/NotificationToCPService?wsdl"
					:(modeVal<=1)?"http://172.31.19.172:8083/tbms/services/NotificationToCPService?wsdl"
					:(modeVal<=2)?"http://172.31.19.175:8082/tbms/services/NotificationToCPService?wsdl"
					:"http://172.31.19.175:8083/tbms/services/NotificationToCPService?wsdl";
			
			URL url = new URL(wsURL);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection)connection;
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buffer = new byte[callbackData.length()];
			buffer = callbackData.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
		
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty("Content-Length",
			String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			//httpConn.setRequestProperty("SOAPAction", SOAPAction);
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			out.write(b);
			out.close();
			//Ready with sending the request.
			 
			//Read the response.
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			 
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				outputString = outputString + responseString;
			}
			in.close();
			in=null;
			httpConn.disconnect();
			httpConn=null;
			url=null;
			logger.debug("SUNTEC RESP "+outputString);
		}catch(Exception e){
			logger.error("Exception while writing data to suntec :"+e.getMessage());
		}
	}
	
	//@JmsListener(destination = "callback.airtelsdp", containerFactory = "myFactory")
    public void receiveMessage(ActivationCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+"|";
	
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			Long msisdn=JocgString.strToLong(callbackData.getMsisdn(),0);
			if(callbackData!=null && msisdn>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+msisdn+", Key "+callbackData.getProductId()+", Amount "+callbackData.getAmount()+", LowBalance "+callbackData.getLowBalance());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getProductId(),JocgString.strToDouble(callbackData.getAmount(),0D));
				if(cpp==null) cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getProductId());
				callbackData.setTemp2(JocgString.formatString(callbackData.getTemp2(), "NA"));
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=""+msisdn.longValue();
					if(msisdnStr.length()<=10) callbackData.setMsisdn(""+JocgString.strToLong("91"+msisdn.longValue(),msisdn));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+msisdn+", Operator Service Key "+serviceKey);
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(msisdn,serviceKey);	
					if(sub==null || sub.getId()==null){
						ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
						PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
						Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
						PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
						Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
						if(service!=null && service.getServiceid()>0){
							sub=subscriberDao.searchSubscriberByChintfAndServiceId(chintf.getChintfid(),""+msisdn.longValue(),msisdn,service.getServiceid(),service.getServiceName());
						}
						
					}
					ChargingCDR cdr=null;
					int actionCode=JocgString.strToInt(callbackData.getErrorCode(), 0);
					String action=(actionCode==1000 && callbackData.getErrorMsg().contains("AOC"))?"ACT":
						(actionCode==1000)?"REN":(actionCode==3404)?"GRACE":(actionCode==1013)?"DCT":(actionCode==3027)?"SUSPEND":"OTHER-"+callbackData.getErrorCode();
					
					
					
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						java.util.Date callbackDate=callbackData.getChargigTime();
						String sdpTransId=callbackData.getTemp2();
						boolean duplicateActOrRenewal=false;
						if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action)){
							cdr=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(cppmaster.getChintfid(),callbackData.getTemp2());
							if(cdr!=null && cdr.getId()!=null && action.equalsIgnoreCase(cdr.getRequestType())){
								duplicateActOrRenewal=true;
							}
						}
						
						logger.debug(loggerHead+"action "+action+", sub :"+sub+"|last RenewalDate "+lastRenewaldate+"|Processing Date "+sdfDate.format(callbackDate));
						
						if(("REN".equalsIgnoreCase(action) || "ACT".equalsIgnoreCase(action)) && duplicateActOrRenewal==true){
							logger.debug(loggerHead+" Duplicate Activation/Renewal Callback, Hence Ignored");
						}else if("DCT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
						cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null){
								if((sub.getStatus()!=JocgStatusCodes.SUB_SUCCESS_ACTIVE && ("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action)))){
									logger.debug(loggerHead+"  Either CDR found but subscriber is not active or SDPTransactionId is different, Process for Existing Subscriber and Non Existing CDR...");
									handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
								}else{
									logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode()+"");
									handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr,action);
								}
						
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
							}
						}else{
							logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
						}
					}else{
						if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
							logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
							handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
						}else{
							logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
						}
						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			
		}
        
    }
	
	public void handleExistingSubscriber(String loggerHead,ActivationCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr,String actionName){
		loggerHead +="HES::";
		try{
			logger.debug(loggerHead+" Processing for existing custmer CDR ......."+actionName+", status "+callbackData.getErrorMsg());
			boolean activatedFromZero=(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && 
					cdr.getChargedPricePoint().doubleValue()==0D && cpp.getPricePoint()>0 && 
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && sub.getChargedAmount().doubleValue()==0D)?true:false;
			
			logger.debug(loggerHead+" activatedFromZero "+activatedFromZero);
			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
			
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,actionName,activatedFromZero);
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(actionName) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getErrorMsg())){
					logger.debug(loggerHead+" Sending Activation SMS .......");
					//Enable on production
//					JocgSMSMessage smsMessage=new JocgSMSMessage();
//					smsMessage.setMsisdn(JocgString.strToLong(callbackData.getMsisdn(),0L));
//					smsMessage.setSmsInterface("airtelin");
//					smsMessage.setChintfId(1);
//					smsMessage.setTextMessage("Successful Activation");
//					jmsTemplate.convertAndSend("notify.sms", smsMessage);
//					logger.debug(loggerHead+" Activation SMS Queued .......SMSData="+smsMessage);
					
					//Setting Notification
//					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					Calendar cal=Calendar.getInstance();
//					cal.setTimeInMillis(System.currentTimeMillis());
//					cal.add(Calendar.MINUTE, 60);
//					cdr.setNotifyScheduleTime(cal.getTime());
					//cdr.setNotifyChurn(true);
//					logger.debug(loggerHead+" S2S Notification Queued, Churn Status Enabled!");
					
				}
				cdrBuilder.save(cdr);
			}else if("DCT".equalsIgnoreCase(actionName)){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
				
//				if(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
//					String urlToInvoke="http://119.252.215.120/DCTNOTIFY?MSISDN="+java.net.URLEncoder.encode(callbackData.getMsisdn(),"utf8")+"&TransactionID="+java.net.URLEncoder.encode(callbackData.getXactionId(),"utf8")+"&OperatorID="+java.net.URLEncoder.encode(cdr.getSourceNetworkCode(),"utf8")+"&VendorID="+java.net.URLEncoder.encode(cdr.getTrafficSourceName(),"utf8")+"&Date="+java.net.URLEncoder.encode(""+callbackData.getChargigTime(),"utf8");
//					JcmsSSLClient sslClient=new JcmsSSLClient();
//					sslClient.setDestURL(urlToInvoke);
//					JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURL(loggerHead);
//					logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH ::URL "+urlToInvoke+"|Resp="+resp.getResponseData()+"|Error="+resp.getErrorMessage()+"|TimeToken="+resp.getResponseTime());
//				}
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,ActivationCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,String actionName){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			if(pkg.getPkgid()!=sub.getPackageId()){
				sub.setPackageId(pkg.getPkgid());
				sub.setPackageName(pkg.getPkgname());
				sub.setPackagePrice(pkg.getPricepoint());
				if(service!=null){
					sub.setServiceId(service.getServiceid());
					sub.setServiceName(service.getServiceName());
				
				}
				if(sub.getPackageChargingInterfaceMapId()!=pkgChintf.getPkgchintfid()){
					sub.setPackageChargingInterfaceMapId(pkgChintf.getPkgchintfid());
				}
				
			}
			//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			java.sql.Timestamp t=callbackData.getChargigTime();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(t.getTime());;
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getSystemId());
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getAdnetworkName());
			ChargingCDR newCDR=null;
			if(callbackData.getReferer().contains("DV-CALLBACK")){
				newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), callbackData.getId(), requestCategory, actionName, sub.getSubscriberId(), sub.getMsisdn(), (platform!=null?platform.getPlatform():"NA"), (platform!=null?platform.getSystemid():"NA"),  
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
						startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getXactionId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				
			}else{
				ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
				TrafficCampaign tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
				newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, actionName, sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
						startDate, startDate, endDate1, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getXactionId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
			}
			
			
			boolean activatedFromZero=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && sub.getChargedAmount().doubleValue()==0D)?true:false;
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,activatedFromZero);
			if("ACT".equalsIgnoreCase(actionName) || "REN".equalsIgnoreCase(actionName) && "1000".equalsIgnoreCase(callbackData.getErrorCode())){
				//Update in case of non existing CDR
				newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
				newCDR.setStatusDescp("Customer Activated");
				newCDR.setChargedPpid(cpp.getPpid());
				newCDR.setChargedPricePoint(cpp.getPricePoint());
				newCDR.setChargedAmount(cpp.getPricePoint());
				newCDR.setChargedOperatorKey(cpp.getOperatorKey());
				newCDR.setChargedOperatorParams(cpp.getOpParams());
				newCDR.setChargingDate(startDate);
				newCDR.setValidityStartDate(startDate);
				newCDR.setValidityEndDate(endDate1);
				logger.debug("Charged PpId "+newCDR.getChargedPpid());
			}
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(actionName)) sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
//			if("DCT".equalsIgnoreCase(actionName)){
//				if(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
//					String urlToInvoke="http://119.252.215.120/DCTNOTIFY?MSISDN="+java.net.URLEncoder.encode(callbackData.getMsisdn(),"utf8")+"&TransactionID="+java.net.URLEncoder.encode(callbackData.getXactionId(),"utf8")+"&OperatorID="+java.net.URLEncoder.encode(newCDR.getSourceNetworkCode(),"utf8")+"&VendorID="+java.net.URLEncoder.encode(newCDR.getTrafficSourceName(),"utf8")+"&Date="+java.net.URLEncoder.encode(""+callbackData.getChargigTime(),"utf8");
//					JcmsSSLClient sslClient=new JcmsSSLClient();
//					sslClient.setDestURL(urlToInvoke);
//					JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURL(loggerHead);
//					logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH ::URL "+urlToInvoke+"|Resp="+resp.getResponseData()+"|Error="+resp.getErrorMessage()+"|TimeToken="+resp.getResponseTime());
//				}
//			}
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,ActivationCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,String actionName,long msisdn){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getSystemId());
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getAdnetworkName());
			
			java.sql.Timestamp t=callbackData.getChargigTime();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(t.getTime());;
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getXactionId(), requestCategory, actionName, ""+msisdn, msisdn, (platform!=null?platform.getPlatform():"NA"), (platform!=null?platform.getSystemid():"NA"), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
						startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA",startDate, callbackData.getErrorCode(), callbackData.toString(), callbackData.getXactionId(), ""+callbackData.getMsisdn(), startDate, 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,false);
				if("ACT".equalsIgnoreCase(actionName)){
					//Update in case of non existing CDR
					newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					newCDR.setStatusDescp("Customer Activated");
					newCDR.setChargedPpid(cpp.getPpid());
					newCDR.setChargedPricePoint(cpp.getPricePoint());
					newCDR.setChargedAmount(cpp.getPricePoint());
					newCDR.setChargedOperatorKey(cpp.getOperatorKey());
					newCDR.setChargedOperatorParams(cpp.getOpParams());
					newCDR.setChargingDate(startDate);
					newCDR.setValidityStartDate(startDate);
					newCDR.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+newCDR.getChargedPpid());
				}
				
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+msisdn,""+msisdn,msisdn,
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",startDate,
						startDate,startDate,startDate,chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process",callbackData.getReferer(),"OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"NA","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),startDate);
				sub.setJocgTransId((callbackData.getId()!=null && callbackData.getId().contains("OAIRT_"))? callbackData.getId():"SDP-"+callbackData.getId());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
//				if("DCT".equalsIgnoreCase(actionName)){
//					if(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
//						String urlToInvoke="http://119.252.215.120/DCTNOTIFY?MSISDN="+java.net.URLEncoder.encode(callbackData.getMsisdn(),"utf8")+"&TransactionID="+java.net.URLEncoder.encode(callbackData.getXactionId(),"utf8")+"&OperatorID="+java.net.URLEncoder.encode(newCDR.getSourceNetworkCode(),"utf8")+"&VendorID="+java.net.URLEncoder.encode(newCDR.getTrafficSourceName(),"utf8")+"&Date="+java.net.URLEncoder.encode(""+callbackData.getChargigTime(),"utf8");
//						JcmsSSLClient sslClient=new JcmsSSLClient();
//						sslClient.setDestURL(urlToInvoke);
//						JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURL(loggerHead);
//						logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH ::URL "+urlToInvoke+"|Resp="+resp.getResponseData()+"|Error="+resp.getErrorMessage()+"|TimeToken="+resp.getResponseTime());
//					}
//				}
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,ActivationCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,String actionName,boolean activatedFromZero){
		loggerHead +="UCS::";
		try{
			
			
			//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			java.sql.Timestamp t=callbackData.getChargigTime();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(t.getTime());;
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("ACT".equalsIgnoreCase(actionName)){
				logger.debug("Updating CDR for "+chargedPricePoint);
				System.out.println(loggerHead+" Updating Successful Activation for "+chargedPricePoint.getPricePoint());
				if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
					cdr.setParkingToActivationDate(startDate);
					cdr.setActivatedFromParking(true);
				}
				cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
				cdr.setStatusDescp("Customer Activated");
				cdr.setChargedPpid(chargedPricePoint.getPpid());
				cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
				cdr.setChargedAmount(chargedPricePoint.getPricePoint());
				cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
				cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
				cdr.setChargingDate(startDate);
				cdr.setValidityStartDate(startDate);
				cdr.setValidityEndDate(endDate1);
				logger.debug("Charged PpId "+cdr.getChargedPpid());
			cdr.setSdpTransactionId(callbackData.getTemp2());
			cdr.setSdpStatusCode(callbackData.getErrorCode());
			cdr.setSdpStatusDescp(callbackData.toString());
			cdr.setSdpResponseTime(startDate);
			System.out.println(loggerHead+" CDR Updated for Successful Activation ");
			cdr.setJocgTransId((callbackData.getId()!=null && callbackData.getId().contains("OAIRT_"))? callbackData.getId():"SDP-"+callbackData.getId());
			
		}else if("DCT".equalsIgnoreCase(actionName) || "SUSPEND".equalsIgnoreCase(actionName)){
				if("DCT".equalsIgnoreCase(actionName)){
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(cdr.getChargingDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn=cal1.getTime();
					cal1.setTime(cdr.getRequestDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn1=cal1.getTime();
					cal1.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal1.getTime();
					SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
					String chargingDateStr=sdfnew.format(cdr.getChargingDate());
					String cDateStr=sdfnew.format(cDate);
					System.out.println(loggerHead+" DCT Dates(20min) : cdate "+cDate+"|expiry20MinChurn "+expiry20MinChurn+"| expiry20MinChurn1"+expiry20MinChurn1);
					if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
						cdr.setChurn20Min(true);
					}
					System.out.println(loggerHead+" DCT Dates(SameDay) : cdate "+cDateStr+"|expiry20MinChurn "+chargingDateStr);
					logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
					if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
						cdr.setChurnSameDay(true);
					}
					
					System.out.println(loggerHead+" DCT 20minchurnflag :"+cdr.getChurn20Min()+"|samedaychurn "+cdr.getChurnSameDay());
				}else {//if("SUSPEND".equalsIgnoreCase(actionName))
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(cdr.getChargingDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn=cal1.getTime();
					cal1.setTime(cdr.getRequestDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn1=cal1.getTime();
					cal1.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal1.getTime();
					SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
					String chargingDateStr=sdfnew.format(cdr.getChargingDate());
					String cDateStr=sdfnew.format(cDate);
					System.out.println(loggerHead+" SUSPEND Dates(20min) : cdate "+cDate+"|expiry20MinChurn "+expiry20MinChurn+"| expiry20MinChurn1"+expiry20MinChurn1);
					
					if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
						cdr.setChurn20Min(true);
					}
					System.out.println(loggerHead+" SUSPEND Dates(SameDay) : cdate "+cDateStr+"|expiry20MinChurn "+chargingDateStr);
					logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
					if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
						cdr.setChurnSameDay(true);
					}
					System.out.println(loggerHead+" SUSPEND 20minchurnflag :"+cdr.getChurn20Min()+"|samedaychurn "+cdr.getChurnSameDay());
				}
				//Create New CDR For DCT
				logger.debug(loggerHead+" Deactivation CDR updated for "+actionName+" status, now creating "+actionName+" CDR....");
				if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType(actionName);
					cdr1.setRequestDate(startDate);
					cdr1.setSdpResponseTime(startDate);
					cdr1.setSdpStatusCode(callbackData.getErrorCode());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setChargedAmount(0D);
					cdr1.setChargedPpid(0);
					cdr1.setChargedOperatorKey("NA");
					cdr1.setChargedOperatorParams("NA");
					cdr1.setChargedPricePoint(0D);
					cdr1.setChargedValidityPeriod("NA");
					cdr1.setChargingDate(startDate);
					cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
					if("SUSPEND".equalsIgnoreCase(actionName)){
						cdr.setStatusDescp("SUSPENDED");
					}else{
						cdr.setStatusDescp("Deactivated");
					}
					cdr1.setSdpTransactionId(callbackData.getTemp2());
					cdr1=cdrBuilder.insert(cdr1);
					System.out.println(loggerHead+" "+actionName+" new CDR created with id  :"+cdr1.getId()+", status "+cdr1.getStatus());
				}else{
					cdr.setRequestType(actionName);
					cdr.setRequestDate(startDate);
					cdr.setSdpResponseTime(startDate);
					cdr.setSdpStatusCode(callbackData.getErrorCode());
					cdr.setSdpStatusDescp(callbackData.toString());
					cdr.setChargedAmount(0D);
					cdr.setChargedPpid(0);
					cdr.setChargedOperatorKey("NA");
					cdr.setChargedOperatorParams("NA");
					cdr.setChargedPricePoint(0D);
					cdr.setChargedValidityPeriod("NA");
					cdr.setChargingDate(startDate);
					cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
					if("SUSPEND".equalsIgnoreCase(actionName)){
						cdr.setStatusDescp("SUSPENDED");
					}else{
						cdr.setStatusDescp("Deactivated");
					}
					cdr.setSdpTransactionId(callbackData.getTemp2());
					System.out.println(loggerHead+" "+actionName+" CDR updated with id  :"+cdr.getId()+", status "+cdr.getStatus());
				}
			
		}else if("REN".equalsIgnoreCase(actionName)){
			System.out.println(loggerHead+" "+actionName+" updateSameCDR : "+updateSameCDR+", activatedFromZero "+activatedFromZero);
				if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("REN");
					cdr1.setSdpResponseTime(startDate);
					cdr1.setSdpStatusCode(callbackData.getErrorCode());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(startDate);
					cdr1.setChargingDate(startDate);
					cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr1.setStatusDescp("Renewal Successful");
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr1.setChargedPpid(chargedPricePoint.getPpid());
						cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						cdr1.setActivatedFromZero(activatedFromZero);
					}
					cdr1.setSdpTransactionId(callbackData.getTemp2());
					cdr1=cdrBuilder.insert(cdr1);
					System.out.println(loggerHead+" "+actionName+" new CDR Created  : id "+cdr1.getId()+", status "+cdr1.getStatus()+", activatedFromZero "+cdr1.getActivatedFromZero());
				}else{
					cdr.setRequestType("REN");
					cdr.setSdpResponseTime(startDate);
					cdr.setSdpStatusCode(callbackData.getErrorCode());
					cdr.setSdpStatusDescp(callbackData.toString());
					cdr.setRequestDate(startDate);
					cdr.setChargingDate(startDate);
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Renewal Successful");
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr.setChargedPpid(chargedPricePoint.getPpid());
						cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						cdr.setActivatedFromZero(activatedFromZero);
					}
					cdr.setSdpTransactionId(callbackData.getTemp2());
					System.out.println(loggerHead+" "+actionName+"  CDR updated  : id "+cdr.getId()+", status "+cdr.getStatus()+", activatedFromZero "+cdr.getActivatedFromZero());
				}
			
		}else if("GRACE".equalsIgnoreCase(actionName)){
			if(updateSameCDR==false){
				ChargingCDR cdr1=copyCDR(loggerHead,cdr);
				cdr1.setRequestType("GRACE");
				cdr1.setSdpResponseTime(startDate);
				cdr1.setSdpStatusCode(callbackData.getErrorCode());
				cdr1.setSdpStatusDescp(callbackData.toString());
				cdr1.setRequestDate(startDate);
				cdr1.setChargingDate(startDate);
				cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
				if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
					cdr1.setChargedAmount(0D);
					cdr1.setChargedOperatorKey("NA");
					cdr1.setChargedOperatorParams("NA");
					cdr1.setChargedPpid(0);
					cdr1.setChargedPricePoint(0D);
					cdr1.setChargedValidityPeriod("NA");
					cdr1.setFallbackCharged(false);
					
				}
				cdr1.setSdpTransactionId(callbackData.getTemp2());
				cdrBuilder.insert(cdr1);
			}else{
					cdr.setRequestType("GRACE");
					cdr.setSdpResponseTime(startDate);
					cdr.setSdpStatusCode(callbackData.getErrorCode());
					cdr.setSdpStatusDescp(callbackData.toString());
					cdr.setRequestDate(startDate);
					cdr.setChargingDate(startDate);
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr.setChargedAmount(0D);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPpid(0);
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setFallbackCharged(false);
					}
					cdr.setSdpTransactionId(callbackData.getTemp2());
				}
			
		}

		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,ActivationCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,String actionName){
		loggerHead +="USS::";
		try{
			//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			java.sql.Timestamp t=callbackData.getChargigTime();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(t.getTime());;
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			
			
			if("ACT".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
				sub.setStatusDescp("Activation Successful");
				sub.setSubSessionRevenue(chargedPricePoint.getPricePoint());
				sub.setChargedAmount(chargedPricePoint.getPricePoint());
				sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
				sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
				sub.setSubChargedDate(startDate);
				sub.setValidityEndDate(endDate1);
				sub.setNextRenewalDate(endDate1);
				sub.setLastRenewalDate(startDate);
				sub.setActivationSource(callbackData.getReferer());
			}else if("DCT".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
				sub.setUnsubDate(startDate);
				//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
			}else if("SUSPEND".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
				sub.setSuspendDate(startDate);
						//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
				logger.debug(loggerHead+" :: SUSPEND :: Subscriber Status set to "+sub.getStatus());
			
			}else if("REN".equalsIgnoreCase(actionName)){
					Calendar cal1=Calendar.getInstance();
					cal.setTime(startDate);
					cal.add(Calendar.DATE, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(startDate);
					logger.debug(loggerHead+" :: REN :: Subscriber Status set to "+sub.getStatus());
			}else if("GRACE".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
				sub.setGraceDate(startDate);
						//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
				logger.debug(loggerHead+" :: GRACE :: Subscriber Status set to "+sub.getStatus());
			
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId){
	 Platform pl=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return null;
 }
	
}
