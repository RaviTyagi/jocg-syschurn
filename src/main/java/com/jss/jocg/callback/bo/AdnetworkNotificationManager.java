package com.jss.jocg.callback.bo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.json.JsonNotifyConfigConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyPercentageConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyUrlsConverter;
import com.jss.jocg.cache.entities.config.json.JsonSubcontrolConfigConverter;
import com.jss.jocg.cache.entities.config.json.NotifyConfig;
import com.jss.jocg.cache.entities.config.json.NotifyPercentage;
import com.jss.jocg.cache.entities.config.json.NotifyUrls;
import com.jss.jocg.cache.entities.config.json.SubcontrolConfig;
import com.jss.jocg.callback.data.IdeaSdpCallback;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class AdnetworkNotificationManager {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	private static final Logger logger = LoggerFactory
			.getLogger(AdnetworkNotificationManager.class);
	static long sentCounter=0;
    static Map<String,TrafficStats> trafficStatsLocal=new HashMap<>();
    
    public TrafficStats findTrafficStatsLocal(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId){
    	TrafficStats ts=null;
		publisherId=(publisherId==null)?"NA":publisherId.trim();
		String key="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId;
		String key2="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue();
		if(trafficStatsLocal.containsKey(key)){
			return trafficStatsLocal.get(key);
		}else if(trafficStatsLocal.containsKey(key2)){
			return trafficStatsLocal.get(key2);
		}else
			return null;
    }
    
    public void updateTrafficStatsLocal(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId,TrafficStats ts){
    	publisherId=(publisherId==null)?"NA":publisherId.trim();
		String key="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId;
		String key2="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue();
		if(!"NA".equalsIgnoreCase(publisherId)){
			trafficStatsLocal.put(key, ts);
		}else
			trafficStatsLocal.put(key2, ts);
    }
    
    
	@JmsListener(destination = "notify.adnetwork", containerFactory = "myFactory")
    public void receiveMessage(ChargingCDR notificationCDR) {
		String logHeader="S2SNotifyProcessor::"+notificationCDR.getMsisdn()+" ::";
		logger.debug(logHeader+"Sending Notification for "+notificationCDR);
		String token=notificationCDR.getTrafficSourceToken();
		logger.debug(logHeader+"requestCategory "+notificationCDR.getRequestCategory()+"|NotifyStatus : "+notificationCDR.getNotifyStatus());
		if("C".equalsIgnoreCase(notificationCDR.getRequestCategory()) && notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_QUEUED ){
			
			NotificationConfig appliedConfig=getNotificationConfig(notificationCDR);
			logger.debug(logHeader+"Applied Notification Config "+appliedConfig);
		
			NotifyConfig notifyConfig=null;NotifyPercentage notifyPercentage=null;
			NotifyUrls notifyUrls=null;SubcontrolConfig sc=null;TrafficStats ts=null;int percentageSent=0;
			try{
				JsonNotifyConfigConverter jncc=new JsonNotifyConfigConverter();
				notifyConfig=jncc.convertToEntityAttribute(appliedConfig.getNotifyConfig());
				
				JsonNotifyPercentageConverter jnpc=new JsonNotifyPercentageConverter();
				notifyPercentage=jnpc.convertToEntityAttribute(appliedConfig.getNotifyPercentage());
				
				JsonNotifyUrlsConverter jnuc=new JsonNotifyUrlsConverter();
				notifyUrls=jnuc.convertToEntityAttribute(appliedConfig.getNotifyUrls());
				
				JsonSubcontrolConfigConverter jscc=new JsonSubcontrolConfigConverter();
				sc=jscc.convertToEntityAttribute(appliedConfig.getSubcontrolConfig());
//				ts=findTrafficStatsLocal(notificationCDR.getChargingInterfaceId(), 
//						notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
//						notificationCDR.getPublisherId());
//				logger.debug("TrafficStats in LocalCache :"+ts);
//				
//				if(ts==null || ts.getSentCount()<=0){
					ts=jocgCache.getTrafficStats(notificationCDR.getChargingInterfaceId(), 
						notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
						notificationCDR.getPublisherId());
					logger.debug("TrafficStats in SystemCache :"+ts);
//				}else{
//					TrafficStats tscache=jocgCache.getTrafficStats(notificationCDR.getChargingInterfaceId(), 
//							notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
//							notificationCDR.getPublisherId());
//					logger.debug("TrafficStats in SystemCache :"+tscache);
//					if(tscache.getSuccessCount()!=ts.getSuccessCount()){
//						logger.debug("TrafficStats in local updated by System cache :"+tscache);
//						ts=tscache;
//						updateTrafficStatsLocal(notificationCDR.getChargingInterfaceId(), 
//								notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
//								notificationCDR.getPublisherId(),ts);
//					}
//				}
				
				if(ts!=null && ts.getSuccessCount()>0){
					//Deduct Minimum same day activation from sent Count
					int sentToCompare=ts.getSentCount()-sc.getMinsda();
					sentToCompare=(sentToCompare<=0)?0:sentToCompare;
					int successCountToCompare=ts.getSuccessCount()-sc.getMinsda();
					
					if(successCountToCompare>0)
						percentageSent=sentToCompare*100/successCountToCompare;
					else
						percentageSent=0;
					logger.debug(logHeader+"Notify Percentage Check sent:"+ts.getSentCount()+
							",successCount:"+ts.getSuccessCount()+", sentToCompare :"+sentToCompare+
							",successCountToCompare:"+successCountToCompare+",percentageSent="+percentageSent);
				}else{
					logger.debug(logHeader+"S2SNotification : Notify Percentage Check Failed, traffic stats is null or success_count<=0 , ts "+ts);
				}
			
			}catch(Exception e){
				logger.debug(logHeader+"S2SNotification : Exception while extracting percentage sent"+e);
			}
			
			logger.debug(logHeader+"S2SNotification : Extracted Notification configs "+notifyConfig+","+notifyPercentage+",  "+notifyUrls+", "+ts+", "+sc+",percentageSent="+percentageSent+",maxPercentageToSend="+notifyPercentage.getCharging());
			
			if(appliedConfig==null || notifyConfig==null || notifyUrls==null || notifyPercentage==null){
			
					notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
					notificationCDR.setNotifyDescp("S2SNotification: Notification Config not defined for this case.");
					notificationCDR.setNotifyTime(new java.util.Date());
				
			}else if((notificationCDR.getStatus()==JocgStatusCodes.CHARGING_SUCCESS &&  notifyConfig.getCharging()==true && notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_QUEUED)){
				boolean notifyFlag=true;
				//Checking for Percentage Sent
				if(percentageSent>=notifyPercentage.getCharging()){
					notifyFlag=false;
					notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
					notificationCDR.setNotifyDescp("Bolcked-Percentage Check");
					notificationCDR.setNotifyTime(new java.util.Date());
					logger.debug(logHeader+" S2SNotification Blocked due to Percentage check. percentageSent:"+percentageSent+",notifyPercentage.getCharging() "+notifyPercentage.getCharging());
				}else{
					logger.debug(logHeader+" Percentage check is not satisfied percentageSent:"+percentageSent+",notifyPercentage.getCharging() "+notifyPercentage.getCharging());
				}
				
				//Block Churned20 min if configured
				if(notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_QUEUED && notifyConfig.getChurn20min()==false && notificationCDR.getChurn20Min()==true){ 
					notifyFlag=false;
					notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
					notificationCDR.setNotifyDescp("Bolcked-20MinChurn");
					notificationCDR.setNotifyTime(new java.util.Date());
				}
				
				//Block Price Point if configured
				String skipPPType=notifyConfig.getSkippp().getType();
				Double skipPPValue=JocgString.strToDouble(notifyConfig.getSkippp().getValue(),0);
				if(notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_QUEUED && (skipPPType.equalsIgnoreCase("BELOW") && notificationCDR.getChargedAmount()<=skipPPValue) ||
						(skipPPType.equalsIgnoreCase("ABOVE") && notificationCDR.getChargedAmount()<=skipPPValue) ||
						(skipPPType.equalsIgnoreCase("EQUALTO") && notificationCDR.getChargedAmount()==skipPPValue)) {
					notifyFlag=false;
					notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
					notificationCDR.setNotifyDescp("Bolcked-Price Point Validation");
					notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
				}
				
				if(notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_QUEUED ){
					String url=(notificationCDR.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && notificationCDR.getChurnSameDay()==true && notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT)?
							notifyUrls.getChurn():notifyUrls.getCharging();
					logger.debug(logHeader+"Notification URL "+url);
					if(url!=null && url.startsWith("http://")){
						url=url+"?"+token;
						logger.debug(logHeader+" Notification URL "+url);
						JcmsSSLClient sslClient=new JcmsSSLClient();
						JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader, url);
						
						logger.debug(logHeader+" Notification URL Response "+resp.getResponseData()+", response time "+resp.getResponseTime()+"ms");
						logger.debug(logHeader+"Detailed Notification process Logs "+resp.getProcessLogs());
						if(resp.getResponseCode()==200){
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_SENT);
							notificationCDR.setNotifyDescp("S2S Pushed Successfully");
						}else{
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
							notificationCDR.setNotifyDescp("S2S Push Failed");
						}
						
//						notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_SENT);
//						notificationCDR.setNotifyDescp("S2S Pushed Successfully");
						notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
						if(ts!=null){
							ts.setSentCount(ts.getSentCount()+1);
							updateTrafficStatsLocal(notificationCDR.getChargingInterfaceId(), 
									notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
									notificationCDR.getPublisherId(),ts);
							logger.debug(logHeader+" Local Cache Updated from campaignId "+notificationCDR.getCampaignId()+" with "+ts);
						}
						
					}else{
						logger.debug(logHeader+" Invalid Notification URL, Notification Ignored!");
						
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
							notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
						
					}
				}else{
					logger.debug(logHeader+"Notification Blocked!");
						notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
						notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
					
				}
				
			}else{
				logger.debug(logHeader+" Notification Ignored!");
				
				notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
				notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
				
			}
			try{
				cdrBuilder.save(notificationCDR);
			}catch(Exception e){
				logger.debug(logHeader+" Exception while saving notificationCDR id after notification process "+notificationCDR.getId());
			}
			
		}else{
			logger.debug(logHeader+" Notification Ignored!");
			notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
			notificationCDR.setNotifyDescp("Bolcked-Category "+notificationCDR.getRequestCategory()+", Notify Status  "+notificationCDR.getNotifyStatus());
			try{
				cdrBuilder.save(notificationCDR);
			}catch(Exception e){}
			
		}
	}
	
	@JmsListener(destination = "notify.adnetworkchurn", containerFactory = "myFactory")
    public void receiveChurnMessage(ChargingCDR notificationCDR) {
		String logHeader="S2SNotifyChurnProcessor::"+notificationCDR.getMsisdn()+" ::";
		logger.debug(logHeader+"Sending Notification for "+notificationCDR);
		String token=notificationCDR.getTrafficSourceToken();
		logger.debug(logHeader+"requestCategory "+notificationCDR.getRequestCategory()+"|NotifyStatus : "+notificationCDR.getNotifyStatus());
		if("C".equalsIgnoreCase(notificationCDR.getRequestCategory()) && notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT && notificationCDR.getNotifyChurnStatus()== JocgStatusCodes.NOTIFICATION_QUEUED){
			
			NotificationConfig appliedConfig=getNotificationConfig(notificationCDR);
			logger.debug(logHeader+"Applied Notification Config "+appliedConfig);
		
			NotifyConfig notifyConfig=null;NotifyPercentage notifyPercentage=null;NotifyUrls notifyUrls=null;SubcontrolConfig sc=null;TrafficStats ts=null;int percentageSent=0;
			try{
				JsonNotifyConfigConverter jncc=new JsonNotifyConfigConverter();
				notifyConfig=jncc.convertToEntityAttribute(appliedConfig.getNotifyConfig());
				
				JsonNotifyPercentageConverter jnpc=new JsonNotifyPercentageConverter();
				notifyPercentage=jnpc.convertToEntityAttribute(appliedConfig.getNotifyPercentage());
				
				JsonNotifyUrlsConverter jnuc=new JsonNotifyUrlsConverter();
				notifyUrls=jnuc.convertToEntityAttribute(appliedConfig.getNotifyUrls());
				
				JsonSubcontrolConfigConverter jscc=new JsonSubcontrolConfigConverter();
				sc=jscc.convertToEntityAttribute(appliedConfig.getSubcontrolConfig());
				ts=jocgCache.getTrafficStats(notificationCDR.getChargingInterfaceId(), 
						notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
						notificationCDR.getPublisherId());
				if(ts!=null && ts.getSuccessCount()>0){
					percentageSent=ts.getSentCount()*100/ts.getSuccessCount();
				}
			
			}catch(Exception e){}
			
			logger.debug(logHeader+"Extracted Notification configs "+notifyConfig+","+notifyPercentage+",  "+notifyUrls+", "+ts+", "+sc);
			
			if(appliedConfig==null || notifyConfig==null || notifyUrls==null || notifyPercentage==null){
					notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_FAILED);
					notificationCDR.setNotifyScheduleTime(new java.util.Date());
				
			}else if(notificationCDR.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && notificationCDR.getChurnSameDay()==true 
					&& notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT && notificationCDR.getNotifyChurnStatus()==JocgStatusCodes.NOTIFICATION_QUEUED){
				
					String url=notifyUrls.getChurn();
					logger.debug(logHeader+"Notification URL "+url);
					if(url!=null && url.startsWith("http://")){
						url=url+"?"+token;
						logger.debug(logHeader+" Notification URL "+url);
						JcmsSSLClient sslClient=new JcmsSSLClient();
						JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader, url);
						logger.debug(logHeader+" Notification URL Response "+resp.getResponseData()+", response time "+resp.getResponseTime()+"ms");
						logger.debug(logHeader+"Detailed Notification process Logs "+resp.getProcessLogs());
						if(notificationCDR.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && notificationCDR.getChurnSameDay()==true && notificationCDR.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
							if(resp.getResponseCode()==200){
								logger.debug(logHeader+" S2S Notification Sent!");
								notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_SENT);
							}else{
								logger.debug(logHeader+" S2S Notification Failed!");
								notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_FAILED);
							}
							notificationCDR.setNotifyScheduleTime(new java.util.Date(System.currentTimeMillis()));
						}else{
							if(resp.getResponseCode()==200){
								logger.debug(logHeader+" S2S Notification Sent!");
								notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_SENT);
							}else{
								logger.debug(logHeader+" S2S Notification Failed!");
								notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
							}
							notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
						}
					}else{
						logger.debug(logHeader+" Invalid Notification URL, Notification Ignored!");
							notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_FAILED);
							notificationCDR.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
						
					}
				
				
			}else{
				logger.debug(logHeader+" Notification Ignored!");
					notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
					notificationCDR.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
				
			}
			try{
				cdrBuilder.save(notificationCDR);
			}catch(Exception e){}
			
		}else{
			logger.debug(logHeader+" Notification Ignored!");
			notificationCDR.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
			notificationCDR.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
			try{
				cdrBuilder.save(notificationCDR);
			}catch(Exception e){}
			
		}
	}
	
	
	public NotificationConfig getNotificationConfig(ChargingCDR notificationCDR){
		NotificationConfig appliedConfig=null;
		try{
		List<NotificationConfig> nc=jocgCache.getNotificationConfig(notificationCDR.getChargingInterfaceId(),notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), notificationCDR.getPublisherId());
		logger.debug("Searching Notification Config into "+nc);
		
		//Search Exact Match including publisher id
		for(NotificationConfig c:nc){
			appliedConfig=(c.getPubid().equalsIgnoreCase(notificationCDR.getPublisherId()) && 
					c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
					c.getTsourceid()==notificationCDR.getTrafficSourceId() && 
					c.getCampaignid()==notificationCDR.getCampaignId())?c:null;
			
			if(appliedConfig!=null) break;
		}
		//Search Exact Match excluding publisher id
		for(NotificationConfig c:nc){
			appliedConfig=(appliedConfig==null &&
					c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
					c.getTsourceid()==notificationCDR.getTrafficSourceId() && 
					c.getCampaignid()==notificationCDR.getCampaignId())?c:appliedConfig;
			if(appliedConfig!=null) break;
		}
		
		//Search  Match pfr chintf and tsource only
		for(NotificationConfig c:nc){
			appliedConfig=(appliedConfig==null &&
					c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
					c.getTsourceid()==notificationCDR.getTrafficSourceId())?c:appliedConfig;
			if(appliedConfig!=null) break;
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return appliedConfig;
	}
	
}
