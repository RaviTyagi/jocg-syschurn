package com.jss.jocg.callback.bo;
/**
 * Not in Use as Offline dump is being processed inside schedular only, Kept for future usage in case offline dump traffic gets increased
 */
 
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.callback.data.AirtelIndiaOfflineDumpRecord;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;

import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class AirtelIndiaSDPOfflineCallbackReceiver {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelIndiaSDPOfflineCallbackReceiver.class);
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");

	/**
	 Subscriber Status Codes:
	Pending/NewSub-0
	CGSUCCESS=1
	PARKING=2
	GRACE=3
	ACTIVE=5
	CGFAILED=-1
	SUSPENDEDFROMPARKING=-2
	SUSPENDEDFROMGRACE=-3
	UNSUB=-5

	 */		


	// zcat ht_SAPI_SAPI_DIGIVIVE_150075_02_Nov_2017.csv.gz|grep -v "Customer is in Grace Period"|grep -v "New-Subscription Fail" >
	
	
	
	//@JmsListener(destination = "callback.airtelsdpoffline", containerFactory = "myFactory")
    public void receiveMessage(AirtelIndiaOfflineDumpRecord callbackData) {
		String loggerHead="AirtelOfflineReceiver::msisdn="+callbackData.getMsisdn()+"|";
	
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			Long msisdn=callbackData.getMsisdn();
			if(callbackData!=null && msisdn>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+msisdn+", Key "+callbackData.getProductId()+", Amount "+callbackData.getAmount()+", Balance "+callbackData.getBalance());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getProductId());
				
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+msisdn+", Operator Service Key "+serviceKey);
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(msisdn,serviceKey);	
					
					ChargingCDR cdr=null;
					
					String action=(callbackData.getTransType().contains("New-Subscription") && "1000".equalsIgnoreCase(callbackData.getErrorCode()))?"ACT":
						(callbackData.getTransType().contains("Re-Subscription") && "1000".equalsIgnoreCase(callbackData.getErrorCode()))?"REN":
						(callbackData.getTransType().contains("Re-Subscription") && "3027".equalsIgnoreCase(callbackData.getErrorCode()))?"SUSPEND":
								(callbackData.getTransType().contains("Re-Subscription") && "3404".equalsIgnoreCase(callbackData.getErrorCode()))?"GRACE":
							(callbackData.getTransType().contains("De-Subscription"))?"DCT":
								callbackData.getTransType().contains("Re-Subscription")?"REN-FAILED":
									callbackData.getTransType().contains("New-Subscription")?"ACT-FAILED":"OTHER";
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						if("OTHER".equalsIgnoreCase(action) || "REN-FAILED".equalsIgnoreCase(action)){
							logger.debug(loggerHead+"Action-"+action+", Ignored Callback "+callbackData);
						}if("REN".equalsIgnoreCase(action) && lastRenewaldate!=null && (sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(callbackData.getTransTime())) || lastRenewaldate.after(callbackData.getTransTime()))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if("DCT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							
							if(cdr!=null && cdr.getId()!=null){
								logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode()+"");
								handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr,action);
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
							}
						}else{
							logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
						}
					}else{
						if("ACT".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
							logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
							handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
						}else{
							logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
						}
						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
			
		}
        
    }
	
	
	
	public void handleExistingSubscriber(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr,String actionName){
		loggerHead +="HES::";
		try{
			logger.debug(loggerHead+" Processing for existing custmer CDR ......."+actionName+", status "+callbackData.getStatus());
			boolean activatedFromZero=(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && 
					cdr.getChargedPricePoint().doubleValue()==0D && cpp.getPricePoint()>0 && 
					sub.getChargedAmount().doubleValue()==0D)?true:false;
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,actionName,activatedFromZero);
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(actionName) ){
				if("1000".equalsIgnoreCase(callbackData.getErrorCode())){
					//Disabled sms and notification in case of offline dump activation
//					logger.debug(loggerHead+" Sending Activation SMS .......");
//					JocgSMSMessage smsMessage=new JocgSMSMessage();
//					smsMessage.setMsisdn(callbackData.getMsisdn());
//					smsMessage.setSmsInterface("smsjust");
//					smsMessage.setChintfId(1);
//					smsMessage.setTextMessage("Successful Activation");
//					jmsTemplate.convertAndSend("notify.sms", smsMessage);
//					logger.debug(loggerHead+" Activation SMS Queued .......SMSData="+smsMessage);
					//Setting Notification
//					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					Calendar cal=Calendar.getInstance();
//					cal.setTimeInMillis(System.currentTimeMillis());
//					cal.add(Calendar.MINUTE, 60);
//					cdr.setNotifyScheduleTime(cal.getTime());
					//cdr.setNotifyChurn(true);
//					logger.debug(loggerHead+" S2S Notification Queued, Churn Status Enabled!");
					
				}
				cdrBuilder.save(cdr);
			}else if("DCT".equalsIgnoreCase(actionName)){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,String actionName){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
			String requestCategory="OP-OFFLINEDUMP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(callbackData.getTransTime().getTime());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, actionName, sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
					startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", callbackData.getTransTime(), "NA", "NA", "NA", callbackData.getTransTime(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getTransactionId(), ""+callbackData.getMsisdn(), callbackData.getTransTime(), 
					null, null, false, false, false);
			
			boolean activatedFromZero=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && sub.getChargedAmount().doubleValue()==0D)?true:false;
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,activatedFromZero);
			if("ACT".equalsIgnoreCase(actionName) || "REN".equalsIgnoreCase(actionName) && "1000".equalsIgnoreCase(callbackData.getErrorCode())){
				//Update in case of non existing CDR
				newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
				newCDR.setStatusDescp("Customer Activated");
				newCDR.setChargedPpid(cpp.getPpid());
				newCDR.setChargedPricePoint(cpp.getPricePoint());
				newCDR.setChargedAmount(cpp.getPricePoint());
				newCDR.setChargedOperatorKey(cpp.getOperatorKey());
				newCDR.setChargedOperatorParams(cpp.getOpParams());
				newCDR.setChargingDate(startDate);
				newCDR.setValidityStartDate(startDate);
				newCDR.setValidityEndDate(endDate1);
				logger.debug("Charged PpId "+newCDR.getChargedPpid());
			}
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(actionName)) sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,String actionName,long msisdn){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-OFFLINEDUMP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(callbackData.getTransTime().getTime());;
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getTransactionId(), requestCategory, actionName, ""+msisdn, msisdn, "NA", "NA", 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(),callbackData.getReferer(), 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", callbackData.getTransTime(), "NA", "NA", "NA", callbackData.getTransTime(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getTransactionId(), ""+callbackData.getMsisdn(), callbackData.getTransTime(), 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,false);
				if("ACT".equalsIgnoreCase(actionName) || "REN".equalsIgnoreCase(actionName) && "1000".equalsIgnoreCase(callbackData.getErrorCode())){
					//Update in case of non existing CDR
					newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					newCDR.setStatusDescp("Customer Activated");
					newCDR.setChargedPpid(cpp.getPpid());
					newCDR.setChargedPricePoint(cpp.getPricePoint());
					newCDR.setChargedAmount(cpp.getPricePoint());
					newCDR.setChargedOperatorKey(cpp.getOperatorKey());
					newCDR.setChargedOperatorParams(cpp.getOpParams());
					newCDR.setChargingDate(startDate);
					newCDR.setValidityStartDate(startDate);
					newCDR.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+newCDR.getChargedPpid());
				}
				
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+msisdn,""+msisdn,msisdn,
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",callbackData.getTransTime(),
						callbackData.getTransTime(),callbackData.getTransTime(),callbackData.getTransTime(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process",callbackData.getReferer(),"OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"NA","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),callbackData.getTransTime());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,String actionName,boolean activatedFromZero){
		loggerHead +="UCS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(callbackData.getTransTime().getTime());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("ACT".equalsIgnoreCase(actionName)){
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					if(chargedPricePoint.getPricePoint()<=0) cdr.setActivatedFromZero(true);
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				cdr.setSdpStatusCode(callbackData.getErrorCode());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(callbackData.getTransTime());
				
				
			}else if("DCT".equalsIgnoreCase(actionName) || "SUSPEND".equalsIgnoreCase(actionName)){
					if("DCT".equalsIgnoreCase(actionName)){
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(cdr.getChargingDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn=cal1.getTime();
						cal1.setTime(cdr.getRequestDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn1=cal1.getTime();
						cal1.setTimeInMillis(System.currentTimeMillis());
						java.util.Date cDate=cal1.getTime();
						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
						String cDateStr=sdfnew.format(cDate);
						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
							cdr.setChurn20Min(true);
						}
						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
							cdr.setChurnSameDay(true);
						}
					}else {//if("SUSPEND".equalsIgnoreCase(actionName))
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(cdr.getChargingDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn=cal1.getTime();
						cal1.setTime(cdr.getRequestDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn1=cal1.getTime();
						cal1.setTimeInMillis(System.currentTimeMillis());
						java.util.Date cDate=cal1.getTime();
						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
						String cDateStr=sdfnew.format(cDate);
						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
							cdr.setChurn20Min(true);
						}
						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
							cdr.setChurnSameDay(true);
						}
					}
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType(actionName);
						cdr1.setRequestDate(callbackData.getTransTime());
						cdr1.setSdpResponseTime(callbackData.getTransTime());
						cdr1.setSdpStatusCode(callbackData.getErrorCode());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setChargingDate(startDate);
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType(actionName);
						cdr.setRequestDate(callbackData.getTransTime());
						cdr.setSdpResponseTime(callbackData.getTransTime());
						cdr.setSdpStatusCode(callbackData.getErrorCode());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setChargingDate(startDate);
					}
				
			}else if("REN".equalsIgnoreCase(actionName)){
				if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("REN");
					cdr1.setActivatedFromZero(activatedFromZero);
					cdr1.setSdpResponseTime(callbackData.getTransTime());
					cdr1.setChargingDate(callbackData.getTransTime());
					cdr1.setSdpStatusCode(callbackData.getErrorCode());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(callbackData.getTransTime());
					cdr1.setChargingDate(startDate);
					
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr1.setStatusDescp("Renewal Successful");
						cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr1.setChargedPpid(chargedPricePoint.getPpid());
						cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}
					cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("REN");
						cdr.setSdpResponseTime(callbackData.getTransTime());
						cdr.setSdpStatusCode(callbackData.getErrorCode());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(callbackData.getTransTime());
						cdr.setChargingDate(startDate);
						
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr.setStatusDescp("Renewal Successful");
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
					}
				
			}else if("GRACE".equalsIgnoreCase(actionName)){
				if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("GRACE");
					cdr1.setSdpResponseTime(callbackData.getTransTime());
					cdr1.setSdpStatusCode(callbackData.getErrorCode());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(callbackData.getTransTime());
					cdr1.setChargingDate(startDate);
					cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setChargedAmount(0D);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPpid(0);
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setFallbackCharged(false);
						
					}
					cdrBuilder.insert(cdr1);
				}else{
						cdr.setRequestType("GRACE");
						cdr.setSdpResponseTime(callbackData.getTransTime());
						cdr.setSdpStatusCode(callbackData.getErrorCode());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(callbackData.getTransTime());
						cdr.setChargingDate(startDate);
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setChargedAmount(0D);
							cdr.setChargedOperatorKey("NA");
							cdr.setChargedOperatorParams("NA");
							cdr.setChargedPpid(0);
							cdr.setChargedPricePoint(0D);
							cdr.setChargedValidityPeriod("NA");
							cdr.setFallbackCharged(false);
						}
					}
				
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,String actionName){
		loggerHead +="USS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(callbackData.getTransTime().getTime());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("ACT".equalsIgnoreCase(actionName)){
				if("1000".equalsIgnoreCase(callbackData.getErrorCode())){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getAmount());
					cal.setTime(endDate1);
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					if(chargedPricePoint.getPricePoint()<=0) sub.setUserField_9("ZERO-PRICE-ACT");
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
				}else{
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
					sub.setChargedAmount(0D);
					sub.setStatusDescp("Charging Failed at SDP|SDP Status="+callbackData.getErrorCode()+",Channel="+callbackData.getChannelName()+",Amount="+callbackData.getAmount());
				}
				sub.setSubChargedDate(startDate);
				sub.setActivationSource(callbackData.getReferer());
			}else if("DCT".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
				sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
				//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
			}else if("SUSPEND".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
				sub.setSuspendDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
				logger.debug(loggerHead+" :: SUSPEND :: Subscriber Status set to "+sub.getStatus());
			
			}else if("REN".equalsIgnoreCase(actionName)){
					Calendar cal1=Calendar.getInstance();
					cal.setTime(startDate);
					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(startDate);
					logger.debug(loggerHead+" :: REN :: Subscriber Status set to "+sub.getStatus());
			}else if("GRACE".equalsIgnoreCase(actionName)){
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
				sub.setGraceDate(startDate);
						//sub.setChargedAmount(0D);
				sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
				logger.debug(loggerHead+" :: GRACE :: Subscriber Status set to "+sub.getStatus());
			
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	
}
