package com.jss.jocg.callback.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.callback.data.voda.VodafoneIndiaSDPCallback;
import com.jss.jocg.charging.handlers.JocgChargingHandler;
import com.jss.jocg.charging.handlers.JocgChargingHandlerImpl;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.CallbackService;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.CircleIdentifier;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

import ch.qos.logback.core.joran.action.NewRuleAction;


@Component
public class VodafoneIndiaSDPCallbackReceiver{
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	 @Autowired public CircleIdentifier circleIndentifier;
	private static final Logger logger = LoggerFactory
			.getLogger(VodafoneIndiaSDPCallbackReceiver.class);
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");

	/**
	 Subscriber Status Codes:
	Pending/NewSub-0
	CGSUCCESS=1
	PARKING=2
	GRACE=3
	ACTIVE=5
	CGFAILED=-1
	SUSPENDEDFROMPARKING=-2
	SUSPENDEDFROMGRACE=-3
	UNSUB=-5

	 */		

	
	
	@JmsListener(destination = "callback.vodasdp", containerFactory = "myFactory")
	 public void receiveMessage(VodafoneIndiaSDPCallback callbackData) {
		processMessage(callbackData);
	}
	
    public void processMessage(VodafoneIndiaSDPCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+"|";
	
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			if(callbackData!=null && callbackData.getMsisdn()>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getServiceKey()+", Price "+callbackData.getPrice());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(), callbackData.getPrice());
				logger.debug(loggerHead+"CPP based on price and key "+cpp);
				int validity="MONTHLY".equalsIgnoreCase(callbackData.getMode())?30:
					("21DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY21".equalsIgnoreCase(callbackData.getMode()))?21:
				     	("15DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY15".equalsIgnoreCase(callbackData.getMode()))?15:
				     	("10DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY10".equalsIgnoreCase(callbackData.getMode()))?10:
				     	("WEEKLY".equalsIgnoreCase(callbackData.getMode()) || "7DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY7".equalsIgnoreCase(callbackData.getMode()))?7:
				     	("5DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY5".equalsIgnoreCase(callbackData.getMode()))?5:
				    	("3DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY3".equalsIgnoreCase(callbackData.getMode()))?3:
				    	("1DAY".equalsIgnoreCase(callbackData.getMode()) || "1DAYS".equalsIgnoreCase(callbackData.getMode()) || "DAILY1".equalsIgnoreCase(callbackData.getMode()) || "DAILY".equalsIgnoreCase(callbackData.getMode()))?1:
				    	0;
				logger.debug(loggerHead+"Validity "+validity);
				if(cpp==null || cpp.getPricePoint()!=callbackData.getPrice()){
					//Search on Validity
					cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(),validity);
					logger.debug(loggerHead+"CPP based on validity "+validity);
				}
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=""+callbackData.getMsisdn().longValue();
					if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+callbackData.getMsisdn().longValue(),callbackData.getMsisdn()));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn().longValue()+", Operator Service Key "+serviceKey);
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(callbackData.getMsisdn(),serviceKey);	
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						
						String operatorTransId=callbackData.getTxnId();
						operatorTransId=(operatorTransId==null || operatorTransId.length()<=0)?"NA":operatorTransId.trim();
						cdr=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(cppmaster.getChintfid(), operatorTransId);
						//if("ACT".equalsIgnoreCase(callbackData.getAction()) && callbackData.getReferenceId().startsWith("PRISM")) callbackData.setAction("REN");
						boolean duplicateRequest=false;
						if(cdr!=null && cdr.getRequestType()!=null && cdr.getRequestType().equalsIgnoreCase(callbackData.getAction()) && !"NA".equalsIgnoreCase(operatorTransId)){
							duplicateRequest=true;
							logger.error(loggerHead+" Originator Id "+operatorTransId+" for msisdn  "+callbackData.getMsisdn().longValue()+" is already processed against CDR Id "+cdr.getId());
						}else{
							duplicateRequest=false;
						}
						
						
						/*if("ACT".equalsIgnoreCase(callbackData.getAction()) && callbackData.getReferenceId().startsWith("PRISM")) callbackData.setAction("REN");
						
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						if("REN".equalsIgnoreCase(callbackData.getAction()) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(new java.util.Date()))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if("DCT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && sub.getValidityEndDate().after(new java.util.Date(System.currentTimeMillis()))){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
						}*/
						
						if(duplicateRequest){
							logger.debug(loggerHead+" Duplicate Callback for Action "+callbackData.getAction()+"TxnId "+operatorTransId+", Hence Ignored");
						}else{
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null){
								if("DCT".equalsIgnoreCase(cdr.getRequestType())){
									logger.debug(loggerHead+"  CDR Found but for DCT, Process for Existing Subscriber and Non Existing CDR...");
									handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
								}else{
									logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
									handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
								}
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
			
		}catch(Exception e){
			logger.debug(loggerHead+" :: Exception "+e);
			e.printStackTrace();
		}
        
    }
	
	public void handleExistingSubscriber(String loggerHead,VodafoneIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";
		try{
			logger.debug(loggerHead+" Processing for existing custmer CDR ......."+callbackData.getAction()+", status "+callbackData.getStatus());
			
			boolean activatedFromZero=(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING && 
					callbackData.getReferenceId().startsWith("PRISM") && cpp.getPricePoint()>0 && 
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING && sub.getChargedAmount().doubleValue()==0D)?true:false;
			
			logger.debug(loggerHead+"AvtivatedFromZero:Validation {Action:"+callbackData.getAction()+",subId :"+sub.getId()+",status:"+sub.getStatus()+",previousChargedAmount:"+sub.getChargedAmount().doubleValue()+",currentChargedAmount:"+cpp.getPricePoint()+",activatedFromZero:"+activatedFromZero+"}");
			
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			if("DCT".equalsIgnoreCase(callbackData.getAction())){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				String dateAct=sdfdate.format(sub.getSubChargedDate());
				
				//String dateDct=sdfdate.format(callbackData.getStartDate());
				String dateDct=sdfdate.format(new java.util.Date(System.currentTimeMillis()));
				sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
				logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
						//String timeStr=callbackData.getStartDate()+" "+callbackData.getStartTime();
						SimpleDateFormat sdfDateTime=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						//java.util.Date unsubTime=sdfDateTime.parse(timeStr);
						Calendar cal=Calendar.getInstance();
						cal.setTimeInMillis(System.currentTimeMillis());
						java.util.Date unsubTime=cal.getTime();
						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.before(unsubTime)|| expiry20Minchurn.equals(unsubTime))?true:false;
						churn24Hour=(expiry24Hourchurn.before(unsubTime)|| expiry24Hourchurn.equals(unsubTime))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(unsubTime)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}
			
			}
			
			
			
			
			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,activatedFromZero,sameDayChurn,churn20Min,churn24Hour);
			Circleinfo circle=circleIndentifier.identifyCircle("127.0.0.1", "", callbackData.getMsisdn());
			
			if(circle.getCircleid()>0 && sub.getCircleId()<=0){
				sub.setCircleId(circle.getCircleid());
				sub.setCircleName(circle.getCirclename());
				sub.setCircleCode(circle.getCirclecode());
			}
			if(circle.getCircleid()>0 && cdr.getCircleId()<=0){
				cdr.setCircleId(circle.getCircleid());
				cdr.setCircleName(circle.getCirclename());
				cdr.setCircleCode(circle.getCirclecode());
			}
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(callbackData.getAction()) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					logger.debug(loggerHead+" Sending Activation SMS .......");
					JocgSMSMessage smsMessage=new JocgSMSMessage();
					smsMessage.setMsisdn(callbackData.getMsisdn());
					smsMessage.setSmsInterface("vodafonein");
					smsMessage.setServiceName(sub.getServiceName());
					smsMessage.setChintfId(sub.getChargingInterfaceId());
					smsMessage.setChargingInterfaceType(sub.getChargingInterfaceType());
					smsMessage.setTextMessage("Successful Activation");
					jmsTemplate.convertAndSend("notify.sms", smsMessage);
					logger.debug(loggerHead+" Activation SMS Queued .......SMSData="+smsMessage);
					//Setting Notification
					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					cal.add(Calendar.MINUTE, 60);
					cdr.setNotifyScheduleTime(cal.getTime());
					cdr.setNotifyChurn(true);
					logger.debug(loggerHead+" S2S Notification Queued, Churn Status Enabled!");
					
				}
				cdrBuilder.save(cdr);
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,VodafoneIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			double prevChargedAmount=sub.getChargedAmount();
			boolean activatedFromZero=("REN".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && 
					prevChargedAmount==0D && cpp.getPricePoint()>0)?true:false;
			logger.debug(loggerHead+"AvtivatedFromZero:Validation {Action:"+callbackData.getAction()+",subId :"+sub.getId()+",status:"+sub.getStatus()+",previousChargedAmount:"+prevChargedAmount+",currentChargedAmount:"+cpp.getPricePoint()+",activatedFromZero:"+activatedFromZero+"}");
		
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			if("DCT".equalsIgnoreCase(callbackData.getAction())){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				String dateAct=sdfdate.format(sub.getSubChargedDate());
				//String dateDct=sdfdate.format(callbackData.getStartDate());
				String dateDct=sdfdate.format(new java.util.Date(System.currentTimeMillis()));
				sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
				logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
//						String timeStr=callbackData.getStartDate()+" "+callbackData.getStartTime();
						SimpleDateFormat sdfDateTime=new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//						java.util.Date unsubTime=sdfDateTime.parse(timeStr);
						Calendar cal=Calendar.getInstance();
						cal.setTimeInMillis(System.currentTimeMillis());
						java.util.Date unsubTime=cal.getTime();
						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.before(unsubTime)|| expiry20Minchurn.equals(unsubTime))?true:false;
						churn24Hour=(expiry24Hourchurn.before(unsubTime)|| expiry24Hourchurn.equals(unsubTime))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(unsubTime)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}
			
			}
			
			
			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			if(circle.getCircleid()<=0)	circle=circleIndentifier.identifyCircle("127.0.0.1", "", callbackData.getMsisdn());
			if(circle.getCircleid()>0 && sub.getCircleId()<=0){
				sub.setCircleId(circle.getCircleid());
				sub.setCircleName(circle.getCirclename());
				sub.setCircleCode(circle.getCirclecode());
			}
			
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			
			
			
			
			
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
//			java.util.Date startDate=sdf.parse(callbackData.getStartDate());
//			java.util.Date endDate1=sdf.parse(callbackData.getEndDate());
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			java.util.Date endDate1=cal.getTime();
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, callbackData.getAction(), sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatus(), callbackData.toString(), callbackData.getReferenceId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
					null, null, false, false, false);
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,activatedFromZero,sameDayChurn,churn20Min,churn24Hour);
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(callbackData.getAction())){ sub.setJocgCDRId(newCDR.getId());
			sub.setSubChargedDate(startDate);
			}else if ("DCT".equalsIgnoreCase(callbackData.getAction()))sub.setUnsubCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,VodafoneIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			if(circle.getCircleid()<=0)	circle=circleIndentifier.identifyCircle("127.0.0.1", "", callbackData.getMsisdn());
			
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			java.util.Date endDate1=cal.getTime();
			
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getReferenceId(), requestCategory, callbackData.getAction(), ""+callbackData.getMsisdn(), callbackData.getMsisdn(), callbackData.getOriginator(), callbackData.getOriginator(), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatus(), callbackData.toString(), callbackData.getReferenceId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				newCDR.setSubRegDate(startDate);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,false,false,false,false);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getMsisdn(),callbackData.getMsisdn(),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,callbackData.getOriginator(),callbackData.getOriginator(),"OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),new java.util.Date());
				
				if("ACT".equalsIgnoreCase(callbackData.getAction())) {sub.setJocgCDRId(newCDR.getId());
				}else if ("DCT".equalsIgnoreCase(callbackData.getAction()))sub.setUnsubCDRId(newCDR.getId());
				sub.setSubChargedDate(startDate);
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,VodafoneIndiaSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,
			boolean activatedFromZero,boolean sameDayChurn,boolean churn20Min,boolean churn24Hour){
		loggerHead +="UCS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal1=Calendar.getInstance();
			//cal1.setTimeInMillis(sdf.parse(callbackData.getStartDate()).getTime());
			cal1.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal1.getTime();
			cal1.add(Calendar.DATE, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal1.getTime();
//			java.util.Date startDate=sdf.parse(callbackData.getStartDate());
//			java.util.Date endDate1=sdf.parse(callbackData.getEndDate());
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setActivatedFromZero(activatedFromZero);
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					cdr.setSubRegDate(startDate);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if("UNSUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					if("PARKING".equalsIgnoreCase(callbackData.getMode())){
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
					}else{
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
					}
					cdr.setSubRegDate(startDate);
				}
				cdr.setSdpStatusCode(callbackData.getStatus());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
//					if("DEACTIVATE".equalsIgnoreCase(callbackData.getMode())){
//						Calendar cal=Calendar.getInstance();
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn1=cal.getTime();
//						
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn1=cal.getTime();
//						
//						cal.setTimeInMillis(System.currentTimeMillis());
//						java.util.Date cDate=cal.getTime();
//						
//						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
//						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
//						String cDateStr=sdfnew.format(cDate);
//						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
//							cdr.setChurn20Min(true);
//						}
//						
//						if(cDate.before(expiry24HourChurn) || cDate.before(expiry24HourChurn1)){
//							cdr.setChurn24Hour(true);
//						}
//						
//						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
//						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
//							cdr.setChurnSameDay(true);
//						}
//					}else if("SUSPEND".equalsIgnoreCase(callbackData.getMode())){
//						Calendar cal=Calendar.getInstance();
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn1=cal.getTime();
//						
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn1=cal.getTime();
//						
//						cal.setTimeInMillis(System.currentTimeMillis());
//						java.util.Date cDate=cal.getTime();
//						
//						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
//						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
//						String cDateStr=sdfnew.format(cDate);
//						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
//							cdr.setChurn20Min(true);
//						}
//						
//						if(cDate.before(expiry24HourChurn) || cDate.before(expiry24HourChurn1)){
//							cdr.setChurn24Hour(true);
//						}
//						
//						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
//						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
//							cdr.setChurnSameDay(true);
//						}
//					}else{
//						//Other Mode like null
//						Calendar cal=Calendar.getInstance();
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.MINUTE,20);
//						java.util.Date expiry20MinChurn1=cal.getTime();
//						
//						cal.setTime(cdr.getChargingDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn=cal.getTime();
//						cal.setTime(cdr.getRequestDate());
//						cal.add(Calendar.HOUR,24);
//						java.util.Date expiry24HourChurn1=cal.getTime();
//						
//						cal.setTimeInMillis(System.currentTimeMillis());
//						java.util.Date cDate=cal.getTime();
//						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
//						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
//						String cDateStr=sdfnew.format(cDate);
//						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
//							cdr.setChurn20Min(true);
//						}
//						
//						if(cDate.before(expiry24HourChurn) || cDate.before(expiry24HourChurn1)){
//							cdr.setChurn24Hour(true);
//						}
//						
//						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
//						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
//							cdr.setChurnSameDay(true);
//						}
//					}
					cdr.setChurn20Min(churn20Min);
					cdr.setChurn24Hour(churn24Hour);
					cdr.setChurnSameDay(sameDayChurn);
					//Create New CDR For DCT
					logger.debug(loggerHead+" De-Activation CDR updated for churn status, now creating DCT CDR.... Flag updateSameCDR "+updateSameCDR);
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatus());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date());
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatus());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(-99);
						cdr.setStatusDescp("Deactivation CDR Created");
					}
				}
			}else if("REN".equalsIgnoreCase(callbackData.getAction())){
				if("GRACE".equalsIgnoreCase(callbackData.getMode())){
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("REN-GRACE");
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatus());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setChargingDate(startDate);
						cdr1.setChargedAmount(0D);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPpid(0);
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setFallbackCharged(false);
						cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_LOWBALANCE);
						cdr1.setStatusDescp("GRACE Callback CDR");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("REN-GRACE");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatus());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						cdr.setChargedAmount(0D);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPpid(0);
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_LOWBALANCE);
						cdr.setStatusDescp("GRACE Callback CDR");
						cdr.setFallbackCharged(false);
					}
				}else if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("REN");
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatus());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setChargingDate(startDate);
						
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr1.setChargedPpid(chargedPricePoint.getPpid());
							cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							cdr1.setActivatedFromZero(activatedFromZero);
						}
						cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr1.setStatusDescp("Customer Renewed");
						cdrBuilder.insert(cdr1);
						
					}else{
						cdr.setRequestType("REN");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatus());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							cdr.setActivatedFromZero(activatedFromZero);
							
						}
						cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr.setStatusDescp("Customer Renewed");
					}
				}
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,VodafoneIndiaSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			
			Calendar cal1=Calendar.getInstance();
			//cal1.setTimeInMillis(sdf.parse(callbackData.getStartDate()).getTime());
			cal1.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal1.getTime();
			cal1.add(Calendar.DATE, chargedPricePoint.getValidity());
			java.util.Date endDate1=cal1.getTime();
			
//			java.util.Date startDate=sdf.parse(callbackData.getStartDate());
//			java.util.Date endDate1=sdf.parse(callbackData.getEndDate());
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode "+callbackData.getMode());
					Calendar cal=Calendar.getInstance();
					cal.setTime(endDate1);
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
				}else if("UNSUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					if("PARKING".equalsIgnoreCase(callbackData.getMode())){
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
						sub.setChargedAmount(0D);
						sub.setParkingDate(startDate);
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setChargedAmount(0D);
						sub.setStatusDescp("Charging Failed at SDP|SDP Status="+callbackData.getStatus()+",Mode="+callbackData.getMode()+",Amount="+callbackData.getPrice());
					}
				}
				
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					if("DEACTIVATE".equalsIgnoreCase(callbackData.getMode())){
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					}else if("SUSPEND".equalsIgnoreCase(callbackData.getMode())){
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
						sub.setSuspendDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					}else{
						//Other Mode like null
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					}
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
				}
			}else if("REN".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus()) && !"GRACE".equalsIgnoreCase(callbackData.getMode())){
					Calendar cal=Calendar.getInstance();
					cal.setTime(startDate);
					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(new java.util.Date());
				}else if("SUCCESS".equalsIgnoreCase(callbackData.getStatus()) && "GRACE".equalsIgnoreCase(callbackData.getMode())){
					Calendar cal=Calendar.getInstance();
					cal.setTime(startDate);
					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
					sub.setStatusDescp("Customer Moved To Grace");
					sub.setGraceDate(new java.util.Date(System.currentTimeMillis()));
				}
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	
	
	
}
