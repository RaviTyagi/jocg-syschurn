package com.jss.jocg.callback.bo;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.bsnl.NetExcelBSNLSDPCallback;
import com.jss.jocg.charging.handlers.JocgChargingHandler;
import com.jss.jocg.charging.handlers.JocgChargingHandlerImpl;	
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.CallbackService;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

import ch.qos.logback.core.joran.action.NewRuleAction;


@Component
public class NetExcelBSNLSDPCallbackReceiver {
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(NetExcelBSNLSDPCallbackReceiver.class);
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");	
	/**
	 Subscriber Status Codes:
	Pending/NewSub-0
	CGSUCCESS=1
	PARKING=2
	GRACE=3
	ACTIVE=5
	CGFAILED=-1
	SUSPENDEDFROMPARKING=-2
	SUSPENDEDFROMGRACE=-3
	UNSUB=-5

	 */		

	
	
	@JmsListener(destination = "callback.bsnlNetExcel", containerFactory = "myFactory")
    public void receiveMessage(NetExcelBSNLSDPCallback callbackData) {
		int esmeID=1;
		String loggerHead="msisdn="+callbackData.getMsisdn()+"|";	
		logger.debug("BsnlNetExcelSDPCallbackReceiver :: Received ||| <" + callbackData + ">");
		try{		
			if(callbackData!=null && callbackData.getMsisdn()>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getServiceId()+", Price "+callbackData.getPrice());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId(), callbackData.getPrice());
				if(cpp==null || cpp.getPpid()<=0) {
					logger.debug(loggerHead+"Price point not found for OpKey "+callbackData.getServiceId()+", Price "+callbackData.getPrice()+", Searching against opkey only.....");
					cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId());
				}
				esmeID=cpp.getChintfid()==8?2:cpp.getChintfid()==9?3:cpp.getChintfid()==10?1:cpp.getChintfid()==11?4:esmeID;
//				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId()):(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" callbackData.getPpid() : "+callbackData.getPpid());
				ChargingPricepoint cppmaster= new ChargingPricepoint();
				if(callbackData.getPpid()!=0){
					 cppmaster=(cpp==null || cpp.getPpid()<=0)?jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId()):(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
					}
					else{
						cppmaster=jocgCache.getPricePointListById(callbackData.getPpid());
					}
				
				
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 ){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=""+callbackData.getMsisdn().longValue();
					if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+callbackData.getMsisdn().longValue(),callbackData.getMsisdn()));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn().longValue()+", Operator Service Key "+serviceKey);
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(callbackData.getMsisdn(),serviceKey);	
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						if(("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(new java.util.Date()))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if(("DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type())) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(callbackData.getReq_type()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
							if(callbackData.getZoneName().equals("STV")){
								logger.debug(loggerHead+"  CDR Not found, Process for Existing STV Subscriber and Non Existing STV CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
							else{
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
							}
						}else{
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null){
								logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
								handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(callbackData.getZoneName()!=null){				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(10);
				String messageToSend=jocgCache.getBsnlMessage();
				messageToSend=URLEncoder.encode(messageToSend);
				String msisdn1=Long.toString(callbackData.getMsisdn());
				String msisdn=(msisdn1.length()<=10)?"91"+msisdn1:msisdn1;
				String smsURL="http://172.31.22.128/DigiSMS/scheduler.jiffy?msisdn="+msisdn+"&sms="+messageToSend+"&esmeID="+esmeID;
				logger.debug(loggerHead+" SMS-URL"+smsURL);
				JcmsSSLClient.JcmsSSLClientResponse resp1=sslClient.invokeURL("SMS URL Invoked ",smsURL);
				logger.debug(loggerHead+" JiffySMS-URL:"+smsURL+"::Resp="+resp1.getResponseData()+":Error="+resp1.getErrorMessage()+"::RespTime="+resp1.getResponseTime());
				}
				else{						
				}
		
		}catch(Exception e1){
			logger.debug(loggerHead+" SMS-URL ERROR "+e1.getMessage());
		}
		}
        
    }
	
	
	public Subscriber handleExistingSubscriber(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";
		try{
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			if("DEACT".equalsIgnoreCase(callbackData.getReq_type())){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdfDateTime=new SimpleDateFormat("yyyyMMddHHmmss");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					java.util.Date dsubDCT=cal.getTime();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
						String dateAct=sdfdate.format(dsubCharged);
						String dateDct=sdfdate.format(dsubDCT);
						sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.after(dsubDCT)|| expiry20Minchurn.equals(dsubDCT))?true:false;
						churn24Hour=(expiry24Hourchurn.after(dsubDCT)|| expiry24Hourchurn.equals(dsubDCT))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(dsubDCT)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}
			
			}						
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			java.util.Date subRegDate=null;
			try{
				if(sub.getSubChargedDate()!=null) subRegDate=sub.getSubChargedDate();
				else if(sub.getValidityStartDate()!=null) subRegDate=sub.getValidityStartDate();
			}catch(Exception e){
				
			}
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,false,sameDayChurn,churn20Min,churn24Hour);
			subscriberDao.save(sub);
			
			
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>1.0){					
					//Setting Notification
					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);					
					cal.setTimeInMillis(System.currentTimeMillis());
					cal.add(Calendar.MINUTE, 60);
					cdr.setNotifyScheduleTime(cal.getTime());
                    cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					
					//Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					cal.add(Calendar.MINUTE, 60);
					cdr.setNotifyScheduleTime(cal.getTime());
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setChargedAmount(callbackData.getPrice());
				
					cdr.setStatusDescp("successfully charged");
					
				}
				chargingCDRDao.save(cdr);
			}else if("DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
					cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
				}
				chargingCDRDao.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		
	
		return sub;
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		if(cppmaster==null){
			cppmaster=cpp;
		}
		loggerHead +="HESNEC::";
		try{
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			if("DEACT".equalsIgnoreCase(callbackData.getReq_type())){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdfDateTime=new SimpleDateFormat("yyyyMMddHHmmss");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					java.util.Date dsubDCT=cal.getTime();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
						String dateAct=sdfdate.format(dsubCharged);
						String dateDct=sdfdate.format(dsubDCT);
						sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.after(dsubDCT)|| expiry20Minchurn.equals(dsubDCT))?true:false;
						churn24Hour=(expiry24Hourchurn.after(dsubDCT)|| expiry24Hourchurn.equals(dsubDCT))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(dsubDCT)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}			
			}			
			java.util.Date subRegDate=null;
			try{
				if(sub.getSubChargedDate()!=null) subRegDate=sub.getSubChargedDate();
				else if(sub.getValidityStartDate()!=null) subRegDate=sub.getValidityStartDate();
			}catch(Exception e){			
			}			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"NA");
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",sub.getTrafficSourceName());
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();			
			
			String requestType=("ACT".equalsIgnoreCase(callbackData.getReq_type()))?"ACT":
				("DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()))?"DCT":
					("REACT".equalsIgnoreCase(callbackData.getReq_type()) || "REN".equalsIgnoreCase(callbackData.getReq_type()))?"REN":callbackData.getReq_type();
			String jocgTransId=(sub.getJocgTransId()!=null && !"NA".equals(sub.getJocgTransId()))?sub.getJocgTransId():"SDP-"+((callbackData.getCg_id()!=null)?callbackData.getCg_id():""+System.currentTimeMillis());
			if(callbackData.getZoneName().equals("STV")){
				jocgTransId=callbackData.getCg_id();
			}
			else{
			jocgTransId=("ACT".equalsIgnoreCase(callbackData.getReq_type()) && !jocgTransId.toUpperCase().startsWith("DIGI"))?"DIGI-"+System.currentTimeMillis():jocgTransId;
			}
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), jocgTransId, requestCategory, requestType, sub.getSubscriberId(), sub.getMsisdn(), (platform!=null?platform.getPlatform():"NA"), (platform!=null?platform.getSystemid():"NA"), 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getConsent_Status(), callbackData.toString(), callbackData.getCg_id(), ""+callbackData.getMsisdn(), new java.util.Date(), 
					null, null, false, false, false);
			if(callbackData.getZoneName().equals("STV")){
				newCDR.setSubRegDate(startDate);
			}
			else{
				newCDR.setSubRegDate(subRegDate);
			}
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,false,sameDayChurn,churn20Min,churn24Hour);					
			newCDR=chargingCDRDao.insert(newCDR);
			
			if("ACT".equalsIgnoreCase(callbackData.getReq_type())){
				sub.setJocgCDRId(newCDR.getId());
				sub.setJocgTransId(newCDR.getJocgTransId());
			}
			sub=subscriberDao.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			if("DEACT".equalsIgnoreCase(callbackData.getReq_type()) && sameDayChurn==true){
				if(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
					try{
						//Handle NotifyDCT
						ChargingCDR cdr=chargingCDRDao.findById(sub.getJocgCDRId());
						if(cdr!=null && cdr.getId()!=null && cdr.getRequestType()!=null && "ACT".equalsIgnoreCase(cdr.getRequestType())){
							cdr.setChurnSameDay(sameDayChurn);
							cdr.setChurn20Min(churn20Min);
							cdr.setChurn24Hour(churn24Hour);
							if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
								cdr.setNotifyChurn(true);
								cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
								cdr.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
								logger.debug(loggerHead+"CHURN NOTIFY LOGIC - DCT Notification QUEUED for churnsameday="+cdr.getChurnSameDay()+", ACT Notify status="+cdr.getNotifyStatus());
							}else{
								logger.debug(loggerHead+"CHURN NOTIFY LOGIC - Condition not satisfied, DCT Notify Flag left unset. Churn sameday="+cdr.getChurnSameDay()+", ACT Notify status="+cdr.getNotifyStatus());
							}
							cdrBuilder.save(cdr);
						}else{
							logger.debug(loggerHead+"CHURN NOTIFY LOGIC - ACT CDR with id "+sub.getJocgCDRId()+" not found!");
						}
						cdr=null;
					}catch(Exception e1){
						logger.debug(loggerHead+"CHURN NOTIFY LOGIC - Exception occured while analysing churn notification scenario, "+e1);
					}
					
				}
			}
			
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		if(cppmaster==null){
			cppmaster=cpp;
		}
		loggerHead +="HNES::";
		String SystemID="NA";
		try{			
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			if(pkg.getPkgname().equals("PK413"))
			{
				SystemID="MJB8Q";
			}
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			String requestType=("ACT".equalsIgnoreCase(callbackData.getReq_type()))?"ACT":
				("DECT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()))?"DCT":
					("REACT".equalsIgnoreCase(callbackData.getReq_type()) || "REN".equalsIgnoreCase(callbackData.getReq_type()))?"REN":callbackData.getReq_type();
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getCg_id(), requestCategory, requestType, ""+callbackData.getMsisdn(), callbackData.getMsisdn(), "NA", SystemID, 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getConsent_Status(), callbackData.toString(), callbackData.getCg_id(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,false,false,false,false);
				newCDR.setSubRegDate(startDate);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getMsisdn(),callbackData.getMsisdn(),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,SystemID,"NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),new java.util.Date());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,boolean activatedFromZero,boolean sameDayChurn,boolean churn20Min,boolean churn24Hour) throws ParseException{
		loggerHead +="UCS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) || "LOWBAL".equalsIgnoreCase(callbackData.getReq_type()) || "FAILED".equalsIgnoreCase(callbackData.getReq_type())){
				if("ACT".equalsIgnoreCase(callbackData.getReq_type()) && "SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>1.0) {
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);					
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if(callbackData.getPrice()<1.0){
					if("LOWBAL".equalsIgnoreCase(callbackData.getReq_type())){
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
					}else{
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
					}
				}else{
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
					cdr.setStatusDescp("Charging Failed at SDP");
					cdr.setChargedAmount(0D);
				}
				cdr.setSdpStatusCode(callbackData.getConsent_Status());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				
			}else if("desubscription".equalsIgnoreCase(callbackData.getReq_type()) || "DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type())){
				
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(cdr.getChargingDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn=cal1.getTime();
					cal1.setTime(cdr.getRequestDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn1=cal1.getTime();
					cal1.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal1.getTime();
					if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
						cdr.setChurn20Min(true);
					}
					if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
						cdr.setChurnSameDay(true);
					}
					
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getConsent_Status());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date());
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getConsent_Status());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
					}
				
			}else if("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					
					if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("REN");
					cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr1.setSdpResponseTime(new java.util.Date());
					cdr1.setSdpStatusCode(callbackData.getConsent_Status());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(new java.util.Date());
					cdr1.setChargingDate(startDate);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr1.setChargedPpid(chargedPricePoint.getPpid());
						cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}
					cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("REN");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getConsent_Status());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
					}
				}
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	
	public Subscriber updateSubscriberStatus(String loggerHead,NetExcelBSNLSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) || "LOWBAL".equalsIgnoreCase(callbackData.getReq_type()) || "FAILED".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode "+callbackData.getMode());
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(endDate1);
					cal1.set(Calendar.HOUR_OF_DAY,23);
					cal1.set(Calendar.MINUTE,59);
					cal1.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setValidityStartDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
				}else if(callbackData.getPrice()<1.0){
					if("LOWBAL".equalsIgnoreCase(callbackData.getReq_type())){
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
						sub.setChargedAmount(0D);
						sub.setParkingDate(startDate);
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setChargedAmount(0D);
						sub.setStatusDescp("Charging Failed at SDP|SDP Status="+callbackData.getConsent_Status()+",Mode="+callbackData.getMode()+",Amount="+callbackData.getPrice());
					}
				}else{
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
					sub.setChargedAmount(0D);
					sub.setParkingDate(startDate);
				}
				
			}else if("DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "desubscription".equalsIgnoreCase(callbackData.getReq_type())){
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
			}else if("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(startDate);
					cal1.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(new java.util.Date());
				}
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
	 Platform pl=null,pldefault=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return (pl!=null?pl:pldefault);
 }
}
