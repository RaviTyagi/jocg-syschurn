package com.jss.jocg.callback.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.callback.data.voda.VodafoneIndiaCGResponse;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;


@Component
public class VodafoneIndiaCGCallbackReceiver {
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	
	 @Autowired ChargingCDRRepository chargingCDRDao;
	 @Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	 
	@JmsListener(destination = "callback.vodacg", containerFactory = "myFactory")
    public void receiveMessage(VodafoneIndiaCGResponse cgCallbackData) {
		
		try{
		if(cgCallbackData!=null && cgCallbackData.getMsisdn()>0){
			Subscriber sub=null;//subscriberDao.findByMsisdnAndStatusCode(cgCallbackData.getMsisdn(),JocgStatusCodes.SUB_SUCCESS_PENDING);
			System.out.println("VodafoneIndiaCGCallbackReceiver :: Subscriber <" + sub + ">");
			
			ChargingCDR cdr=chargingCDRDao.findByjocgTransIdAndStatus(sub.getJocgCDRId(),JocgStatusCodes.CHARGING_INPROCESS);
			System.out.println("VodafoneIndiaCGCallbackReceiver :: CDR <" + cdr + ">");
		
			
			if("SUCCESS".equalsIgnoreCase(cgCallbackData.getCgStatus()) && "CGW202".equalsIgnoreCase(cgCallbackData.getCgStatusCode())){
				//CG Success
				sub.setCgResponseTime(new java.sql.Timestamp(System.currentTimeMillis()));
				sub.setCgStatusCode(cgCallbackData.getCgStatusCode());
				sub.setCgTransactionId(cgCallbackData.getCgTransactionId());
				//sub.setStatusCode(JocgStatusCodes.SUB_SUCCESS_CG);
				
				//cdr.setCgStatus("SUCCESS");
				//cdr.setCgStatusCode(cgCallbackData.getCgStatusCode());
				//cdr.setCgTransId(cgCallbackData.getCgTransactionId());
				cdr.setStatus(JocgStatusCodes.CHARGING_CGSUCCESS);
			}else{
				//CG Failed
				sub.setCgResponseTime(new java.sql.Timestamp(System.currentTimeMillis()));
				sub.setCgStatusCode(cgCallbackData.getCgStatusCode());
				sub.setCgTransactionId(cgCallbackData.getCgTransactionId());
				//sub.setStatusCode(JocgStatusCodes.SUB_FAILED_CG);
				
				//cdr.setCgStatus("FAILED");
				cdr.setCgStatusCode(cgCallbackData.getCgStatusCode());
				//cdr.setCgTransId(cgCallbackData.getCgTransactionId());
				cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			}
			
			subMgr.save(sub);
			cdrBuilder.save(cdr);
		}
		}catch(Exception e){
			e.printStackTrace();
		}
        System.out.println("VodafoneIndiaCGCallbackReceiver :: Received <" + cgCallbackData + ">");
    }
}
