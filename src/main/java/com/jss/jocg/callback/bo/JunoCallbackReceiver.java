package com.jss.jocg.callback.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.JunoSDPCallback;

import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class JunoCallbackReceiver {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(JunoCallbackReceiver.class);
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");
	public JunoCallbackReceiver(){}
	
	public String toHex(String str){
		StringBuilder sb=new StringBuilder();
		try{
		
		}catch(Exception e){}
		return sb.toString();
		
	}
	
	
	@JmsListener(destination = "callback.junosdp", containerFactory = "myFactory")
    public void receiveMessage(JunoSDPCallback callbackData) {
		String loggerHead="junotransid="+callbackData.getJunoTransId()+"|";
	System.out.println("Juno Receiver Invoked");
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			if(callbackData!=null && callbackData.getMsisdn()!=null && callbackData.getMsisdn().length()>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getJunoProductId()+", Price "+callbackData.getPrice());
				ChargingInterface chintf=jocgCache.getChargingInterfaceByOpcode(callbackData.getOperatorCode());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getJunoProductId(), chintf.getChintfid(), callbackData.getPrice());
				logger.debug(loggerHead+"CPP based on price and key "+cpp);
				
				if(cpp==null || cpp.getPricePoint()!=callbackData.getPrice()){
					cpp=jocgCache.getPricePointListByOperatorKeyAndOperator(callbackData.getJunoProductId(), chintf.getChintfid());
					
					logger.debug(loggerHead+"CPP based on Operator Key only "+cpp);
				}
								
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=""+callbackData.getMsisdn();
					
					callbackData.setAction(("ACT".equalsIgnoreCase(callbackData.getAction()) || "ACT".equalsIgnoreCase(callbackData.getActionRubyOnRails()) )?"ACT":
						("act_park".equalsIgnoreCase(callbackData.getAction())  || "act_park".equalsIgnoreCase(callbackData.getActionRubyOnRails()) )?"PARKING":
							("act_grace".equalsIgnoreCase(callbackData.getAction())  || "act_grace".equalsIgnoreCase(callbackData.getActionRubyOnRails()) )?"GRACE":	
								("update".equalsIgnoreCase(callbackData.getAction()) || "update".equalsIgnoreCase(callbackData.getActionRubyOnRails()) )?"REN":
									("dct".equalsIgnoreCase(callbackData.getAction())  || "dct".equalsIgnoreCase(callbackData.getActionRubyOnRails()) )?"DCT":	callbackData.getAction());
					
					if("ACT".equalsIgnoreCase(callbackData.getAction()) || "PARKING".equalsIgnoreCase(callbackData.getAction())
							|| "GRACE".equalsIgnoreCase(callbackData.getAction()) || "REN".equalsIgnoreCase(callbackData.getAction()) 
							|| "DCT".equalsIgnoreCase(callbackData.getAction())){
					
							logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn()+", Operator Service Key "+serviceKey);
							//To be checked-Rishi
							Subscriber sub=subscriberDao.findByJocgTransId(callbackData.getJocgTransId());
							if(sub==null) sub=("dialog_lkr".equalsIgnoreCase(callbackData.getOperatorCode()))?
									subscriberDao.findBySubscriberIdAndOperatorServiceKey(callbackData.getSubscriberId(),serviceKey)
									: subscriberDao.findByMsisdnAndOperatorServiceKey(JocgString.strToLong(callbackData.getMsisdn(),0L),serviceKey);	
							if(sub==null) sub=("dialog_lkr".equalsIgnoreCase(callbackData.getOperatorCode()))?subscriberDao.findByMsisdnAndOperatorServiceKey(JocgString.strToLong(callbackData.getMsisdn(),0L),serviceKey):sub;
							if(sub==null || sub.getId()==null){
								chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
								PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
								Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
								PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
								Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
								if(service!=null && service.getServiceid()>0){
									sub=subscriberDao.searchSubscriberByChintfAndServiceId(chintf.getChintfid(),callbackData.getSubscriberId(),JocgString.strToLong(callbackData.getMsisdn(),0L),service.getServiceid(),service.getServiceName());
								}
								
							}
							
							ChargingCDR cdr=null;
							if(sub!=null && sub.getId()!=null){
								
								logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
								java.util.Date lastRenewaldate=sub.getLastRenewalDate();
								java.util.Date callbackDate=callbackData.getReceiveDate();
								/*if("REN".equalsIgnoreCase(callbackData.getAction()) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(callbackDate)) && (callbackData.getPrice()==sub.getChargedAmount() || callbackData.getPrice()<=0)){
									logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
								}else if("DCT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
									logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
								}else if("ACT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
									logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
								}*/
								ChargingCDR cdrTrans=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(cppmaster.getChintfid(), callbackData.getJunoTransId());
									if(("REN".equalsIgnoreCase(callbackData.getAction()) || "ACT".equalsIgnoreCase(callbackData.getAction())) && cdrTrans!=null && cdrTrans.getId()!=null){
										
									}else if("DCT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
										logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
									}else{
										
										cdr=chargingCDRDao.findById(sub.getJocgCDRId());
										
										if(cdr!=null && cdr.getId()!=null){
											if(cdr.getJocgTransId().equalsIgnoreCase(callbackData.getJocgTransId())){
												handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
											}else if(sub!=null && sub.getId()!=null){
												logger.debug(loggerHead+"  CDR Found but for DCT, Process for Existing Subscriber and Non Existing CDR...");
												handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
											}else{
												logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
												handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
											}
											
										}else{
											logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
											handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
										}
									}
							}else{
								logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
								handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
							}
					}else{
						logger.error(loggerHead+" Unknown Action '"+callbackData.getAction()+"' in callback, Callback Ignored!|"+callbackData);
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!|"+callbackData);
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!|"+callbackData);
			
		}catch(Exception e){
			logger.debug(loggerHead+" :: Exception "+e);
			e.printStackTrace();
		}
        
    }
	
	public void handleExistingSubscriber(String loggerHead,JunoSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";
		try{
			logger.debug(loggerHead+" Processing for existing custmer CDR ......."+callbackData.getAction()+", status "+callbackData.getStatusCode());
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			cdr.setSdpTransactionId(callbackData.getJunoTransId());
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false);
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(callbackData.getAction()) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
//					logger.debug(loggerHead+" Sending Activation SMS .......");
//					JocgSMSMessage smsMessage=new JocgSMSMessage();
//					smsMessage.setMsisdn(JocgString.strToLong(callbackData.getMsisdn(),0L));
//					smsMessage.setSmsInterface("junosms");
//					smsMessage.setServiceName(sub.getServiceName());
//					smsMessage.setChintfId(sub.getChargingInterfaceId());
//					smsMessage.setChargingInterfaceType(sub.getChargingInterfaceType());
//					smsMessage.setTextMessage("Successful Activation");
//					jmsTemplate.convertAndSend("notify.sms", smsMessage);
//					logger.debug(loggerHead+" Activation SMS Queued .......SMSData="+smsMessage);
//					//Setting Notification
//					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					Calendar cal=Calendar.getInstance();
//					cal.setTimeInMillis(System.currentTimeMillis());
//					cal.add(Calendar.MINUTE, 60);
//					cdr.setNotifyScheduleTime(cal.getTime());
//					cdr.setNotifyChurn(true);
//					logger.debug(loggerHead+" S2S Notification Queued, Churn Status Enabled!");
					
				}
				cdrBuilder.save(cdr);
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,JunoSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			
			java.util.Date startDate=callbackData.getReceiveDate();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(startDate.getTime());
			cal.add(Calendar.DATE, cpp.getValidity());
			java.util.Date endDate1=new java.util.Date(cal.getTimeInMillis());
			Platform platform=getPlatformBySystemId(loggerHead,sub.getSystemId(),"BLANK-1");
			TrafficSource ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
			TrafficCampaign tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, callbackData.getAction(), sub.getSubscriberId(), sub.getMsisdn(), platform.getPlatform(), platform.getSystemid(), 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatusCode(), callbackData.toString(), callbackData.getJunoTransId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
					null, null, false, false, false);
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(callbackData.getAction())) sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,JunoSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			long msisdn=JocgString.strToLong(callbackData.getMsisdn(), 0L);
			java.util.Date startDate=callbackData.getReceiveDate();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(startDate.getTime());
			cal.add(Calendar.DATE, cpp.getValidity());
			java.util.Date endDate1=new java.util.Date(cal.getTimeInMillis());
			Platform platform=getPlatformBySystemId(loggerHead,"BLANK","BLANK");
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getJocgTransId(), requestCategory, callbackData.getAction(), callbackData.getSubscriberId(), msisdn, platform.getPlatform(), platform.getSystemid(), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatusCode(), callbackData.toString(), callbackData.getJunoTransId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),callbackData.getSubscriberId(),msisdn,
						"OP-SDP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,platform.getSystemid(),platform.getPlatform(),"OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),new java.util.Date());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,JunoSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR){
		loggerHead +="UCS::";
		try{
			java.util.Date startDate=callbackData.getReceiveDate();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(startDate.getTime());
			cal.add(Calendar.DATE, chargedPricePoint.getValidity());
			java.util.Date endDate1=new java.util.Date(cal.getTimeInMillis());
			
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setSubRegDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if("low_bal".equalsIgnoreCase(callbackData.getStatusCode())){
					    cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
				}else{//failure
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
				}
				
				cdr.setSdpStatusCode(callbackData.getStatusCode());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpTransactionId(callbackData.getJunoTransId());
				cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				
			}else if("PARKING".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					logger.debug("Updating CDR for "+chargedPricePoint);
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if("low_bal".equalsIgnoreCase(callbackData.getStatusCode())){
					    cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
				}else{//failure
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
				}
				
				cdr.setSdpStatusCode(callbackData.getStatusCode());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				
			}else if(("DCT".equalsIgnoreCase(callbackData.getAction()) && "SUCCESS".equalsIgnoreCase(callbackData.getStatusCode()))){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(cdr.getChargingDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn=cal1.getTime();
						cal1.setTime(cdr.getRequestDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn1=cal1.getTime();
						cal1.setTimeInMillis(System.currentTimeMillis());
						java.util.Date cDate=cal1.getTime();
						
						SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
						String chargingDateStr=sdfnew.format(cdr.getChargingDate());
						String cDateStr=sdfnew.format(cDate);
						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
							cdr.setChurn20Min(true);
						}
						logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
						if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
							cdr.setChurnSameDay(true);
						}
					
					//Create New CDR For DCT
					logger.debug(loggerHead+" De-Activation CDR updated for churn status, now creating DCT CDR.... Flag updateSameCDR "+updateSameCDR);
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatusCode());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(callbackData.getReceiveDate());
						cdr.setSdpResponseTime(callbackData.getReceiveDate());
						cdr.setSdpStatusCode(callbackData.getStatusCode());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(-99);
						cdr.setStatusDescp("Deactivation CDR Created");
					}
				}else{
					logger.debug(loggerHead+" Deactivation ignored for CDR as failed status is received in callback.");
				}
			}else if("REN".equalsIgnoreCase(callbackData.getAction()) && "SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
				if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("REN");
					cdr1.setSdpResponseTime(new java.util.Date());
					cdr1.setSdpStatusCode(callbackData.getStatusCode());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(new java.util.Date());
					cdr1.setChargingDate(startDate);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr1.setChargedPpid(chargedPricePoint.getPpid());
						cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}
					cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr1.setStatusDescp("Customer Renewed");
					cdrBuilder.insert(cdr1);
					
				}else{
					cdr.setRequestType("REN");
					cdr.setSdpResponseTime(new java.util.Date());
					cdr.setSdpStatusCode(callbackData.getStatusCode());
					cdr.setSdpStatusDescp(callbackData.toString());
					cdr.setRequestDate(new java.util.Date());
					cdr.setChargingDate(startDate);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr.setChargedPpid(chargedPricePoint.getPpid());
						cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Renewed");
				}
			}else if("GRACE".equalsIgnoreCase(callbackData.getAction()) && ("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode()) || "low_bal".equalsIgnoreCase(callbackData.getStatusCode()))){
				
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("REN-GRACE");
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatusCode());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setChargingDate(startDate);
						cdr1.setChargedAmount(0D);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPpid(0);
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setFallbackCharged(false);
						cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_LOWBALANCE);
						cdr1.setStatusDescp("GRACE-"+callbackData.getChargingMode());
						
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("REN-GRACE");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatusCode());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						cdr.setChargedAmount(0D);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPpid(0);
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_LOWBALANCE);
						cdr.setStatusDescp("GRACE-"+callbackData.getChargingMode());
						cdr.setFallbackCharged(false);
					}
				
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,JunoSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			java.util.Date startDate=callbackData.getReceiveDate();
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(startDate.getTime());
			cal.add(Calendar.DATE, chargedPricePoint.getValidity());
			java.util.Date endDate1=new java.util.Date(cal.getTimeInMillis());
			sub.setMsisdn(JocgString.strToLong(callbackData.getMsisdn(),sub.getMsisdn()));
			sub.setSubscriberId(callbackData.getSubscriberId());
			
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
					if("SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
						logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode "+callbackData.getChargingMode());
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(endDate1);
						cal1.set(Calendar.HOUR_OF_DAY,23);
						cal1.set(Calendar.MINUTE,59);
						cal1.set(Calendar.SECOND,59);
	//					cal.setTime(startDate);
	//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
						java.util.Date endDate=cal1.getTime();
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
						sub.setStatusDescp("Customer Activated");
						double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
						logger.debug("SubSession Revenue "+subSRevenue);
						sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
						sub.setChargedAmount(chargedPricePoint.getPricePoint());
						sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
						sub.setSubChargedDate(startDate);
						sub.setValidityEndDate(endDate);
						sub.setNextRenewalDate(endDate);
					}else if("low_bal".equalsIgnoreCase(callbackData.getStatusCode())){
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setStatusDescp("LOW Balance");
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setStatusDescp("Subscription Failed");
					}
			}else if("PARKING".equalsIgnoreCase(callbackData.getAction()) && "SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
					sub.setChargedAmount(0D);
					sub.setParkingDate(startDate);
					sub.setChargingMode(callbackData.getChargingMode());
			}else if("DCT".equalsIgnoreCase(callbackData.getAction()) && "SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
					sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
					//sub.setChargedAmount(0D);
					sub.setUserField_0("UNSUBMODE-"+callbackData.getChargingMode());
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
				
			}else if("REN".equalsIgnoreCase(callbackData.getAction()) && "SUCCESS".equalsIgnoreCase(callbackData.getStatusCode())){
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(startDate);
					cal1.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(callbackData.getReceiveDate());
				
			}else if("GRACE".equalsIgnoreCase(callbackData.getAction())){
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(startDate);
					cal1.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
					sub.setStatusDescp("Customer Moved To Grace");
					sub.setGraceDate(callbackData.getReceiveDate());
				
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
		 Platform pl=null,pldefault=null;
		 try{
			Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
			Iterator<Platform> i=platfromList.values().iterator();
			while(i.hasNext()){
				pl=i.next();
				if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
				if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
				else pl=null;
			}
		 }catch(Exception e){
			 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
		 }
		 return (pl!=null?pl:pldefault);
	 }
	
	
}
