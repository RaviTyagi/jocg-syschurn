package com.jss.jocg.callback.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.CommonSDPCallback;

import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class CommonSDPCallbackReceiver {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(CommonSDPCallbackReceiver.class);
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");

	
	//@JmsListener(destination = "callback.paypal", containerFactory = "myFactory")
    public void receivPaypaleMessage(CommonSDPCallback callbackData) {
		String loggerHead="PAYPAL|msisdn="+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+"|";	
		processMessage(loggerHead,callbackData);
	}
	
	//@JmsListener(destination = "callback.google", containerFactory = "myFactory")
    public void receivGoogleMessage(CommonSDPCallback callbackData) {
		String loggerHead="GOOGLE|msisdn="+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+"|";
		processMessage(loggerHead,callbackData);
	}
	
	//@JmsListener(destination = "callback.ios", containerFactory = "myFactory")
    public void receivIosMessage(CommonSDPCallback callbackData) {
		String loggerHead="IOS|msisdn="+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+"|";	
		processMessage(loggerHead,callbackData);
	}
	
	//@JmsListener(destination = "callback.connectmigrated", containerFactory = "myFactory")
	public void receiveMessage(CommonSDPCallback callbackData) {
		String loggerHead="CONNECT|msisdn="+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+"|";	
		processMessage(loggerHead,callbackData);
		
	}
	
	public void processMessage(String loggerHead,CommonSDPCallback callbackData) {
			
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		System.out.println(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			String msisdnStr=""+callbackData.getMsisdn().longValue();
			if(msisdnStr.length()<=10 && msisdnStr.length()>1)
				msisdnStr="91"+msisdnStr;
			callbackData.setMsisdn(JocgString.strToLong(msisdnStr,callbackData.getMsisdn().longValue()));
			System.out.println(loggerHead+" :: Processing for msisdn <" + callbackData.getMsisdn() + ">");
		
			if(callbackData!=null && (callbackData.getMsisdn()>0 || (callbackData.getSubscriberId()!=null && callbackData.getSubscriberId().length()>0))){	
				PackageDetail packageDetail=null;ChargingInterface chintf=null;ChargingPricepoint cpp=null,cppmaster=null;
				if(!"NA".equalsIgnoreCase(callbackData.getPackageName())){
					packageDetail=jocgCache.getPackageDetailsByName(callbackData.getPackageName());
				}
				System.out.println("Package "+packageDetail);
				if(!"NA".equalsIgnoreCase(callbackData.getOperatorCode())){
					chintf=jocgCache.getChargingInterfaceByOpcode(callbackData.getOperatorCode());
				}
				System.out.println("Chintf "+chintf);
				if(packageDetail!=null && packageDetail.getPkgid()>0 &&
						chintf!=null && chintf.getChintfid()>0){
					cpp=jocgCache.getPricePointListByChargingInterface(chintf.getChintfid(), callbackData.getPrice());
					if(cpp==null || cpp.getPpid()<=0){
						List<ChargingPricepoint> chppList=jocgCache.getPricePointListByChargingInterface(chintf.getChintfid());
						for(ChargingPricepoint chpp1:chppList){
							if(chpp1.getValidity()==callbackData.getValidity() && chpp1.getPricePoint()==callbackData.getPrice()){
								cpp=chpp1;break;
							}
						}
					}
					
					cppmaster=(cpp!=null && cpp.getPpid()>0 && cpp.getIsfallback()>0)?jocgCache.getPricePointListById(cpp.getMppid()):
						(cpp!=null && cpp.getPpid()>0)?cpp:null;
				}
				System.out.println("cpp "+cpp);
				System.out.println("cppmaster "+cppmaster);
				
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					//String msisdnStr=""+callbackData.getMsisdn().longValue();
					//if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+callbackData.getMsisdn().longValue(),callbackData.getMsisdn()));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn().longValue()+", Operator Service Key "+serviceKey);
					
					Subscriber sub=null;
					if(msisdnStr.length()>10)
						sub=subscriberDao.findByMsisdnAndOperatorServiceKey(callbackData.getMsisdn(),serviceKey);	
					String subscriberId=JocgString.formatString(callbackData.getSubscriberId(), "NA");
					if((sub==null || sub.getId()==null) && (subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId) && subscriberId.length()>0)){
						sub=subscriberDao.findBySubscriberIdAndOperatorServiceKey(callbackData.getSubscriberId(),serviceKey);	
					}
					
					System.out.println("sub "+sub);
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						java.util.Date requestDate=callbackData.getReceiveTime();
						//duplicatecase
						if("REN".equalsIgnoreCase(callbackData.getRequestType())  && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(new java.util.Date()))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if("DCT".equalsIgnoreCase(callbackData.getRequestType())  && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(callbackData.getRequestType()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
						}else{
							if("ACT".equalsIgnoreCase(callbackData.getRequestType())){
								logger.debug(loggerHead+" New ACTG handling as existing subscriber non existing CDR ");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}else{
								cdr=chargingCDRDao.findById(sub.getJocgCDRId());
								logger.debug(loggerHead+" cdr "+cdr.getId());
								if(cdr!=null && cdr.getId()!=null){
									logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
									handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
								}else{
									logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
									handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
								}
							}
							
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepoint, Callback Ignored!");
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
	
		}catch(Exception e){
			e.printStackTrace();
		}
	    
    }
	
	
	public Subscriber handleExistingSubscriber(String loggerHead,CommonSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		
		loggerHead +="HES::";
		try{
			System.out.println(loggerHead+" Processing data");
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false);
			subscriberDao.save(sub);
			if("act".equalsIgnoreCase(callbackData.getRequestType())){
				if(callbackData.getPrice()>0){
					JocgSMSMessage smsMessage=new JocgSMSMessage();
					smsMessage.setMsisdn(callbackData.getMsisdn());
					smsMessage.setSmsInterface("smsconnect");
					smsMessage.setChintfId(1);
					smsMessage.setTextMessage("Successful Activation");
					jmsTemplate.convertAndSend("notify.sms", smsMessage);
//					//Setting Notification
//					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					Calendar cal=Calendar.getInstance();
//					cal.setTimeInMillis(System.currentTimeMillis());
//					cal.add(Calendar.MINUTE, 60);
//					cdr.setNotifyScheduleTime(cal.getTime());
					
				}
				chargingCDRDao.save(cdr);
			}else if("dct".equalsIgnoreCase(callbackData.getRequestType())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
//					cdr.setNotifyChurn(true);
//					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				chargingCDRDao.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		
	
		return sub;
		
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,CommonSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			System.out.println(loggerHead+" Processing data");
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			ChargingInterface sourceNetwork=("DEN".equalsIgnoreCase(callbackData.getSourceOperatorId()))?jocgCache.getChargingInterfaceByName("DEN"):jocgCache.getChargingInterfaceByName("CMOF");
			if(sourceNetwork==null && !"DEN".equalsIgnoreCase(callbackData.getSourceOperatorId())){
				sourceNetwork=new ChargingInterface();
				sourceNetwork.setChintfid(39);
				sourceNetwork.setChintfType("OPERATOR");
				sourceNetwork.setName("CommonOffdeck");
				sourceNetwork.setCountry("India");
				sourceNetwork.setOpcode("CMOF");
			}
			
			Circleinfo circle=new Circleinfo();
			PackageDetail pkg=jocgCache.getPackageDetailsByName(callbackData.getPackageName());
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(sourceNetwork.getChintfid(), chintf.getChintfid(), pkg.getPkgid(), cppmaster.getPpid());
			if(pkg.getPkgid()!=sub.getPackageId()){
				sub.setPackageId(pkg.getPkgid());
				sub.setPackageName(pkg.getPkgname());
				sub.setPackagePrice(pkg.getPricepoint());
				if(service!=null){
					sub.setServiceId(service.getServiceid());
					sub.setServiceName(service.getServiceName());
				
				}
				if(sub.getPackageChargingInterfaceMapId()!=pkgChintf.getPkgchintfid()){
					sub.setPackageChargingInterfaceMapId(pkgChintf.getPkgchintfid());
				}
			}
			
			
			//SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
		
			java.util.Date startDate=callbackData.getReceiveTime();
			//java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			String requestType=("act".equalsIgnoreCase(callbackData.getRequestType()) )?"ACT":
				("dct".equalsIgnoreCase(callbackData.getRequestType()))?"DCT":
					("ren".equalsIgnoreCase(callbackData.getRequestType()))?"REN":callbackData.getRequestType();
			
			Platform platform=(chintf.getOpcode().equalsIgnoreCase("CM_OF") || chintf.getName().contains("Connect"))?getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"1VHS94"):
				getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"1JBNCV");
			
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",sub.getTrafficSourceName());
			if(ts==null) ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
			String jocgTransId=sub.getJocgTransId();
			if("ACT".equalsIgnoreCase(requestType)) jocgTransId="DIGI"+System.currentTimeMillis();
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), jocgTransId, requestCategory, requestType, sub.getSubscriberId(), sub.getMsisdn(), platform.getPlatform(), platform.getSystemid(), 
					new JocgRequest(), sourceNetwork, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated",startDate, "NA", "NA", "NA", startDate, "SUCCESS", callbackData.toString(), callbackData.getSubscriberId(), ""+callbackData.getMsisdn(),startDate, 
					null, null, false, false, false);
			newCDR.setSdpTransactionId(callbackData.getTransactionId());
			newCDR.setSubscriberId(callbackData.getSubscriberId());
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
			newCDR=chargingCDRDao.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(callbackData.getRequestType())) sub.setJocgCDRId(newCDR.getId());
			sub.setCustomerId(callbackData.getSubscriberId());
			
			sub=subscriberDao.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,CommonSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			System.out.println(loggerHead+" Processing data");
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			ChargingInterface sourceNetwork=("DEN".equalsIgnoreCase(callbackData.getSourceOperatorId()))?jocgCache.getChargingInterfaceByName("DEN"):jocgCache.getChargingInterfaceByName("CMOF");
			if(sourceNetwork==null && !"DEN".equalsIgnoreCase(callbackData.getSourceOperatorId())){
				sourceNetwork=new ChargingInterface();
				sourceNetwork.setChintfid(39);
				sourceNetwork.setChintfType("OPERATOR");
				sourceNetwork.setName("CommonOffdeck");
				sourceNetwork.setCountry("India");
				sourceNetwork.setOpcode("CMOF");
			}
			System.out.println(loggerHead+" sourceNetwork "+sourceNetwork);
			PackageDetail pkg=jocgCache.getPackageDetailsByName(callbackData.getPackageName());
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			System.out.println(loggerHead+" searching charging config for  "+sourceNetwork.getChintfid()+","+chintf.getChintfid()+","+cppmaster.getPpid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(sourceNetwork.getChintfid(), chintf.getChintfid(), pkg.getPkgid(), cppmaster.getPpid());
			
			System.out.println(loggerHead+" pkgChintf "+pkgChintf);
			Circleinfo circle=new Circleinfo();
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			
			java.util.Date startDate=callbackData.getReceiveTime(); //cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			String requestType=("act".equalsIgnoreCase(callbackData.getRequestType()) )?"ACT":
				("dct".equalsIgnoreCase(callbackData.getRequestType()))?"DCT":
					("ren".equalsIgnoreCase(callbackData.getRequestType()))?"REN":callbackData.getRequestType();
			
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				Platform platform=(chintf.getOpcode().equalsIgnoreCase("CM_OF") || chintf.getName().contains("Connect"))?getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::","1VHS94","1VHS94"):
					getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::","1JBNCV","1JBNCV");
				
				TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
		
				
				
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getTransactionId(), requestCategory, requestType, ""+callbackData.getSubscriberId(), callbackData.getMsisdn(), platform.getPlatform(), platform.getSystemid(),  
						new JocgRequest(), sourceNetwork, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", startDate, "NA", "NA", "NA", startDate, "SUCCESS", callbackData.toString(), callbackData.getSubscriberId(), ""+callbackData.getMsisdn(), startDate, 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
				newCDR=chargingCDRDao.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getSubscriberId(),callbackData.getMsisdn(),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),sourceNetwork.getChintfid(),sourceNetwork.getOpcode(),sourceNetwork.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",startDate,
						startDate,startDate,endDate1,chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"NA","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),startDate);
				sub.setSubscriberId(callbackData.getSubscriberId());
				sub.setCustomerId(callbackData.getSubscriberId());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subscriberDao.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	
	
	@SuppressWarnings("deprecation")
	public ChargingCDR updateCDRStatus(String loggerHead,CommonSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,Boolean updateSameCDR){
		loggerHead +="UCS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			
			//String chargingDate = callbackData.getChargingdate();
			java.util.Date startDate=callbackData.getReceiveTime();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			
			if("act".equalsIgnoreCase(callbackData.getRequestType())){
				if( callbackData.getPrice()>0) {
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(callbackData.getPrice());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if("FAIL".equalsIgnoreCase(callbackData.getRequestType())){

						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);

				}else{
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
					cdr.setStatusDescp("Charging Failed at SDP");
					cdr.setChargedAmount(0D);
				}
				cdr.setSdpStatusCode("SUCCESS");
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(startDate);
				
			}else if("dct".equalsIgnoreCase(callbackData.getRequestType())){
						Calendar cal1=Calendar.getInstance();
						cal1.setTime(cdr.getChargingDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn=cal1.getTime();
						cal1.setTime(cdr.getRequestDate());
						cal1.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn1=cal1.getTime();
						
						cal1.setTime(cdr.getChargingDate());
						cal1.add(Calendar.HOUR,24);
						java.util.Date expiry24HourChurn=cal1.getTime();
						cal1.setTime(cdr.getRequestDate());
						cal1.add(Calendar.HOUR,24);
						java.util.Date expiry24HourChurn1=cal1.getTime();
						
						cal1.setTimeInMillis(System.currentTimeMillis());
						java.util.Date cDate=cal1.getTime();
						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
							cdr.setChurn20Min(true);
						}
						
						if(cDate.before(expiry24HourChurn) || cDate.before(expiry24HourChurn1)){
							cdr.setChurn24Hour(true);
						}
						
						if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
							cdr.setChurnSameDay(true);
						}

				
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode("SUCCESS-DCT");
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						
						cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr1.setStatusDescp("Customer Deactivated");
						chargingCDRDao.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date());
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode("SUCCESS-DCT");
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						chargingCDRDao.save(cdr);
					}
				
			}else if("ren".equalsIgnoreCase(callbackData.getRequestType()) && callbackData.getPrice()>0){
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("REN");
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr1.setSdpStatusCode("SUCCESS");
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setChargingDate(startDate);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr1.setChargedPpid(chargedPricePoint.getPpid());
							cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
						chargingCDRDao.insert(cdr1);
					}else{
						cdr.setRequestType("REN");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode("SUCCESS");
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
					}
				
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
	}
	
	public Subscriber updateSubscriberStatus(String loggerHead,CommonSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");				
		
			java.util.Date startDate=callbackData.getReceiveTime();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("act".equalsIgnoreCase(callbackData.getRequestType()) ){
				if(callbackData.getPrice()>0) {
					
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode :NA"); //+callbackData.getConsentSource());
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(endDate1);
					cal1.set(Calendar.HOUR_OF_DAY,23);
					cal1.set(Calendar.MINUTE,59);
					cal1.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					
					sub.setSubSessionRevenue(chargedPricePoint.getPricePoint());
					sub.setOperatorPricePoint(masterPricePoint.getPricePoint());
					sub.setOperatorParams(masterPricePoint.getOpParams());
					sub.setOperatorServiceKey(masterPricePoint.getOperatorKey());
				
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setCustomerId(callbackData.getSubscriberId());
				}else{

						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setChargedAmount(0D);
						sub.setStatusDescp("Charging Failed at SDP|SDP Validity="+callbackData.getValidity()+",Mode=NA,Amount="+callbackData.getPrice());
				}
				
			}else if("dct".equalsIgnoreCase(callbackData.getRequestType())){
				
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setStatusDescp("Customer Deactivated");
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("NA");//+callbackData.getConsentSource());
					

				logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
			}else if("REN".equalsIgnoreCase(callbackData.getRequestType()) && callbackData.getPrice()>0){
				
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(startDate);
					cal1.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+callbackData.getPrice());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(new java.util.Date());
			
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(System.currentTimeMillis()),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
	 Platform pl=null,pldefault=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return (pl!=null?pl:pldefault);
 }
	
}

