package com.jss.jocg.callback.bo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.AircelIndiaCGCallback;
import com.jss.jocg.callback.data.AircelIndiaSDPCallback;
import com.jss.jocg.callback.data.IdeaSdpCallback;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class AircelIndiaCallbackReceiver {

	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(AircelIndiaCallbackReceiver.class);
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");
	
	
	@JmsListener(destination = "callback.aircelincg", containerFactory = "myFactory")
	public void processCGCallback(AircelIndiaCGCallback callbackData) {
		
	}
	
	@JmsListener(destination = "callback.aircelsdp", containerFactory = "myFactory")
	public void processCGCallback(AircelIndiaSDPCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+"|";
		
		if(callbackData.getCgTransId()==null){
			callbackData.setCgTransId("DIGIV"+System.currentTimeMillis()+callbackData.getMsisdn());
		}
		
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			String msisdnStr=""+callbackData.getMsisdn();
			if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+callbackData.getMsisdn(),callbackData.getMsisdn()));
			if(callbackData!=null && callbackData.getMsisdn()>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getProductName()+", Price "+callbackData.getAmountCharged());
			
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getProductName()+", Price "+callbackData.getAmountCharged());
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getProductName(), callbackData.getAmountCharged());
				
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?jocgCache.getPricePointListByOperatorKey(callbackData.getProductName()):(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn()+", Operator Service Key "+serviceKey);
					Subscriber sub=subscriberDao.findByJocgTransId(callbackData.getCgTransId());
					if(sub==null || sub.getId()==null){
						sub=subscriberDao.findByMsisdnAndOperatorServiceKey(callbackData.getMsisdn(), serviceKey);
					}
					
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						java.util.Date subChargedDate=sub.getSubChargedDate();
						if(subChargedDate==null) subChargedDate=sub.getValidityStartDate();
						if(( callbackData.getNotifyType()!=null && callbackData.getNotifyType().contains("RENEW-2")) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_GRACE){
							logger.debug(loggerHead+" Duplicate GRACE Callback, Hence Ignored");
						}else if(( callbackData.getNotifyType()!=null && callbackData.getNotifyType().contains("RENEW-3")) && sub.getStatus()==JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE){
							logger.debug(loggerHead+" Duplicate SUSPENSION Callback, Hence Ignored");
						}else if(( callbackData.getNotifyType()!=null && callbackData.getNotifyType().contains("RENEW-1")) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(new java.util.Date(System.currentTimeMillis())))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored. Lastrenewaldate "+sdfDate.format(lastRenewaldate)+"|Renew date "+sdfDate.format(new java.util.Date(System.currentTimeMillis())) );
						}else if((callbackData.getNotifyType()!=null && callbackData.getNotifyType().contains("UNSUB")) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if(callbackData.getNotifyType().contains("SUB-2") && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING){
							logger.debug(loggerHead+" Duplicate Parking Callback, Hence Ignored "+sub.getStatus());
						}else if(callbackData.getNotifyType().contains("SUB-1") && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && subChargedDate==null || (subChargedDate!=null && sdfDate.format(subChargedDate).equalsIgnoreCase(sdfDate.format(new java.util.Date(System.currentTimeMillis()))))){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored "+sub.getStatus());
						}else{
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null){
								logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
								handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
						
					}
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
		}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void handleExistingSubscriber(String loggerHead,AircelIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";		
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			String actionName=callbackData.getNotifyType();
			actionName=(actionName!=null && actionName.length()>0)?actionName.toUpperCase().trim():"NA";
			if(actionName.contains("UNSUB")){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdfDateTime=new SimpleDateFormat("yyyyMMddHHmmss");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					java.util.Date dsubDCT=cal.getTime();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
						String dateAct=sdfdate.format(dsubCharged);
						String dateDct=sdfdate.format(dsubDCT);
						sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.after(dsubDCT)|| expiry20Minchurn.equals(dsubDCT))?true:false;
						churn24Hour=(expiry24Hourchurn.after(dsubDCT)|| expiry24Hourchurn.equals(dsubDCT))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(dsubDCT)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}
			
			}
			
			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			
			java.util.Date subRegDate=null;
			try{
				if(sub.getSubChargedDate()!=null) subRegDate=sub.getSubChargedDate();
				else if(sub.getValidityStartDate()!=null) subRegDate=sub.getValidityStartDate();
			}catch(Exception e){
				
			}
			
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,false,sameDayChurn,churn20Min,churn24Hour);
			subscriberDao.save(sub);

			if("SUB-1".equalsIgnoreCase(callbackData.getNotifyType()) ){				
				if( callbackData.getAmountCharged()>0){					
					JocgSMSMessage smsMessage=new JocgSMSMessage();
					smsMessage.setMsisdn(callbackData.getMsisdn());
					smsMessage.setSmsInterface("smsaircel");
					smsMessage.setChintfId(1);
					smsMessage.setTextMessage("Successful Activation");
					jmsTemplate.convertAndSend("notify.sms", smsMessage);
					//Setting Notification
					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					
					//Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					cal.add(Calendar.MINUTE, 60);
					cdr.setNotifyScheduleTime(cal.getTime());
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setChargedAmount(callbackData.getAmountCharged());
				
					cdr.setStatusDescp("successfully charged");
					
				}
				chargingCDRDao.save(cdr);
			}else if("UNSUB".startsWith(callbackData.getNotifyType())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
					cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
				}
				chargingCDRDao.save(cdr);
			}
	
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,AircelIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			
			boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
			String actionName=callbackData.getNotifyType();
			actionName=(actionName!=null && actionName.length()>0)?actionName.toUpperCase().trim():"NA";
			if(actionName.contains("UNSUB")){
				SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat sdfDateTime=new SimpleDateFormat("yyyyMMddHHmmss");
				try{
					java.util.Date dsubCharged=sub.getSubChargedDate();
					java.util.Date dsubDCT=cal.getTime();
					if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
					if(dsubCharged!=null){
						String dateAct=sdfdate.format(dsubCharged);
						String dateDct=sdfdate.format(dsubDCT);
						sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
						
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.MINUTE, 20);
						java.util.Date expiry20Minchurn=cal.getTime();
						cal.setTimeInMillis(dsubCharged.getTime());
						cal.add(Calendar.HOUR, 24);
						java.util.Date expiry24Hourchurn=cal.getTime();
						churn20Min=(expiry20Minchurn.after(dsubDCT)|| expiry20Minchurn.equals(dsubDCT))?true:false;
						churn24Hour=(expiry24Hourchurn.after(dsubDCT)|| expiry24Hourchurn.equals(dsubDCT))?true:false;
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDateTime.format(sub.getSubChargedDate())
						+",ValidityStart : "+sdfDateTime.format(sub.getValidityStartDate())+", DCT Time :"+sdfDateTime.format(dsubDCT)+",Expiry20MinChurn : "+
						sdfDateTime.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDateTime.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
					}else{
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
					}
				}catch(Exception e){
					logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
				}
			
			}
			
			java.util.Date subRegDate=null;
			try{
				if(sub.getSubChargedDate()!=null) subRegDate=sub.getSubChargedDate();
				else if(sub.getValidityStartDate()!=null) subRegDate=sub.getValidityStartDate();
			}catch(Exception e){
				
			}
			//-------------------------------------------------------------
			
			
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=jocgCache.getCircleInfo(12, "AL");
//			sub.setCircleCode(callbackData.getCircle());
//			if(circle!=null && circle.getCircleid()>0){
//				sub.setCircleId(circle.getCircleid());
//				sub.setCircleName(circle.getCirclename());
//			}
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"NA");
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",sub.getTrafficSourceName());
			//Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();		
			String action=(callbackData.getNotifyType().contains("SUB") && !callbackData.getNotifyType().contains("UNSUB") )?"ACT":callbackData.getNotifyType().contains("UNSUB")?"UNSUB":"REN";
			String jocgTransId=(sub.getJocgTransId()!=null && !"NA".equals(sub.getJocgTransId()))?sub.getJocgTransId():"SDP-"+((callbackData.getCgTransId()!=null)?callbackData.getCgTransId():""+System.currentTimeMillis());
			jocgTransId=("ACT".equalsIgnoreCase(action) && !jocgTransId.toUpperCase().startsWith("DIGI"))?"DIGI-"+System.currentTimeMillis():jocgTransId;
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), jocgTransId, requestCategory, action, ""+callbackData.getMsisdn(), callbackData.getMsisdn(), (platform!=null?platform.getPlatform():"NA"), (platform!=null?platform.getSystemid():"NA"), 
					new JocgRequest(), chintf, chintf, pkgChintf,circle , pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(System.currentTimeMillis()), "NA", "NA", callbackData.getCgTransId(), new java.util.Date(), "", callbackData.toString(), callbackData.getCgTransId(), ""+callbackData.getMsisdn(), new java.util.Date(System.currentTimeMillis()), 
					null, null, false, false, false);
			
			newCDR.setSubRegDate(subRegDate);
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,false,sameDayChurn,churn20Min,churn24Hour);
			newCDR=chargingCDRDao.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			
			//sub.setJocgCDRId(newCDR.getId());
			if("ACT".equalsIgnoreCase(action)){
				sub.setJocgCDRId(newCDR.getId());
				sub.setJocgTransId(newCDR.getJocgTransId());
			}
			sub=subscriberDao.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			
			if("DCT".equalsIgnoreCase(actionName) && sameDayChurn==true){
				if(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
					try{
						//Handle NotifyDCT
						ChargingCDR cdr=chargingCDRDao.findById(sub.getJocgCDRId());
						if(cdr!=null && cdr.getId()!=null && cdr.getRequestType()!=null && "ACT".equalsIgnoreCase(cdr.getRequestType())){
							cdr.setChurnSameDay(sameDayChurn);
							cdr.setChurn20Min(churn20Min);
							cdr.setChurn24Hour(churn24Hour);
							if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
								cdr.setNotifyChurn(true);
								cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
								cdr.setNotifyTimeChurn(new java.util.Date(System.currentTimeMillis()));
								logger.debug(loggerHead+"CHURN NOTIFY LOGIC - DCT Notification QUEUED for churnsameday="+cdr.getChurnSameDay()+", ACT Notify status="+cdr.getNotifyStatus());
							}else{
								logger.debug(loggerHead+"CHURN NOTIFY LOGIC - Condition not satisfied, DCT Notify Flag left unset. Churn sameday="+cdr.getChurnSameDay()+", ACT Notify status="+cdr.getNotifyStatus());
							}
							cdrBuilder.save(cdr);
						}else{
							logger.debug(loggerHead+"CHURN NOTIFY LOGIC - ACT CDR with id "+sub.getJocgCDRId()+" not found!");
						}
						cdr=null;
					}catch(Exception e1){
						logger.debug(loggerHead+"CHURN NOTIFY LOGIC - Exception occured while analysing churn notification scenario, "+e1);
					}
					
				}
			}
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,AircelIndiaSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=jocgCache.getCircleInfo(12, "AL");
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();					
	        String action=(callbackData.getNotifyType().contains("SUB") && !callbackData.getNotifyType().contains("UNSUB") )?"ACT":callbackData.getNotifyType().contains("UNSUB")?"UNSUB":"REN";
	        String jocgTransId="SDP-"+((callbackData.getCgTransId()!=null)?callbackData.getCgTransId():""+System.currentTimeMillis());
			jocgTransId=("ACT".equalsIgnoreCase(action) && !jocgTransId.toUpperCase().startsWith("DIGI"))?"DIGI-"+System.currentTimeMillis():jocgTransId;
			
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				// new JocgRequest() String, String, String, String, String, Long, String, String, JocgRequest, ChargingInterface, ChargingInterface, PackageChintfmap, Circleinfo, PackageDetail, Service, ChargingPricepoint, ChargingPricepoint, VoucherDetail, String, Double, Double, String, Date, Date, Date, String, String, String, TrafficSource, TrafficCampaign, String, String, Integer, String, Date, Integer, Date, Date, Boolean, Boolean, Boolean, LandingPageSchedule, Routing, CgimageSchedule, Integer, String, Integer, String, Date, String, String, String, Date, String, String, String, String, Date, Date, Date, Boolean, Boolean, Boolean) in the type CDRBuilder is not applicable for the arguments (String, String, String, String, String, Long, String, String, JocgRequest, ChargingInterface, ChargingInterface, PackageChintfmap, String, PackageDetail, Service, ChargingPricepoint, ChargingPricepoint, null, String, double, double, String, String, String, String, String, String, String, null, null, String, String, int, String, null, int, null, null, boolean, boolean, boolean, null, null, null, int, String, int, String, Date, String, String, String, Date, String, String, String, String, Date, null, null, boolean, boolean, boolean
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), jocgTransId, requestCategory, action, ""+callbackData.getMsisdn(), callbackData.getMsisdn(), "NA", "NA", 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(System.currentTimeMillis()), "NA", "NA", "NA", new java.util.Date(System.currentTimeMillis()), callbackData.getProductName(), callbackData.toString(), "", JocgString.doubleToStr(callbackData.getMsisdn()), new java.util.Date(System.currentTimeMillis()), 
						null, null, false, false, false);
				
				newCDR.setSubRegDate(new java.util.Date(System.currentTimeMillis()));
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,false,false,false,false);
				logger.debug(loggerHead+"Update CDR for Non Existing Subscriber "+newCDR);
				newCDR=chargingCDRDao.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),action, callbackData.getMsisdn()
						, "OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"NA","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(), new java.util.Date(System.currentTimeMillis()));
				sub.setJocgCDRId(newCDR.getId());
				sub.setJocgTransId(newCDR.getJocgTransId());
				sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subscriberDao.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,AircelIndiaSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,
			boolean activatedFromZero,boolean sameDayChurn,boolean churn20Min,boolean churn24Hour) throws ParseException{
		logger.debug(loggerHead+"Updating CDR for "+chargedPricePoint+" updateSameCDR "+updateSameCDR+" NotifyType "+callbackData.getNotifyType());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");			
	
		Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        java.util.Date startDate=cal.getTime();
        cal.add(Calendar.DATE, callbackData.getValidityPeriod());
        java.util.Date endDate=cal.getTime();
        String endDate1 = sdf.format(cal.getTime());			
		String action=(callbackData.getNotifyType().contains("SUB") && !callbackData.getNotifyType().contains("UNSUB") )?"ACT":callbackData.getNotifyType().contains("UNSUB")?"UNSUB":"REN";		
		try{
			if("SUB-1".equalsIgnoreCase(callbackData.getNotifyType())){
				try{
					logger.debug(loggerHead+"Updating CDR for "+chargedPricePoint.getPricePoint());									
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);	
					cdr.setValidityEndDate(endDate);
					cdr.setSubRegDate(startDate);
					
					logger.debug(loggerHead+"Charged PpId "+cdr.getChargedPpid());
				}
				catch(Exception Ex){
					logger.error(loggerHead+" Exception :  "+Ex);
				}
			}
			else if("SUB-2".equalsIgnoreCase(callbackData.getNotifyType())){					
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
						cdr.setSubRegDate(startDate);
				}
			else if("UNSUB".equalsIgnoreCase(callbackData.getNotifyType())){										
						/*cal.setTime(cdr.getChargingDate());
						cal.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn=cal.getTime();
						cal.setTime(cdr.getRequestDate());
						cal.add(Calendar.MINUTE,20);
						java.util.Date expiry20MinChurn1=cal.getTime();
						cal.setTimeInMillis(System.currentTimeMillis());
						java.util.Date cDate=cal.getTime();
						if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
							cdr.setChurn20Min(true);
						}
						if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
							cdr.setChurnSameDay(true);
						}*/
						cdr.setChurn20Min(churn20Min);
						cdr.setChurn24Hour(churn24Hour);
						cdr.setChurnSameDay(sameDayChurn);
					
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date(System.currentTimeMillis()));
						cdr1.setChargingDate(new java.util.Date(System.currentTimeMillis()));
						cdr1.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
						cdr1.setSdpStatusCode("1");
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						chargingCDRDao.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date(System.currentTimeMillis()));
						cdr.setChargingDate(new java.util.Date(System.currentTimeMillis()));
						cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
						cdr.setSdpStatusCode("1");
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
					}
			}else if("RENEW-1".equalsIgnoreCase(callbackData.getNotifyType()) || "RENEW-2".equalsIgnoreCase(callbackData.getNotifyType()) || "RENEW-3".equalsIgnoreCase(callbackData.getNotifyType())){
					logger.debug(loggerHead+" : Notify Type : "+callbackData.getNotifyType());
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("REN");//REN/GRACE
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode("1");
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date(System.currentTimeMillis()));
						cdr1.setChargingDate(new java.util.Date(System.currentTimeMillis()));
						if("RENEW-1".equalsIgnoreCase(callbackData.getNotifyType())){
							logger.debug(loggerHead+" : Notify Type : "+callbackData.getNotifyType()+" PricePointz : "+chargedPricePoint.getPricePoint()+" OperatorKey : "+chargedPricePoint.getOperatorKey());
							cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr1.setChargedPpid(chargedPricePoint.getPpid());
							cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
						chargingCDRDao.insert(cdr1);
					}else if("RENEW-1".equalsIgnoreCase(callbackData.getNotifyType())){
						cdr.setRequestType("REN");//REN/GRACE
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode("1");
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date(System.currentTimeMillis()));
						cdr.setChargingDate(new java.util.Date(System.currentTimeMillis()));
						cdr.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr.setChargedPpid(chargedPricePoint.getPpid());
						cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}else if("RENEW-3".equalsIgnoreCase(callbackData.getNotifyType())){
						cdr.setRequestType("REN");//REN/GRACE
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode("1");
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
						cdr.setCgStatusDescp("Customer Moved to Suspend");
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(new java.util.Date());									
					}
					else if("RENEW-2".equalsIgnoreCase(callbackData.getNotifyType())){
						cdr.setRequestType("REN");//REN/GRACE
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode("1");
						cdr.setStatus(JocgStatusCodes.CHARGING_SDPGRACE);
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(new java.util.Date());									
					}
			}			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,AircelIndiaSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
			
			Date date = new Date();
			String startDate=sdf.format(date);
			Calendar cal = Calendar.getInstance();
	        cal.setTime(sdf.parse(sdf.format(date)));
	        cal.add(Calendar.DATE, callbackData.getValidityPeriod());
	        String endDate1 = sdf.format(cal.getTime());			
			if(callbackData.getNotifyType().startsWith("SUB")){				
				if(callbackData.getNotifyType().equalsIgnoreCase("SUB-1")){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getAmountCharged()+", Callback Mode "+callbackData.getChannelType());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					
				}
				if(callbackData.getNotifyType().equalsIgnoreCase("RENEW-1")){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getAmountCharged()+", Callback Mode "+callbackData.getChannelType());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
					sub.setValidityEndDate(endDate);
					sub.setLastRenewalDate(new java.util.Date(System.currentTimeMillis()));
					sub.setNextRenewalDate(endDate);
				}
				else if(callbackData.getNotifyType().equalsIgnoreCase("SUB-2")){
					//Low Balance/Parking
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
					sub.setStatusDescp("Low Balance/Parking");
					sub.setChargedAmount(0D);
//					sub.setParkingDate(startDate);
				}
					else if(callbackData.getNotifyType().equalsIgnoreCase("SUB-3")){
						//Low Balance/Parking
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
						sub.setStatusDescp("Suspended");
						sub.setChargedAmount(0D);
//						sub.setParkingDate(startDate);
					}else{
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
					sub.setChargedAmount(0D);
					sub.setStatusDescp("Charging Failed at SDP|SDP Type="+callbackData.getNotifyType()+",Channel="+callbackData.getChannelType()+",Amount="+callbackData.getAmountCharged());
				}
				sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
			}
		else if("UNSUB".startsWith(callbackData.getNotifyType())){				
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelType());
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
				}
			else if("RENEW-1".equalsIgnoreCase(callbackData.getNotifyType())){				
					cal=Calendar.getInstance();
					Date date1 = new Date();
					cal.setTime(date1);			
					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);	
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(new java.util.Date(System.currentTimeMillis()));
			}else if("RENEW-2".equalsIgnoreCase(callbackData.getNotifyType())){
				cal=Calendar.getInstance();
				Date date1 = new Date();
				cal.setTime(date1);			
				cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
				java.util.Date endDate=cal.getTime();
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
				sub.setStatusDescp("Customer Moved to Grace");
				sub.setValidityEndDate(endDate);
				sub.setNextRenewalDate(endDate);
				sub.setGraceDate(new java.util.Date());
			}
			else if("RENEW-3".equalsIgnoreCase(callbackData.getNotifyType())){
				cal=Calendar.getInstance();
				Date date1 = new Date();
				cal.setTime(date1);			
				cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
				java.util.Date endDate=cal.getTime();
				sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE);
				sub.setStatusDescp("Customer Moved to Suspend");
				sub.setValidityEndDate(endDate);
				sub.setNextRenewalDate(endDate);
				sub.setGraceDate(new java.util.Date());
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}	
	
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
	 Platform pl=null,pldefault=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
		logger.debug(logHeader+" :: systemId="+systemId+",defaultSystemId="+defaultSystemId+":pl="+pl+"|pldefault="+pldefault);
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return (pl!=null?pl:pldefault);
 }
	
	
}

