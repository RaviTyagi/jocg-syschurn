package com.jss.jocg.callback.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.IdeaSdpCallback;
import com.jss.jocg.callback.data.voda.VodafoneIndiaSDPCallback;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.CallbackService;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class IdeaSdpCallbackReceiver_WithMsisdn {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(IdeaSdpCallbackReceiver_WithMsisdn.class);
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");
	/**
	 * should be used only when msisdn is being read from headers while receiving request
	 * @param callbackData
	 */
	
	//@JmsListener(destination = "callback.ideasdp", containerFactory = "myFactory")
	public void receiveMessage(IdeaSdpCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+"|";
		logger.debug(loggerHead+" :: Received (Original Idea)||| <" + callbackData + ">");
		System.out.println("Received Original idea "+callbackData);
		try{
			String msisdnStr=""+callbackData.getMsisdn();
			if(msisdnStr.length()<=10) callbackData.setMsisdn(""+JocgString.strToLong("91"+callbackData.getMsisdn(),0L));
			if(callbackData!=null && JocgString.strToLong(callbackData.getMsisdn(),0L)>0 && !"91910".equalsIgnoreCase(callbackData.getMsisdn()) && !"910".equalsIgnoreCase(callbackData.getMsisdn()) ){	
				callbackData.setAction("NETCHRN".equalsIgnoreCase(callbackData.getAction())?"DCT":"SDCT".equalsIgnoreCase(callbackData.getAction())?"DCT":callbackData.getAction());
				
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getServiceKey()+", Price "+callbackData.getPrice());
			
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getServiceKey()+", Price "+callbackData.getPrice());
				int defaultOperatorId=callbackData.getServiceKey().equalsIgnoreCase("RDIGTV")?14:5;
				ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(),defaultOperatorId,JocgString.strToDouble(callbackData.getPrice(),0D));
				//Search into Not default Operator
				if(cpp==null) cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(),(defaultOperatorId==14?5:14),JocgString.strToDouble(callbackData.getPrice(),0D));
				
				if(cpp==null){
					cpp=jocgCache.getPricePointListByOperatorKeyAndOperator(callbackData.getServiceKey(),defaultOperatorId);
					if(cpp==null) cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(),(defaultOperatorId==14?5:14));
				}
				
				ChargingPricepoint cppmaster=null;
				
				
				logger.debug(loggerHead+" Charged Price Point "+cpp);
				if(cpp!=null && cpp.getPpid()>0){
					callbackData.setStartDate(validateDate(callbackData.getStartDate(),0));
					callbackData.setEndDate(validateDate(callbackData.getEndDate(),cpp.getValidity()));
					cppmaster=(cpp.getIsfallback()==0)?cpp:jocgCache.getPricePointListById(cpp.getMppid());
					logger.debug(loggerHead+" Master Price Point "+cppmaster);
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn()+", Operator Service Key "+serviceKey);
					Subscriber sub=subscriberDao.findByJocgTransId(callbackData.getRefrenceId());
					if(sub==null || sub.getId()==null){
						sub=subscriberDao.findByMsisdnAndOperatorServiceKey(JocgString.strToLong(callbackData.getMsisdn(), 0), serviceKey);
						
						if(sub!=null && cpp!=null && sub.getChargingInterfaceId()!=cpp.getChintfid()){
							logger.debug(loggerHead+" Subscriber Charging Interface is different, Searching Charging price point for registered Operator "+sub.getChargingInterfaceId()+", key "+serviceKey);
							ChargingPricepoint cpp1=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(),sub.getChargingInterfaceId(),JocgString.strToDouble(callbackData.getPrice(),0D));
							if(cpp1!=null){
								cpp=cpp1;
								cppmaster=(cpp.getIsfallback()==0)?cpp:jocgCache.getPricePointListById(cpp.getMppid());
								logger.debug(loggerHead+" Changed Charged Price Point "+cpp+", master pp "+cppmaster);
							}
							
						}
						
						
					}else{
						try{
							if(sub.getSubscriberId().startsWith(".equalsIgnoreCase(sourceOperator)") && JocgString.strToLong(callbackData.getMsisdn(),0)!=sub.getMsisdn()) {
								sub.setMsisdn(JocgString.strToLong(callbackData.getMsisdn(),0));
								sub.setSubscriberId(callbackData.getMsisdn());
							}
						}catch(Exception e){
							logger.error(loggerHead+" Exception while updating msisdn "+callbackData.getMsisdn()+", sub msi "+sub.getMsisdn());
						}
					}
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						
						if( "REN".equalsIgnoreCase(callbackData.getAction()) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(callbackData.getStartDate())){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if("DCT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(callbackData.getAction()) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && sdfDate.format(sub.getSubChargedDate()).equalsIgnoreCase(callbackData.getStartDate()) ){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
						}else{
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null){
								if("DCT".equalsIgnoreCase(cdr.getRequestType())){
									logger.debug(loggerHead+"  CDR Found but for DCT, Process for Existing Subscriber and Non Existing ACT CDR...");
									handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
								}else{
									logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
									handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
								}
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);
						
					}
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored! "+callbackData);
		}else logger.error(loggerHead+" Invalid Msisdn "+callbackData.getMsisdn()+" in callback, Callback Ignored!");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				
//					String suntechURL=callbackData.getDigiviveCallbackURL();
//					JcmsSSLClient sslClient=new JcmsSSLClient();
//					sslClient.setConnectionTimeoutInSecond(5);
//					sslClient.setReadTimeoutInSecond(10);
//					JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL("IDEA-REFWD-SUNTECH",suntechURL);
//					logger.debug(loggerHead+" SUNTECH-URL:"+suntechURL+"|Resp="+resp.getResponseData()+"|Error="+resp.getErrorMessage()+"|RespTime="+resp.getResponseTime());
			
			}catch(Exception e1){
				logger.debug(loggerHead+" SUNTECH-URL ERROR "+e1.getMessage());
			}
		}
		
	}
	
	public void handleExistingSubscriber(String loggerHead,IdeaSdpCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false);
			
			Circleinfo circle=jocgCache.getCircleInfo(cppmaster.getChintfid(), callbackData.getCircle());
			if(circle!=null && circle.getCircleid()>0){
				sub.setCircleId(circle.getCircleid());
				sub.setCircleName(circle.getCirclename());
				cdr.setCircleId(circle.getCircleid());
				cdr.setCircleName(circle.getCirclename());
			}else{
				circle=new Circleinfo();
				circle.setCirclecode(callbackData.getCircle());
				circle.setCirclename(callbackData.getCircle());
			}
			sub.setCircleCode(callbackData.getCircle());
			cdr.setCircleCode(callbackData.getCircle());
			
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(callbackData.getAction()) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus()) && JocgString.strToDouble(callbackData.getPrice(), 0D)>0){
					JocgSMSMessage smsMessage=new JocgSMSMessage();
					smsMessage.setMsisdn(JocgString.strToLong(callbackData.getMsisdn(),0L));
					smsMessage.setSmsInterface("ideain");
					smsMessage.setServiceName(sub.getServiceName());
					smsMessage.setChintfId(sub.getChargingInterfaceId());
					smsMessage.setChargingInterfaceType(sub.getChargingInterfaceType());
					smsMessage.setTextMessage("Successful Activation");
					jmsTemplate.convertAndSend("notify.sms", smsMessage);
					//Setting Notification
					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					cal.add(Calendar.MINUTE, 60);
					cdr.setNotifyScheduleTime(cal.getTime());
					
				}
				cdrBuilder.save(cdr);
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,IdeaSdpCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=jocgCache.getCircleInfo(cppmaster.getChintfid(), callbackData.getCircle());
			if(circle==null) circle=new Circleinfo();
			logger.debug(loggerHead+"Circle "+circle+", callback circle "+callbackData.getCircle());
			circle.setCirclecode(callbackData.getCircle());
			if(circle.getCircleid()<=0) circle.setCirclename(callbackData.getCircle());
			sub.setCircleCode(callbackData.getCircle());
			if(circle!=null && circle.getCircleid()>0){
				sub.setCircleId(circle.getCircleid());
				sub.setCircleName(circle.getCirclename());
				sub.setCircleCode(circle.getCirclecode());
			}else{
				sub.setCircleCode(callbackData.getCircle());
				sub.setCircleName(callbackData.getCircle());
			}
			logger.debug(loggerHead+"After update Circle "+circle);
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
			java.util.Date startDate=sdf.parse(callbackData.getStartDate());
			java.util.Date endDate1=sdf.parse(callbackData.getEndDate());
			
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"NA");
			if(platform==null){
				platform=new Platform();
				platform.setSystemid("NA");
				platform.setPlatform("NA");
			}
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, callbackData.getAction(), sub.getSubscriberId(), sub.getMsisdn(), platform.getPlatform(), platform.getSystemid(), 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatus(), callbackData.toString(), callbackData.getRefrenceId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
					null, null, false, false, false);
			logger.debug(loggerHead+"CDR Object Formed ");
			try{
			newCDR.setSubRegDate(sub.getSubChargedDate());
			}catch(Exception ee){
				newCDR.setSubRegDate(startDate);
			}
			logger.debug(loggerHead+"Subregdate is set.");
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
			logger.debug(loggerHead+"CDR Status updated.");
			newCDR.setCircleCode(callbackData.getCircle());
			if(circle!=null && circle.getCircleid()>0){
				newCDR.setCircleId(circle.getCircleid());
				newCDR.setCircleName(circle.getCirclename());
			}
			logger.debug(loggerHead+"Circle Information added.");
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(callbackData.getAction())) sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,IdeaSdpCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			System.out.println("chintf :"+chintf);
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			System.out.println("pkgChintf :"+pkgChintf);
			Circleinfo circle=jocgCache.getCircleInfo(cppmaster.getChintfid(), callbackData.getCircle());
			if(circle==null) circle=new Circleinfo();
			circle.setCirclecode(callbackData.getCircle());
			if(circle.getCircleid()<=0) circle.setCirclename(callbackData.getCircle());
			System.out.println("circle :"+circle);
			
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			System.out.println("pkg :"+pkg);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			System.out.println("service :"+service);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
			java.util.Date startDate=sdf.parse(callbackData.getStartDate());
			java.util.Date endDate1=sdf.parse(callbackData.getEndDate());
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::","NA","NA");
				if(platform==null){
					platform=new Platform();
					platform.setSystemid("NA");
					platform.setPlatform("NA");
				}
				
				TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getRefrenceId(), requestCategory, callbackData.getAction(), ""+callbackData.getMsisdn(), JocgString.strToLong(callbackData.getMsisdn(),0L), platform.getPlatform(), platform.getSystemid(), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), callbackData.getStatus(), callbackData.toString(), callbackData.getRefrenceId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getMsisdn(),JocgString.strToLong(callbackData.getMsisdn(),0L),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"NA","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),new java.util.Date());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub.setCircleCode(callbackData.getCircle());
				if(circle!=null && circle.getCircleid()>0){
					sub.setCircleId(circle.getCircleid());
					sub.setCircleName(circle.getCirclename());
				}
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,IdeaSdpCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR){
		loggerHead +="UCS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			java.util.Date startDate=sdf.parse(callbackData.getStartDate()+""+callbackData.getStartTime());
			java.util.Date endDate1=sdf.parse(callbackData.getEndDate() + ""+callbackData.getEndTime());
			
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setSubRegDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if("BAL-LOW".equalsIgnoreCase(callbackData.getStatus())){
					if("PARKING".equalsIgnoreCase(callbackData.getMode())){
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
					}else{
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
					}
				}else{
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
				}
				cdr.setSdpStatusCode(callbackData.getStatus());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				
			}else if("DCT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					Calendar cal=Calendar.getInstance();
					cal.setTime(cdr.getChargingDate());
					cal.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn=cal.getTime();
					cal.setTime(cdr.getRequestDate());
					cal.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn1=cal.getTime();
					
					cal.setTime(cdr.getChargingDate());
					cal.add(Calendar.HOUR,24);
					java.util.Date expiry24HourChurn=cal.getTime();
					cal.setTime(cdr.getRequestDate());
					cal.add(Calendar.HOUR,24);
					java.util.Date expiry24HourChurn1=cal.getTime();
					
					cal.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal.getTime();
					
					SimpleDateFormat sdfnew=new SimpleDateFormat("yyyyMMdd");
					String chargingDateStr=sdfnew.format(cdr.getChargingDate());
					String cDateStr=sdfnew.format(cDate);
					if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
						cdr.setChurn20Min(true);
					}
					
					if(cDate.before(expiry24HourChurn) || cDate.before(expiry24HourChurn1)){
						cdr.setChurn24Hour(true);
					}
					
					logger.debug(loggerHead+" Comparing "+cDateStr+" with "+chargingDateStr);
					if(cDateStr.equalsIgnoreCase(chargingDateStr)){//if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
						cdr.setChurnSameDay(true);
					}
					
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatus());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr1.setStatusDescp("Customer Deactivated");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date());
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatus());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
						cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						cdr.setStatusDescp("Customer Deactivated");
					}
				}
			}else if("REN".equalsIgnoreCase(callbackData.getAction()) || "GRACE".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType(callbackData.getAction());//REN/GRACE
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getStatus());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setChargingDate(startDate);
						if("REN".equalsIgnoreCase(callbackData.getAction())  && chargedPricePoint!=null && chargedPricePoint.getPpid()>0 ){
							cdr1.setRequestType("REN");
							cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr1.setChargedPpid(chargedPricePoint.getPpid());
							cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr1.setStatusDescp("Renewal Successful");
						}else if("GRACE".equalsIgnoreCase(callbackData.getAction())){
							cdr1.setRequestType("REN-GRACE");
							cdr1.setChargedAmount(0D);
							cdr1.setChargedOperatorKey("NA");
							cdr1.setChargedOperatorParams("NA");
							cdr1.setChargedPpid(0);
							cdr1.setChargedPricePoint(0D);
							cdr1.setChargedValidityPeriod("NA");
							cdr1.setFallbackCharged(false);
							cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
							cdr1.setStatusDescp("Moved to Grace");
							
						}
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType(callbackData.getAction());//REN/GRACE
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getStatus());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						if("REN".equalsIgnoreCase(callbackData.getAction())  && chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setRequestType("REN");
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr.setStatusDescp("Renewal Successful");
						}else if("GRACE".equalsIgnoreCase(callbackData.getAction())){
							cdr.setRequestType("REN-GRACE");
							cdr.setChargedAmount(0D);
							cdr.setChargedOperatorKey("NA");
							cdr.setChargedOperatorParams("NA");
							cdr.setChargedPpid(0);
							cdr.setChargedPricePoint(0D);
							cdr.setChargedValidityPeriod("NA");
							cdr.setFallbackCharged(false);
							cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
							cdr.setStatusDescp("Moved to Grace");
						}
					}
				}
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,IdeaSdpCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
			java.util.Date startDate=sdf.parse(callbackData.getStartDate()+""+callbackData.getStartTime());
			java.util.Date endDate1=sdf.parse(callbackData.getEndDate() + ""+callbackData.getEndTime());
			if("ACT".equalsIgnoreCase(callbackData.getAction())){
				
				if("success".equalsIgnoreCase(callbackData.getStatus())){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode "+callbackData.getMode());
					Calendar cal=Calendar.getInstance();
					cal.setTime(endDate1);
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,59);
					cal.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
				}else if("BAL-LOW".equalsIgnoreCase(callbackData.getStatus())){
					//Low Balance/Parking
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
					sub.setStatusDescp("Low Balance/Parking");
					sub.setChargedAmount(0D);
					sub.setParkingDate(startDate);
				}else{
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
					sub.setChargedAmount(0D);
					sub.setStatusDescp("Charging Failed at SDP|SDP Status="+callbackData.getStatus()+",Mode="+callbackData.getMode()+",Amount="+callbackData.getPrice());
				}
				sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
			}else if("DCT".equalsIgnoreCase(callbackData.getAction()) || "SDCT".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					if(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING)
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
					else if(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_GRACE)
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE);
					else
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
				}
			}else if("REN".equalsIgnoreCase(callbackData.getAction())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getStatus())){
					Calendar cal=Calendar.getInstance();
					cal.setTime(startDate);
					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);	
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(new java.util.Date());
				}
			}else if("GRACE".equalsIgnoreCase(callbackData.getAction())){
				Calendar cal=Calendar.getInstance();
				cal.setTime(startDate);
				cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
				java.util.Date endDate=cal.getTime();
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
				sub.setStatusDescp("Customer Moved to Grace");
				sub.setValidityEndDate(endDate);
				sub.setNextRenewalDate(endDate);
				sub.setGraceDate(new java.util.Date());
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
	 Platform pl=null,pldefault=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return (pl!=null?pl:pldefault);
 }
 
 public String validateDate(String dateStr,int validity){
	 //sdfDate
	 java.util.Date d=null;
	 try{
		 d=sdfDate.parse(dateStr);
	 }catch(Exception e){
		 Calendar cal=Calendar.getInstance();
		 cal.setTimeInMillis(System.currentTimeMillis());
		 cal.add(Calendar.DATE, validity);
		 d=cal.getTime();
	 }
	 return sdfDate.format(d);
 }
 
}
