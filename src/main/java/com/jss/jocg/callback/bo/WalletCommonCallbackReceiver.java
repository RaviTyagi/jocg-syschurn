package com.jss.jocg.callback.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.data.WalletCommonCallback;
import com.jss.jocg.callback.data.voda.VodafoneIndiaSDPCallback;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class WalletCommonCallbackReceiver {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(WalletCommonCallbackReceiver.class);
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");

	
	
	
	@JmsListener(destination = "callback.walletsdp", containerFactory = "myFactory")
    public void receiveMessage(WalletCommonCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+"|";
	
		logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
		try{
			long msisdnLong=JocgString.strToLong(callbackData.getMsisdn(), 0L);
			if(callbackData!=null && (msisdnLong>0 || (callbackData.getSubscriberId()!=null && callbackData.getSubscriberId().length()>0))){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+",subId "+callbackData.getSubscriberId()+", Key "+callbackData.getPackageName()+", Price "+callbackData.getBasePrice());
				ChargingInterface sourceChintf=jocgCache.getChargingInterfaceByOpcode(callbackData.getSourceOperatorCode());
				PackageDetail pack=jocgCache.getPackageDetailsByName(callbackData.getPackageName());
				
				ChargingPricepoint cpp=null;PackageChintfmap chintfmap=null;
				if(sourceChintf!=null && sourceChintf.getChintfid()>0 && pack!=null && pack.getPkgid()>0){
				chintfmap=jocgCache.getPackageChargingInterfaceMap(sourceChintf.getChintfid(), pack.getPkgid(), callbackData.getBasePrice());
					if(chintfmap!=null && chintfmap.getPkgchintfid()>0){
						cpp=jocgCache.getPricePointListById(chintfmap.getOpPpid());
					}else{
						logger.debug(loggerHead+"Failed to find Price point map for  Pack "+pack.getPkgid()+", Source Network  "+sourceChintf.getChintfid()+", Price "+callbackData.getBasePrice());
					}
				}else{
					logger.debug(loggerHead+"Failed to search pack and Source network details against packcode "+callbackData.getPackageName()+", opcode "+callbackData.getSourceOperatorCode());
				}
				
				//ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getServiceKey(), callbackData.getPrice());
				logger.debug(loggerHead+"CPP based on price and key "+cpp);
				
				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=callbackData.getMsisdn().trim();
					if(msisdnStr.length()<=10) callbackData.setMsisdn(""+JocgString.strToLong("91"+msisdnStr,msisdnLong));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn()+", Operator Service Key "+serviceKey);
					
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(JocgString.strToLong(callbackData.getMsisdn(), 0L),serviceKey);	
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						String action="ACT";
					//	if("ACT".equalsIgnoreCase(callbackData.getAction()) && callbackData.getReferenceId().startsWith("PRISM")) callbackData.setAction("REN");
						
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						if("ACT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
						}else{
							handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,sourceChintf,chintfmap,pack);
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sourceChintf,chintfmap,pack);
						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
			
		}catch(Exception e){
			logger.debug(loggerHead+" :: Exception "+e);
			e.printStackTrace();
		}
        
    }
	
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,WalletCommonCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingInterface sourceChintf,PackageChintfmap pkgChintf,PackageDetail pkg){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="DV-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			//PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();
			//PackageDetail pkg=jocgCache.getPackageDetailsByName(callbackData.getPackageName());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			if(pkg.getPkgid()!=sub.getPackageId()){
				sub.setPackageId(pkg.getPkgid());
				sub.setPackageName(pkg.getPkgname());
				sub.setPackagePrice(pkg.getPricepoint());
				if(service!=null){
					sub.setServiceId(service.getServiceid());
					sub.setServiceName(service.getServiceName());
				
				}
				if(sub.getPackageChargingInterfaceMapId()!=pkgChintf.getPkgchintfid()){
					sub.setPackageChargingInterfaceMapId(pkgChintf.getPkgchintfid());
				}
			}
			
		
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DATE, cpp.getValidity());
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			java.util.Date endDate=cal.getTime();
			
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",sub.getSystemId(),"1JBNCV");
			
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",sub.getTrafficSourceName());
			if(ts==null) ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, "ACT", sub.getSubscriberId(), sub.getMsisdn(), platform.getPlatform(), platform.getSystemid(), 
					new JocgRequest(), sourceChintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "DV-CALLBACK", 
					startDate, startDate, endDate, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), "SUCCESS", callbackData.toString(), callbackData.getTransId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
					null, null, false, false, false);
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,WalletCommonCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,ChargingInterface sourceChintf,PackageChintfmap pkgChintf,PackageDetail pkg){
		loggerHead +="HNES::";
		try{
			String requestCategory="DV-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			//PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();
			//PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DATE, cpp.getValidity());
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			java.util.Date endDate=cal.getTime();
			
			Platform platform=getPlatformBySystemId(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getSystemId(),"1JBNCV");
		
			TrafficSource ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::",callbackData.getVendorName());
			if(ts==null) ts=getTrafficSourceByName(loggerHead+""+callbackData.getMsisdn()+"::","NA");
			
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getTransId(), requestCategory, "ACT", ""+callbackData.getMsisdn(), JocgString.strToLong(callbackData.getMsisdn(),0L),platform.getPlatform(), platform.getSystemid(), 
						new JocgRequest(), sourceChintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "DV-CALLBACK", 
						startDate, startDate, endDate, "NA", "NA", "NA", ts, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", new java.util.Date(), "NA", "NA", "NA", new java.util.Date(), "SUCCESS", callbackData.toString(), callbackData.getTransId(), ""+callbackData.getMsisdn(), new java.util.Date(), 
						null, null, false, false, false);
				
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getMsisdn(),JocgString.strToLong(callbackData.getMsisdn(),0L),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),sourceChintf.getChintfid(),sourceChintf.getOpcode(),sourceChintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",new java.util.Date(),
						new java.util.Date(),new java.util.Date(),new java.util.Date(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,platform.getSystemid(),platform.getPlatform(),"W","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),new java.util.Date());
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,WalletCommonCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR){
		loggerHead +="UCS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DATE, chargedPricePoint.getValidity());
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			java.util.Date endDate=cal.getTime();
			
			logger.debug(loggerHead+"Updating CDR for "+chargedPricePoint);
				cdr.setActivatedFromParking(false);
			cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
			cdr.setStatusDescp("Customer Activated");
			cdr.setChargedPpid(chargedPricePoint.getPpid());
			cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
			double chargedAmount=callbackData.getBasePrice()-callbackData.getDiscount();
			cdr.setDiscountGiven(callbackData.getDiscount());
			cdr.setChargedAmount(chargedAmount);
			cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
			cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
			cdr.setChargingDate(startDate);
			cdr.setValidityStartDate(startDate);
			cdr.setValidityEndDate(endDate);
			
			logger.debug(loggerHead+"Charged PpId "+cdr.getChargedPpid());
			cdr.setSdpStatusCode("NA");
			cdr.setSdpStatusDescp("NA");
			cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	public Subscriber updateSubscriberStatus(String loggerHead,WalletCommonCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DATE, chargedPricePoint.getValidity());
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			java.util.Date endDate=cal.getTime();
			
			logger.debug(loggerHead+"Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getBasePrice());
			sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
			sub.setStatusDescp("Customer Activated");
			sub.setPricepointId(masterPricePoint.getPpid());
			sub.setOperatorPricePoint(masterPricePoint.getPricePoint());
			sub.setOperatorServiceKey(masterPricePoint.getOperatorKey());
			sub.setOperatorParams(masterPricePoint.getOpParams());
		
			double chargedAmount=callbackData.getBasePrice()-callbackData.getDiscount();
			double subSRevenue=sub.getSubSessionRevenue()+chargedAmount;
			logger.debug(loggerHead+"SubSession Revenue "+subSRevenue);
			sub.setSubSessionRevenue(subSRevenue);
			sub.setChargedAmount(chargedAmount);
			sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
			sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
			sub.setSubChargedDate(startDate);
			sub.setValidityEndDate(endDate);
			sub.setNextRenewalDate(endDate);
			sub.setLifecycleId(callbackData.getLifecycleId());
			sub.setCustomerId(callbackData.getLifecycleId());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
 public Platform getPlatformBySystemId(String logHeader,String systemId,String defaultSystemId){
	 Platform pl=null,pldefault=null;
	 try{
		Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
		Iterator<Platform> i=platfromList.values().iterator();
		while(i.hasNext()){
			pl=i.next();
			if(pldefault==null && pl.getSystemid().equalsIgnoreCase(defaultSystemId)) pldefault=pl;
			if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
			else pl=null;
		}
	 }catch(Exception e){
		 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
	 }
	 return (pl!=null?pl:pldefault);
 }

}
