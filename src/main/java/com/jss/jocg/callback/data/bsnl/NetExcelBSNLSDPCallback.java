package com.jss.jocg.callback.data.bsnl;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class NetExcelBSNLSDPCallback implements Serializable{
	
	
	public final String KEY_KEYWORD="keyword";//
	public final String KEY_REQ_TYPE="reqtype";//reqtype
	public final String KEY_REQUEST_TIMESTAMP="requesttimestamp";//
	public final String KEY_SERVICE_ID="serviceid";//
	public final String KEY_MODE="mode";//
	public final String KEY_CONSENT_MODE="consentmode";//
	public final String KEY_CONSENT_STATUS="consentstatus";//
	public final String KEY_REMARKS="remarks";//
	public final String KEY_MSISDN="msisdn";//
	public final String KEY_AUTORENEWAL="autorenewal";//
	public final String KEY_CG_ID="cgid";//
	public final String KEY_PRICE="price";//
	public final String KEY_VALIDITY="VALIDITY";//			
	public final String KEY_TID="transactionid";//	
	public final String KEY_PARAM1="param1";
	public final String KEY_PARAM="param";
	public final String KEY_SHORT_CODE="shortcode";
	//msisdn=919403666734&serviceid=00703638&keyword=00&shortcode=&reqtype=ACT&requesttimestamp=141028134519&
	//price=10.0&validity=4320&mode=WAP&consentmode=WAP&consentstatus=SUCCESS&remarks=&autorenewal=1&
	//transactionid=TX007BWOOFOW0004129358&cgid=CG007001140928134522176&param1=1234&param1=1&param=1 
	String zoneName;
	String keyword;//
	String req_type;//
	Long msisdn;//
	String mode;//
	
	int ppid;
	
	String short_code;//
	String request_timestamp;//
	String serviceId;//
	String consent_Status;//
	String remarks;//
	String consent_mode;//
	String auto_renewal;//
	String cg_id;//	
	String param;
	Double price;//
	String VALIDITY;//
	String transactionId;//	
	String param1;
	java.util.Date stvPackStartDate;
	
	public NetExcelBSNLSDPCallback(){
		
	}
	
	public String getDigiviveCallbackURL(){		 
		String msisdnTemp=Long.toString(this.msisdn).length()>10?Long.toString(this.msisdn).substring(Long.toString(this.msisdn).length()-10):Long.toString(this.msisdn);
		if(!"910".equalsIgnoreCase(Long.toString(this.msisdn)) && !"0".equalsIgnoreCase(Long.toString(this.msisdn)) && !"91910".equalsIgnoreCase(Long.toString(this.msisdn))){
		String ZonePort=this.zoneName.equals("BW_OF")?"8096":"8093";
		String digiviveURL="http://172.31.19.183:"+ZonePort+"/SdplService/HttpPostCollector/"+this.zoneName+"?"
				+ "msisdn="+this.msisdn.longValue()+"&serviceid="+this.serviceId+"&keyword=00&shortcode=&reqtype="+this.req_type+
				"&requesttimestamp="+this.request_timestamp+"&price="+this.price.doubleValue()+"&validity="+this.VALIDITY+"&mode=WAP&consentmode=WAP&"
						+ "consentstatus=&remarks=&autorenewal=1&transactionid="+this.transactionId+"&cgid="+this.cg_id+"&param1="+this.param1+"&param1=1&param=1";		
		return digiviveURL;
		}else 
		return "";
		
	}
	
	
	public NetExcelBSNLSDPCallback(String keyword,String req_type,Long msisdn,String mode,String short_code,String serviceId,String request_timestamp,String consent_Status,String remarks,
	String consent_mode,String auto_renewal,String cg_id,String param,Double price,String transactionId,String VALIDITY, String param1){

		
		
	this.keyword=keyword;
	this.req_type=req_type;
	this.msisdn=msisdn;
	this.mode=mode;
	this.short_code=short_code;	
	this.request_timestamp=request_timestamp;
	this.serviceId=serviceId;
	this.consent_Status=consent_Status;
	this.remarks=remarks;
	this.consent_mode=consent_mode;
	this.auto_renewal=auto_renewal;
	this.cg_id=cg_id;
	this.param=param;
	this.param1=param1;
	this.VALIDITY=VALIDITY;
	this.price=price;
	this.transactionId=transactionId;
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("NetExcelBSNLSDPCallback{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	
	public void addNewField(String key,String value){
		try{
			if(KEY_KEYWORD.equalsIgnoreCase(key)){
				this.setKeyword((value));
			}else if(KEY_CONSENT_MODE.equalsIgnoreCase(key)){
				this.setConsent_mode(value);
			}else if(KEY_MSISDN.equalsIgnoreCase(key)){
				value=value.trim();
				value=(value.length()<=10)?"91"+value:value;
				this.setMsisdn(JocgString.strToLong(value, 0L));
			}else if(KEY_SERVICE_ID.equalsIgnoreCase(key)){
				this.setServiceId(value);
			}else if(KEY_SHORT_CODE.equalsIgnoreCase(key)){
				this.setShort_code(value);
			}else if(KEY_REQ_TYPE.equalsIgnoreCase(key)){
				this.setReq_type(value);
			}else if(KEY_MODE.equalsIgnoreCase(key)){
				this.setMode(value);
			}else if(KEY_REQUEST_TIMESTAMP.equalsIgnoreCase(key)){
				this.setRequest_timestamp(value);
			}else if(KEY_CONSENT_STATUS.equalsIgnoreCase(key)){
				this.setConsent_Status(value);
			}else if(KEY_REMARKS.equalsIgnoreCase(key)){
				this.setRemarks(value);
			}else if(KEY_CONSENT_MODE.equalsIgnoreCase(key)){
				this.setConsent_mode(value);
			}else if(KEY_AUTORENEWAL.equalsIgnoreCase(key)){
				this.setAuto_renewal(value);
			}else if(KEY_CG_ID.equalsIgnoreCase(key)){
				this.setCg_id(value);
			}else if(KEY_PARAM.equalsIgnoreCase(key)){
				this.setParam(value);
			}else if(KEY_VALIDITY.equalsIgnoreCase(key)){
				this.setVALIDITY(value);
			}else if(KEY_PARAM1.equalsIgnoreCase(key)){
				this.setParam1(value);
			}else if(KEY_PRICE.equalsIgnoreCase(key)){
				this.setPrice(JocgString.strToDouble(value, 0L));
			}else if(KEY_TID.equalsIgnoreCase(key)){
				this.setTransactionId(value);
			}
			
			}catch(Exception e){}
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getReq_type() {
		return req_type;
	}

	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}

	public int getPpid() {
		return ppid;
	}

	public void setPpid(int ppid) {
		this.ppid = ppid;
	}

	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}

	public String getRequest_timestamp() {
		return request_timestamp;
	}

	public void setRequest_timestamp(String request_timestamp) {
		this.request_timestamp = request_timestamp;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getConsent_Status() {
		return consent_Status;
	}

	public void setConsent_Status(String consent_Status) {
		this.consent_Status = consent_Status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getConsent_mode() {
		return consent_mode;
	}

	public void setConsent_mode(String consent_mode) {
		this.consent_mode = consent_mode;
	}

	public String getAuto_renewal() {
		return auto_renewal;
	}

	public void setAuto_renewal(String auto_renewal) {
		this.auto_renewal = auto_renewal;
	}

	public String getCg_id() {
		return cg_id;
	}

	public void setCg_id(String cg_id) {
		this.cg_id = cg_id;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getVALIDITY() {
		return VALIDITY;
	}

	public void setVALIDITY(String vALIDITY) {
		VALIDITY = vALIDITY;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getKEY_KEYWORD() {
		return KEY_KEYWORD;
	}

	public String getKEY_REQ_TYPE() {
		return KEY_REQ_TYPE;
	}

	public String getKEY_REQUEST_TIMESTAMP() {
		return KEY_REQUEST_TIMESTAMP;
	}

	public String getKEY_SERVICE_ID() {
		return KEY_SERVICE_ID;
	}

	public String getKEY_MODE() {
		return KEY_MODE;
	}

	public String getKEY_CONSENT_MODE() {
		return KEY_CONSENT_MODE;
	}

	public String getKEY_CONSENT_STATUS() {
		return KEY_CONSENT_STATUS;
	}

	public String getKEY_REMARKS() {
		return KEY_REMARKS;
	}

	public String getKEY_MSISDN() {
		return KEY_MSISDN;
	}

	public String getKEY_AUTORENEWAL() {
		return KEY_AUTORENEWAL;
	}

	public String getKEY_CG_ID() {
		return KEY_CG_ID;
	}

	public String getKEY_PRICE() {
		return KEY_PRICE;
	}

	public String getKEY_VALIDITY() {
		return KEY_VALIDITY;
	}

	public String getKEY_TID() {
		return KEY_TID;
	}

	public String getKEY_PARAM1() {
		return KEY_PARAM1;
	}

	public String getKEY_PARAM() {
		return KEY_PARAM;
	}

	public String getKEY_SHORT_CODE() {
		return KEY_SHORT_CODE;
	}

	public String getZoneName() {
		return zoneName;
	}

	public void setZoneName(String zoneName) {
		this.zoneName = zoneName;
	}

	public java.util.Date getStvPackStartDate() {
		return stvPackStartDate;
	}

	public void setStvPackStartDate(java.util.Date stvPackStartDate) {
		this.stvPackStartDate = stvPackStartDate;
	}

	
}
