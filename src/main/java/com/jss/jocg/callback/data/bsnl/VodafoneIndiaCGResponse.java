package com.jss.jocg.callback.data.bsnl;

import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class VodafoneIndiaCGResponse {
	public final String KEY_CGTRXID="CG_TrxnId";
	public final String KEY_TRXID="TRXID";
	public final String KEY_MSISDN="MSISDN";
	public final String KEY_CGSTATUS="CGStatus";
	public final String KEY_CGSTATUSCODE="CGStatusCode";
	public final String KEY_CGSTATUSTEXT="CGStatusText";
	//Tue Aug 29 22:14:10 IST 2017|CG_TrxnId=9199996319901504025049649|
	//TRXID=|MSISDN=919999631990|CGStatus=Success|CGStatusCode=CGW202|
	//CGStatusText=SMS/USSD message has been sent to customer to provide second consent
	
	public String cgTransactionId;
	public String transId;
	public Long msisdn;
	public String cgStatus;
	public String cgStatusCode;
	public String cgStatusText;
	public VodafoneIndiaCGResponse(){}
	public VodafoneIndiaCGResponse(String cgTransactionId,String transId,Long msisdn,String cgStatus,
			String cgStatusCode,String cgStatusText){
		this.cgTransactionId=cgTransactionId;
		this.transId=transId;
		this.msisdn=msisdn;
		this.cgStatus=cgStatus;
		this.cgStatusCode=cgStatusCode;
		this.cgStatusText=cgStatusText;
	}
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("VodafoneIndiaCGResponse{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	
	public void addNewField(String key,String value){
		try{
			if(KEY_CGTRXID.equalsIgnoreCase(key)){
				this.setCgTransactionId(value);
			}else if(KEY_TRXID.equalsIgnoreCase(key)){
				this.setTransId(value);
			}else if(KEY_MSISDN.equalsIgnoreCase(key)){
				value=value.trim();
				value=(value.length()<=10)?"91"+value:value;
				this.setMsisdn(JocgString.strToLong(value, 0L));
			}else if(KEY_CGSTATUS.equalsIgnoreCase(key)){
				this.setCgStatus(value);;
			}else if(KEY_CGSTATUSCODE.equalsIgnoreCase(key)){
				this.setCgStatusCode(value);;
			}else if(KEY_CGSTATUSTEXT.equalsIgnoreCase(key)){
				this.setCgStatusText(value);;
			}
		}catch(Exception e){}
	}
	public String getCgTransactionId() {
		return cgTransactionId;
	}
	public void setCgTransactionId(String cgTransactionId) {
		this.cgTransactionId = cgTransactionId;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public Long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}
	public String getCgStatus() {
		return cgStatus;
	}
	public void setCgStatus(String cgStatus) {
		this.cgStatus = cgStatus;
	}
	public String getCgStatusCode() {
		return cgStatusCode;
	}
	public void setCgStatusCode(String cgStatusCode) {
		this.cgStatusCode = cgStatusCode;
	}
	public String getCgStatusText() {
		return cgStatusText;
	}
	public void setCgStatusText(String cgStatusText) {
		this.cgStatusText = cgStatusText;
	}
	
	
}
