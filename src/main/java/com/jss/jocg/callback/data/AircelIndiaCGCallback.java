package com.jss.jocg.callback.data;

import java.lang.reflect.Field;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.jss.jocg.util.JocgString;

public class AircelIndiaCGCallback {
	  Long msisdn;
      String result;
      String reason;
      String seresponseCode;
      String transactionId;
      String cgID;
      public AircelIndiaCGCallback(){}
      public String getCDR(){
           StringBuilder sb=new StringBuilder();
          Field[] fields = this.getClass().getDeclaredFields();
          try {
              for (Field field : fields) {
                  sb.append(field.getName()).append("=").append(field.get(this)).append("|");
              }
          } catch (IllegalArgumentException ex) {
              System.out.println(ex);
          } catch (IllegalAccessException ex) {
              System.out.println(ex);
          }
           return sb.toString();
          
      }
      public boolean parseCGResponse(HttpServletRequest request){
          boolean status=true;
          try{
        	  Enumeration<String> e=request.getParameterNames();
        	  
             while(e.hasMoreElements()){
            	  String newKey=e.nextElement();
                  if(newKey.equalsIgnoreCase("msisdn"))
                      this.setMsisdn(JocgString.strToLong(newKey,0L));
                  else if(newKey.equalsIgnoreCase("result"))
                      this.setResult(JocgString.formatString(request.getParameter(newKey),"NA"));
                  else if(newKey.equalsIgnoreCase("reason")) 
                      this.setReason(JocgString.formatString(request.getParameter(newKey),"NA"));
                  else if(newKey.equalsIgnoreCase("serespcode")) 
                      this.setSeresponseCode(JocgString.formatString(request.getParameter(newKey),"NA"));
                  else if(newKey.equalsIgnoreCase("transid")) 
                      this.setTransactionId(JocgString.formatString(request.getParameter(newKey),"NA"));
                  else if(newKey.equalsIgnoreCase("cgid")) 
                      this.setCgID(JocgString.formatString(request.getParameter(newKey),"NA"));
              }
          }catch(Exception e){
              status=false;
              e.printStackTrace();
          }
          return status;
      }
      public Long getMsisdn() {
          return msisdn;
      }

      public void setMsisdn(Long msisdn) {
          this.msisdn = msisdn;
      }

      public String getResult() {
          return result;
      }

      public void setResult(String result) {
          this.result = result;
      }

      public String getReason() {
          return reason;
      }

      public void setReason(String reason) {
          this.reason = reason;
      }

      public String getSeresponseCode() {
          return seresponseCode;
      }

      public void setSeresponseCode(String seresponseCode) {
          this.seresponseCode = seresponseCode;
      }

      public String getTransactionId() {
          return transactionId;
      }

      public void setTransactionId(String transactionId) {
          this.transactionId = transactionId;
      }

      public String getCgID() {
          return cgID;
      }

      public void setCgID(String cgID) {
          this.cgID = cgID;
      }
}
