package com.jss.jocg.callback.data;

import java.lang.reflect.Field;

public class CommonSDPCallback {

	
	//msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
	
	Long msisdn;
	String sourceOperatorId;
	String operatorCode;
	double price;
	int validity;
	String requestType;
	String transactionId;
	String packageName;
	String param1;
	String param2;
	String param3;
	String param4;
	String subscriberId;
	java.util.Date receiveTime;
	String lifecycleId;
	
	public CommonSDPCallback(){
		
	}
	
	public CommonSDPCallback(Long msisdn,String operatorCode,double price,int validity,String requestType,String transactionId,String packageName,String param1,String param2,String param3,String param4,String subscriberId,java.util.Date receiveTime){
		this.msisdn=msisdn;
		this.operatorCode=operatorCode;
		this.price=price;
		this.validity=validity;
		this.requestType=requestType;
		this.transactionId=transactionId;
		this.packageName=packageName;
		this.param1=param1;
		this.param2=param2;
		this.param3=param3;
		this.param4=param4;
		this.subscriberId=subscriberId;
		this.receiveTime=receiveTime;

	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("ConnectMigratedSDPCallback{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam4() {
		return param4;
	}

	public void setParam4(String param4) {
		this.param4 = param4;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public java.util.Date getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(java.util.Date receiveTime) {
		this.receiveTime = receiveTime;
	}

	public String getLifecycleId() {
		return lifecycleId;
	}

	public void setLifecycleId(String lifecycleId) {
		this.lifecycleId = lifecycleId;
	}

	public String getSourceOperatorId() {
		return sourceOperatorId;
	}

	public void setSourceOperatorId(String sourceOperatorId) {
		this.sourceOperatorId = sourceOperatorId;
	}
	
	
	
}
