package com.jss.jocg.callback.data;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class ConnectSDPCallback implements Serializable{
	
	public final String KEY_MSISDN="msisdn";
	public final String KEY_ACCOUNT_ID="accid";
	public final String KEY_ACTION="action";
	public final String KEY_SERVICE_KEY="servicekey";
	public final String KEY_AMOUNT_CHARGED="charge";
	public final String KEY_STATUS="status";
	public final String KEY_STATUS_DESC="statusdesc";
	public final String KEY_CHARGING_DATE="chargingdate";		
	/*
	 * http://ip:80/jocg/callback/connectsdpcb?msisdn=<msisdn>&accountid=<accountid>
	 * &action=<ACT/DCT/REN>&servicekey=<servicekey>
	 * &amountcharged=<amountcharged>&status=<status>&statusdesc=<statusdesc>
	 * &chargingdate=<yyyy-MM-dd>
	 */

	Long msisdn;
	String accountid;
	String action;
	String serviceId;
	String charge;
	String status="1";
	String statusdesc="SUCCESS";
	String chargingdate;
	
	public ConnectSDPCallback(){
		
	}
	
	public ConnectSDPCallback(Long msisdn, String accountid, String action, String serviceId, String charge,
			String status, String statusdesc, String chargingdate) {
		super();
		this.msisdn = msisdn;
		this.accountid = accountid;
		this.action = action;
		this.serviceId = serviceId;
		this.charge = charge;
		this.status = status;
		this.statusdesc = statusdesc;
		this.chargingdate = chargingdate;
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("ConnectSDPCallback{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	
	public void addNewField(String key,String value){
		try{
			if(KEY_MSISDN.equalsIgnoreCase(key)){
				value=value.trim();
				value=(value.length()<=10)?"91"+value:value;
				this.setMsisdn(JocgString.strToLong(value, 0L));
			}else if(KEY_ACCOUNT_ID.equalsIgnoreCase(key)){
				this.setAccountid(value);
			}else if(KEY_ACTION.equalsIgnoreCase(key)){
				this.setAction(value);
			}else if(KEY_SERVICE_KEY.equalsIgnoreCase(key)){
				this.setServiceId(value);
			}else if(KEY_AMOUNT_CHARGED.equalsIgnoreCase(key)){
				this.setCharge(value);
			}else if(KEY_STATUS.equalsIgnoreCase(key)){
				this.setStatus(value);
			}else if(KEY_STATUS_DESC.equalsIgnoreCase(key)){
				this.setStatusdesc(value);
			}else if(KEY_CHARGING_DATE.equalsIgnoreCase(key)){
				this.setChargingdate(value);
			
			}
			}catch(Exception e){}
	}

	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getAccountid() {
		return accountid;
	}

	public void setAccountid(String accountid) {
		this.accountid = accountid;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusdesc() {
		return statusdesc;
	}

	public void setStatusdesc(String statusdesc) {
		this.statusdesc = statusdesc;
	}

	public String getChargingdate() {
		return chargingdate;
	}

	public void setChargingdate(String chargingdate) {
		this.chargingdate = chargingdate;
	}

	
	
}
