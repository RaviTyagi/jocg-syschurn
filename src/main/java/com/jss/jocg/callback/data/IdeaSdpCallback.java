package com.jss.jocg.callback.data;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.jss.jocg.util.JocgString;

public class IdeaSdpCallback {

String msisdn;
String serviceKey;
String precharge;
String price;
String cpName;
String refrenceId;
String info;
String password;
String originator;
String refId;
String map2Route;
String user;
String serviceId;
String startDate;
String type;
String endDate;
String providerName;
String circle;
String endTime;
String mode;
String startTime;
String msgMode;
String msgType;
String action;
String nmsRetry;
String instance;
String serviceListName;
String retryNumber;
String shortcode;
String status;
SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");
SimpleDateFormat sdfTime=new SimpleDateFormat("HHmmss");

public IdeaSdpCallback(){}

public String getDigiviveCallbackURL(){
//	//http://119.252.216.58:8089/SdplService/HttpPostCollector/ID_ON?User=Digivive&Pass=Ekzsi42h&msisdn=8651269708&srvkey=MYTVD&circle=BH&Type=P&Refid=Digivive_IF000_84113121&mode=WAP&Info=contentfetchmode:PUSH&price=5&status=SUCCESS&action=ACT&precharge=N&startDate=20150813&endDate=20150814&startTime=081620&endTime=081617&orginator=digivive  
	String msisdnTemp=this.msisdn.length()>10?msisdn.substring(msisdn.length()-10):msisdn;
	if(!"910".equalsIgnoreCase(msisdn) && !"0".equalsIgnoreCase(msisdn) && !"91910".equalsIgnoreCase(msisdn)){
		try{
			this.info=this.info.replaceAll(" ", "+");
		}catch(Exception e){}
	String digiviveURL="http://119.252.216.58:8089/SdplService/HttpPostCollector/ID_ON?User="+this.user+"&Pass="+this.password+"&msisdn="+msisdnTemp
			+"&srvkey="+this.serviceKey+"&circle="+this.circle+"&Type="+this.type+"&Refid="+this.refrenceId+"&mode="+this.mode+"&Info="+this.info+"&price="+this.price+"&status="+this.status+"&action="+this.action
			+"&precharge="+this.precharge+"&startDate="+this.startDate+"&endDate="+this.endDate+"&startTime="+this.startTime+"&endTime="+this.endTime+"&orginator="+this.originator;
	return digiviveURL;
	}else 
	return "";
	
}

public String getDate(String dt){
	String retrDt="";
	java.util.Date d=null;
	try{
		d=sdfDate.parse(dt);
		retrDt = dt;
	}catch(Exception e){
		d=new java.util.Date(System.currentTimeMillis());
		retrDt = sdfTime.format(d);
	}
	return retrDt;
}

public String getTime(String tm) {
	String retrTm="";
	java.util.Date d=null;
	try{
		d=sdfTime.parse(tm);
		retrTm = tm;
	}catch(Exception e){
		d=new java.util.Date(System.currentTimeMillis());
		retrTm = sdfTime.format(d);
	}
	return retrTm;
}

public void extractOfflineCDRRecord(String cdrData){
	//map2_route=MAP2_CPGENV2&User=Numero&service_id=9599&startDate=20170920&Type=P&endDate=20170921&
		
		String[] fieldsData=cdrData.split(",");
		StringBuilder sb=new StringBuilder();
		for(String s:fieldsData){
			String paramName=s.substring(0, s.indexOf("="));
			String paramValue=s.substring(s.indexOf("=")+1);
			sb.append(paramName).append("=").append(paramValue).append("&");
			if("action".equalsIgnoreCase(paramName))
				this.setAction(paramValue);
			else if("map2_route".equalsIgnoreCase(paramName))
				this.setMap2Route(paramValue);
			else if("User".equalsIgnoreCase(paramName))
				this.setUser(paramValue);
			else if("service_id".equalsIgnoreCase(paramName))
				this.setServiceId(paramValue);
			else if("startDate".equalsIgnoreCase(paramName))
				this.setStartDate(getDate(paramValue));
			else if("endDate".equalsIgnoreCase(paramName))
				this.setEndDate(getDate(paramValue));
			else if("Type".equalsIgnoreCase(paramName))
				this.setType(paramValue);
			else if("msgmode".equalsIgnoreCase(paramName))
				this.setMsgMode(paramValue);
			else if("msgtype".equalsIgnoreCase(paramName))	
				this.setMsgType(paramValue);
			else if("nsms_retry".equalsIgnoreCase(paramName))
				this.setNmsRetry(paramValue);
			else if("instance".equalsIgnoreCase(paramName))
				this.setInstance(paramValue);
			else if("service_listname".equalsIgnoreCase(paramName))
				this.setServiceListName(paramValue);
			else if("retry_num".equalsIgnoreCase(paramName))
				this.setRetryNumber(paramValue);
			else if("short_code".equalsIgnoreCase(paramName))
				this.setShortcode(paramValue);
			else if("status".equalsIgnoreCase(paramName))
				this.setStatus(paramValue);
			else if("msisdn".equalsIgnoreCase(paramName)){
				if(paramValue.length()<=10 && !"0".equalsIgnoreCase(paramValue)) paramValue="91"+paramValue;
				this.setMsisdn(paramValue);
			}else if("srvkey".equalsIgnoreCase(paramName))
				this.setServiceKey(paramValue);
			else if("precharge".equalsIgnoreCase(paramName))
				this.setPrecharge(paramValue);
			else if("price".equalsIgnoreCase(paramName))
				this.setPrice(paramValue);
			else if("cp_name".equalsIgnoreCase(paramName))
				this.setCpName(paramValue);
			else if("ref_id".equalsIgnoreCase(paramName))
				this.setRefId(paramValue);
			else if("Info".equalsIgnoreCase(paramName))
				this.setInfo(paramValue);
			else if("Pass".equalsIgnoreCase(paramName))
				this.setPassword(paramValue);
			else if("originator".equalsIgnoreCase(paramName))
				this.setOriginator(paramValue);
			else if("Refid".equalsIgnoreCase(paramName))
				this.setRefrenceId(paramValue);
			else if("prov_name".equalsIgnoreCase(paramName))
				this.setProviderName(paramValue);
			else if("circle".equalsIgnoreCase(paramName))
				this.setCircle(paramValue);
			else if("startTime".equalsIgnoreCase(paramName))
				this.setStartTime(getTime(paramValue));
			else if("endTime".equalsIgnoreCase(paramName))
				this.setEndTime(getTime(paramValue));
			else if("mode".equalsIgnoreCase(paramName))
				this.setMode(paramValue);
			
		}
		
	 System.out.println("Idea Offline Callback Params :"+sb.toString());
	    	    
}


public void extractParams(HttpServletRequest request){
	//map2_route=MAP2_CPGENV2&User=Numero&service_id=9599&startDate=20170920&Type=P&endDate=20170921&
		
		//msgmode=CPCallback&msgtype=Internal&action=ACT&nsms_retry=1&instance=ibmse&
		//service_listname=numun015_numero_digital&retry_num=0&short_code=54300&status=SUCCESS&
		//msisdn=918435917150&srvkey=NUMUN015&precharge=N&price=3.00&cp_name=Numero_Digital&ref_id=O2101031945404&
		//Info=contentfetchmode%3APUSH+Without+Time%7Ccontenttype%3AX%7Ccontentdelivtime%3AX%7Ctargetmdn%3AX%7Csongid%3AX%7Cssaction%3AX%7C&
		//Pass=NDI%402015&originator=CLIPTUBE&Refid=O2101031945404
	//prov_name=Numero_Digital_Numerodigital&circle=MP&endTime=111511&mode=WAP&startTime=111511&
		Enumeration<String> e=request.getParameterNames();
		String paramName="",paramValue="";
	StringBuilder sb=new StringBuilder();
	
		while(e.hasMoreElements()){
			paramName=e.nextElement();
			paramValue=request.getParameter(paramName);
			sb.append(paramName).append("=").append(paramValue).append("&");
			if("action".equalsIgnoreCase(paramName))
				this.setAction(paramValue);
			else if("map2_route".equalsIgnoreCase(paramName))
				this.setMap2Route(paramValue);
			else if("User".equalsIgnoreCase(paramName))
				this.setUser(paramValue);
			else if("service_id".equalsIgnoreCase(paramName))
				this.setServiceId(paramValue);
			else if("startDate".equalsIgnoreCase(paramName))
				this.setStartDate(paramValue);
			else if("endDate".equalsIgnoreCase(paramName))
				this.setEndDate(paramValue);
			else if("Type".equalsIgnoreCase(paramName))
				this.setType(paramValue);
			else if("msgmode".equalsIgnoreCase(paramName))
				this.setMsgMode(paramValue);
			else if("msgtype".equalsIgnoreCase(paramName))	
				this.setMsgType(paramValue);
			else if("nsms_retry".equalsIgnoreCase(paramName))
				this.setNmsRetry(paramValue);
			else if("instance".equalsIgnoreCase(paramName))
				this.setInstance(paramValue);
			else if("service_listname".equalsIgnoreCase(paramName))
				this.setServiceListName(paramValue);
			else if("retry_num".equalsIgnoreCase(paramName))
				this.setRetryNumber(paramValue);
			else if("short_code".equalsIgnoreCase(paramName))
				this.setShortcode(paramValue);
			else if("status".equalsIgnoreCase(paramName))
				this.setStatus(paramValue);
			else if("msisdn".equalsIgnoreCase(paramName)){
				if(paramValue.length()<=10) paramValue="91"+paramValue;
				this.setMsisdn(paramValue);
			}else if("srvkey".equalsIgnoreCase(paramName))
				this.setServiceKey(paramValue);
			else if("precharge".equalsIgnoreCase(paramName))
				this.setPrecharge(paramValue);
			else if("price".equalsIgnoreCase(paramName))
				this.setPrice(paramValue);
			else if("cp_name".equalsIgnoreCase(paramName))
				this.setCpName(paramValue);
			else if("ref_id".equalsIgnoreCase(paramName)){
				this.setRefId(paramValue);
				this.setRefrenceId(paramValue);
			}else if("Info".equalsIgnoreCase(paramName))
				this.setInfo(paramValue);
			else if("Pass".equalsIgnoreCase(paramName))
				this.setPassword(paramValue);
			else if("originator".equalsIgnoreCase(paramName))
				this.setOriginator(paramValue);
			else if("prov_name".equalsIgnoreCase(paramName))
				this.setProviderName(paramValue);
			else if("circle".equalsIgnoreCase(paramName))
				this.setCircle(paramValue);
			else if("startTime".equalsIgnoreCase(paramName))
				this.setStartTime(paramValue);
			else if("endTime".equalsIgnoreCase(paramName))
				this.setEndTime(paramValue);
			else if("mode".equalsIgnoreCase(paramName))
				this.setMode(paramValue);
			
			/*else if("Refid".equalsIgnoreCase(paramName))
				this.setRefrenceId(paramValue);*/
			
		}
		String startDateTemp=""+this.startDate+""+this.startTime;
		String endDateTemp=""+this.endDate+""+this.endTime;
		this.startDate=startDateTemp;
		this.endDate=endDateTemp;
		
	  System.out.println("Idea Callback Params :"+sb.toString());
	
	    	    
}

@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder("IdeaIndiaSDPCallback{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( getfieldName(field.getName())).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}

public String getfieldName(String fieldKey){
	return "msisdn".equalsIgnoreCase(fieldKey)?"msisdn":
		"serviceKey".equalsIgnoreCase(fieldKey)?"srvkey":
			"precharge".equalsIgnoreCase(fieldKey)?"precharge":
			"price".equalsIgnoreCase(fieldKey)?"price":
			"cpName".equalsIgnoreCase(fieldKey)?"cp_name":
			"refrenceId".equalsIgnoreCase(fieldKey)?"ref_id":
			"info".equalsIgnoreCase(fieldKey)?"Info":
			"password".equalsIgnoreCase(fieldKey)?"Pass":
			"originator".equalsIgnoreCase(fieldKey)?"originator":
			"refId".equalsIgnoreCase(fieldKey)?"ref_id":
			"map2Route".equalsIgnoreCase(fieldKey)?"map2_route":
			"user".equalsIgnoreCase(fieldKey)?"User":
			"serviceId".equalsIgnoreCase(fieldKey)?"service_id":
			"startDate".equalsIgnoreCase(fieldKey)?"startDate":
			"type".equalsIgnoreCase(fieldKey)?"Type":
			"endDate".equalsIgnoreCase(fieldKey)?"endDate":
			"providerName".equalsIgnoreCase(fieldKey)?"prov_name":
			"circle".equalsIgnoreCase(fieldKey)?"circle":
			"endTime".equalsIgnoreCase(fieldKey)?"endTime":
			"mode".equalsIgnoreCase(fieldKey)?"mode":
			"startTime".equalsIgnoreCase(fieldKey)?"startTime":
			"msgMode".equalsIgnoreCase(fieldKey)?"msgmode":
			"msgType".equalsIgnoreCase(fieldKey)?"msgtype":
			"action".equalsIgnoreCase(fieldKey)?"action":
			"nmsRetry".equalsIgnoreCase(fieldKey)?"nsms_retry":
			"instance".equalsIgnoreCase(fieldKey)?"instance":
			"serviceListName".equalsIgnoreCase(fieldKey)?"service_listname":
			"retryNumber".equalsIgnoreCase(fieldKey)?"retry_num":
			"shortcode".equalsIgnoreCase(fieldKey)?"short_code":
			"status".equalsIgnoreCase(fieldKey)?"status":fieldKey;
}

public String getMsisdn() {
	return msisdn;
}
public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
}
public String getServiceKey() {
	return serviceKey;
}
public void setServiceKey(String serviceKey) {
	this.serviceKey = serviceKey;
}
public String getPrecharge() {
	return precharge;
}
public void setPrecharge(String precharge) {
	this.precharge = precharge;
}
public String getPrice() {
	return price;
}
public void setPrice(String price) {
	this.price = price;
}
public String getCpName() {
	return cpName;
}
public void setCpName(String cpName) {
	this.cpName = cpName;
}
public String getRefrenceId() {
	return refrenceId;
}
public void setRefrenceId(String refrenceId) {
	this.refrenceId = refrenceId;
}
public String getInfo() {
	return info;
}
public void setInfo(String info) {
	this.info = info;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getOriginator() {
	return originator;
}
public void setOriginator(String originator) {
	this.originator = originator;
}
public String getRefId() {
	return refId;
}
public void setRefId(String refId) {
	this.refId = refId;
}
public String getMap2Route() {
	return map2Route;
}
public void setMap2Route(String map2Route) {
	this.map2Route = map2Route;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}
public String getServiceId() {
	return serviceId;
}
public void setServiceId(String serviceId) {
	this.serviceId = serviceId;
}
public String getStartDate() {
	return startDate;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
	
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getEndDate() {
	return endDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
	
}
public String getProviderName() {
	return providerName;
}
public void setProviderName(String providerName) {
	this.providerName = providerName;
}
public String getCircle() {
	return circle;
}
public void setCircle(String circle) {
	this.circle = circle;
}
public String getEndTime() {
	return endTime;
}
public void setEndTime(String endTime) {
	
	this.endTime = endTime;
}
public String getMode() {
	return mode;
}
public void setMode(String mode) {
	this.mode = mode;
}
public String getStartTime() {
	return startTime;
}
public void setStartTime(String startTime) {
	this.startTime = startTime;
	
}
public String getMsgMode() {
	return msgMode;
}
public void setMsgMode(String msgMode) {
	this.msgMode = msgMode;
}
public String getMsgType() {
	return msgType;
}
public void setMsgType(String msgType) {
	this.msgType = msgType;
}
public String getAction() {
	return action;
}
public void setAction(String action) {
	this.action = action;
}
public String getNmsRetry() {
	return nmsRetry;
}
public void setNmsRetry(String nmsRetry) {
	this.nmsRetry = nmsRetry;
}
public String getInstance() {
	return instance;
}
public void setInstance(String instance) {
	this.instance = instance;
}
public String getServiceListName() {
	return serviceListName;
}
public void setServiceListName(String serviceListName) {
	this.serviceListName = serviceListName;
}
public String getRetryNumber() {
	return retryNumber;
}
public void setRetryNumber(String retryNumber) {
	this.retryNumber = retryNumber;
}
public String getShortcode() {
	return shortcode;
}
public void setShortcode(String shortcode) {
	this.shortcode = shortcode;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}



	
}
