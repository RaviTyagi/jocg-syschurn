package com.jss.jocg.callback.data;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;

import org.springframework.format.annotation.DateTimeFormat;

import com.jss.jocg.util.JocgString;

public class AirtelIndiaOfflineDumpRecord {
//TRANSACTION_ID,CUST_MSISDN,CP_ID,PRODUCT_ID,TRANSACTION_START_TIME,TRANS_TYPE,STATUS, ERROR_CODE,ID,AMOUT,BALANCE,VCODE,PARTY_B,EVENT_BASE_ID,CHANNEL_NAME
	String transactionId;
	Long msisdn;
	String cpId;
	String productId;
	java.util.Date transTime;
	String transType;
	String status;
	String errorCode;
	String id;
	double amount;
	double balance;
	String vcode;
	String partyB;
	String eventBaseId;
	String channelName;
	String parsingError;
	String referer;
	
	public AirtelIndiaOfflineDumpRecord(){
		this.referer="OFFLINE-DUMP";
	}
	
public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }

	public boolean parseRecord(String newRecord){
		boolean flag=true;
		try{
			newRecord=newRecord.replaceAll("\"", "");
			String[] strArr=newRecord.split(",");
			if(strArr!=null && strArr.length>=14){
				this.setTransactionId(JocgString.formatString(strArr[0], "NA"));
				String msisdnStr=JocgString.formatString(strArr[1], "0");
				if("0".equalsIgnoreCase(msisdnStr) || msisdnStr.length()<=10) msisdnStr="91"+msisdnStr;
				
				this.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
				System.out.println(msisdnStr+"--"+this.getMsisdn());
				this.setCpId(JocgString.formatString(strArr[2], "NA"));
				this.setProductId(JocgString.formatString(strArr[3], "NA"));
				
				SimpleDateFormat sdfSource=new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
				SimpleDateFormat sdfDest=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				java.util.Date transTime=new java.util.Date(sdfSource.parse(strArr[4]).getTime());
				this.setTransTime(sdfDest.parse(sdfDest.format(transTime)));
				
				this.setTransType(JocgString.formatString(strArr[5], "NA"));
				this.setStatus(JocgString.formatString(strArr[6], "NA"));
				this.setErrorCode(JocgString.formatString(strArr[7], "NA"));
				this.setId(JocgString.formatString(strArr[8], "NA"));
				this.setAmount(JocgString.strToDouble(strArr[9], 0));
				this.setBalance(JocgString.strToDouble(strArr[10], 0L));
				this.setVcode(JocgString.formatString(strArr[11], "NA"));
				this.setPartyB(JocgString.formatString(strArr[12], "NA"));
				this.setEventBaseId((strArr.length>=14)?JocgString.formatString(strArr[13], "NA"):"NA");
				this.setChannelName((strArr.length>=15)?JocgString.formatString(strArr[14], "NA"):"NA");
				
			}else if(strArr==null){
				flag=false;
				parsingError="Unparseable Record, Null Array returned after parsing.";
			}else{
				flag=false;
				parsingError="Unparseable Record, Found fields "+strArr.length+" out of 15";
			}
			
		}catch(Exception e){
			flag=false;
			this.parsingError=""+e.getMessage();
		}
		return flag;
	}
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}
	public String getCpId() {
		return cpId;
	}
	public void setCpId(String cpId) {
		this.cpId = cpId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public java.util.Date getTransTime() {
		return transTime;
	}
	public void setTransTime(java.util.Date transTime) {
		this.transTime = transTime;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getPartyB() {
		return partyB;
	}
	public void setPartyB(String partyB) {
		this.partyB = partyB;
	}
	public String getEventBaseId() {
		return eventBaseId;
	}
	public void setEventBaseId(String eventBaseId) {
		this.eventBaseId = eventBaseId;
	}
	public String getChannelName() {
		return channelName;
	}
	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
	public String getParsingError() {
		return parsingError;
	}
	public void setParsingError(String parsingError) {
		this.parsingError = parsingError;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}
	
}
