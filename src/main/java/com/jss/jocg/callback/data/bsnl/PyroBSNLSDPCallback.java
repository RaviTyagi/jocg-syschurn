package com.jss.jocg.callback.data.bsnl;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class PyroBSNLSDPCallback implements Serializable{
	
	public final String KEY_ORGINATOR="orginator";
	public final String KEY_CONST_SOURCE="consent_source";
	public final String KEY_ACTION="action";
	public final String KEY_SUBSCRIPTION_ID="subscription_id";
	public final String KEY_SERVICE_ID="service_id";
	public final String KEY_CONST_STATUS="consent_status";
	public final String KEY_BILL_STATUS="billing_status";
	public final String KEY_ERROR_CODE="errorcode";
	public final String KEY_DESCP="description";
	public final String KEY_MSISDN="msisdn";
	public final String KEY_DATE="DATE";
	public final String KEY_NEXT_RENEWAL_DATE="NEXT_RENEWAL_DATE";
	public final String KEY_CHARGE_AMOUNT="CHARGE";
	public final String KEY_VALIDITY="VALIDITY";
	
	
	public final String KEY_TID="tid";	
	public final String KEY_UF1="user_field_1";
	//tid=1040140188&orginator=cp&consent_source=WAP&action=new_subscription&subscription_id=MONFA000&
	//msisdn=911122331234&service_id=104000020077776&consent_status=0&billing_status=0&errorcode=0&
	//description=SUCCESS&DATE=2014-09-04%2011:59:46&NEXT_RENEWAL_DATE=2014-09-0712:00:05&
	//NEXT_RENEWAL_DATE=6/10/2015:24:54PM&CHARGE=35&VALIDITY=10

	
	String orginator;
	String consentSource;
	Long msisdn;
	String action;
	String subscriptionId;
	String serviceId;
	String consentStatus;
	String billingStatus;
	String errorCode;
	String description;
	String DATE;
	String NEXT_RENEWAL_DATE;
	String ACTUAL_RENEWAL_DATE;
	String CHARGE;
	String VALIDITY;
	String tid;	
	String endTime;
	
	String ppid;
	
	public PyroBSNLSDPCallback(){
		
	}
	
	public String getDigiviveCallbackURL(){		 
		String msisdnTemp=Long.toString(this.msisdn).length()>10?Long.toString(this.msisdn).substring(Long.toString(this.msisdn).length()-10):Long.toString(this.msisdn);
		if(!"910".equalsIgnoreCase(Long.toString(this.msisdn)) && !"0".equalsIgnoreCase(Long.toString(this.msisdn)) && !"91910".equalsIgnoreCase(Long.toString(this.msisdn))){
			try{
				this.NEXT_RENEWAL_DATE=this.NEXT_RENEWAL_DATE.replaceAll(" ", "+");
				this.ACTUAL_RENEWAL_DATE=this.ACTUAL_RENEWAL_DATE.replaceAll(" ", "+");
			}catch(Exception e){}
		String digiviveURL="http://172.31.19.183:8093/SdplService/HttpPostCollector/BN_OF?tid="+this.tid+"&"
				+ "orginator="+this.orginator+"&consent_source="+this.consentSource+"&action="+this.action+"&subscription_id="+this.subscriptionId+"&"
				+ "msisdn="+this.msisdn+"&service_id="+this.serviceId+"&consent_status="+this.consentStatus+"&billing_status="+this.billingStatus+"&errorcode="+this.errorCode+"&"
				+ "description="+this.description+"&cp_id=&source=null&language=null&service_name=null&price=null&VALIDITY="+this.VALIDITY+"&"
				+ "validity_type=null&USER_FIELD1=null&USER_FIELD2=null&USER_FIELD3=null&USER_FIELD4=null&CHARGE="+this.CHARGE+"&"
				+ "DATE=null&ACTUAL_RENEWAL_DATE="+this.ACTUAL_RENEWAL_DATE+"&NEXT_RENEWAL_DATE="+this.NEXT_RENEWAL_DATE;
		
		return digiviveURL;
		}else 
		return "";
		
	}
	
	
	public PyroBSNLSDPCallback(String orginator,String consentSource,Long msisdn,String action,String subscriptionId,String serviceId,String consentStatus,String billingStatus,String errorCode,
	String description,String DATE,String NEXT_RENEWAL_DATE,String CHARGE,String VALIDITY,String tid,String endTime){

		
		
	this.orginator=orginator;
	this.consentSource=consentSource;
	this.msisdn=msisdn;
	this.action=action;
	this.subscriptionId=subscriptionId;
	this.serviceId=serviceId;
	this.consentStatus=consentStatus;
	this.billingStatus=billingStatus;
	this.errorCode=errorCode;
	this.description=description;
	this.DATE=DATE;
	this.NEXT_RENEWAL_DATE=NEXT_RENEWAL_DATE;
	this.ACTUAL_RENEWAL_DATE=ACTUAL_RENEWAL_DATE;
	this.CHARGE=CHARGE;
	this.VALIDITY=VALIDITY;
	this.tid=tid;
	this.endTime=endTime;	
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("PyroBSNLSDPCallback{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	
	public void addNewField(String key,String value){
		try{
			if(KEY_ORGINATOR.equalsIgnoreCase(key)){
				this.setOrginator((value));
			}else if(KEY_CONST_SOURCE.equalsIgnoreCase(key)){
				this.setConsentSource(value);
			}else if(KEY_MSISDN.equalsIgnoreCase(key)){
				value=value.trim();
				value=(value.length()<=10)?"91"+value:value;
				this.setMsisdn(JocgString.strToLong(value, 0L));
			}else if(KEY_SUBSCRIPTION_ID.equalsIgnoreCase(key)){
				this.setSubscriptionId(value);
			}else if(KEY_SERVICE_ID.equalsIgnoreCase(key)){
				this.setServiceId(value);
			}else if(KEY_CONST_STATUS.equalsIgnoreCase(key)){
				this.setConsentStatus(value);
			}else if(KEY_BILL_STATUS.equalsIgnoreCase(key)){
				this.setBillingStatus(value);
			}else if(KEY_ERROR_CODE.equalsIgnoreCase(key)){
				this.setErrorCode(value);
			}else if(KEY_DESCP.equalsIgnoreCase(key)){
				this.setDescription(value);
			}else if(KEY_DATE.equalsIgnoreCase(key)){
				this.setDATE(value);
			}else if(KEY_NEXT_RENEWAL_DATE.equalsIgnoreCase(key)){
				this.setNEXT_RENEWAL_DATE(value);
			}else if(KEY_ACTION.equalsIgnoreCase(key)){
				this.setAction(value);
			}else if(KEY_CHARGE_AMOUNT.equalsIgnoreCase(key)){
				this.setCHARGE(value);
			}else if(KEY_VALIDITY.equalsIgnoreCase(key)){
				this.setVALIDITY(value);
			}else if(KEY_TID.equalsIgnoreCase(key)){
				this.setTid(value);
			}
			}catch(Exception e){}
	}

	public String getOrginator() {
		return orginator;
	}

	public void setOrginator(String orginator) {
		this.orginator = orginator;
	}

	public String getConsentSource() {
		return consentSource;
	}

	public void setConsentSource(String consentSource) {
		this.consentSource = consentSource;
	}

	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getConsentStatus() {
		return consentStatus;
	}

	public String getPpid() {
		return ppid;
	}

	public void setPpid(String ppid) {
		this.ppid = ppid;
	}

	public void setConsentStatus(String consentStatus) {
		this.consentStatus = consentStatus;
	}

	public String getBillingStatus() {
		return billingStatus;
	}

	public void setBillingStatus(String billingStatus) {
		this.billingStatus = billingStatus;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDATE() {
		return DATE;
	}

	public void setDATE(String dATE) {
		DATE = dATE;
	}

	public String getNEXT_RENEWAL_DATE() {
		return NEXT_RENEWAL_DATE;
	}

	public void setNEXT_RENEWAL_DATE(String nEXT_RENEWAL_DATE) {
		NEXT_RENEWAL_DATE = nEXT_RENEWAL_DATE;
	}
	
	public String getACTUAL_RENEWAL_DATE() {
		return ACTUAL_RENEWAL_DATE;
	}

	public void setACTUAL_RENEWAL_DATE(String aCTUAL_RENEWAL_DATE) {
		ACTUAL_RENEWAL_DATE = aCTUAL_RENEWAL_DATE;
	}

	public String getCHARGE() {
		return CHARGE;
	}

	public void setCHARGE(String cHARGE) {
		CHARGE = cHARGE;
	}

	public String getVALIDITY() {
		return VALIDITY;
	}

	public void setVALIDITY(String vALIDITY) {
		VALIDITY = vALIDITY;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getKEY_ORGINATOR() {
		return KEY_ORGINATOR;
	}

	public String getKEY_CONST_SOURCE() {
		return KEY_CONST_SOURCE;
	}

	public String getKEY_ACTION() {
		return KEY_ACTION;
	}

	public String getKEY_SUBSCRIPTION_ID() {
		return KEY_SUBSCRIPTION_ID;
	}

	public String getKEY_SERVICE_ID() {
		return KEY_SERVICE_ID;
	}

	public String getKEY_CONST_STATUS() {
		return KEY_CONST_STATUS;
	}

	public String getKEY_BILL_STATUS() {
		return KEY_BILL_STATUS;
	}

	public String getKEY_ERROR_CODE() {
		return KEY_ERROR_CODE;
	}

	public String getKEY_DESCP() {
		return KEY_DESCP;
	}

	public String getKEY_MSISDN() {
		return KEY_MSISDN;
	}

	public String getKEY_DATE() {
		return KEY_DATE;
	}

	public String getKEY_NEXT_RENEWAL_DATE() {
		return KEY_NEXT_RENEWAL_DATE;
	}

	public String getKEY_CHARGE_AMOUNT() {
		return KEY_CHARGE_AMOUNT;
	}

	public String getKEY_VALIDITY() {
		return KEY_VALIDITY;
	}

	public String getKEY_TID() {
		return KEY_TID;
	}

	public String getKEY_UF1() {
		return KEY_UF1;
	}
		
	
}
