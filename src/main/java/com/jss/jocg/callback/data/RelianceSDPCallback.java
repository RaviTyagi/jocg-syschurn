package com.jss.jocg.callback.data;

import java.lang.reflect.Field;

public class RelianceSDPCallback {
//action=1&appid=198&channelid=20&mdn=1234509877&pname=RCOM-RW&startdate=110220151500&
	//expirydate=120220151500&status=0&validdays=1 
		int action;
		int appId;
		int channelId;
		String msisdn;
		String productName;
		java.util.Date startDate;
		java.util.Date expiryDate;
		int status;
		int validity;
	public RelianceSDPCallback(){
		
	}
	
	public RelianceSDPCallback(int action, int appId, int channelId, String msisdn, String productName, java.util.Date startDate, java.util.Date expiryDate, int status, int validity){
		this.action=action;
		this.appId=appId;
		this.channelId=channelId;
		this.msisdn=msisdn;
		this.productName=productName;
		this.startDate=startDate;
		this.expiryDate=expiryDate;
		this.status=status;
		this.validity=validity;
	
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("RelianceSDPCallback{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append(field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public java.util.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public java.util.Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(java.util.Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}
	
	
	
}
