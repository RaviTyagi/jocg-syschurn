package com.jss.jocg.callback.data.voda;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class VodafoneIndiaSDPCallback implements Serializable{
	//Santosh Callback
	//Tue Aug 29 22:06:08 IST 2017|User=VOD_ON|Pass=VOD_ON|msisdn=9582700318|srvkey=NXGTV_FAVSHOWW|circle=0|
		//Type=U|Refid=25081114|mode=WEEKLY|Info=WAP_D2C|price=35|status=SUCCESS|action=ACT|precharge=Y|
		//startDate=8/29/2017|endDate=8/29/2017|startTime=|endTime=|orginator=12607830995|
		//user_field_1=POSTPAID|
	
	//Vodafone Callback for DCT
	//MSISDN=919769311037&SERVICE_ID=NEXGTV_IPLPRE_W&CLASS=IPLPRE_W&TXNID=49663622483&ACTION=DCT
	//&STATUS=SUCCESS&CHARGING_MODE=SUSPEND&REQUEST_ID=PRISM832565349&request_mode=SM&CIRCLE_ID=2&SUB_TYPE=PREPAID
	
	public final String KEY_USER="User";
	public final String KEY_PASS="Pass";
	public final String KEY_MSISDN="msisdn";
	public final String KEY_SRVKEY="srvkey";
	public final String KEY_SRVKEYALT="serviceKey";
	public final String KEY_SRVKEYALT_VODA="SERVICE_ID";
	public final String KEY_CLASS="CLASS";
	
	public final String KEY_CIRCLE="circle";
	public final String KEY_CIRCLE_VODA="CIRCLE_ID";
	
	
	public final String KEY_TYPE="Type";
	public final String KEY_REFID="Refid";
	public final String KEY_REFID_VODA="REQUEST_ID";
	
	public final String KEY_TXNID_VODA="TXNID";
	
	public final String KEY_MODE="mode";
	public final String KEY_MODE_VODA="CHARGING_MODE";
	
	public final String KEY_INFO="Info";
	public final String KEY_PRICE="price";
	public final String KEY_STATUS="status";
	public final String KEY_ACTION="action";
	public final String KEY_PRECHARGE="precharge";
	public final String KEY_STARTDATE="startDate";
	public final String KEY_ENDDATE="endDate";
	public final String KEY_STARTTIME="startTime";
	public final String KEY_ENDTIME="endTime";
	public final String KEY_ORIGINATOR="orginator";
	
	public final String KEY_UF1="user_field_1";
	public final String KEY_SUB_TYPE="SUB_TYPE";
	

	
	String user;
	String pass;
	Long msisdn;
	String serviceKey;
	String classValue;
	Integer circleId;
	String type;
	String referenceId;
	String mode;
	String info;
	Double price=0D;
	String status;
	String action;
	String precharge;
	String startDate;
	String endDate;
	String startTime;
	String endTime;
	String originator;
	String userField1;
	String txnId;
	
	public VodafoneIndiaSDPCallback(){
		
	}
	
	public VodafoneIndiaSDPCallback(String user,String pass,Long msisdn,String serviceKey,Integer circleId,String type,String referenceId,String mode,String info,
	Double price,String status,String action,String precharge,String startDate,String endDate,String startTime,String endTime,String
	originator,String userField1){

	this.user=user;
	this.pass=pass;
	this.msisdn=msisdn;
	this.serviceKey=serviceKey;
	this.circleId=circleId;
	this.type=type;
	this.referenceId=referenceId;
	this.mode=mode;
	this.info=info;
	this.price=price;
	this.status=status;
	this.action=action;
	this.precharge=precharge;
	this.startDate=startDate;
	this.endDate=endDate;
	this.startTime=startTime;
	this.endTime=endTime;
	this.originator=originator;
	this.userField1=userField1;
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("VodafoneIndiaSDPCallback{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	
	public void addNewField(String key,String value){
		try{
			//MSISDN=919769311037&SERVICE_ID=NEXGTV_IPLPRE_W&CLASS=IPLPRE_W&TXNID=49663622483&ACTION=DCT
			//&STATUS=SUCCESS&CHARGING_MODE=SUSPEND&REQUEST_ID=PRISM832565349&request_mode=SM&CIRCLE_ID=2&SUB_TYPE=PREPAID
			
			if(KEY_USER.equalsIgnoreCase(key)){
				this.setUser(value);
			}else if(KEY_PASS.equalsIgnoreCase(key)){
				this.setPass(value);
			}else if(KEY_MSISDN.equalsIgnoreCase(key)){
				value=value.trim();
				value=(value.length()<=10)?"91"+value:value;
				this.setMsisdn(JocgString.strToLong(value, 0L));
			}else if(KEY_SRVKEY.equalsIgnoreCase(key) || KEY_SRVKEYALT.equalsIgnoreCase(key) || KEY_SRVKEYALT_VODA.equalsIgnoreCase(key)){
				this.setServiceKey(value);
			}else if(KEY_CLASS.equalsIgnoreCase(key)){
					this.setClassValue(value);
			}else if(KEY_CIRCLE.equalsIgnoreCase(key) || "CIRCLE_ID".equalsIgnoreCase(key)){
				this.setCircleId(JocgString.strToInt(value, 0));
			}else if(KEY_TYPE.equalsIgnoreCase(key)){
				this.setType(value);
			}else if(KEY_REFID.equalsIgnoreCase(key) || KEY_REFID_VODA.equalsIgnoreCase(key)){
				this.setReferenceId(value);
			}else if(KEY_MODE.equalsIgnoreCase(key) || KEY_MODE_VODA.equalsIgnoreCase(key)){
				this.setMode(value);
			}else if(KEY_INFO.equalsIgnoreCase(key)){
				this.setInfo(value);
			}else if(KEY_PRICE.equalsIgnoreCase(key)){
				this.setPrice(JocgString.strToDouble(value, 0D));
			}else if(KEY_STATUS.equalsIgnoreCase(key)){
				this.setStatus(value);
			}else if(KEY_ACTION.equalsIgnoreCase(key)){
				this.setAction(value);
			}else if(KEY_PRECHARGE.equalsIgnoreCase(key)){
				this.setPrecharge(value);
			}else if(KEY_STARTDATE.equalsIgnoreCase(key)){
				this.setStartDate(value);
			}else if(KEY_ENDDATE.equalsIgnoreCase(key)){
				this.setEndDate(value);
			}else if(KEY_STARTTIME.equalsIgnoreCase(key)){
				this.setStartTime(value);
			}else if(KEY_ENDTIME.equalsIgnoreCase(key)){
				this.setEndTime(value);
			}else if(KEY_ORIGINATOR.equalsIgnoreCase(key) || "request_mode".equalsIgnoreCase(key)){
				this.setOriginator(value);
			}else if(KEY_UF1.equalsIgnoreCase(key) || "SUB_TYPE".equalsIgnoreCase(key)){
				this.setUserField1(value);
			}else if(KEY_TXNID_VODA.equalsIgnoreCase(key)){
				this.setTxnId(value);
				
			}
		}catch(Exception e){}
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}
	public String getServiceKey() {
		return serviceKey;
	}
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	public Integer getCircleId() {
		return circleId;
	}
	public void setCircleId(Integer circleId) {
		this.circleId = circleId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getPrecharge() {
		return precharge;
	}
	public void setPrecharge(String precharge) {
		this.precharge = precharge;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getOriginator() {
		return originator;
	}
	public void setOriginator(String originator) {
		this.originator = originator;
	}
	public String getUserField1() {
		return userField1;
	}
	public void setUserField1(String userField1) {
		this.userField1 = userField1;
	}
	public String getKEY_USER() {
		return KEY_USER;
	}
	public String getKEY_PASS() {
		return KEY_PASS;
	}
	public String getKEY_MSISDN() {
		return KEY_MSISDN;
	}
	public String getKEY_SRVKEY() {
		return KEY_SRVKEY;
	}
	public String getKEY_CIRCLE() {
		return KEY_CIRCLE;
	}
	public String getKEY_TYPE() {
		return KEY_TYPE;
	}
	public String getKEY_REFID() {
		return KEY_REFID;
	}
	public String getKEY_MODE() {
		return KEY_MODE;
	}
	public String getKEY_INFO() {
		return KEY_INFO;
	}
	public String getKEY_PRICE() {
		return KEY_PRICE;
	}
	public String getKEY_STATUS() {
		return KEY_STATUS;
	}
	public String getKEY_ACTION() {
		return KEY_ACTION;
	}
	public String getKEY_PRECHARGE() {
		return KEY_PRECHARGE;
	}
	public String getKEY_STARTDATE() {
		return KEY_STARTDATE;
	}
	public String getKEY_ENDDATE() {
		return KEY_ENDDATE;
	}
	public String getKEY_STARTTIME() {
		return KEY_STARTTIME;
	}
	public String getKEY_ENDTIME() {
		return KEY_ENDTIME;
	}
	public String getKEY_ORIGINATOR() {
		return KEY_ORIGINATOR;
	}
	public String getKEY_UF1() {
		return KEY_UF1;
	}

	public String getTxnId() {
		return txnId;
	}

	public void setTxnId(String txnId) {
		this.txnId = txnId;
	}

	public String getClassValue() {
		return classValue;
	}

	public void setClassValue(String classValue) {
		this.classValue = classValue;
	}
	
	
}
