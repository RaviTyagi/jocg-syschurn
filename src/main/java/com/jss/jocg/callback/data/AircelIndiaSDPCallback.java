package com.jss.jocg.callback.data;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import com.jss.jocg.util.JocgString;

public class AircelIndiaSDPCallback {
	 /**
     * Params: ProductName=<ProductName>&ServiceName=<ServiceName>&SID=<MSISDN>&NotifyType=<Type>&AmountCharged=<Amountvalue>&SubscriptionValidity=<4>&ChannelType=<Type>&Reason=<Reason>
    
    * List of NotifyType:
    SUB-1 (subscription)
    SUB-2 (grace)
    SUB-3 (suspended)
    RENEW-1 (renewal)
    RENEW-2 (renewal grace)
    RENEW-3 (renewal suspended)
    UNSUB-1 (unsubscription)

     */
    Long msisdn;
    String cgTransId;
    String serviceName;
    String notifyType;
    double amountCharged;
    String productName;
    String channelType;
    String reason;
    int validityPeriod;
    String validityType;
    public AircelIndiaSDPCallback(){
        
    }
    
    @Override
   	public String toString() {
   			
   	        Field[] fields = this.getClass().getDeclaredFields();
   	        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
   	        try {
   	            for (Field field : fields) {
   	                str.append( field.getName()).append("=").append(field.get(this)).append(",");
   	            }
   	        } catch (IllegalArgumentException ex) {
   	            System.out.println(ex);
   	        } catch (IllegalAccessException ex) {
   	            System.out.println(ex);
   	        }
   	        str.append("}");
   	        return str.toString();
   	    }
    
    public boolean parseSDPResponse(HttpServletRequest request){
        boolean status=true;
        try{
        	Enumeration<String> e=request.getParameterNames();
            while(e.hasMoreElements()){
            	String newKey=e.nextElement();
            	if(newKey.equalsIgnoreCase("SID")){
                    //this.setMsisdn(JocgString.strToLong(newKey,0L));
                	this.setMsisdn(JocgString.strToLong(request.getParameter(newKey),0L));
                }
                else if(newKey.equalsIgnoreCase("Consent_id"))
                    this.setCgTransId(JocgString.formatString(request.getParameter(newKey),"NA"));
                else if(newKey.equalsIgnoreCase("NotifyType")) 
                    this.setNotifyType(JocgString.formatString(request.getParameter(newKey),"NA"));
                else if(newKey.equalsIgnoreCase("ServiceName")) 
                    this.setServiceName(JocgString.formatString(request.getParameter(newKey),"NA"));
                else if(newKey.equalsIgnoreCase("AmountCharged")) 
                    this.setAmountCharged(JocgString.strToDouble(request.getParameter(newKey),0D));
                else if(newKey.equalsIgnoreCase("ProductName")) 
                    this.setProductName(JocgString.formatString(request.getParameter(newKey),"NA"));
                 else if(newKey.equalsIgnoreCase("ChannelType")) 
                    this.setChannelType(JocgString.formatString(request.getParameter(newKey),"NA"));
                 else if(newKey.equalsIgnoreCase("Reason")) 
                    this.setReason(JocgString.formatString(request.getParameter(newKey),"NA"));
                else if(newKey.equalsIgnoreCase("SubscriptionValidity")){
                    String validityStr=JocgString.formatString(request.getParameter(newKey),"NA");
                    if(!"NA".equalsIgnoreCase(validityStr)){
                        //Parse Validity Part
                        if (validityStr.contains("days")) {
                            String[] validityDays = validityStr.split("days");
                            this.setValidityPeriod(JocgString.strToInt(validityDays[0], 0));
                        }else this.setValidityPeriod(JocgString.strToInt(validityStr, 0));
                        this.setValidityType("DAYS");
                    }
                }
            }
        }catch(Exception e){
            status=false;
           e.printStackTrace();
        }
        return status;
    }
    public Long getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(Long msisdn) {
        this.msisdn = msisdn;
    }

    public String getCgTransId() {
        return cgTransId;
    }

    public void setCgTransId(String cgTransId) {
        this.cgTransId = cgTransId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    public double getAmountCharged() {
        return amountCharged;
    }

    public void setAmountCharged(double amountCharged) {
        this.amountCharged = amountCharged;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(int validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public String getValidityType() {
        return validityType;
    }

    public void setValidityType(String validityType) {
        this.validityType = validityType;
    }
}
