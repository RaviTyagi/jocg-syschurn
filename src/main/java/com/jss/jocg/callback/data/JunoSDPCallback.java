package com.jss.jocg.callback.data;

import java.lang.reflect.Field;

public class JunoSDPCallback {

	String jocgTransId;//cptxnid
	String junoProductId;//pid
	String cgTransId;//cgtxnid
	String operatorTransId;//optxnid
	String msisdn;//m
	double price;//pr
	String statusCode;//status
	String circleName;//circle_name
	String action;//action
	String chargingMode;//charging_mode
	String operatorCode;//operator_id
	String networkType;//nettype
	String junoTransId;//jtxnid
	String actionRubyOnRails;//jaction
	java.util.Date receiveDate;
	String subscriberId;

	public JunoSDPCallback(String cptxnid,String pid,String cgtxnid,String optxnid,String m,double pr,String status,String circle_name,String action,String charging_mode,String operator_id,String nettype,String jtxnid,String jaction,java.util.Date receiveDate){
		this.jocgTransId=cptxnid;
		this.junoProductId=pid;
		this.cgTransId=cgtxnid;
		this.operatorTransId=optxnid;
		this.msisdn=m;
		this.price=pr;
		this.statusCode=status;
		this.circleName=circle_name;
		this.action=action;
		this.chargingMode=charging_mode;
		this.operatorCode=operator_id;
		this.networkType=nettype;
		this.junoTransId=jtxnid;
		this.actionRubyOnRails=jaction;
		this.receiveDate=receiveDate;

	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("JunoSDPCallback{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append(field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	public String getJocgTransId() {
		return jocgTransId;
	}

	public void setJocgTransId(String jocgTransId) {
		this.jocgTransId = jocgTransId;
	}

	public String getJunoProductId() {
		return junoProductId;
	}

	public void setJunoProductId(String junoProductId) {
		this.junoProductId = junoProductId;
	}

	public String getCgTransId() {
		return cgTransId;
	}

	public void setCgTransId(String cgTransId) {
		this.cgTransId = cgTransId;
	}

	public String getOperatorTransId() {
		return operatorTransId;
	}

	public void setOperatorTransId(String operatorTransId) {
		this.operatorTransId = operatorTransId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getChargingMode() {
		return chargingMode;
	}

	public void setChargingMode(String chargingMode) {
		this.chargingMode = chargingMode;
	}

	public String getOperatorCode() {
		return operatorCode;
	}

	public void setOperatorCode(String operatorCode) {
		this.operatorCode = operatorCode;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getJunoTransId() {
		return junoTransId;
	}

	public void setJunoTransId(String junoTransId) {
		this.junoTransId = junoTransId;
	}

	public String getActionRubyOnRails() {
		return actionRubyOnRails;
	}

	public void setActionRubyOnRails(String actionRubyOnRails) {
		this.actionRubyOnRails = actionRubyOnRails;
	}

	public java.util.Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(java.util.Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	
	
	
}
