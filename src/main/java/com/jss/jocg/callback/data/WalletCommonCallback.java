package com.jss.jocg.callback.data;

import java.lang.reflect.Field;

public class WalletCommonCallback {

	//transid=<transactionId>&msisdn=<msisdn>&subscriberid=<subscriberid>&
	//servicename=<servicename>&systemid=<systemid>&opcode=<opcode>&packname=<packname>
	//&packvalidity=<packvalidity>&discounted=<true/fale>&baseprice=<baseprice>&
	//discount_amt=<discount_amt>&transtime=<yyyyMMddHHmmss>
	String transId;
	String msisdn;
	String subscriberId;
	String serviceName;
	String systemId;
	String sourceOperatorCode;
	String packageName;
	int packValidity;
	boolean discountGiven;
	double basePrice;
	double discount;
	java.util.Date transactionTime;
	String lifecycleId;
	String vendorName;
	
	public WalletCommonCallback(){
		
	}
	public WalletCommonCallback(String transId,String msisdn,String subscriberId,String serviceName,String systemId,String sourceOperatorCode,String packageName,int packValidity,boolean discountGiven,double basePrice,double discount,java.util.Date transactionTime){
		this.transId=transId;
		this.msisdn=msisdn;
		this.subscriberId=subscriberId;
		this.serviceName=serviceName;
		this.systemId=systemId;
		this.sourceOperatorCode=sourceOperatorCode;
		this.packageName=packageName;
		this.packValidity=packValidity;
		this.discountGiven=discountGiven;
		this.basePrice=basePrice;
		this.discount=discount;
		this.transactionTime=transactionTime;

	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("WalletCommonCallback{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append(field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getSourceOperatorCode() {
		return sourceOperatorCode;
	}
	public void setSourceOperatorCode(String sourceOperatorCode) {
		this.sourceOperatorCode = sourceOperatorCode;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public int getPackValidity() {
		return packValidity;
	}
	public void setPackValidity(int packValidity) {
		this.packValidity = packValidity;
	}
	public boolean isDiscountGiven() {
		return discountGiven;
	}
	public void setDiscountGiven(boolean discountGiven) {
		this.discountGiven = discountGiven;
	}
	public double getBasePrice() {
		return basePrice;
	}
	public void setBasePrice(double basePrice) {
		this.basePrice = basePrice;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
	public java.util.Date getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(java.util.Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	public String getLifecycleId() {
		return lifecycleId;
	}
	public void setLifecycleId(String lifecycleId) {
		this.lifecycleId = lifecycleId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	
	
}
