package com.jss.jocg.util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import org.apache.commons.codec.binary.Base64;

import org.springframework.beans.factory.annotation.Value;

public class JocgString {
	
	public static String convertToHex(byte[] bytes)
	  {
	    StringBuilder buffer = new StringBuilder(bytes.length * 2);
	    
	    byte[] arrayOfByte = bytes;
	    int j = bytes.length; 
	    for (int i = 0; i < j; i++) { 
	    	 Byte b = Byte.valueOf(arrayOfByte[i]);
	    	 String str = Integer.toHexString(b.byteValue());
	    	 int len = str.length();
		      if (len == 8){
		        buffer.append(str.substring(6));
		      }else if (str.length() == 2) {
		        buffer.append(str);
		      } else {
		        buffer.append("0" + str);
		      }
	    }
	    return buffer.toString();
	  }
	
	
	public static String getBase64DecodedString(String str){
        byte[] byteArray = Base64.decodeBase64(str.getBytes());
        String decodedString = new String(byteArray);
        return decodedString;
        
    }
    public static String getBase64EncodedString(String str){
        byte[] byteArray = Base64.encodeBase64(str.getBytes());
        String encodedString = new String(byteArray);
        return encodedString;
        
    }
    
    public static String getMD5EncodedString(String str){
        String respStr="";
        try{
        MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(str.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}
          respStr=sb.toString();
        }catch(Exception e){
        }
        return respStr;
    }
    
	public static Long generateChecksum(String str,String key){
		Long checksum=new Long(0);
		try{
			str=str+key;
			byte[] byteArr=str.getBytes();
	        Checksum checksumObj = new CRC32();
	        checksumObj.update(byteArr, 0, byteArr.length);
	        checksum=checksumObj.getValue(); 
	    }catch(Exception e){}
		return checksum;
	}
	public static String parseXmlField(String str,String fieldName){
		String fieldVal="";
		try{
			String fieldNameStart="<"+fieldName+">";
			String fieldNameEnd="</"+fieldName+">";
			String str1=str.substring(str.indexOf(fieldNameStart));
			str1=str1.substring(fieldNameStart.length()+1);
			fieldVal=str1.substring(0, str1.indexOf(fieldNameEnd));
		}catch(Exception e){  fieldVal="NA";}
		return fieldVal;
	}
	
	
	
	public static int deleteAllFiles(String source){
        int fileCount=0;
        try{
            File f=new File(source);
            File[] filesList=null;
            if(f.exists() && f.isDirectory()){
                filesList=f.listFiles();
            }else if(f.exists()){
                fileCount +=f.delete()?1:0;
            }
            f=null;
            for(File f1:filesList){
                fileCount +=f1.delete()?1:0;
                f1=null;
            }
            filesList=null;
        }catch(Exception e){
            System.out.println("RUtil.class :: moveFolder() :: "+e);
        }
        return fileCount;
    }
    
    public static int moveFolder(String source,String dest){
        int fileCount=0;
        try{
            File f=new File(dest);
            if(!f.exists()) f.mkdirs();
            f=null;
            
            f=new File(source);
            File[] filesList=null;
            if(f.exists() && f.isDirectory()){
                filesList=f.listFiles();
            }
            f=null;
            
            for(File f1:filesList){
                System.out.println("Moving file "+f1.getCanonicalPath()+" to "+dest+f1.getName());
                fileCount +=f1.renameTo(new File(dest+f1.getName()))?1:0;
                f1=null;
            }
            filesList=null;
        }catch(Exception e){
            System.out.println("RUtil.class :: moveFolder() :: "+e);
        }
        return fileCount;
    }
    
    public static boolean moveFile(String sourceFilePath, String destFilePath){
        boolean flag=false;
        try{
            //Delete file if already exists
            File f1=new File(destFilePath);
            if(f1.exists()) f1.delete();
            f1=null;
            //Move File
            File f=new File(sourceFilePath);
            flag=f.renameTo(new File(destFilePath));
            f=null;
        }catch(Exception e){
            flag=false;
            System.out.println("Exception while moving file '" + sourceFilePath + "' to '" + destFilePath + "' Error: "+e);
        }
        return flag;
    }
    public static List<String> convertStrToList(String str,String separator,String defaultVal,String ignoreVal){
        List<String> retObj=new ArrayList<>();
        str=(str==null)?defaultVal:str.trim();
        if(!str.equalsIgnoreCase(ignoreVal)){
             String[] tempArr=str.split(separator);
             for(String t:tempArr){
                 t=(t==null)?ignoreVal:t.trim();
                 if(!t.equalsIgnoreCase(ignoreVal)) retObj.add(t);
                 t=null;
             }
        }
        return retObj;
    }
    
    public static List<String> convertStrToList(String str,String separator){
        List<String> respData=new ArrayList<>();
         try{
             int indx=str.indexOf(separator);
             if(indx<0 && str.length()>0)
                 respData.add(str);
             int cutIndx=0;
             while(indx>=0){
                 if(indx==0){
                     respData.add("");
                 }else{
                     respData.add(str.substring(0,indx));
                 }
                 cutIndx=indx+separator.length();
                 if(str.length()>cutIndx){
                     str=str.substring(cutIndx);
                 }else{
                     str="";
                 }
                 indx=str.indexOf(separator);
                 if(indx<0 && str.length()>0){respData.add(str); str="";}
                     
             }
         }catch(Exception e){
             System.out.println("splitToArray() :: Exception : "+e.getMessage());
         }
         return respData;
     }
     
     
     
     public static String readFile(String folderPath,String fileToRead,String lineFeedChar){
         StringBuilder respData=new StringBuilder();
         try{
             folderPath=(folderPath==null)?"":folderPath.trim();
             String filePath=folderPath.endsWith("/")?folderPath+fileToRead:folderPath+"/"+fileToRead;
             lineFeedChar=(lineFeedChar==null)?"":lineFeedChar.trim();
             BufferedReader br=new BufferedReader(new FileReader(filePath));
             String newLine="";
             while((newLine=br.readLine())!=null){
                 if(respData.length()>0)
                     respData.append("<BR>").append(newLine);
                 else
                     respData.append(newLine);
             }
            br.close();
            br=null;
         }catch(IOException e){
             System.out.println("readFile() :: Exception while reading file, "+e.getMessage());
         }
         return respData.toString();
     }
     
     public static int writeToFile(InputStream is,String folderPath,String destFile){
         int retVal=0;
         try{
             File f=new File(folderPath);
             if(!f.exists()) f.mkdirs();
             f=null;
             BufferedOutputStream bos=new BufferedOutputStream(new FileOutputStream(new File(destFile)));
             int x=0;
            byte[] b=new byte[2048];
             while((x=is.read(b))!=-1){
                 bos.write(b,0,x);
                 retVal=retVal+x;
             }
             bos.close();
             bos=null;
             is.close();
             is=null;
         }catch(IOException e){
             System.out.println("Exception while writing to "+destFile+", "+e.getMessage());
         }
         return retVal;
     }
    /* public static String getBase64DecodedString(String str){
         byte[] byteArray = Base64.decodeBase64(str.getBytes());
         String decodedString = new String(byteArray);
         return decodedString;
         
     }
     public static String getBase64EncodedString(String str){
         byte[] byteArray = Base64.encodeBase64(str.getBytes());
         String encodedString = new String(byteArray);
         return encodedString;
         
     }
     
     public static String getMD5EncodedString(String str){
         String respStr="";
         try{
         MessageDigest md = MessageDigest.getInstance("MD5");
 		md.update(str.getBytes());
 		byte[] digest = md.digest();
 		StringBuffer sb = new StringBuffer();
 		for (byte b : digest) {
 			sb.append(String.format("%02x", b & 0xff));
 		}
           respStr=sb.toString();
         }catch(Exception e){
         }
         return respStr;
     }*/
     public static String formatString(String strVal,String defaultVal){
         strVal=(strVal==null)?defaultVal:strVal.trim();
         return strVal;
     }
     public static String formatNumber(int number,int digitCount,boolean fillPrefix){
         String retVal=""+number;
         while(digitCount>retVal.length()) retVal=(fillPrefix)?"0"+retVal:retVal+"0";
         return retVal;
     }
     public static int boolToInt(boolean x){
         return (x)?1:0;
     }
     
     public static boolean intToBool(int x){
         return (x>0)?true:false;
     }
     
     public static boolean strToBool(String x,boolean defaultVal){
        return (strToInt(x,(defaultVal==true)?1:0)>0)?true:(x.trim().equalsIgnoreCase("yes") || x.trim().equalsIgnoreCase("true") || x.trim().equalsIgnoreCase("y"))?true:false;
        
     }
     
     public static int strToInt(String str,int defaultVal){
         int retVal=defaultVal;
         try{
             retVal=Integer.parseInt(str);
         }catch(Exception e){
             
                 retVal=defaultVal;
             
         }
         return retVal;
     }
      public static long strToLong(String str,long defaultVal){
         long retVal=defaultVal;
         try{
             retVal=Long.parseLong(str);
         }catch(Exception e){
             retVal=defaultVal;
         }
         return retVal;
     }
       public static double strToDouble(String str,double defaultVal){
         double retVal=defaultVal;
         try{
             retVal=Double.parseDouble(str);
         }catch(Exception e){
             retVal=defaultVal;
         }
         return retVal;
     }
       
     public static String doubleToStr(double val){
         String valStr=""+val;
         if(valStr.indexOf(".")>0) valStr=valStr.substring(0,valStr.indexOf("."));
         return valStr;
     }
}
