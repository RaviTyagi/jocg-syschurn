package com.jss.jocg.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.reflect.Field;
import org.slf4j.Logger;

public class SubscriberVendorUpdateRecord {
	//SUBSCRIBER_ACT_NO,MSISDN,OPERATOR_ID,SERVICE_ID,VENDOR_NAME,CUST_STATUS,PACK_CODE,PACK_STATUS,FIRST_SUBSCRIBE_DATE,FALLBACK_PRICE,ORIGINAL_PRICE,PYMT_SOURCE_ID
	//14535168,9425088805,BW_OF,MOBTV,,A,PK387,A,2/9/2012 18:15,85,99,BSNL	
		String customerId;
		Long msisdn;
		String operatorCode;
		String serviceName;
		String adNetworkName;
		String customerStatus;
		String packageName;
		String packageStatus;
		java.util.Date firstSubscribeDate;
		int fallbackPrice;
		int masterPrice;
		String chargingOperatorName;
		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy HH:mm");
		
		public SubscriberVendorUpdateRecord(){
			
		}

		

		public void parseRecord(Logger logger,String logHeader,String newCDR){
			
			try{
				String[] strArr=newCDR.split(",");
				for(int i=0;i<strArr.length;i++){
					if(i==0) this.setCustomerId(JocgString.formatString(strArr[0],"NA"));
					else if(i==1) this.setMsisdn(JocgString.strToLong(strArr[1],0L));
					else if(i==2) this.setOperatorCode(JocgString.formatString(strArr[2],"NA"));
					else if(i==3) this.setServiceName(JocgString.formatString(strArr[3],"NA"));
					else if(i==4) this.setAdNetworkName(JocgString.formatString(strArr[4],"NA"));
					else if(i==5) this.setCustomerStatus(JocgString.formatString(strArr[5],"NA"));
					else if(i==6) this.setPackageName(JocgString.formatString(strArr[6],"NA"));
					else if(i==7) this.setPackageStatus(JocgString.formatString(strArr[7],"NA"));
					else if(i==8){
						try{
							Calendar cal=Calendar.getInstance();
							cal.setTimeInMillis(sdf.parse(JocgString.formatString(strArr[8],"NA")).getTime());
							this.setFirstSubscribeDate(cal.getTime());
						}catch(Exception ee){
							this.setFirstSubscribeDate(null);
						}
					}else if(i==9) this.setFallbackPrice(JocgString.strToInt(strArr[9],0));
					else if(i==10) this.setMasterPrice(JocgString.strToInt(strArr[10],0));
					else if(i==11) this.setChargingOperatorName(JocgString.formatString(strArr[11],"NA"));
					
				}
			}catch(Exception e){
				logger.error(logHeader+" Exception while parsing record "+newCDR+"| "+e.getMessage());
			}
		}

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public Long getMsisdn() {
			return msisdn;
		}

		public void setMsisdn(Long msisdn) {
			this.msisdn = msisdn;
		}

		public String getOperatorCode() {
			return operatorCode;
		}

		public void setOperatorCode(String operatorCode) {
			this.operatorCode = operatorCode;
		}

		public String getServiceName() {
			return serviceName;
		}

		public void setServiceName(String serviceName) {
			this.serviceName = serviceName;
		}

		public String getAdNetworkName() {
			return adNetworkName;
		}

		public void setAdNetworkName(String adNetworkName) {
			this.adNetworkName = adNetworkName;
		}

		public String getCustomerStatus() {
			return customerStatus;
		}

		public void setCustomerStatus(String customerStatus) {
			this.customerStatus = customerStatus;
		}

		public String getPackageName() {
			return packageName;
		}

		public void setPackageName(String packageName) {
			this.packageName = packageName;
		}

		

		public String getPackageStatus() {
			return packageStatus;
		}

		public void setPackageStatus(String packageStatus) {
			this.packageStatus = packageStatus;
		}

		public java.util.Date getFirstSubscribeDate() {
			return firstSubscribeDate;
		}

		public void setFirstSubscribeDate(java.util.Date firstSubscribeDate) {
			this.firstSubscribeDate = firstSubscribeDate;
		}

		public int getFallbackPrice() {
			return fallbackPrice;
		}

		public void setFallbackPrice(int fallbackPrice) {
			this.fallbackPrice = fallbackPrice;
		}

		public int getMasterPrice() {
			return masterPrice;
		}

		public void setMasterPrice(int masterPrice) {
			this.masterPrice = masterPrice;
		}

		public String getChargingOperatorName() {
			return chargingOperatorName;
		}

		public void setChargingOperatorName(String chargingOperatorName) {
			this.chargingOperatorName = chargingOperatorName;
		}
		
}
