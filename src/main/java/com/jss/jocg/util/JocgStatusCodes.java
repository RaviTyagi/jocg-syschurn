package com.jss.jocg.util;

public class JocgStatusCodes {

	public static int UNSUB_SUCCESS_PENDING=-90;
	public static int UNSUB_FAILED=-91;
	public static int UNSUB_FAILED_NOTSUBSCRIBED=-92;
	public static int UNSUB_FAILED_LINKFAILURE=-93;
	public static int UNSUB_SUCCESS=-99;
	
	public static int REN_NOT_REQUIRED=-1;
	public static int REN_NOT_INITIATED=0;
	public static int REN_INPROCESS=1;
	public static int REN_COMPLETED=2;
	
	public static int SUB_SUCCESS_PENDING=0;
	public static int SUB_SUCCESS_CG=1;
	public static int SUB_SUCCESS_PARKING=2;
	public static int SUB_SUCCESS_GRACE=3;
	public static int SUB_SUCCESS_ACTIVE=5;
	public static int SUB_SUCCESS_FREE=10;
	
	public static int SUB_FAILED_TRANS=-10;
	public static int SUB_FAILED_CG=-1;
	public static int SUB_FAILED_SUSPENDFROMPARKING=-2;
	public static int SUB_FAILED_SUSPENDFROMGRACE=-3;
	public static int SUB_FAILED_SDPCHARGING=-5;
	
	
	public static int CHARGING_INPROCESS=0;
	public static int CHARGING_CGSUCCESS=1;
	public static int CHARGING_SDPPARKING=2;
	public static int CHARGING_SDPGRACE=3;
	public static int CHARGING_SUCCESS=4;
	
	public static int CHARGING_FAILED_CGCONSENT=-1;
	public static int CHARGING_FAILED_SDPPARKING=-2;
	public static int CHARGING_FAILED_SDPGRACE=-3;
	public static int CHARGING_FAILED=-4;
	public static int CHARGING_FAILED_LOWBALANCE=-5;
	
	public static int CHARGING_FAILED_INVREQPARAMS=-100;
	public static int CHARGING_FAILED_INVOPERATOR=-101;
	public static int CHARGING_FAILED_INVPRICEPOINT=-102;
	public static int CHARGING_FAILED_INVCHINTF=-103;
	public static int CHARGING_FAILED_INVHANDLER=-104;
	public static int CHARGING_FAILED_INVCHURL=-105;
	public static int CHARGING_FAILED_INVCHPARAMS=-106;
	public static int CHARGING_FAILED_ALYSUBSCRIBED=-110;
	public static int CHARGING_FAILED_PREVTRANSNOTEXPIRED=-111;
	public static int CHARGING_FAILED_DUPLICATEREQUEST=-112;
	public static int CHARGING_FAILED_EXCEPTION=-120;
	
	public static int NOTIFICATION_PENDING=0;
	public static int NOTIFICATION_QUEUED=1;
	public static int NOTIFICATION_SENT=2;
	
	public static int NOTIFICATION_NOTSENT=-1;
	public static int NOTIFICATION_FAILED=-2;
	
	
	public static String getStatusDescription(int code){
		if(code==CHARGING_CGSUCCESS)  return "CHARGING_CGSUCCESS"; 
		else if(code==CHARGING_FAILED) return "CHARGING_FAILED";
		else if(code==CHARGING_FAILED_ALYSUBSCRIBED) return "CHARGING_FAILED_ALYSUBSCRIBED";
		else if(code==CHARGING_FAILED_CGCONSENT) return "CHARGING_FAILED_CGCONSENT";
		else if(code==CHARGING_FAILED_DUPLICATEREQUEST) return "CHARGING_FAILED_DUPLICATEREQUEST";
		else if(code==CHARGING_FAILED_EXCEPTION) return "CHARGING_FAILED_EXCEPTION";
		else if(code==CHARGING_FAILED_INVCHINTF) return "CHARGING_FAILED_INVCHINTF";
		else if(code==CHARGING_FAILED_INVCHPARAMS) return "CHARGING_FAILED_INVCHPARAMS";
		else if(code==CHARGING_FAILED_INVCHURL) return "CHARGING_FAILED_INVCHURL";
		else if(code==CHARGING_FAILED_INVHANDLER) return "CHARGING_FAILED_INVHANDLER";
		else if(code==CHARGING_FAILED_INVOPERATOR) return "CHARGING_FAILED_INVOPERATOR";
		else if(code==CHARGING_FAILED_INVPRICEPOINT) return "CHARGING_FAILED_INVPRICEPOINT";
		else if(code==CHARGING_FAILED_INVREQPARAMS) return "CHARGING_FAILED_INVREQPARAMS";
		else if(code==CHARGING_FAILED_LOWBALANCE) return "CHARGING_FAILED_LOWBALANCE";
		else if(code==CHARGING_FAILED_SDPGRACE) return "CHARGING_FAILED_SDPGRACE";
		else if(code==CHARGING_FAILED_SDPPARKING) return "CHARGING_FAILED_SDPPARKING";
		else if(code==CHARGING_INPROCESS) return "CHARGING_INPROCESS";
		else if(code==CHARGING_SDPGRACE) return "CHARGING_SDPGRACE";
		else if(code==CHARGING_SDPPARKING) return "CHARGING_SDPPARKING";
		else if(code==CHARGING_SUCCESS) return "CHARGING_SUCCESS";
		else if(code==NOTIFICATION_FAILED) return "NOTIFICATION_FAILED";
		else if(code==NOTIFICATION_NOTSENT) return "NOTIFICATION_NOTSENT";
		else if(code==NOTIFICATION_PENDING) return "NOTIFICATION_PENDING";
		else if(code==NOTIFICATION_QUEUED) return "NOTIFICATION_QUEUED";
		else if(code==NOTIFICATION_SENT) return "NOTIFICATION_SENT";
		else if(code==SUB_FAILED_CG) return "SUB_FAILED_CG";
		else if(code==SUB_FAILED_SDPCHARGING) return "SUB_FAILED_SDPCHARGING";
		else if(code==SUB_FAILED_SUSPENDFROMGRACE) return "SUB_FAILED_SUSPENDFROMGRACE";
		else if(code==SUB_FAILED_SUSPENDFROMPARKING) return "SUB_FAILED_SUSPENDFROMPARKING";
		else if(code==SUB_FAILED_TRANS) return "SUB_FAILED_TRANS";
		else if(code==SUB_SUCCESS_ACTIVE) return "SUB_SUCCESS_ACTIVE";
		else if(code==SUB_SUCCESS_CG) return "SUB_SUCCESS_CG";
		else if(code==SUB_SUCCESS_FREE) return "SUB_SUCCESS_FREE";
		else if(code==SUB_SUCCESS_GRACE) return "SUB_SUCCESS_GRACE";
		else if(code==SUB_SUCCESS_PARKING) return "SUB_SUCCESS_PARKING";
		else if(code==SUB_SUCCESS_PENDING) return "SUB_SUCCESS_PENDING";
		else if(code==UNSUB_SUCCESS) return "UNSUB_SUCCESS";
		else return "SYSTEM ERROR";

	}
	
}
