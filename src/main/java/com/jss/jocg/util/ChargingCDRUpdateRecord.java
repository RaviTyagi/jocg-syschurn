package com.jss.jocg.util;

import java.lang.reflect.Field;

import org.slf4j.Logger;

public class ChargingCDRUpdateRecord {
String chardingCDRid;
String referer;
@Override
public String toString(){
    StringBuilder sb=new StringBuilder();
    Field[] fields = this.getClass().getDeclaredFields();
    sb.append(this.getClass().getName()).append("|");
    try {
        for (Field field : fields) {
            sb.append(field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    sb.append("|");
    return sb.toString();
}

public void parseRecord(Logger logger,String logHeader,String newCDR){
	try{
		String[] strArr=newCDR.split(",");
		this.setChardingCDRid(JocgString.formatString(strArr[0],"NA"));
		this.setReferer(JocgString.formatString(strArr[1],"NA"));
	}catch(Exception e){
		
	}
}

public String getChardingCDRid() {
	return chardingCDRid;
}

public void setChardingCDRid(String chardingCDRid) {
	this.chardingCDRid = chardingCDRid;
}

public String getReferer() {
	return referer;
}

public void setReferer(String referer) {
	this.referer = referer;
}



}
