package com.jss.jocg.util;

import java.lang.reflect.Field;

import org.slf4j.Logger;

public class SubscriberUpdateRecord {
	//ID,MSISDN,systemId,CIRCLENAME,OPERATORNAME,PACKAGEID,PACKAGENAME,SERVICEID,SERVICENAME,JOCGCDRID,JOCGTRANSID,OPERATORSERVICEKEY,OPERATORPRICEPOINT,CHARGEDAMOUNT,STATUS,Vendor_Name

	String subscriberid;
	Long msisdn;
	String systemId;
	String circleName;
	String operatorName;
	int packageId;
	String packageName;
	int serviceId;
	String serviceName;
	String jocgCDRId;
	String jocgTransId;
	String operatorServiceKey;
	String operatorPricePoint;
	String chargedAmount;
	int status;
	String adNetworkName;
	
	public SubscriberUpdateRecord(){
		
	}

	 @Override
	    public String toString(){
	        StringBuilder sb=new StringBuilder();
	        Field[] fields = this.getClass().getDeclaredFields();
	        sb.append(this.getClass().getName()).append("|");
	        try {
	            for (Field field : fields) {
	                sb.append(field.getName()).append("=").append(field.get(this)).append(",");
	            }
	        } catch (IllegalArgumentException ex) {
	            System.out.println(ex);
	        } catch (IllegalAccessException ex) {
	            System.out.println(ex);
	        }
	        sb.append("|");
	        return sb.toString();
	    }

	public void parseRecord(Logger logger,String logHeader,String newCDR){
		try{
			String[] strArr=newCDR.split(",");
			for(int i=0;i<strArr.length;i++){
				if(i==0) this.setSubscriberid(JocgString.formatString(strArr[0],"NA"));
				else if(i==1) this.setMsisdn(JocgString.strToLong("91"+strArr[1],0L));
				else if(i==2) this.setSystemId(JocgString.formatString(strArr[2],"NA"));
				else if(i==3) this.setCircleName(JocgString.formatString(strArr[3],"NA"));
				else if(i==4) this.setOperatorName(JocgString.formatString(strArr[4],"NA"));
				else if(i==5) this.setPackageId(JocgString.strToInt(strArr[5],0));
				else if(i==6) this.setPackageName(JocgString.formatString(strArr[6],"NA"));
				else if(i==7) this.setServiceId(JocgString.strToInt(strArr[7],0));
				else if(i==8) this.setServiceName(JocgString.formatString(strArr[8],"NA"));
				else if(i==9) this.setJocgCDRId(JocgString.formatString(strArr[9],"NA"));
				else if(i==10) this.setJocgTransId(JocgString.formatString(strArr[10],"NA"));
				else if(i==11) this.setOperatorServiceKey(JocgString.formatString(strArr[11],"NA"));
				else if(i==12) this.setOperatorPricePoint(JocgString.formatString(strArr[12],"NA"));
				else if(i==13) this.setChargedAmount(JocgString.formatString(strArr[13],"NA"));
				else if(i==14) this.setStatus(JocgString.strToInt(strArr[14],0));
				else if(i==15) this.setAdNetworkName(JocgString.formatString(strArr[15],"NA"));
			}
		}catch(Exception e){
			logger.error(logHeader+" Exception while parsing record "+newCDR+"| "+e.getMessage());
		}
	}
	
	public String getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(String subscriberid) {
		this.subscriberid = subscriberid;
	}

	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public int getPackageId() {
		return packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getJocgCDRId() {
		return jocgCDRId;
	}

	public void setJocgCDRId(String jocgCDRId) {
		this.jocgCDRId = jocgCDRId;
	}

	public String getJocgTransId() {
		return jocgTransId;
	}

	public void setJocgTransId(String jocgTransId) {
		this.jocgTransId = jocgTransId;
	}

	public String getOperatorServiceKey() {
		return operatorServiceKey;
	}

	public void setOperatorServiceKey(String operatorServiceKey) {
		this.operatorServiceKey = operatorServiceKey;
	}

	public String getOperatorPricePoint() {
		return operatorPricePoint;
	}

	public void setOperatorPricePoint(String operatorPricePoint) {
		this.operatorPricePoint = operatorPricePoint;
	}

	public String getChargedAmount() {
		return chargedAmount;
	}

	public void setChargedAmount(String chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAdNetworkName() {
		return adNetworkName;
	}

	public void setAdNetworkName(String adNetworkName) {
		this.adNetworkName = adNetworkName;
	}
	
	
	
}
