package com.jss.jocg.util;

public class ServiceException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String errorCode;
	String errorDescription;
	
	public ServiceException(){
		super();
	}
	
	public ServiceException(String errorCode,String description){
		this.errorCode=errorCode;
		this.errorDescription=description;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
	
	
}
