package com.jss.jocg.util;

import net.sourceforge.wurfl.core.Device;
import net.sourceforge.wurfl.core.MarkUp;

public class JocgDevice implements java.io.Serializable{

    public final String KEY_BRAND = "brand_name";
    public final String KEY_MODEL = "model_name";
    public final String KEY_MARKETING_NAME = "marketing_name";
    public final String KEY_MODEL_EXT = "model_extra_info";
    public final String KEY_IS_WIRELESS = "is_wireless_device";
    public final String KEY_IS_TABLET = "is_tablet";
    public final String KEY_WEB_SUPPORT = "device_claims_web_support";
    public final String KEY_NOKIA_SERIES = "nokia_series";
    public final String KEY_NOKIA_EDITION = "nokia_edition";
    public final String KEY_NOKIA_FEATURE_PACK = "nokia_feature_pack";
    public final String KEY_DEVICE_OS = "device_os";
    public final String KEY_DEVICE_OS_VERSION = "device_os_version";
    public final String KEY_MOBILE_BROWSER = "mobile_browser";
    public final String KEY_MOBILE_BROWSER_VERSION = "mobile_browser_version";
    public final String KEY_CAN_HAVE_MOBILE_NUMBER = "can_assign_phone_number";
    public final String KEY_XHTML_SUPPORT_LEVEL = "xhtml_support_level";
    public final String KEY_PREFERRED_MARKUP = "preferred_markup";
    public final String KEY_SUPPORTS_VOICEXML = "voicexml";
    public final String KEY_SUPPORTS_MULTIPORT = "multipart_support";
    public final String KEY_RESOLUTION_WIDTH = "resolution_width";
    public final String KEY_RESOLUTION_HEIGHT = "resolution_height";
    public final String KEY_COLUMNS = "columns";
    public final String KEY_ROWS = "rows";
    public final String KEY_MAX_IMAGE_WIDTH = "max_image_width";
    public final String KEY_MAX_IMAGE_HEIGHT = "max_image_height";
    public final String KEY_UA_PROFFILE = "uaprof";
    public final String KEY_MAX_DUAL_ORIENTATION = "dual_orientation";
    public final String KEY_BRAND_NAME = "brand_name";
    public final String KEY_IS_WIRELESS_DEVICE = "is_wireless_device";
    public final String KEY_MODEL_EXTRA_INFO = "model_extra_info";
    public final String KEY_MODEL_NAME = "model_name";
    public final String KEY_POINTING_METHOD = "pointing_method";
    public final String KEY_UAPROF = "uaprof";
    public final String KEY_UX_FULL_DESKTOP = "ux_full_desktop";
    public final String KEY_XHTMLMP_PREFERRED_MIME_TYPE = "xhtmlmp_preferred_mime_type";
    public final String KEY_DEVICE_CLAIMS_WEB_SUPPORT = "device_claims_web_support";
    public final String KEY_MULTIPART_SUPPORT = "multipart_support";
    public final String KEY_WAV = "wav";
    public final String KEY_SP_MIDI = "sp_midi";
    public final String KEY_MP3 = "mp3";
    public final String KEY_AMR = "amr";
    public final String KEY_MIDI_MONOPHONIC = "midi_monophonic";
    public final String KEY_IMELODY = "imelody";
    public final String KEY_JPG = "jpg";
    public final String KEY_GIF = "gif";
    public final String KEY_BMP = "bmp";
    public final String KEY_WBMP = "wbmp";
    public final String KEY_PNG = "png";
    public final String KEY_PLAYBACK_MP4 = "playback_mp4";
    public final String KEY_PLAYBACK_3GPP = "playback_3gpp";

    public Device device;

    public JocgDevice() {

    }

    public String getId() {
        String deviceId = "";
        try {
            deviceId = device.getId();
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        return deviceId;
    }

    public String getRootId() {
        String deviceRootId = "";
        try {
            deviceRootId = device.getDeviceRootId();
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        return deviceRootId;
    }

    public int getHeight(int defaultVal) {
        int ht = 0;
        ht = getProperty(KEY_RESOLUTION_HEIGHT, defaultVal);
        if (ht != defaultVal) {
            ht = getProperty(KEY_MAX_IMAGE_HEIGHT, defaultVal);
        }
        return ht;
    }

    public int getWidth(int defaultVal) {
        int wd = 0;
        wd = getProperty(KEY_RESOLUTION_WIDTH, defaultVal);
        if (wd != defaultVal) {
            wd = getProperty(KEY_MAX_IMAGE_WIDTH, defaultVal);

        }
        return wd;
    }

    public int getProperty(String key, int defaultVal) {
        int val = defaultVal;
        try {
            String valStr = device.getCapability(key);
            valStr = (valStr == null) ? "" + defaultVal : valStr.trim();
            val = Integer.parseInt(valStr);
        } catch (Exception e) {
            val = defaultVal;
        }

        return val;
    }

    public boolean getProperty(String key, boolean defaultVal) {
        boolean val = defaultVal;
        try {
            String valStr = device.getCapability(key);
            valStr = (valStr == null) ? "" + defaultVal : valStr.trim();
            val = valStr.equalsIgnoreCase("trye") ? true : false;
        } catch (Exception e) {
            val = defaultVal;
        }

        return val;
    }

    public String getView() {
        String viewMarkup = "xhtmlmp";
        try {
            MarkUp markUp = device.getMarkUp();
            if (MarkUp.XHTML_ADVANCED.equals(markUp)) {
                viewMarkup = "xhtmladv";
            } else if (MarkUp.XHTML_SIMPLE.equals(markUp)) {
                viewMarkup = "xhtmlmp";
            } else if (MarkUp.CHTML.equals(markUp)) {
                viewMarkup = "chtml";
            } else if (MarkUp.WML.equals(markUp)) {
                viewMarkup = "wml";
            }
        } catch (Exception e) {
            viewMarkup = "xhtmlmp";
        }

        return viewMarkup;
    }

    public String getProperty(String key, String defaultVal) {
        String val = defaultVal;
        try {
            String valStr = device.getCapability(key);
            valStr = (valStr == null) ? "" + defaultVal : valStr.trim();
            val = valStr.trim();
        } catch (Exception e) {
            val = defaultVal;
        }

        return val;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

}
