package com.jss.jocg.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.codec.binary.Base64;


public class JcmsSSLClient {
	//Import .cer file using following command to keystore
    //keytool -import -file sla-alacrity-ca-public.cer  -keystore publicKey.store
	SimpleDateFormat sdf;
    String destURL;
    String acceptDataFormat;
    String contentType;
    String httpMethod;
    int connectionTimeoutInSecond;
    int readTimeoutInSecond;
    String authUser;
    String authPwd;
    String authToken;
    boolean useAuthTokenInBasicAuth;
    String hostName;
    boolean useHostNameInHeader;
    String requestData;
    boolean useBasicAuthentication;
    String certificateKeystorePath;
    String trustStorePassword;
    JcmsSSLClientResponse urlResponse;
    
   /* public static void main(String[] args){
        //Import certificat eusing keytool command like as follows:
        //Import to java default keystore cacerts
            ///usr/java/jdk1.7.0/bin/keytool -import -alias "alacritymart" -file sla-alacrity-ca-public.cer -keystore /usr/java/jdk1.7.0/jre/lib/security/cacerts
            //keytool -import -alias "vodanmro" -file MSG_CERT.cer -keystore /usr/java/jdk1.8.0_45/jre/lib/security/cacerts
        //Import to private keystore other then java default
            // keytool -import -file sla-alacrity-ca-public.cer  -keystore publicKey.store
        //Test Command java -cp ".:./jcms-common-classes.jar:./commons-codec-1.9.jar:./commons-logging-1.1.3.jar:./mysql-connector-java-commercial-5.1.6-bin.jar" net.rocg.sslclient.JcmsSSLClient "https://sandbox.api.sla-alacrity.com/1.0/tel%3A7417382749/transactions" "/opt/tomcat80/certificates/alakrity_zen/publicKey.store" "changeit" "mobilart_690_sbox" "shastachunka" "application/xml" "application/xml" "POST" "<purchase-request xmlns=\"urn:alacrity:api\"> <amount>1.99</amount>  <merchant>partner:dc1c566d-474b-4c8b-8841-2e13b7f095a6</merchant> <campaign>campaign:c71ee0c6263b44bd671b56f04761d5bbc64abc8c</campaign> <reference>partner reference</reference> <customer-reference>customer reference</customer-reference> <purchase-locale>ar_IQ</purchase-locale> <content-description>comment to appear on customer bill</content-description> <content-id>123</content-id> <channel>WEB</channel> <language-locale>ar_IQ</language-locale> <correlator>100000002</correlator> </purchase-request>"
        
        String destURL=args[0];
        String keystorePath=args[1];
        String keystorePwd=args[2];
        String userName=args[3];
        String pwd=args[4];
        String acceptFormat=args[5];
        String contentType=args[6];
        String httpMethod=args[7];
        String dataToSend=args[8];
        JcmsSSLClient sslClient=new JcmsSSLClient();
        sslClient.setDestURL(destURL);
        sslClient.setCertificateKeystorePath(keystorePath);
        sslClient.setTrustStorePassword(keystorePwd);
        sslClient.setUseBasicAuthentication(true);
//        sslClient.setAuthUser(userName);
//        sslClient.setAuthPwd(pwd);
         sslClient.setAuthUser("NumEROd!g!TAL");
        sslClient.setAuthPwd("NUM3roD!G!T@l");
        sslClient.setAcceptDataFormat(acceptFormat);
        sslClient.setContentType(contentType);
        sslClient.setHttpMethod(httpMethod);
//        sslClient.setRequestData(dataToSend);
        sslClient.setRequestData("<methodCall><methodName>Subscription.DeleteSubscription</methodName><params><param><value><struct><member><name>MSISDN</name><value><string>9999631990</string></value></member><member><name>service</name><value><string>NUD_CLIPSD</string></value></member><member><name>class</name><value><string>NUD_CLIPSD</string></value></member><member><name>mode</name><value><string>WAP</string></value></member><member><name>requestid</name><value><string>209</string></value></member><member><name>action</name><value><string>DCT</string></value></member><member><name>org_id</name><value><string>NUMERODIGITAL</string></value></member><member><name>loginid</name><value><string>NumEROd!g!TAL</string></value></member><member><name>password</name><value><string>NUM3roD!G!T@l</string></value></member><member><name>timestamp</name><value><string>150917005230</string></value></member></struct></value></param></params></methodCall>");
        sslClient.setConnectionTimeoutInSecond(30);
        sslClient.setReadTimeoutInSecond(30);
        JcmsSSLClientResponse resp=sslClient.connectURL();
        System.out.println("Response code "+resp.getResponseCode());
        System.out.println("Response Data "+resp.getResponseData());
        System.out.println("Response Error "+resp.getErrorMessage());
        
        
    }*/
    
    public JcmsSSLClient(){
        this.setDestURL("NA");
        this.setAcceptDataFormat("application/xml");
        this.setUseBasicAuthentication(false);
        this.setContentType("application/xml");
        this.setHttpMethod("GET");
        this.setConnectionTimeoutInSecond(10);
        this.setReadTimeoutInSecond(10);
        this.setCertificateKeystorePath("NA");
        urlResponse=new JcmsSSLClientResponse();
        this.setRequestData("NA");
        this.setAuthPwd("NA");
        this.setAuthUser("NA");
        this.setTrustStorePassword("NA");
    }
    
   public JcmsSSLClientResponse invokeURL(String logHeader,String urlStr){
	   String resp="";
	   long reqTime=System.currentTimeMillis();
	   StringBuilder sb=new StringBuilder();
	   
	   try{
		   sb.append("Invoking URL "+urlStr);
		   URL url=new URL(urlStr);
		   HttpURLConnection con=(HttpURLConnection)url.openConnection();
		   con.setDoInput(true);
           con.setDoOutput(true);
           con.setUseCaches(false);
           sb.append("|Timeout Setting 10Sec|");
           // System.out.println(logHeader+"Setting Timeout Properties "+this.getConnectionTimeoutInSecond()+", "+this.getReadTimeoutInSecond()+", "+this.getHttpMethod());
           con.setConnectTimeout( 10*1000 );
           con.setReadTimeout( 10*1000 );
           con.connect();
           sb.append("|Connected|");
           int responseCode = con.getResponseCode();
           sb.append("|responseCode "+responseCode+"|Reading Input....|Response Data : ");
          // System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
           InputStream inputStream;
           if (responseCode == HttpURLConnection.HTTP_OK) 
             inputStream = con.getInputStream();
           else
             inputStream = con.getErrorStream();
           // Process the response
           BufferedReader reader;
           String line = null;
           StringBuilder sb1=new StringBuilder();
           reader = new BufferedReader( new InputStreamReader( inputStream ) );
           while( ( line = reader.readLine() ) != null ){
             sb1.append(line);
             sb.append(line);
           }
           
           inputStream.close();
           con.disconnect();
          
          //  System.out.println(logHeader+" Data Received "+sb.toString());
           con=null;
           url=null;
           this.urlResponse.setResponseCode(responseCode);
           this.urlResponse.setResponseData(sb1.toString());
	   }catch(Exception e){
		   this.urlResponse.setResponseCode(-102);
           this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
      }
	   
	   this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
	   sb.append("|Response Time (ms) "+ this.urlResponse.getResponseTime());
	   this.urlResponse.setProcessLogs(sb.toString());
	   System.out.println(new java.sql.Timestamp(System.currentTimeMillis())+"::"+logHeader+sb.toString()+"| Output sent to Application:"+urlResponse.getResponseData());
	   return  this.urlResponse;
   }

    public JcmsSSLClientResponse connectInsecureURL(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else{
           

                try {
                          
                          URL url = new URL( this.getDestURL() );
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          System.out.println(logHeader+"Connection created "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                           System.out.println(logHeader+"Setting Timeout Properties "+this.getConnectionTimeoutInSecond()+", "+this.getReadTimeoutInSecond()+", "+this.getHttpMethod());
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          System.out.println(logHeader+"Setting Content Type Properties "+this.getContentType()+", "+this.getAcceptDataFormat());
                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());
                         if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                            System.out.println(logHeader+" :: Auth Data Set, "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            System.out.println(logHeader+" :: Auth Data Set, "+encoding);
                          }
                          
                          con.connect();
                          System.out.println(logHeader+"Connected.  ");
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+"Writing outData  "+this.getRequestData());
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                          }
                          
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                           System.out.println(logHeader+" Data Received "+sb.toString());
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    public JcmsSSLClientResponse connectInsecureURLBasicAuthOxigen(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
              try {
                          URL url = new URL(this.getDestURL());
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          System.out.println(logHeader+"Connection created "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          System.out.println(logHeader+"Setting Timeout Properties "+this.getConnectionTimeoutInSecond()+", "+this.getReadTimeoutInSecond()+", "+this.getHttpMethod());
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                         
                          if(this.isUseHostNameInHeader())
                            con.setRequestProperty("Host", this.getHostName());
                          System.out.println(logHeader+"  Host :  "+this.getHostName());
                          System.out.println(logHeader+"  Request Method :  "+this.getHttpMethod().toUpperCase().trim());
                          System.out.println(logHeader+"Setting Content Type Properties "+this.getContentType()+", "+this.getAcceptDataFormat());
                          con.setRequestProperty( "content-type", this.getContentType() );
                           System.out.println(logHeader+"  content-type :  "+this.getContentType());
                          con.setRequestProperty("accept", this.getAcceptDataFormat());
                           System.out.println(logHeader+"  accept :  "+this.getAcceptDataFormat());
                          
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("authToken", this.getAuthToken().trim()); 
                            System.out.println(logHeader+" :: authToken : "+this.getAuthToken().trim());
                          }
                          
                          if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            System.out.println(logHeader+" :: Authorization : Basic "+encoding);
                          }
                          
                          con.connect();
                          System.out.println(logHeader+"Connected.  ");
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+"Request Data :  "+this.getRequestData());
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                          }
                          
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                           System.out.println(logHeader+" Data Received "+sb.toString());
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    
    
    public JcmsSSLClientResponse connectInsecureURLOxigen(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else{
           

                try {
                          
                          URL url = new URL( this.getDestURL() );
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          System.out.println(logHeader+"Connection created "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                           System.out.println(logHeader+"Setting Timeout Properties "+this.getConnectionTimeoutInSecond()+", "+this.getReadTimeoutInSecond()+", "+this.getHttpMethod());
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          System.out.println(logHeader+"Setting Content Type Properties "+this.getContentType()+", "+this.getAcceptDataFormat());
//                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());                          
                         if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("authToken", this.getAuthToken().trim());                             
                          }
                          
                          if(this.isUseBasicAuthentication()){
                        	String basicAuthStr1=this.getAuthUser()+":"+this.getAuthPwd();                                                        
							String basicAuthStr=URLEncoder.encode(basicAuthStr1);                            
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));                            
                            con.setRequestProperty("Authorization", ":Basic " +encoding);
//                            con.set                            
                            System.out.println("--||---> Authorization "+"Basic " +encoding);
                         }                         
                          con.connect();
                          System.out.println(logHeader+"Connected.  ");
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+"Writing outData  "+this.getRequestData());
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());                              
                              outputStream.close();
                          }                          
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                           System.out.println(logHeader+" Data Received "+sb.toString());
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    public JcmsSSLClientResponse connectInsecureURLOxigenPlain(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else{
           

                try {                         
                          URL url = new URL( this.getDestURL() );
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          System.out.println(logHeader+"Connection created "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);                           
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );                          
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());                          
                         if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());                         
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim());                             
                          }else if(this.isUseBasicAuthentication()){
                        	String basicAuthStr1=this.getAuthUser()+":"+this.getAuthPwd();                                                        //							                           
                            String encoding =new String( Base64.encodeBase64(basicAuthStr1.getBytes()));                            
                            con.setRequestProperty("Authorization", "Basic " +encoding);                            
                         }                         
                          con.connect();                                                                             
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                           System.out.println(logHeader+" Data Received "+sb.toString());
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    
    public JcmsSSLClientResponse connectInsecureURLBasicAuth(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
           

                try {
                          
                          URL url = new URL( this.getDestURL() );
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          System.out.println(logHeader+"Connection created "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          System.out.println(logHeader+"Setting Timeout Properties "+this.getConnectionTimeoutInSecond()+", "+this.getReadTimeoutInSecond()+", "+this.getHttpMethod());
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                         
                          if(this.isUseHostNameInHeader())
                            con.setRequestProperty("Host", this.getHostName());
                          System.out.println(logHeader+"  Host :  "+this.getHostName());
                          System.out.println(logHeader+"  Request Method :  "+this.getHttpMethod().toUpperCase().trim());
                          System.out.println(logHeader+"Setting Content Type Properties "+this.getContentType()+", "+this.getAcceptDataFormat());
                          con.setRequestProperty( "content-type", this.getContentType() );
                           System.out.println(logHeader+"  content-type :  "+this.getContentType());
                          con.setRequestProperty("accept", this.getAcceptDataFormat());
                           System.out.println(logHeader+"  accept :  "+this.getAcceptDataFormat());
                          
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("authorization", "Bearer " + this.getAuthToken().trim()); 
                            System.out.println(logHeader+" :: Authorization : Bearer "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            System.out.println(logHeader+" :: Authorization : Basic "+encoding);
                          }
                          
                          con.connect();
                          System.out.println(logHeader+"Connected.  ");
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+"Request Data :  "+this.getRequestData());
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                          }
                          
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" responseCode "+responseCode+", Reading Input .... ");
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                           System.out.println(logHeader+" Data Received "+sb.toString());
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage(logHeader+" Exception "+e);
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    public JcmsSSLClientResponse connectInsecureURLBasicAuth(){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
            

                try {
                          
                          URL url = new URL( this.getDestURL() );
                          HttpURLConnection con = (HttpURLConnection) url.openConnection();
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());
                          if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                           // System.out.println(" :: Auth Data Set, "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            //System.out.println(" :: Auth Data Set, "+encoding);
                          }
                          if(this.getRequestData().length()>0){
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                          }
                          int responseCode = con.getResponseCode();
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage("Airtel Exception "+e.getMessage());
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    public JcmsSSLClientResponse connectURLViaSSLIgnore(){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
            
                TrustManager[] trustAllCerts = new TrustManager[]{
                  new X509TrustManager(){
                    public X509Certificate[] getAcceptedIssuers(){ return null; }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                 }};
  

                try {
                          SSLContext sslContext = SSLContext.getInstance("SSL");
                          sslContext.init(null, trustAllCerts, new SecureRandom());
                          HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                          
                          URL url = new URL( this.getDestURL() );
                          HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          con.setConnectTimeout(this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout(this.getReadTimeoutInSecond()*1000 );
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());
                          if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                           // System.out.println(" :: Auth Data Set, "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            //System.out.println(" :: Auth Data Set, "+encoding);
                          }
                          
                          if(this.getRequestData().length()>0){
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                          }
                          int responseCode = con.getResponseCode();
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                          inputStream.close();
                          con.disconnect();
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage("Exception "+e.getMessage());
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
    public JcmsSSLClientResponse connectURLViaSSLIgnore(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
            System.out.println(logHeader+" :: Creating Blank Trust Manager to Ignore certificate");
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

  
                System.out.println(logHeader+" :: Creating SSL Context");
                try {
                          SSLContext sslContext = SSLContext.getInstance("SSL");
                          sslContext.init(null, trustAllCerts, new SecureRandom());
                          HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                          System.out.println(logHeader+" :: SSL Factory set");
                          
                          // Create all-trusting host name verifier
                          HostnameVerifier allHostsValid = new HostnameVerifier() {
                              public boolean verify(String hostname, SSLSession session) {
                                  return true;
                              }
                          };
                          
                          // Install the all-trusting host verifier
                          HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
                          
//                          System.out.println("URL "+this.getDestURL());
//                          System.out.println("Connection Timeout  "+this.getConnectionTimeoutInSecond()*1000);
//                          System.out.println("Read Timeout  "+this.getReadTimeoutInSecond()*1000);
//                          System.out.println("Request Method  "+this.getHttpMethod().toUpperCase().trim());
//                          System.out.println("Content-Type  "+this.getContentType());
//                          System.out.println("Accept  "+this.getAcceptDataFormat());
//                          System.out.println("Host  "+this.getHostName());
//                          
//                          System.out.println("Request Data "+this.getRequestData());
//                          
                          URL url = new URL( this.getDestURL() );
                          System.out.println(logHeader+" :: URL "+url);
                          
                          HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                          System.out.println(logHeader+" :: CON "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          con.setConnectTimeout(this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout(this.getReadTimeoutInSecond()*1000 );
                          System.out.println(logHeader+" :: Timeout Set ");
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());
                          //set basic auth parameters
                         if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                            System.out.println(logHeader+" :: Auth Data Set, "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            System.out.println(logHeader+" :: Auth Data Set, "+encoding);
                          }
                          
                          System.out.println(logHeader+" :: Request Data "+this.getRequestData());
                          
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+" :: Writing Data to output Stream......");
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                               System.out.println(logHeader+" :: Data Written.");
                          }else{
                              System.out.println(logHeader+" :: Request Data is Blank not writing anything in request");
                          }
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" :: responseCode "+responseCode);
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          System.out.println(logHeader+" :: inputStream "+inputStream);
                          
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                           System.out.println(logHeader+" :: reader "+reader);
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                           System.out.println(logHeader+" :: sb "+sb.toString());
                          inputStream.close();
                          con.disconnect();
                          System.out.println(logHeader+" :: Connection Disconnected ");
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage("Exception "+e.getMessage());
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }

    /*public JcmsSSLClientResponse connectURLViaSSLIgnore(String logHeader){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else{
            System.out.println(logHeader+" :: Creating Blank Trust Manager to Ignore certificate");
                TrustManager[] trustAllCerts = new TrustManager[]{
                  new X509TrustManager(){
                    public X509Certificate[] getAcceptedIssuers(){ return null; }
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                    public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                 }};
  
                System.out.println(logHeader+" :: Creating SSL Context");
                try {
                          SSLContext sslContext = SSLContext.getInstance("SSL");
                          sslContext.init(null, trustAllCerts, new SecureRandom());
                          HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                          System.out.println(logHeader+" :: SSL Factory set");
                          
                          URL url = new URL( this.getDestURL() );
                          System.out.println(logHeader+" :: URL "+url);
                          
                          HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                          System.out.println(logHeader+" :: CON "+con);
                          con.setRequestProperty( "Connection", "close" );
                          con.setDoInput(true);
                          con.setDoOutput(true);
                          con.setUseCaches(false);
                          con.setConnectTimeout(this.getConnectionTimeoutInSecond()*1000 );
                          con.setReadTimeout(this.getReadTimeoutInSecond()*1000 );
                          System.out.println(logHeader+" :: Timeout Set ");
                          con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                          con.setRequestProperty( "Content-Type", this.getContentType() );
                          con.setRequestProperty("Accept", this.getAcceptDataFormat());
                          //set basic auth parameters
                         if(this.isUseHostNameInHeader())
                            con.setRequestProperty("HOST", this.getHostName());
                          //set basic auth parameters
                          if(this.isUseAuthTokenInBasicAuth()){
                            con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                            System.out.println(logHeader+" :: Auth Data Set, "+this.getAuthToken().trim());
                          }else if(this.isUseBasicAuthentication()){
                            String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                            String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                            con.setRequestProperty("Authorization", "Basic " + encoding);  
                            System.out.println(logHeader+" :: Auth Data Set, "+encoding);
                          }
                          
                          System.out.println(logHeader+" :: Request Data "+this.getRequestData());
                          
                          if(this.getRequestData().length()>0){
                              System.out.println(logHeader+" :: Writing Data to output Stream......");
                              OutputStream outputStream = con.getOutputStream();
                              outputStream.write(this.getRequestData().getBytes());
                              outputStream.close();
                               System.out.println(logHeader+" :: Data Written.");
                          }else{
                              System.out.println(logHeader+" :: Request Data is Blank not writing anything in request");
                          }
                          int responseCode = con.getResponseCode();
                          System.out.println(logHeader+" :: responseCode "+responseCode);
                          InputStream inputStream;
                          if (responseCode == HttpURLConnection.HTTP_OK) 
                            inputStream = con.getInputStream();
                          else
                            inputStream = con.getErrorStream();
                          // Process the response
                          BufferedReader reader;
                          String line = null;
                          StringBuilder sb=new StringBuilder();
                          System.out.println(logHeader+" :: inputStream "+inputStream);
                          
                          reader = new BufferedReader( new InputStreamReader( inputStream ) );
                           System.out.println(logHeader+" :: reader "+reader);
                          while( ( line = reader.readLine() ) != null ){
                            sb.append(line);
                          }
                           System.out.println(logHeader+" :: sb "+sb.toString());
                          inputStream.close();
                          con.disconnect();
                          System.out.println(logHeader+" :: Connection Disconnected ");
                          con=null;
                          url=null;
                          this.urlResponse.setResponseCode(responseCode);
                          this.urlResponse.setResponseData(sb.toString());
                    } catch (Exception e){
                         this.urlResponse.setResponseCode(-102);
                         this.urlResponse.setErrorMessage("Exception "+e.getMessage());
                    }

        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }*/
    
    public JcmsSSLClientResponse connectURL(){
        long reqTime=System.currentTimeMillis();
        if(this.getDestURL().equalsIgnoreCase("NA")){
            //error invalid URL
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Dest URL");
        }else if(this.isUseAuthTokenInBasicAuth() && (this.getAuthToken().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Auth Token details");
        }else if(this.isUseHostNameInHeader() && (this.getHostName().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid HOST Name Header");
        }else if(this.isUseBasicAuthentication() && (this.getAuthUser().equalsIgnoreCase("NA") || this.getAuthPwd().equalsIgnoreCase("NA"))){
            //Invalid Authentication details
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Authentication details");
        }else if(this.getCertificateKeystorePath().equalsIgnoreCase("NA") || this.getTrustStorePassword().equalsIgnoreCase("NA")){
            //Invalid Keystore path
            this.urlResponse.setResponseCode(-101);
            this.urlResponse.setErrorMessage("Invalid Certificate file path or truststore password");
        }else{
            try{
                  Properties systemProps = System.getProperties();
                  systemProps.put( "javax.net.debug", "ssl,keymanager");
                  //systemProps.put("jdk.tls.trustNameService", "true");
                  System.setProperty("javax.net.ssl.trustStore", this.getCertificateKeystorePath());
                  System.setProperty("javax.net.ssl.trustStorePassword", this.getTrustStorePassword());
                  System.setProperties(systemProps);
                  
                  URL url = new URL( this.getDestURL() );
                  HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
                  con.setRequestProperty( "Connection", "close" );
                    con.setDoInput(true);
                    con.setDoOutput(true);
                    con.setUseCaches(false);
                    con.setConnectTimeout( this.getConnectionTimeoutInSecond()*1000 );
                    con.setReadTimeout( this.getReadTimeoutInSecond()*1000 );
                    con.setRequestMethod( this.getHttpMethod().toUpperCase().trim() );
                    con.setRequestProperty( "Content-Type", this.getContentType() );
                    con.setRequestProperty("Accept", this.getAcceptDataFormat());
                    if(this.isUseHostNameInHeader())
                        con.setRequestProperty("HOST", this.getHostName());
                    //set basic auth parameters
                    if(this.isUseAuthTokenInBasicAuth()){
                        con.setRequestProperty("Authorization", "Bearer " + this.getAuthToken().trim()); 
                    }else if(this.isUseBasicAuthentication()){
                       String basicAuthStr=this.getAuthUser()+":"+this.getAuthPwd();
                        String encoding =new String( Base64.encodeBase64(basicAuthStr.getBytes()));
                        con.setRequestProperty("Authorization", "Basic " + encoding);  
                    }
                    if(this.getRequestData().length()>0){
                        OutputStream outputStream = con.getOutputStream();
                        outputStream.write(this.getRequestData().getBytes());
                        outputStream.close();
                    }
                    int responseCode = con.getResponseCode();
                    InputStream inputStream;
                    if (responseCode == HttpURLConnection.HTTP_OK) 
                      inputStream = con.getInputStream();
                    else
                      inputStream = con.getErrorStream();
                    // Process the response
                    BufferedReader reader;
                    String line = null;
                    StringBuilder sb=new StringBuilder();
                    reader = new BufferedReader( new InputStreamReader( inputStream ) );
                    while( ( line = reader.readLine() ) != null ){
                      sb.append(line).append("\n\r");
                    }
                    
                    inputStream.close();
                    this.urlResponse.setResponseCode(responseCode);
                    this.urlResponse.setResponseData(sb.toString());
            }catch(Exception e){
                this.urlResponse.setResponseCode(-102);
                this.urlResponse.setErrorMessage("Exception "+e.getMessage());
            }
            //919999188114
        }
        this.urlResponse.setResponseTime((int)(System.currentTimeMillis()-reqTime));
        return this.getUrlResponse();
    }
    
   
    

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public boolean isUseHostNameInHeader() {
        return useHostNameInHeader;
    }

    public void setUseHostNameInHeader(boolean useHostNameInHeader) {
        this.useHostNameInHeader = useHostNameInHeader;
    }

    public String getTrustStorePassword() {
        return trustStorePassword;
    }

    public void setTrustStorePassword(String trustStorePassword) {
        this.trustStorePassword = trustStorePassword;
    }

    public boolean isUseAuthTokenInBasicAuth() {
        return useAuthTokenInBasicAuth;
    }

    public void setUseAuthTokenInBasicAuth(boolean useAuthTokenInBasicAuth) {
        this.useAuthTokenInBasicAuth = useAuthTokenInBasicAuth;
    }
    
    
    
    
    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    
    public JcmsSSLClientResponse getUrlResponse() {
        return urlResponse;
    }

    public void setUrlResponse(JcmsSSLClientResponse urlResponse) {
        this.urlResponse = urlResponse;
    }

    
    
    public String getDestURL() {
        return destURL;
    }

    public void setDestURL(String destURL) {
        this.destURL = destURL;
    }

    public String getAcceptDataFormat() {
        return acceptDataFormat;
    }

    public void setAcceptDataFormat(String acceptDataFormat) {
        this.acceptDataFormat = acceptDataFormat;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getConnectionTimeoutInSecond() {
        return connectionTimeoutInSecond;
    }

    public void setConnectionTimeoutInSecond(int connectionTimeoutInSecond) {
        this.connectionTimeoutInSecond = connectionTimeoutInSecond;
    }

    public int getReadTimeoutInSecond() {
        return readTimeoutInSecond;
    }

    public void setReadTimeoutInSecond(int readTimeoutInSecond) {
        this.readTimeoutInSecond = readTimeoutInSecond;
    }

    public String getAuthUser() {
        return authUser;
    }

    public void setAuthUser(String authUser) {
        this.authUser = authUser;
    }

    public String getAuthPwd() {
        return authPwd;
    }

    public void setAuthPwd(String authPwd) {
        this.authPwd = authPwd;
    }

    public boolean isUseBasicAuthentication() {
        return useBasicAuthentication;
    }

    public void setUseBasicAuthentication(boolean useBasicAuthentication) {
        this.useBasicAuthentication = useBasicAuthentication;
    }

    public String getCertificateKeystorePath() {
        return certificateKeystorePath;
    }

    public void setCertificateKeystorePath(String certificateKeystorePath) {
        this.certificateKeystorePath = certificateKeystorePath;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
    
    public class JcmsSSLClientResponse{
        int responseCode;
        String responseData;
        String errorMessage;
        String processLogs;
        int responseTime;//In Milli Second
        public JcmsSSLClientResponse(){}

        
        public String getProcessLogs() {
			return processLogs;
		}


		public void setProcessLogs(String processLogs) {
			this.processLogs = processLogs;
		}


		public int getResponseTime() {
            return responseTime;
        }

        public void setResponseTime(int responseTime) {
            this.responseTime = responseTime;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public void setResponseCode(int responseCode) {
            this.responseCode = responseCode;
        }

        public String getResponseData() {
            return responseData;
        }

        public void setResponseData(String responseData) {
            this.responseData = responseData;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }
        
    }
}
