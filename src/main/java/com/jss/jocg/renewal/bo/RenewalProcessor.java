package com.jss.jocg.renewal.bo;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;

public class RenewalProcessor {

	 @Value("${jocg.renewal.transid.template}")
	 public String jocgRenewalTransIdTemplate;
	 
	 @Value("${ssl.keystore}")
		public String keyStoreFile;
		
		@Value("${keystore.pwd}")
		public String keyStorePassword;
	 
	@Autowired SubscriberRepository subscriberDao;
	@Autowired SubscriptionManager subMgr;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	 SimpleDateFormat idformat=new SimpleDateFormat("ddMM");
	 
		protected String generateUniqueId(Integer operatorId,Integer adnetworkId){
			String idTemplate="";
			try{
			idTemplate=this.jocgRenewalTransIdTemplate;
			String uniqNumber=idformat.format(new java.util.Date(System.currentTimeMillis()))+jocgCache.getTransactionSequence(operatorId, 7);
			String adnStr=""+adnetworkId.intValue();
			while(adnStr.length()<3) adnStr="0"+adnStr;
		
			idTemplate=idTemplate.replaceAll("<ADNETWORK>", adnStr);
			
			idTemplate=idTemplate.replaceAll("<TRANSID>", uniqNumber);
//			if(operatorId==3){
//				idTemplate=idTemplate.substring(idTemplate.lastIndexOf("_")+1);
//			}
			}catch(Exception e){
				e.printStackTrace();
				
			}
			
			System.out.println("Unique Id Generated "+idTemplate);
			return idTemplate;
		}
	
}
