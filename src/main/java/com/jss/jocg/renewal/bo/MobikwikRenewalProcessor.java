package com.jss.jocg.renewal.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.charging.handlers.PayTMIndiaChargingHandler;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.MobikwikChargingParams;
import com.jss.jocg.charging.model.PaytmIndiaChargingParams;
import com.jss.jocg.charging.model.parsers.MobikwikResponse;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;
import com.jss.jocg.util.JocgString;
import com.paytm.pg.merchant.CheckSumServiceHelper;

@Component
public class MobikwikRenewalProcessor extends RenewalProcessor{
	private static final Logger logger = LoggerFactory
			.getLogger(MobikwikRenewalProcessor.class);
	
	SimpleDateFormat sdfnew=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@JmsListener(destination = "renewal.mobikwik", containerFactory = "myFactory")
	public void processRenewal(Subscriber sub){
		
		String logHeader="RENEWAL::MOBIKWIK::";
		if(sub!=null ) logHeader +="{subid="+sub.getSubscriberId()+", msisdn="+sub.getMsisdn()+"} ::";
		List<ChargingUrl> chUrls=jocgCache.getChargingUrlByChintfId(sub.getChargingInterfaceId());
		logger.debug(logHeader+" URL "+chUrls);
		ChargingUrl churl=null;
		for(ChargingUrl ch:chUrls) if("SDP-CHARGE".equalsIgnoreCase(ch.getUrlCatg())) {churl=ch;break;}
		logger.debug(logHeader+" URL "+churl);
		
		try{
		if(churl!=null && churl.getChurlId()>0){
			String accessToken=sub.getCustomerId();
			accessToken=(accessToken==null||accessToken.length()<=0)?"NA":accessToken;
			List<ChargingUrlParam> churlParams=jocgCache.getChargingUrlParams(churl.getChurlId());
			
			MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
			String msisdnStr=""+sub.getMsisdn().longValue();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			mobikwikParams.setAmount(sub.getOperatorPricePoint().intValue());
			mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
			String renewalTransId=generateUniqueId(sub.getChargingInterfaceId(),sub.getTrafficSourceId());
			//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
			mobikwikParams.extractParams(logger, logHeader, sub.getOperatorPricePoint(), "", renewalTransId, accessToken,renewalTransId, churlParams);
			String dataTemplate=mobikwikParams.generateChecksum(logger, logHeader, MobikwikChargingParams.RQTYPE_CHARGE, churl.getDataTemplate());
			
			System.out.println(logHeader+"URL To Invoke "+churl.getUrl());
			System.out.println(logHeader+"URL Data "+dataTemplate);
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/url-encoded");
			sslClient.setHttpMethod("POST");
			sslClient.setContentType("application/x-www-form-urlencoded");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setDestURL(churl.getUrl());
			sslClient.setRequestData(dataTemplate);
			sslClient.setConnectionTimeoutInSecond(10);
			sslClient.setReadTimeoutInSecond(20);
			System.out.println(logHeader+"Invoking URL....... ");
			
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(logHeader);
			MobikwikResponse mp=new MobikwikResponse();
			mp.parseXML(resp.getResponseData());
			System.out.println(logHeader+"Mobikwik Response "+mp);
			String respData=resp.getResponseData();
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(System.currentTimeMillis());
				cal.add(Calendar.DAY_OF_MONTH, sub.getValidity());
				sub.setValidityEndDate(cal.getTime());
				sub.setNextRenewalDate(cal.getTime());
				sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
				sub=subMgr.save(sub);
				buildCDR(logHeader,sub,renewalTransId,JocgStatusCodes.CHARGING_SUCCESS,resp.getResponseData());
				logger.debug(logHeader+"Renewal Successful for subId"+sub.getId()+", msisdn "+sub.getMsisdn()+"|MobikwikResponse "+resp.getResponseData()+"|Update Query :db.subscriber.update({msisdn:"+sub.getMsisdn()+"},{$set:{validityEndDate:ISODate('"+sdfnew.format(cal.getTime())+"'),nextRenewalDate:ISODate('"+sdfnew.format(cal.getTime())+"')}},{ multi: false,upsert: false})");
			}else{
				logger.debug(logHeader+"Renewal Failed for subId"+sub.getId()+", msisdn "+sub.getMsisdn()+"|MobikwikResponse "+resp.getResponseData());
			}
			
			
			
		}
		}catch(Exception e){
			logger.debug(logHeader+" Exception : "+e);
		}
		
		
		
		
	}
	
	public void buildCDR(String logHeader,Subscriber sub,String transId,Integer status,String statusDescp){
		logHeader +="buildCDR ::";
		try{
			logger.debug(logHeader+"Method Invoked ..");
		ChargingInterface sourceNetwork=jocgCache.getChargingInterface(sub.getSourceNetworkId());
		logger.debug(logHeader+"sourceNetwork "+sourceNetwork);
		ChargingInterface chintf=jocgCache.getChargingInterface(sub.getChargingInterfaceId());
		logger.debug(logHeader+"chintf "+chintf);
		PackageChintfmap pkgchintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
		logger.debug(logHeader+"pkgchintf "+pkgchintf);
		Circleinfo circle=jocgCache.getCircleInfo(sub.getCircleId());
		if(circle==null) circle=new Circleinfo();
		logger.debug(logHeader+"circle "+circle);
		PackageDetail pkg=jocgCache.getPackageDetailsByName(sub.getPackageName());
		logger.debug(logHeader+"pkg "+pkg);
		Service service=jocgCache.getServicesById(pkg.getServiceid());
		logger.debug(logHeader+"service "+service);
		ChargingPricepoint chppmaster=jocgCache.getPricePointListById(sub.getPricepointId());
		logger.debug(logHeader+"service "+chppmaster);
		ChargingPricepoint chpp=chppmaster;//must be changed when fallback is implemented
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		java.util.Date chargingDate=cal.getTime();
		cal.add(Calendar.DATE, chpp.getValidity());
		java.util.Date validityEndDate=cal.getTime();
		TrafficSource ts=null;TrafficCampaign tc=null;
		try{
		ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
		logger.debug(logHeader+"traffic Source "+ts);
		tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
		logger.debug(logHeader+"traffic Campaign "+tc);
		}catch(Exception e1){
			logger.error(logHeader+"Exception while searching traffic source id "+sub.getTrafficSourceId()+", and campaign "+sub.getCampaignId());
		}
		ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), transId, "SYSTEM-REN", "REN", sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
				new JocgRequest(), sourceNetwork, chintf, pkgchintf, circle, pkg, service, chppmaster, chpp, null, "WALLET", 0D, chpp.getPricePoint(), "SYSTEM-RENEWAL", 
				chargingDate, sub.getValidityStartDate(), validityEndDate, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
				null, null, null, -1, "NA", JocgStatusCodes.CHARGING_SUCCESS, "Renewal Successful", new java.util.Date(System.currentTimeMillis()), "NA", "NA", "NA", new java.util.Date(System.currentTimeMillis()), ""+status, statusDescp, "NA", ""+sub.getMsisdn(), new java.util.Date(), 
				null, null, false, false, false);
		logger.debug(logHeader+"Inserting new Renewal CDR......");
		newCDR=cdrBuilder.insert(newCDR);
		logger.info(logHeader+"buildCDR():: Renewal CDR Created with ID "+newCDR.getId());
		}catch(Exception e){
			logger.error(logHeader+"buildCDR()::"+e.getMessage());
		}
	
	}
	
	public String calculateCheckSum(String paytmChecksumKey,String urlData){
		String checkSum="";
		try{
		urlData=urlData.substring(0,urlData.lastIndexOf("&"));
		List<String> strList=JocgString.convertStrToList(urlData, "&");
		TreeMap t=new TreeMap();
		for(String str:strList){
			try{
				
				String key=str.substring(0, str.indexOf("="));
				String value=str.substring(str.indexOf("=")+1);
				logger.debug(key+"="+value);
			t.put(key, value);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		logger.debug("Paytm TreeMap "+t);
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		 
		checkSum=checksumHelper.genrateCheckSum(paytmChecksumKey, t);
		}catch(Exception e1){
			checkSum="";
			e1.printStackTrace();
		}
		return checkSum;
		
	}
	
}
