package com.jss.jocg.sms.data;

import java.lang.reflect.Field;

public class JocgSMSMessage {
Long msisdn;
Integer  chintfId;
String smsInterface;
String textMessage;
String serviceName;
String chargingInterfaceType;

public JocgSMSMessage(){}

@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder("JocgSMSMessage{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}
public Long getMsisdn() {
	return msisdn;
}

public void setMsisdn(Long msisdn) {
	this.msisdn = msisdn;
}

public Integer getChintfId() {
	return chintfId;
}

public void setChintfId(Integer chintfId) {
	this.chintfId = chintfId;
}

public String getSmsInterface() {
	return smsInterface;
}

public void setSmsInterface(String smsInterface) {
	this.smsInterface = smsInterface;
}

public String getTextMessage() {
	return textMessage;
}

public void setTextMessage(String textMessage) {
	this.textMessage = textMessage;
}

public String getServiceName() {
	return serviceName;
}

public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}

public String getChargingInterfaceType() {
	return chargingInterfaceType;
}

public void setChargingInterfaceType(String chargingInterfaceType) {
	this.chargingInterfaceType = chargingInterfaceType;
}

	
}
