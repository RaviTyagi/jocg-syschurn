package com.jss.jocg.sms.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;

@Component
public class SMSPushService {
	private static final Logger logger = LoggerFactory
			.getLogger(SMSPushService.class);
	
	@Autowired JocgCacheManager jocgCache;
	 @Value("${ssl.keystore}")
		public String keyStoreFile;
		
		@Value("${keystore.pwd}")
		public String keyStorePassword;
	
		//Idea SMS Templates
		/**
		smsjust.idea.data=mobtv:48972,news:48972,kids:48973,flix:55826
		smsjust.idea.wallet=mobtv:48972,news:48972,kids:48973,flix:55826
		 */
		@Value("${smsjust.idea.data}")
		public String ideaDataSMSTemplates;
		
		@Value("${smsjust.idea.wallet}")
		public String ideaWalletSMSTemplates;
		
		@Value("${smsjust.voda.data}")
		public String vodaDataSMSTemplates;
		
		@Value("${smsjust.voda.wallet}")
		public String vodaWalletSMSTemplates;
		
		@Value("${smsjust.connect.data}")
		public String connectDataSMSTemplates;
		
		@Value("${smsjust.connect.wallet}")
		public String connectWalletSMSTemplates;
		
		
	@JmsListener(destination = "notify.sms", containerFactory = "myFactory")
	 public void receiveMessage(JocgSMSMessage smsData) {
		logger.debug("Received SMS Message to send via interface "+smsData.getSmsInterface());
		if(smsData!=null && "ideain".equalsIgnoreCase(smsData.getSmsInterface())){
			sendIdeaIndiaSMS(smsData);
		}else if(smsData!=null && "vodafonein".equalsIgnoreCase(smsData.getSmsInterface())){
			sendVodaIndiaSMS(smsData);
		}else if(smsData!=null && ("smsjust".equalsIgnoreCase(smsData.getSmsInterface()) || 
				"smsaircelin".equalsIgnoreCase(smsData.getSmsInterface()) ||
				"smspyrobsnl".equalsIgnoreCase(smsData.getSmsInterface())||
				"smsnetexcelbsnl".equalsIgnoreCase(smsData.getSmsInterface()))){
			sendMessageViaSMSJust(smsData);
		}else if(smsData!=null && "airtelin".equalsIgnoreCase(smsData.getSmsInterface())){
			sendAirtelIndiaSms(smsData);
		}else if(smsData!=null && "smsconnect".equalsIgnoreCase(smsData.getSmsInterface())){
			sendConnectIndiaSMS(smsData);
		}
	}
	
	private void sendConnectIndiaSMS(JocgSMSMessage smsData){
		String logHeader="SMSPush(ConnectIndia):"+smsData.getMsisdn()+"::";
		String smsTemplate="";
		try{
			String templateData=("WALLET".equalsIgnoreCase(smsData.getChargingInterfaceType()))?connectDataSMSTemplates:connectWalletSMSTemplates;
			String[] tempArr=templateData.split(",");
			for(String s:tempArr){
				if(smsData.getServiceName().toLowerCase().contains(s.substring(0, s.indexOf(":")))){
					smsTemplate=s.substring(s.indexOf(":")+1);
				}
			}
		}catch(Exception e){}
		if(smsTemplate!=null && smsTemplate.length()>0){
		String url="http://www.smsjust.com/blank/sms/user/urlsmstemp.php?username=zorawer.aujla@digivive.com&pass=123456&senderid=NEXGTV&dest_mobileno="+smsData.getMsisdn()+"&tempid="+smsTemplate+"&F1=Dominos-NEXT100&F2=Koovs-HELLO500&F3=&response=Y";
		logger.debug("Invoking URL "+url);
				JcmsSSLClient sslClient=new JcmsSSLClient();
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader,url);
		logger.debug(logHeader+"URL Resp"+resp.getResponseCode()+"|"+resp.getResponseData()+"|"+resp.getErrorMessage());
		}else{
			logger.error(logHeader+"SMS Template for service "+smsData.getServiceName()+" not defined!");
		}
	}
	
	
	private void sendIdeaIndiaSMS(JocgSMSMessage smsData){
		String logHeader="SMSPush(IdeaIndia):"+smsData.getMsisdn()+"::";
		String smsTemplate="";
		try{
			String templateData=("WALLET".equalsIgnoreCase(smsData.getChargingInterfaceType()))?ideaWalletSMSTemplates:ideaDataSMSTemplates;
			String[] tempArr=templateData.split(",");
			for(String s:tempArr){
				if(smsData.getServiceName().toLowerCase().contains(s.substring(0, s.indexOf(":")))){
					smsTemplate=s.substring(s.indexOf(":")+1);
				}
			}
		}catch(Exception e){}
		if(smsTemplate!=null && smsTemplate.length()>0){
		String url="http://www.smsjust.com/blank/sms/user/urlsmstemp.php?username=zorawer.aujla@digivive.com&pass=123456&senderid=NEXGTV&dest_mobileno="+smsData.getMsisdn()+"&tempid="+smsTemplate+"&F1=Dominos-NEXT100&F2=Koovs-HELLO500&F3=&response=Y";
		logger.debug("Invoking URL "+url);
				JcmsSSLClient sslClient=new JcmsSSLClient();
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader,url);
		logger.debug(logHeader+"URL Resp"+resp.getResponseCode()+"|"+resp.getResponseData()+"|"+resp.getErrorMessage());
		}else{
			logger.error(logHeader+"SMS Template for service "+smsData.getServiceName()+" not defined!");
		}
	}
	
	private void sendVodaIndiaSMS(JocgSMSMessage smsData){
		String logHeader="SMSPush(VodaIndia):"+smsData.getMsisdn()+"::";
		String smsTemplate="";
		try{
			String templateData=("WALLET".equalsIgnoreCase(smsData.getChargingInterfaceType()))?vodaWalletSMSTemplates:vodaDataSMSTemplates;
			String[] tempArr=templateData.split(",");
			for(String s:tempArr){
				if(smsData.getServiceName().toLowerCase().contains(s.substring(0, s.indexOf(":")))){
					smsTemplate=s.substring(s.indexOf(":")+1);
				}
			}
		}catch(Exception e){}
		if(smsTemplate!=null && smsTemplate.length()>0){
		String url="http://www.smsjust.com/blank/sms/user/urlsmstemp.php?username=zorawer.aujla@digivive.com&pass=123456&senderid=NEXGTV&dest_mobileno="+smsData.getMsisdn()+"&tempid="+smsTemplate+"&F1=Dominos-NEXT100&F2=Koovs-HELLO500&F3=&response=Y";
		logger.debug("Invoking URL "+url);
				JcmsSSLClient sslClient=new JcmsSSLClient();
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader,url);
		logger.debug(logHeader+"URL Resp"+resp.getResponseCode()+"|"+resp.getResponseData()+"|"+resp.getErrorMessage());
		}else{
			logger.error(logHeader+"SMS Template for service "+smsData.getServiceName()+" not defined!");
		}
	}
	
	
	private void sendMessageViaSMSJust(JocgSMSMessage smsData){
		String logHeader="SMSPush:"+smsData.getMsisdn()+"::";
		String url="http://www.smsjust.com/blank/sms/user/urlsmstemp.php?username=zorawer.aujla@digivive.com&pass=123456&senderid=NEXGTV&dest_mobileno="+smsData.getMsisdn()+"&tempid=48816&F1=Dominos-NEXT100&F2=Koovs-HELLO500&F3=&response=Y";
		logger.debug("Invoking URL "+url);
				JcmsSSLClient sslClient=new JcmsSSLClient();
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader,url);
		logger.debug(logHeader+"URL Resp"+resp.getResponseCode()+"|"+resp.getResponseData()+"|"+resp.getErrorMessage());
	}
	
	private void sendAirtelIndiaSms(JocgSMSMessage smsData){
		String logHeader="sendAirtelIndiaSms()::{"+smsData.getMsisdn()+"}";
		if(smsData.getServiceName()!=null && "MNM".equalsIgnoreCase(smsData.getServiceName()) ){
			try{
				
			//https://mbnb.airtelworld.com:8443/bharti?login=digivive&pass=vivedigi&sms=
			String url="https://mbnb.airtelworld.com:8443/bharti?login=digivive&pass=vivedigi&msisdn="+smsData.getMsisdn()+"&src=A$-DGLIVE&type=text&sms="+java.net.URLEncoder.encode(jocgCache.getSMSTemplate("airtelin"),"utf8");
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setHttpMethod("POST");
			sslClient.setContentType("text/plain");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setHttpMethod("GET");
			sslClient.setDestURL(url);
			sslClient.setCertificateKeystorePath(this.keyStoreFile);
			sslClient.setTrustStorePassword(this.keyStorePassword);
			JcmsSSLClientResponse urlResp=sslClient.connectURL();
			logger.debug(logHeader+" URL Response Code : "+urlResp.getResponseCode());
			logger.debug(logHeader+" URL Response Data : "+urlResp.getResponseData());
			logger.debug(logHeader+" URL Response Error : "+urlResp.getErrorMessage());
			}catch(Exception e){
				logger.error(logHeader+" Exception "+e);
			}
		}else{
			logger.debug(logHeader+" Ignored SMS request for service : "+smsData.getServiceName());
		}
	
	}
	
}
