package com.jss.jocg;

import javax.jms.ConnectionFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@SpringBootApplication
@EnableScheduling
@EnableJms
@ComponentScan(basePackages = { "com.jss.jocg.web","com.jss.jocg.model","com.jss.jocg","com.jss.jocg.services","com.jss.jocg.services.config",
		"com.jss.jocg.services.bo","com.jss.jocg.services.dao","com.jss.jocg.services.data",
		"com.jss.jocg.cache.entities","com.jss.jocg.cache.entities.config",
		"com.jss.jocg.cache.dao","com.jss.jocg.cache.data","com.jss.jocg.cache.bo",
		"com.jss.jocg.cache.service","com.jss.jocg.renewal.bo","com.jss.jocg.sms.bo","com.jss.jocg.services.scheduled"})
public class JocgApplication {

	
	@Bean
    public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
                                                    DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.
        return factory;
    }
	
	@Bean // Serialize message content to json using TextMessage
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_type");
        return converter;
    }

	
	public static void main(String[] args) {
		
		SpringApplication app = new SpringApplication(JocgApplication.class);
		app.setBannerMode(org.springframework.boot.Banner.Mode.OFF);
		app.run(args);
		
	}
}
