package com.jss.jocg.cache.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;

@Component
public class CacheTrafficStatsReloader {
	private static final Logger logger = LoggerFactory
			.getLogger(CacheReloader.class);
	
	@Autowired JocgCacheManager jocgCache;
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	//   @Scheduled(initialDelay=1, fixedRate=60000)
	   public void reloadCache() {
		   Boolean flag=false;
			try{
			flag=jocgCache.reloadStats();
			}catch(Exception e){
				flag=false;
				e.printStackTrace();
				
			}
			System.out.println("Traffic Cache Updated at time {"+dateFormat.format(new Date())+"} status "+flag);
			logger.info("Traffic Cache Updated at time {"+dateFormat.format(new Date())+"} status "+flag);
		   
	   }
	   
	   
	   
}
