package com.jss.jocg.cache.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;


@Controller
@RequestMapping("/cache")
public class CacheService {

	@Autowired
	JocgCacheManager jocgCache;
	
	
	/**
	 * Methods for Accessing Cached Data for Charging Interfaces
	 * @param chargingIntegrfaceId
	 * @return
	 */
	@RequestMapping("/trafficstats/{chargingIntegrfaceId}")
	public @ResponseBody TrafficStats getTrafficStats(@PathVariable Integer chargingIntegrfaceId, @RequestParam("tsid") Integer tsourceId,
			@RequestParam("cmpid") Integer campaignId,@RequestParam("pubname") String publisherId){
		return jocgCache.getTrafficStats(chargingIntegrfaceId, tsourceId, campaignId, publisherId);
	}
	
	@RequestMapping("/chintf/{chargingIntegrfaceId}")
	public @ResponseBody ChargingInterface getChargingInterfaceById(@PathVariable Integer chargingIntegrfaceId){
		return jocgCache.getChargingInterface(chargingIntegrfaceId);
	}
	
	@RequestMapping("/chintf/all")
	public @ResponseBody  Map<Integer, ChargingInterface> getChargingInterfaceFullCache(){
		return jocgCache.getChargingInterface();
	}
	
	
	/**
	 * Methods for Accessing Cached Data of scheduled cg images
	 * @param chargingIntegrfaceId
	 * @return
	 */
	
	@RequestMapping("/cgimage/{chargingIntegrfaceId}")
	public @ResponseBody Map<Integer,CgimageSchedule> getScheduledImageById(@PathVariable Integer chargingIntegrfaceId,@RequestParam Integer tsourceId){
			return jocgCache.getScheduledImages(chargingIntegrfaceId,tsourceId);
		
	}
	
	@RequestMapping("/cgimage/all")
	public @ResponseBody Map<String, Map<Integer, CgimageSchedule>> getScheduledImageByStatus(){
		return jocgCache.getScheduledImages();
	}
	
	/**
	 * Methods for Accessing Cached Data of Charging PricePoints
	 * @param chargingIntegrfaceId
	 * @return
	 */
	
	@RequestMapping("/chpricepoint/{chargingPricePointId}")
	public @ResponseBody ChargingPricepoint getPricePointById(@PathVariable Integer chargingPricePointId){
			return jocgCache.getPricePointListById(chargingPricePointId);
		
	}
	
	@RequestMapping("/choperator/{packId}")
	public @ResponseBody PackageDetail getPackById(@PathVariable Integer packId){
		System.out.println("PackageDetail : "+packId);
			return jocgCache.getPackageDetailsByID(packId);
			}
	
	@RequestMapping("/chpricepoint/all")
	public @ResponseBody  Map<Integer, ChargingPricepoint> getPricePointByStatus(){
		return jocgCache.getPricePointList();
	}
	
	@RequestMapping("/chpricepoint")
	public @ResponseBody  ChargingPricepoint getPricePointByKey(@RequestParam("opkey") String operatorKey,@RequestParam("pp") Double pricePoint){
		ChargingPricepoint cpp= jocgCache.getPricePointListByOperatorKey(operatorKey, pricePoint);
		System.out.println("cpp     ..... "+cpp);
		return cpp;
	}
	
	@RequestMapping("/chpricepoint/chintf/{chintfId}")
	public @ResponseBody  List<ChargingPricepoint> getPricePointByChargingInterface(@PathVariable Integer chintfId){
		return jocgCache.getPricePointListByChargingInterface(chintfId);
	}
	
	/**
	 * Methods for Accessing Cached Data of Charging Urls
	 * @param chargingIntegrfaceId
	 * @return
	 */
	
	@RequestMapping("/churls/{churlId}")
	public @ResponseBody ChargingUrl getChargingUrlById(@PathVariable Integer churlId){
			return jocgCache.getChargingUrlById(churlId);
		
	}
	
	@RequestMapping("/churls/all")
	public @ResponseBody  Map<Integer, ChargingUrl> getChargingUrls(){
		return jocgCache.getChargingUrls();
	}
	
	@RequestMapping("/churls/chintf/{chintfId}")
	public @ResponseBody  List<ChargingUrl> getChargingUrlsByChargingInterface(@PathVariable Integer chintfId){
		return jocgCache.getChargingUrlByChintfId(chintfId);
	}
	
	/**
	 * Methods for Accessing Cached Data of Package Charging Interface mapping
	 * @param pkgChintfMapId
	 * @return
	 */
	@RequestMapping("/pkgchintfmap/{pkgChintfMapId}")
	public @ResponseBody PackageChintfmap getPackageChaintfMapById(@PathVariable Integer pkgChintfMapId){
			return jocgCache.getPackageChargingInterfaceMapById(pkgChintfMapId);
		
	}
	
	@RequestMapping("/pkgchintfmap/all")
	public @ResponseBody Map<Integer,PackageChintfmap> getPackageChaintfMap(){
			return jocgCache.getPackageChargingInterfaceMap();
		
	}
	
	@RequestMapping("/pkgchintfmap")
	public @ResponseBody PackageChintfmap getPricePointByKey(@RequestParam("network") Integer sourceNetworkId,@RequestParam("chintfid") Integer chintfId,@RequestParam("pkgid") Integer packageId,@RequestParam("opppid") Integer opPPId){
		return jocgCache.getPackageChargingInterfaceMap(sourceNetworkId,chintfId, packageId, opPPId);
	}
	
	@RequestMapping("/pkgs")
	public @ResponseBody PackageDetail getPackageChargingInterfaceMap(@RequestParam("network") Integer sourceNetworkId,@RequestParam("opppid") Double opPPId){
	System.out.println("Call Land in Find Packages");
		return jocgCache.getPackageChargingInterfaceMap(sourceNetworkId, opPPId);
	}
	/**
	 * Methods for Accessing Cached Data of Package Details
	 * @param pkgName
	 * @return
	 */
	@RequestMapping("/pkgs/{pkgName}")
	public @ResponseBody PackageDetail getPackageDetailByName(@PathVariable String pkgName){
			return jocgCache.getPackageDetailsByName(pkgName);
		
	}
	
	@RequestMapping("/pkgs/all")
	public @ResponseBody Map<String,PackageDetail> getPackageDetail(){
			return jocgCache.getPackageDetails();
		
	}
	
	/**
	 * Methods for Accessing Cached Data of Services
	 * @param serviceId
	 * @return
	 */
	@RequestMapping("/services/{serviceId}")
	public @ResponseBody Service getServicesByName(@PathVariable Integer serviceId){
			return jocgCache.getServicesById(serviceId);
		
	}
	
	@RequestMapping("/services/all")
	public @ResponseBody Map<Integer,Service> getServices(){
			return jocgCache.getServices();
		
	}
	
	/**
	 * Methods for Accessing Cached Data of TrafficCampaign
	 * @param campaignId
	 * @return
	 */
	@RequestMapping("/campaign/{campaignId}")
	public @ResponseBody TrafficCampaign getCampaignById(@PathVariable Integer campaignId){
			return jocgCache.getTrafficCampaignById(campaignId);
		
	}
	
	@RequestMapping("/campaign/all")
	public @ResponseBody Map<Integer,TrafficCampaign> getTrafficCampaigns(){
			return jocgCache.getTrafficCampaigns();
		
	}
	
	/**
	 * Methods for Accessing Cached Data of TrafficSource
	 * @param tsourceId
	 * @return
	 */
	@RequestMapping("/tsource/{tsourceId}")
	public @ResponseBody TrafficSource getTrafficSourcesById(@PathVariable Integer tsourceId){
			return jocgCache.getTrafficSourceById(tsourceId);
		
	}
	
	@RequestMapping("/tsource/all")
	public @ResponseBody Map<Integer,TrafficSource> getTrafficSources(){
			return jocgCache.getTrafficSources();
		
	}
	
	/**
	 * Methods for Accessing Cached Data of TrafficSource
	 * @param tsourceId
	 * @return
	 */
	@RequestMapping("/voucher/{voucherCode}")
	public @ResponseBody VoucherDetail getVoucherByCode(@PathVariable String voucherCode){
			return jocgCache.getVoucherDetails(voucherCode);
		
	}
	
//	@RequestMapping("/voucher/all")
//	public @ResponseBody Map<String,VoucherDetail> getVoucherDetails(){
//			return jocgCache.getVoucherDetails();
//		
//	}
	@RequestMapping("/reload")
	public @ResponseBody Boolean rebuildCache(){
		return jocgCache.rebuildCache();
	}
	
}
