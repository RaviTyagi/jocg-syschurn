/**
 * 
 */
package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.ChargingPricepoint;


/**
 * @author Rishi Tyagi
 *
 */
public interface ChargingPricepointDao extends CrudRepository<ChargingPricepoint, Integer>{
//	public ChargingPricepoint getByppid();
//	public List<ChargingPricepoint> getByChintfid();
//	public List<ChargingPricepoint> getByOperatorKey();
//	public List<ChargingPricepoint> getByPricePoint(Integer chintfid,Double pricePoint);
	public List<ChargingPricepoint> getByStatus(Integer status);
}
