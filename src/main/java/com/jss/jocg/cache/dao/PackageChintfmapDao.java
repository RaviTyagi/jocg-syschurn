package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.PackageChintfmap;

public interface PackageChintfmapDao extends CrudRepository<PackageChintfmap, Integer> {
	public List<PackageChintfmap> findAll();
	public List<PackageChintfmap> getByStatus(Integer status);
	public List<PackageChintfmap> getByChintfid(Integer chintfid);
	
	@Query("select c from PackageChintfmap c where c.chintfid=:chintfid and c.pkgid=:pkgid")
	public List<PackageChintfmap> getByPkgid(Integer chintfid,Integer pkgid);
	
	@Query("select c from PackageChintfmap c where c.chintfid=:chintfid and c.pkgid=:pkgid and c.opPpid=:opPpid")
	public List<PackageChintfmap> getByopPpid(Integer chintfid,Integer pkgid,Integer opPpid);
}
