package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;


public interface LandingPageScheduleDao extends CrudRepository<LandingPageSchedule, Integer> {
	public LandingPageSchedule getByLandingPageId(Integer landingPageId);
	public List<LandingPageSchedule> getByChintfid(Integer chintfid);
	public List<LandingPageSchedule> getByStatus(Integer status);
}
