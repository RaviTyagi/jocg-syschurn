package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.CgimageSchedule;

public interface CGImageScheduleDao extends CrudRepository<CgimageSchedule, Integer> {
	public CgimageSchedule getByImgid(Integer imgid);
	public List<CgimageSchedule> getByStatus(Integer status);
	
}
