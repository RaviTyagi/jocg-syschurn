package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.NotificationConfig;


public interface NotificationConfigDao  extends CrudRepository<NotificationConfig, Integer> {
	public NotificationConfig getByNotifyConfigId(Integer notifyConfigId);
	public List<NotificationConfig> getByTsourceid(Integer tsourceid);
	public List<NotificationConfig> getByStatus(Integer status);
}

