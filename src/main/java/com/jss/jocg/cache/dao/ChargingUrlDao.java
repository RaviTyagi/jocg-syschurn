package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.ChargingUrl;

public interface ChargingUrlDao extends CrudRepository<ChargingUrl, Integer> {
	public ChargingUrl getByChurlId(Integer churlId);
	public ChargingUrl getByChintfid(Integer chintfid);
	public List<ChargingUrl> getByStatus(Integer status);
}
