package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.MsisdnSeries;

public interface MsisdnSeriesDao extends CrudRepository<MsisdnSeries, Integer> {
	public MsisdnSeries getByMsisdnSeriesId(Integer msisdnSeriesId);
	public List<MsisdnSeries> getByChintfid(Integer chintfid);
	public List<MsisdnSeries> getByStatus(Byte status);
}

