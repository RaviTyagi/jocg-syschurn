package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.Routing;


public interface RoutingDao extends CrudRepository<Routing, Integer> {
	public List<Routing> findAll();
	public List<Routing> getByStatus(byte status);
	
}