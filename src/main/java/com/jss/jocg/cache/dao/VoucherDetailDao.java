package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.VoucherDetail;

public interface VoucherDetailDao  extends CrudRepository<VoucherDetail, Integer> {
 public List<VoucherDetail> findAll();
 public List<VoucherDetail> getByStatus(Integer status);
 public VoucherDetail getByCouponid(Integer couponid);
 public VoucherDetail getByVoucherId(String voucherId);
}
