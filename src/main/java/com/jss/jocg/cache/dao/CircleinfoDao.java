package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.Circleinfo;

public interface CircleinfoDao extends CrudRepository<Circleinfo, Integer> {
	public Circleinfo getByCircleid(Integer circleid);
	public Circleinfo getByChintfid(Integer chintfid);
	public List<Circleinfo> getByStatus(Integer status);
}
