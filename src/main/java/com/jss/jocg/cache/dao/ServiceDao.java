/**
 * 
 */
package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.Service;

/**
 * @author Rishi Tyagi
 *
 */
public interface ServiceDao extends CrudRepository<Service, Integer> {
	public List<Service> findAll();
	public List<Service> getByStatus(Integer status);

}
