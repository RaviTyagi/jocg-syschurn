package com.jss.jocg.cache.dao;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.ChargingInterface;

public interface ChargingInterfaceDao  extends CrudRepository<ChargingInterface, Integer> {
		public ChargingInterface getByChintfid(Integer chintfid);
		public List<ChargingInterface> getByStatus(Integer status);
		public List<ChargingInterface> getByPchintfid(Integer pchintfid);
}
