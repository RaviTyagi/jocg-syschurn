package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.Platform;


public interface PlatformDao  extends CrudRepository<Platform, Integer> {
	public List<Platform> findAll();
	
}
