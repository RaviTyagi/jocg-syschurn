package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.ImsiSeries;

public interface ImsiSeriesDao extends CrudRepository<ImsiSeries, Integer> {
	public ImsiSeries getByImsiSeriesId(Integer imsiSeriesId);
	public List<ImsiSeries> getByChintfid(Integer chintfid);
	public List<ImsiSeries> getByStatus(Integer status);
}
