package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.ChargingUrlParam;

public interface ChargingUrlParamDao extends CrudRepository<ChargingUrlParam, Integer>{
	public ChargingUrlParam getByChurlParamId(Integer churlParamId);
	public List<ChargingUrlParam> getByChurlId(Integer churlId);
	public List<ChargingUrlParam> getByStatus(Integer status);
}
