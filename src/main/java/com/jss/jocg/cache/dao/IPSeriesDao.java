package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.IPSeries;


public interface IPSeriesDao  extends CrudRepository<IPSeries, Integer> {
	public IPSeries getByIpSeriesId(Integer ipSeriesId);
	public List<IPSeries> getByChintfid(Integer chintfid);
	public List<IPSeries> getByStatus(Integer status);
}
