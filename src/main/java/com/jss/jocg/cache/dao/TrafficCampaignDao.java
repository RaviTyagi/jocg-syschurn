/**
 * 
 */
package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.jss.jocg.cache.entities.config.TrafficCampaign;


/**
 * @author Rishi Tyagi
 *
 */
public interface TrafficCampaignDao extends CrudRepository<TrafficCampaign, Integer> {
public List<TrafficCampaign> findAll();
public List<TrafficCampaign> getByStatus(String status);
public TrafficCampaign getByCampaignid(Integer campaignid);
public TrafficCampaign getByCampaignname(String campaignname);
}
