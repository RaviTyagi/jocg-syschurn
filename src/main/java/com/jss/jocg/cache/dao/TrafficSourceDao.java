package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.TrafficSource;

public interface TrafficSourceDao  extends CrudRepository<TrafficSource, Integer> {
	public List<TrafficSource> findAll();
	public List<TrafficSource> getByStatus(Integer status);
	public TrafficSource getByTsourceid(Integer tsourceid);
	public TrafficSource getBySourcename(String sourcename);

}
