package com.jss.jocg.cache.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.jss.jocg.cache.entities.config.PackageDetail;

public interface PackageDetailDao extends CrudRepository<PackageDetail, Integer> {
	public List<PackageDetail> findAll();
	public List<PackageDetail> getByStatus(Integer status);
	public List<PackageDetail> getByPkgname(String pkgname);
}
