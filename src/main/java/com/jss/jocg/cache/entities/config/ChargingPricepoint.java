package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_charging_pricepoints database table.
 * 
 */
@Entity
@Table(name="tb_charging_pricepoints")
@NamedQuery(name="ChargingPricepoint.findAll", query="SELECT c FROM ChargingPricepoint c")
public class ChargingPricepoint implements Serializable {
	private static final long serialVersionUID = 1L;
	private int ppid;
	private int chintfid;
	private String cpid;
	private String ctype;
	private String currency;
	private int hasfallback;
	private int isfallback;
	private int mppid;
	private String opParams;
	private String operatorKey;
	private double pricePoint;
	private Date regDate;
	private int renewable;
	private int status;
	private int validity;
	private String validityType;

	public ChargingPricepoint() {
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getPpid() {
		return this.ppid;
	}

	public void setPpid(int ppid) {
		this.ppid = ppid;
	}


	@Column(nullable=false)
	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public String getCpid() {
		return this.cpid;
	}

	public void setCpid(String cpid) {
		this.cpid = cpid;
	}


	@Column(length=50)
	public String getCtype() {
		return this.ctype;
	}

	public void setCtype(String ctype) {
		this.ctype = ctype;
	}


	@Column(length=10)
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public int getHasfallback() {
		return this.hasfallback;
	}

	public void setHasfallback(int hasfallback) {
		this.hasfallback = hasfallback;
	}


	public int getIsfallback() {
		return this.isfallback;
	}

	public void setIsfallback(int isfallback) {
		this.isfallback = isfallback;
	}


	public int getMppid() {
		return this.mppid;
	}

	public void setMppid(int mppid) {
		this.mppid = mppid;
	}


	@Column(name="op_params", length=2000)
	public String getOpParams() {
		return this.opParams;
	}

	public void setOpParams(String opParams) {
		this.opParams = opParams;
	}


	@Column(name="operator_key", length=50)
	public String getOperatorKey() {
		return this.operatorKey;
	}

	public void setOperatorKey(String operatorKey) {
		this.operatorKey = operatorKey;
	}


	@Column(name="price_point")
	public double getPricePoint() {
		return this.pricePoint;
	}

	public void setPricePoint(double pricePoint) {
		this.pricePoint = pricePoint;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	public int getRenewable() {
		return this.renewable;
	}

	public void setRenewable(int renewable) {
		this.renewable = renewable;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public int getValidity() {
		return this.validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}


	@Column(name="validity_type", length=1)
	public String getValidityType() {
		return this.validityType;
	}

	public void setValidityType(String validityType) {
		this.validityType = validityType;
	}

}