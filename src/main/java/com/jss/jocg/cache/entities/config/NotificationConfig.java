package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;

import com.jss.jocg.cache.entities.config.json.JsonNotifyConfigConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyPercentageConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyUrlsConverter;
import com.jss.jocg.cache.entities.config.json.NotifyConfig;
import com.jss.jocg.cache.entities.config.json.NotifyPercentage;
import com.jss.jocg.cache.entities.config.json.NotifyUrls;


/**
 * The persistent class for the tb_notification_config database table.
 * 
 */
@Entity
@Table(name="tb_notification_config")
@NamedQuery(name="NotificationConfig.findAll", query="SELECT n FROM NotificationConfig n")
public class NotificationConfig implements Serializable {
	private static final long serialVersionUID = 1L;
	private int notifyConfigId;
	private int campaignid;
	private int chintfid;
	
	@Column(name="notify_config", length=200)
	private String notifyConfig;
	
	@Column(name="notify_percentage", length=200)
	private String notifyPercentage;
	
	
	@Column(name="notify_urls", length=2000)
	private String notifyUrls;
	private byte offlineDump;
	private byte parkingToFresh;
	private String pubid;
	private int status;
	private String subControlLevel;
	
	private String subcontrolConfig;
	private int tsourceid;
	private byte zeroPp;

	
	public NotificationConfig() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	@Id
	@Column(name="notify_config_id", unique=true, nullable=false)
	public int getNotifyConfigId() {
		return this.notifyConfigId;
	}

	public void setNotifyConfigId(int notifyConfigId) {
		this.notifyConfigId = notifyConfigId;
	}


	public int getCampaignid() {
		return this.campaignid;
	}

	public void setCampaignid(int campaignid) {
		this.campaignid = campaignid;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(name="notify_config", length=200)
	public String getNotifyConfig() {
		return this.notifyConfig;
	}

	public void setNotifyConfig(String notifyConfig) {
		this.notifyConfig = notifyConfig;
	}


	@Column(name="notify_percentage", length=200)
	public String getNotifyPercentage() {
		return this.notifyPercentage;
	}

	public void setNotifyPercentage(String notifyPercentage) {
		this.notifyPercentage = notifyPercentage;
	}


	@Column(name="notify_urls", length=2000)
	public String getNotifyUrls() {
		return this.notifyUrls;
	}

	public void setNotifyUrls(String notifyUrls) {
		this.notifyUrls = notifyUrls;
	}


	@Column(name="offline_dump", nullable=false)
	public byte getOfflineDump() {
		return this.offlineDump;
	}

	public void setOfflineDump(byte offlineDump) {
		this.offlineDump = offlineDump;
	}


	@Column(name="parking_to_fresh", nullable=false)
	public byte getParkingToFresh() {
		return this.parkingToFresh;
	}

	public void setParkingToFresh(byte parkingToFresh) {
		this.parkingToFresh = parkingToFresh;
	}


	@Column(length=50)
	public String getPubid() {
		return this.pubid;
	}

	public void setPubid(String pubid) {
		this.pubid = pubid;
	}


	@Column(nullable=false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(name="sub_control_level", length=1)
	public String getSubControlLevel() {
		return this.subControlLevel;
	}

	public void setSubControlLevel(String subControlLevel) {
		this.subControlLevel = subControlLevel;
	}


	@Column(name="subcontrol_config", length=200)
	public String getSubcontrolConfig() {
		return this.subcontrolConfig;
	}

	public void setSubcontrolConfig(String subcontrolConfig) {
		this.subcontrolConfig = subcontrolConfig;
	}


	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}


	@Column(name="zero_pp", nullable=false)
	public byte getZeroPp() {
		return this.zeroPp;
	}

	public void setZeroPp(byte zeroPp) {
		this.zeroPp = zeroPp;
	}

	
	

}