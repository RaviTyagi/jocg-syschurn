package com.jss.jocg.cache.entities.config.json;

import javax.persistence.AttributeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.codehaus.jackson.map.ObjectMapper;


public class JsonSubcontrolConfigConverter  implements AttributeConverter<SubcontrolConfig, String>{
	 private final static ObjectMapper objectMapper = new ObjectMapper();
		static Log logger = LogFactory.getLog(JsonSubcontrolConfigConverter.class.getName());
		
		static{
			//objectMapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
			//objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
			
		}
		
		 @Override
		  public String convertToDatabaseColumn(SubcontrolConfig meta) {
		    try {
		      return objectMapper.writeValueAsString(meta);
		    } catch (Exception ex) {
		    	logger.error("convertToDatabaseColumn::  " + ex);
		      return null;
		      // or throw an error
		    }
		  }

		  @Override
		  public SubcontrolConfig convertToEntityAttribute(String dbData) {
		    try {
		      return objectMapper.readValue(dbData, SubcontrolConfig.class);
		    } catch (Exception ex) {
		       logger.error("convertToEntityAttribute::" + dbData+", exception: "+ex);
		      return null;
		    }
		  }	 	
		
		
}
