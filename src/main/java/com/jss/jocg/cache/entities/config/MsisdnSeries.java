package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_msisdn_series database table.
 * 
 */
@Entity
@Table(name="tb_msisdn_series")
@NamedQuery(name="MsisdnSeries.findAll", query="SELECT m FROM MsisdnSeries m")
public class MsisdnSeries implements Serializable {
	private static final long serialVersionUID = 1L;
	private int msisdnSeriesId;
	private int chintfid;
	private int circleid;
	private String msisdnPrefix;
	private Date regDate;
	private byte status;
	private long msisdnStart;
	private long msisdnEnd;

	public MsisdnSeries() {
	}
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(name="msisdn_series_id", unique=true, nullable=false)
	public int getMsisdnSeriesId() {
		return this.msisdnSeriesId;
	}

	public void setMsisdnSeriesId(int msisdnSeriesId) {
		this.msisdnSeriesId = msisdnSeriesId;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	@Column(name="msisdn_prefix", nullable=false, length=50)
	public String getMsisdnPrefix() {
		return this.msisdnPrefix;
	}

	public void setMsisdnPrefix(String msisdnPrefix) {
		this.msisdnPrefix = msisdnPrefix;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(nullable=false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}
	@Column(name="msisdn_start")
	public long getMsisdnStart() {
		return msisdnStart;
	}
	public void setMsisdnStart(long msisdnStart) {
		this.msisdnStart = msisdnStart;
	}
	
	@Column(name="msisdn_end")
	public long getMsisdnEnd() {
		return msisdnEnd;
	}
	public void setMsisdnEnd(long msisdnEnd) {
		this.msisdnEnd = msisdnEnd;
	}
	
	

}