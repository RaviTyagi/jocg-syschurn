package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_routing database table.
 * 
 */
@Entity
@Table(name="tb_routing")
@NamedQuery(name="Routing.findAll", query="SELECT r FROM Routing r")
public class Routing implements Serializable {
	private static final long serialVersionUID = 1L;
	private int routingId;
	private int campaignid;
	private String casename;
	private int chintfid;
	private int circleid;
	private String nwType;
	private String otherConfig;
	private String pubid;
	private Date regDate;
	private String routingCatg;
	private String routingDef;
	private byte status;
	private int tsourceid;

	public Routing() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("Routing{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}


	@Id
	@Column(name="routing_id", unique=true, nullable=false)
	public int getRoutingId() {
		return this.routingId;
	}

	public void setRoutingId(int routingId) {
		this.routingId = routingId;
	}


	public int getCampaignid() {
		return this.campaignid;
	}

	public void setCampaignid(int campaignid) {
		this.campaignid = campaignid;
	}


	@Column(nullable=false, length=50)
	public String getCasename() {
		return this.casename;
	}

	public void setCasename(String casename) {
		this.casename = casename;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	@Column(name="nw_type", length=50)
	public String getNwType() {
		return this.nwType;
	}

	public void setNwType(String nwType) {
		this.nwType = nwType;
	}


	@Column(name="other_config", nullable=false, length=200)
	public String getOtherConfig() {
		return this.otherConfig;
	}

	public void setOtherConfig(String otherConfig) {
		this.otherConfig = otherConfig;
	}


	public String getPubid() {
		return this.pubid;
	}

	public void setPubid(String pubid) {
		this.pubid = pubid;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(name="routing_catg", nullable=false, length=50)
	public String getRoutingCatg() {
		return this.routingCatg;
	}

	public void setRoutingCatg(String routingCatg) {
		this.routingCatg = routingCatg;
	}


	@Column(name="routing_def", nullable=false, length=50)
	public String getRoutingDef() {
		return this.routingDef;
	}

	public void setRoutingDef(String routingDef) {
		this.routingDef = routingDef;
	}


	@Column(nullable=false)
	public byte getStatus() {
		return this.status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}


	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}

}