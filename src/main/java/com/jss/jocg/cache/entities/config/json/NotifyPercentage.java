package com.jss.jocg.cache.entities.config.json;

import java.lang.reflect.Field;

import javax.persistence.Convert;

public class NotifyPercentage {
/**
 Boolean parking;
	Boolean renewal;
	Boolean charging;
	Boolean churn;
	Boolean grace;
	Boolean churn20min;
	@Convert(converter=JsonSkipPricePointConverter.class)
	SkipPricePoint skippp;
 */
	Integer parking;
	Integer renewal;
	Integer charging;
	Integer churn;
	Integer grace;
	Integer churn20min;
	
public NotifyPercentage(){}
@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder("NotifyPercentage{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}
public Integer getParking() {
	return parking;
}
public void setParking(Integer parking) {
	this.parking = parking;
}
public Integer getRenewal() {
	return renewal;
}
public void setRenewal(Integer renewal) {
	this.renewal = renewal;
}
public Integer getCharging() {
	return charging;
}
public void setCharging(Integer charging) {
	this.charging = charging;
}
public Integer getChurn() {
	return churn;
}
public void setChurn(Integer churn) {
	this.churn = churn;
}
public Integer getGrace() {
	return grace;
}
public void setGrace(Integer grace) {
	this.grace = grace;
}
public Integer getChurn20min() {
	return churn20min;
}
public void setChurn20min(Integer churn20min) {
	this.churn20min = churn20min;
}

}
