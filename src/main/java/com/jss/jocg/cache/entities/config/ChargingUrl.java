package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_charging_urls database table.
 * 
 */
@Entity
@Table(name="tb_charging_urls")
@NamedQuery(name="ChargingUrl.findAll", query="SELECT c FROM ChargingUrl c")
public class ChargingUrl implements Serializable {
	private static final long serialVersionUID = 1L;
	private int churlId;
	private String authTemplate;
	private String authType;
	private int chintfid;
	private String dataTemplate;
	private String httpMethod;
	private Date regDate;
	private byte ssl;
	private String sslCertificate;
	private int status;
	private String url;
	private String urlCatg;
	private byte useSslIgnore;

	public ChargingUrl() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(name="churl_id", unique=true, nullable=false)
	public int getChurlId() {
		return this.churlId;
	}

	public void setChurlId(int churlId) {
		this.churlId = churlId;
	}


	@Column(name="auth_template", length=200)
	public String getAuthTemplate() {
		return this.authTemplate;
	}

	public void setAuthTemplate(String authTemplate) {
		this.authTemplate = authTemplate;
	}


	@Column(name="auth_type", nullable=false, length=1)
	public String getAuthType() {
		return this.authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}


	@Column(nullable=false)
	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(name="data_template", nullable=false, length=500)
	public String getDataTemplate() {
		return this.dataTemplate;
	}

	public void setDataTemplate(String dataTemplate) {
		this.dataTemplate = dataTemplate;
	}


	@Column(name="http_method", nullable=false, length=1)
	public String getHttpMethod() {
		return this.httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(name="ssl_used",nullable=false)
	public byte getSsl() {
		return this.ssl;
	}

	public void setSsl(byte ssl) {
		this.ssl = ssl;
	}


	@Column(name="ssl_certificate", nullable=false, length=200)
	public String getSslCertificate() {
		return this.sslCertificate;
	}

	public void setSslCertificate(String sslCertificate) {
		this.sslCertificate = sslCertificate;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(nullable=false, length=200)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	@Column(name="url_catg", nullable=false, length=1)
	public String getUrlCatg() {
		return this.urlCatg;
	}

	public void setUrlCatg(String urlCatg) {
		this.urlCatg = urlCatg;
	}


	@Column(name="use_ssl_ignore")
	public byte getUseSslIgnore() {
		return this.useSslIgnore;
	}

	public void setUseSslIgnore(byte useSslIgnore) {
		this.useSslIgnore = useSslIgnore;
	}

}