package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_services database table.
 * 
 */
@Entity
@Table(name="tb_services")
@NamedQuery(name="Service.findAll", query="SELECT s FROM Service s")
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;
	private int serviceid;
	private String serviceCatg;
	private String serviceDescp;
	private String serviceName;
	private int status;

	public Service() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	@Id
	@Column(unique=true, nullable=false)
	public int getServiceid() {
		return this.serviceid;
	}

	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}


	@Column(name="service_catg", length=200)
	public String getServiceCatg() {
		return this.serviceCatg;
	}

	public void setServiceCatg(String serviceCatg) {
		this.serviceCatg = serviceCatg;
	}


	@Column(name="service_descp", length=200)
	public String getServiceDescp() {
		return this.serviceDescp;
	}

	public void setServiceDescp(String serviceDescp) {
		this.serviceDescp = serviceDescp;
	}


	@Column(name="service_name", nullable=false, length=50)
	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}