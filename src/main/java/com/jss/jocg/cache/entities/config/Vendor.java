package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tb_vendors database table.
 * 
 */
@Entity
@Table(name="tb_vendors")
@NamedQuery(name="Vendor.findAll", query="SELECT v FROM Vendor v")
public class Vendor implements Serializable {
	private static final long serialVersionUID = 1L;
	private int vendorid;
	private int status;
	private String vendorCode;
	private String vendorName;

	public Vendor() {
	}


	@Id
	@Column(unique=true, nullable=false)
	public int getVendorid() {
		return this.vendorid;
	}

	public void setVendorid(int vendorid) {
		this.vendorid = vendorid;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(name="vendor_code", length=50)
	public String getVendorCode() {
		return this.vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}


	@Column(name="vendor_name", nullable=false, length=50)
	public String getVendorName() {
		return this.vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

}