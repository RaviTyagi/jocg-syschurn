package com.jss.jocg.cache.entities.config.json;

import java.lang.reflect.Field;

public class NotifyUrls {
//{"waste":"http:\/\/csent\/waste.aspx","parking":"http:\/\/csent\/parking.aspx","renewal":"http:\/\/csent\/renewal.aspx",
	//"charging":"http:\/\/csent\/charging.aspx","churn":"http:\/\/csent\/churn.aspx","grace":"http:\/\/csent\/grace.aspx",
	//"20minchurn":"http:\/\/csent\/20minchurn.aspx"}
	
	String waste;
	String parking;
	String renewal;
	String charging;
	String churn;
	String grace;
	String churn20min;
	public NotifyUrls(){}
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("NotifyUrls{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	public String getWaste() {
		return waste;
	}
	public void setWaste(String waste) {
		this.waste = waste;
	}
	public String getParking() {
		return parking;
	}
	public void setParking(String parking) {
		this.parking = parking;
	}
	public String getRenewal() {
		return renewal;
	}
	public void setRenewal(String renewal) {
		this.renewal = renewal;
	}
	public String getCharging() {
		return charging;
	}
	public void setCharging(String charging) {
		this.charging = charging;
	}
	public String getChurn() {
		return churn;
	}
	public void setChurn(String churn) {
		this.churn = churn;
	}
	public String getGrace() {
		return grace;
	}
	public void setGrace(String grace) {
		this.grace = grace;
	}
	public String getChurn20min() {
		return churn20min;
	}
	public void setChurn20min(String churn20min) {
		this.churn20min = churn20min;
	}
	
}
