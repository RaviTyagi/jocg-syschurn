package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_traffic_sources database table.
 * 
 */
@Entity
@Table(name="tb_traffic_sources")
@NamedQuery(name="TrafficSource.findAll", query="SELECT t FROM TrafficSource t")
public class TrafficSource implements Serializable {
	private static final long serialVersionUID = 1L;
	private int tsourceid;
	private Date lastUpdate;
//	private String notifyConfig;
//	private String notifyPercentage;
//	private String notifyUrls;
	private String othersource;
	private Date regDate;
	private String sourcename;
	private int status;
//	private String subControlLevel;
//	private String subcontrolConfig;
	private String tokenParams;
	private String tsourcetype;
	private String wasteUrl;

	public TrafficSource() {
	}


	@Id
	@Column(unique=true, nullable=false)
	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update")
	public Date getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}


//	@Column(name="notify_config", length=200)
//	public String getNotifyConfig() {
//		return this.notifyConfig;
//	}
//
//	public void setNotifyConfig(String notifyConfig) {
//		this.notifyConfig = notifyConfig;
//	}


//	@Column(name="notify_percentage", length=200)
//	public String getNotifyPercentage() {
//		return this.notifyPercentage;
//	}
//
//	public void setNotifyPercentage(String notifyPercentage) {
//		this.notifyPercentage = notifyPercentage;
//	}


//	@Column(name="notify_urls", length=200)
//	public String getNotifyUrls() {
//		return this.notifyUrls;
//	}
//
//	public void setNotifyUrls(String notifyUrls) {
//		this.notifyUrls = notifyUrls;
//	}


	@Column(length=50)
	public String getOthersource() {
		return this.othersource;
	}

	public void setOthersource(String othersource) {
		this.othersource = othersource;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(length=100)
	public String getSourcename() {
		return this.sourcename;
	}

	public void setSourcename(String sourcename) {
		this.sourcename = sourcename;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


//	@Column(name="sub_control_level", length=1)
//	public String getSubControlLevel() {
//		return this.subControlLevel;
//	}
//
//	public void setSubControlLevel(String subControlLevel) {
//		this.subControlLevel = subControlLevel;
//	}


//	@Column(name="subcontrol_config", length=200)
//	public String getSubcontrolConfig() {
//		return this.subcontrolConfig;
//	}
//
//	public void setSubcontrolConfig(String subcontrolConfig) {
//		this.subcontrolConfig = subcontrolConfig;
//	}


	@Column(name="token_params", length=200)
	public String getTokenParams() {
		return this.tokenParams;
	}

	public void setTokenParams(String tokenParams) {
		this.tokenParams = tokenParams;
	}


	@Column(length=1)
	public String getTsourcetype() {
		return this.tsourcetype;
	}

	public void setTsourcetype(String tsourcetype) {
		this.tsourcetype = tsourcetype;
	}


	@Column(name="waste_url", length=100)
	public String getWasteUrl() {
		return this.wasteUrl;
	}

	public void setWasteUrl(String wasteUrl) {
		this.wasteUrl = wasteUrl;
	}

}