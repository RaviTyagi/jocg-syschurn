package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_charging_url_params database table.
 * 
 */
@Entity
@Table(name="tb_charging_url_params")
@NamedQuery(name="ChargingUrlParam.findAll", query="SELECT c FROM ChargingUrlParam c")
public class ChargingUrlParam implements Serializable {
	private static final long serialVersionUID = 1L;
	private int churlParamId;
	private int churlId;
	private String dataType;
	private String defaultValue;
	private String maxValue;
	private String minValue;
	private String paramCatg;
	private String paramKey;
	private short status;
	private byte validate;
	private String validationType;

	public ChargingUrlParam() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	@Id
	@Column(name="churl_param_id", unique=true, nullable=false)
	public int getChurlParamId() {
		return this.churlParamId;
	}

	public void setChurlParamId(int churlParamId) {
		this.churlParamId = churlParamId;
	}


	@Column(name="churl_id", nullable=false)
	public int getChurlId() {
		return this.churlId;
	}

	public void setChurlId(int churlId) {
		this.churlId = churlId;
	}


	@Column(name="data_type", length=1)
	public String getDataType() {
		return this.dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}


	@Column(name="default_value", nullable=false, length=200)
	public String getDefaultValue() {
		return this.defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	@Column(name="max_value", length=50)
	public String getMaxValue() {
		return this.maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}


	@Column(name="min_value", length=50)
	public String getMinValue() {
		return this.minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}


	@Column(name="param_catg", nullable=false, length=1)
	public String getParamCatg() {
		return this.paramCatg;
	}

	public void setParamCatg(String paramCatg) {
		this.paramCatg = paramCatg;
	}


	@Column(name="param_key", nullable=false, length=50)
	public String getParamKey() {
		return this.paramKey;
	}

	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}


	public short getStatus() {
		return this.status;
	}

	public void setStatus(short status) {
		this.status = status;
	}


	@Column(nullable=false)
	public byte getValidate() {
		return this.validate;
	}

	public void setValidate(byte validate) {
		this.validate = validate;
	}


	@Column(name="validation_type", length=1)
	public String getValidationType() {
		return this.validationType;
	}

	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}

}