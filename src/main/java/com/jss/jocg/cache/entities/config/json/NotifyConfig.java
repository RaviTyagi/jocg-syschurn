package com.jss.jocg.cache.entities.config.json;

import java.lang.reflect.Field;

import javax.persistence.Convert;

public class NotifyConfig {
	Boolean parking;
	Boolean renewal;
	Boolean charging;
	Boolean churn;
	Boolean grace;
	Boolean churn20min;
	@Convert(converter=JsonSkipPricePointConverter.class)
	SkipPricePoint skippp;
	
	public NotifyConfig(){}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("NotifyConfig{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	public Boolean getParking() {
		return parking;
	}
	public void setParking(Boolean parking) {
		this.parking = parking;
	}
	public Boolean getRenewal() {
		return renewal;
	}
	public void setRenewal(Boolean renewal) {
		this.renewal = renewal;
	}
	public Boolean getCharging() {
		return charging;
	}
	public void setCharging(Boolean charging) {
		this.charging = charging;
	}
	public Boolean getChurn() {
		return churn;
	}
	public void setChurn(Boolean churn) {
		this.churn = churn;
	}
	public Boolean getGrace() {
		return grace;
	}
	public void setGrace(Boolean grace) {
		this.grace = grace;
	}
	public Boolean getChurn20min() {
		return churn20min;
	}
	public void setChurn20min(Boolean churn20min) {
		this.churn20min = churn20min;
	}
	public SkipPricePoint getSkippp() {
		return skippp;
	}
	public void setSkippp(SkipPricePoint skippp) {
		this.skippp = skippp;
	}
	
	//{"parking":false,"renewal":false,"charging":true,"churn":false,"grace":false,"churn20min":false,"skippp":{"type":"below","value":""}}

}
