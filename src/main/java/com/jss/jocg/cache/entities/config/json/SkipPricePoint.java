package com.jss.jocg.cache.entities.config.json;

import java.lang.reflect.Field;

public class SkipPricePoint {
String type;
String value;
@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder("SkipPricePoint{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}
public String getType() {
	return type;
}
public void setType(String type) {
	this.type = type;
}
public String getValue() {
	return value;
}
public void setValue(String value) {
	this.value = value;
}

}
