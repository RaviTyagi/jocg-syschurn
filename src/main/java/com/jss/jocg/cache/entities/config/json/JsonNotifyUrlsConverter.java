package com.jss.jocg.cache.entities.config.json;

import javax.persistence.AttributeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.codehaus.jackson.map.ObjectMapper;

public class JsonNotifyUrlsConverter  implements AttributeConverter<NotifyUrls, String>{
	 private final static ObjectMapper objectMapper = new ObjectMapper();
		static Log logger = LogFactory.getLog(JsonNotifyUrlsConverter.class.getName());
		
		static{
			//objectMapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
			//objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
			
		}
		
		 @Override
		  public String convertToDatabaseColumn(NotifyUrls meta) {
		    try {
		      return objectMapper.writeValueAsString(meta);
		    } catch (Exception ex) {
		    	logger.error("convertToDatabaseColumn::  " + ex);
		      return null;
		      // or throw an error
		    }
		  }

		  @Override
		  public NotifyUrls convertToEntityAttribute(String dbData) {
		    try {
		      return objectMapper.readValue(dbData, NotifyUrls.class);
		    } catch (Exception ex) {
		       logger.error("convertToEntityAttribute::" + dbData+", exception: "+ex);
		      return null;
		    }
		  }	 	
		
		
}