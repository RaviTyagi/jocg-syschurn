package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_charging_interfaces database table.
 * 
 */
@Entity
@Table(name="tb_charging_interfaces")
@NamedQuery(name="ChargingInterface.findAll", query="SELECT c FROM ChargingInterface c")
public class ChargingInterface implements Serializable {
	private static final long serialVersionUID = 1L;
	private int chintfid;
	private String chintfType;
	private String country;
	private String descp;
	private String gmtDiff;
	private String handlerClass;
	private String name;
	private String opcode;
	private int pchintfid;
	private int renewalRequired;
	private String renewalType;
	private int status;
	private int useparenthandler;

	public ChargingInterface() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(name="chintf_type", length=1)
	public String getChintfType() {
		return this.chintfType;
	}

	public void setChintfType(String chintfType) {
		this.chintfType = chintfType;
	}


	@Column(length=50)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	@Column(length=200)
	public String getDescp() {
		return this.descp;
	}

	public void setDescp(String descp) {
		this.descp = descp;
	}


	@Column(name="gmt_diff", length=10)
	public String getGmtDiff() {
		return this.gmtDiff;
	}

	public void setGmtDiff(String gmtDiff) {
		this.gmtDiff = gmtDiff;
	}


	@Column(name="handler_class", length=500)
	public String getHandlerClass() {
		return this.handlerClass;
	}

	public void setHandlerClass(String handlerClass) {
		this.handlerClass = handlerClass;
	}


	@Column(length=50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Column(length=50)
	public String getOpcode() {
		return this.opcode;
	}

	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}


	public int getPchintfid() {
		return this.pchintfid;
	}

	public void setPchintfid(int pchintfid) {
		this.pchintfid = pchintfid;
	}


	@Column(name="renewal_required")
	public int getRenewalRequired() {
		return this.renewalRequired;
	}

	public void setRenewalRequired(int renewalRequired) {
		this.renewalRequired = renewalRequired;
	}


	@Column(name="renewal_type", length=1)
	public String getRenewalType() {
		return this.renewalType;
	}

	public void setRenewalType(String renewalType) {
		this.renewalType = renewalType;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(nullable=false)
	public int getUseparenthandler() {
		return this.useparenthandler;
	}

	public void setUseparenthandler(int useparenthandler) {
		this.useparenthandler = useparenthandler;
	}

}