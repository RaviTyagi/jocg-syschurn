package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_platforms database table.
 * 
 */
@Entity
@Table(name="tb_platforms")
@NamedQuery(name="Platform.findAll", query="SELECT p FROM Platform p")
public class Platform implements Serializable {
	private static final long serialVersionUID = 1L;
	private int platformid;
	private String platform;
	private String systemid;
	private String systemDescription;
	private String systemMode;

	public Platform() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	@Id
	@Column(unique=true, nullable=false)
	public int getPlatformid() {
		return this.platformid;
	}

	public void setPlatformid(int platformid) {
		this.platformid = platformid;
	}


	@Column(length=50)
	public String getPlatform() {
		return this.platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}


	@Column(length=50)
	public String getSystemid() {
		return this.systemid;
	}

	public void setSystemid(String systemid) {
		this.systemid = systemid;
	}


	@Column(name="csi_system_desc")
	public String getSystemDescription() {
		return systemDescription;
	}


	public void setSystemDescription(String systemDescription) {
		this.systemDescription = systemDescription;
	}

	@Column(name="csi_system_mode")
	public String getSystemMode() {
		return systemMode;
	}


	public void setSystemMode(String systemMode) {
		this.systemMode = systemMode;
	}
	
	
	
	

}