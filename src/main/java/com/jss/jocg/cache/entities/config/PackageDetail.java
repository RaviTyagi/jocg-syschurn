package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_package_details database table.
 * 
 */
@Entity
@Table(name="tb_package_details")
@NamedQuery(name="PackageDetail.findAll", query="SELECT p FROM PackageDetail p")
public class PackageDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	private int pkgid;
	private int allowCountPerCredit;
	private byte allowCredit;
	private String appMode;
	private byte autorenew;
	private String availableOnApp;
	private byte campaignOnly;
	private String channelList;
	private String channelListAppin;
	private String currency;
	private byte discountApplicable;
	private Date endDate;
	private String group;
	private String groupDesc;
	private String kidPickList;
	private Date launchDate;
	private String pkgDesc;
	private String pkgType;
	private String pkgname;
	private double pricepoint;
	private String purchaseMode;
	private String purchaseModeDesc;
	private Date regDate;
	private int serviceid;
	private int status;
	private int validity;
	private String validityCatg;
	private String validityUnit;

	public PackageDetail() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getPkgid() {
		return this.pkgid;
	}

	public void setPkgid(int pkgid) {
		this.pkgid = pkgid;
	}


	@Column(name="allow_count_per_credit")
	public int getAllowCountPerCredit() {
		return this.allowCountPerCredit;
	}

	public void setAllowCountPerCredit(int allowCountPerCredit) {
		this.allowCountPerCredit = allowCountPerCredit;
	}


	@Column(name="allow_credit")
	public byte getAllowCredit() {
		return this.allowCredit;
	}

	public void setAllowCredit(byte allowCredit) {
		this.allowCredit = allowCredit;
	}


	@Column(name="app_mode", length=1)
	public String getAppMode() {
		return this.appMode;
	}

	public void setAppMode(String appMode) {
		this.appMode = appMode;
	}


	public byte getAutorenew() {
		return this.autorenew;
	}

	public void setAutorenew(byte autorenew) {
		this.autorenew = autorenew;
	}


	@Column(name="available_on_app", length=10)
	public String getAvailableOnApp() {
		return this.availableOnApp;
	}

	public void setAvailableOnApp(String availableOnApp) {
		this.availableOnApp = availableOnApp;
	}


	@Column(name="campaign_only")
	public byte getCampaignOnly() {
		return this.campaignOnly;
	}

	public void setCampaignOnly(byte campaignOnly) {
		this.campaignOnly = campaignOnly;
	}


	@Column(name="channel_list", length=200)
	public String getChannelList() {
		return this.channelList;
	}

	public void setChannelList(String channelList) {
		this.channelList = channelList;
	}


	@Column(name="channel_list_appin", length=200)
	public String getChannelListAppin() {
		return this.channelListAppin;
	}

	public void setChannelListAppin(String channelListAppin) {
		this.channelListAppin = channelListAppin;
	}


	@Column(length=10)
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}


	@Column(name="discount_applicable")
	public byte getDiscountApplicable() {
		return this.discountApplicable;
	}

	public void setDiscountApplicable(byte discountApplicable) {
		this.discountApplicable = discountApplicable;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_date")
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	@Column(name="group_name", length=10)
	public String getGroup() {
		return this.group;
	}

	public void setGroup(String group) {
		this.group = group;
	}


	@Column(name="group_desc", length=200)
	public String getGroupDesc() {
		return this.groupDesc;
	}

	public void setGroupDesc(String groupDesc) {
		this.groupDesc = groupDesc;
	}


	@Column(name="kid_pick_list", length=200)
	public String getKidPickList() {
		return this.kidPickList;
	}

	public void setKidPickList(String kidPickList) {
		this.kidPickList = kidPickList;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="launch_date")
	public Date getLaunchDate() {
		return this.launchDate;
	}

	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}


	@Column(name="pkg_desc", length=45)
	public String getPkgDesc() {
		return this.pkgDesc;
	}

	public void setPkgDesc(String pkgDesc) {
		this.pkgDesc = pkgDesc;
	}


	@Column(name="pkg_type", length=45)
	public String getPkgType() {
		return this.pkgType;
	}

	public void setPkgType(String pkgType) {
		this.pkgType = pkgType;
	}


	@Column(length=50)
	public String getPkgname() {
		return this.pkgname;
	}

	public void setPkgname(String pkgname) {
		this.pkgname = pkgname;
	}


	public double getPricepoint() {
		return this.pricepoint;
	}

	public void setPricepoint(double pricepoint) {
		this.pricepoint = pricepoint;
	}


	@Column(name="purchase_mode", length=10)
	public String getPurchaseMode() {
		return this.purchaseMode;
	}

	public void setPurchaseMode(String purchaseMode) {
		this.purchaseMode = purchaseMode;
	}


	@Column(name="purchase_mode_desc", length=200)
	public String getPurchaseModeDesc() {
		return this.purchaseModeDesc;
	}

	public void setPurchaseModeDesc(String purchaseModeDesc) {
		this.purchaseModeDesc = purchaseModeDesc;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	public int getServiceid() {
		return this.serviceid;
	}

	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public int getValidity() {
		return this.validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}


	@Column(name="validity_catg", length=1)
	public String getValidityCatg() {
		return this.validityCatg;
	}

	public void setValidityCatg(String validityCatg) {
		this.validityCatg = validityCatg;
	}


	@Column(name="validity_unit", length=1)
	public String getValidityUnit() {
		return this.validityUnit;
	}

	public void setValidityUnit(String validityUnit) {
		this.validityUnit = validityUnit;
	}

}