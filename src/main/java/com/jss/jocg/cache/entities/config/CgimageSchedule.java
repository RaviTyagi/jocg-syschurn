package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_cgimage_schedule database table.
 * 
 */
@Entity
@Table(name="tb_cgimage_schedule")
@NamedQuery(name="CgimageSchedule.findAll", query="SELECT c FROM CgimageSchedule c")
public class CgimageSchedule implements Serializable {
	private static final long serialVersionUID = 1L;
	private int imgid;
	private String daysOfWeek;
	private String hrsOfDay;
	private String imagePath;
	private String imageUrl;
	private int pkgchintfid;
	private String scheduleType;
	private int status;
	private int tsourceid;

	public CgimageSchedule() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getImgid() {
		return this.imgid;
	}

	public void setImgid(int imgid) {
		this.imgid = imgid;
	}


	@Column(name="days_of_week", length=1)
	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}


	@Column(name="hrs_of_day", length=200)
	public String getHrsOfDay() {
		return this.hrsOfDay;
	}

	public void setHrsOfDay(String hrsOfDay) {
		this.hrsOfDay = hrsOfDay;
	}


	@Column(name="image_path", length=500)
	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	@Column(name="image_url", length=500)
	public String getImageUrl() {
		return this.imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}


	public int getPkgchintfid() {
		return this.pkgchintfid;
	}

	public void setPkgchintfid(int pkgchintfid) {
		this.pkgchintfid = pkgchintfid;
	}


	@Column(name="schedule_type", length=1)
	public String getScheduleType() {
		return this.scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}

}