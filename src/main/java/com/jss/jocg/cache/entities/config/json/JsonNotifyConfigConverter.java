package com.jss.jocg.cache.entities.config.json;


import javax.persistence.AttributeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.codehaus.jackson.map.ObjectMapper;


public class JsonNotifyConfigConverter implements AttributeConverter<NotifyConfig, String>{
	 private final static ObjectMapper objectMapper = new ObjectMapper();
		static Log logger = LogFactory.getLog(JsonNotifyConfigConverter.class.getName());
		
		static{
			//objectMapper.configure(Feature.WRITE_DATES_AS_TIMESTAMPS, false);
			//objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
			
		}
		
		 @Override
		  public String convertToDatabaseColumn(NotifyConfig meta) {
		    try {
		      return objectMapper.writeValueAsString(meta);
		    } catch (Exception ex) {
		    	logger.error("convertToDatabaseColumn::  " + ex);
		      return null;
		      // or throw an error
		    }
		  }

		  @Override
		  public NotifyConfig convertToEntityAttribute(String dbData) {
		    try {
		      return objectMapper.readValue(dbData, NotifyConfig.class);
		    } catch (Exception ex) {
		       logger.error("convertToEntityAttribute::" + dbData+", exception: "+ex);
		      return null;
		    }
		  }	 	
		
		
}
