package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_traffic_campaigns database table.
 * 
 */
@Entity
@Table(name="tb_traffic_campaigns")
@NamedQuery(name="TrafficCampaign.findAll", query="SELECT t FROM TrafficCampaign t")
public class TrafficCampaign implements Serializable {
	private static final long serialVersionUID = 1L;
	private int campaignid;
	private String campaigncode;
	private String campaigndescp;
	private String campaignname;
	private int chintfid;
	private int circleid;
	private String contenttype;
	private int enabledatecheck;
	private int pkgid;
	private int packageValidity;
	private double packagePrice;
	private Date regDate;
	private int serviceid;
	private String status;
	private int tsourceid;
	private Date validityend;
	private Date validitystart;

	public TrafficCampaign() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	@Id
	@Column(unique=true, nullable=false)
	public int getCampaignid() {
		return this.campaignid;
	}

	public void setCampaignid(int campaignid) {
		this.campaignid = campaignid;
	}


	@Column(length=45)
	public String getCampaigncode() {
		return this.campaigncode;
	}

	public void setCampaigncode(String campaigncode) {
		this.campaigncode = campaigncode;
	}


	@Column(length=200)
	public String getCampaigndescp() {
		return this.campaigndescp;
	}

	public void setCampaigndescp(String campaigndescp) {
		this.campaigndescp = campaigndescp;
	}


	@Column(nullable=false, length=100)
	public String getCampaignname() {
		return this.campaignname;
	}

	public void setCampaignname(String campaignname) {
		this.campaignname = campaignname;
	}


	@Column(nullable=false)
	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(nullable=false)
	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	@Column(length=50)
	public String getContenttype() {
		return this.contenttype;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}


	public int getEnabledatecheck() {
		return this.enabledatecheck;
	}

	public void setEnabledatecheck(int enabledatecheck) {
		this.enabledatecheck = enabledatecheck;
	}


	@Column(nullable=false)
	public int getPkgid() {
		return this.pkgid;
	}

	public void setPkgid(int pkgid) {
		this.pkgid = pkgid;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(nullable=false)
	public int getServiceid() {
		return this.serviceid;
	}

	public void setServiceid(int serviceid) {
		this.serviceid = serviceid;
	}


	@Column(length=1)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	@Column(nullable=false)
	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getValidityend() {
		return this.validityend;
	}

	public void setValidityend(Date validityend) {
		this.validityend = validityend;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getValiditystart() {
		return this.validitystart;
	}

	public void setValiditystart(Date validitystart) {
		this.validitystart = validitystart;
	}

	@Column(name="packvalidity")
	public int getPackageValidity() {
		return packageValidity;
	}

	public void setPackageValidity(int packageValidity) {
		this.packageValidity = packageValidity;
	}

	@Column(name="pricepoint")
	public double getPackagePrice() {
		return packagePrice;
	}

	public void setPackagePrice(double packagePrice) {
		this.packagePrice = packagePrice;
	}

	
	
}