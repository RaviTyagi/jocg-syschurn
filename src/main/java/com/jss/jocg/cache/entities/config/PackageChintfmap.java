package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_package_chintfmap database table.
 * 
 */

@Entity
@Table(name="tb_package_chintfmap")
@NamedQuery(name="PackageChintfmap.findAll", query="SELECT p FROM PackageChintfmap p")
public class PackageChintfmap implements Serializable {
	private static final long serialVersionUID = 1L;
	private int pkgchintfid;
	private int chintfid;
	private int networkid;
	private int opPpid;
	private int pkgid;
	private int showOrder;
	private int status;

	public PackageChintfmap() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getPkgchintfid() {
		return this.pkgchintfid;
	}

	public void setPkgchintfid(int pkgchintfid) {
		this.pkgchintfid = pkgchintfid;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public int getNetworkid() {
		return this.networkid;
	}

	public void setNetworkid(int networkid) {
		this.networkid = networkid;
	}


	@Column(name="op_ppid")
	public int getOpPpid() {
		return this.opPpid;
	}

	public void setOpPpid(int opPpid) {
		this.opPpid = opPpid;
	}


	public int getPkgid() {
		return this.pkgid;
	}

	public void setPkgid(int pkgid) {
		this.pkgid = pkgid;
	}


	@Column(name="show_order")
	public int getShowOrder() {
		return this.showOrder;
	}

	public void setShowOrder(int showOrder) {
		this.showOrder = showOrder;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}