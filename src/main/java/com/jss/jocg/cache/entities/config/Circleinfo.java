package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_circleinfo database table.
 * 
 */
@Entity
@Table(name="tb_circleinfo")
@NamedQuery(name="Circleinfo.findAll", query="SELECT c FROM Circleinfo c")
public class Circleinfo implements Serializable {
	private static final long serialVersionUID = 1L;
	private int circleid;
	private int chintfid;
	private String circlecode;
	private String circlename;
	private int status;

	public Circleinfo() {
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(unique=true, nullable=false)
	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(length=20)
	public String getCirclecode() {
		return this.circlecode;
	}

	public void setCirclecode(String circlecode) {
		this.circlecode = circlecode;
	}


	@Column(length=50)
	public String getCirclename() {
		return this.circlename;
	}

	public void setCirclename(String circlename) {
		this.circlename = circlename;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}