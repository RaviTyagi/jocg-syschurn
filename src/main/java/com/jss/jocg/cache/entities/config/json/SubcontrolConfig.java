package com.jss.jocg.cache.entities.config.json;

import java.lang.reflect.Field;

import org.codehaus.jackson.annotate.JsonProperty;

public class SubcontrolConfig {
//{"MINSDA":"1000","MAX20MINC":"10","MAXSDC":"10","MAXSDA":"5000"}
	@JsonProperty("MINSDA")
	Integer minsda;
	@JsonProperty("MAX20MINC")
	Integer max20minc;
	@JsonProperty("MAXSDC")
	Integer maxsdc;
	@JsonProperty("MAXSDA")
	Integer maxsda;
	
	public SubcontrolConfig(){}
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("SubcontrolConfig{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	public Integer getMinsda() {
		return minsda;
	}
	public void setMinsda(Integer minsda) {
		this.minsda = minsda;
	}
	public Integer getMax20minc() {
		return max20minc;
	}
	public void setMax20minc(Integer max20minc) {
		this.max20minc = max20minc;
	}
	public Integer getMaxsdc() {
		return maxsdc;
	}
	public void setMaxsdc(Integer maxsdc) {
		this.maxsdc = maxsdc;
	}
	public Integer getMaxsda() {
		return maxsda;
	}
	public void setMaxsda(Integer maxsda) {
		this.maxsda = maxsda;
	}
	
}
