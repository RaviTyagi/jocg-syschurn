package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_imsi_series database table.
 * 
 */
@Entity
@Table(name="tb_imsi_series")
@NamedQuery(name="ImsiSeries.findAll", query="SELECT i FROM ImsiSeries i")
public class ImsiSeries implements Serializable {
	private static final long serialVersionUID = 1L;
	private int imsiSeriesId;
	private int chintfid;
	private int circleid;
	private String imsi;
	private Date regDate;
	private int status;

	public ImsiSeries() {
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

	@Id
	@Column(name="imsi_series_id", unique=true, nullable=false)
	public int getImsiSeriesId() {
		return this.imsiSeriesId;
	}

	public void setImsiSeriesId(int imsiSeriesId) {
		this.imsiSeriesId = imsiSeriesId;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	@Column(nullable=false, length=50)
	public String getImsi() {
		return this.imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(nullable=false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}