package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_voucher_details database table.
 * 
 */
@Entity
@Table(name="tb_voucher_details")
@NamedQuery(name="VoucherDetail.findAll", query="SELECT v FROM VoucherDetail v")
public class VoucherDetail implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long couponid;
	private String batchid;
	private Date creationDate;
	private String currency;
	private String discountType;
	private int discuountValue;
	private Date firstUsageTime;
	private Date lastUsageTime;
	private int pkgchintfid;
	private int chargedPPId;
	private int pkgid;
	private int status;
	private String uf1;
	private String uf2;
	private String uf3;
	private String uf4;
	private String uf5;
	private int usedCount;
	private int usedSequence;
	private Date validityStart;
	private Date validtyEnd;
	private int vendorId;
	private String voucherId;
	private String voucher_SHELLIFE;
	private String voucherType;
	

	public VoucherDetail() {
	}


	@Id
	@Column(unique=true, nullable=false)
	public Long getCouponid() {
		return this.couponid;
	}

	public void setCouponid(Long couponid) {
		this.couponid = couponid;
	}


	@Column(length=50)
	public String getBatchid() {
		return this.batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="creation_date")
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}


	@Column(length=10)
	public String getCurrency() {
		return this.currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}


	@Column(name="discount_type", length=1)
	public String getDiscountType() {
		return this.discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}


	@Column(name="discuount_value")
	public int getDiscuountValue() {
		return this.discuountValue;
	}

	public void setDiscuountValue(int discuountValue) {
		this.discuountValue = discuountValue;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="first_usage_time")
	public Date getFirstUsageTime() {
		return this.firstUsageTime;
	}

	public void setFirstUsageTime(Date firstUsageTime) {
		this.firstUsageTime = firstUsageTime;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_usage_time")
	public Date getLastUsageTime() {
		return this.lastUsageTime;
	}

	public void setLastUsageTime(Date lastUsageTime) {
		this.lastUsageTime = lastUsageTime;
	}


	public int getPkgchintfid() {
		return this.pkgchintfid;
	}

	public void setPkgchintfid(int pkgchintfid) {
		this.pkgchintfid = pkgchintfid;
	}


	public int getPkgid() {
		return this.pkgid;
	}

	public void setPkgid(int pkgid) {
		this.pkgid = pkgid;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	@Column(name="uf1", length=50)
	public String getUf1() {
		return this.uf1;
	}

	public void setUf1(String uf1) {
		this.uf1 = uf1;
	}


	@Column(name="uf2", length=50)
	public String getUf2() {
		return this.uf2;
	}

	public void setUf2(String uf2) {
		this.uf2 = uf2;
	}


	@Column(name="uf_3", length=50)
	public String getUf3() {
		return this.uf3;
	}

	public void setUf3(String uf3) {
		this.uf3 = uf3;
	}


	@Column(name="uf_4", length=50)
	public String getUf4() {
		return this.uf4;
	}

	public void setUf4(String uf4) {
		this.uf4 = uf4;
	}


	@Column(name="uf_5", length=50)
	public String getUf5() {
		return this.uf5;
	}

	public void setUf5(String uf5) {
		this.uf5 = uf5;
	}


	@Column(name="used_count")
	public int getUsedCount() {
		return this.usedCount;
	}

	public void setUsedCount(int usedCount) {
		this.usedCount = usedCount;
	}


	@Column(name="used_sequence")
	public int getUsedSequence() {
		return this.usedSequence;
	}

	public void setUsedSequence(int usedSequence) {
		this.usedSequence = usedSequence;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="validity_start")
	public Date getValidityStart() {
		return this.validityStart;
	}

	public void setValidityStart(Date validityStart) {
		this.validityStart = validityStart;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="validty_end")
	public Date getValidtyEnd() {
		return this.validtyEnd;
	}

	public void setValidtyEnd(Date validtyEnd) {
		this.validtyEnd = validtyEnd;
	}


	@Column(name="vendor_id")
	public int getVendorId() {
		return this.vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}


	@Column(name="voucher_id", length=20)
	public String getVoucherId() {
		return this.voucherId;
	}

	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}


	@Column(length=100)
	public String getVoucher_SHELLIFE() {
		return this.voucher_SHELLIFE;
	}

	public void setVoucher_SHELLIFE(String voucher_SHELLIFE) {
		this.voucher_SHELLIFE = voucher_SHELLIFE;
	}


	@Column(name="voucher_type", length=50)
	public String getVoucherType() {
		return this.voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	@Column(name="charged_ppid")
	public int getChargedPPId() {
		return chargedPPId;
	}


	public void setChargedPPId(int chargedPPId) {
		this.chargedPPId = chargedPPId;
	}
	
	
	

}