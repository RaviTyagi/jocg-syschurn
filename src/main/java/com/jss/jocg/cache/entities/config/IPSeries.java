package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tb_ip_series database table.
 * 
 */

@Entity
@Table(name="tb_ip_series")
@NamedQuery(name="IPSeries.findAll", query="SELECT i FROM IPSeries i")
public class IPSeries implements Serializable {
	private static final long serialVersionUID = 1L;
	private int ipSeriesId;
	private int chintfid;
	private int circleid;
	private String ipMask;
	private Date regDate;
	private int status;

	public IPSeries() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	@Id
	@Column(name="ip_series_id", unique=true, nullable=false)
	public int getIpSeriesId() {
		return this.ipSeriesId;
	}

	public void setIpSeriesId(int ipSeriesId) {
		this.ipSeriesId = ipSeriesId;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	public int getCircleid() {
		return this.circleid;
	}

	public void setCircleid(int circleid) {
		this.circleid = circleid;
	}


	@Column(name="ip_mask", nullable=false, length=50)
	public String getIpMask() {
		return this.ipMask;
	}

	public void setIpMask(String ipMask) {
		this.ipMask = ipMask;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="reg_date")
	public Date getRegDate() {
		return this.regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}


	@Column(nullable=false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}