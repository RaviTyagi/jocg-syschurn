package com.jss.jocg.cache.entities.config;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;


/**
 * The persistent class for the tb_landing_page_schedule database table.
 * 
 */
@Entity
@Table(name="tb_landing_page_schedule")
@NamedQuery(name="LandingPageSchedule.findAll", query="SELECT l FROM LandingPageSchedule l")
public class LandingPageSchedule implements Serializable {
	private static final long serialVersionUID = 1L;
	private int landingPageId;
	private int chintfid;
	private String daysOfWeek;
	private String hrsOfDay;
	private String landingUrl;
	private String pageType;
	private String requestMode;
	private String scheduleType;
	private int status;
	private int tsourceid;

	public LandingPageSchedule() {
	}

	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	@Id
	@Column(name="landing_page_id", unique=true, nullable=false)
	public int getLandingPageId() {
		return this.landingPageId;
	}

	public void setLandingPageId(int landingPageId) {
		this.landingPageId = landingPageId;
	}


	public int getChintfid() {
		return this.chintfid;
	}

	public void setChintfid(int chintfid) {
		this.chintfid = chintfid;
	}


	@Column(name="days_of_week", length=1)
	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}


	@Column(name="hrs_of_day", length=200)
	public String getHrsOfDay() {
		return this.hrsOfDay;
	}

	public void setHrsOfDay(String hrsOfDay) {
		this.hrsOfDay = hrsOfDay;
	}


	@Column(name="landing_url", length=500)
	public String getLandingUrl() {
		return this.landingUrl;
	}

	public void setLandingUrl(String landingUrl) {
		this.landingUrl = landingUrl;
	}


	@Column(name="page_type", length=1)
	public String getPageType() {
		return this.pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}


	@Column(name="request_mode", nullable=false, length=50)
	public String getRequestMode() {
		return this.requestMode;
	}

	public void setRequestMode(String requestMode) {
		this.requestMode = requestMode;
	}


	@Column(name="schedule_type", length=1)
	public String getScheduleType() {
		return this.scheduleType;
	}

	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public int getTsourceid() {
		return this.tsourceid;
	}

	public void setTsourceid(int tsourceid) {
		this.tsourceid = tsourceid;
	}

}