package com.jss.jocg.cache.bo;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.jss.jocg.cache.dao.CGImageScheduleDao;
import com.jss.jocg.cache.dao.ChargingInterfaceDao;
import com.jss.jocg.cache.dao.ChargingPricepointDao;
import com.jss.jocg.cache.dao.ChargingUrlDao;
import com.jss.jocg.cache.dao.ChargingUrlParamDao;
import com.jss.jocg.cache.dao.CircleinfoDao;
import com.jss.jocg.cache.dao.IPSeriesDao;
import com.jss.jocg.cache.dao.ImsiSeriesDao;
import com.jss.jocg.cache.dao.LandingPageScheduleDao;
import com.jss.jocg.cache.dao.MsisdnSeriesDao;
import com.jss.jocg.cache.dao.NotificationConfigDao;
import com.jss.jocg.cache.dao.PackageChintfmapDao;
import com.jss.jocg.cache.dao.PackageDetailDao;
import com.jss.jocg.cache.dao.PlatformDao;
import com.jss.jocg.cache.dao.RoutingDao;
import com.jss.jocg.cache.dao.ServiceDao;
import com.jss.jocg.cache.dao.TrafficCampaignDao;
import com.jss.jocg.cache.dao.TrafficSourceDao;
import com.jss.jocg.cache.dao.VoucherDetailDao;
import com.jss.jocg.cache.data.JocgCachedObjects;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.cache.service.CacheReloader;
import com.jss.jocg.services.dao.ChargingCDRRepositoryImpl;
import com.jss.jocg.util.JocgString;



public class JocgCacheManager {
	private static final Logger logger = LoggerFactory
			.getLogger(JocgCacheManager.class);

	@Autowired CGImageScheduleDao imageScheduleDao;
	@Autowired ChargingInterfaceDao chintfDao;
	@Autowired ChargingPricepointDao pricePointDao;
	@Autowired ChargingUrlDao chargingUrlDao;
	@Autowired ChargingUrlParamDao chargingUrlParamDao;
	@Autowired PackageChintfmapDao packageChintfDao;
	@Autowired PackageDetailDao packageDetailDao;
	@Autowired ServiceDao serviceDao;
	@Autowired TrafficCampaignDao trafficCampaignDao;
	@Autowired TrafficSourceDao trafficSourceDao;
	@Autowired VoucherDetailDao voucherDetailDao;
	@Autowired JocgCachedObjects cachedObjects;

	@Autowired CircleinfoDao circleInfoDao;
	@Autowired ImsiSeriesDao imsiSeriesDao;
	@Autowired IPSeriesDao ipSeriesDao;
	@Autowired LandingPageScheduleDao landingPageDao;
	@Autowired MsisdnSeriesDao msisdnSeriesDao;
	@Autowired NotificationConfigDao notificationDao;
	@Autowired RoutingDao routingDao;
	@Autowired PlatformDao platformDao;
	@Autowired ChargingCDRRepositoryImpl chargingCDRDaoImpl;
	 @Value("${jocg.transcounter.file}")
	 public String transCounterFile;
	 
	 @Value("${jocg.triggerfile}")
	 public String triggerFile;

	 public String getBsnlSTVPacks(){
			return cachedObjects.getBsnlSTVPacks();
		}
	 public String getBsnlMessage(){
			return cachedObjects.getBsnlMessage();
		}
	public String getTransactionSequence(Integer operatorId,Integer maxCounterDigit){
		return cachedObjects.getTransactionSequence(operatorId, maxCounterDigit,transCounterFile);
	}
	
	public Integer getServerId(){
		return cachedObjects.getJocgTriggers().get("SERVERID");
	}
	public Boolean isAirtelPendingEnabled(){
		return cachedObjects.getJocgTriggers().get("PROCESSAIRTELPENDING").intValue()>0?true:false;
	}
	
	public Boolean isRenewalEnabled(){
		return cachedObjects.getJocgTriggers().get("RENEWAL").intValue()>0?true:false;
	}
	
	public Boolean isNotifyS2SEnabled(){
		return cachedObjects.getJocgTriggers().get("NOTIFYS2S").intValue()>0?true:false;
	}
	
	public Boolean isNotifyS2SChurnEnabled(){
		return cachedObjects.getJocgTriggers().get("NOTIFYS2SCHURN").intValue()>0?true:false;
	}
	
	public Boolean isOfflineProcessEnabled(){
		return cachedObjects.getJocgTriggers().get("OFFLINEPROCESS").intValue()>0?true:false;
	}
	
	public String getSMSTemplate(String templateSuffix){
		System.out.println("RAVI   "+cachedObjects.getJocgSMSTemplates());
		return cachedObjects.getJocgSMSTemplates().get("smstemplate."+templateSuffix);
	}
	public Map<String,String> getSMSTemplate(){
	
		return cachedObjects.getJocgSMSTemplates();
	}
	
	

	/**
	 * Cache Search Methods starts here
	 */
	/**
	 * getScheduledImages() method returns the compete Cached Images dump
	 * @return Object of Type Map<String,Map<Integer,CgimageSchedule>>
	 */
	public Map<String,Map<Integer,CgimageSchedule>> getScheduledImages(){
		return cachedObjects.getImageSchedule();
	}

	/**
	 * getScheduledImages(Integer chargingIntegrfaceId) method returns the all scheduled images against charging interface id passed into it
	 * @return Object of Type Map<String,Map<Integer,CgimageSchedule>>
	 */
	public Map<String,Map<Integer,CgimageSchedule>> getScheduledImages(Integer chargingIntegrfaceId){
		String keyToSearch=	"SIMG_"+chargingIntegrfaceId+"_";
		Map<String,Map<Integer,CgimageSchedule>> images=new HashMap<>();
		
		for(String key:cachedObjects.getImageSchedule().keySet()){
				if(key.startsWith(keyToSearch)){
					images.put(key, cachedObjects.getImageSchedule().get(key));
				}
			}
		return images;
	}
	
	/**
	 * getScheduledImages(Integer chargingIntegrfaceId,Integer tsourceId) method returns the all scheduled images against charging interface id passed into it
	 * @return Object of Type Map<Integer,CgimageSchedule>
	 */
	public Map<Integer,CgimageSchedule> getScheduledImages(Integer chargingIntegrfaceId,Integer tsourceId){
		String keyToSearch=	"SIMG_"+chargingIntegrfaceId+"_"+tsourceId,keyToSearch1="SIMG_"+chargingIntegrfaceId+"_0";
		if(cachedObjects.getImageSchedule().containsKey(keyToSearch)) return cachedObjects.getImageSchedule().get(keyToSearch);
		else return cachedObjects.getImageSchedule().get(keyToSearch1);
	}

	/**
	 * getChargingInterface() method returns the all cached Charging Interfaces where status is Enabled
	 * @return Object of Type Map<Integer,ChargingInterface>
	 */
	public Map<Integer,ChargingInterface> getChargingInterface(){
		return cachedObjects.getChargingInterfaces();
	}

	/**
	 * getChargingInterface(Integer chintfId) method returns the cached Charging Interfaces where id is matched
	 * @return Object of Type ChargingInterface
	 */
	public ChargingInterface getChargingInterface(Integer chintfId){
		return cachedObjects.getChargingInterfaces().get(chintfId);
	}
	/**
	 * getChargingInterface(String chintfName/operatorName) method returns the cached Charging Interfaces where chargingInterfaceName or operatorName is matched
	 * @return Object of Type ChargingInterface
	 */
	public ChargingInterface getChargingInterfaceByName(String operator){
		ChargingInterface chintf=null;
		Map<Integer,ChargingInterface> cachedChintf=cachedObjects.getChargingInterfaces();
		Iterator<Integer> it=cachedChintf.keySet().iterator();
		Integer newKey=null;
		while(it.hasNext()){
			newKey=it.next();
			chintf=cachedChintf.get(newKey);
			if(chintf.getName().equalsIgnoreCase(operator)) break;
			else chintf=null;
		}
		cachedChintf=null;it=null;
		return chintf;
	}

	/**
	 * getChargingInterfaceByOpcode(String operatorCode) method returns the cached Charging Interfaces where chargingInterfaceCode is matched
	 * @return Object of Type ChargingInterface
	 */
	public ChargingInterface getChargingInterfaceByOpcode(String operatorCode){
		ChargingInterface chintf=null;
		Map<Integer,ChargingInterface> cachedChintf=cachedObjects.getChargingInterfaces();
		Iterator<Integer> it=cachedChintf.keySet().iterator();
		Integer newKey=null;
		while(it.hasNext()){
			newKey=it.next();
			chintf=cachedChintf.get(newKey);
			if(chintf.getOpcode().equalsIgnoreCase(operatorCode)) break;
			else chintf=null;
		}
		cachedChintf=null;it=null;
		return chintf;
	}
	/**
	 * getPricePointList() method returns the all cached Charging Price Point where status is Enabled
	 * @return Object of Type Map<Integer,ChargingPricePoint>
	 */
	public Map<Integer,ChargingPricepoint> getPricePointList(){
		return cachedObjects.getChargingPricePoint();
	}

	/**
	 * getChargingInterface(Integer ppId) method returns the cached Charging Interfaces where ppid is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public ChargingPricepoint getPricePointListById(Integer ppId){
		return cachedObjects.getChargingPricePoint().get(ppId);
	}
	
	/**
	 *
	 * getPricePointListByOperatorKey(String operatorKeyword,Double pricePoint) method returns the cached Charging Pricepoint where operator Key is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public ChargingPricepoint getPricePointListByOperatorKey(String operatorKeyword,Integer validity){
		String key=operatorKeyword+"_";
		String key1=operatorKeyword+"_";
		Iterator i=cachedObjects.getOperatorKey().keySet().iterator();
		ChargingPricepoint retobj=null;
		while(i.hasNext()){
			key=(String)i.next();
			if(key.startsWith(operatorKeyword+"_")){
				retobj=cachedObjects.getOperatorKey().get(key);
				if(retobj.getValidity()==validity) break;
				else retobj=null;
			}
		}
		if(retobj==null) retobj=getPricePointListByOperatorKey(operatorKeyword);
		
		return retobj;
	}

	/**
	 *
	 * getPricePointListByOperatorKey(String operatorKeyword,Double pricePoint) method returns the cached Charging Pricepoint where operator Key is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public ChargingPricepoint getPricePointListByOperatorKey(String operatorKeyword,Double price){
		String key=operatorKeyword+"_"+price.intValue();
		String key1=operatorKeyword+"_"+price;
		
		ChargingPricepoint retobj=null;
		if(cachedObjects.getOperatorKey().containsKey(key)){
			retobj=cachedObjects.getOperatorKey().get(key);
		}else if(cachedObjects.getOperatorKey().containsKey(key1)){
			retobj=cachedObjects.getOperatorKey().get(key1);
		}else{
			retobj=getPricePointListByOperatorKey(operatorKeyword);
		}

		return retobj;
	}
	/**
	 *
	 * getPricePointListByOperatorKey(String operatorKeyword) method returns the cached Charging Pricepoint where operator Key is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public ChargingPricepoint getPricePointListByOperatorKey(String operatorKeyword){
		  String key=operatorKeyword+"_";
		  System.out.println("Searching for operatorKeyword "+operatorKeyword+" IN "+cachedObjects.getOperatorKey().keySet());
		  Iterator<String> e=cachedObjects.getOperatorKey().keySet().iterator();
		  ChargingPricepoint retobj=null;String key1="";
		  while(e.hasNext()){
			   key1=e.next();
			   if(key1.equalsIgnoreCase(operatorKeyword)){
			        retobj=cachedObjects.getOperatorKey().get(key1);
				    if(retobj.getIsfallback()==0){
				    	logger.debug("Key Matched with master pp, return obj  "+retobj);
				    	break;
				    }else retobj=null;
			   }
		  }
		  
		  if(retobj==null){
			  e=cachedObjects.getOperatorKey().keySet().iterator();
			  while(e.hasNext()){
				   key1=e.next();
				   if(key1.startsWith(operatorKeyword)){
				        retobj=cachedObjects.getOperatorKey().get(key1);
					    logger.debug("Key Matched with fallabck, return obj  "+retobj);
					    break;
				 }
			  }
		  }
		  return retobj;
		 }
	
	public ChargingPricepoint getPricePointListByOperatorKey(String operatorKeyword,Integer chintfId,Double price){
		  List<ChargingPricepoint> chppList=getPricePointListByChargingInterface(chintfId);
		  ChargingPricepoint chppMaster=null,chppFallback=null;
		  
		  for(ChargingPricepoint ch:chppList){
			  if(ch.getOperatorKey().equalsIgnoreCase(operatorKeyword) && ch.getIsfallback()==0 && ch.getPricePoint()==price.doubleValue())
				chppMaster=ch;
			  else if(ch.getOperatorKey().equalsIgnoreCase(operatorKeyword) && ch.getIsfallback()!=0  && ch.getPricePoint()==price.doubleValue())
				chppFallback=ch;
			  
			 ch=null;
		  }
		  
		 		  
		  return chppMaster!=null?chppMaster:chppFallback;

	}
	public ChargingPricepoint getPricePointListByOperatorKeyAndOperator(String operatorKeyword,Integer chintfId){
		
		  List<ChargingPricepoint> chppList=getPricePointListByChargingInterface(chintfId);
		  ChargingPricepoint chppMaster=null,chppFallback=null;
		  
		  for(ChargingPricepoint ch:chppList){
			  if(ch.getOperatorKey().equalsIgnoreCase(operatorKeyword) && ch.getIsfallback()==0)
				chppMaster=ch;
			  else if(ch.getOperatorKey().equalsIgnoreCase(operatorKeyword) && ch.getIsfallback()!=0)
				chppFallback=ch;
				
			 ch=null;
		  }
		  
		  return chppMaster!=null?chppMaster:chppFallback;
		 }


	/**
	 *
	 * getPricePointListByChargingInterface(Integer chargingInterfaceId) method returns the cached Charging Pricepoint where operator Key is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public List<ChargingPricepoint> getPricePointListByChargingInterface(Integer chintfId){
		List<ChargingPricepoint> chintfList=new ArrayList<>();
		ChargingPricepoint chpp=null;
		for(Integer nk:cachedObjects.getChargingPricePoint().keySet()){
			chpp=cachedObjects.getChargingPricePoint().get(nk);
			if(chpp.getChintfid()==chintfId) chintfList.add(chpp);
			chpp=null;
		}

		return chintfList;
	}

	/**
	 *
	 * getPricePointListByChargingInterface(Integer chargingInterfaceId) method returns the cached master Charging Pricepoint where operator Key is matched
	 * @return Object of Type ChargingPricepoint
	 */
	public ChargingPricepoint getPricePointListByChargingInterface(Integer chintfId,Double pricePoint){

		ChargingPricepoint chpp=null;
		for(Integer nk:cachedObjects.getChargingPricePoint().keySet()){
			chpp=cachedObjects.getChargingPricePoint().get(nk);
			if(chpp.getChintfid()==chintfId && chpp.getPricePoint()==pricePoint && chpp.getIsfallback()==0) break;
			else chpp=null;
		}

		return chpp;
	}


	/**
	 *
	 * getChargingUrlByChintfId(Integer chintfId) method returns the cached Charging URL by chintfId
	 * @return Object of Type List<ChargingUrl>
	 */
	public List<ChargingUrl> getChargingUrlByChintfId(Integer chintfId){
		List<ChargingUrl> churlList=new ArrayList<>();
		ChargingUrl churl=null;
		for(Integer nk:cachedObjects.getChargingUrls().keySet()){
			churl=cachedObjects.getChargingUrls().get(nk);
			if(churl.getChintfid()==chintfId) churlList.add(churl);
			churl=null;
		}

		return churlList;
	}

	/**
	 *
	 * getChargingUrlById(Integer churlId) method returns the cached Charging URL by churlId
	 * @return Object of Type List<ChargingUrl>
	 */
	public ChargingUrl getChargingUrlById(Integer churlId){
		return cachedObjects.getChargingUrls().get(churlId);
	}

	/**
	 * getChargingUrls() method returns the all cached Charging Urls where status is Enabled
	 * @return Object of Type Map<Integer,ChargingUrl>
	 */
	public Map<Integer,ChargingUrl> getChargingUrls(){
		return cachedObjects.getChargingUrls();
	}

	/**
	 * getPackageChargingInterfaceMap() method returns the all cached Charging Urls where status is Enabled
	 * @return Object of Type Map<Integer,PackageChintfmap>
	 */
	public Map<Integer,PackageChintfmap> getPackageChargingInterfaceMap(){
		return cachedObjects.getPackageChintfMap();
	}

	/**
	 * getPackageChargingInterfaceMapById(Integer pkhChintfmapId) method returns the all cached Charging Urls where status is Enabled
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMapById(Integer pkhChintfmapId){
		return cachedObjects.getPackageChintfMap().get(pkhChintfmapId);
	}
	
	public PackageDetail getPackageDetailsByID(Integer packID){
		System.out.println("PackageDetail : "+cachedObjects.getPackageDetails());
		PackageDetail pack=null;
		Iterator<PackageDetail> pa=cachedObjects.getPackageDetails().values().iterator();
		while(pa.hasNext()){
			pack=pa.next();
			if(pack.getPkgid()==packID.intValue()) break;
			else pack=null;
		}
		if(pack==null) pack=new PackageDetail();
		return pack;
	}	
	/**
	 *getPackageChargingInterfaceMap(Integer chargingOperatorId,Integer ppId) method returns the PackageChintfmap where status is Enabled
	 *and chintfId,and opPPId is matched
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMap(Integer chargingOperatorId,Integer ppId){
		PackageChintfmap nm=null;ChargingPricepoint chpp=null;
		//System.out.println("Inside getPackageChargingInterfaceMap() .... Searching for networkId:"+networkId+", packageId :"+packageId+",pricePoint :"+pricePoint);
		for(Integer pmk:cachedObjects.getPackageChintfMap().keySet()){
			nm=cachedObjects.getPackageChintfMap().get(pmk);
			if(nm.getChintfid()==chargingOperatorId.intValue() && nm.getOpPpid()==ppId.intValue()) break;
			else{ nm=null; chpp=null;}
		}
		return nm;
	}
	
	
	/**
	 *getPackageChargingInterfaceMap(Integer networkId,Integer packageId,Double pricePoint) method returns the PackageChintfmap where status is Enabled
	 *and chintfId,packageId and opPPId is matched
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMap(Integer networkId,Integer packageId,Double pricePoint){
		PackageChintfmap nm=null;ChargingPricepoint chpp=null;
		System.out.println("Inside getPackageChargingInterfaceMap() .... Searching for networkId:"+networkId+", packageId :"+packageId+",pricePoint :"+pricePoint);
		for(Integer pmk:cachedObjects.getPackageChintfMap().keySet()){
			nm=cachedObjects.getPackageChintfMap().get(pmk);
			chpp=this.getPricePointListById(nm.getOpPpid());
			//System.out.println("Comparing with NetworkId "+nm.getNetworkId()+", PkgId "+nm.getPkgid()+", chpp "+chpp);
			if(nm.getNetworkid()==networkId.intValue() && nm.getPkgid()==packageId.intValue() && chpp.getPricePoint()==pricePoint.doubleValue()) break;
			else{ nm=null; chpp=null;}
		}
		return nm;
	}
	
	/**
	 *getPackageChargingInterfaceMap(Integer networkId,Integer packageId,Double pricePoint) method returns the PackageChintfmap where status is Enabled
	 *and chintfId,packageId and opPPId is matched
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMapByPriceAndValidity(Integer networkId,Integer packageId,Double pricePoint,int validity){
		PackageChintfmap nm=null;ChargingPricepoint chpp=null;
		System.out.println("Inside getPackageChargingInterfaceMap() .... Searching for networkId:"+networkId+", packageId :"+packageId+",validity :"+validity);
		for(Integer pmk:cachedObjects.getPackageChintfMap().keySet()){
			nm=cachedObjects.getPackageChintfMap().get(pmk);
			chpp=this.getPricePointListById(nm.getOpPpid());
			//System.out.println("Comparing with NetworkId "+nm.getNetworkId()+", PkgId "+nm.getPkgid()+", chpp "+chpp);
			if(nm.getNetworkid()==networkId.intValue() && nm.getPkgid()==packageId.intValue() && chpp.getPricePoint()==pricePoint && chpp.getValidity()==validity) break;
			else{ nm=null; chpp=null;}
		}
		return nm;
	}

	public PackageDetail getPackageChargingInterfaceMap(Integer networkId,Double pricePoint){
			PackageDetail nm=null;PackageDetail chpp=null;
			//System.out.println("Inside getPackageChargingInterfaceMap() .... Searching for networkId:"+networkId+", packageId :"+packageId+",pricePoint :"+pricePoint);
			for(String pmk:cachedObjects.getPackageDetails().keySet()){
				System.out.println("After Loop");
				nm=cachedObjects.getPackageDetails().get(pmk);
				if( nm.getPricepoint()==pricePoint.doubleValue()) break;
				else{ nm=null; chpp=null;}
			}
			return nm;
	}

	/**
	 *getPackageChargingInterfaceMap(Integer chintfId,Integer packageId,Integer operatorPricePointId) method returns the PackageChintfmap where status is Enabled
	 *and chintfId,packageId and opPPId is matched
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMap(Integer networkId,Integer chintfId,Integer operatorPricePointId){
		PackageChintfmap nm=null;
		for(Integer pmk:cachedObjects.getPackageChintfMap().keySet()){
			nm=cachedObjects.getPackageChintfMap().get(pmk);
			if(nm.getNetworkid()==networkId.intValue() && nm.getChintfid()==chintfId.intValue() &&  nm.getOpPpid()==operatorPricePointId.intValue()) break;
			else nm=null;
		}
		return nm;
	}
	/**
	 *getPackageChargingInterfaceMap(Integer chintfId,Integer packageId,Integer operatorPricePointId) method returns the PackageChintfmap where status is Enabled
	 *and chintfId,packageId and opPPId is matched
	 * @return Object of Type PackageChintfmap
	 */
	public PackageChintfmap getPackageChargingInterfaceMap(Integer networkId,Integer chintfId,Integer packageId,Integer operatorPricePointId){
		PackageChintfmap nm=null;
		for(Integer pmk:cachedObjects.getPackageChintfMap().keySet()){
			nm=cachedObjects.getPackageChintfMap().get(pmk);
			if(nm.getNetworkid()==networkId.intValue() && nm.getChintfid()==chintfId.intValue() && nm.getPkgid()==packageId.intValue() && nm.getOpPpid()==operatorPricePointId.intValue()) break;
			else nm=null;
		}
		return nm;
	}


	/**
	 * getPackageDetails() method returns the all cached Charging Urls where status is Enabled
	 * @return Object of Type Map<String,PackageDetail>
	 */
	public Map<String,PackageDetail> getPackageDetails(){
		return cachedObjects.getPackageDetails();
	}

	/**
	 * getPackageDetailsByName(String packageName) method returns the all cached Package where status is Enabled and packageName is matched
	 * @return Object of Type PackageDetail
	 */
	public PackageDetail getPackageDetailsByName(String packageName){
		return cachedObjects.getPackageDetails().get(packageName);
	}

	/**
	 * getServices() method returns the all cached Services where status is Enabled
	 * @return Object of Type Map<Integer,Service>
	 */
	public Map<Integer,Service> getServices(){
		return cachedObjects.getServiceDetails();
	}

	/**
	 * getServicesById(Integer serviceId) method returns the all cached Service where status is Enabled and packageName is matched
	 * @return Object of Type Service
	 */
	public Service getServicesById(Integer serviceId){
		return cachedObjects.getServiceDetails().get(serviceId);
	}

	/**
	 * getServicesByName(String packageName) method returns the all cached Service where status is Enabled and packageName is matched
	 * @return Object of Type Service
	 */
	public Service getServicesByName(String serviceName){
		Service service=null;
		try{
			List<Service> e=(List<Service>)cachedObjects.getServiceDetails().values();
			for(Service s:e) if(s.getServiceName().equalsIgnoreCase(serviceName)) {service=s; break;}
			e=null;
		}catch(Exception e){}
		finally{
			if(service==null) service=new Service();
		}
		return service;

	}
	/**
	 * getServices() method returns the all cached Services where status is Enabled
	 * @return Object of Type Map<Integer,Service>
	 */
	public Map<Integer,TrafficCampaign> getTrafficCampaigns(){
		return cachedObjects.getTrafficCampaigns();
	}

	/**
	 * getTrafficCampaignById(Integer campaignId) method returns the all cached Traffic Campaigns where status is Enabled and campaignId is matched
	 * @return Object of Type PackageDetail
	 */
	public TrafficCampaign getTrafficCampaignById(Integer campaignId){
		return cachedObjects.getTrafficCampaigns().get(campaignId);
	}

	/**
	 * getTrafficSources() method returns the all cached Traffic Sources where status is Enabled
	 * @return Object of Type Map<Integer,Service>
	 */
	public Map<Integer,TrafficSource> getTrafficSources(){
		return cachedObjects.getTrafficSources();
	}

	/**
	 * getTrafficSourceById(Integer trafficSourceId) method returns the all cached TrafficSources where status is Enabled and trafficSourceId is matched
	 * @return Object of Type PackageDetail
	 */
	public TrafficSource getTrafficSourceById(Integer trafficSourceId){
		return cachedObjects.getTrafficSources().get(trafficSourceId);
	}

	//------------------------------------------------------

		/**
		 * getVoucherDetails(String voucherId) method returns the all cached VoucherDetail where status is Enabled and trafficSourceId is matched
		 * @return Object of Type VoucherDetail
		 */
		public VoucherDetail getVoucherDetails(String voucherId){
			return voucherDetailDao.getByVoucherId(voucherId);
		}
		
		/**
		 * updateVoucherDetails(VoucherDetail voucher) method returns void 
		 */
		public void updateVoucherDetails(VoucherDetail voucher){
			voucherDetailDao.save(voucher);
		}

		//------------------------------------------------------
		/**
		 * getCircleInfo() method returns the all cached Circleinfo Sources where status is Enabled
		 * @return Object of Type  Map<Integer,Circleinfo>
		 */
		public Map<Integer,Circleinfo> getCircleInfo(){
			return cachedObjects.getCircleInfo();
		}

		/**
		 * getCircleInfo(Integer circleId) method returns the all cached Circleinfo where status is Enabled and trafficSourceId is matched
		 * @return Object of Type Circleinfo
		 */
		public Circleinfo getCircleInfo(Integer circleId){
			return cachedObjects.getCircleInfo().get(circleId);
		}
		
		/**
		 * getCircleInfo(Integer circleId) method returns the all cached Circleinfo where status is Enabled and trafficSourceId is matched
		 * @return Object of Type Circleinfo
		 */
		public Circleinfo getCircleInfo(Integer chintfId,String circleCode){
			Circleinfo circle=null;
			Iterator<Circleinfo> i=cachedObjects.getCircleInfo().values().iterator();
			while(i.hasNext()){
				circle=i.next();
				if(circle.getChintfid()==chintfId.intValue() && circle.getCirclecode().equalsIgnoreCase(circleCode)) break;
				else circle=null;
			}
			return circle;
		}
		
		//------------------------------------------------------
		/**
		 * getImsiSeries() method returns the all cached ImsiSeries Sources where status is Enabled
		 * @return Object of Type  Map<String,ImsiSeries>
		 */
		public Map<String,ImsiSeries> getImsiSeries(){
			return cachedObjects.getImsiSeries();
		}

		/**
		 * getVoucherDetails(String voucherId) method returns the all cached ImsiSeries where status is Enabled and trafficSourceId is matched
		 * @return Object of Type ImsiSeries
		 */
		public ImsiSeries getImsiSeries(String imsiSeries){
			return cachedObjects.getImsiSeries().get(imsiSeries);
		}

		//------------------------------------------------------
		/**
		 * getIPSeries() method returns the all cached IPSeries Sources where status is Enabled
		 * @return Object of Type  Map<String,IPSeries>
		 */
		public Map<String,IPSeries> getIPSeries(){
			return cachedObjects.getIpSeries();
		}

		/**
		 * getIPSeries(String ipSeries) method returns the all cached IPSeries where status is Enabled and trafficSourceId is matched
		 * @return Object of Type IPSeries
		 */
		public IPSeries getIPSeries(String ipSeries){
			return cachedObjects.getIpSeries().get(ipSeries);
		}

		//------------------------------------------------------
				/**
				 * getLandingPageSchedule() method returns the all cached LandingPageSchedule Sources where status is Enabled
				 * @return Object of Type  Map<String,LandingPageSchedule>
				 */
				public Map<String,LandingPageSchedule> getLandingPageSchedule(){
					return cachedObjects.getLandingPageSchedule();
				}

				/**
				 * getLandingPageSchedule(Integer chintfId,Integer tsourceId) method returns the all cached LandingPageSchedule where status is Enabled and trafficSourceId is matched
				 * @return Object of Type LandingPageSchedule
				 */
				public List<LandingPageSchedule> getLandingPageSchedule(Integer chintfId,Integer tsourceId){
					List<LandingPageSchedule> retr=new ArrayList<>();
					try{
						Iterator<String> i=cachedObjects.getLandingPageSchedule().keySet().iterator();
						String newKey="";String keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_";
						while(i.hasNext()){
							newKey=i.next();
							if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getLandingPageSchedule().get(newKey));
							newKey="";
						}
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_";
							i=cachedObjects.getLandingPageSchedule().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getLandingPageSchedule().get(newKey));
								newKey="";
							}

						}
					}catch(Exception e){

					}
					return retr;
				}

				//------------------------------------------------------
				/**
				 * getMsisdnSeries() method returns the all cached MsisdnSeries Sources where status is Enabled
				 * @return Object of Type  Map<String,MsisdnSeries>
				 */
				public Map<String,MsisdnSeries> getMsisdnSeries(){
					return cachedObjects.getMsisdnSeries();
				}

				/**
				 * getIPSeries(String msisdnSeries) method returns the all cached MsisdnSeries where status is Enabled and trafficSourceId is matched
				 * @return Object of Type MsisdnSeries
				 */
				public MsisdnSeries getMsisdnSeries(String msisdnSeries){
					return cachedObjects.getMsisdnSeries().get(msisdnSeries);
				}


				//------------------------------------------------------
				/**
				 * getNotificationConfig() method returns the all cached NotificationConfig Sources where status is Enabled
				 * @return Object of Type  Map<String,NotificationConfig>
				 */
				public Map<String,NotificationConfig> getNotificationConfig(){
					return cachedObjects.getNotificationConfigs();
				}
				/**
				 * getNotificationConfig(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId) method returns the all cached NotificationConfig where status is Enabled and parameters are matched
				 * @return Object of Type  List<NotificationConfig>
				 */
				
				public List<NotificationConfig> getNotificationConfig(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId){
					List<NotificationConfig> retr=new ArrayList<>();
					try{
						Iterator<String> i=cachedObjects.getNotificationConfigs().keySet().iterator();
						logger.debug("Master Notification Config Keyset "+cachedObjects.getNotificationConfigs().keySet());
						String newKey="";String keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId.trim()+"_";
						logger.debug("Searching against key "+keyToCompare);
						NotificationConfig newObj=null;
						while(i.hasNext()){
							newKey=i.next();
							if(newKey.endsWith(keyToCompare)){
								newObj=cachedObjects.getNotificationConfigs().get(newKey);
								
								retr.add(newObj);
								newObj=null;
							}
							newKey="";
						}
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_";
							logger.debug("Searching against key "+keyToCompare);
							i=cachedObjects.getNotificationConfigs().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)){
									newObj=cachedObjects.getNotificationConfigs().get(newKey);
									
									retr.add(newObj);
								}
								newKey="";
							}

						}
					}catch(Exception e){

					}
					return retr;
				}
				/**
				 * getNotificationConfig(Integer tsourceId,Integer campaignId,String publisherId) method returns the all cached NotificationConfig where status is Enabled and parameters are matched
				 * @return Object of Type  List<NotificationConfig>
				 */
				public List<NotificationConfig> getNotificationConfig(Integer tsourceId,Integer campaignId,String publisherId){
					List<NotificationConfig> retr=new ArrayList<>();
					try{
						Iterator<String> i=cachedObjects.getNotificationConfigs().keySet().iterator();
						String newKey="";String keyToCompare=tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId.trim()+"_";
						while(i.hasNext()){
							newKey=i.next();
							if(newKey.endsWith(keyToCompare)) retr.add(cachedObjects.getNotificationConfigs().get(newKey));
							newKey="";
						}
						if(retr.size()<=0){
							keyToCompare=tsourceId.intValue()+"_"+campaignId.intValue()+"_";
							i=cachedObjects.getNotificationConfigs().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getNotificationConfigs().get(newKey));
								newKey="";
							}

						}
					}catch(Exception e){

					}
					return retr;
				}

				//------------------------------------------------------
				/**
				 * getPlatformConfig() method returns the all cached Platform Sources where status is Enabled
				 * @return Object of Type  Map<String,Platform>
				 */
				public Map<String,Platform> getPlatformConfig(){
					return cachedObjects.getPlatformConfig();
				}

				//------------------------------------------------------
				/**
				 * getPlatformConfig(String platformName) method returns the matched Platform Sources where status is Enabled
				 * @return Object of Type  Platform
				 */
				public Platform getPlatformConfig(String platformName){
					return cachedObjects.getPlatformConfig().get(platformName);
				}


				//------------------------------------------------------
				/**
				 * getRoutingConfig() method returns the all cached Routing Sources where status is Enabled
				 * @return Object of Type  Map<String,Routing>
				 */
				public Map<String,Routing> getRoutingConfig(){
					return cachedObjects.getRoutingConfig();
				}

				/**
				 * getRoutingConfig(Integer chintfId,Integer tsourceId,Integer campaignId,Integer circleId,String publisherId) method returns the all cached Routing where status is Enabled and parameters are matched
				 * @return Object of Type  List<Routing>
				 */
				public List<Routing> getRoutingConfig(Integer chintfId,Integer tsourceId,Integer campaignId,Integer circleId,String publisherId){
					List<Routing> retr=new ArrayList<>();
					try{
					//	logger.debug("All Routing config "+cachedObjects.getRoutingConfig());
						Iterator<String> i=cachedObjects.getRoutingConfig().keySet().iterator();
						//key=cs.getChintfid()+"_"+cs.getTsourceid()+"_"+cs.getCampaignid()+"_"+cs.getCircleid()+"_"+cs.getPubid()+"_"+cs.getRoutingId();
						String newKey="";String keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+circleId.intValue()+"_"+publisherId.trim()+"_";
						while(i.hasNext()){
							newKey=i.next();
							if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
							newKey="";
						}
					//	logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_0_"+publisherId.trim()+"_";
							i=cachedObjects.getRoutingConfig().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
								newKey="";
							}

						}
					//	logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+circleId.intValue()+"_";
							i=cachedObjects.getRoutingConfig().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
								newKey="";
							}

						}
					//	logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_";
							i=cachedObjects.getRoutingConfig().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
								newKey="";
							}

						}
					//	logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
						/*if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_"+tsourceId.intValue()+"_";
							i=cachedObjects.getRoutingConfig().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
								newKey="";
							}

						}
						//logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
						if(retr.size()<=0){
							keyToCompare=chintfId.intValue()+"_";
							i=cachedObjects.getRoutingConfig().keySet().iterator();
							while(i.hasNext()){
								newKey=i.next();
								if(newKey.startsWith(keyToCompare)) retr.add(cachedObjects.getRoutingConfig().get(newKey));
								newKey="";
							}

						}*/
					//	logger.debug("Routing Search Key "+keyToCompare+", Size after search "+retr.size());
					}catch(Exception e){

					}
					return retr;
				}

				/**
				 * getChargingUrlParams(Integer churlId) method returns all charging url params against urlId
				 * @param churlId
				 * @return type List<ChargingUrlParam>
				 */
				public List<ChargingUrlParam> getChargingUrlParams(Integer churlId){
					//logger.info("Cached URL Params "+cachedObjects.getChargingUrlParams());
					logger.info("Searching Params  for URL id "+churlId);
					List<ChargingUrlParam> urlParams=cachedObjects.getChargingUrlParams().get(churlId);
					logger.info("Found Params count "+((urlParams!=null)?urlParams.size():0));
					return urlParams;
				}
				
				/**
				 * getTrafficStats(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId) method returns traffic stats where params are matched else return null
				 * @param Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId
				 * @return type TrafficStats
				 */
				public TrafficStats getTrafficStats(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId){
					//logger.info("Cached URL Params "+cachedObjects.getChargingUrlParams());
					TrafficStats ts=null;
					publisherId=(publisherId==null)?"NA":publisherId.trim();
					String key="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId;
					String key1="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_NA";
					String key2="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue();
					if(cachedObjects.getTrafficStats().containsKey(key)){
						return cachedObjects.getTrafficStats().get(key);
					}else if(cachedObjects.getTrafficStats().containsKey(key1)){
						return cachedObjects.getTrafficStats().get(key1);
					}else if(cachedObjects.getTrafficStats().containsKey(key2)){
						return cachedObjects.getTrafficStats().get(key2);
					}else
						return null;
				}
				
				public Map<String,TrafficStats> getTrafficStats(){
					return cachedObjects.getTrafficStats();
				}
				
				 
	/**
	 * Cache Builder Methods starts here
	 * rebuildCache() invokes all processes involved to create cache
	 */

	public Boolean rebuildCache(){
		Boolean flag= reloadImageScheduleCache();
		flag=reloadChargingInterfaceCache();
		flag=reloadChargingPricepointCache();
		flag=reloadChargingUrlCache();
		flag=reloadChargingUrlParamsCache();
		flag=reloadPackageChintfMapCache();
		flag=reloadPackageDetailCache();
		flag=reloadServiceCache();
		flag=reloadTrafficCampaignCache();
		flag=reloadTrafficSourceCache();
		//flag=reloadVoucherDetailCache(); // Not loading in cache as expected records count is very high and hence not recommended to be maintained in cache

		flag=reloadCircleDetailCache();
		flag=reloadImsiSeriesCache();
		flag=reloadIPSeriesCache();
		flag=reloadLandingPageScheduleCache();
		flag=reloadMsisdnSeriesCache();
		flag=reloadNotificationConfigCache();
		flag=reloadRoutingCache();
		flag=reloadPlatformCache();
		flag=reloadTransCounter(transCounterFile);
		flag= reloadTriggers();
		//flag= reloadStats();
		return flag;
	}

	/**
	 * reloadImageScheduleCache() method rebuilds the Scheduled Images cache
	 * @return Boolean
	 */

	public Boolean reloadImageScheduleCache(){
		Map<String,Map<Integer,CgimageSchedule>> cdata=new HashMap<>();
		try{
		List<CgimageSchedule> schList=imageScheduleDao.getByStatus(new Integer(1));
		String key="";
		logger.debug("SYSTEM::reloadImageScheduleCache():: Reloading Image Schedule Cache ......");
		Map<Integer,CgimageSchedule> cdataItem=null;
		for(CgimageSchedule cs:schList){
			key="SIMG_"+cs.getPkgchintfid()+"_"+cs.getTsourceid();
			cdataItem=(cdata.containsKey(key))?cdata.get(key):new HashMap<>();
			cdataItem.put(cs.getImgid(), cs);
			cdata.put(key, cdataItem);
			//logger.debug("SYSTEM::reloadImageScheduleCache():: Adding to Cache Key "+key+", Value "+cdataItem);
			cdataItem=null;

		}
		}catch(Exception e){
			logger.error("reloadImageScheduleCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getImageSchedule()==null) cachedObjects.setImageSchedule(cdata);
		return true;
	}

	/**
	 * reloadChargingInterfaceCache() method rebuilds the Charging Interfaces cache
	 * @return Boolean
	 */

	public Boolean reloadChargingInterfaceCache(){

		Map<Integer,ChargingInterface> cdata=new HashMap<>();
		logger.debug("SYSTEM::reloadChargingInterfaceCache():: Reloading ChargingInterface Cache ......");
		try{
			List<ChargingInterface> schList=chintfDao.getByStatus(new Integer(1));
			String key="";


			for(ChargingInterface cs:schList){
				cdata.put(cs.getChintfid(), cs);
			//	logger.debug("SYSTEM::reloadChargingInterfaceCache():: Adding to Cache Key "+cs.getChintfid()+", Value "+cs);

			}
		}catch(Exception e){
			logger.error("reloadChargingInterfaceCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getChargingInterfaces()==null) cachedObjects.setChargingInterfaces(cdata);;
		return true;
	}

	public Boolean reloadChargingPricepointCache(){
		  Map<Integer,ChargingPricepoint> cdata=new HashMap<>();
		  Map<String,ChargingPricepoint> cdatakey=new HashMap<>();
		  try{
		   List<ChargingPricepoint> schList=pricePointDao.getByStatus(new Integer(1));

		   for(ChargingPricepoint cs:schList){
		    cdata.put(cs.getPpid(), cs);
		    //logger.debug("SYSTEM::reloadChargingPricepointCache():: Adding to Cache Key "+cs.getChintfid()+", Value "+cs);
		    if(cs.getIsfallback()==0 && ! cdatakey.containsKey(cs.getOperatorKey())) cdatakey.put(cs.getOperatorKey(), cs);
		    cdatakey.put(cs.getOperatorKey()+"_"+(int)cs.getPricePoint(), cs);
		    
		    //logger.debug("SYSTEM::reloadChargingPricepointCache():: Adding to Cache Key "+cs.getOperatorKey()+", Value "+cs);
		   }
		  }catch(Exception e){
		   logger.error("reloadChargingPricepointCache() :: Error :"+e);
		  }
		  if(cdata.size()>0 || cachedObjects.getChargingPricePoint()==null) {
			  cachedObjects.setChargingPricePoint(cdata);
			  cachedObjects.setOperatorKey(cdatakey);
		  }
		  return true;
		 }


	public Boolean reloadChargingUrlCache(){
		Map<Integer,ChargingUrl> cdata=new HashMap<>();
		try{
			List<ChargingUrl> schList=chargingUrlDao.getByStatus(new Integer(1));

			for(ChargingUrl cs:schList){
				cdata.put(cs.getChurlId(), cs);
				//logger.debug("SYSTEM::reloadChargingUrlCache():: Adding to Cache Key "+cs.getChurlId()+", Value "+cs);
			}
		}catch(Exception e){
			logger.error("reloadChargingUrlCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getChargingUrls()==null) 
			cachedObjects.setChargingUrls(cdata);

		return true;
	}


	public Boolean reloadChargingUrlParamsCache(){
		Map<Integer,List<ChargingUrlParam>> cdata=new HashMap<>();
		try{
			Iterator<Integer> i=cachedObjects.getChargingUrls().keySet().iterator();
			Integer nextUrlId=0;
			while(i.hasNext()){
				nextUrlId=i.next();
				try{
						cdata.put(nextUrlId, chargingUrlParamDao.getByChurlId(nextUrlId));
						//logger.debug("SYSTEM :: reloadChargingUrlParamsCache() :: URLParams for url id "+nextUrlId+", "+cdata.get(nextUrlId));
				}catch(Exception e1){
					logger.error("reloadChargingUrlParams() :: Error while loading Param List for Url Id ("+nextUrlId+"):"+e1);

				}
			}

		}catch(Exception e){
			logger.error("reloadChargingUrlParams() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getChargingUrlParams()==null) cachedObjects.setChargingUrlParams(cdata);

		return true;
	}

	public Boolean reloadPackageChintfMapCache(){
		Map<Integer,PackageChintfmap> cdata=new HashMap<>();
		try{
			List<PackageChintfmap> schList=packageChintfDao.getByStatus(new Integer(1));

			for(PackageChintfmap cs:schList){
				cdata.put(cs.getPkgchintfid(), cs);
				//logger.debug("SYSTEM::reloadPackageChintfMapCache():: Adding to Cache Key "+cs.getPkgchintfid()+", Value "+cs);
			}
		}catch(Exception e){
			logger.error("reloadPackageChintfMapCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getPackageChintfMap()==null) cachedObjects.setPackageChintfMap(cdata);
		return true;
	}

	public Boolean reloadPackageDetailCache(){
		Map<String,PackageDetail> cdata=new HashMap<>();
		try{
				List<PackageDetail> schList=packageDetailDao.getByStatus(new Integer(1));
				for(PackageDetail cs:schList){
					cdata.put(cs.getPkgname(), cs);
					//logger.debug("SYSTEM::reloadPackageDetailCache():: Adding to Cache Key "+cs.getPkgname()+", Value "+cs);
				}
		}catch(Exception e){
			logger.error("reloadPackageDetailCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getPackageDetails()==null) cachedObjects.setPackageDetails(cdata);
		return true;
	}
	
	

	public Boolean reloadServiceCache(){
		Map<Integer,Service> cdata=new HashMap<>();
		try{
			List<Service> schList=serviceDao.getByStatus(new Integer(1));

			for(Service cs:schList){
				cdata.put(cs.getServiceid(), cs);
				//logger.debug("SYSTEM::reloadServiceCache():: Adding to Cache Key "+cs.getServiceid()+", Value "+cs);
			}
		}catch(Exception e){
			logger.error("reloadServiceCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getServiceDetails()==null) cachedObjects.setServiceDetails(cdata);
		return true;
	}

	public Boolean reloadTrafficCampaignCache(){
		Map<Integer,TrafficCampaign> cdata=new HashMap<>();
		try{
				List<TrafficCampaign> schList=trafficCampaignDao.getByStatus("ACTIVE");

				for(TrafficCampaign cs:schList){
					cdata.put(cs.getCampaignid(), cs);
				//	logger.debug("SYSTEM::reloadTrafficCampaignCache():: Adding to Cache Key Campaign Id "+cs.getCampaignid()+", Value "+cs);
				}
				schList=null;
				schList=trafficCampaignDao.getByStatus("TESTING");
				for(TrafficCampaign cs:schList){
					cdata.put(cs.getCampaignid(), cs);
					//logger.debug("SYSTEM::reloadTrafficCampaignCache():: Adding to Cache Key Test Campaign Id "+cs.getCampaignid()+", Value "+cs);
				}
				schList=null;
		}catch(Exception e){
			logger.error("reloadTrafficCampaignCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getTrafficCampaigns()==null) cachedObjects.setTrafficCampaigns(cdata);
		return true;
	}

	public Boolean reloadTrafficSourceCache(){
		Map<Integer,TrafficSource> cdata=new HashMap<>();
		try{
			List<TrafficSource> schList=trafficSourceDao.getByStatus(new Integer(1));
			for(TrafficSource cs:schList){
				cdata.put(cs.getTsourceid(), cs);
				//logger.debug("SYSTEM::reloadTrafficSourceCache():: Adding to Cache Key "+cs.getTsourceid()+", Value "+cs);
			}
			schList=null;
		}catch(Exception e){
			logger.error("reloadTrafficSourceCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getTrafficSources()==null) cachedObjects.setTrafficSources(cdata);
		return true;
	}

	public Boolean reloadVoucherDetailCache(){
		Map<String,VoucherDetail> cdata=new HashMap<>();
		try{
			List<VoucherDetail> schList=voucherDetailDao.getByStatus(new Integer(1));

			for(VoucherDetail cs:schList){
				cdata.put(cs.getVoucherId(), cs);
			//	logger.debug("SYSTEM::reloadVoucherDetailCache():: Adding to Cache Key "+cs.getVoucherId()+", Value "+cs);
			}
			schList=null;
		}catch(Exception e){
			logger.error("reloadVoucherDetailCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getVoucherDetails()==null) cachedObjects.setVoucherDetails(cdata);
		return true;
	}



	public Boolean reloadCircleDetailCache(){
		Map<Integer, Circleinfo> cdata=new HashMap<>();
		try{
			List<Circleinfo> schList=circleInfoDao.getByStatus((int)1);

			for(Circleinfo cs:schList){
				cdata.put(cs.getCircleid(), cs);

			}
			//logger.debug("SYSTEM::reloadCircleDetailCache():: Circles Cache reloaded with  "+cdata.size()+"Items.");
			schList=null;
		}catch(Exception e){
			logger.error("reloadCircleDetailCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getCircleInfo()==null) cachedObjects.setCircleInfo(cdata);
		return true;
	}




	public Boolean reloadImsiSeriesCache(){
		Map<String, ImsiSeries> cdata=new HashMap<>();
		try{
			List<ImsiSeries> schList=imsiSeriesDao.getByStatus((int)1);

			for(ImsiSeries cs:schList){
				cdata.put(cs.getImsi(), cs);
			}
		//	logger.debug("SYSTEM::reloadImsiSeriesCache():: Imsi Cache reloaded with  "+cdata.size()+"Items.");
			schList=null;
		}catch(Exception e){
			logger.error("reloadImsiSeriesCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getImsiSeries()==null) cachedObjects.setImsiSeries(cdata);
		return true;
	}




	public Boolean reloadIPSeriesCache(){
		Map<String, IPSeries> cdata=new HashMap<>();
		try{
			List<IPSeries> schList=ipSeriesDao.getByStatus((int)1);

			for(IPSeries cs:schList){
				cdata.put(cs.getIpMask(), cs);
			}
			//logger.debug("SYSTEM::reloadIPSeriesCache():: IP Cache reloaded with  "+cdata.size()+"Items.");
			schList=null;
		}catch(Exception e){
			logger.error("reloadIPSeriesCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getIpSeries()==null) cachedObjects.setIpSeries(cdata);
		return true;
	}


	public Boolean reloadLandingPageScheduleCache(){
		Map<String, LandingPageSchedule> cdata=new HashMap<>();
		try{
			List<LandingPageSchedule> schList=landingPageDao.getByStatus(new Integer(1));

			String key="";
			for(LandingPageSchedule cs:schList){
				key=cs.getChintfid()+"_"+cs.getTsourceid()+"_"+cs.getLandingPageId();
				//logger.debug("SYSTEM::reloadLandingPageScheduleCache():: Adding to Cache Key "+key+", Value "+cs);
				cdata.put(key, cs);
				key="";
			}
			key=null;
			schList=null;
		}catch(Exception e){
			logger.error("reloadLandingPageScheduleCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getLandingPageSchedule()==null) cachedObjects.setLandingPageSchedule(cdata);
		return true;
	}



	public Boolean reloadMsisdnSeriesCache(){
		Map<String, MsisdnSeries> cdata=new HashMap<>();
		try{
			List<MsisdnSeries> schList=msisdnSeriesDao.getByStatus((byte)1);

			for(MsisdnSeries cs:schList){
				cdata.put(cs.getMsisdnPrefix(), cs);
			}
			//logger.debug("SYSTEM::reloadMsisdnSeriesCache():: Msisdn Series Cache reloaded with  "+cdata.size()+"Items.");
			schList=null;
		}catch(Exception e){
			logger.error("reloadMsisdnSeriesCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getMsisdnSeries()==null) cachedObjects.setMsisdnSeries(cdata);
		return true;
	}


	public Boolean reloadNotificationConfigCache(){
		Map<String, NotificationConfig> cdata=new HashMap<>();
		try{
			List<NotificationConfig> schList=notificationDao.getByStatus(new Integer(1));
			String key="";
			for(NotificationConfig cs:schList){
				key=cs.getChintfid()+"_"+cs.getTsourceid()+"_"+cs.getCampaignid()+"_"+cs.getPubid()+"_"+cs.getNotifyConfigId();
				//logger.debug("SYSTEM::reloadNotificationConfigCache():: Adding to Cache Key "+key+", Value "+cs);
				cdata.put(key, cs);
				key="";
			}
			key=null;
			schList=null;
		}catch(Exception e){
			logger.error("reloadNotificationConfigCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getNotificationConfigs()==null) cachedObjects.setNotificationConfigs(cdata);
		return true;
	}

	public Boolean reloadRoutingCache(){
		Map<String, Routing> cdata=new HashMap<>();
		try{
			List<Routing> schList=routingDao.getByStatus(new Byte((byte)1));
			String key="";
			for(Routing cs:schList){
				key=cs.getChintfid()+"_"+cs.getTsourceid()+"_"+cs.getCampaignid()+"_"+cs.getCircleid()+"_"+cs.getPubid()+"_"+cs.getRoutingId();
				//logger.debug("SYSTEM::reloadRoutingCache():: Adding to Cache Key "+key+", Value "+cs);
				cdata.put(key, cs);
				key="";
			}
			key=null;
			schList=null;
		}catch(Exception e){
			logger.error("reloadRoutingCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getRoutingConfig()==null) cachedObjects.setRoutingConfig(cdata);
		return true;
	}

	public Boolean reloadPlatformCache(){
		Map<String, Platform> cdata=new HashMap<>();
		try{
			List<Platform> schList=platformDao.findAll();
			String key="";
			for(Platform cs:schList){
				key=cs.getPlatform();
				//logger.debug("SYSTEM::reloadPlatformCache():: Adding to Cache Key "+key+", Value "+cs);
				cdata.put(key, cs);
				key="";
			}
			key=null;
			schList=null;
		}catch(Exception e){
			logger.error("reloadRoutingCache() :: Error :"+e);
		}
		if(cdata.size()>0 || cachedObjects.getPlatformConfig()==null) cachedObjects.setPlatformConfig(cdata);
		return true;
	}

	public Boolean reloadTransCounter(String transCounterFile){
		try{
			StringBuilder sb=new StringBuilder();
			BufferedReader br=new BufferedReader(new FileReader(transCounterFile));
			String line="";
			while((line=br.readLine())!=null){
				sb.append(line);
			}
			br.close();br=null;
			String[] strArr=sb.toString().split(",");
			Map<Integer,Integer> transCounter=new HashMap<>();
			for(String s:strArr){
				if(s.startsWith("maxcounter")) cachedObjects.setMaxCounter(JocgString.strToInt(s.substring(s.indexOf("=")+1), 0));
				else
				transCounter.put(JocgString.strToInt(s.substring(0, s.indexOf("=")), 0),
						JocgString.strToInt(s.substring(s.indexOf("=")+1), 0));
			}
			
			cachedObjects.setTransCounter(transCounter);
			logger.debug("Transaction counter cache "+transCounter+", maxCounter "+cachedObjects.getMaxCounter());
		}catch(Exception e){}

		return true;
	}


	public boolean reloadTriggers(){
		Map<String,Integer> triggers=new HashMap<>();
		Map<String,String> smsTemplates=new HashMap<>();
		try{
			InputStream is=(InputStream)new FileInputStream(triggerFile);
			Properties props=new Properties();
			props.load(is);
			Integer serverId=JocgString.strToInt(props.getProperty("SERVERID"),0);
			cachedObjects.setBsnlSTVPacks(JocgString.formatString(props.getProperty("BSNLSTVPACKS"),""));
			cachedObjects.setBsnlMessage(JocgString.formatString(props.getProperty("BSNLMSG"),""));
			Integer renewalStatus=("ON".equalsIgnoreCase(props.getProperty("RENEWAL")))?1:0;
			Integer airtelPendingProcessStatus=("ON".equalsIgnoreCase(props.getProperty("PROCESSAIRTELPENDING")))?1:0;
			Integer notifyS2S=("ON".equalsIgnoreCase(props.getProperty("NOTIFYS2S")))?1:0;
			Integer notifyS2SChurn=("ON".equalsIgnoreCase(props.getProperty("NOTIFYS2SCHURN")))?1:0;
			Integer enableOfflineProcess=("ON".equalsIgnoreCase(props.getProperty("OFFLINEPROCESS")))?1:0;
			
			
			triggers.put("SERVERID", serverId);
			triggers.put("RENEWAL", renewalStatus);
			triggers.put("PROCESSAIRTELPENDING", airtelPendingProcessStatus);
			triggers.put("NOTIFYS2S", notifyS2S);
			triggers.put("NOTIFYS2SCHURN", notifyS2SChurn);
			triggers.put("OFFLINEPROCESS", enableOfflineProcess);
			
			Enumeration e=props.keys();
			while(e.hasMoreElements()){
				String key=(String)e.nextElement();
				if(key.startsWith("smstemplate.")){
					smsTemplates.put(key, props.getProperty(key));
				}
			}
			is=null;
			props=null;
			System.out.println(smsTemplates);
		}catch(Exception e){
			if(!triggers.containsKey("SERVERID")) triggers.put("SERVERID", new Integer(0));
			if(!triggers.containsKey("RENEWAL")) triggers.put("RENEWAL", new Integer(0));
			if(!triggers.containsKey("NOTIFYS2S")) triggers.put("NOTIFYS2S", new Integer(0));
			triggers.put("PROCESSAIRTELPENDING", new Integer(0));
			if(!triggers.containsKey("NOTIFYS2SCHURN")) triggers.put("NOTIFYS2SCHURN", new Integer(0));
		}finally{
			cachedObjects.setJocgSMSTemplates(smsTemplates);
			cachedObjects.setJocgTriggers(triggers);
			logger.debug("Triggers Loaded "+cachedObjects.getJocgTriggers());
		}
		return true;
	}
	
	public boolean reloadStats(){
		Map<String,TrafficStats> trafficStats=new HashMap<>();
		try{
			trafficStats=chargingCDRDaoImpl.getCDRTrafficStats();
//			key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
//			 
//			
//			Collection<NotificationConfig> nc=cachedObjects.getNotificationConfigs().values();
//			for(NotificationConfig nc1:nc){
//				String pubId=JocgString.formatString(nc1.getPubid(),"NA");
//				if(!"NA".equalsIgnoreCase(pubId)){
//					//Fetch data against campaign Id, trafficSourceId,chintfId,pubId
//					
//				}else{
//					//Fetch data against campaign Id, trafficSourceId,chintfId
//					nc1.getCampaignid();
//					nc1.getTsourceid();
//					nc1.getChintfid();
//					
//				}
//			}
		}catch(Exception e){
			//logger.debug("TrafficStats Loaded "+cachedObjects);
		}finally{
			cachedObjects.setTrafficStats(trafficStats);
			logger.debug("Traffic Stats Loaded, Stats Count "+trafficStats.size());
			trafficStats=null;
		}
		return true;
	}

	
	
	
}
