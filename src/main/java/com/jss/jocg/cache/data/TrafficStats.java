package com.jss.jocg.cache.data;

import java.lang.reflect.Field;

import org.springframework.data.annotation.Id;

public class TrafficStats {
//	@Id
//	String id;
//	Double totalPrice=0d;
//	Double avgChargedPrice=0d;
//	Double count;
	Integer chargingInterfaceId;
	Integer trafficSourceId;
	Integer campaignId;
	String publisherId;
	
	Integer totalCount;
	Integer distinctMsisdn;
	Integer successCount;
	Integer totalChurn;
	Integer samedayChurn;
	Integer churn20Min;
	Integer sentCount;
	
	String configKey;
	
	
public TrafficStats(){
	publisherId="NA";
	totalCount=0;
	distinctMsisdn=0;
	successCount=0;
	totalChurn=0;
	samedayChurn=0;
	churn20Min=0;
	sentCount=0;
	
}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("TrafficStats{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}

//	public String getId() {
//		return id;
//	}
//
//	public void setId(String id) {
//		this.id = id;
//	}
//
//	public Double getTotalPrice() {
//		return totalPrice;
//	}
//
//	public void setTotalPrice(Double totalPrice) {
//		this.totalPrice = totalPrice;
//	}
//
//	public Double getAvgChargedPrice() {
//		return avgChargedPrice;
//	}
//
//	public void setAvgChargedPrice(Double avgChargedPrice) {
//		this.avgChargedPrice = avgChargedPrice;
//	}
//
//	public Double getCount() {
//		return count;
//	}
//
//	public void setCount(Double count) {
//		this.count = count;
//	}

	public Integer getChargingInterfaceId() {
		return chargingInterfaceId;
	}

	public void setChargingInterfaceId(Integer chargingInterfaceId) {
		this.chargingInterfaceId = chargingInterfaceId;
	}

	public Integer getTrafficSourceId() {
		return trafficSourceId;
	}

	public void setTrafficSourceId(Integer trafficSourceId) {
		this.trafficSourceId = trafficSourceId;
	}

	public Integer getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}

	public String getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getDistinctMsisdn() {
		return distinctMsisdn;
	}

	public void setDistinctMsisdn(Integer distinctMsisdn) {
		this.distinctMsisdn = distinctMsisdn;
	}

	public Integer getSuccessCount() {
		return successCount;
	}

	public void setSuccessCount(Integer successCount) {
		this.successCount = successCount;
	}

	public Integer getTotalChurn() {
		return totalChurn;
	}

	public void setTotalChurn(Integer totalChurn) {
		this.totalChurn = totalChurn;
	}

	public Integer getSamedayChurn() {
		return samedayChurn;
	}

	public void setSamedayChurn(Integer samedayChurn) {
		this.samedayChurn = samedayChurn;
	}

	public Integer getChurn20Min() {
		return churn20Min;
	}

	public void setChurn20Min(Integer churn20Min) {
		this.churn20Min = churn20Min;
	}

	public Integer getSentCount() {
		return sentCount;
	}

	public void setSentCount(Integer sentCount) {
		this.sentCount = sentCount;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}	
	
	
	
	
}
