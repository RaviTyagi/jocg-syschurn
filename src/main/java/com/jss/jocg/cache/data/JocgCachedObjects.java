package com.jss.jocg.cache.data;

import java.io.FileWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;

public class JocgCachedObjects {

	private Map<String,Map<Integer,CgimageSchedule>> imageSchedule=new HashMap<>();
	private Map<Integer,ChargingInterface> chargingInterfaces=new HashMap<>();
	private Map<Integer,ChargingPricepoint> chargingPricePoint=new HashMap<>();
	private Map<String,ChargingPricepoint> operatorKey=new HashMap<>();
	private Map<Integer,ChargingUrl> chargingUrls=new HashMap<>();
	private Map<Integer,List<ChargingUrlParam>> chargingUrlParams=new HashMap<>();
	private Map<Integer,PackageChintfmap> packageChintfMap=new HashMap<>();
	private Map<String,PackageDetail> packageDetails=new HashMap<>();
	private Map<Integer,Service> serviceDetails=new HashMap<>();
	private Map<Integer,TrafficCampaign> trafficCampaigns=new HashMap<>();
	private Map<Integer,TrafficSource> trafficSources=new HashMap<>();
	private Map<String,VoucherDetail> voucherDetails=new HashMap<>();
	
	private Map<Integer,Circleinfo> circleInfo=new HashMap<>();
	private Map<String,MsisdnSeries> msisdnSeries=new HashMap<>();
	private Map<String,ImsiSeries> imsiSeries=new HashMap<>();
	private Map<String,IPSeries> ipSeries=new HashMap<>();
	private Map<String,LandingPageSchedule> landingPageSchedule=new HashMap<>();
	private Map<String,NotificationConfig> notificationConfigs=new HashMap<>();
	private Map<String,Routing> routingConfig=new HashMap<>();
	private Map<String,Platform> platformConfig=new HashMap<>();
	private Map<Integer,Integer> transCounter=new HashMap<>();
	private Map<String,Integer> jocgTriggers=new HashMap<>();
	private Map<String,String> jocgSMSTemplates=new HashMap<>();
	private Map<String,TrafficStats> trafficStats=new HashMap<>();
	private String bsnlSTVPacks="NA";
	private String bsnlMessage="NA";
	public String getBsnlMessage() {
		return bsnlMessage;
	}

	public void setBsnlMessage(String bsnlMessage) {
		this.bsnlMessage = bsnlMessage;
	}

	private Integer maxCounter=999999;

	
	public JocgCachedObjects(){
	}

	public  String getBsnlSTVPacks(){
		return bsnlSTVPacks;
	}
	
	public void setBsnlSTVPacks(String stvPackList){
		this.bsnlSTVPacks=stvPackList;
	}
	
	public String getTransactionSequence(Integer operatorId,int maxCounterDigit,String filePath){
		String newCounter="";
		try{
			Integer counter=1001;
			if(transCounter.containsKey(operatorId)){
				counter=transCounter.get(operatorId);
				counter++;
				String cstr=""+counter;
				if(cstr.length()>maxCounterDigit) counter=1;
			}
			transCounter.put(operatorId, counter);
			newCounter=""+operatorId.intValue()+""+counter.intValue();
		}catch(Exception e){
			
		}finally{
			try{
				Iterator<Integer> e=transCounter.keySet().iterator();
				Integer key=0;
				StringBuilder sb=new StringBuilder();
				while(e.hasNext()){
					key=e.next();
					sb.append(key.intValue()).append("=").append(transCounter.get(key)).append(",");
				}
				FileWriter fw=new FileWriter(filePath,false);
				fw.write(sb.toString());
				fw.flush();
				fw.close();
				fw=null;
			}catch(Exception e){}
		}
		return newCounter;
	}
	
	public Map<String, Routing> getRoutingConfig() {
		return routingConfig;
	}

	public void setRoutingConfig(Map<String, Routing> routingConfig) {
		this.routingConfig = routingConfig;
	}

	public Map<String, Map<Integer, CgimageSchedule>> getImageSchedule() {
		return imageSchedule;
	}

	public void setImageSchedule(Map<String, Map<Integer, CgimageSchedule>> imageSchedule) {
		this.imageSchedule = imageSchedule;
	}

	public Map<Integer, ChargingInterface> getChargingInterfaces() {
		return chargingInterfaces;
	}

	public void setChargingInterfaces(Map<Integer, ChargingInterface> chargingInterfaces) {
		this.chargingInterfaces = chargingInterfaces;
	}

	public Map<Integer, ChargingPricepoint> getChargingPricePoint() {
		return chargingPricePoint;
	}

	public void setChargingPricePoint(Map<Integer, ChargingPricepoint> chargingPricePoint) {
		this.chargingPricePoint = chargingPricePoint;
	}

	public Map<String, ChargingPricepoint> getOperatorKey() {
		return operatorKey;
	}

	public void setOperatorKey(Map<String, ChargingPricepoint> operatorKey) {
		this.operatorKey = operatorKey;
	}

	public Map<Integer, ChargingUrl> getChargingUrls() {
		return chargingUrls;
	}

	public void setChargingUrls(Map<Integer, ChargingUrl> chargingUrls) {
		this.chargingUrls = chargingUrls;
	}

	public Map<Integer, PackageChintfmap> getPackageChintfMap() {
		return packageChintfMap;
	}

	public void setPackageChintfMap(Map<Integer, PackageChintfmap> packageChintfMap) {
		this.packageChintfMap = packageChintfMap;
	}

	public Map<String, PackageDetail> getPackageDetails() {
		return packageDetails;
	}

	public void setPackageDetails(Map<String, PackageDetail> packageDetails) {
		this.packageDetails = packageDetails;
	}

	public Map<Integer, TrafficCampaign> getTrafficCampaigns() {
		return trafficCampaigns;
	}

	public void setTrafficCampaigns(Map<Integer, TrafficCampaign> trafficCampaigns) {
		this.trafficCampaigns = trafficCampaigns;
	}

	public Map<Integer, TrafficSource> getTrafficSources() {
		return trafficSources;
	}

	public void setTrafficSources(Map<Integer, TrafficSource> trafficSources) {
		this.trafficSources = trafficSources;
	}

	public Map<String, VoucherDetail> getVoucherDetails() {
		return voucherDetails;
	}

	public void setVoucherDetails(Map<String, VoucherDetail> voucherDetails) {
		this.voucherDetails = voucherDetails;
	}

	public Map<Integer, Service> getServiceDetails() {
		return serviceDetails;
	}

	public void setServiceDetails(Map<Integer, Service> serviceDetails) {
		this.serviceDetails = serviceDetails;
	}

	public Map<String, MsisdnSeries> getMsisdnSeries() {
		return msisdnSeries;
	}

	public void setMsisdnSeries(Map<String, MsisdnSeries> msisdnSeries) {
		this.msisdnSeries = msisdnSeries;
	}

	public Map<String, ImsiSeries> getImsiSeries() {
		return imsiSeries;
	}

	public void setImsiSeries(Map<String, ImsiSeries> imsiSeries) {
		this.imsiSeries = imsiSeries;
	}

	public Map<String, IPSeries> getIpSeries() {
		return ipSeries;
	}

	public void setIpSeries(Map<String, IPSeries> ipSeries) {
		this.ipSeries = ipSeries;
	}

	public Map<String, LandingPageSchedule> getLandingPageSchedule() {
		return landingPageSchedule;
	}

	public void setLandingPageSchedule(Map<String, LandingPageSchedule> landingPageSchedule) {
		this.landingPageSchedule = landingPageSchedule;
	}

	public Map<String, NotificationConfig> getNotificationConfigs() {
		return notificationConfigs;
	}

	public void setNotificationConfigs(Map<String, NotificationConfig> notificationConfigs) {
		this.notificationConfigs = notificationConfigs;
	}

	public Map<Integer, Circleinfo> getCircleInfo() {
		return circleInfo;
	}

	public void setCircleInfo(Map<Integer, Circleinfo> circleInfo) {
		this.circleInfo = circleInfo;
	}

	public Map<Integer, List<ChargingUrlParam>> getChargingUrlParams() {
		return chargingUrlParams;
	}

	public void setChargingUrlParams(Map<Integer, List<ChargingUrlParam>> chargingUrlParams) {
		this.chargingUrlParams = chargingUrlParams;
	}

	public Map<String, Platform> getPlatformConfig() {
		return platformConfig;
	}

	public void setPlatformConfig(Map<String, Platform> platformConfig) {
		this.platformConfig = platformConfig;
	}

	public Map<Integer, Integer> getTransCounter() {
		return transCounter;
	}

	public void setTransCounter(Map<Integer, Integer> transCounter) {
		this.transCounter = transCounter;
	}

	public Integer getMaxCounter() {
		return maxCounter;
	}
	
	public void setMaxCounter(Integer maxCounter) {
		this.maxCounter=maxCounter;
	}

	public Map<String, Integer> getJocgTriggers() {
		return jocgTriggers;
	}

	public void setJocgTriggers(Map<String, Integer> jocgTriggers) {
		this.jocgTriggers = jocgTriggers;
	}

	public Map<String, String> getJocgSMSTemplates() {
		return jocgSMSTemplates;
	}

	public void setJocgSMSTemplates(Map<String, String> jocgSMSTemplates) {	
		this.jocgSMSTemplates = jocgSMSTemplates;
	}

	public Map<String, TrafficStats> getTrafficStats() {
		return trafficStats;
	}

	public void setTrafficStats(Map<String, TrafficStats> trafficStats) {
		this.trafficStats = trafficStats;
	}

	
	
}
