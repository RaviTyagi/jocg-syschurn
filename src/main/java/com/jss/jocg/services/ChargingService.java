package com.jss.jocg.services;

import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.charging.handlers.FreechargeChargingHandler;
import com.jss.jocg.charging.handlers.MobikwikChargingHandler;
import com.jss.jocg.charging.handlers.OxigenChargingHandler;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgRequestParameters;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.RequestValidator;
import com.jss.jocg.services.bo.ResponseBuilder;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@RestController
@RequestMapping("jocg/charge")
public class ChargingService extends JocgService {
	private static final Logger logger = LoggerFactory
			.getLogger(ChargingService.class);
	
	@Autowired RequestValidator requestValidator;
	@Autowired ResponseBuilder responseBuilder;
		
	@RequestMapping("/{catg}/{operator}")
	public ModelAndView processCharging(Model model, 
			@PathVariable(value="catg") String requestCategory,
			@PathVariable(value="operator") String sourceOperator,
			@RequestParam(value="subid",required=false,defaultValue="NA") String subscriberId,
			@RequestParam(value="msisdn",required=false,defaultValue="0") Long requestMsisdn,
			@RequestParam(value="platform",required=false,defaultValue="NA") String platformName,
			@RequestParam(value="pkgname",required=false,defaultValue="NA") String packageName,
			@RequestParam(value="pkgprice",required=false,defaultValue="0") Double packagePrice,
			@RequestParam(value="pkgvalidity",required=false,defaultValue="0") Integer packageValidity,
			@RequestParam(value="nettype",required=false,defaultValue="M") String networkType,
			@RequestParam(value="vendor",required=false,defaultValue="NA") String voucherVendor,
			@RequestParam(value="chargeamount",required=false,defaultValue="0") Double amountToBeCharged,
			@RequestParam(value="voucher",required=false,defaultValue="NA") String voucherCode,
			@RequestParam(value="checksum",required=false,defaultValue="0") Long checkSum,
			@RequestParam(value="cmp",required=false,defaultValue="0") Integer campaignId,
			@RequestParam(value="pubid",required=false,defaultValue="NA") String publisherId){
				String logHeader=""+requestCategory+"|"+sourceOperator+"|";
				ModelAndView mv=null;
				JocgRequest newReq=new JocgRequest();
				newReq.setRequestType("ACT");
				Integer processStatus=JocgStatusCodes.CHARGING_INPROCESS;
				String statusDescp="";
				try{
					//Select Subscriber Id
					//Set Request Parameters
					if(campaignId==13) sourceOperator="OAIRT";
					if(sourceOperator!=null && "CM_OF".equalsIgnoreCase(sourceOperator)) sourceOperator="CMOF";
					
					newReq.setRequestParams(new JocgRequestParameters(requestCategory,sourceOperator,subscriberId,parseMsisdn(),requestMsisdn,parseImsi(),platformName,packageName,packagePrice,packageValidity,networkType,voucherVendor,amountToBeCharged,voucherCode,checkSum,campaignId,publisherId,requestContext.getRemoteAddr(),requestContext.getHeader("referer"),requestContext.getHeader("user-agent"),requestContext));
				    logger.debug(logHeader+" Request Params "+newReq.getRequestParams());
				  //Validate Request Params
					RequestValidator.ValidationResponse vr=requestValidator.validateRequest(newReq.getRequestParams());
					
					if(vr.getValidationStatus()<0){
						processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
						statusDescp=vr.getStatusDescription();
					}
					logger.debug(logHeader+" After Param Validation  , processStatus "+processStatus+", descp "+statusDescp);
					
					if(processStatus.intValue()==JocgStatusCodes.CHARGING_INPROCESS){
						//Select Msisdn & Subscriber Id for Processing
						//newReq.setMsisdn(requestValidator.selectMsisdn(newReq.getRequestParams().getHeaderMsisdn(),newReq.getRequestParams().getRequestMsisdn()));
						newReq.setMsisdn(requestValidator.selectMsisdn(requestCategory, newReq.getRequestParams().getHeaderMsisdn(), newReq.getRequestParams().getRequestMsisdn()));
						newReq.setSubscriberId(requestValidator.prepareSubscriberId(newReq.getRequestParams().getSubscriberId(),newReq.getMsisdn()));
						logHeader +="{"+newReq.getSubscriberId()+","+newReq.getMsisdn()+"}|";
						
						//Identify Circle Info
						newReq.setCircle(circleIndentifier.identifyCircle(requestContext.getRemoteAddr(), parseImsi(), newReq.getMsisdn()));
						if(newReq.getCircle()==null || newReq.getCircle().getCircleid()<=0){
							logger.debug(logHeader+" Circle detailes not found , proceeding without circle details...");
						}
						
						//Fetch Service & Package Details from Cache
						newReq=requestValidator.fetchServiceDetails(logHeader,newReq,requestContext);
						logger.debug(logHeader+" Service "+newReq.getService());
						logger.debug(logHeader+" Package "+newReq.getPackageDetail());
						logger.debug(logHeader+" Chintf "+newReq.getChargingOperator());
						processStatus=newReq.getResponseObject().getResponseCode();
						statusDescp=newReq.getResponseObject().getResponseDescription();
					}
					
					if(processStatus.intValue()==JocgStatusCodes.CHARGING_INPROCESS){
						//Get Subscriber Details if subscribed already and also Get Routing Info if applied
						Subscriber sub=subMgr.searchSubscriber(newReq.getSubscriberId(),newReq.getMsisdn(), newReq.getService().getServiceid(), newReq.getService().getServiceName());

						//Campaign Routing Fixes
						//Condition changed to get routing info in case of Missing MSISDN as subscriber not found in such case
						//subscriber validation already exist in the function 
						//null subscriber check placed as required in the function flow
						//if(sub!=null && sub.getChargingInterfaceId()>0 && sub.getSourceNetworkId()>0 && newReq.getChargingOperator()!=null && newReq.getChargingOperator().getChintfid()!=2 && newReq.getChargingOperator().getChintfid()!=3){
						if(newReq.getChargingOperator()!=null && newReq.getChargingOperator().getChintfid()!=2 && newReq.getChargingOperator().getChintfid()!=3){
							newReq=requestValidator.validateSubscriptionAndRoutingInfo(logHeader,newReq,sub,requestContext);
							processStatus=newReq.getResponseObject().getResponseCode();
							statusDescp=newReq.getResponseObject().getResponseDescription();
						}
					}
					
					//Validate Checksum if applied
					if(processStatus.intValue()==JocgStatusCodes.CHARGING_INPROCESS && checkSumCategories!=null && checkSumCategories.contains(","+newReq.getRequestParams().getRequestCategory().toUpperCase()+",")){
						//Prepare for Checksum Validation
						StringBuilder cs=new StringBuilder();
						int packPriceInt=newReq.getRequestParams().getPackagePrice().intValue();
						if("W".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory().toUpperCase()) ||
								"OP".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory().toUpperCase()) )//subid|msisdn|platform|packageName|packagePrice|packageValidity ==> Calculate checksum and pass into checksum parameter
							cs.append(newReq.getRequestParams().getSubscriberId()).append("|").append(newReq.getRequestParams().getRequestMsisdn().longValue())
							.append("|").append(newReq.getRequestParams().getPlatformName()).append("|").append(newReq.getRequestParams().getPackageName())
							.append("|").append(packPriceInt).append("|").append(newReq.getRequestParams().getPackageValidity());
							
						else if("V".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory().toUpperCase())) //subid|msisdn|platform|voucher ==> Calculate checksum and pass into checksum parameter
							cs.append(newReq.getRequestParams().getSubscriberId()).append("|").append(newReq.getRequestParams().getRequestMsisdn().longValue())
							.append("|").append(newReq.getRequestParams().getPlatformName()).append("|").append(newReq.getRequestParams().getVoucherCode());
						
						logger.debug(logHeader+" Checksum Data to Validate "+cs.toString()+", checksumKey "+checksumKey+", checkSumCategories "+checkSumCategories);
						if(cs.length()>0 && !requestValidator.validateChecksum(cs.toString(),checkSum,checksumKey)){
							processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
							statusDescp="Checksum Validation Failed";
						}
					}
					
					//Campaign Routing Fixes
					//Condition Changed to process Routing to diff SERVICE in case Already Subscribed
					//if(processStatus.intValue()==JocgStatusCodes.CHARGING_INPROCESS){
					if(processStatus.intValue()==JocgStatusCodes.CHARGING_INPROCESS 
							|| ("C".equalsIgnoreCase(requestCategory) && processStatus.intValue()==JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED && newReq.getRoutingInfo()!=null && newReq.getRoutingInfo().getRoutingCatg().equalsIgnoreCase("SERVICE")) ){
						newReq.setRequestType("ACT");
						if("V".equalsIgnoreCase(requestCategory)){
							//newReq.setRequestType("VOUCHER-PRECHARGED");
							newReq=chargingProcessor.processVoucherActivation(newReq, requestContext);
						}else if("W".equalsIgnoreCase(requestCategory)){
							//newReq.setRequestType("WALLET-CHARGING");
							
							newReq=chargingProcessor.processWalletCharging(newReq, requestContext);
						}else if("OP".equalsIgnoreCase(requestCategory) || "TEST".equalsIgnoreCase(requestCategory)){
							//newReq.setRequestType("OPERATOR-CHARGING");
							newReq=chargingProcessor.processOperatorCharging(newReq, requestContext);
						}else if("C".equalsIgnoreCase(requestCategory)){
							//newReq.setRequestType("CAMPAIGN-CHARGING");
							newReq=chargingProcessor.processCampaignCharging(newReq, requestContext);
						}else{
							//Set Error Response
							newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS);
							newReq.getResponseObject().setResponseDescription("Unsupported Request Category "+requestCategory);
						}
				
					}else{ 
						Routing routingInfo=newReq.getRoutingInfo();
						if(routingInfo!=null && ("URL".equalsIgnoreCase(routingInfo.getRoutingCatg()) ||  "WASTE-URL".equalsIgnoreCase(routingInfo.getRoutingCatg()) )){
							logger.debug(logHeader+"Routing Request to "+routingInfo.getRoutingDef());
							JocgResponse resp=new JocgResponse();
							resp.setResponseCode(processStatus);
							resp.setResponseDescription(statusDescp);
							mv=new ModelAndView("redirect:"+routingInfo.getRoutingDef());
							resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
							resp.setRespView(mv);
							newReq.setResponseObject(resp);
						}else{
							//Set Error Response
							newReq.getResponseObject().setResponseCode(processStatus);
							newReq.getResponseObject().setResponseDescription(statusDescp);
						}
					}
					
				}catch(Exception e){
					//Set Error Response
					newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					newReq.getResponseObject().setResponseDescription("System Error!");
					e.printStackTrace();
				}
				
				//DB Transactions
				createDBLogging(newReq);
				
		//Commit Response		
		logger.debug("Invoking response builder "+responseBuilder);
		return responseBuilder.buildResponse(newReq).getRespView();
	}

	@RequestMapping("/airtelincg")
	public ModelAndView handleAirtelCGRedirect(@RequestParam(value="product",required=false,defaultValue="NA") String productName,
			@RequestParam(value="code",required=false,defaultValue="NA") String cgAuthCode,
			@RequestParam(value="state",required=false,defaultValue="NA") String jocgTransId){
			String logHead="CHSERVICE::chargeCustomer()::REQ-CATG=CG-REDIRECT::";
			
			StringBuilder sb=new StringBuilder();
			sb.append("product=").append(productName).append("|");
			sb.append("code=").append(cgAuthCode).append("|");
			sb.append("state=").append(jocgTransId).append("|");
			
			if(jocgTransId.startsWith("AI")) jocgTransId=jocgTransId.substring(2);
			sb.append("extracted_state=").append(jocgTransId).append("|");
			
			logger.debug(logHead+"Searching JocgTransId "+jocgTransId);
			ChargingCDR cdr=searchCDRByJocgTransId(jocgTransId);
			Subscriber sub=searchSubscriberByTransId(cdr.getId());
			Integer statusCode=JocgStatusCodes.CHARGING_INPROCESS;
			logger.debug(logHead+"Found  CDR ID "+cdr.getId());
			logger.debug(logHead+"Invoking Charging Processor ");
			if(cdr.getId()!=null && cdr.getJocgTransId()!=null && !cdr.getJocgTransId().equals("NA")){
				if(cdr.getStatus()==JocgStatusCodes.CHARGING_INPROCESS){
					cdr= chargingProcessor.processAirtelIndiaCGRedirect(cdr,productName, cgAuthCode, jocgTransId);
					logHead+="{"+cdr.getMsisdn()+"} ::";
					cdr.setSubRegDate(cdr.getChargingDate());
					this.updateCDR(logHead, cdr, false);
					sub=cdrBuilder.buildSubscriber(cdr);
					
					sub.setLifecycleId(cdr.getSdpLifeCycleId());
					sub.setMsisdn(cdr.getMsisdn());
					sub.setJocgCDRId(cdr.getId());
					sub.setJocgTransId(cdr.getJocgTransId());
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_CGSUCCESS){
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_CG);
					}else if(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS){
						sub.setChargedAmount(cdr.getChargedAmount());
						sub.setChargedOperatorKey(cdr.getChargedOperatorKey());
						sub.setChargedOperatorParams(cdr.getChargedOperatorParams());
						sub.setValidityEndDate(cdr.getValidityEndDate());
						sub.setValidity(cdr.getValidityPeriod());
						sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
						sub.setSubSessionRevenue(cdr.getChargedAmount());
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
						//SEnd SMS for Successful Activation
						logger.debug(cdr.getMsisdn()+" Sending Activation SMS .......");
						String smsURL="http://172.31.22.121/jocg/sms/push?msisdn="+cdr.getMsisdn()+"&chintfid=2&smsinterface=airtelin&text=usetemplate&service="+(sub.getServiceName()==null?"NA":sub.getServiceName())+"&chintftype=operator";
						JcmsSSLClient sslClient=new JcmsSSLClient();
						JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL("AIRTEL ACT SMS "+cdr.getMsisdn()+":: ", smsURL);
						logger.debug(logHead+" Activation SMS Push Logs "+resp.getProcessLogs());
						/*JocgSMSMessage smsMessage=new JocgSMSMessage();
						smsMessage.setMsisdn(cdr.getMsisdn());
						smsMessage.setSmsInterface("airtelin");
						smsMessage.setChintfId(1);
						smsMessage.setTextMessage("Successful Activation");
						jmsTemplate.convertAndSend("notify.sms", smsMessage);
						
						logger.debug(cdr.getMsisdn()+" Activation SMS Queued .......SMSData="+smsMessage);
						*/
						
						
						
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_CG);
					}
					this.updateSubscriber(logHead, sub, false);
					statusCode=cdr.getStatus();
				}else{
					statusCode=JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED;
				}
			}else{
				//No update, Transaction Not found
				logger.info(logHead+" FAILED CDR :: "+sb.toString());
			}
			logger.debug("CDR After processing "+cdr);
			return responseBuilder.buildCGRedirectResponse(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS?"Thank you. Your request has been successfully processed.<br>This service will get automatically renewed.":cdr.getStatus()>0?"Thank you. We have received your request for activation <br>of Movies n Music Service.<br>This service will get automatically renewed.":"Sorry! Your request for service activation failed. Please try again.");
//			ModelAndView mv=new ModelAndView();
//			mv.setViewName("status");
//			String msg=(statusCode==JocgStatusCodes.CHARGING_SUCCESS)?"Charging Processed Successfully!":(statusCode==JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED)?"Already processed! Duplicate Request.":"Charging Failed!";
//			mv.addObject("message", msg);
//			return mv;
		
	}
	
	
	public void createDBLogging(JocgRequest newReq){
		String logHeader="initiateDBLogging ::"+newReq.getMsisdn()+"::";
		//DB Transactions
		try{
			ChargingCDR newCDR=cdrBuilder.buildCDR(newReq,requestContext);
			logger.debug(logHeader+"CDR Object "+newCDR);
			if(newCDR!=null && newCDR.getId()!=null && 
					(newCDR.getStatus()==JocgStatusCodes.CHARGING_INPROCESS || newCDR.getStatus()==JocgStatusCodes.CHARGING_CGSUCCESS ||
					newCDR.getStatus()==JocgStatusCodes.CHARGING_SDPGRACE || 
					newCDR.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING || newCDR.getStatus()==JocgStatusCodes.CHARGING_SUCCESS)){
				//Create Subscriber Entry
				if("C".equalsIgnoreCase(newCDR.getRequestCategory()) && 
						((newCDR.getChargingInterfaceId()<2 || newCDR.getChargingInterfaceId()>3) && (newCDR.getChargingInterfaceId()!=5 && newCDR.getChargingInterfaceId()!=6 && newCDR.getChargingInterfaceId()!=14) )){
					Subscriber sub=cdrBuilder.buildSubscriber(newCDR,newReq,requestContext);
					logger.debug(logHeader+"Subscriber Object "+sub);
				}else if(!"C".equalsIgnoreCase(newCDR.getRequestCategory())){
					Subscriber sub=cdrBuilder.buildSubscriber(newCDR,newReq,requestContext);
					logger.debug(logHeader+"Subscriber Object "+sub);
				}else{
					logger.debug(logHeader+"Subscriber Object not created.RequestCategory="+newCDR.getRequestCategory()+",OperatorId="+newCDR.getChargingInterfaceId());
				}
			}
			
			if("V".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory()) || "W".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory())){
				VoucherDetail v=newReq.getVoucher();
				if(v!=null && v.getCouponid()!=null && v.getCouponid()>0 && newReq.getResponseObject().getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS){
					if(v.getUsedCount()<=0){
						v.setFirstUsageTime(new java.util.Date(System.currentTimeMillis()));
					}
					v.setLastUsageTime(new java.util.Date(System.currentTimeMillis()));
					if("MULTIPLE-TIME-ANYUSER".equalsIgnoreCase(v.getVoucherType())){
						v.setUsedCount(v.getUsedCount()+1);
					}else{
						v.setStatus(2);
						v.setUsedCount(v.getUsedCount()+1);
					}
					jocgCache.updateVoucherDetails(v);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/walletreg")
	public ModelAndView getMsisdnForWallet(@RequestParam(value="email",required=false,defaultValue="NA") String emailId,
			@RequestParam(value="msisdn",required=false,defaultValue="") String msisdnStr,
			@RequestParam(value="transid",required=false,defaultValue="NA") String jocgTransId,
			@RequestParam(value="vname",required=false,defaultValue="NA") String viewName){
		String logHeader="CHSERVICE:GETMSISDN:"+jocgTransId+":";
		ModelAndView mv=new ModelAndView();
		mv.setViewName("wchargestatus");
		String message="Received Msisdn "+msisdnStr+", email :"+emailId+",jocgTransId :"+jocgTransId;
		try{
			ChargingCDR cdr=null;boolean responseComitted=false;
			boolean idValidEmail=requestValidator.validateEmail(emailId);
			boolean isValidMsisdn=requestValidator.validateMsisdn(msisdnStr);
			if(idValidEmail==false && isValidMsisdn==false){
				message="Valid Email or msisdn must be entered!";
				mv.setViewName(viewName);
				mv.addObject("transid", jocgTransId);
				responseComitted=true;
			}else{
				cdr=chargingCDRDao.findByjocgTransId(jocgTransId);
				if(cdr==null || cdr.getId()==null || cdr.getStatus()!=JocgStatusCodes.CHARGING_INPROCESS){
					message="Invalid Session State! Please try again later.";
					responseComitted=true;
					
					mv.setViewName(viewName);
					mv.addObject("transid", jocgTransId);
					responseComitted=true;
				}
			}
			
			if(responseComitted==false){
				//Identify Wallet id to be processed and revalidate
				String walletId=(isValidMsisdn)?msisdnStr:idValidEmail?emailId:"NA";
				if("NA".equalsIgnoreCase(walletId)){
					//Unreachable check but required
					message="Valid Email or msisdn must be entered!";
					mv.setViewName("wregoxigen");
					mv.addObject("transid", jocgTransId);
					responseComitted=true;
				}else if(isValidMsisdn){
				
					msisdnStr=(msisdnStr.length()<=10)?"91"+msisdnStr:msisdnStr;
				}
			}
			
			if(responseComitted==false){
				//Process Request
				Subscriber sub=subscriberDao.findByJocgCDRId(cdr.getId());
				if(idValidEmail){
					cdr.setEmailId(emailId);
					if(sub!=null && sub.getId()!=null)	sub.setEmailId(emailId);
				}
				
				long msisdnVal=JocgString.strToLong(msisdnStr, 0);
				if(isValidMsisdn && msisdnVal>0) {
					cdr.setMsisdn(msisdnVal);
					if(sub!=null && sub.getId()!=null)	sub.setMsisdn(msisdnVal);
					
				}
				subMgr.save(sub);
				
				ChargingInterface chintf=jocgCache.getChargingInterface(cdr.getChargingInterfaceId());
				List<ChargingUrl> urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
				ChargingUrl url=null;
				for(ChargingUrl churl:urlList){
					if("OTP-SEND".equalsIgnoreCase(churl.getUrlCatg())){
						System.out.println("-------------------->----->> "+churl.getUrl());
						url=churl;							
					}
				}
				if(url!=null && url.getChurlId()>0){
					List<ChargingUrlParam> urlParams=jocgCache.getChargingUrlParams(url.getChurlId());
					JocgResponse resp=null;
					if(chintf.getName().toLowerCase().contains("mobikwik")){
						MobikwikChargingHandler handler=new MobikwikChargingHandler();
						resp=handler.sendOTP(cdr,url,chintf,urlParams);
					}else if(chintf.getName().toLowerCase().contains("freecharge")){
						FreechargeChargingHandler handler=new FreechargeChargingHandler();
						resp=handler.sendOTP(cdr,url,chintf,urlParams);
					}else if(chintf.getName().toLowerCase().contains("oxigen")){
						OxigenChargingHandler handler=new OxigenChargingHandler();
						resp=handler.sendOTP(cdr,url,chintf,urlParams);
					}
					
					if(resp!=null && resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW && resp.getRespView()!=null)
						mv=resp.getRespView();
					else if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
						message=resp.getResponseDescription();
					}else{
						message=resp.getResponseDescription();
					}
					cdr.setStatus(resp.getResponseCode());
					cdr.setStatusDescp(resp.getResponseDescription());
					cdrBuilder.save(cdr);
					//sendOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams)
				}else{
					message="System Error! Please try again later.";
				}
				
			}else{
				cdr=null;
			}
			
			logger.debug(message);
		}catch(Exception e){
			message="System Error! Please try again later.";
		}
		mv.addObject("message", message);
		return mv;
	}
	@RequestMapping("/verifyotp")
	public ModelAndView verifyOtp(@RequestParam(value="otppin",required=false,defaultValue="NA") String otpEntered,
			@RequestParam(value="transid",required=false,defaultValue="NA") String jocgTransId,
			@RequestParam(value="vname",required=false,defaultValue="NA") String viewName){
		System.out.println("------------------>"+requestContext.getParameter("otppin"));
		String logHeader="CHSERVICE:VERIFYOTP:"+jocgTransId+":";
		ModelAndView mv=new ModelAndView();
		String validatedOtp=JocgString.formatString(otpEntered, "NA");
		String message="Entered OTP "+otpEntered+", TransId "+jocgTransId;
		try{
			System.out.println("------------------>OTP ID : "+requestContext.getParameter("otpId"));
			
			ChargingCDR cdr=chargingCDRDao.findByjocgTransId(jocgTransId);
			if(cdr!=null && cdr.getId()!=null){
				logHeader+=""+cdr.getMsisdn()+":";
//				cdr.setOtpPin(otpEntered+"$"+requestContext.getParameter("otpId"));
				JocgResponse resp=new JocgResponse();
				ChargingInterface chintf=jocgCache.getChargingInterface(cdr.getChargingInterfaceId());
				String accessToken="";
				if(chintf!=null && chintf.getChintfid()>0){
					List<ChargingUrl> urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
					System.out.println(" URL List "+((urlList!=null)?urlList.size():urlList));
					ChargingUrl url=null;
					if(chintf.getName().toLowerCase().contains("freecharge") || chintf.getName().toLowerCase().contains("oxigen")){
					cdr.setOtpPin(otpEntered+","+requestContext.getParameter("otpid"));
					}
					else{
						cdr.setOtpPin(otpEntered);
					}
					for(ChargingUrl churl:urlList){
						if("OTP-VALIDATE".equalsIgnoreCase(churl.getUrlCatg())){
							System.out.println("-------------------->----->> "+churl.getUrl());
							url=churl;							
						}
						
					}									
					
					if(url!=null && url.getChurlId()>0){
						List<ChargingUrlParam> urlParams=jocgCache.getChargingUrlParams(url.getChurlId());
						System.out.println("URL Params "+urlParams);					
						if(chintf.getName().toLowerCase().contains("mobikwik")){
							MobikwikChargingHandler handler=new MobikwikChargingHandler();
							resp=handler.verifyOTP(cdr, url, chintf,urlParams);
							if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
								//Set Access Token into subregid
								cdr.setSubRegId(resp.getAccessToken());
								accessToken=resp.getAccessToken();
								//Change URL for charging
								resp=chargeWalletCustomer(cdr,chintf);
							
								//Commit Response
								if(resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
									message="Wallet does not exists. Create new Wallet, In Process";
								}else{
									message=resp.getResponseDescription();
								}
							}else{
								//Token Request Failed
								message=resp.getResponseDescription();
							}
							//message=""+resp.toString();
						}else if(chintf.getName().toLowerCase().contains("freecharge")){																				
							FreechargeChargingHandler handler=new FreechargeChargingHandler();
							resp=handler.verifyOTP(cdr, url, chintf, urlParams);
							
							if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
								//Set Access Token into subregid
								cdr.setSubRegId(resp.getAccessToken());
								accessToken=resp.getAccessToken();
								//Change URL for charging
								resp=chargeWalletCustomer(cdr,chintf);
							
								//Commit Response
								if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
									message="Wallet does not exists. Create new Wallet, In Process";
								}else{
									message=resp.getResponseDescription();
								}
							}else{
								//Token Request Failed
								message=resp.getResponseDescription();
							}
							
						}else if(chintf.getName().toLowerCase().contains("oxigen")){
							OxigenChargingHandler handler=new OxigenChargingHandler();
							resp=handler.verifyOTP(cdr, url, chintf, urlParams);
							if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
								//Set Access Token into subregid
								cdr.setSubRegId(resp.getAccessToken());
								accessToken=resp.getAccessToken();
								//Change URL for charging
								resp=chargeWalletCustomer(cdr,chintf);
							
								//Commit Response
								if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
									message="Wallet does not exists. Create new Wallet, In Process";
								}else{
									message=resp.getResponseDescription();
								}
							}else{
								//Token Request Failed
								message=resp.getResponseDescription();
							}
							
						}else{
							message="OTP Verification failed/Not supported";
							resp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
							resp.setResponseDescription("OTP Verification failed/Not supported");
						}
						
					}
				}else{
					//INvalid Chintf
					message="Invalid Charging Interface Id while verifying OTP";
					resp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					resp.setResponseDescription("Invalid Charging Interface Id while verifying OTP");
					
				}
				
				//Commit Transaction Logs in Database
				
				
				if(resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS){
					cdr.setChargedAmount(cdr.getChargedPricePoint());
					cdr.setChargingDate(new java.util.Date(System.currentTimeMillis()));
					//Send SMS if required to be sent
				}
				cdr.setStatus(resp.getResponseCode());
				cdr.setStatusDescp(resp.getResponseDescription());
				
				
				try{
					Subscriber sub=subscriberDao.findByJocgCDRId(cdr.getId());
					if(sub!=null && sub.getId()!=null){
						if(resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS){
							sub.setChargedAmount(cdr.getChargedAmount());
							sub.setCustomerId(accessToken);
							sub.setSubSessionRevenue(sub.getSubSessionRevenue()+cdr.getChargedAmount());
							sub.setChargedOperatorKey(cdr.getChargedOperatorKey());
							sub.setChargedOperatorParams(cdr.getChargedOperatorParams());
							ChargingPricepoint cpp=jocgCache.getPricePointListById(cdr.getChargedPpid());
							sub.setValidity(cpp.getValidity());
							sub.setLastRenewalDate(new java.util.Date(System.currentTimeMillis()));
							sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
							sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
							sub=updateSubscriber(logHeader,sub,false);
							//subscriberDao.save(sub);
							
						}else{
							sub.setStatus(JocgStatusCodes.SUB_FAILED_TRANS);
							sub=updateSubscriber(logHeader,sub,false);
						}
						cdr.setSubRegId(sub.getId());
					}
					
					updateCDR(logHeader,cdr,false);
				}catch(Exception ee){
					logger.error(logHeader+"Failed to update Subscriber Data , Error "+ee.getMessage());
				}
				
				//Set View if not set already
				if(resp.getRespType()==null || resp.getRespType()!=JocgResponse.RESPTYPE_MODELANDVIEW){
					mv.setViewName("wchargestatus");
					mv.addObject("message", message);
				}
			}else{
				message=" Invalid Jocg Trans Id "+jocgTransId;
				mv.setViewName("wchargestatus");
				mv.addObject("message", message);
			}
		}catch(Exception e){
			message=" System Error!";
			mv.addObject("transid",jocgTransId);
			mv.setViewName(viewName);
			mv.addObject("message", message);
		}
		
		
		return mv;
		
	}
	
	
	public JocgResponse chargeWalletCustomer(ChargingCDR cdr,ChargingInterface chintf){
		JocgResponse jresp=new JocgResponse();
		List<ChargingUrl> urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
		System.out.println(" URL List "+((urlList!=null)?urlList.size():urlList));
		ChargingUrl url=null;
		for(ChargingUrl churl:urlList){
			if("SDP-CHARGE".equalsIgnoreCase(churl.getUrlCatg())) url=churl;
		}
		
		List<ChargingUrlParam> urlParams=jocgCache.getChargingUrlParams(url.getChurlId());
		if(chintf.getName().toLowerCase().contains("mobikwik")){
			MobikwikChargingHandler handler=new MobikwikChargingHandler();
			jresp=handler.chargeCustomer(cdr, url, chintf,urlParams);
		}else if(chintf.getName().toLowerCase().contains("freecharge")){
			FreechargeChargingHandler handler=new FreechargeChargingHandler();
			jresp=handler.chargeCustomer(cdr, url, chintf,urlParams);
		}else if(chintf.getName().toLowerCase().contains("oxigen")){
			OxigenChargingHandler handler=new OxigenChargingHandler();
			jresp=handler.chargeCustomer(cdr, url, chintf,urlParams);
		}else{
			jresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			jresp.setResponseDescription("Invalid Wallet! Cant not process charging Interface.");
		}
		
		return jresp;
	}
	
	@RequestMapping("/createwallet")
	public ModelAndView chargecreateWalletAccount(@RequestParam(value="transid",required=false,defaultValue="NA") String jocgTransId,
			@RequestParam(value="email",required=false,defaultValue="NA") String emailId){
		String resp="";
		String logHeader="CreateWallet:"+jocgTransId+":";
		JocgResponse jresp=new JocgResponse();
		ModelAndView mv=null;
		try{
			System.out.println("Processng Token Id "+emailId);
			ChargingCDR cdr=chargingCDRDao.findByjocgTransId(jocgTransId);
			if(cdr!=null && cdr.getId()!=null){
				logHeader +=""+cdr.getMsisdn()+":";
				cdr.setSubscriberId(emailId);;
				
				ChargingInterface chintf=jocgCache.getChargingInterface(cdr.getChargingInterfaceId());
				if(chintf!=null && chintf.getChintfid()>0){
					List<ChargingUrl> urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
					System.out.println(" URL List "+((urlList!=null)?urlList.size():urlList));
					ChargingUrl url=null;
					for(ChargingUrl churl:urlList){
						if("WALLET-CREATE".equalsIgnoreCase(churl.getUrlCatg())) url=churl;
					}
					
					List<ChargingUrlParam> urlParams=jocgCache.getChargingUrlParams(url.getChurlId());
					MobikwikChargingHandler handler=new MobikwikChargingHandler();
					jresp=handler.createNewWallet(cdr,url,chintf, urlParams,emailId);
					
					if(jresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
						//Charge Customer after Wallet is created Successfully
						jresp=chargeWalletCustomer(cdr,chintf);
						
						resp=jresp.getResponseDescription();
						if(jresp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
							resp="In Process";
						}else{
							resp=jresp.getResponseDescription();
						}
					}else{
						resp="Failed to register new wallet!";
					}
				}else
					resp="Charging Interface id "+cdr.getChargingInterfaceId()+" not found";
				
				//Commit Transaction Logs in Database
				
				
				if(jresp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS){
					cdr.setChargedAmount(cdr.getChargedPricePoint());
					cdr.setChargingDate(new java.util.Date(System.currentTimeMillis()));
					//Send SMS if required to be sent
				}
				cdr.setStatus(jresp.getResponseCode());
				cdr.setStatusDescp(jresp.getResponseDescription());
				updateCDR(logHeader,cdr,false);
				
				try{
					Subscriber sub=subscriberDao.findByJocgCDRId(cdr.getId());
					if(sub!=null && sub.getId()!=null){
						if(jresp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS){
							sub.setChargedAmount(cdr.getChargedAmount());
							sub.setSubSessionRevenue(sub.getSubSessionRevenue()+cdr.getChargedAmount());
							sub.setChargedOperatorKey(cdr.getChargedOperatorKey());
							sub.setChargedOperatorParams(cdr.getChargedOperatorParams());
							ChargingPricepoint cpp=jocgCache.getPricePointListById(cdr.getChargedPpid());
							sub.setValidity(cpp.getValidity());
							sub.setLastRenewalDate(new java.util.Date(System.currentTimeMillis()));
							sub.setSubChargedDate(new java.util.Date(System.currentTimeMillis()));
							sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
							updateSubscriber(logHeader,sub,false);
							subscriberDao.save(sub);
						}else if(jresp.getResponseCode()!=JocgStatusCodes.CHARGING_INPROCESS){
							sub.setStatus(JocgStatusCodes.SUB_FAILED_TRANS);
							updateSubscriber(logHeader,sub,false);
						}
					}
				}catch(Exception ee){
					logger.error(logHeader+"Failed to update Subscriber Data , Error "+ee.getMessage());
				}
				
				
				
			}else{
				resp="Transaction id "+jocgTransId+" not found";
			}
		}catch(Exception e){
			
			resp="Exception "+e.getMessage();
		}
		
		
		
		if(jresp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW && jresp.getRespView()!=null){
			return jresp.getRespView();
		}else{
			mv=new ModelAndView();
			mv.setViewName("status");
			mv.addObject("message", resp);
			return mv;
		}
		
	}
	
}
