package com.jss.jocg.services.data;

import java.lang.reflect.Field;

public class SubscriberTest {
String id;
String name;
int age;
public SubscriberTest(String id,String name,int age){
	this.id=id;
	this.name=name;
	this.age=age;
}

public String toString() {
	
    Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
    try {
        for (Field field : fields) {
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}


public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}



}
