package com.jss.jocg.services.data;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class TestDB {

	@org.springframework.data.annotation.Id
	String id;
	String name;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date subRequestDate;
	public TestDB(){}
	public TestDB(String id,String name,Date subRequestDate){
		this.id=id;
		this.name=name;
		this.subRequestDate=subRequestDate;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getSubRequestDate() {
		return subRequestDate;
	}
	public void setSubRequestDate(Date subRequestDate) {
		this.subRequestDate = subRequestDate;
	}
	
	
	
}
