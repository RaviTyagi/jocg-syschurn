package com.jss.jocg.services.data;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;


import org.springframework.format.annotation.DateTimeFormat;

import com.jss.jocg.util.JocgStatusCodes;

public class Subscriber {
	
	@org.springframework.data.annotation.Id
	String id;
	String lifecycleId;
	String subscriberId;
	Long msisdn;
	String transactionCategory;
	
	Integer sourceNetworkId;
	String sourceNetworkCode;
	String sourceNetworkName;
	
	Integer circleId;
	String circleName;
	String circleCode;
	
	String requestIP;
	
	Integer parentChintfId;
	Integer useParentChargingHandler;
	Integer chargingInterfaceId;
	String operatorName;
	String countryName;
	String currency;
	String gmtDifference;
	String chargingInterfaceType;
	
	Integer packageId;
	String packageName;
	Integer serviceId;
	String serviceName;
	String serviceCategory;
	Double packagePrice;
	
	Integer packageChargingInterfaceMapId;
	Integer pricepointId;
	Double operatorPricePoint;
	String validityCategory;
	String validityUnit;
	Integer validity;
	
	Double chargedAmount;
	String chargingCategory;
	
	
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date subRequestDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date subChargedDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	
	Date validityStartDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	
	Date validityEndDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	
	String renewalCategory;
	
	Boolean renewalRequired;
	Integer renewalStatus;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date lastRenewalDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date nextRenewalDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date parkingDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date graceDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date pendingDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date suspendDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date unsubDate;
	
	String contentType;
	String contentProffileType;
	Integer allowedContentCount;
	Integer usedContentCount;
	
	String jocgCDRId;
	String jocgTransId;
	String unsubCDRId;
	
	String cgTransactionId;
	String cgStatusCode;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date cgResponseTime;
	
	String operatorServiceKey;
	String operatorParams;
	String chargedOperatorKey;
	String chargedOperatorParams;
	Double subSessionRevenue;
	
	Integer status;
	String statusDescp;
	
	String activationSource;
	String chargingMode;
	
	Integer campaignId;
	String campaignName;
	Integer trafficSourceId;
	String trafficSourceName;
	String publisherId;
	
	Long voucherId;
	String voucherName;
	String voucherVendor;
	String voucherType;
	String voucherDiscountType;
	Double voucherDiscount;
	Double voucherChargedAmount;
	
	String systemId;
	String platformId;
	String customerCategory;
	
	String userField_0;
	String userField_1;
	String userField_2;
	String userField_3;
	String userField_4;
	String userField_5;
	String userField_6;
	String userField_7;
	String userField_8;
	String userField_9;
	String customerId;
	java.util.Date lastUpdated;
	String emailId;
	Boolean reportGenerated;
	Boolean churnProcessed;
	
	public Subscriber(){
		this.churnProcessed=false;
		this.reportGenerated=false;
		this.lastUpdated=new java.util.Date();
		this.circleId=0;
		this.circleName="NA";
		this.circleCode="NA";
		this.requestIP="NA";
		this.activationSource="NA";
		this.allowedContentCount=0;
		this.campaignId=0;
		this.campaignName="NA";
		this.cgResponseTime=null;
		this.cgStatusCode="NA";
		this.cgTransactionId="NA";
		this.chargedAmount=0D;
		this.chargedOperatorKey="NA";
		this.chargedOperatorParams="NA";
		this.chargingCategory="NA";
		this.chargingInterfaceId=0;
		this.chargingInterfaceType="NA";
		this.chargingMode="NA";
		this.contentProffileType="LIMITED";
		this.contentType="video";
		this.countryName="NA";
		this.currency="NA";
		this.customerCategory="NA";
		this.gmtDifference="NA";
		this.graceDate=null;
		this.jocgCDRId="NA";
		this.jocgTransId="NA";
		this.unsubCDRId="NA";
		this.lastRenewalDate=null;
		this.lifecycleId="NA";
		this.msisdn=0L;
		this.nextRenewalDate=null;
		this.operatorName="NA";
		this.operatorParams="NA";
		this.operatorPricePoint=0D;
		this.operatorServiceKey="NA";
		this.packageChargingInterfaceMapId=0;
		this.packageId=0;
		this.packageName="NA";
		this.packagePrice=0D;
		this.parentChintfId=0;
		this.parkingDate=null;
		this.pendingDate=null;
		this.platformId="NA";
		this.pricepointId=0;
		this.publisherId="NA";
		this.renewalCategory="OPERATOR";
		this.renewalRequired=true;
		this.renewalStatus=JocgStatusCodes.REN_NOT_REQUIRED;
		this.serviceCategory="NA";
		this.serviceId=0;
		this.serviceName="NA";
		this.sourceNetworkCode="NA";
		this.sourceNetworkId=0;
		this.sourceNetworkName="NA";
		this.status=0;
		this.statusDescp="NA";
		this.subChargedDate=null;
		this.subRequestDate=null;
		this.subscriberId="NA";
		this.subSessionRevenue=0D;
		this.suspendDate=null;
		this.systemId="NA";
		this.trafficSourceId=0;
		this.trafficSourceName="NA";
		this.transactionCategory="NA";
		this.unsubDate=null;
		this.usedContentCount=0;
		this.useParentChargingHandler=0;
		this.userField_0="NA";
		this.userField_1="NA";
		this.userField_2="NA";
		this.userField_3="NA";
		this.userField_4="NA";
		this.userField_5="NA";
		this.userField_6="NA";
		this.userField_7="NA";
		this.userField_8="NA";
		this.userField_9="NA";
		this.validity=0;
		this.validityCategory="NA";
		this.validityEndDate=null;
		this.validityStartDate=null;
		this.validityUnit="DAY";
		this.voucherChargedAmount=0D;
		this.voucherDiscount=0D;
		this.voucherDiscountType="NA";
		this.voucherId=0L;
		this.voucherName="NA";
		this.voucherType="NA";
		this.voucherVendor="NA";
		this.customerId="NA";
		this.emailId="NA";
		}

	
	public Subscriber(String id,Integer circleId,String circleName,String circleCode,String requestIP,String lifecycleId,String subscriberId,Long msisdn,String transactionCategory,Integer parentChintfId,Integer useParentChargingHandler,Integer sourceNetworkId,String sourceNetworkCode,String sourceNetworkName,Integer chargingInterfaceId,String operatorName,String countryName,String currency,String gmtDifference,String chargingInterfaceType,Integer packageId,String packageName,Integer serviceId,String serviceName,String serviceCategory,Double packagePrice,Integer packageChargingInterfaceMapId,Integer pricepointId,Double operatorPricePoint,String validityCategory,String validityUnit,Integer validity,Double chargedAmount,String chargingCategory,Date subRequestDate,Date subChargedDate,Date validityStartDate,Date validityEndDate,String renewalCategory,Boolean renewalRequired,Integer renewalStatus,Date lastRenewalDate,Date nextRenewalDate,Date parkingDate,Date graceDate,Date pendingDate,Date suspendDate,Date unsubDate,String contentType,String contentProffileType,Integer allowedContentCount,Integer usedContentCount,String jocgCDRId,String jocgTransId,String unsubCDRId,String cgTransactionId,String cgStatusCode,Date cgResponseTime,String operatorServiceKey,String operatorParams,String chargedOperatorKey,String chargedOperatorParams,Double subSessionRevenue,Integer status,String statusDescp,String activationSource,String chargingMode,Integer campaignId,String campaignName,Integer trafficSourceId,String trafficSourceName,String publisherId,Long voucherId,String voucherName,String voucherVendor,String voucherType,String voucherDiscountType,Double voucherDiscount,Double voucherChargedAmount,String systemId,String platformId,String customerCategory,String userField_0,String userField_1,String userField_2,String userField_3,String userField_4,String userField_5,String userField_6,String userField_7,String userField_8,String userField_9,String customerId,java.util.Date lastUpdated){
		this.id=id;
		this.renewalStatus=renewalStatus;
		this.unsubCDRId=unsubCDRId;
		this.circleId=circleId;
		this.circleName=circleName;
		this.circleCode=circleCode;
		this.requestIP=requestIP;
		this.activationSource=activationSource;
		this.allowedContentCount=allowedContentCount;
		this.campaignId=campaignId;
		this.campaignName=campaignName;
		this.cgResponseTime=cgResponseTime;
		this.cgStatusCode=cgStatusCode;
		this.cgTransactionId=cgTransactionId;
		this.chargedAmount=chargedAmount;
		this.chargedOperatorKey=chargedOperatorKey;
		this.chargedOperatorParams=chargedOperatorParams;
		this.chargingCategory=chargingCategory;
		this.chargingInterfaceId=chargingInterfaceId;
		this.chargingInterfaceType=chargingInterfaceType;
		this.chargingMode=chargingMode;
		this.contentProffileType=contentProffileType;
		this.contentType=contentType;
		this.countryName=countryName;
		this.currency=currency;
		this.customerCategory=customerCategory;
		this.gmtDifference=gmtDifference;
		this.graceDate=graceDate;
		this.jocgCDRId=jocgCDRId;
		this.jocgTransId=jocgTransId;
		this.lastRenewalDate=lastRenewalDate;
		this.lifecycleId=lifecycleId;
		this.msisdn=msisdn;
		this.nextRenewalDate=nextRenewalDate;
		this.operatorName=operatorName;
		this.operatorParams=operatorParams;
		this.operatorPricePoint=operatorPricePoint;
		this.operatorServiceKey=operatorServiceKey;
		this.packageChargingInterfaceMapId=packageChargingInterfaceMapId;
		this.packageId=packageId;
		this.packageName=packageName;
		this.packagePrice=packagePrice;
		this.parentChintfId=parentChintfId;
		this.parkingDate=parkingDate;
		this.pendingDate=pendingDate;
		this.platformId=platformId;
		this.pricepointId=pricepointId;
		this.publisherId=publisherId;
		this.renewalCategory=renewalCategory;
		this.renewalRequired=renewalRequired;
		this.serviceCategory=serviceCategory;
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.sourceNetworkCode=sourceNetworkCode;
		this.sourceNetworkId=sourceNetworkId;
		this.sourceNetworkName=sourceNetworkName;
		this.status=status;
		this.statusDescp=statusDescp;
		this.subChargedDate=subChargedDate;
		this.subRequestDate=subRequestDate;
		this.subscriberId=subscriberId;
		this.subSessionRevenue=subSessionRevenue;
		this.suspendDate=suspendDate;
		this.systemId=systemId;
		this.trafficSourceId=trafficSourceId;
		this.trafficSourceName=trafficSourceName;
		this.transactionCategory=transactionCategory;
		this.unsubDate=unsubDate;
		this.usedContentCount=usedContentCount;
		this.useParentChargingHandler=useParentChargingHandler;
		this.userField_0=userField_0;
		this.userField_1=userField_1;
		this.userField_2=userField_2;
		this.userField_3=userField_3;
		this.userField_4=userField_4;
		this.userField_5=userField_5;
		this.userField_6=userField_6;
		this.userField_7=userField_7;
		this.userField_8=userField_8;
		this.userField_9=userField_9;
		this.validity=validity;
		this.validityCategory=validityCategory;
		this.validityEndDate=validityEndDate;
		this.validityStartDate=validityStartDate;
		this.validityUnit=validityUnit;
		this.voucherChargedAmount=voucherChargedAmount;
		this.voucherDiscount=voucherDiscount;
		this.voucherDiscountType=voucherDiscountType;
		this.voucherId=voucherId;
		this.voucherName=voucherName;
		this.voucherType=voucherType;
		this.voucherVendor=voucherVendor;
		this.customerId=customerId;
		this.lastUpdated=lastUpdated;
	}

public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }


public String getId() {
	return id;
}


public void setId(String id) {
	this.id = id;
}


public String getLifecycleId() {
	return lifecycleId;
}


public void setLifecycleId(String lifecycleId) {
	this.lifecycleId = lifecycleId;
}


public String getSubscriberId() {
	return subscriberId;
}


public void setSubscriberId(String subscriberId) {
	this.subscriberId = subscriberId;
}


public Long getMsisdn() {
	return msisdn;
}


public void setMsisdn(Long msisdn) {
	this.msisdn = msisdn;
}


public String getTransactionCategory() {
	return transactionCategory;
}


public void setTransactionCategory(String transactionCategory) {
	this.transactionCategory = transactionCategory;
}


public Integer getParentChintfId() {
	return parentChintfId;
}


public void setParentChintfId(Integer parentChintfId) {
	this.parentChintfId = parentChintfId;
}


public Integer getUseParentChargingHandler() {
	return useParentChargingHandler;
}


public void setUseParentChargingHandler(Integer useParentChargingHandler) {
	this.useParentChargingHandler = useParentChargingHandler;
}


public Integer getSourceNetworkId() {
	return sourceNetworkId;
}


public void setSourceNetworkId(Integer sourceNetworkId) {
	this.sourceNetworkId = sourceNetworkId;
}


public String getSourceNetworkCode() {
	return sourceNetworkCode;
}


public void setSourceNetworkCode(String sourceNetworkCode) {
	this.sourceNetworkCode = sourceNetworkCode;
}


public String getSourceNetworkName() {
	return sourceNetworkName;
}


public void setSourceNetworkName(String sourceNetworkName) {
	this.sourceNetworkName = sourceNetworkName;
}


public Integer getChargingInterfaceId() {
	return chargingInterfaceId;
}


public void setChargingInterfaceId(Integer chargingInterfaceId) {
	this.chargingInterfaceId = chargingInterfaceId;
}


public String getOperatorName() {
	return operatorName;
}


public void setOperatorName(String operatorName) {
	this.operatorName = operatorName;
}


public String getCountryName() {
	return countryName;
}


public void setCountryName(String countryName) {
	this.countryName = countryName;
}


public String getCurrency() {
	return currency;
}


public void setCurrency(String currency) {
	this.currency = currency;
}


public String getGmtDifference() {
	return gmtDifference;
}


public void setGmtDifference(String gmtDifference) {
	this.gmtDifference = gmtDifference;
}


public String getChargingInterfaceType() {
	return chargingInterfaceType;
}


public void setChargingInterfaceType(String chargingInterfaceType) {
	this.chargingInterfaceType = chargingInterfaceType;
}


public Integer getPackageId() {
	return packageId;
}


public void setPackageId(Integer packageId) {
	this.packageId = packageId;
}


public String getPackageName() {
	return packageName;
}


public void setPackageName(String packageName) {
	this.packageName = packageName;
}


public Integer getServiceId() {
	return serviceId;
}


public void setServiceId(Integer serviceId) {
	this.serviceId = serviceId;
}


public String getServiceName() {
	return serviceName;
}


public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}


public String getServiceCategory() {
	return serviceCategory;
}


public void setServiceCategory(String serviceCategory) {
	this.serviceCategory = serviceCategory;
}


public Double getPackagePrice() {
	return packagePrice;
}


public void setPackagePrice(Double packagePrice) {
	this.packagePrice = packagePrice;
}


public Integer getPackageChargingInterfaceMapId() {
	return packageChargingInterfaceMapId;
}


public void setPackageChargingInterfaceMapId(Integer packageChargingInterfaceMapId) {
	this.packageChargingInterfaceMapId = packageChargingInterfaceMapId;
}


public Integer getPricepointId() {
	return pricepointId;
}


public void setPricepointId(Integer pricepointId) {
	this.pricepointId = pricepointId;
}


public Double getOperatorPricePoint() {
	return operatorPricePoint;
}


public void setOperatorPricePoint(Double operatorPricePoint) {
	this.operatorPricePoint = operatorPricePoint;
}


public String getValidityCategory() {
	return validityCategory;
}


public void setValidityCategory(String validityCategory) {
	this.validityCategory = validityCategory;
}


public String getValidityUnit() {
	return validityUnit;
}


public void setValidityUnit(String validityUnit) {
	this.validityUnit = validityUnit;
}


public Integer getValidity() {
	return validity;
}


public void setValidity(Integer validity) {
	this.validity = validity;
}


public Double getChargedAmount() {
	return chargedAmount;
}


public void setChargedAmount(Double chargedAmount) {
	this.chargedAmount = chargedAmount;
}


public String getChargingCategory() {
	return chargingCategory;
}


public void setChargingCategory(String chargingCategory) {
	this.chargingCategory = chargingCategory;
}


public Date getSubRequestDate() {
	return subRequestDate;
}


public void setSubRequestDate(Date subRequestDate) {
	this.subRequestDate = subRequestDate;
}


public Date getSubChargedDate() {
	return subChargedDate;
}


public void setSubChargedDate(Date subChargedDate) {
	this.subChargedDate = subChargedDate;
}


public Date getValidityStartDate() {
	return validityStartDate;
}


public void setValidityStartDate(Date validityStartDate) {
	this.validityStartDate = validityStartDate;
}


public Date getValidityEndDate() {
	return validityEndDate;
}


public void setValidityEndDate(Date validityEndDate) {
	this.validityEndDate = validityEndDate;
}


public String getRenewalCategory() {
	return renewalCategory;
}


public void setRenewalCategory(String renewalCategory) {
	this.renewalCategory = renewalCategory;
}


public Boolean getRenewalRequired() {
	return renewalRequired;
}


public void setRenewalRequired(Boolean renewalRequired) {
	this.renewalRequired = renewalRequired;
}


public Date getLastRenewalDate() {
	return lastRenewalDate;
}


public void setLastRenewalDate(Date lastRenewalDate) {
	this.lastRenewalDate = lastRenewalDate;
}


public Date getNextRenewalDate() {
	return nextRenewalDate;
}


public void setNextRenewalDate(Date nextRenewalDate) {
	this.nextRenewalDate = nextRenewalDate;
}


public Date getParkingDate() {
	return parkingDate;
}


public void setParkingDate(Date parkingDate) {
	this.parkingDate = parkingDate;
}


public Date getGraceDate() {
	return graceDate;
}


public void setGraceDate(Date graceDate) {
	this.graceDate = graceDate;
}


public Date getPendingDate() {
	return pendingDate;
}


public void setPendingDate(Date pendingDate) {
	this.pendingDate = pendingDate;
}


public Date getSuspendDate() {
	return suspendDate;
}


public void setSuspendDate(Date suspendDate) {
	this.suspendDate = suspendDate;
}


public Date getUnsubDate() {
	return unsubDate;
}


public void setUnsubDate(Date unsubDate) {
	this.unsubDate = unsubDate;
}


public String getContentType() {
	return contentType;
}


public void setContentType(String contentType) {
	this.contentType = contentType;
}


public String getContentProffileType() {
	return contentProffileType;
}


public void setContentProffileType(String contentProffileType) {
	this.contentProffileType = contentProffileType;
}


public Integer getAllowedContentCount() {
	return allowedContentCount;
}


public void setAllowedContentCount(Integer allowedContentCount) {
	this.allowedContentCount = allowedContentCount;
}


public Integer getUsedContentCount() {
	return usedContentCount;
}


public void setUsedContentCount(Integer usedContentCount) {
	this.usedContentCount = usedContentCount;
}


public String getJocgCDRId() {
	return jocgCDRId;
}


public void setJocgCDRId(String jocgCDRId) {
	this.jocgCDRId = jocgCDRId;
}


public String getCgTransactionId() {
	return cgTransactionId;
}


public void setCgTransactionId(String cgTransactionId) {
	this.cgTransactionId = cgTransactionId;
}


public String getCgStatusCode() {
	return cgStatusCode;
}


public void setCgStatusCode(String cgStatusCode) {
	this.cgStatusCode = cgStatusCode;
}


public Date getCgResponseTime() {
	return cgResponseTime;
}


public void setCgResponseTime(Date cgResponseTime) {
	this.cgResponseTime = cgResponseTime;
}


public String getOperatorServiceKey() {
	return operatorServiceKey;
}


public void setOperatorServiceKey(String operatorServiceKey) {
	this.operatorServiceKey = operatorServiceKey;
}


public String getOperatorParams() {
	return operatorParams;
}


public void setOperatorParams(String operatorParams) {
	this.operatorParams = operatorParams;
}


public String getChargedOperatorKey() {
	return chargedOperatorKey;
}


public void setChargedOperatorKey(String chargedOperatorKey) {
	this.chargedOperatorKey = chargedOperatorKey;
}


public String getChargedOperatorParams() {
	return chargedOperatorParams;
}


public void setChargedOperatorParams(String chargedOperatorParams) {
	this.chargedOperatorParams = chargedOperatorParams;
}


public Double getSubSessionRevenue() {
	return subSessionRevenue;
}


public void setSubSessionRevenue(Double subSessionRevenue) {
	this.subSessionRevenue = subSessionRevenue;
}


public Integer getStatus() {
	return status;
}


public void setStatus(Integer status) {
	this.status = status;
}


public String getStatusDescp() {
	return statusDescp;
}


public void setStatusDescp(String statusDescp) {
	this.statusDescp = statusDescp;
}


public String getActivationSource() {
	return activationSource;
}


public void setActivationSource(String activationSource) {
	this.activationSource = activationSource;
}


public String getChargingMode() {
	return chargingMode;
}


public void setChargingMode(String chargingMode) {
	this.chargingMode = chargingMode;
}


public Integer getCampaignId() {
	return campaignId;
}


public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
}


public String getCampaignName() {
	return campaignName;
}


public void setCampaignName(String campaignName) {
	this.campaignName = campaignName;
}


public Integer getTrafficSourceId() {
	return trafficSourceId;
}


public void setTrafficSourceId(Integer trafficSourceId) {
	this.trafficSourceId = trafficSourceId;
}


public String getTrafficSourceName() {
	return trafficSourceName;
}


public void setTrafficSourceName(String trafficSourceName) {
	this.trafficSourceName = trafficSourceName;
}


public String getPublisherId() {
	return publisherId;
}


public void setPublisherId(String publisherId) {
	this.publisherId = publisherId;
}


public Long getVoucherId() {
	return voucherId;
}


public void setVoucherId(Long voucherId) {
	this.voucherId = voucherId;
}


public String getVoucherName() {
	return voucherName;
}


public void setVoucherName(String voucherName) {
	this.voucherName = voucherName;
}


public String getVoucherVendor() {
	return voucherVendor;
}


public void setVoucherVendor(String voucherVendor) {
	this.voucherVendor = voucherVendor;
}


public String getVoucherType() {
	return voucherType;
}


public void setVoucherType(String voucherType) {
	this.voucherType = voucherType;
}


public String getVoucherDiscountType() {
	return voucherDiscountType;
}


public void setVoucherDiscountType(String voucherDiscountType) {
	this.voucherDiscountType = voucherDiscountType;
}


public Double getVoucherDiscount() {
	return voucherDiscount;
}


public void setVoucherDiscount(Double voucherDiscount) {
	this.voucherDiscount = voucherDiscount;
}


public Double getVoucherChargedAmount() {
	return voucherChargedAmount;
}


public void setVoucherChargedAmount(Double voucherChargedAmount) {
	this.voucherChargedAmount = voucherChargedAmount;
}


public String getSystemId() {
	return systemId;
}


public void setSystemId(String systemId) {
	this.systemId = systemId;
}


public String getPlatformId() {
	return platformId;
}


public void setPlatformId(String platformId) {
	this.platformId = platformId;
}


public String getCustomerCategory() {
	return customerCategory;
}


public void setCustomerCategory(String customerCategory) {
	this.customerCategory = customerCategory;
}


public String getUserField_0() {
	return userField_0;
}


public void setUserField_0(String userField_0) {
	this.userField_0 = userField_0;
}


public String getUserField_1() {
	return userField_1;
}


public void setUserField_1(String userField_1) {
	this.userField_1 = userField_1;
}


public String getUserField_2() {
	return userField_2;
}


public void setUserField_2(String userField_2) {
	this.userField_2 = userField_2;
}


public String getUserField_3() {
	return userField_3;
}


public void setUserField_3(String userField_3) {
	this.userField_3 = userField_3;
}


public String getUserField_4() {
	return userField_4;
}


public void setUserField_4(String userField_4) {
	this.userField_4 = userField_4;
}


public String getUserField_5() {
	return userField_5;
}


public void setUserField_5(String userField_5) {
	this.userField_5 = userField_5;
}


public String getUserField_6() {
	return userField_6;
}


public void setUserField_6(String userField_6) {
	this.userField_6 = userField_6;
}


public String getUserField_7() {
	return userField_7;
}


public void setUserField_7(String userField_7) {
	this.userField_7 = userField_7;
}


public String getUserField_8() {
	return userField_8;
}


public void setUserField_8(String userField_8) {
	this.userField_8 = userField_8;
}


public String getUserField_9() {
	return userField_9;
}


public void setUserField_9(String userField_9) {
	this.userField_9 = userField_9;
}


public Integer getCircleId() {
	return circleId;
}


public void setCircleId(Integer circleId) {
	this.circleId = circleId;
}


public String getCircleName() {
	return circleName;
}


public void setCircleName(String circleName) {
	this.circleName = circleName;
}


public String getCircleCode() {
	return circleCode;
}


public void setCircleCode(String circleCode) {
	this.circleCode = circleCode;
}


public String getRequestIP() {
	return requestIP;
}


public void setRequestIP(String requestIP) {
	this.requestIP = requestIP;
}


public String getJocgTransId() {
	return jocgTransId;
}


public void setJocgTransId(String jocgTransId) {
	this.jocgTransId = jocgTransId;
}


public Integer getRenewalStatus() {
	return renewalStatus;
}


public void setRenewalStatus(Integer renewalStatus) {
	this.renewalStatus = renewalStatus;
}


public String getUnsubCDRId() {
	return unsubCDRId;
}


public void setUnsubCDRId(String unsubCDRId) {
	this.unsubCDRId = unsubCDRId;
}


public String getCustomerId() {
	return customerId;
}


public void setCustomerId(String customerId) {
	this.customerId = customerId;
}


public java.util.Date getLastUpdated() {
	return lastUpdated;
}


public void setLastUpdated(java.util.Date lastUpdated) {
	this.lastUpdated = lastUpdated;
}


public String getEmailId() {
	return emailId;
}


public void setEmailId(String emailId) {
	this.emailId = emailId;
}


public Boolean getReportGenerated() {
	return reportGenerated;
}


public void setReportGenerated(Boolean reportGenerated) {
	this.reportGenerated = reportGenerated;
}


public Boolean getChurnProcessed() {
	return churnProcessed;
}


public void setChurnProcessed(Boolean churnProcessed) {
	this.churnProcessed = churnProcessed;
}

	
			

}
