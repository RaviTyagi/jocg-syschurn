package com.jss.jocg.services.data;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class ChargingCDR2 {
		
	@org.springframework.data.annotation.Id
	String id;
	String subscriberId;
	String requestType;
	Long msisdn;
	String requestSource;
	String requestCategory;
	Integer chintfId;
	String chintfName;
	String country;
	Integer aggregatorId;
	String aggregatorName;
	Integer circleId;
	String circleName;
	String circleCode;
	String gmtDifference;
	Integer packageId;
	String packageName;
	Integer serviceId;
	String serviceName;
	String serviceCategory;
	Integer trafficSourceId;
	String trafficSourceName;
	String trafficSourceToken;
	Integer campaignId;
	String campaignName;
	String contentType;
	String publisherId;
	String jocgTransId;
	String cgTransId;
	String cgStatus;
	String cgStatusCode;
	String sdpTransId;
	String sdpStatus;
	String sdpStatusCode;
	Double packagePrice;
	Double packagePriceCharged;
	Double sdpChargedAmount;
	Long voucherId;
	String voucherName;
	String discountType;
	Double discountValue;
	String voucherType;
	Integer voucherVendorId;
	String voucherVendorName;
	String voucherbatchId;
	Integer status;
	String statusDescp;
	Integer notifyStatus;
	String notifyDescp;
	Integer otpStatus;
	String otpPin;
	String deviceInfo;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date requestDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date cgResponseDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date sdpResponseDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date subChargedDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date validityStartDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	Date validityEndDate;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	String renewalCategory;
	Boolean renewalRequired;
	
	public ChargingCDR2(){}
	
	public ChargingCDR2(String subscriberId,String requestType,Long msisdn,String requestSource,Integer chintfId,String chintfName,
			String country,Integer aggregatorId,String aggregatorName,Integer circleId,String circleName,String circleCode,
			String gmtDifference,Integer packageId,String packageName,Integer serviceId,String serviceName,
			String serviceCategory,Integer trafficSourceId,String trafficSourceName,String trafficSourceToken,
			Integer campaignId, String campaignName, String contentType, String publisherId, String cgTransId, 
			String cgStatus, String cgStatusCode,String sdpTransId, String sdpStatus, String sdpStatusCode, 
			Double packagePrice, Double packagePriceCharged, Double sdpChargedAmount, Long voucherId,
			String voucherName, String discountType, Double discountValue, String voucherType, Integer voucherVendorId,
			String voucherVendorName, String voucherbatchId, Integer status, String statusDescp, Integer notifyStatus,
			String notifyDescp, Integer otpStatus, String otpPin, String deviceInfo,String jocgTransId){
		this.subscriberId=subscriberId;
		this.requestType=requestType;
		this.msisdn=msisdn;
		this.requestSource=requestSource;
		this.chintfId=chintfId;
		this.chintfName=chintfName;
		this.country=country;
		this.aggregatorId=aggregatorId;
		this.aggregatorName=aggregatorName;
		this.circleId=circleId;
		this.circleName=circleName;
		this.circleCode=circleCode;
		this.gmtDifference=gmtDifference;
		this.packageId=packageId;
		this.packageName=packageName;
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.serviceCategory=serviceCategory;
		this.trafficSourceId=trafficSourceId;
		this.trafficSourceName=trafficSourceName;
		this.trafficSourceToken=trafficSourceToken;
		this.campaignId=campaignId;
		this.campaignName=campaignName;
		this.contentType=contentType;
		this.publisherId=publisherId;
		this.cgTransId=cgTransId;
		this.cgStatus=cgStatus;
		this.cgStatusCode=cgStatusCode;
		this.sdpTransId=sdpTransId;
		this.sdpStatus=sdpStatus;
		this.sdpStatusCode=sdpStatusCode;
		this.packagePrice=packagePrice;
		this.packagePriceCharged=packagePriceCharged;
		this.sdpChargedAmount=sdpChargedAmount;
		this.voucherId=voucherId;
		this.voucherName=voucherName;
		this.discountType=discountType;
		this.discountValue=discountValue;
		this.voucherType=voucherType;
		this.voucherVendorId=voucherVendorId;
		this.voucherVendorName=voucherVendorName;
		this.voucherbatchId=voucherbatchId;
		this.status=status;
		this.statusDescp=statusDescp;
		this.notifyStatus=notifyStatus;
		this.notifyDescp=notifyDescp;
		this.otpStatus=otpStatus;
		this.otpPin=otpPin;
		this.deviceInfo=deviceInfo;
		this.jocgTransId=jocgTransId;

		
	}
public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getSubscriberId() {
	return subscriberId;
}

public void setSubscriberId(String subscriberId) {
	this.subscriberId = subscriberId;
}

public String getRequestType() {
	return requestType;
}

public void setRequestType(String requestType) {
	this.requestType = requestType;
}

public Long getMsisdn() {
	return msisdn;
}

public void setMsisdn(Long msisdn) {
	this.msisdn = msisdn;
}

public String getRequestSource() {
	return requestSource;
}

public void setRequestSource(String requestSource) {
	this.requestSource = requestSource;
}

public Integer getChintfId() {
	return chintfId;
}

public void setChintfId(Integer chintfId) {
	this.chintfId = chintfId;
}

public String getChintfName() {
	return chintfName;
}

public void setChintfName(String chintfName) {
	this.chintfName = chintfName;
}

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public Integer getAggregatorId() {
	return aggregatorId;
}

public void setAggregatorId(Integer aggregatorId) {
	this.aggregatorId = aggregatorId;
}

public String getAggregatorName() {
	return aggregatorName;
}

public void setAggregatorName(String aggregatorName) {
	this.aggregatorName = aggregatorName;
}

public Integer getCircleId() {
	return circleId;
}

public void setCircleId(Integer circleId) {
	this.circleId = circleId;
}

public String getCircleName() {
	return circleName;
}

public void setCircleName(String circleName) {
	this.circleName = circleName;
}

public String getCircleCode() {
	return circleCode;
}

public void setCircleCode(String circleCode) {
	this.circleCode = circleCode;
}

public String getGmtDifference() {
	return gmtDifference;
}

public void setGmtDifference(String gmtDifference) {
	this.gmtDifference = gmtDifference;
}

public Integer getPackageId() {
	return packageId;
}

public void setPackageId(Integer packageId) {
	this.packageId = packageId;
}

public String getPackageName() {
	return packageName;
}

public void setPackageName(String packageName) {
	this.packageName = packageName;
}

public Integer getServiceId() {
	return serviceId;
}

public void setServiceId(Integer serviceId) {
	this.serviceId = serviceId;
}

public String getServiceName() {
	return serviceName;
}

public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}

public String getServiceCategory() {
	return serviceCategory;
}

public void setServiceCategory(String serviceCategory) {
	this.serviceCategory = serviceCategory;
}

public Integer getTrafficSourceId() {
	return trafficSourceId;
}

public void setTrafficSourceId(Integer trafficSourceId) {
	this.trafficSourceId = trafficSourceId;
}

public String getTrafficSourceName() {
	return trafficSourceName;
}

public void setTrafficSourceName(String trafficSourceName) {
	this.trafficSourceName = trafficSourceName;
}

public String getTrafficSourceToken() {
	return trafficSourceToken;
}

public void setTrafficSourceToken(String trafficSourceToken) {
	this.trafficSourceToken = trafficSourceToken;
}

public Integer getCampaignId() {
	return campaignId;
}

public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
}

public String getCampaignName() {
	return campaignName;
}

public void setCampaignName(String campaignName) {
	this.campaignName = campaignName;
}

public String getContentType() {
	return contentType;
}

public void setContentType(String contentType) {
	this.contentType = contentType;
}

public String getPublisherId() {
	return publisherId;
}

public void setPublisherId(String publisherId) {
	this.publisherId = publisherId;
}

public String getCgTransId() {
	return cgTransId;
}

public void setCgTransId(String cgTransId) {
	this.cgTransId = cgTransId;
}

public String getCgStatus() {
	return cgStatus;
}

public void setCgStatus(String cgStatus) {
	this.cgStatus = cgStatus;
}

public String getCgStatusCode() {
	return cgStatusCode;
}

public void setCgStatusCode(String cgStatusCode) {
	this.cgStatusCode = cgStatusCode;
}

public String getSdpTransId() {
	return sdpTransId;
}

public void setSdpTransId(String sdpTransId) {
	this.sdpTransId = sdpTransId;
}

public String getSdpStatus() {
	return sdpStatus;
}

public void setSdpStatus(String sdpStatus) {
	this.sdpStatus = sdpStatus;
}

public String getSdpStatusCode() {
	return sdpStatusCode;
}

public void setSdpStatusCode(String sdpStatusCode) {
	this.sdpStatusCode = sdpStatusCode;
}

public Double getPackagePrice() {
	return packagePrice;
}

public void setPackagePrice(Double packagePrice) {
	this.packagePrice = packagePrice;
}

public Double getPackagePriceCharged() {
	return packagePriceCharged;
}

public void setPackagePriceCharged(Double packagePriceCharged) {
	this.packagePriceCharged = packagePriceCharged;
}

public Double getSdpChargedAmount() {
	return sdpChargedAmount;
}

public void setSdpChargedAmount(Double sdpChargedAmount) {
	this.sdpChargedAmount = sdpChargedAmount;
}

public Long getVoucherId() {
	return voucherId;
}

public void setVoucherId(Long voucherId) {
	this.voucherId = voucherId;
}

public String getVoucherName() {
	return voucherName;
}

public void setVoucherName(String voucherName) {
	this.voucherName = voucherName;
}

public String getDiscountType() {
	return discountType;
}

public void setDiscountType(String discountType) {
	this.discountType = discountType;
}

public Double getDiscountValue() {
	return discountValue;
}

public void setDiscountValue(Double discountValue) {
	this.discountValue = discountValue;
}

public String getVoucherType() {
	return voucherType;
}

public void setVoucherType(String voucherType) {
	this.voucherType = voucherType;
}

public Integer getVoucherVendorId() {
	return voucherVendorId;
}

public void setVoucherVendorId(Integer voucherVendorId) {
	this.voucherVendorId = voucherVendorId;
}

public String getVoucherVendorName() {
	return voucherVendorName;
}

public void setVoucherVendorName(String voucherVendorName) {
	this.voucherVendorName = voucherVendorName;
}

public String getVoucherbatchId() {
	return voucherbatchId;
}

public void setVoucherbatchId(String voucherbatchId) {
	this.voucherbatchId = voucherbatchId;
}

public Integer getStatus() {
	return status;
}

public void setStatus(Integer status) {
	this.status = status;
}

public String getStatusDescp() {
	return statusDescp;
}

public void setStatusDescp(String statusDescp) {
	this.statusDescp = statusDescp;
}

public Integer getNotifyStatus() {
	return notifyStatus;
}

public void setNotifyStatus(Integer notifyStatus) {
	this.notifyStatus = notifyStatus;
}

public String getNotifyDescp() {
	return notifyDescp;
}

public void setNotifyDescp(String notifyDescp) {
	this.notifyDescp = notifyDescp;
}

public Integer getOtpStatus() {
	return otpStatus;
}

public void setOtpStatus(Integer otpStatus) {
	this.otpStatus = otpStatus;
}

public String getOtpPin() {
	return otpPin;
}

public void setOtpPin(String otpPin) {
	this.otpPin = otpPin;
}

public String getDeviceInfo() {
	return deviceInfo;
}

public void setDeviceInfo(String deviceInfo) {
	this.deviceInfo = deviceInfo;
}
public String getJocgTransId() {
	return jocgTransId;
}
public void setJocgTransId(String jocgTransId) {
	this.jocgTransId = jocgTransId;
}

	

}
