package com.jss.jocg.services.data;

import java.sql.Timestamp;

import org.springframework.format.annotation.DateTimeFormat;

public class AirtelIndiaPendingTransactions {
	@org.springframework.data.annotation.Id
	String id;
	String accessTokenStr;
	String cdrId;
	String subscriberId;
	Integer status;
	String statusDescp;
	Integer retryCount;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date transTime;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date lastprocessTime;
	
	public AirtelIndiaPendingTransactions(){
		accessTokenStr="";
		cdrId="";
		subscriberId="";
		status=0;
		statusDescp="";
		retryCount=0;
		transTime=new java.util.Date();
		lastprocessTime=new java.util.Date();
	}
	
	public AirtelIndiaPendingTransactions(String id,String accessTokenStr,String cdrId,
			String subscriberId,Integer status,String statusDescp,Integer retryCount,java.util.Date transTime,java.util.Date lastprocessTime){
		this.id=id;
		this.accessTokenStr=accessTokenStr;
		this.cdrId=cdrId;
		this.subscriberId=subscriberId;
		this.status=status;
		this.statusDescp=statusDescp;
		this.retryCount=retryCount;
		this.transTime=transTime;
		this.lastprocessTime=lastprocessTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessTokenStr() {
		return accessTokenStr;
	}

	public void setAccessTokenStr(String accessTokenStr) {
		this.accessTokenStr = accessTokenStr;
	}

	public String getCdrId() {
		return cdrId;
	}

	public void setCdrId(String cdrId) {
		this.cdrId = cdrId;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public java.util.Date getTransTime() {
		return transTime;
	}

	public void setTransTime(java.util.Date transTime) {
		this.transTime = transTime;
	}

	public java.util.Date getLastprocessTime() {
		return lastprocessTime;
	}

	public void setLastprocessTime(java.util.Date lastprocessTime) {
		this.lastprocessTime = lastprocessTime;
	}

	public String getStatusDescp() {
		return statusDescp;
	}

	public void setStatusDescp(String statusDescp) {
		this.statusDescp = statusDescp;
	}
	
	
	
}
