package com.jss.jocg.services.data;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.util.JocgStatusCodes;

public class ChargingCDR {
		
	@org.springframework.data.annotation.Id
	String id;
	String subRegId;
	String jocgTransId;
	
	//Request Parameters
	String requestCategory;
	String requestType;
	String subscriberId;
	Long msisdn;
	String platformName;
	String rqPackageName;
	Double rqPackagePrice;
	Integer rqPackageValidity;
	String rqNetworkType;
	String rqVoucherVendor;
	Double rqAmountToBeCharged;
	String rqVoucherCode;
	
	String systemId;
	/**
	 VoucherDetail voucher,PackageDetail packDetails,
			ChargingInterface sourceChintf,ChargingInterface chargingOperator,ChargingPricepoint chpp,
			PackageChintfmap pkgchintfMap
	 */
	
	
	//Source Network Details
	Integer sourceNetworkId;
	String sourceNetworkName;
	String sourceNetworkCode;
	
	//Charging Interface Name
	Integer aggregatorId;
	String aggregatorName;
	Boolean useParentHandler;
	Integer chargingInterfaceId;
	String chargingInterfaceName;
	String countryName;
	String currencyName;
	String gmtDiff;
	String chargingInterfaceType;
	Integer packageChintfId;
	//Circle Details
	Integer circleId;
	String circleName;
	String circleCode;
	
	//Add Package Details
	
	Integer packageId;
	String packageName;
	Double packagePrice;
	Integer serviceId;
	String serviceName;
	String serviceCategory;
	
		
	//Price Point Details
	Integer pricePointId;
	Double operatorPricePoint;
	String validityCategory;
	String validityUnit;
	Integer validityPeriod;
	String mppOperatorKey;
	String mppOperatorParams;
	
	//Charged Pricepoint details
	Integer chargedPpid;
	Double chargedPricePoint;
	Boolean fallbackCharged;
	String chargedValidityPeriod;
	String chargedOperatorKey;
	String chargedOperatorParams;
	
	
	
	
	//Add Voucher Details
	Long voucherId;
	String voucherName;
	Integer voucherVendorId;
	String voucherVendorName;
	String voucherType;
	Double voucherDiscount;
	String voucherDiscountType;
	
	
	
	String chargingCategory;
	Double discountGiven;
	Double chargedAmount;
	
	Boolean renewalRequired;
	String renewalCategory;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date chargingDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date subRegDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date validityStartDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date validityEndDate;


	String deviceDetails;
	String userAgent;
	String deviceOS;
	
	String referer;
	
	//Campaign Details
	Integer trafficSourceId;
	String trafficSourceName;
	String trafficSourceToken;
	Integer campaignId;
	String campaignName;
	String contentType;
	String publisherId;
	Integer notifyStatus;
	String notifyDescp;
		@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date notifyTime;
		
	Boolean churn20Min;
	Boolean churnSameDay;
	Boolean churn24Hour;
	Boolean notifyChurn;
	Integer notifyChurnStatus;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date notifyTimeChurn;
		
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date notifyScheduleTime;
	Integer landingPageId;
	Integer cgImageId;
	
	Integer landingPageScheduleId;
	Integer routingScheduleId;
	
	Integer otpStatus;
	String otpPin;
	
	//Transaction Status Fields
	Integer status;
	String statusDescp;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date requestDate;
	
	String cgStatusCode;
	String cgStatusDescp;
	String cgTransactionId;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date cgResponseTime;
	
	String sdpStatusCode;
	String sdpStatusDescp;
	String sdpTransactionId;
	String sdpLifeCycleId;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date sdpResponseTime;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date parkingDate;
	
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date parkingToActivationDate;
	
	Boolean activatedFromZero;
	Boolean activatedFromParking;
	Boolean activatedFromGrace;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date lastUpdated;
	Boolean reportGenerated;
	String emailId;
	
	public ChargingCDR(){
		this.reportGenerated=false;
		this.lastUpdated=new java.util.Date();
		this.subRegId="NA";
		this.landingPageScheduleId=0;
		this.routingScheduleId=0;
		this.jocgTransId="NA";
		this.requestCategory="NA";
		this.requestType="NA";
		this.subscriberId="NA";
		this.msisdn=0L;
		this.platformName="NA";
		this.rqPackageName="NA";
		this.rqPackagePrice=0D;
		this.rqPackageValidity=0;
		this.rqNetworkType="NA";
		this.rqVoucherVendor="NA";
		this.rqAmountToBeCharged=0D;
		this.rqVoucherCode="NA";
		this.systemId="NA";
		this.sourceNetworkId=0;
		this.sourceNetworkName="NA";
		this.sourceNetworkCode="NA";
		this.aggregatorId=0;
		this.aggregatorName="NA";
		this.useParentHandler=false;
		this.chargingInterfaceId=0;
		this.chargingInterfaceName="NA";
		this.countryName="NA";
		this.currencyName="NA";
		this.gmtDiff="NA";
		this.chargingInterfaceType="NA";
		this.packageChintfId=0;
		this.circleId=0;
		this.circleName="NA";
		this.circleCode="NA";
		this.packageId=0;
		this.packageName="NA";
		this.packagePrice=0D;
		this.serviceId=0;
		this.serviceName="NA";
		this.serviceCategory="NA";
		this.pricePointId=0;
		this.operatorPricePoint=0D;
		this.validityCategory="NA";
		this.validityUnit="NA";
		this.validityPeriod=0;
		this.mppOperatorKey="NA";
		this.mppOperatorParams="NA";
		this.chargedPpid=0;
		this.chargedPricePoint=0D;
		this.fallbackCharged=false;
		this.chargedValidityPeriod="NA";
		this.chargedOperatorKey="NA";
		this.chargedOperatorParams="NA";
		this.voucherId=0L;
		this.voucherName="NA";
		this.voucherVendorId=0;
		this.voucherVendorName="NA";
		this.voucherType="NA";
		this.voucherDiscount=0D;
		this.voucherDiscountType="NA";
		this.chargingCategory="NA";
		this.discountGiven=0D;
		this.chargedAmount=0D;
		this.renewalRequired=false;
		this.renewalCategory="NA";
		this.chargingDate=null;
		this.subRegDate=null;
		this.validityStartDate=null;
		this.validityEndDate=null;
		this.deviceDetails="NA";
		this.userAgent="NA";
		this.referer="NA";
		this.trafficSourceId=0;
		this.trafficSourceName="NA";
		this.trafficSourceToken="NA";
		this.campaignId=0;
		this.campaignName="NA";
		this.contentType="NA";
		this.publisherId="NA";
		this.notifyStatus=0;
		this.notifyDescp="NA";
		this.notifyTime=null;
		this.notifyScheduleTime=null;
		
		churn20Min=false;
		churnSameDay=false;
		churn24Hour=false;
		notifyChurn=false;
		notifyChurnStatus=JocgStatusCodes.NOTIFICATION_NOTSENT;
		notifyTimeChurn=null;
		
		this.landingPageId=0;
		this.cgImageId=0;
		this.otpStatus=0;
		this.otpPin="NA";
		this.status=0;
		this.statusDescp="NA";
		this.requestDate=null;
		this.cgStatusCode="NA";
		this.cgStatusDescp="NA";
		this.cgTransactionId="NA";
		this.cgResponseTime=null;
		this.sdpStatusCode="NA";
		this.sdpStatusDescp="NA";
		this.sdpTransactionId="NA";
		this.sdpLifeCycleId="NA";
		this.sdpResponseTime=null;
		this.parkingDate=null;
		this.parkingToActivationDate=null;
		this.emailId="NA";

		
	}
	
	
	public ChargingCDR(String id,String subRegId,Integer landingPageScheduleId,Integer routingScheduleId,String jocgTransId,String requestCategory,String requestType,String subscriberId,Long msisdn,String platformName,String rqPackageName,Double rqPackagePrice,Integer rqPackageValidity,String rqNetworkType,String rqVoucherVendor,Double rqAmountToBeCharged,String rqVoucherCode,String systemId,Integer sourceNetworkId,String sourceNetworkName,String sourceNetworkCode,Integer aggregatorId,String aggregatorName,Boolean useParentHandler,Integer chargingInterfaceId,String chargingInterfaceName,String countryName,String currencyName,String gmtDiff,String chargingInterfaceType,Integer packageChintfId,Integer circleId,String circleName,String circleCode,Integer packageId,String packageName,Double packagePrice,Integer serviceId,String serviceName,String serviceCategory,Integer pricePointId,Double operatorPricePoint,String validityCategory,String validityUnit,Integer validityPeriod,String mppOperatorKey,String mppOperatorParams,Integer chargedPpid,Double chargedPricePoint,Boolean fallbackCharged,String chargedValidityPeriod,String chargedOperatorKey,String chargedOperatorParams,Long voucherId,String voucherName,Integer voucherVendorId,String voucherVendorName,String voucherType,Double voucherDiscount,String voucherDiscountType,String chargingCategory,Double discountGiven,Double chargedAmount,Boolean renewalRequired,String renewalCategory,java.util.Date chargingDate,java.util.Date validityStartDate,java.util.Date validityEndDate,String deviceDetails,String userAgent,String referer,Integer trafficSourceId,String trafficSourceName,String trafficSourceToken,Integer campaignId,String campaignName,String contentType,String publisherId,Integer notifyStatus,String notifyDescp,java.util.Date notifyTime,java.util.Date notifyScheduleTime,Boolean churn20Min,Boolean churnSameDay,Boolean notifyChurn,Integer notifyChurnStatus,java.util.Date notifyTimeChurn,Integer landingPageId,Integer cgImageId,Integer otpStatus,String otpPin,Integer status,String statusDescp,java.util.Date requestDate,String cgStatusCode,String cgStatusDescp,String cgTransactionId,java.util.Date cgResponseTime,String sdpStatusCode,String sdpStatusDescp,String sdpTransactionId,String sdpLifeCycleId,java.util.Date sdpResponseTime,java.util.Date lastUpdated,Boolean reportGenerated,Boolean churn24Hour,java.util.Date  subRegDate){
		
		this.subRegId=subRegId;
		this.landingPageScheduleId=landingPageScheduleId;
		this.routingScheduleId=routingScheduleId;
		this.sdpLifeCycleId=sdpLifeCycleId;
		this.jocgTransId=jocgTransId;
		this.requestCategory=requestCategory;
		this.requestType=requestType;
		this.subscriberId=subscriberId;
		this.msisdn=msisdn;
		this.platformName=platformName;
		this.rqPackageName=rqPackageName;
		this.rqPackagePrice=rqPackagePrice;
		this.rqPackageValidity=rqPackageValidity;
		this.rqNetworkType=rqNetworkType;
		this.rqVoucherVendor=rqVoucherVendor;
		this.rqAmountToBeCharged=rqAmountToBeCharged;
		this.rqVoucherCode=rqVoucherCode;
		this.systemId=systemId;
		this.sourceNetworkId=sourceNetworkId;
		this.sourceNetworkName=sourceNetworkName;
		this.sourceNetworkCode=sourceNetworkCode;
		this.aggregatorId=aggregatorId;
		this.aggregatorName=aggregatorName;
		this.useParentHandler=useParentHandler;
		this.chargingInterfaceId=chargingInterfaceId;
		this.chargingInterfaceName=chargingInterfaceName;
		this.countryName=countryName;
		this.currencyName=currencyName;
		this.gmtDiff=gmtDiff;
		this.chargingInterfaceType=chargingInterfaceType;
		this.packageChintfId=packageChintfId;
		this.circleId=circleId;
		this.circleName=circleName;
		this.circleCode=circleCode;
		this.packageId=packageId;
		this.packageName=packageName;
		this.packagePrice=packagePrice;
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.serviceCategory=serviceCategory;
		this.pricePointId=pricePointId;
		this.operatorPricePoint=operatorPricePoint;
		this.validityCategory=validityCategory;
		this.validityUnit=validityUnit;
		this.validityPeriod=validityPeriod;
		this.mppOperatorKey=mppOperatorKey;
		this.mppOperatorParams=mppOperatorParams;
		this.chargedPpid=chargedPpid;
		this.chargedPricePoint=chargedPricePoint;
		this.fallbackCharged=fallbackCharged;
		this.chargedValidityPeriod=chargedValidityPeriod;
		this.chargedOperatorKey=chargedOperatorKey;
		this.chargedOperatorParams=chargedOperatorParams;
		this.voucherId=voucherId;
		this.voucherName=voucherName;
		this.voucherVendorId=voucherVendorId;
		this.voucherVendorName=voucherVendorName;
		this.voucherType=voucherType;
		this.voucherDiscount=voucherDiscount;
		this.voucherDiscountType=voucherDiscountType;
		this.chargingCategory=chargingCategory;
		this.discountGiven=discountGiven;
		this.chargedAmount=chargedAmount;
		this.renewalRequired=renewalRequired;
		this.renewalCategory=renewalCategory;
		this.chargingDate=chargingDate;
		this.subRegDate=subRegDate;
		this.validityStartDate=validityStartDate;
		this.validityEndDate=validityEndDate;
		this.deviceDetails=deviceDetails;
		this.userAgent=userAgent;
		this.referer=referer;
		this.trafficSourceId=trafficSourceId;
		this.trafficSourceName=trafficSourceName;
		this.trafficSourceToken=trafficSourceToken;
		this.campaignId=campaignId;
		this.campaignName=campaignName;
		this.contentType=contentType;
		this.publisherId=publisherId;
		this.notifyStatus=notifyStatus;
		this.notifyDescp=notifyDescp;
		this.notifyTime=notifyTime;
		this.notifyScheduleTime=notifyScheduleTime;
		this.churn20Min=churn20Min;
		this.churnSameDay=churnSameDay;
		this.churn24Hour=churn24Hour;
		this.notifyChurn=notifyChurn;
		this.notifyChurnStatus=notifyChurnStatus;
		this.notifyTimeChurn=notifyTimeChurn;
		this.landingPageId=landingPageId;
		this.cgImageId=cgImageId;
		this.otpStatus=otpStatus;
		this.otpPin=otpPin;
		this.status=status;
		this.statusDescp=statusDescp;
		this.requestDate=requestDate;
		this.cgStatusCode=cgStatusCode;
		this.cgStatusDescp=cgStatusDescp;
		this.cgTransactionId=cgTransactionId;
		this.cgResponseTime=cgResponseTime;
		this.sdpStatusCode=sdpStatusCode;
		this.sdpStatusDescp=sdpStatusDescp;
		this.sdpTransactionId=sdpTransactionId;
		this.sdpResponseTime=sdpResponseTime;
		this.lastUpdated=lastUpdated;
		this.reportGenerated=reportGenerated;
		
	}

public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }


public String getId() {
	return id;
}


public void setId(String id) {
	this.id = id;
}


public String getJocgTransId() {
	return jocgTransId;
}


public void setJocgTransId(String jocgTransId) {
	this.jocgTransId = jocgTransId;
}


public String getRequestCategory() {
	return requestCategory;
}


public void setRequestCategory(String requestCategory) {
	this.requestCategory = requestCategory;
}


public String getSubscriberId() {
	return subscriberId;
}


public void setSubscriberId(String subscriberId) {
	this.subscriberId = subscriberId;
}


public Long getMsisdn() {
	return msisdn;
}


public void setMsisdn(Long msisdn) {
	this.msisdn = msisdn;
}


public String getPlatformName() {
	return platformName;
}


public void setPlatformName(String platformName) {
	this.platformName = platformName;
}


public String getRqPackageName() {
	return rqPackageName;
}


public void setRqPackageName(String rqPackageName) {
	this.rqPackageName = rqPackageName;
}


public Double getRqPackagePrice() {
	return rqPackagePrice;
}


public void setRqPackagePrice(Double rqPackagePrice) {
	this.rqPackagePrice = rqPackagePrice;
}


public Integer getRqPackageValidity() {
	return rqPackageValidity;
}


public void setRqPackageValidity(Integer rqPackageValidity) {
	this.rqPackageValidity = rqPackageValidity;
}


public String getRqNetworkType() {
	return rqNetworkType;
}


public void setRqNetworkType(String rqNetworkType) {
	this.rqNetworkType = rqNetworkType;
}


public String getRqVoucherVendor() {
	return rqVoucherVendor;
}


public void setRqVoucherVendor(String rqVoucherVendor) {
	this.rqVoucherVendor = rqVoucherVendor;
}


public Double getRqAmountToBeCharged() {
	return rqAmountToBeCharged;
}


public void setRqAmountToBeCharged(Double rqAmountToBeCharged) {
	this.rqAmountToBeCharged = rqAmountToBeCharged;
}


public String getRqVoucherCode() {
	return rqVoucherCode;
}


public void setRqVoucherCode(String rqVoucherCode) {
	this.rqVoucherCode = rqVoucherCode;
}


public String getSystemId() {
	return systemId;
}


public void setSystemId(String systemId) {
	this.systemId = systemId;
}


public Integer getSourceNetworkId() {
	return sourceNetworkId;
}


public void setSourceNetworkId(Integer sourceNetworkId) {
	this.sourceNetworkId = sourceNetworkId;
}


public String getSourceNetworkName() {
	return sourceNetworkName;
}


public void setSourceNetworkName(String sourceNetworkName) {
	this.sourceNetworkName = sourceNetworkName;
}


public String getSourceNetworkCode() {
	return sourceNetworkCode;
}


public void setSourceNetworkCode(String sourceNetworkCode) {
	this.sourceNetworkCode = sourceNetworkCode;
}


public Integer getAggregatorId() {
	return aggregatorId;
}


public void setAggregatorId(Integer aggregatorId) {
	this.aggregatorId = aggregatorId;
}


public String getAggregatorName() {
	return aggregatorName;
}


public void setAggregatorName(String aggregatorName) {
	this.aggregatorName = aggregatorName;
}


public Boolean getUseParentHandler() {
	return useParentHandler;
}


public void setUseParentHandler(Boolean useParentHandler) {
	this.useParentHandler = useParentHandler;
}


public Integer getChargingInterfaceId() {
	return chargingInterfaceId;
}


public void setChargingInterfaceId(Integer chargingInterfaceId) {
	this.chargingInterfaceId = chargingInterfaceId;
}


public String getChargingInterfaceName() {
	return chargingInterfaceName;
}


public void setChargingInterfaceName(String chargingInterfaceName) {
	this.chargingInterfaceName = chargingInterfaceName;
}


public String getCountryName() {
	return countryName;
}


public void setCountryName(String countryName) {
	this.countryName = countryName;
}


public String getCurrencyName() {
	return currencyName;
}


public void setCurrencyName(String currencyName) {
	this.currencyName = currencyName;
}


public String getGmtDiff() {
	return gmtDiff;
}


public void setGmtDiff(String gmtDiff) {
	this.gmtDiff = gmtDiff;
}


public String getChargingInterfaceType() {
	return chargingInterfaceType;
}


public void setChargingInterfaceType(String chargingInterfaceType) {
	this.chargingInterfaceType = chargingInterfaceType;
}


public Integer getPackageChintfId() {
	return packageChintfId;
}


public void setPackageChintfId(Integer packageChintfId) {
	this.packageChintfId = packageChintfId;
}


public Integer getCircleId() {
	return circleId;
}


public void setCircleId(Integer circleId) {
	this.circleId = circleId;
}


public String getCircleName() {
	return circleName;
}


public void setCircleName(String circleName) {
	this.circleName = circleName;
}


public String getCircleCode() {
	return circleCode;
}


public void setCircleCode(String circleCode) {
	this.circleCode = circleCode;
}


public Integer getPackageId() {
	return packageId;
}


public void setPackageId(Integer packageId) {
	this.packageId = packageId;
}


public String getPackageName() {
	return packageName;
}


public void setPackageName(String packageName) {
	this.packageName = packageName;
}


public Double getPackagePrice() {
	return packagePrice;
}


public void setPackagePrice(Double packagePrice) {
	this.packagePrice = packagePrice;
}


public Integer getServiceId() {
	return serviceId;
}


public void setServiceId(Integer serviceId) {
	this.serviceId = serviceId;
}


public String getServiceName() {
	return serviceName;
}


public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}


public String getServiceCategory() {
	return serviceCategory;
}


public void setServiceCategory(String serviceCategory) {
	this.serviceCategory = serviceCategory;
}


public Integer getPricePointId() {
	return pricePointId;
}


public void setPricePointId(Integer pricePointId) {
	this.pricePointId = pricePointId;
}


public Double getOperatorPricePoint() {
	return operatorPricePoint;
}


public void setOperatorPricePoint(Double operatorPricePoint) {
	this.operatorPricePoint = operatorPricePoint;
}


public String getValidityCategory() {
	return validityCategory;
}


public void setValidityCategory(String validityCategory) {
	this.validityCategory = validityCategory;
}


public String getValidityUnit() {
	return validityUnit;
}


public void setValidityUnit(String validityUnit) {
	this.validityUnit = validityUnit;
}


public Integer getValidityPeriod() {
	return validityPeriod;
}


public void setValidityPeriod(Integer validityPeriod) {
	this.validityPeriod = validityPeriod;
}


public String getMppOperatorKey() {
	return mppOperatorKey;
}


public void setMppOperatorKey(String mppOperatorKey) {
	this.mppOperatorKey = mppOperatorKey;
}


public String getMppOperatorParams() {
	return mppOperatorParams;
}


public void setMppOperatorParams(String mppOperatorParams) {
	this.mppOperatorParams = mppOperatorParams;
}


public Integer getChargedPpid() {
	return chargedPpid;
}


public void setChargedPpid(Integer chargedPpid) {
	this.chargedPpid = chargedPpid;
}


public Double getChargedPricePoint() {
	return chargedPricePoint;
}


public void setChargedPricePoint(Double chargedPricePoint) {
	this.chargedPricePoint = chargedPricePoint;
}


public Boolean getFallbackCharged() {
	return fallbackCharged;
}


public void setFallbackCharged(Boolean fallbackCharged) {
	this.fallbackCharged = fallbackCharged;
}


public String getChargedValidityPeriod() {
	return chargedValidityPeriod;
}


public void setChargedValidityPeriod(String chargedValidityPeriod) {
	this.chargedValidityPeriod = chargedValidityPeriod;
}


public String getChargedOperatorKey() {
	return chargedOperatorKey;
}


public void setChargedOperatorKey(String chargedOperatorKey) {
	this.chargedOperatorKey = chargedOperatorKey;
}


public String getChargedOperatorParams() {
	return chargedOperatorParams;
}


public void setChargedOperatorParams(String chargedOperatorParams) {
	this.chargedOperatorParams = chargedOperatorParams;
}


public Long getVoucherId() {
	return voucherId;
}


public void setVoucherId(Long voucherId) {
	this.voucherId = voucherId;
}


public String getVoucherName() {
	return voucherName;
}


public void setVoucherName(String voucherName) {
	this.voucherName = voucherName;
}


public Integer getVoucherVendorId() {
	return voucherVendorId;
}


public void setVoucherVendorId(Integer voucherVendorId) {
	this.voucherVendorId = voucherVendorId;
}


public String getVoucherVendorName() {
	return voucherVendorName;
}


public void setVoucherVendorName(String voucherVendorName) {
	this.voucherVendorName = voucherVendorName;
}


public String getVoucherType() {
	return voucherType;
}


public void setVoucherType(String voucherType) {
	this.voucherType = voucherType;
}


public Double getVoucherDiscount() {
	return voucherDiscount;
}


public void setVoucherDiscount(Double voucherDiscount) {
	this.voucherDiscount = voucherDiscount;
}


public String getVoucherDiscountType() {
	return voucherDiscountType;
}


public void setVoucherDiscountType(String voucherDiscountType) {
	this.voucherDiscountType = voucherDiscountType;
}


public String getChargingCategory() {
	return chargingCategory;
}


public void setChargingCategory(String chargingCategory) {
	this.chargingCategory = chargingCategory;
}


public Double getDiscountGiven() {
	return discountGiven;
}


public void setDiscountGiven(Double discountGiven) {
	this.discountGiven = discountGiven;
}


public Double getChargedAmount() {
	return chargedAmount;
}


public void setChargedAmount(Double chargedAmount) {
	this.chargedAmount = chargedAmount;
}


public Boolean getRenewalRequired() {
	return renewalRequired;
}


public void setRenewalRequired(Boolean renewalRequired) {
	this.renewalRequired = renewalRequired;
}


public String getRenewalCategory() {
	return renewalCategory;
}


public void setRenewalCategory(String renewalCategory) {
	this.renewalCategory = renewalCategory;
}


public java.util.Date getChargingDate() {
	return chargingDate;
}


public void setChargingDate(java.util.Date chargingDate) {
	this.chargingDate = chargingDate;
}


public java.util.Date getValidityStartDate() {
	return validityStartDate;
}


public void setValidityStartDate(java.util.Date validityStartDate) {
	this.validityStartDate = validityStartDate;
}


public java.util.Date getValidityEndDate() {
	return validityEndDate;
}


public void setValidityEndDate(java.util.Date validityEndDate) {
	this.validityEndDate = validityEndDate;
}


public String getDeviceDetails() {
	return deviceDetails;
}


public void setDeviceDetails(String deviceDetails) {
	this.deviceDetails = deviceDetails;
}


public String getUserAgent() {
	return userAgent;
}


public void setUserAgent(String userAgent) {
	this.userAgent = userAgent;
}


public String getDeviceOS() {
	return deviceOS;
}


public void setDeviceOS(String deviceOS) {
	this.deviceOS = deviceOS;
}


public String getReferer() {
	return referer;
}


public void setReferer(String referer) {
	this.referer = referer;
}


public Integer getTrafficSourceId() {
	return trafficSourceId;
}


public void setTrafficSourceId(Integer trafficSourceId) {
	this.trafficSourceId = trafficSourceId;
}


public String getTrafficSourceName() {
	return trafficSourceName;
}


public void setTrafficSourceName(String trafficSourceName) {
	this.trafficSourceName = trafficSourceName;
}


public String getTrafficSourceToken() {
	return trafficSourceToken;
}


public void setTrafficSourceToken(String trafficSourceToken) {
	this.trafficSourceToken = trafficSourceToken;
}


public Integer getCampaignId() {
	return campaignId;
}


public void setCampaignId(Integer campaignId) {
	this.campaignId = campaignId;
}


public String getCampaignName() {
	return campaignName;
}


public void setCampaignName(String campaignName) {
	this.campaignName = campaignName;
}


public String getContentType() {
	return contentType;
}


public void setContentType(String contentType) {
	this.contentType = contentType;
}


public String getPublisherId() {
	return publisherId;
}


public void setPublisherId(String publisherId) {
	this.publisherId = publisherId;
}


public Integer getNotifyStatus() {
	return notifyStatus;
}


public void setNotifyStatus(Integer notifyStatus) {
	this.notifyStatus = notifyStatus;
}


public String getNotifyDescp() {
	return notifyDescp;
}


public void setNotifyDescp(String notifyDescp) {
	this.notifyDescp = notifyDescp;
}


public java.util.Date getNotifyTime() {
	return notifyTime;
}


public void setNotifyTime(java.util.Date notifyTime) {
	this.notifyTime = notifyTime;
}


public Integer getOtpStatus() {
	return otpStatus;
}


public void setOtpStatus(Integer otpStatus) {
	this.otpStatus = otpStatus;
}


public String getOtpPin() {
	return otpPin;
}


public void setOtpPin(String otpPin) {
	this.otpPin = otpPin;
}


public Integer getStatus() {
	return status;
}


public void setStatus(Integer status) {
	this.status = status;
}


public String getStatusDescp() {
	return statusDescp;
}


public void setStatusDescp(String statusDescp) {
	this.statusDescp = statusDescp;
}


public java.util.Date getRequestDate() {
	return requestDate;
}


public void setRequestDate(java.util.Date requestDate) {
	this.requestDate = requestDate;
}


public String getCgStatusCode() {
	return cgStatusCode;
}


public void setCgStatusCode(String cgStatusCode) {
	this.cgStatusCode = cgStatusCode;
}


public String getCgStatusDescp() {
	return cgStatusDescp;
}


public void setCgStatusDescp(String cgStatusDescp) {
	this.cgStatusDescp = cgStatusDescp;
}


public String getCgTransactionId() {
	return cgTransactionId;
}


public void setCgTransactionId(String cgTransactionId) {
	this.cgTransactionId = cgTransactionId;
}


public java.util.Date getCgResponseTime() {
	return cgResponseTime;
}


public void setCgResponseTime(java.util.Date cgResponseTime) {
	this.cgResponseTime = cgResponseTime;
}


public String getSdpStatusCode() {
	return sdpStatusCode;
}


public void setSdpStatusCode(String sdpStatusCode) {
	this.sdpStatusCode = sdpStatusCode;
}


public String getSdpStatusDescp() {
	return sdpStatusDescp;
}


public void setSdpStatusDescp(String sdpStatusDescp) {
	this.sdpStatusDescp = sdpStatusDescp;
}


public String getSdpTransactionId() {
	return sdpTransactionId;
}


public void setSdpTransactionId(String sdpTransactionId) {
	this.sdpTransactionId = sdpTransactionId;
}


public java.util.Date getSdpResponseTime() {
	return sdpResponseTime;
}


public void setSdpResponseTime(java.util.Date sdpResponseTime) {
	this.sdpResponseTime = sdpResponseTime;
}


public java.util.Date getParkingDate() {
	return parkingDate;
}


public void setParkingDate(java.util.Date parkingDate) {
	this.parkingDate = parkingDate;
}


public java.util.Date getParkingToActivationDate() {
	return parkingToActivationDate;
}


public void setParkingToActivationDate(java.util.Date parkingToActivationDate) {
	this.parkingToActivationDate = parkingToActivationDate;
}


public String getRequestType() {
	return requestType;
}


public void setRequestType(String requestType) {
	this.requestType = requestType;
}


public String getSdpLifeCycleId() {
	return sdpLifeCycleId;
}


public void setSdpLifeCycleId(String sdpLifeCycleId) {
	this.sdpLifeCycleId = sdpLifeCycleId;
}


public java.util.Date getNotifyScheduleTime() {
	return notifyScheduleTime;
}


public void setNotifyScheduleTime(java.util.Date notifyScheduleTime) {
	this.notifyScheduleTime = notifyScheduleTime;
}


public Integer getLandingPageId() {
	return landingPageId;
}


public void setLandingPageId(Integer landingPageId) {
	this.landingPageId = landingPageId;
}


public Integer getCgImageId() {
	return cgImageId;
}


public void setCgImageId(Integer cgImageId) {
	this.cgImageId = cgImageId;
}


public Boolean getActivatedFromZero() {
	return activatedFromZero;
}


public void setActivatedFromZero(Boolean activatedFromZero) {
	this.activatedFromZero = activatedFromZero;
}


public Boolean getActivatedFromParking() {
	return activatedFromParking;
}


public void setActivatedFromParking(Boolean activatedFromParking) {
	this.activatedFromParking = activatedFromParking;
}


public Boolean getActivatedFromGrace() {
	return activatedFromGrace;
}


public void setActivatedFromGrace(Boolean activatedFromGrace) {
	this.activatedFromGrace = activatedFromGrace;
}


public Boolean getChurn20Min() {
	return churn20Min;
}


public void setChurn20Min(Boolean churn20Min) {
	this.churn20Min = churn20Min;
}


public Boolean getChurnSameDay() {
	return churnSameDay;
}


public void setChurnSameDay(Boolean churnSameDay) {
	this.churnSameDay = churnSameDay;
}


public Boolean getNotifyChurn() {
	return notifyChurn;
}


public void setNotifyChurn(Boolean notifyChurn) {
	this.notifyChurn = notifyChurn;
}


public Integer getNotifyChurnStatus() {
	return notifyChurnStatus;
}


public void setNotifyChurnStatus(Integer notifyChurnStatus) {
	this.notifyChurnStatus = notifyChurnStatus;
}


public java.util.Date getNotifyTimeChurn() {
	return notifyTimeChurn;
}


public void setNotifyTimeChurn(java.util.Date notifyTimeChurn) {
	this.notifyTimeChurn = notifyTimeChurn;
}


public Integer getLandingPageScheduleId() {
	return landingPageScheduleId;
}


public void setLandingPageScheduleId(Integer landingPageScheduleId) {
	this.landingPageScheduleId = landingPageScheduleId;
}


public Integer getRoutingScheduleId() {
	return routingScheduleId;
}


public void setRoutingScheduleId(Integer routingScheduleId) {
	this.routingScheduleId = routingScheduleId;
}


public String getSubRegId() {
	return subRegId;
}


public void setSubRegId(String subRegId) {
	this.subRegId = subRegId;
}


public java.util.Date getLastUpdated() {
	return lastUpdated;
}


public void setLastUpdated(java.util.Date lastUpdated) {
	this.lastUpdated = lastUpdated;
}


public Boolean getReportGenerated() {
	return reportGenerated;
}


public void setReportGenerated(Boolean reportGenerated) {
	this.reportGenerated = reportGenerated;
}


public java.util.Date getSubRegDate() {
	return subRegDate;
}


public void setSubRegDate(java.util.Date subRegDate) {
	this.subRegDate = subRegDate;
}


public Boolean getChurn24Hour() {
	return churn24Hour;
}


public void setChurn24Hour(Boolean churn24Hour) {
	this.churn24Hour = churn24Hour;
}


public String getEmailId() {
	return emailId;
}


public void setEmailId(String emailId) {
	this.emailId = emailId;
}

	

}
