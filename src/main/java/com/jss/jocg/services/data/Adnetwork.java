package com.jss.jocg.services.data;

import java.lang.reflect.Field;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.data.annotation.Id;

public class Adnetwork {

	@Id
	String id;
	String clickId;
	String campaignId;
	String pubId;
	String redirectUrl;
	Integer status;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date requestTime;
	Integer callbackStatus;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date callbackTime;
	
	Integer churnStatus;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date churnCallbackTime;
	
	Integer wasteStatus;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date wasteCallbackTime;
	
	public Adnetwork(){
		this.wasteStatus=0;
		this.wasteCallbackTime=null;
		this.churnStatus=0;
		this.churnCallbackTime=null;
		this.campaignId="NA";
		this.clickId="NA";
		this.pubId="NA";
		this.redirectUrl="NA";
		this.status=0;
		this.requestTime=null;
		this.callbackStatus=0;
		this.callbackTime=null;
	}
	
	public Adnetwork(String id,String clickId,String campaignId,String pubId,String redirectUrl,Integer status,
		java.util.Date requestTime,Integer callbackStatus,java.util.Date callbackTime,Integer churnStatus,
		java.util.Date churnCallbackTime,Integer wasteStatus,java.util.Date wasteCallbackTime){
		this.wasteStatus=wasteStatus;
		this.wasteCallbackTime=wasteCallbackTime;
		this.churnStatus=churnStatus;
		this.churnCallbackTime=churnCallbackTime;
		this.id=id;
		this.clickId=clickId;
		this.campaignId=campaignId;
		this.pubId=pubId;
		this.redirectUrl=redirectUrl;
		this.status=status;
		this.requestTime=requestTime;
		this.callbackStatus=callbackStatus;
		this.callbackTime=callbackTime;
		
	}

public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCampaignId() {
		return campaignId;
	}

	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getPubId() {
		return pubId;
	}

	public void setPubId(String pubId) {
		this.pubId = pubId;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public java.util.Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(java.util.Date requestTime) {
		this.requestTime = requestTime;
	}

	public Integer getCallbackStatus() {
		return callbackStatus;
	}

	public void setCallbackStatus(Integer callbackStatus) {
		this.callbackStatus = callbackStatus;
	}

	public java.util.Date getCallbackTime() {
		return callbackTime;
	}

	public void setCallbackTime(java.util.Date callbackTime) {
		this.callbackTime = callbackTime;
	}

	public String getClickId() {
		return clickId;
	}

	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

	public Integer getChurnStatus() {
		return churnStatus;
	}

	public void setChurnStatus(Integer churnStatus) {
		this.churnStatus = churnStatus;
	}

	public java.util.Date getChurnCallbackTime() {
		return churnCallbackTime;
	}

	public void setChurnCallbackTime(java.util.Date churnCallbackTime) {
		this.churnCallbackTime = churnCallbackTime;
	}

	public Integer getWasteStatus() {
		return wasteStatus;
	}

	public void setWasteStatus(Integer wasteStatus) {
		this.wasteStatus = wasteStatus;
	}

	public java.util.Date getWasteCallbackTime() {
		return wasteCallbackTime;
	}

	public void setWasteCallbackTime(java.util.Date wasteCallbackTime) {
		this.wasteCallbackTime = wasteCallbackTime;
	}
	
	
	
	
}
