package com.jss.jocg.services.data;

public class SubscriberActiveBase {

	String sourceNetworkCode;
	String serviceName;
	String packageName;
	String operatorName;
	String circleName;
	Integer activeBase;
	
	public SubscriberActiveBase(){
		
	}
	
	@Override
	public String toString(){
		return "SubscriberActiveBase{operatorName:"+operatorName+",circleName:"+circleName+",activeBaseCount:"+activeBase+"}";
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getCircleName() {
		return circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}

	public Integer getActiveBase() {
		return activeBase;
	}

	public void setActiveBase(Integer activeBase) {
		this.activeBase = activeBase;
	}

	public String getSourceNetworkCode() {
		return sourceNetworkCode;
	}

	public void setSourceNetworkCode(String sourceNetworkCode) {
		this.sourceNetworkCode = sourceNetworkCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	
	
}
