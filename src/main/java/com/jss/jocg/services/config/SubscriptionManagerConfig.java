package com.jss.jocg.services.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.ChargingProcessor;
import com.jss.jocg.services.bo.CircleIdentifier;
import com.jss.jocg.services.bo.JocgDeviceManager;
import com.jss.jocg.services.bo.NotificationProcessor;
import com.jss.jocg.services.bo.RequestValidator;
import com.jss.jocg.services.bo.ResponseBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;

@Configuration
public class SubscriptionManagerConfig {

	@Bean
	public SubscriptionManager getSubscriptionManagerBean(){
		return new SubscriptionManager();
	}
	
	@Bean
	public JocgDeviceManager getJocgDeviceManagerBean(){
		return new JocgDeviceManager();
	}
	
	@Bean
	public ChargingProcessor getChargingProcessorBean(){
		return new ChargingProcessor();
	}
	
	@Bean 
	public CircleIdentifier getCircleIdentifier(){
		return new CircleIdentifier();
	}
	
	@Bean RequestValidator getRequestValidator(){
		return new RequestValidator();
	}
	
	@Bean ResponseBuilder getResponseBuilder(){
		return new ResponseBuilder();
		
	}
	@Bean NotificationProcessor getNotificationProcessor(){
		return new NotificationProcessor();
	}
	
	@Bean CDRBuilder getCDRBuilder(){
		return new CDRBuilder();
	}
}
