package com.jss.jocg.services;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.callback.bo.CommonSDPCallbackReceiver;
import com.jss.jocg.callback.bo.JunoCallbackReceiver;
import com.jss.jocg.callback.bo.PyroBSNLSDPCallbackReceiver;
import com.jss.jocg.callback.bo.VodafoneIndiaSDPCallbackReceiver;
import com.jss.jocg.callback.data.AircelIndiaCGCallback;
import com.jss.jocg.callback.data.AircelIndiaSDPCallback;
import com.jss.jocg.callback.data.CommonSDPCallback;
import com.jss.jocg.callback.data.ConnectSDPCallback;
import com.jss.jocg.callback.data.IdeaSdpCallback;
import com.jss.jocg.callback.data.JunoSDPCallback;
import com.jss.jocg.callback.data.RelianceSDPCallback;
import com.jss.jocg.callback.data.WalletCommonCallback;
import com.jss.jocg.callback.data.bsnl.NetExcelBSNLSDPCallback;
import com.jss.jocg.callback.data.bsnl.PyroBSNLSDPCallback;
import com.jss.jocg.callback.data.voda.VodafoneIndiaCGResponse;
import com.jss.jocg.callback.data.voda.VodafoneIndiaSDPCallback;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.NotificationProcessor;
import com.jss.jocg.services.bo.ResponseBuilder;
import com.jss.jocg.services.bo.airtel.ActivationCallback;
import com.jss.jocg.services.bo.airtel.AirtelApiResponseWraper;
import com.jss.jocg.services.dao.ChargingCDRRepositoryImpl;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.mongodb.DBObject;

/**
 Digivive Idea Call Back URL : http://119.252.215.63/digiviveCallback/? 

Digivive Vodafone Call Back URL : https://nexgtv.digivive.com/OVOD/VendorCallback.aspx?
//https://nexgtv.digivive.com/OVOD/VendorCallback.aspx?MSISDN=919403519815&SERVICE_ID=NXGTV_FAVSHOWM&CLASS=NXGTV_FAVSHOWM&TXNID=9534733295&ACTION=ACT&STATUS=SUCCESS&CHARGING_MODE=MONTHLY&REQUEST_ID=Digivive_BF038_158206605&request_mode=WAP_D2C&CIRCLE_ID=15&SUB_TYPE=PREPAID
 
 */

@RestController
@RequestMapping("/")
public class CallbackService extends JocgService{
	private static final Logger logger = LoggerFactory
			.getLogger(CallbackService.class);

	SimpleDateFormat sdf=new SimpleDateFormat("yyyy|MM|dd|HH|mm|ss|");
	SimpleDateFormat sdffile=new SimpleDateFormat("yyyyMMdd");
	
	@Autowired JmsTemplate jmsTemplate;
	@Autowired ResponseBuilder respBuilder;
	@Autowired NotificationProcessor notificationHandler;
	@Autowired ChargingCDRRepositoryImpl chrIMpl;
	
	@Autowired JunoCallbackReceiver junoReceiver;
	@Autowired CommonSDPCallbackReceiver commonSDPreceiver;
	@Autowired VodafoneIndiaSDPCallbackReceiver vodaJmsListener;
	
	@RequestMapping("jocg/callback/test.aspx")
	public @ResponseBody String getHelloTest(@RequestParam String name){
	//	AggregationResults<TrafficStats> result=chrIMpl.getTrafficStats(null);
	//	List<TrafficStats> ts= subscriberDao.findAggregate();//result.getMappedResults();
		//List<Object> resultObj=null;//result.getMappedResults();
		//List<ChargingCDR> cdr=chrIMpl.getTrafficStats(null);
		return "Hello '"+name+"' from Callback Service <br/>"+chrIMpl.getCDRTrafficStats();
	}
	
	/**BSNL STV Pack Callback*/
	@RequestMapping("/bsnlRetail/DataBundleService.svc/trn")
	public @ResponseBody String processSTVCallback(@RequestParam(value="msisdn" ,required=false,defaultValue="0") Long msisdn,
			@RequestParam(value="status" ,required=false,defaultValue="NA") String status,
			@RequestParam(value="VASID" ,required=false,defaultValue="NA") String transactionId,
			@RequestParam(value="type" ,required=false,defaultValue="NA") String validity){
		logger.debug("STVCallback : RecivedParamList : "+requestContext.getQueryString());
		logger.debug("STVCallback : Recived : "+msisdn);
		int operatorId=9;
		//IdentifyOperator Circle based on msisdn series
		MsisdnSeries circle=circleIndentifier.identifyMsisdnSeriesByMsisdn(msisdn);
		if(circle!=null && circle.getCircleid()>0)
			operatorId=circle.getChintfid();		
		logger.debug("STVCallback : Recived : "+operatorId);
		ChargingInterface chintf=jocgCache.getChargingInterface(operatorId);		
		logger.debug("STVCallback : chintf : "+chintf);
		String bsnlStvPacks=jocgCache.getBsnlSTVPacks();	
		logger.debug("STVCallback : triggers property : "+bsnlStvPacks);
		List<String> packList=JocgString.convertStrToList(bsnlStvPacks, "|");
		String keyToSearch=""+operatorId+"_"+validity;
		String key="",value="";int chppId=0;
		for(String s:packList){
			key=s.substring(0,s.indexOf("="));
			value=s.substring(s.indexOf("=")+1);
			if(key.equalsIgnoreCase(keyToSearch)){
				logger.debug("STVCallback : key matched with : "+s);
				chppId=JocgString.strToInt(value, 0);
				if(chppId>0) break;
			}
		}
		logger.debug("STVCallback : chppId : "+chppId);
		if(chppId>0){
			ChargingPricepoint chpp=jocgCache.getPricePointListById(chppId);
			logger.debug("STVCallback : chpp : "+chpp);
			PackageChintfmap chintfMap=jocgCache.getPackageChargingInterfaceMap(chpp.getChintfid(),chpp.getChintfid(), chpp.getPpid());
			logger.debug("STVCallback : chintfMap : "+chintfMap);
			PackageDetail pkgDetail=jocgCache.getPackageDetailsByID(chintfMap.getPkgid());
			logger.debug("STVCallback : pkgDetail : "+pkgDetail);
			//Now process for the Package subscription			
			if(operatorId==9 || operatorId==8){												
			
				logger.debug("pyroSTVCallback : Recived :  chppId "+chppId);
				logger.debug("pyroSTVCallback : Recived :  operatorId "+operatorId);
				logger.debug("pyroSTVCallback : Recived :  Price Point "+chpp.getPricePoint());	
				logger.debug("pyroSTVCallback : Recived :  msisdn "+msisdn);
				logger.debug("pyroSTVCallback : Recived :  Service  "+chpp.getOperatorKey());
				logger.debug("pyroSTVCallback : Recived :  ChargingInterface | "+chpp.getChintfid());
				
				
				PyroBSNLSDPCallback pyroSdpCallback=new PyroBSNLSDPCallback();
				pyroSdpCallback.setAction("new_subscription");
				pyroSdpCallback.setServiceId(chpp.getOperatorKey());
				pyroSdpCallback.setMsisdn(msisdn);
				pyroSdpCallback.setConsentSource("STV");
				pyroSdpCallback.setCHARGE(Double.toString(chpp.getPricePoint()));
				pyroSdpCallback.setDescription("SUCCESS");
				pyroSdpCallback.setPpid(Integer.toString(chppId));
				pyroSdpCallback.setTid(transactionId);
				
				jmsTemplate.convertAndSend("callback.bsnlpyro", pyroSdpCallback);				
				
				
			}else if (operatorId==11 || operatorId==10){
				logger.debug("NetExcelSTVCallback : Recived :  chppId "+chppId);
				logger.debug("NetExcelSTVCallback : Recived :  operatorId "+operatorId);
				logger.debug("NetExcelSTVCallback : Recived :  Price Point "+chpp.getPricePoint());	
				logger.debug("NetExcelSTVCallback : Recived :  msisdn "+msisdn);
				logger.debug("NetExcelSTVCallback : Recived :  Service  "+chpp.getOperatorKey());
				logger.debug("NetExcelSTVCallback : Recived :  ChargingInterface | "+chpp.getChintfid());
				
				
				NetExcelBSNLSDPCallback NetxlSdpCallback=new NetExcelBSNLSDPCallback();
				NetxlSdpCallback.setReq_type("ACT");
				NetxlSdpCallback.setServiceId(chpp.getOperatorKey());
				NetxlSdpCallback.setMsisdn(msisdn);				
				NetxlSdpCallback.setConsent_Status("SUCCESS");
				NetxlSdpCallback.setPrice(chpp.getPricePoint());
				NetxlSdpCallback.setZoneName("STV");
				NetxlSdpCallback.setPpid(chppId);
				NetxlSdpCallback.setCg_id(transactionId);
				
				jmsTemplate.convertAndSend("callback.bsnlNetExcel", NetxlSdpCallback);				
				
			}
			else{
				logger.debug("STVCallback : Recived & Avoid becauase not matched the conf ");
			}
		}else{
			logger.error("BSNL STV Packs : Invalid Price point Validity |msisdn="+msisdn+",VASID="+transactionId+",validity="+validity+", Status="+status);
		}
		SimpleDateFormat sdfBsnl=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String respStr="OK "+sdfBsnl.format(new java.util.Date(System.currentTimeMillis()));
		
		return respStr;
	}
	
	
	@RequestMapping("jocg/callback/aircelcg")
    public ModelAndView processAircelCGRedirect(HttpServletRequest request){
          //msisdn=9999631990&result=SUCCESS&reason=&transid=10101&serespcode=201&contentid=416&cgid=CGTEST
          String transId=request.getParameter("transid");
          String cgStatus=request.getParameter("result");
          String cgStatusCode=request.getParameter("serespcode");
          String msisdn=request.getParameter("msisdn");         
          String cgid=request.getParameter("cgid");
          StringBuilder sb=new StringBuilder();           
          logger.debug("AircelIndiaCG-Redirect::msisdn="+msisdn+"|CG_TrxnId="+cgid+"|CGStatus="+cgStatus+"|CGStatusCode="+cgStatusCode+"|TRXID="+transId+"|QueryString="+request.getQueryString());            
          String respMsg="";
          try{
                if("201".equalsIgnoreCase(cgStatusCode)){
                      respMsg="Transaction is accpted and under processing.";
                }else{
                      respMsg="Sorry! Transaction Failed.";
                }
                transId=JocgString.formatString(transId, "NA");
                Long msisdnVal=JocgString.strToLong(msisdn, 0);
                ChargingCDR cdr=null;
                if(!"NA".equalsIgnoreCase(transId))
                      cdr=searchCDRByJocgTransId(transId);
          
                if((cdr==null || cdr.getId()==null) && msisdnVal.longValue()>0){
                      logger.debug("AircelIndiaCG-Redirect::msisdn="+msisdn+" Failed to search by TransId, Searching by Msisdn "+msisdnVal);
                      List<ChargingCDR> cdrList=searchLastCDRByMsisdn(msisdnVal);
                      logger.debug("cdrList "+cdrList);
                      for(ChargingCDR newCDR:cdrList){
                            try{
                                  logger.debug("Comparing Date "+newCDR.getRequestDate());
                                  logger.debug("With Date "+cdr.getRequestDate());
                                  cdr=(cdr==null || cdr.getId()==null)?newCDR:newCDR.getRequestDate().after(cdr.getRequestDate())?newCDR:cdr;
                            }catch(Exception e){}
                      }
                      
                }
                
                logger.debug("AircelIndiaCG-Redirect::msisdn="+msisdn+" CDR Found "+cdr);
                if(cdr!=null && cdr.getId()!=null){
                      cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
                      cdr.setCgStatusCode(cgStatusCode);
                      cdr.setCgStatusDescp(cgStatus+"|"+request.getQueryString());
                      cdr.setCgTransactionId(cgid);
                      logger.debug("AircelCGRedirect::"+msisdn+":: Status Codes :"+cgStatusCode);
                      if("CGW202".equalsIgnoreCase(cgStatusCode)){
                            cdr.setStatus(JocgStatusCodes.CHARGING_CGSUCCESS);
                            cdr.setStatusDescp("CG Consent Received,Transaction is Process");
                            respMsg="Transaction is accepted and under processing.";
                      }else{
                            //logger.debug("VodaCGRedirect::"+msisdn+"::'"+cgStatusCode+"' not matched with CGW202");
                            cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
                            cdr.setStatusDescp("CG Consent Failed");
                            respMsg="Sorry! Transaction Failed.";
                      }
                      updateCDR("AircelCGRedirect::"+msisdn+"::",cdr,false);
                      logger.debug("AircelCGRedirect::"+msisdn+":: CDR Status  :"+cdr.getStatus()+", Status Descp :"+cdr.getStatusDescp()+", CG Code "+cdr.getCgStatusCode());
                }else{
                      logger.debug("AircelCGRedirect::"+msisdn+":: CDR Not found");
                }
                cdr=null;
          }catch(Exception e){
                e.printStackTrace();
                
          }
          return respBuilder.buildCGRedirectResponse(respMsg);
    
    }

	
	
	@RequestMapping("jocg/callback/vodacgerror")
	public ModelAndView  processVodaCGError(HttpServletRequest request){
		
		String transId=request.getParameter("CG_TrxnId");
		String cgStatus=request.getParameter("CGStatus");
		String cgStatusCode=request.getParameter("CGStatusCode");
		String msisdn=request.getParameter("MSISDN");
		String cgStatusText=request.getParameter("CGStatusText");
		
		logger.debug("VodafoneIndiaCG-ERROR-Redirect::msisdn="+msisdn+"|CG_TrxnId="+transId+"|CGStatus="+cgStatus+"|CGStatusCode="+cgStatusCode+"|cgStatusText="+cgStatusText);
		try{
			ChargingCDR cdr=searchCDRByJocgTransId(transId);
			logger.debug("VodafoneIndiaCG-ERROR-Redirect::msisdn="+msisdn+"|"+cdr);
			if(cdr!=null && cdr.getId()!=null){
				cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
				cdr.setCgStatusCode(cgStatusCode);
				cdr.setCgStatusDescp(cgStatus+"|"+cgStatusText);
				cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cdr.setStatusDescp("CG Consent Error");
				updateCDR("VodaCGError::"+msisdn+"::",cdr,false);
				Subscriber sub=subscriberDao.findByJocgCDRId(cdr.getId());
				sub.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				sub.setStatusDescp(cgStatus+"|"+cgStatusText);
				subscriberDao.save(sub);
			}
			cdr=null;
		}catch(Exception e){
		logger.error("msisdn="+msisdn+"|Exception while processing "+e);
			
		}
		return respBuilder.buildErrorResponse(cgStatusCode);
	}
	
	@RequestMapping("jocg/callback/bsnlecg")
	public ModelAndView processBsnlENRedirect(HttpServletRequest request){
		logger.debug("BSNL_EN-Redirect::QueryString="+request.getQueryString());
		String respMsg="Transaction is accpted and under processing.";
		return respBuilder.buildCGRedirectResponse(respMsg);
	}
	
	@RequestMapping("jocg/callback/bsnlwcg")
	public ModelAndView processBsnlWSRedirect(HttpServletRequest request){
		logger.debug("BSNL_WS-Redirect::QueryString="+request.getQueryString());
		String respMsg="Transaction is accpted and under processing.";
		return respBuilder.buildCGRedirectResponse(respMsg);
	}
	
	@RequestMapping("jocg/callback/vodacgredirect")
	public ModelAndView processVodaCGRedirect(HttpServletRequest request){
		//CG_TrxnId=9178678886811504964434584&TRXID=&MSISDN=917867888681&CGStatus=Success&CGStatusCode=CGW202&CGStatusText=SMS/USSD
		String transId=request.getParameter("CG_TrxnId");
		String cgStatus=request.getParameter("CGStatus");
		String cgStatusCode=request.getParameter("CGStatusCode");
		String msisdn=request.getParameter("MSISDN");
		String cgStatusText=request.getParameter("CGStatusText");
		String cgTransId=request.getParameter("TRXID");
		StringBuilder sb=new StringBuilder();
		
		logger.debug("VodafoneIndiaCG-Redirect::msisdn="+msisdn+"|CG_TrxnId="+transId+"|CGStatus="+cgStatus+"|CGStatusCode="+cgStatusCode+"|cgStatusText="+cgStatusText+"|TRXID="+cgTransId+"|QueryString="+request.getQueryString());
		
		String respMsg="";
		try{
			if("CGW202".equalsIgnoreCase(cgStatusCode)){
				respMsg="Transaction is accpted and under processing.";
			}else{
				respMsg="Sorry! Transaction Failed.";
			}
			transId=JocgString.formatString(transId, "NA");
			
			Long msisdnVal=JocgString.strToLong(msisdn, 0);
			ChargingCDR cdr=null;
			if(!"NA".equalsIgnoreCase(transId))
				cdr=searchCDRByJocgTransId(transId);
		
			if((cdr==null || cdr.getId()==null) && msisdnVal.longValue()>0){
				logger.debug("VodafoneIndiaCG-Redirect::msisdn="+msisdn+" Failed to search by TransId, Searching by Msisdn "+msisdnVal);
				List<ChargingCDR> cdrList=searchLastCDRByMsisdn(msisdnVal);
				logger.debug("cdrList "+cdrList);
				for(ChargingCDR newCDR:cdrList){
					try{
						logger.debug("Comparing Date "+newCDR.getRequestDate());
						logger.debug("With Date "+cdr.getRequestDate());
						cdr=(cdr==null || cdr.getId()==null)?newCDR:newCDR.getRequestDate().after(cdr.getRequestDate())?newCDR:cdr;
					}catch(Exception e){}
				}
				
			}
			
			logger.debug("VodafoneIndiaCG-Redirect::msisdn="+msisdn+" CDR Found "+cdr);
			if(cdr!=null && cdr.getId()!=null){
				cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
				cdr.setCgStatusCode(cgStatusCode);
				cdr.setCgStatusDescp(cgStatus+"|"+request.getQueryString());
				cdr.setCgTransactionId(transId);
				logger.debug("VodaCGRedirect::"+msisdn+":: Status Codes :"+cgStatusCode);
				if("CGW202".equalsIgnoreCase(cgStatusCode)){
					cdr.setStatus(JocgStatusCodes.CHARGING_CGSUCCESS);
					cdr.setStatusDescp("CG Consent Received,Transaction is Process");
					respMsg="Transaction is accpted and under processing.";
				}else{
					//logger.debug("VodaCGRedirect::"+msisdn+"::'"+cgStatusCode+"' not matched with CGW202");
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cdr.setStatusDescp("CG Consent Failed");
					respMsg="Sorry! Transaction Failed.";
				}
				updateCDR("VodaCGRedirect::"+msisdn+"::",cdr,false);
				logger.debug("VodaCGRedirect::"+msisdn+":: CDR Status  :"+cdr.getStatus()+", Status Descp :"+cdr.getStatusDescp()+", CG Code "+cdr.getCgStatusCode());
			}else{
				logger.debug("VodaCGRedirect::"+msisdn+":: CDR Not found");
			}
			cdr=null;
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return respBuilder.buildCGRedirectResponse(respMsg);
		
	}
	
	@RequestMapping("OVOD/VendorUpdate.aspx")
	public @ResponseBody String processVodaRenewalCallbackLive(HttpServletRequest request){
		String respStr="RESPONSE_STATUS:900:"+System.currentTimeMillis()+":Callback successfully Accepted.";
		StringBuilder sb=new StringBuilder();
		try{
			logger.debug("VodaRenewalCallback Received "+request.getQueryString());
			Enumeration<String> params=request.getParameterNames();
			
			String fileName="",action="renewal";
			sb.append("VodaSDPCallback|");
			VodafoneIndiaSDPCallback vodaSdpCallback=new VodafoneIndiaSDPCallback();
			vodaSdpCallback.addNewField("action","REN");
			vodaSdpCallback.addNewField("STATUS","SUCCESS");
			while(params.hasMoreElements()){
				String param=params.nextElement();
				String value=request.getParameter(param);
				vodaSdpCallback.addNewField(param,value);
				sb.append(param).append("=").append(value).append("|");
			}
			
			fileName="voda_sdp_"+action+"_";
			logger.info("Voda Handling  Renewal Callback  "+sb.toString());
			SimpleDateFormat sdfDateV=new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat sdfTimeV=new SimpleDateFormat("HH:mm:ss");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date cdate=cal.getTime();
			
			vodaSdpCallback.setStartDate(sdfDateV.format(cdate));
			vodaSdpCallback.setStartTime(sdfTimeV.format(cdate));
			int validity="MONTHLY".equalsIgnoreCase(vodaSdpCallback.getMode())?30:
				("21DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY21".equalsIgnoreCase(vodaSdpCallback.getMode()))?21:
			     	("15DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY15".equalsIgnoreCase(vodaSdpCallback.getMode()))?15:
			     	("10DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY10".equalsIgnoreCase(vodaSdpCallback.getMode()))?10:
			     	("WEEKLY".equalsIgnoreCase(vodaSdpCallback.getMode()) || "7DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY7".equalsIgnoreCase(vodaSdpCallback.getMode()))?7:
			     	("5DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY5".equalsIgnoreCase(vodaSdpCallback.getMode()))?5:
			    	("3DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY3".equalsIgnoreCase(vodaSdpCallback.getMode()))?3:
			    	("1DAY".equalsIgnoreCase(vodaSdpCallback.getMode()) || "1DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY1".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY".equalsIgnoreCase(vodaSdpCallback.getMode()))?1:
			    	0;
			cal.add(Calendar.DATE, validity);
			cdate=cal.getTime();
			vodaSdpCallback.setEndDate(sdfDateV.format(cdate));
			vodaSdpCallback.setEndTime(sdfTimeV.format(cdate));
			if(vodaSdpCallback.getReferenceId()==null || vodaSdpCallback.getReferenceId().length()<=0) vodaSdpCallback.setReferenceId(vodaSdpCallback.getTxnId());
			vodaSdpCallback.setOriginator(vodaSdpCallback.getTxnId());
			jmsTemplate.convertAndSend("callback.vodasdp", vodaSdpCallback);
			
			File f=new File("/home/jocg/data/callback/vodain");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/vodain/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){
			logger.error("VodaRenewalCallback Exception "+e);
		}
		logger.info("VodaRenewal Message Sent to JMS Queue "+sb.toString());
		return respStr;
		
		
		
	}
	
	
	@RequestMapping("OVOD/VendorCallback.aspx")
	public @ResponseBody String processVodaCallbackLive(HttpServletRequest request){
		return processVodaCallback("TRUE",request);
	}
	
	@RequestMapping("jocg/callback/voda")
	public @ResponseBody String processVodaCallback(@RequestParam(value="ignorejms",required=false, defaultValue="FALSE") String ignoreJMS,HttpServletRequest request){
	/*public @ResponseBody String processVodaCallback(HttpServletRequest request,@RequestParam(value="MSISDN",required=false, defaultValue="NA") String msisdn,
			@RequestParam(value="SERVICE_ID",required=false, defaultValue="NA") String serviceId,@RequestParam(value="CLASS",required=false, defaultValue="NA") String chargingClass,
			@RequestParam(value="TXNID",required=false, defaultValue="NA") String txnId,@RequestParam(value="ACTION",required=false, defaultValue="NA") String chargingAction,
			@RequestParam(value="STATUS",required=false, defaultValue="NA") String status,@RequestParam(value="CHARGING_MODE",required=false, defaultValue="NA") String chargingMode, 
			@RequestParam(value="REQUEST_MODE",required=false, defaultValue="NA") String requestMode,@RequestParam(value="REQUEST_ID",required=false, defaultValue="NA") String requestId){*/
		String respStr="RESPONSE_STATUS:800:"+System.currentTimeMillis()+":Callback successfully Accepted.";
	
		try{
			logger.debug("VodaCallback Received "+request.getQueryString());
			//http://localhost:8101/jocg/callback/voda?CG_TrxnId=1234&TRXID=&MSISDN=919999631990&CGStatus=SUCCESS&CGStatusCode=CGW202&CGStatusText=Accepted
			//http://localhost:8101/jocg/callback/voda?User=VOD_ON&Pass=VOD_ON&msisdn=9167251230&srvkey=DIGI_JOBS120M&circle=0&Type=U&Refid=PRISM305681659&mode=SUSPEND&Info=SM&price=1&status=SUCCESS&action=DCT&precharge=Y&startDate=8/29/2017&endDate=8/29/2017&startTime=&endTime=&orginator=12657160766&user_field_1=PREPAID
			Enumeration<String> params=request.getParameterNames();
			StringBuilder sb=new StringBuilder();
			String fileName="",action="";
			if("NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("CG_TrxnId"), "NA")) && !"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("ACTION"), "NA"))){
				//SDP Callback
				sb.append("VodaSDPCallback|");
				VodafoneIndiaSDPCallback vodaSdpCallback=new VodafoneIndiaSDPCallback();
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					if("action".equalsIgnoreCase(param)){ 
						if(value.startsWith("REN")){
							action="renewal";
							respStr="RESPONSE_STATUS:900:"+System.currentTimeMillis()+":update request successfully received. ";
						}else if(value.startsWith("DCT")){
							respStr="RESPONSE_STATUS:802:"+System.currentTimeMillis()+":Service successfully deactivated.";
						}else
							action="activation";
						
					}
					vodaSdpCallback.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}
				
				fileName="voda_sdp_"+action+"_";
				logger.info("Voda Handling  Callback  "+sb.toString());
				logger.info("Voda ignoreJMS Flag  "+ignoreJMS);
				if("TRUE".equalsIgnoreCase(ignoreJMS)){
					SimpleDateFormat sdfDateV=new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat sdfTimeV=new SimpleDateFormat("HH:mm:ss");
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cdate=cal.getTime();
					
					vodaSdpCallback.setStartDate(sdfDateV.format(cdate));
					vodaSdpCallback.setStartTime(sdfTimeV.format(cdate));
					int validity="MONTHLY".equalsIgnoreCase(vodaSdpCallback.getMode())?30:
						("21DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY21".equalsIgnoreCase(vodaSdpCallback.getMode()))?21:
					     	("15DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY15".equalsIgnoreCase(vodaSdpCallback.getMode()))?15:
					     	("10DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY10".equalsIgnoreCase(vodaSdpCallback.getMode()))?10:
					     	("WEEKLY".equalsIgnoreCase(vodaSdpCallback.getMode()) || "7DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY7".equalsIgnoreCase(vodaSdpCallback.getMode()))?7:
					     	("5DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY5".equalsIgnoreCase(vodaSdpCallback.getMode()))?5:
					    	("3DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY3".equalsIgnoreCase(vodaSdpCallback.getMode()))?3:
					    	("1DAY".equalsIgnoreCase(vodaSdpCallback.getMode()) || "1DAYS".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY1".equalsIgnoreCase(vodaSdpCallback.getMode()) || "DAILY".equalsIgnoreCase(vodaSdpCallback.getMode()))?1:
					    	0;
					cal.add(Calendar.DATE, validity);
					cdate=cal.getTime();
					vodaSdpCallback.setEndDate(sdfDateV.format(cdate));
					vodaSdpCallback.setEndTime(sdfTimeV.format(cdate));
					
//					logger.info("Voda Invoking VodaCallback Listener  "+vodaJmsListener);
//					vodaJmsListener.processMessage(vodaSdpCallback);
					jmsTemplate.convertAndSend("callback.vodasdp", vodaSdpCallback);
					logger.info("VodaCallback Message Sent to JMS Queue "+sb.toString());
				}else{
					jmsTemplate.convertAndSend("callback.vodasdp", vodaSdpCallback);
					logger.info("VodaCallback Message Sent to JMS Queue "+sb.toString());
				}
			}else{
				//CG Callback
				sb.append("VodaCGCallback|");
				VodafoneIndiaCGResponse vodaCGResp=new VodafoneIndiaCGResponse();
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					vodaCGResp.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}	
				fileName="voda_cg";
				 jmsTemplate.convertAndSend("callback.vodacg", vodaCGResp);
				 logger.info("Message Sent to JMS Queue "+sb.toString());
				
			}
		
			
			
			File f=new File("/home/jocg/data/callback/vodain");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/vodain/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){
			logger.error("VodaCallback Exception "+e);
		}
		return respStr;
	}
	
	
	@RequestMapping("jocg/callback/paytmredirect")
	public ModelAndView handlePayTMRedirect(@RequestParam(value="MID" ,required=false,defaultValue="NA") String MID,
			@RequestParam(value="TXNID" ,required=false,defaultValue="NA") String TXNID,
			@RequestParam(value="ORDERID" ,required=false,defaultValue="NA") String ORDERID,
			@RequestParam(value="BANKTXNID" ,required=false,defaultValue="NA") String BANKTXNID,
			@RequestParam(value="TXNAMOUNT" ,required=false,defaultValue="NA") String TXNAMOUNT,
			@RequestParam(value="CURRENCY" ,required=false,defaultValue="NA") String CURRENCY,
			@RequestParam(value="STATUS" ,required=false,defaultValue="NA") String STATUS,
			@RequestParam(value="RESPCODE" ,required=false,defaultValue="NA") String RESPCODE,
			@RequestParam(value="RESPMSG" ,required=false,defaultValue="NA") String RESPMSG,
			@RequestParam(value="TXNDATE" ,required=false,defaultValue="NA") String TXNDATE,
			@RequestParam(value="GATEWAYNAME" ,required=false,defaultValue="NA") String GATEWAYNAME,
			@RequestParam(value="BANKNAME" ,required=false,defaultValue="NA") String BANKNAME,
			@RequestParam(value="PAYMENTMODE" ,required=false,defaultValue="NA") String PAYMENTMODE,
			@RequestParam(value="SUBS_ID" ,required=false,defaultValue="NA") String SUBS_ID,
			@RequestParam(value="PROMO_CAMP_ID" ,required=false,defaultValue="NA") String PROMO_CAMP_ID,
			@RequestParam(value="PROMO_STATUS" ,required=false,defaultValue="NA") String PROMO_STATUS,
			@RequestParam(value="PROMO_RESPCODE" ,required=false,defaultValue="NA") String PROMO_RESPCODE,
			@RequestParam(value="CHECKSUMHASH" ,required=false,defaultValue="NA") String CHECKSUMHASH,HttpServletRequest request){
		String statusMessage="";
		String logHeader="paytmredirect::"+ORDERID+"::";
		try{
			
			Enumeration e=request.getParameterNames();
			StringBuilder sb=new StringBuilder();
			while(e.hasMoreElements()){
				String key=(String)e.nextElement();
				sb.append(key).append("=").append(request.getParameter(key)).append("|");
			}
			logger.debug(logHeader+" Parameters Received "+sb.toString());
			
			ChargingCDR cdr=searchCDRByJocgTransId(ORDERID);
			Subscriber sub=null;
			Double txnAmountCharged=JocgString.strToDouble(TXNAMOUNT, 0d);
			if(cdr!=null && cdr.getId()!=null){
				logHeader +="{subid="+cdr.getSubscriberId()+",msisdn="+cdr.getMsisdn()+"} ::";
				logger.debug(logHeader+"CDR Found for JocgTransId "+ORDERID+", Processing Status Update for CDR Id "+cdr.getId());
				//Update CDR Transaction
				if("TXN_SUCCESS".equalsIgnoreCase(STATUS) && "01".equalsIgnoreCase(RESPCODE)){
					
					statusMessage="Charged Successfully!";
					cdr.setChargingCategory("WALLET");
					cdr.setChargedAmount(txnAmountCharged);
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setSdpStatusCode(RESPCODE);
					cdr.setSdpStatusDescp(sb.toString());
					cdr.setSdpTransactionId(TXNID);
					cdr.setSdpLifeCycleId(SUBS_ID);
					cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
					
					
					
				}else if("TXN_FAILURE".equalsIgnoreCase(STATUS)){
					statusMessage="Transaction Failed!";
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
					cdr.setSdpStatusCode(RESPCODE);
					cdr.setSdpStatusDescp(sb.toString());
					cdr.setSdpResponseTime(new java.util.Date(System.currentTimeMillis()));
				}else{
					statusMessage=STATUS;
					cdr.setStatus(JocgStatusCodes.CHARGING_CGSUCCESS);
					cdr.setCgStatusCode(RESPCODE);
					cdr.setCgStatusDescp(sb.toString());
					cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
				}
				
				cdr=updateCDR("PayTMCGRedirect::"+ORDERID+"::",cdr,false);
				logger.debug(logHeader+"CDR Updated Successfully for PaytmOrderId "+ORDERID+",  CDR Id "+cdr.getId());
				
				sub=this.searchSubscriberByTransId(cdr.getId());
				//Update Subscriber Record
				if(sub!=null && sub.getId()!=null){
					logger.debug(logHeader+"Subscriber Found for JocgTransId "+ORDERID+" Processing Status Update ..");
					if("TXN_SUCCESS".equalsIgnoreCase(STATUS) && "01".equalsIgnoreCase(RESPCODE)){
						sub.setChargedAmount(txnAmountCharged);
						sub.setSubSessionRevenue(sub.getSubSessionRevenue()+txnAmountCharged);
						sub.setChargingCategory("WALLET");
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
						sub.setStatusDescp("RESPCODE="+RESPCODE+"|STATUS="+STATUS);
						sub.setLifecycleId(SUBS_ID);
						//Send SMS
						JocgSMSMessage smsMessage=new JocgSMSMessage();
						smsMessage.setMsisdn(sub.getMsisdn());
						String smsInterface="OVOD".equalsIgnoreCase(cdr.getSourceNetworkCode())?"vodafonein":("ID_OF".equalsIgnoreCase(cdr.getSourceNetworkCode()) || "ID_TEST".equalsIgnoreCase(cdr.getSourceNetworkCode()) || "ID_ON".equalsIgnoreCase(cdr.getSourceNetworkCode()))?"ideain":"NA";
						if(!"NA".equalsIgnoreCase(smsInterface)){
							smsMessage.setSmsInterface(smsInterface);
							smsMessage.setServiceName(sub.getServiceName());
							smsMessage.setChintfId(sub.getSourceNetworkId());
							smsMessage.setChargingInterfaceType("WALLET");
							smsMessage.setTextMessage("Successful Activation");
							jmsTemplate.convertAndSend("notify.sms", smsMessage);
						}
						
						
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setStatusDescp("RESPCODE="+RESPCODE+"|STATUS="+STATUS);
						sub.setLifecycleId(SUBS_ID);
					}
					this.updateSubscriber(logHeader, sub, false);
					logger.debug(logHeader+"Subscriber Status Updated successfully for Id "+sub.getId());
				}
				
				//Invoke Notification Processor
				notificationHandler.processNotification(cdr);
				
			}else{
				logger.debug(logHeader+"No CDR Found for Orderid  "+ORDERID+", Creating File ....");
				File f=new File("/home/jocg/data/callback/paytm");
				if(!f.exists()) f.mkdirs();
				f=null;
				String fileName="paytm_unused_callback_";
				FileWriter fw=new FileWriter("/home/jocg/data/callback/paytm/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
				fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+sb.toString());
				fw.flush();
				fw.close();
				fw=null;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return respBuilder.buildCGRedirectResponse(statusMessage);
	}
	
	@RequestMapping("digiviveCallback/")
	public @ResponseBody String processIdeaSDPCallbackLive(HttpServletRequest request){
		return processIdeaSDPCallback(request);
	}
	
	@RequestMapping("jocg/callback/ideasdpcb")
	public @ResponseBody String processIdeaSDPCallback(HttpServletRequest request){
		IdeaSdpCallback isc=new IdeaSdpCallback();
		String cdrData="";
		try{
			isc.extractParams(request);
			cdrData=isc.toString();
			logger.debug("IdeaCallback::CDR::"+isc);
			jmsTemplate.convertAndSend("callback.ideasdp", isc);
		}catch(Exception e){
			logger.debug("IdeaCallback :: Exception "+e);
		}finally{
			try{
			File f=new File("/home/jocg/data/callback/paytm");
			if(!f.exists()) f.mkdirs();
			f=null;
			String fileName="ideasdp_callback_";
			FileWriter fw=new FileWriter("/home/jocg/data/callback/ideain/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+cdrData);
			fw.flush();
			fw.close();
			fw=null;
			}catch(Exception e1){}
		}
		isc=null;
		return "HTTP/1.0 200 OK";
		
	}
	
	//@RequestMapping("jocg/callback/bsnlsdpcb/BN_OF")	
	@RequestMapping("SdplService/HttpPostCollector/BN_OF")
	public @ResponseBody String processBsnlPyroCallback(HttpServletRequest request){
		String respStr="200|Accepted|transID";
		try{
			Enumeration<String> params=request.getParameterNames();
			StringBuilder sb=new StringBuilder();
			String fileName="",action="";
			if(!"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("tid"), "NA")) && !"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("action"), "NA"))){
				//SDP Callback
				sb.append("BsnlPyro|");
				PyroBSNLSDPCallback pyroSdpCallback=new PyroBSNLSDPCallback();
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					if("action".equalsIgnoreCase(param)){ 
						if(value.startsWith("subscription_renewal")){
							action="renewal";
							respStr="200|Accepted|transID";
						}else
							action="activation";
						respStr="200|Accepted|"+request.getParameter("tid")+"";
					}
					pyroSdpCallback.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}			
				fileName="BsnlPyro_sdp"+action+"_";
				jmsTemplate.convertAndSend("callback.bsnlpyro", pyroSdpCallback);
			}else{
				//CG Callback
				sb.append("BsnlPyroCG|");
				VodafoneIndiaCGResponse vodaCGResp=new VodafoneIndiaCGResponse();
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					vodaCGResp.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}	
				fileName="BsnlPyro_CG";
				 jmsTemplate.convertAndSend("callback.bsnlpyro", vodaCGResp);			
			}
		
			logger.info("Send JMS Message "+sb.toString());
			
			File f=new File("/home/jocg/data/callback/BsnlPyro");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/BsnlPyro/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){
			
		}
		return respStr;
	}	
	/**
	 * BSNL West Callback URL
	 * */
	
	@RequestMapping("SdplService/HttpPostCollector/BW_OF")
	public @ResponseBody String processBsnlWestNetExcelCallback(HttpServletRequest request){
		String respStr="200|Accepted|00000";
		logger.info("Send JMS Message ! "+request);
		try{
			Enumeration<String> params=request.getParameterNames();
			StringBuilder sb=new StringBuilder();
			String fileName="",action="";
			logger.info("Send JMS Message ! "+request.getParameter("transactionid"));
			if(!"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("transactionid"), "NA")) && !"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("reqtype"), "NA"))){
				//SDP Callback
				sb.append("BsnlNetExcel|");
				logger.info("Send JMS Message !! "+request.getParameter("transactionid"));
			NetExcelBSNLSDPCallback netExcelSdpCallback=new NetExcelBSNLSDPCallback();
			netExcelSdpCallback.setZoneName("BW_OF");
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					if("reqtype".equalsIgnoreCase(param)){ 
						if(value.startsWith("subscription_renewal")){
							action="REACT";
							respStr="200|Accepted|transID|"+request.getParameter("transactionid");
						}else
							action="ACT";
						respStr="200|Accepted|"+request.getParameter("transactionid");
					}
					netExcelSdpCallback.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}			
				fileName="BsnlNetExcel_sdp"+action+"_";
				jmsTemplate.convertAndSend("callback.bsnlNetExcel", netExcelSdpCallback);
			}else{
				//CG Callback
//				sb.append("BsnlPyroCG|");
//				VodafoneIndiaCGResponse vodaCGResp=new VodafoneIndiaCGResponse();
//				while(params.hasMoreElements()){
//					String param=params.nextElement();
//					String value=request.getParameter(param);
//					vodaCGResp.addNewField(param,value);
//					sb.append(param).append("=").append(value).append("|");
//				}	
//				fileName="BsnlPyro_CG";
//				 jmsTemplate.convertAndSend("callback.bsnlPyro", vodaCGResp);			
			}
		
			logger.info("Send JMS Message "+sb.toString());
			
			File f=new File("/home/jocg/data/callback/BsnlNetExcel");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/BsnlNetExcel/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){
			
		}
		return respStr;
	}
	//@RequestMapping("jocg/callback/bsnlsdpcb/BS_OF")	
	@RequestMapping("SdplService/HttpPostCollector/BS_OF")
	public @ResponseBody String processBsnlNetExcelCallback(HttpServletRequest request){
		String respStr="200|Accepted|00000";
		logger.info("Send JMS Message ! "+request);
		try{
			Enumeration<String> params=request.getParameterNames();
			StringBuilder sb=new StringBuilder();
			String fileName="",action="";
			logger.info("Send JMS Message ! "+request.getParameter("transactionid"));
			if(!"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("transactionid"), "NA")) && !"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("reqtype"), "NA"))){
				//SDP Callback
				sb.append("BsnlNetExcel|");
				logger.info("Send JMS Message !! "+request.getParameter("transactionid"));
			NetExcelBSNLSDPCallback netExcelSdpCallback=new NetExcelBSNLSDPCallback();
			netExcelSdpCallback.setZoneName("BS_OF");
				while(params.hasMoreElements()){
					String param=params.nextElement();
					String value=request.getParameter(param);
					if("reqtype".equalsIgnoreCase(param)){ 
						if(value.startsWith("subscription_renewal")){
							action="REACT";
							respStr="200|Accepted|transID|"+request.getParameter("transactionid");
						}else
							action="ACT";
						respStr="200|Accepted|"+request.getParameter("transactionid");
					}
					netExcelSdpCallback.addNewField(param,value);
					sb.append(param).append("=").append(value).append("|");
				}			
				fileName="BsnlNetExcel_sdp"+action+"_";
				jmsTemplate.convertAndSend("callback.bsnlNetExcel", netExcelSdpCallback);
			}else{
				//CG Callback
//				sb.append("BsnlPyroCG|");
//				VodafoneIndiaCGResponse vodaCGResp=new VodafoneIndiaCGResponse();
//				while(params.hasMoreElements()){
//					String param=params.nextElement();
//					String value=request.getParameter(param);
//					vodaCGResp.addNewField(param,value);
//					sb.append(param).append("=").append(value).append("|");
//				}	
//				fileName="BsnlPyro_CG";
//				 jmsTemplate.convertAndSend("callback.bsnlPyro", vodaCGResp);			
			}
		
			logger.info("Send JMS Message "+sb.toString());
			
			File f=new File("/home/jocg/data/callback/BsnlNetExcel");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/BsnlNetExcel/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){
			
		}
		return respStr;
	}

////////Apple IOS//////////////////
@RequestMapping("jocg/callback/iossdp")		
public @ResponseBody String processIOSCallback(HttpServletRequest request){
//msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
String resp="0";
CommonSDPCallback callbackData=null;
try{
	String param3=JocgString.formatString(request.getParameter("param3"),"NA");
	callbackData=new CommonSDPCallback(JocgString.strToLong(request.getParameter("msisdn"),0L),
			
			JocgString.formatString(request.getParameter("Operator_Id"),"NA"),
			JocgString.strToDouble(request.getParameter("Price"),0D),
			JocgString.strToInt(request.getParameter("Validity"),0),
			JocgString.formatString(request.getParameter("reqtype"),"NA"),
			JocgString.formatString(request.getParameter("transactionid"),"NA"),
			JocgString.formatString(request.getParameter("pack"),"NA"),
			JocgString.formatString(request.getParameter("param1"),"NA"),
			JocgString.formatString(request.getParameter("param2"),"NA"),
			param3,
			JocgString.formatString(request.getParameter("param4"),"NA"),
			param3,
			new java.util.Date(System.currentTimeMillis()));
	callbackData.setSourceOperatorId(callbackData.getOperatorCode());
	callbackData.setOperatorCode("IOS");
	commonSDPreceiver.receivIosMessage(callbackData);
	//jmsTemplate.convertAndSend("callback.ios", callbackData);
}catch(Exception e){
	logger.error("IOS Callback "+e.getMessage());
}finally{
	try{
		File f=new File("/home/jocg/data/callback/common");
		if(!f.exists()) f.mkdirs();
		f=null;
		String fileName="ios_";
		FileWriter fw=new FileWriter("/home/jocg/data/callback/common/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
		fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+request.getQueryString()+"|"+callbackData);
		fw.flush();
		fw.close();
		fw=null;
	}catch(Exception e){}
}
return resp;
}
	
	
////////PAYPAL//////////////////
@RequestMapping("jocg/callback/paypalsdp")		
public @ResponseBody String processPaypalCallback(HttpServletRequest request){
	//msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
	String resp="0";
	CommonSDPCallback callbackData=null;
	try{
		String param3=JocgString.formatString(request.getParameter("param3"),"NA");
		callbackData=new CommonSDPCallback(JocgString.strToLong(request.getParameter("msisdn"),0L),
				
				JocgString.formatString(request.getParameter("Operator_Id"),"NA"),
				JocgString.strToDouble(request.getParameter("Price"),0D),
				JocgString.strToInt(request.getParameter("Validity"),0),
				JocgString.formatString(request.getParameter("reqtype"),"NA"),
				JocgString.formatString(request.getParameter("transactionid"),"NA"),
				JocgString.formatString(request.getParameter("pack"),"NA"),
				JocgString.formatString(request.getParameter("param1"),"NA"),
				JocgString.formatString(request.getParameter("param2"),"NA"),
				param3,
				JocgString.formatString(request.getParameter("param4"),"NA"),
				param3,
				new java.util.Date(System.currentTimeMillis()));
		callbackData.setSourceOperatorId(callbackData.getOperatorCode());
		callbackData.setOperatorCode("PAYPAL");
		commonSDPreceiver.receivPaypaleMessage(callbackData);
		//jmsTemplate.convertAndSend("callback.paypal", callbackData);
	}catch(Exception e){
		logger.debug("PAYPAL Callback:"+e.getMessage());
	}finally{
		try{
			File f=new File("/home/jocg/data/callback/common");
			if(!f.exists()) f.mkdirs();
			f=null;
			String fileName="paypal_";
			FileWriter fw=new FileWriter("/home/jocg/data/callback/common/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+request.getQueryString()+"|"+callbackData);
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){}
	}
	return resp;
}

	
	
	////////Google//////////////////
	@RequestMapping("jocg/callback/googlesdp")		
	public @ResponseBody String processGoogleCallback(HttpServletRequest request){
		//msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
		String resp="0";
		CommonSDPCallback callbackData=null;
		try{
			String param3=JocgString.formatString(request.getParameter("param3"),"NA");
			callbackData=new CommonSDPCallback(JocgString.strToLong(request.getParameter("msisdn"),0L),
					
					JocgString.formatString(request.getParameter("Operator_Id"),"NA"),
					JocgString.strToDouble(request.getParameter("Price"),0D),
					JocgString.strToInt(request.getParameter("Validity"),0),
					JocgString.formatString(request.getParameter("reqtype"),"NA"),
					JocgString.formatString(request.getParameter("transactionid"),"NA"),
					JocgString.formatString(request.getParameter("pack"),"NA"),
					JocgString.formatString(request.getParameter("param1"),"NA"),
					JocgString.formatString(request.getParameter("param2"),"NA"),
					param3,
					JocgString.formatString(request.getParameter("param4"),"NA"),
					param3,
					new java.util.Date(System.currentTimeMillis()));
				callbackData.setSourceOperatorId(callbackData.getOperatorCode());
				callbackData.setOperatorCode("GOOGLE");
			commonSDPreceiver.receivGoogleMessage(callbackData);
			//jmsTemplate.convertAndSend("callback.google", callbackData);
		}catch(Exception e){
			logger.error("GOOGLE Callback "+e.getMessage());
		}finally{
			try{
				File f=new File("/home/jocg/data/callback/common");
				if(!f.exists()) f.mkdirs();
				f=null;
				String fileName="google_";
				FileWriter fw=new FileWriter("/home/jocg/data/callback/common/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
				fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+request.getQueryString()+"|"+callbackData);
				fw.flush();
				fw.close();
				fw=null;
			}catch(Exception e){}
		}
		return resp;
	}
	
	
	
///////////////////////Connect///////////////////
//http://119.252.215.58:8098/SdplService/HttpPostCollector/VA_BN?msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
	@RequestMapping("SdplService/HttpPostCollector/VA_BN")		
	public @ResponseBody String processConnectMigratedCallback(HttpServletRequest request){
		//msisdn=9877012647&Operator_Id=CM_OF&Price=49&Validity=30&reqtype=ACT&transactionid=9877012647&pack=PK573&param1=CONNECT&param2=&param3=<subscriberId>&param4=
		String resp="0";
		CommonSDPCallback callbackData=null;
		try{
			String param3=JocgString.formatString(request.getParameter("param3"),"NA");
			callbackData=new CommonSDPCallback(JocgString.strToLong(request.getParameter("msisdn"),0L),
					JocgString.formatString(request.getParameter("Operator_Id"),"NA"),
					JocgString.strToDouble(request.getParameter("Price"),0D),
					JocgString.strToInt(request.getParameter("Validity"),0),
					JocgString.formatString(request.getParameter("reqtype"),"NA"),
					JocgString.formatString(request.getParameter("transactionid"),"NA"),
					JocgString.formatString(request.getParameter("pack"),"NA"),
					JocgString.formatString(request.getParameter("param1"),"NA"),
					JocgString.formatString(request.getParameter("param2"),"NA"),
					param3,
					JocgString.formatString(request.getParameter("param4"),"NA"),
					param3,
					new java.util.Date(System.currentTimeMillis()));
			callbackData.setSourceOperatorId(callbackData.getOperatorCode());
			commonSDPreceiver.receiveMessage(callbackData);
			//jmsTemplate.convertAndSend("callback.connectmigrated", callbackData);
		}catch(Exception e){
			logger.error("ConnectMigrated Callback "+e.getMessage());
		}finally{
			try{
				File f=new File("/home/jocg/data/callback/Connect");
				if(!f.exists()) f.mkdirs();
				f=null;
				String fileName="ConnectMigrated_";
				FileWriter fw=new FileWriter("/home/jocg/data/callback/Connect/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
				fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+request.getQueryString()+"|"+callbackData);
				fw.flush();
				fw.close();
				fw=null;
			}catch(Exception e){}
		}
		return resp;
	}
	
	
/*
* http://ip:80/jocg/callback/connectsdpcb?msisdn=<msisdn>&accid=<accountid>
* &action=<ACT/DCT/REN>&servicekey=<servicekey>
* &amtcharged=<amountcharged>&status=<>&statusdesc=<statusdesc>
* &chargingdate=<yyyy-MM-dd>
*/
@RequestMapping("jocg/callback/connectsdpcb")    
public @ResponseBody String processConnectCallback(HttpServletRequest request){
String respStr="200|Accepted|accId|";
try{
Enumeration<String> params=request.getParameterNames();
StringBuilder sb=new StringBuilder();
String fileName="",action="";
if(!"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("accid"), "NA")) && !"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("action"), "NA"))){
//SDP Callback
sb.append("Connect|");
ConnectSDPCallback connectSdpCallback=new ConnectSDPCallback();
while(params.hasMoreElements()){
String param=params.nextElement();
String value=request.getParameter(param);
if("action".equalsIgnoreCase(param)){ 
if(value.equalsIgnoreCase("REN"))
      action="renewal";
else if(value.equalsIgnoreCase("ACT"))
      action="activation";
else 
      action="deactivation";

respStr="200|Accepted|"+request.getParameter("accid")+"|";
}
connectSdpCallback.addNewField(param,value);
sb.append(param).append("=").append(value).append("|");
}                 
fileName="Connect_sdp"+action+"_";
jmsTemplate.convertAndSend("callback.connect", connectSdpCallback);
}
//else{
////CG Callback
//sb.append("BsnlPyroCG|");
//VodafoneIndiaCGResponse vodaCGResp=new VodafoneIndiaCGResponse();
//while(params.hasMoreElements()){
//String param=params.nextElement();
//String value=request.getParameter(param);
//vodaCGResp.addNewField(param,value);
//sb.append(param).append("=").append(value).append("|");
//}     
//fileName="BsnlPyro_CG";
//jmsTemplate.convertAndSend("callback.bsnlpyro", vodaCGResp);                  
//}

logger.info("Send JMS Message "+sb.toString());

File f=new File("/home/jocg/data/callback/Connect");
if(!f.exists()) f.mkdirs();
f=null;

FileWriter fw=new FileWriter("/home/jocg/data/callback/Connect/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
fw.write("\n"+sb.toString());
fw.flush();
fw.close();
fw=null;
}catch(Exception e){

}
return respStr;
}    

//AR_ON?ProductName=DIGTVD&ServiceName=DIGTVD&SID=7503766435&NotifyType=SUB-1&AmountCharged=5.0&SubscriptionValidity=1days&ChannelType=WAP&Consent_id=CG21102171011165406166617&ExtTxId=null
@RequestMapping("SdplService/HttpPostCollector/AR_ON")
public @ResponseBody String AircelSDPCallback(HttpServletRequest request){
	String respStr="0";
	StringBuilder sb=new StringBuilder();
	String fileName="default_",action="NA";
	try{
		Enumeration<String> params=request.getParameterNames();
		if(!"NA".equalsIgnoreCase(JocgString.formatString(request.getParameter("NotifyType"), "NA"))){
			//SDP Callback
			sb.append("AircelSDP|");
			AircelIndiaSDPCallback arclSdpCallback=new AircelIndiaSDPCallback();
			while(params.hasMoreElements()){
				String param=params.nextElement();
				String value=request.getParameter(param);
				if("NotifyType".equalsIgnoreCase(param)){ 
					if(value.startsWith("REN")){
						action="renewal";
						respStr="0";
					}else
						action="activation";
					respStr="0";
				}
				arclSdpCallback.parseSDPResponse(request);
				sb.append(param).append("=").append(value).append("|");
			}			
			fileName="AircelSDP"+action+"_";
			jmsTemplate.convertAndSend("callback.aircelsdp", arclSdpCallback);
		}else{
			//CG Callback
			sb.append("AircelCG|");
			AircelIndiaCGCallback aircelCGResp=new AircelIndiaCGCallback();
			while(params.hasMoreElements()){
				String param=params.nextElement();
				String value=request.getParameter(param);
				aircelCGResp.parseCGResponse(request);					
			}	
			fileName="AIRCEL_CG";
			 jmsTemplate.convertAndSend("callback.AIRCELCG", aircelCGResp);			
		}
	
		logger.info("Callback sent to JMS Queue"+sb.toString());
		
		
	}catch(Exception e){
		sb.append("|Exception "+e);
	}finally{
		try{
			File f=new File("/home/jocg/data/callback/Aircel");
			if(!f.exists()) f.mkdirs();
			f=null;
			
			FileWriter fw=new FileWriter("/home/jocg/data/callback/Aircel/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+request.getQueryString()+"|ParsedParams="+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
		}catch(Exception e){}
	}
	return respStr;
}	

@RequestMapping("jocg/callback/junosdpcb")
public @ResponseBody String processJunoSDPCallback(@RequestParam(value="cptxnid" ,required=false,defaultValue="NA") String cptxnid,
		@RequestParam(value="pid" ,required=false,defaultValue="NA") String pid,
		@RequestParam(value="cgtxnid" ,required=false,defaultValue="NA") String cgtxnid,
		@RequestParam(value="optxnid" ,required=false,defaultValue="NA") String optxnid,
		@RequestParam(value="m" ,required=false,defaultValue="NA") String m,
		@RequestParam(value="pr" ,required=false,defaultValue="NA") String pr,
		@RequestParam(value="status" ,required=false,defaultValue="NA") String status,
		@RequestParam(value="circle_name" ,required=false,defaultValue="NA") String circle_name,
		@RequestParam(value="action" ,required=false,defaultValue="NA") String action,
		@RequestParam(value="charging_mode" ,required=false,defaultValue="NA") String charging_mode,
		@RequestParam(value="operator_id" ,required=false,defaultValue="NA") String operator_id,
		@RequestParam(value="nettype" ,required=false,defaultValue="NA") String nettype,
		@RequestParam(value="jtxnid" ,required=false,defaultValue="NA") String jtxnid,
		@RequestParam(value="jaction" ,required=false,defaultValue="NA") String jaction,
		@RequestParam(value="lcid" ,required=false,defaultValue="0") String lcid,
		@RequestParam(value="receiveTime" ,required=false,defaultValue="NA") String receiveTime,
		HttpServletRequest request){
	String respStr="0";
	String queryString=request.getQueryString();
	try{
		logger.debug("JunoCallback :: Received "+request.getQueryString());
		
		//JunoSDPCallback sdpCallback=new JunoSDPCallback(cptxnid,pid,cgtxnid,optxnid,JocgString.convertToHex(m.getBytes()),JocgString.strToDouble(pr,0),status,circle_name,action,charging_mode,operator_id,
			//	nettype,jtxnid,jaction,new java.util.Date(System.currentTimeMillis()));
		SimpleDateFormat sdfjuno=new SimpleDateFormat("yyyyMMddHHmmss");//2017 11 17 00 14 03
		java.util.Date chargingDate=null;
		try{
			chargingDate=sdfjuno.parse(receiveTime);
		}catch(Exception e){
			chargingDate=new java.util.Date(System.currentTimeMillis());
		}
		JunoSDPCallback sdpCallback=new JunoSDPCallback(cptxnid,pid,cgtxnid,optxnid,m,JocgString.strToDouble(pr,0),status,circle_name,action,charging_mode,operator_id,
				nettype,jtxnid,jaction,chargingDate);
		
		sdpCallback.setMsisdn("dialog_lkr".equalsIgnoreCase(sdpCallback.getOperatorCode())?lcid:m);
		
		sdpCallback.setOperatorCode("dialog_lkr".equalsIgnoreCase(sdpCallback.getOperatorCode())?"DI_SL":
			"du".equalsIgnoreCase(sdpCallback.getOperatorCode())?"DU_ME":
				"ooredoo_qat".equalsIgnoreCase(sdpCallback.getOperatorCode())?"OR_QT":sdpCallback.getOperatorCode());
		
		sdpCallback.setSubscriberId(m);		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		logger.debug("JunoCallback :: Parsed Data "+sdpCallback);
		System.out.println("writing to Juno queue "+sdpCallback);
		
		junoReceiver.receiveMessage(sdpCallback);
		//jmsTemplate.convertAndSend("callback.junosdp", sdpCallback);	
				
		File f=new File("/home/jocg/data/callback/Juno");
		if(!f.exists()) f.mkdirs();
		f=null;
		String fileName="Juno_"+operator_id+"_"+action+"_";
		FileWriter fw=new FileWriter("/home/jocg/data/callback/Juno/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
		fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+action+"|"+queryString+"&receiveTime="+sdf.format(new java.util.Date(System.currentTimeMillis())));
		fw.flush();
		fw.close();
		fw=null;
	}catch(Exception e){
		
		logger.error("JunoCallbackReceiver :: "+e.getMessage());
		e.printStackTrace();
	}
	return respStr;
}

@RequestMapping("SdplService/HttpPostCollector/RL_OF")
//?action=1&appid=198&channelid=20&mdn=1234509877&pname=RCOM-RW&startdate=11 02 2015 1500&expirydate=120220151500&status=0&validdays=1 
public @ResponseBody String processRelianceSDPCallback(HttpServletRequest request){
	String respStr="0";
	String queryString=request.getQueryString();
	try{
		int validity=JocgString.strToInt(request.getParameter("validdays"), 1);
		SimpleDateFormat sdfRel=new SimpleDateFormat("ddMMyyyyHHmm");
		java.util.Date startDate=null,endDate=null;
		try{
			startDate=sdfRel.parse(request.getParameter("startdate"));
		}catch(Exception e){
			startDate=new java.util.Date(System.currentTimeMillis());
		}
		try{
			endDate=sdfRel.parse(request.getParameter("expirydate"));
		}catch(Exception e){
			Calendar cal=Calendar.getInstance();
			cal.setTime(startDate);
			cal.add(Calendar.DATE, validity);
			endDate=cal.getTime();
		}
		RelianceSDPCallback callbackData=new RelianceSDPCallback(JocgString.strToInt(request.getParameter("action"), 0), 
				JocgString.strToInt(request.getParameter("appid"), 0), 
				JocgString.strToInt(request.getParameter("channelid"), 0),
				JocgString.formatString(request.getParameter("mdn"), "0"), 
				JocgString.formatString(request.getParameter("pname"), "NA"), 
				startDate, 
				endDate, 
				JocgString.strToInt(request.getParameter("status"), 0), 
				validity);
		
		jmsTemplate.convertAndSend("callback.reliancesdp", callbackData);	
		
		File f=new File("/home/jocg/data/callback/Rcom");
		if(!f.exists()) f.mkdirs();
		f=null;
		String fileName="Rcom_SDP_";
		FileWriter fw=new FileWriter("/home/jocg/data/callback/Rcom/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
		fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+queryString+"|"+callbackData);
		fw.flush();
		fw.close();
		fw=null;
	}catch(Exception e){
		
	}
	return respStr;
}

@RequestMapping("jocg/callback/walletsdpcb")
public @ResponseBody String processWalletCallbackChargedAtDigiviveSystem(HttpServletRequest request){
	String respStr="0";
	String queryString=request.getQueryString();
	try{
		//transid=<transactionId>&msisdn=<msisdn>&subscriberid=<subscriberid>&servicename=<servicename>&systemid=<systemid>&opcode=<opcode>&packname=<packname>
		//&packvalidity=<packvalidity>&discounted=<true/fale>&baseprice=<baseprice>&discount_amt=<discount_amt>&transtime=<yyyyMMddHHmmss>
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		java.util.Date receiveDate=null;
		try{
			receiveDate=sdf.parse(request.getParameter("transtime"));
		}catch(Exception e){
			receiveDate=new java.util.Date(System.currentTimeMillis());
		}
		WalletCommonCallback callbackData=new WalletCommonCallback(JocgString.formatString(request.getParameter("transid"),"NA"),
				JocgString.formatString(request.getParameter("msisdn"),"0"),
				JocgString.formatString(request.getParameter("subscriberid"),"NA"),
				JocgString.formatString(request.getParameter("servicename"),"NA"),
				JocgString.formatString(request.getParameter("systemid"),"NA"),
				JocgString.formatString(request.getParameter("opcode"),"NA"),
				JocgString.formatString(request.getParameter("packname"),"NA"),
				JocgString.strToInt(request.getParameter("packvalidity"),0),
				JocgString.strToBool(request.getParameter("discounted"),true),
				JocgString.strToDouble(request.getParameter("baseprice"),0D),
				JocgString.strToDouble(request.getParameter("discount_amt"),0D),
				receiveDate);
		callbackData.setSourceOperatorCode("CM_OF".equalsIgnoreCase(callbackData.getSourceOperatorCode())?"CMOF":callbackData.getSourceOperatorCode());
		callbackData.setLifecycleId(JocgString.formatString(request.getParameter("lcid"),"NA"));
		
		this.jmsTemplate.convertAndSend("callback.walletsdp", callbackData);
		
		File f=new File("/home/jocg/data/callback/Wallet");
		if(!f.exists()) f.mkdirs();
		f=null;
		String fileName="Wallet_SDP_";
		FileWriter fw=new FileWriter("/home/jocg/data/callback/Wallet/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
		fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+queryString+"|"+callbackData);
		fw.flush();
		fw.close();
		fw=null;
	}catch(Exception e){
		
	}
	return respStr;
}

@RequestMapping("jocg/callback/airtelsdpcb")
public @ResponseBody String processAirtelSDP(HttpServletRequest requestContext){
	String logHeader="CALLBACK::processAirtelSDP() :: ";
	 StringBuilder sb = new StringBuilder();
	    try {
	        java.io.BufferedReader bis = new java.io.BufferedReader(new java.io.InputStreamReader(requestContext.getInputStream()));
	        String line = "";
	        while ((line = bis.readLine()) != null) {
	            sb.append(line);
	        }
	        bis.close();
	        ActivationCallback activationCallback = null;
	        try {

	            AirtelApiResponseWraper airtelApiResponseWraper = new AirtelApiResponseWraper();
	            activationCallback = airtelApiResponseWraper.parseChargingActivationCallBack(sb.toString());
	            logger.debug(logHeader+" Parsed SDP Date "+ activationCallback);
	            
	            String msisdn=activationCallback.getMsisdn();
	            if(msisdn.length()<=10) msisdn="91"+msisdn;
	            activationCallback.setMsisdn(msisdn);
	            activationCallback.setReferer("SDP-CALLBACK");
	            //callback.airtelsdp
	            jmsTemplate.convertAndSend("callback.airtelsdp", activationCallback);
	            
	          //  jmsTemplate.convertAndSend("callback.tbmssdp", sb.toString());
	           
	        } catch (Exception e) {
	           
	           // sb.append(logHeader + ":: Exception : " + e);
	        } finally {
	           // createDBLog(logHeader, wapReq, activationCallback);
	        }
	        
	        File f=new File("/home/jocg/data/callback/airtelin");
			if(!f.exists()) f.mkdirs();
			f=null;
			String fileName="AIRTEL_CB";
			FileWriter fw=new FileWriter("/home/jocg/data/callback/airtelin/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\nTime "+new java.util.Date()+"|"+sb.toString());
			fw.flush();
			fw.close();
			fw=null;
	        
	    } catch (Exception e) {
	        sb.append("Airtel Exception : " + e);
	    }
	    
	  
	    
	    
	    String res = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
	            + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
	            + "<soapenv:Body>"
	            + "<notificationToCPResponse xmlns=\"http://SubscriptionEngine.ibm.com\"/> "
	            + "</soapenv:Body>"
	            + "</soapenv:Envelope>";

	
	return res;
}



}
