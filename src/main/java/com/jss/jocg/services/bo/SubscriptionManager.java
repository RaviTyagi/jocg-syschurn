package com.jss.jocg.services.bo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.jss.jocg.util.ServiceException;

public class SubscriptionManager {
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriptionManager.class);
	
	//@Autowired MongoTemplate mongoTemplate;
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	
	String logHead="";
	public int statusCode;
	public String statucDescp;

	public SubscriptionManager(){
		logHead="JOCGV1::SUBMGR::";
		
	}
	
	public Subscriber searchSubscriber(Long msisdn,Integer serviceId){
		Subscriber sub=null;
		try{
			sub=searchSubscriber(""+msisdn,msisdn,serviceId,"NA");//subscriberDao.findByMsisdnAndServiceId(msisdn, serviceId);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(sub==null) sub=new Subscriber();
		}
		return sub;
	}
	
	public Subscriber searchSubscriber(String subscriberId,Long msisdn,Integer serviceId,String serviceName){
		String logHead="searchSubscriber(subid="+subscriberId+",msisdn="+msisdn+",serviceId="+serviceId+",serviceName="+serviceName+") ::";
		Subscriber sub=null;
		try{
			
			sub=subscriberDao.searchActiveSubscriber(subscriberId,msisdn,serviceId,serviceName);
			
			/*List<Subscriber> subList=null;
			if(msisdn.longValue()>0 && serviceId.intValue()>0){
				//subList=subscriberDao.findByMsisdnAndServiceId(msisdn.longValue(), serviceId.intValue());
				subList=subscriberDao.findByMsisdnAndServiceName(msisdn.longValue(), serviceName);//(msisdn.longValue(), serviceId.intValue());
				if(subList!=null && subList.size()>0){
					//Check if exists some subscribed pack
					for(Subscriber s:subList){
						if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE || s.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE ){
							sub=s;
							break;
						}else sub=null;
					}
					
					if(sub==null){
						for(Subscriber s:subList){
							if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING || s.getStatus()==JocgStatusCodes.SUB_SUCCESS_GRACE){
								sub=s;
								break;
							}else sub=null;
						}
					}
					if(sub==null){
						//Check if exists some pending pack
						for(Subscriber s:subList){
							if(s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_CG || s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_PENDING){
								sub=s;
								break;
							}else sub=null;
						}
					}
					
					if(sub==null && subList!=null && subList.size()>0){
						//Check if exists pack in any status
						sub=subList.get(0);
					}
				}
				logger.debug(logHead +" Search By Msisdn  Result "+sub);
			}
			subList=null;
			
			if(sub==null && subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId) && serviceName!=null && !"NA".equalsIgnoreCase(serviceName)){
				subList=subscriberDao.findBySubscriberIdAndServiceName(subscriberId, serviceName);
				//Check if exists some subscribed pack
				for(Subscriber s:subList){
					if(s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_ACTIVE || s.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE || s.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING  || s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_GRACE){
						sub=s;
						break;
					}else sub=null;
				}
				
				if(sub==null){
					//Check if exists some pending pack
					for(Subscriber s:subList){
						if(s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_CG || s.getStatus()!=JocgStatusCodes.SUB_SUCCESS_PENDING){
							sub=s;
							break;
						}else sub=null;
					}
				}
				if(sub==null && subList!=null && subList.size()>0){
					//Check if exists pack in any status
					sub=subList.get(0);
				}
				logger.debug(logHead +" Search By SubscriberId Result "+sub);
			}
			subList=null;*/
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(sub==null) sub=new Subscriber();
		}
		return sub;
	}
	
	public Subscriber getOperatorChargingSubscriber(String subscriberId,Long msisdn,Circleinfo circle,VoucherDetail voucher,PackageDetail packDetails,
			ChargingInterface sourceChintf,ChargingInterface chargingOperator,ChargingPricepoint chpp,
			PackageChintfmap pkgchintfMap) throws ServiceException{
		String logHeader=logHead+"getOperatorChargingSubscriber()::{subscriberId="+subscriberId+",msisdn="+msisdn+"}::";
		Subscriber sub=new Subscriber();
		try{
			//Add Subscriber identity
			sub.setSubscriberId(subscriberId);
			sub.setMsisdn(msisdn);
			sub.setTransactionCategory("SUB");
			
			//Add Circle Details
			sub.setCircleId(circle.getCircleid());
			sub.setCircleName(circle.getCirclename());
			sub.setCircleCode(circle.getCirclecode());
			
			//Add Package Details
			sub.setPackageId(packDetails.getPkgid());
			sub.setPackageName(packDetails.getPkgname());
			sub.setPackagePrice(packDetails.getPricepoint());
			Service serviceObj=jocgCache.getServicesById(packDetails.getServiceid());
				sub.setServiceId(serviceObj.getServiceid());
				sub.setServiceName(serviceObj.getServiceName());
				sub.setServiceCategory(serviceObj.getServiceCatg());
	
			//Add Source Network Details
			sub.setSourceNetworkId(sourceChintf.getChintfid());
			sub.setSourceNetworkName(sourceChintf.getName());
			sub.setSourceNetworkCode(sourceChintf.getOpcode());
	
			//Add ChargingOperator Details
			sub.setParentChintfId(chargingOperator.getPchintfid());
			sub.setUseParentChargingHandler(chargingOperator.getUseparenthandler());
			sub.setChargingInterfaceId(chargingOperator.getChintfid());
			sub.setOperatorName(chargingOperator.getName());
			sub.setCountryName(chargingOperator.getCountry());
			sub.setCurrency("INR");
			sub.setCountryName(chargingOperator.getCountry());
			sub.setGmtDifference(chargingOperator.getGmtDiff());
			sub.setChargingInterfaceType(chargingOperator.getChintfType());
			
			//Add Charging Pricepoint details
			sub.setPricepointId(chpp.getPpid());
			sub.setOperatorPricePoint(chpp.getPricePoint());
			sub.setValidityCategory("LIMITED");
			sub.setValidityUnit(chpp.getValidityType());
			sub.setValidity(chpp.getValidity());
			sub.setOperatorServiceKey(chpp.getOperatorKey());
			sub.setOperatorParams(chpp.getOpParams());
		
			//Add Package Charging Operator Map details
			sub.setPackageChargingInterfaceMapId(pkgchintfMap.getPkgchintfid());
			//set Status=activated
			sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PENDING);
			java.util.Date reqTime=new java.util.Date(System.currentTimeMillis());
			sub.setSubRequestDate(reqTime);
			sub.setCgStatusCode("NA");
			sub.setCgResponseTime(reqTime);
			sub.setCgTransactionId("NA");
			sub.setSubChargedDate(reqTime);
			sub.setChargingCategory("OPERATOR");
			
			
		}catch(Exception e){
			logger.error(logHeader+" Exception while creation subscriber for Voucher Activation "+e);
			e.printStackTrace();
		}
		return sub;
	}
	
	
	public Subscriber getVoucherActivatedSubscriber(String subscriberId,Long msisdn,Circleinfo circle,VoucherDetail voucher,PackageDetail packDetails,
			ChargingInterface sourceChintf,ChargingInterface chargingOperator,ChargingPricepoint chpp,
			PackageChintfmap pkgchintfMap) throws ServiceException{
		String logHeader=logHead+"getVoucherActivatedSubscriber()::{subscriberId="+subscriberId+",msisdn="+msisdn+"}::";
		Subscriber sub=new Subscriber();
		try{
			//Add Subscriber identity
			sub.setSubscriberId(subscriberId);
			sub.setMsisdn(msisdn);
			sub.setTransactionCategory("SUB");
			
			//Add Circle Details
			sub.setCircleId(circle.getCircleid());
			sub.setCircleName(circle.getCirclename());
			sub.setCircleCode(circle.getCirclecode());
			
			//Add Package Details
			sub.setPackageId(packDetails.getPkgid());
			sub.setPackageName(packDetails.getPkgname());
			sub.setPackagePrice(packDetails.getPricepoint());
			Service serviceObj=jocgCache.getServicesById(packDetails.getServiceid());
				sub.setServiceId(serviceObj.getServiceid());
				sub.setServiceName(serviceObj.getServiceName());
				sub.setServiceCategory(serviceObj.getServiceCatg());
	
			//Add Source Network Details
			sub.setSourceNetworkId(sourceChintf.getChintfid());
			sub.setSourceNetworkName(sourceChintf.getName());
			sub.setSourceNetworkCode(sourceChintf.getOpcode());
	
			//Add ChargingOperator Details
			sub.setParentChintfId(chargingOperator.getPchintfid());
			sub.setUseParentChargingHandler(chargingOperator.getUseparenthandler());
			sub.setChargingInterfaceId(chargingOperator.getChintfid());
			sub.setOperatorName(chargingOperator.getName());
			sub.setCountryName(chargingOperator.getCountry());
			sub.setCurrency("INR");
			sub.setCountryName(chargingOperator.getCountry());
			sub.setGmtDifference(chargingOperator.getGmtDiff());
			sub.setChargingInterfaceType(chargingOperator.getChintfType());
			
			//Add Charging Pricepoint details
			sub.setPricepointId(chpp.getPpid());
			sub.setOperatorPricePoint(chpp.getPricePoint());
			sub.setValidityCategory("LIMITED");
			sub.setValidityUnit(chpp.getValidityType());
			sub.setValidity(chpp.getValidity());
		
			//Add Package Charging Operator Map details
			sub.setPackageChargingInterfaceMapId(pkgchintfMap.getPkgchintfid());
			//set Status=activated
			sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
			java.util.Date reqTime=new java.util.Date(System.currentTimeMillis());
			sub.setSubRequestDate(reqTime);
			sub.setCgStatusCode("NA");
			sub.setCgResponseTime(reqTime);
			sub.setCgTransactionId("NA");
			sub.setSubChargedDate(reqTime);
			
			//Add Voucher Details
			sub.setVoucherId(voucher.getCouponid());
			sub.setVoucherName(voucher.getVoucherId());
			sub.setVoucherVendor(""+voucher.getVendorId());
			sub.setVoucherType(voucher.getVoucherType());
			sub.setVoucherDiscount((double)voucher.getDiscuountValue());
			sub.setVoucherDiscountType(voucher.getDiscountType());
			sub=calculateVoucherDiscount(sub,chpp.getPricePoint(),voucher.getDiscountType(),voucher.getDiscuountValue());
			sub.setChargingCategory("VOUCHER-PRECHARGED");
			
			
		}catch(Exception e){
			logger.error(logHeader+" Exception while creation subscriber for Voucher Activation "+e);
			e.printStackTrace();
		}
		return sub;
	}
	
	private Subscriber calculateVoucherDiscount(Subscriber sub,Double packagePrice,String discountType,Integer discountValue){
		String logHeader=logHead+"calculateVoucherDiscount()::{subscriberId="+sub.getSubscriberId()+",msisdn="+sub.getMsisdn()+"} :: ";
		try{
		//PERCENTAGE/FIX-AMOUNT/NO-OF-DAYS
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		java.util.Date startDate=cal.getTime();
		cal.add(Calendar.DATE, "NO-OF-DAYS".equalsIgnoreCase(discountType)?discountValue.intValue():sub.getValidity());
		java.util.Date endDate=cal.getTime();
		
		sub.setRenewalRequired(false);
		sub.setSubChargedDate(startDate);
		sub.setSubRequestDate(startDate);
		sub.setValidityStartDate(startDate);
		sub.setValidityEndDate(endDate);
		
		
		if("PERCENTAGE".equalsIgnoreCase(discountType)){
			double discount=(discountValue*100)/packagePrice;
			sub.setVoucherDiscount(discount);
			sub.setChargedAmount(packagePrice-discount);
			
		}else if("FIX-AMOUNT".equalsIgnoreCase(discountType)){
			sub.setVoucherDiscount((double)discountValue);
			sub.setChargedAmount(packagePrice-discountValue);
			
		}else if("NO-OF-DAYS".equalsIgnoreCase(discountType)){
			sub.setVoucherDiscount((double)discountValue);
			sub.setChargedAmount(packagePrice);
		}
		}catch(Exception e){
			logger.error(logHeader+" Exception while calculating discount for Voucher Activation "+e);
			e.printStackTrace();
		}
		
		return sub;
		
	}
	
	
	
	//Query Subscriber Status
		public Subscriber checkStatus(String subscriberId,Long msisdn,String serviceName){
			Subscriber sub=new Subscriber();
			subscriberId=JocgString.formatString(subscriberId, ""+msisdn);
			String loggerHead=logHead+"checkStatus()::{msisdn="+msisdn+",subscriberId="+subscriberId+"}"+"::";
			try{
				//Search Service Details
				//Service service=jocgCache.getServicesByName(serviceName);
			//	logger.info(loggerHead+" Found Service object against '"+serviceName+"' is "+service);
				//if(service.getServiceid()>0){
				  serviceName=(serviceName==null)?"MOBTV":serviceName.trim();
					logger.info(loggerHead+" Searching Subscriber for subscriberId '"+subscriberId+"',serviceName"+serviceName);
					sub=searchSubscriber(subscriberId,msisdn,0,serviceName);//subscriberDao.findBySubscriberIdAndServiceName(subscriberId,serviceName);
					if((sub==null || sub.getId()==null) && msisdn.longValue()>0) {
						List<Subscriber> sub1=subscriberDao.findByMsisdn(msisdn);
						for(Subscriber s:sub1) if((serviceName.equalsIgnoreCase(s.getServiceName()) || s.getServiceId()==0) && s.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){ sub=s;break;}
						sub1=null;
					}
				//}
			}catch(Exception e){
				logger.error(loggerHead+e.getMessage());
				sub=new Subscriber();
			}
			return sub;
		}
		

		public String parseSubscriberStatus(Integer subStatus,java.util.Date validityEnd){
			String status="N";
			try{
				Calendar cal=Calendar.getInstance();
				if(validityEnd!=null)
					cal.setTimeInMillis(validityEnd.getTime());
				
				java.util.Date dateExpiry=cal.getTime();
				cal.setTimeInMillis(System.currentTimeMillis());
				java.util.Date dateNow=cal.getTime();
				//P=Pending,A=Active/G=Grace/S=Suspended/D=Deactivated/N=Not exists
				if((subStatus==JocgStatusCodes.SUB_SUCCESS_FREE || subStatus==JocgStatusCodes.SUB_SUCCESS_ACTIVE) && (dateNow.before(dateExpiry) || dateNow.equals(dateExpiry))){// && dateNow.before(dateExpiry)){
					status="A";
				}else if((subStatus==JocgStatusCodes.SUB_SUCCESS_FREE || subStatus==JocgStatusCodes.SUB_SUCCESS_ACTIVE)){
					status="P";
				}else if(subStatus==JocgStatusCodes.SUB_SUCCESS_GRACE ){
					status="P";
				}else if(subStatus==JocgStatusCodes.SUB_SUCCESS_PENDING || subStatus==JocgStatusCodes.SUB_SUCCESS_CG){
					status="N";
				}else if(subStatus==JocgStatusCodes.SUB_SUCCESS_PARKING ){
					status="P";
				}else if(subStatus==JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE || subStatus==JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING){
					status="P";
				}else if(subStatus==JocgStatusCodes.UNSUB_SUCCESS){
					status="D";
				}else if(validityEnd!=null){
					status="D";
				}
			}catch(Exception e){
				
			}
			return status;
		}
		
		public Subscriber insert(Subscriber sub){
			sub.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
			return subscriberDao.insert(sub);
		}
		public Subscriber save(Subscriber sub){
			sub.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
			return subscriberDao.save(sub);
		}
}