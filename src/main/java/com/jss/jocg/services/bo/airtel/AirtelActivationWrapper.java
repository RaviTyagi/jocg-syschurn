package com.jss.jocg.services.bo.airtel;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AirtelActivationWrapper {
	
	 	@JsonProperty(value = "object")
	    private ActivationObject activationObject;
	    @JsonProperty(value = "response")
	    private ActivationResponse activationResponse;

	    /**
	     * @return the activationObject
	     */
	    public ActivationObject getActivationObject() {
	        return activationObject;
	    }

	    /**
	     * @param activationObject the activationObject to set
	     */
	    public void setActivationObject(ActivationObject activationObject) {
	        this.activationObject = activationObject;
	    }

	    /**
	     * @return the activationResponse
	     */
	    public ActivationResponse getActivationResponse() {
	        return activationResponse;
	    }

	    /**
	     * @param activationResponse the activationResponse to set
	     */
	    public void setActivationResponse(ActivationResponse activationResponse) {
	        this.activationResponse = activationResponse;
	    }
}
