package com.jss.jocg.services.bo;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;

import com.jss.jocg.model.DeviceInfo;
import com.jss.jocg.model.DeviceResolver;


public class JocgDeviceManager {
	private static final Logger logger = LoggerFactory
			.getLogger(JocgDeviceManager.class);
	
    @Resource
    private DeviceResolver deviceResolver;
    
   
    
    
    public DeviceInfo identifyDevice(String logHeader,Model model, HttpServletRequest request) {
    	 DeviceInfo deviceInfo=null;
    	 try{
	        deviceInfo = updateModel(model, request);
	        StringBuilder sb=new StringBuilder();
	        sb.append(deviceInfo.getUserAgent()).append("<br/>").append(deviceInfo.getMarkUp()).append("<br/>");
	        sb.append(deviceInfo.toString());
	        logger.info(logHeader+" Exctracted Device Details "+sb.toString());
    	 }catch(Exception e){
    		 logger.error(logHeader+" error while extracting Device Details "+e);
    	 }
        return deviceInfo;
       // return getView(deviceInfo.isDisplayNormal());
    }
    
    private DeviceInfo updateModel(Model model, HttpServletRequest request) {
        DeviceInfo deviceInfo = deviceResolver.resolveDevice(request);
        model.addAttribute("device", deviceInfo);
        return deviceInfo;
    }
//http://localhost:8080/index.htm
    private String getView(boolean normal) {
        return normal ? "index" : "index-mobile";
    }
    
}
