package com.jss.jocg.services.bo.model;

public class QRLastTransaction {
	//Last Pack subscribed/tried by customer
	String lastTransaction;

	public String getLastTransaction() {
		return lastTransaction;
	}

	public void setLastTransaction(String lastTransaction) {
		this.lastTransaction = lastTransaction;
	}

	
}
