package com.jss.jocg.services.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.ui.Model;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.model.DeviceInfo;
import com.jss.jocg.model.DeviceResolver;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.dao.SubscriberTestRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class JocgService {
	private static final Logger logger = LoggerFactory
			.getLogger(JocgService.class);
	
	 @Value("${jocg.msisdn.headers}")
	 public String msisdnHeaderList;
	 
	 @Value("${jocg.imsi.headers}")
	 public String imsiHeaderList;
	 
	 @Value("${jocg.transid.template}")
	 public String jocgTransIdTemplate;
	 
	 @Value("${jocg.checksumkey}")
	 public String checksumKey;
	 
	 @Value("${jocg.checksumreq}")
	 public String checkSumCategories;
	 

	 
	 //Ingest Subscription Manager
	 @Autowired public SubscriptionManager subMgr;
	 @Autowired public SubscriberTestRepository subtestDao;
	 
	 @Autowired public JocgCacheManager jocgCache;
	
	 //Ingest Device Handler Beans
	 @Resource  public DeviceResolver deviceResolver;
	 @Autowired public JocgDeviceManager deviceMgr;
	 
	 //Ingest Charging Processing Bean
	 @Autowired public ChargingProcessor chargingProcessor;
	 @Autowired public CDRBuilder cdrBuilder;
	 
	 @Autowired public CircleIdentifier circleIndentifier;
		
	 @Autowired public HttpServletRequest requestContext;
	 @Autowired public HttpServletResponse responseContext;
	 
	 @Autowired public ChargingCDRRepository chargingCDRDao;
	 @Autowired public SubscriberRepository subscriberDao;
	 @Autowired public JmsTemplate jmsTemplate;
		
	 @PostConstruct
	 public void init(){
		 try{
			 
		 }catch(Exception e){}
	 }
	 
	 SimpleDateFormat idformat=new SimpleDateFormat("ddMM");
	protected String generateUniqueId(Integer operatorId,Integer adnetworkId){
		String idTemplate="";
		try{
		idTemplate=this.jocgTransIdTemplate;
		String uniqNumber=idformat.format(new java.util.Date(System.currentTimeMillis()))+jocgCache.getTransactionSequence(operatorId, 7);
		String adnStr=""+adnetworkId.intValue();
		while(adnStr.length()<3) adnStr="0"+adnStr;
	
		idTemplate=idTemplate.replaceAll("<ADNETWORK>", adnStr);
		
		idTemplate=idTemplate.replaceAll("<TRANSID>", uniqNumber);
		idTemplate=idTemplate.replaceAll("<SERVERID>", ""+jocgCache.getServerId().intValue());
//		if(operatorId==3){
//			idTemplate=idTemplate.substring(idTemplate.lastIndexOf("_")+1);
//		}
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		System.out.println("Unique Id Generated "+idTemplate);
		return idTemplate;
	}
	 
	public TrafficCampaign searchCampaign(String campaignId){
		TrafficCampaign cmp=null;
		try{
			cmp=jocgCache.getTrafficCampaignById(JocgString.strToInt(campaignId,0));
		}catch(Exception e){
			
		}finally{
			if(cmp==null){
				cmp=new TrafficCampaign();
				cmp.setCampaignid(0);
				cmp.setTsourceid(0);
			}
			
		}
		return cmp;
		
	}
	
	
	protected Circleinfo identifyCircle(String sourceIP,String imsi,Long msisdn){
		return circleIndentifier.identifyCircle(sourceIP, imsi, msisdn);
	}
	
	protected String parseImsi(){
		String imsi="";
		try{
			String[] str=imsiHeaderList.split(",");
			String msiStr="";
			for(String s:str){
				String sU=s.toUpperCase().trim();
				msiStr=requestContext.getHeader(s);
				msiStr=(msiStr==null)?"NA":msiStr.trim();
				if("NA".equalsIgnoreCase(msiStr)){
					msiStr=requestContext.getHeader(s.toUpperCase());
					msiStr=(msiStr==null)?"NA":msiStr.trim();
				}
				if("NA".equalsIgnoreCase(msiStr)){
					msiStr=requestContext.getHeader(s.toLowerCase());
					msiStr=(msiStr==null)?"NA":msiStr.trim();
				}
				if(!"NA".equalsIgnoreCase(msiStr)){
					imsi=msiStr.trim();
					if(imsi.length()>0) break;
				}
			}
		}catch(Exception e){
			
		}
		return imsi;
	}
	
	protected Long parseMsisdn(){
		Long msisdn=0L;
		try{
			String[] str=msisdnHeaderList.split(",");
			String msiStr="";
			for(String s:str){
				String sU=s.toUpperCase().trim();
				msiStr=requestContext.getHeader(s);
				msiStr=(msiStr==null)?"NA":msiStr.trim();
				if("NA".equalsIgnoreCase(msiStr)){
					msiStr=requestContext.getHeader(s.toUpperCase());
					msiStr=(msiStr==null)?"NA":msiStr.trim();
				}
				if("NA".equalsIgnoreCase(msiStr)){
					msiStr=requestContext.getHeader(s.toLowerCase());
					msiStr=(msiStr==null)?"NA":msiStr.trim();
				}
				if(!"NA".equalsIgnoreCase(msiStr)){
					msisdn=Long.parseLong(msiStr);
					if(msisdn.longValue()>0) break;
				}
			}
		}catch(Exception e){
			
		}
		return msisdn;
	}
	
	public String parseDevice(String logHeader,Model model){
		StringBuilder sb=new StringBuilder();
		try{
			DeviceInfo device=deviceMgr.identifyDevice(logHeader, model, requestContext);
			sb.append("Brand=").append(device.getBrand()).append("|Model=").append(device.getModel()).append("|").append("OS=").append(device.getOsName()).append("|OSVersion=").append(device.getOsVersion());
			device=null;
		}catch(Exception e){
			
		}
		return sb.toString();
		
	}
	
	public Subscriber searchSubscriberByTransId(String cdrId){
		Subscriber sub=null;
		try{
			sub=subscriberDao.findByJocgCDRId(cdrId);
		}catch(Exception e){}
		finally{
			if(sub==null) sub=new Subscriber();
		}
		return sub;
	}
	
	
	
	public List<ChargingCDR> searchLastCDRByMsisdn(Long msisdn){
		List<ChargingCDR> cdr=null;
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cdr=chargingCDRDao.findByMsisdnAndRequestDateAndStatus(msisdn,cal.getTime(),JocgStatusCodes.CHARGING_INPROCESS);
		}catch(Exception e){}
		finally{
			if(cdr==null) cdr=new ArrayList<>();
		}
		return cdr;
	}
	
	public ChargingCDR searchCDRByJocgTransId(String jocgTransId){
		ChargingCDR cdr=null;
		try{
			cdr=chargingCDRDao.findByjocgTransId(jocgTransId);
		}catch(Exception e){}
		finally{
			if(cdr==null) cdr=new ChargingCDR();
		}
		return cdr;
	}
	
	public ChargingCDR updateCDR(String logHead,ChargingCDR cdr,ChargingResponse cresp,boolean insertFlag){
		
		try{
			if(cresp!=null){
				cdr.setStatus(cresp.getStatusCode());
				cdr.setStatusDescp(cresp.getRedirectURL());
			}
		}catch(Exception e){
			logger.error(logHead+"==> updateCDR(1) :: Exception "+e);
		}
		return updateCDR(logHead,cdr,insertFlag);
	}
	
	public ChargingCDR updateCDR(String logHead,ChargingCDR cdr,boolean insertFlag){
		logHead +="==> updateCDR() :: ";
		try{
			if(insertFlag)
				cdr=cdrBuilder.insert(cdr);
			else
				cdr=cdrBuilder.save(cdr);
		}catch(Exception e){
			logger.error(logHead+" Exception "+e);
		}
		return cdr;
	}
	
	public Subscriber updateSubscriber(String logHead,Subscriber sub,boolean insertFlag){
		logHead +="==> updateSubscriber() :: ";
		try{
			if(insertFlag)
				sub=subMgr.insert(sub);
			else
				sub=subMgr.save(sub);
		}catch(Exception e){
			logger.error(logHead+" Exception "+e);
		}
		return sub;
	}
	protected ChargingCDR prepareDBCDR(String logHeader,Model model,String rpackageName,Double rpackagePrice,String rNetworkType,String rvoucherCode,Double ramountToBeCharged,
			String rVoucherVendor,String subscriberId,Long msisdn,Subscriber sub){
		ChargingCDR newCDR=new ChargingCDR();
		logHeader=logHeader+"prepareDBCDR()::";
		try{
			//Append Device Details
			newCDR.setDeviceDetails(parseDevice(logHeader,model));
			newCDR.setUserAgent(requestContext.getHeader("user-agent"));
			newCDR.setRequestCategory(sub.getTransactionCategory());
			newCDR.setSubscriberId(sub.getSubscriberId());
			newCDR.setMsisdn(sub.getMsisdn());
			newCDR.setPlatformName(sub.getPlatformId());
			newCDR.setSystemId(sub.getSystemId());
			newCDR.setRqPackageName(rpackageName);
			newCDR.setRqPackagePrice(rpackagePrice);
			newCDR.setRqNetworkType(rNetworkType);
			newCDR.setRqVoucherCode(rvoucherCode);
			newCDR.setRqAmountToBeCharged(ramountToBeCharged);
			newCDR.setRqVoucherVendor(rVoucherVendor);
			newCDR.setSystemId(sub.getSystemId());
			newCDR.setSourceNetworkId(sub.getSourceNetworkId());
			newCDR.setSourceNetworkName(sub.getSourceNetworkName());
			newCDR.setSourceNetworkCode(sub.getSourceNetworkCode());
			newCDR.setAggregatorId(sub.getParentChintfId());
			ChargingInterface chintf=jocgCache.getChargingInterface(sub.getParentChintfId());
			if (chintf!=null) newCDR.setAggregatorName(chintf.getName());
			chintf=null;
			newCDR.setUseParentHandler(sub.getUseParentChargingHandler()>0?true:false);
			newCDR.setChargingInterfaceId(sub.getChargingInterfaceId());
			chintf=jocgCache.getChargingInterface(sub.getChargingInterfaceId());
			if(chintf!=null){
				newCDR.setChargingInterfaceName(chintf.getName());
				newCDR.setCountryName(chintf.getCountry());
				newCDR.setCurrencyName("INR");
				newCDR.setGmtDiff(chintf.getGmtDiff());
				newCDR.setChargingInterfaceType(chintf.getChintfType());
				newCDR.setPackageChintfId(sub.getPackageChargingInterfaceMapId());
			}
			chintf=null;
			newCDR.setCircleId(sub.getCircleId());
			newCDR.setCircleName(sub.getCircleName());
			newCDR.setCircleCode(sub.getCircleCode());
			newCDR.setPackageId(sub.getPackageId());
			newCDR.setPackageName(sub.getPackageName());
			newCDR.setPackagePrice(sub.getPackagePrice());
			newCDR.setServiceId(sub.getServiceId());
			newCDR.setServiceName(sub.getServiceName());
			newCDR.setServiceCategory(sub.getServiceCategory());
			newCDR.setPricePointId(sub.getPricepointId());
			newCDR.setOperatorPricePoint(sub.getOperatorPricePoint());
			newCDR.setValidityCategory(sub.getValidityCategory());
			newCDR.setValidityUnit(sub.getValidityUnit());
			newCDR.setValidityPeriod(sub.getValidity());
			newCDR.setMppOperatorKey(sub.getOperatorServiceKey());
			newCDR.setMppOperatorParams(sub.getOperatorParams());
			newCDR.setChargedAmount(sub.getChargedAmount());
			newCDR.setVoucherId(sub.getVoucherId());
			newCDR.setVoucherName(sub.getVoucherName());
			//this.voucherVendorId=0;
			newCDR.setVoucherVendorName(sub.getVoucherVendor());
			newCDR.setVoucherType(sub.getVoucherType());
			newCDR.setVoucherDiscount(sub.getVoucherDiscount());
			newCDR.setVoucherDiscountType(sub.getVoucherDiscountType());
			newCDR.setDiscountGiven(sub.getVoucherDiscount());
			newCDR.setDiscountGiven(sub.getVoucherDiscount());
			if("VOUCHER-PRECHARGED".equalsIgnoreCase(newCDR.getChargingCategory()))
				newCDR.setChargedAmount(sub.getVoucherChargedAmount());
			else
				newCDR.setChargedAmount(sub.getChargedAmount());
			newCDR.setRenewalRequired(sub.getRenewalRequired());
			newCDR.setRenewalCategory(sub.getRenewalCategory());
			newCDR.setChargingDate(sub.getSubChargedDate());
			newCDR.setValidityStartDate(sub.getValidityStartDate());
			newCDR.setValidityEndDate(sub.getValidityEndDate());
			newCDR.setReferer(requestContext.getHeader("referer"));
			newCDR.setTrafficSourceId(sub.getTrafficSourceId());
			newCDR.setTrafficSourceName(sub.getTrafficSourceName());
				
		}catch(Exception e){
		}
		return newCDR;
	}
	
	
	

}
