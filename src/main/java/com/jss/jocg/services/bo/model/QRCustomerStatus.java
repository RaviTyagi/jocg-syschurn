package com.jss.jocg.services.bo.model;

public class QRCustomerStatus {
    java.util.Date closeDate; 
    String customerCategory;
    String customerStatus;
    java.util.Date openDate;
    String subscriberID;
    String subRegId;
	public java.util.Date getCloseDate() {
		return closeDate;
	}
	public void setCloseDate(java.util.Date closeDate) {
		this.closeDate = closeDate;
	}
	public String getCustomerCategory() {
		return customerCategory;
	}
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}
	public String getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}
	public java.util.Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(java.util.Date openDate) {
		this.openDate = openDate;
	}
	public String getSubscriberID() {
		return subscriberID;
	}
	public void setSubscriberID(String subscriberID) {
		this.subscriberID = subscriberID;
	}
	public String getSubRegId() {
		return subRegId;
	}
	public void setSubRegId(String subRegId) {
		this.subRegId = subRegId;
	}
    
}
