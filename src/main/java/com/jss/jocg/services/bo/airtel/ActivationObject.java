package com.jss.jocg.services.bo.airtel;

public class ActivationObject {
	 private String cpId;
	    private String productId;
	    private String msisdn;
	    private String seTransactionId;
	    private String seTransactionTime;
	    private String xActionId;
	    private String lifecycleId;

	    /**
	     * @return the cpId
	     */
	    public String getCpId() {
	        return cpId;
	    }

	    /**
	     * @param cpId the cpId to set
	     */
	    public void setCpId(String cpId) {
	        this.cpId = cpId;
	    }

	    /**
	     * @return the productId
	     */
	    public String getProductId() {
	        return productId;
	    }

	    /**
	     * @param productId the productId to set
	     */
	    public void setProductId(String productId) {
	        this.productId = productId;
	    }

	    /**
	     * @return the msisdn
	     */
	    public String getMsisdn() {
	        return msisdn;
	    }

	    /**
	     * @param msisdn the msisdn to set
	     */
	    public void setMsisdn(String msisdn) {
	        this.msisdn = msisdn;
	    }

	    /**
	     * @return the seTransactionId
	     */
	    public String getSeTransactionId() {
	        return seTransactionId;
	    }

	    /**
	     * @param seTransactionId the seTransactionId to set
	     */
	    public void setSeTransactionId(String seTransactionId) {
	        this.seTransactionId = seTransactionId;
	    }

	    /**
	     * @return the seTransactionTime
	     */
	    public String getSeTransactionTime() {
	        return seTransactionTime;
	    }

	    /**
	     * @param seTransactionTime the seTransactionTime to set
	     */
	    public void setSeTransactionTime(String seTransactionTime) {
	        this.seTransactionTime = seTransactionTime;
	    }

	    /**
	     * @return the xActionId
	     */
	    public String getxActionId() {
	        return xActionId;
	    }

	    /**
	     * @param xActionId the xActionId to set
	     */
	    public void setxActionId(String xActionId) {
	        this.xActionId = xActionId;
	    }

	    /**
	     * @return the lifecycleId
	     */
	    public String getLifecycleId() {
	        return lifecycleId;
	    }

	    /**
	     * @param lifecycleId the lifecycleId to set
	     */
	    public void setLifecycleId(String lifecycleId) {
	        this.lifecycleId = lifecycleId;
	    }
	    
}
