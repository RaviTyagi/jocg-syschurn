package com.jss.jocg.services.bo.airtel;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AirtelApiResponseWraper {
	 private static   SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	
	    public AirtelApiResponseWraper() {
	    }

//	    public static void main(String[] str) {
//	        AirtelApiResponseWraper jr = new AirtelApiResponseWraper();
//	        //jr.parseResponse("{\"access_token\":\"3783b03c-274c-4274-bb87-cf6ac5db06e0\",\"token_type\":\"bearer\",\"refresh_token\":\"51b33c0f-ef05-48ba-851c-02da6778ea0d\",\"expires_in\":259199,\"scope\":\"subscription\"}");
//	        //jr.parseActivationResponse("");
//	        try{
//	        FileInputStream fi=new FileInputStream("C:\\Users\\SutjitKumar\\Desktop\\temp\\26022016\\t.txt");
//	        int i=-1;
//	        String s="";
//	        while((i=fi.read())!=-1){
//	            s+=(char)i;
//	        }
//	        ActivationCallback activationCallback= jr.parseChargingActivationCallBack(s);
//	        System.out.println("activationCallback "+activationCallback);
//	        }catch(Exception ex){
//	        System.out.println("Exception "+ex);
//	        }
//	    }

	    public AirtelActivationWrapper parseActivationResponseNew(String respStr) {
	        AirtelActivationWrapper apiResponse = null;
	        ObjectMapper mapper = new ObjectMapper();
	        try {
	            apiResponse = mapper.readValue(respStr, AirtelActivationWrapper.class);
	        } catch (Exception e) {
	            apiResponse = null;
	            System.out.println("AirtelApiResponseWraper:: parseActivationResponseNew() :: Exception " + e);
	        }
	        return apiResponse;
	    }

	    public ActivationCallback parseChargingActivationCallBack(String str) {
	        String rootPath = "/Envelope/Body/notificationToCP/notificationRespDTO/";
	        ActivationCallback activationCallback = new ActivationCallback();
	        try {
	            //Create DocumentBuilderFactory for reading xml file
	            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder builder = factory.newDocumentBuilder();
	            InputStream stream = new ByteArrayInputStream(str.getBytes());
	            Document doc = builder.parse(stream);

	            // Create XPathFactory for creating XPath Object
	            XPathFactory xPathFactory = XPathFactory.newInstance();

	            // Create XPath object from XPathFactory
	            XPath xpath = xPathFactory.newXPath();

	            XPathExpression xPathExpr = xpath.compile(rootPath + "amount/text()");
	            Object result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setAmount(result.toString());

	            xPathExpr = xpath.compile(rootPath + "chargigTime/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setChargigTime(parseTime(result.toString()));

	            xPathExpr = xpath.compile(rootPath + "errorCode/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setErrorCode(result.toString());

	            xPathExpr = xpath.compile(rootPath + "errorMsg/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setErrorMsg(result.toString());

	            xPathExpr = xpath.compile(rootPath + "lowBalance/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setLowBalance(result.toString());

	            xPathExpr = xpath.compile(rootPath + "msisdn/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setMsisdn(result.toString());

	            xPathExpr = xpath.compile(rootPath + "productId/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setProductId(result.toString());

	            xPathExpr = xpath.compile(rootPath + "temp1/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setTemp1(result.toString());

	            xPathExpr = xpath.compile(rootPath + "temp2/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setTemp2(result.toString());
	                
	            xPathExpr = xpath.compile(rootPath + "temp3/text()");
	            Object temp3 = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            InputStream stream2 = new ByteArrayInputStream(temp3.toString().getBytes());
	            Document doc2 = builder.parse(stream2);

	            xPathExpr = xpath.compile("/XML/ID/text()");
	            result = xPathExpr.evaluate(doc2, XPathConstants.STRING);
	            activationCallback.setId(result.toString());
	             System.out.println("ID:: result:::::  "+result);
	            xPathExpr = xpath.compile("/XML/ChargingType/text()");
	            result = xPathExpr.evaluate(doc2, XPathConstants.STRING);
	            activationCallback.setChargingType(result.toString());

	            xPathExpr = xpath.compile("/XML/VCode/text()");
	            result = xPathExpr.evaluate(doc2, XPathConstants.STRING);
	            activationCallback.setvCode(result.toString());

	            xPathExpr = xpath.compile("/XML/partyB/text()");
	            result = xPathExpr.evaluate(doc2, XPathConstants.STRING);
	            activationCallback.setPartyB(result.toString());

	            xPathExpr = xpath.compile("/XML/channelName/text()");
	            result = xPathExpr.evaluate(doc2, XPathConstants.STRING);
	            activationCallback.setChannelName(result.toString());

	            xPathExpr = xpath.compile(rootPath+"xactionId/text()");
	            result = xPathExpr.evaluate(doc, XPathConstants.STRING);
	            activationCallback.setXactionId(result.toString());
	            
	        } catch (Exception e) {
	            activationCallback=null;
	            System.out.println("AirtelApiResponseWraper:: parseChargingActivationCallBack() :: Exception " + e);
	        }finally{
	        if(activationCallback==null){
	        activationCallback=new ActivationCallback();
	        }
	        activationCallback.setCallbackResp(str);
	        }
	        return activationCallback;
	    }

	    public ActivationApiResponse parseActivationResponse(String respStr) {
	        ActivationApiResponse apiResponse = null;
	        ObjectMapper mapper = new ObjectMapper();
	        try {
	            apiResponse = mapper.readValue(respStr, ActivationApiResponse.class);
	        } catch (Exception e) {
	            apiResponse = null;
	            System.out.println("parseResponse() :: Exception " + e);
	        }
	        return apiResponse;
	    }

	    public AirtelSmartApi parseResponse(String respStr) {
	        AirtelSmartApi apiResponse = null;
	        ObjectMapper mapper = new ObjectMapper();
	        try {
	            apiResponse = mapper.readValue(respStr, AirtelSmartApi.class);
	        } catch (Exception e) {
	            apiResponse = null;
	            System.out.println("parseResponse() :: Exception " + e);
	        }
	        return apiResponse;
	    }
	    
	    private Timestamp parseTime(String str){
	         try {
	        	 Calendar cal=Calendar.getInstance();
	             cal.setTimeInMillis(sdf.parse(str).getTime());
	        	 if(str.contains("Z") || str.contains("z")){
	        		  cal.add(Calendar.HOUR, 5);
	                  cal.add(Calendar.MINUTE, 30);
	        		 return  new Timestamp(cal.getTimeInMillis());
	        	 }else
	        		 return new Timestamp(cal.getTimeInMillis());
	        } catch (ParseException ex) {
	           System.out.println("AIRTEL-CALLBACK-CHARGING-TIME-ERROR :: "+ex.getMessage());
	           return null;
	        }
	         
	    }

}
