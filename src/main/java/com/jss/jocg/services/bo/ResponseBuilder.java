package com.jss.jocg.services.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.util.JocgStatusCodes;

public class ResponseBuilder {
	private static final Logger logger = LoggerFactory
			.getLogger(ResponseBuilder.class);
	
	public ResponseBuilder(){}
	
	public ModelAndView buildErrorResponse(String errorMessage){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("status");
		mv.addObject("message", errorMessage);
		return mv;
	}
	
	public ModelAndView buildCGRedirectResponse(String message){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("cgredirect");
		mv.addObject("message", message);
		return mv;
	}
	
	public JocgResponse buildResponse(JocgRequest req){
		logger.debug("Building Response ......"+req+", Resp "+req.getResponseObject()+", RespType="+req.getResponseObject().getRespType());
		JocgResponse resp=(req.getResponseObject()!=null)?req.getResponseObject():new JocgResponse();
		logger.debug("Building Response ......"+resp+", Request Params "+req.getRequestParams());
		logger.debug("Building Response ......"+resp+", Category "+req.getRequestParams().getRequestCategory());
		try{
			//Build Logic for response as per request Category and Landing Page schedules
				if("V".equalsIgnoreCase(req.getRequestParams().getRequestCategory())){
					resp.setValidity((req.getChargedPrice()!=null && req.getChargedPrice().getValidity()>0)?req.getChargedPrice().getValidity():0);
					logger.debug("Validity Set to "+resp.getValidity());
					resp=buildVoucherResponse(resp,req);
				}else if("W".equalsIgnoreCase(req.getRequestParams().getRequestCategory())){
					resp=buildWalletResponse(resp,req);
				}else if("OP".equalsIgnoreCase(req.getRequestParams().getRequestCategory())){
					resp=buildOperatorResponse(resp,req);
				}else if("C".equalsIgnoreCase(req.getRequestParams().getRequestCategory())){
					resp=buildCampaignResponse(resp,req);
				}else{
					resp.setRespView(buildErrorResponse(req.getResponseObject().getResponseDescription()));
				}
		}catch(Exception e){
			e.printStackTrace();
		}
		return resp;
	}
	
	public JocgResponse buildVoucherResponse(JocgResponse resp,JocgRequest req){
		
		ModelAndView mv = new ModelAndView(new MappingJackson2JsonView());
		
		//ModelAndView mv=new ModelAndView();
		//mv.setViewName("voucherresp");
		//
		//
		//Service Details Not found
		//Package Details not found
		/**
		 0 Success
	1 Invalid Request
	2  Voucher Code Already Used
	3 Telco Pack Already Associated/Asset Overlapping
	4 Failed To Process
	5 Customer Creation Failed

		 */
		//Customizing Response Object as required by Frontend
		int status=(resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS)?0:
			(resp.getResponseCode()==JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED)?3:
			(resp.getResponseDescription().contains("Voucher Already Used"))?2:
				(resp.getResponseDescription().contains("Invalid Voucher Code"))?1:	
					4;
		String statusDescp=(resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS)?"Success":
			(resp.getResponseCode()==JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED)?"Telco Pack Already Associated/Asset Overlapping":
			(resp.getResponseDescription().contains("Voucher Already Used"))?"Voucher Code Already Used":
				(resp.getResponseDescription().contains("Invalid Voucher Code"))?"Invalid Request":	
					"Failed To Process";
		int validity=(status==0)?resp.getValidity():0;
		//logger.debug("Final Validity "+validity);
		mv.addObject("status",new Integer(status));
		mv.addObject("statusDescription", statusDescp);
		mv.addObject("validityPeriod", new Integer(validity));
		
		resp.setRespView(mv);
		resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
		return resp;
	}
	
	
	public JocgResponse buildWalletResponse(JocgResponse resp,JocgRequest req){
		String loggerHead="buildWalletResponse()::{subid="+req.getSubscriberId()+",msisdn="+req.getMsisdn()+"} :: ";
		logger.debug(loggerHead+"Response code "+resp.getResponseCode());
		if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_CGSUCCESS){
			logger.debug(loggerHead+"Response Type "+resp.getRespType());
			if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_REDIRECTURL){
				logger.debug(loggerHead+"Redirecting to URL "+resp.getRedirectUrl());
				resp.setRespView(new ModelAndView("redirect:"+resp.getRedirectUrl()));
			}else if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
				//Do Nothing Response View is already set from handler
				logger.debug(loggerHead+"Returning ModelAndView  "+resp.getRespView());
			}else{
				//resp.getRespType()==null, check response status and sent model & view
				
			}
		}else{
			//Return Error URL
			logger.debug(loggerHead+"Building Error Respons  for Resp Code "+resp.getResponseCode()+",Response Descp "+resp.getResponseDescription());
			ModelAndView mv=new ModelAndView();
			mv.setViewName("status");
			mv.addObject("message", "StatusCode:"+resp.getResponseCode()+",Message:"+
					resp.getResponseDescription()+",TransId:"+req.getJocgTransactionId());
			logger.debug("resp obj "+resp);
			resp.setRespView(mv);
			resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			
		}
			
		return resp;
	}
		
	public JocgResponse buildOperatorResponse(JocgResponse resp,JocgRequest req){
		String loggerHead="buildOperatorResponse()::{subid="+req.getSubscriberId()+",msisdn="+req.getMsisdn()+"} :: ";
		logger.debug(loggerHead+"Response code "+resp.getResponseCode());
		if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_CGSUCCESS){
			logger.debug(loggerHead+"Response Type "+resp.getRespType());
			if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_REDIRECTURL){
				logger.debug(loggerHead+"Redirecting to URL "+resp.getRedirectUrl());
				resp.setRespView(new ModelAndView("redirect:"+resp.getRedirectUrl()));
			}else if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
				//Do Nothing Response View is already set from handler
				logger.debug(loggerHead+"Returning ModelAndView  "+resp.getRespView());
			}else{
				//resp.getRespType()==null, check response status and sent model & view
				ModelAndView mv=new ModelAndView();
				mv.setViewName("cgredirect");
				mv.addObject("message",resp.getResponseDescription());
				logger.debug("resp obj "+resp);
				resp.setRespView(mv);
				resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			}
		}else{
			//Return Error URL
			logger.debug(loggerHead+"Building Error Respons  for Resp Code "+resp.getResponseCode()+",Response Descp "+resp.getResponseDescription());
			ModelAndView mv=new ModelAndView();
			mv.setViewName("cgredirect");
			mv.addObject("message",resp.getResponseDescription());
			logger.debug("resp obj "+resp);
			resp.setRespView(mv);
			resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			
		}
			
		return resp;
	}
	
	public JocgResponse buildCampaignResponse(JocgResponse resp,JocgRequest req){
		String loggerHead="buildCampaignResponse()::{subid="+req.getSubscriberId()+",msisdn="+req.getMsisdn()+"} :: ";
		logger.debug(loggerHead+"Response code "+resp.getResponseCode());
		if(resp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_SUCCESS || 
				resp.getResponseCode()==JocgStatusCodes.CHARGING_CGSUCCESS){
			logger.debug(loggerHead+"Response Type "+resp.getRespType());
			if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_REDIRECTURL){
				logger.debug(loggerHead+"Redirecting to URL "+resp.getRedirectUrl());
				resp.setRespView(new ModelAndView("redirect:"+resp.getRedirectUrl()));
			}else if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
				//Do Nothing Response View is already set from handler
				logger.debug(loggerHead+"Returning ModelAndView  "+resp.getRespView());
			}else{
				//resp.getRespType()==null, check response status and sent model & view
				ModelAndView mv=new ModelAndView();
				mv.setViewName("status");
				mv.addObject("message",resp.getResponseDescription());
				logger.debug("resp obj "+resp);
				resp.setRespView(mv);
				resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			}
		}else{
			//Campaign Routing Fixes
			//added condition to handle routing in failure case CLICKCOUNT.
			if(resp.getRespType()!=null && resp.getRespType()==JocgResponse.RESPTYPE_MODELANDVIEW){
				//Do Nothing Response View is already set from handler
				logger.debug(loggerHead+"Returning ModelAndView  "+resp.getRespView());
			}else{
				
				//Return Error URL
				logger.debug(loggerHead+"Building Error Respons  for Resp Code "+resp.getResponseCode()+",Response Descp "+resp.getResponseDescription());
				ModelAndView mv=new ModelAndView();
				mv.setViewName("status");
				mv.addObject("message",resp.getResponseDescription());
				logger.debug("resp obj "+resp);
				resp.setRespView(mv);
				resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			}
			
		}
			
		return resp;
	}
	/*public JocgResponse buildCampaignResponse(JocgResponse resp,JocgRequest req){
		logger.debug("Response Object "+resp);
		if(resp.getRespView()==null || (resp.getRespType()==JocgResponse.RESPTYPE_REDIRECTURL && (resp.getRedirectUrl()==null || resp.getRedirectUrl().length()<=0)) ){
			logger.debug("buildCampaignResponse :: Invalid redirect URL "+resp.getRedirectUrl());
			resp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			ModelAndView mv=new ModelAndView();
			mv.setViewName("status");
			mv.addObject("message","System Error! Please try again later.");
		}else{
			logger.debug("buildCampaignResponse :: redirecting to  "+resp.getRedirectUrl());
		}
		return resp;
	}*/

}
