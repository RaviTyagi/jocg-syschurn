package com.jss.jocg.services.bo;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.json.JsonSubcontrolConfigConverter;
import com.jss.jocg.cache.entities.config.json.SubcontrolConfig;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgRequestParameters;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class RequestValidator {
	private static final Logger logger = LoggerFactory
			.getLogger(RequestValidator.class);
	
	public static int TPROCESS_REQ=1;
	public static int TPROCESS_REQ_DIFFSERVICE=2;
	public static int TPROCESS_REQ_DIFFPACKAGE=3;
	public static int TPROCESS_REQ_DIFFCAMPAIGN=4;
	public static int TPROCESS_REROUTE_URL=5;
	public static int TPROCESS_DENY=-1;
	
	
	
	@Value("${transexpiry}")
	Integer transactionExpiryHours;
	
	@Value("${unsubexpiry}")
	Integer unsubExpiryHours;
	
	String errorMessage;
	@Autowired public JocgCacheManager jocgCache;
	
	 @Autowired ChargingCDRRepository chargingCDRDao;
	 @Autowired SubscriberRepository subscriberDao;
	 
	public RequestValidator(){
		
	}
	
	
	public LandingPageSchedule getLandingPageSchedule(Subscriber sub,JocgRequest req){
		LandingPageSchedule lps=null;
		
		return lps;
	}
	
	public JocgRequest validateSubscriptionAndRoutingInfo(String logHeader,JocgRequest newReq,Subscriber sub,HttpServletRequest requestContext){
			boolean alreadySubscribed=false;Routing routingInfo=null;
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			Integer subStatus=validateSubscription(sub, newReq);
			newReq.setSubscriber((sub!=null && sub.getId()!=null)?sub:new Subscriber());
			if(subStatus==1){
				newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED);
				newReq.getResponseObject().setResponseDescription("Already subscribed for service "+newReq.getService().getServiceName());
			}
			
			newReq.setClickCount(getClickCount(newReq.getMsisdn()));
			routingInfo=getRoutingInfo(sub,newReq,subStatus);
			newReq.setRoutingInfo(routingInfo);
			
			logger.info("Applying Routing INfo "+routingInfo);
			if(routingInfo!=null && "CAMPAIGN".equalsIgnoreCase(routingInfo.getRoutingCatg())){
				newReq=applyCampaignRouting(logHeader,newReq,routingInfo,requestContext);
			}else if(routingInfo!=null && "WASTE-URL".equalsIgnoreCase(routingInfo.getRoutingCatg())){
				newReq=applyWasteURLRouting(logHeader,newReq,routingInfo,requestContext);
			}else if(routingInfo!=null && "URL".equalsIgnoreCase(routingInfo.getRoutingCatg())){
				logger.debug(logHeader+"Applying URL Routing Rule "+routingInfo.getRoutingId());
				newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				newReq.getResponseObject().setResponseDescription(routingInfo.getCasename()+" | Routing to URL against routing id "+routingInfo.getRoutingId());
			}else if(routingInfo!=null && ("PACKAGE".equalsIgnoreCase(routingInfo.getRoutingCatg()) || "SERVICE".equalsIgnoreCase(routingInfo.getRoutingCatg()))){
				newReq=applyServiceOrPackageRoutingLogic(routingInfo.getRoutingCatg(),newReq,routingInfo);
			}
			
		
		return newReq;
	}
	
	private JocgRequest applyServiceOrPackageRoutingLogic(String routingCategory,JocgRequest newReq,Routing routingInfo){
		String logHeader="applyRoutingLogic() :: {"+newReq.getSubscriberId()+",msisdn="+newReq.getMsisdn()+"} ::";
		try{
			logger.debug(logHeader+"Applying Routing Logic for Routing Category "+routingCategory+", RoutingInfo "+routingInfo);
			if("SERVICE".equalsIgnoreCase(routingCategory)){
				//Process Request for Different Service configured on the Platform
				List<String> routingDefinition=JocgString.convertStrToList(routingInfo.getRoutingDef(),"|");
				int serviceId=0,packageId=0;
				for(String s:routingDefinition){
					if(s.toUpperCase().contains("SERVICE"))
						serviceId=JocgString.strToInt(s.substring(s.indexOf(":")+1), 0);
					else if(s.toUpperCase().contains("PACKAGE"))
						packageId=JocgString.strToInt(s.substring(s.indexOf(":")+1), 0);
				}
				
				if(serviceId>0 && packageId>0){
					//Search Service && Package Details and set into JocgRequest
					PackageDetail pack=jocgCache.getPackageDetailsByID(packageId);
					if(pack!=null && pack.getPkgid()>0 && pack.getServiceid()!=newReq.getService().getServiceid()){
						Service newService=jocgCache.getServicesById(pack.getServiceid());
						if(newService!=null && newService.getServiceid()>0 && newService.getServiceid()==serviceId){
							newReq.setService(newService);
							newReq.setPackageDetail(pack);
							logger.debug(logHeader+" Service & package name changed by Routing Logic successfully!");
						}else if(newService!=null && newService.getServiceid()>0 && newService.getServiceid()!=serviceId){
							newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
							newReq.getResponseObject().setResponseDescription("Service Id ("+serviceId+") defined in routing def is not matching with Package service id ("+newService.getServiceid()+"), Routing Failed!");
						}else{
							newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
							newReq.getResponseObject().setResponseDescription("Failed to search service Id "+pack.getServiceid()+" defined in Routing Logic. Routing Failed");
						}
					}else{
						newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
						newReq.getResponseObject().setResponseDescription("Failed to search Package Id "+packageId+" defined in Routing Logic. Routing Failed");
					}
				}else{
					newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					newReq.getResponseObject().setResponseDescription("Invalid Routing Config for routing Category 'SERVICE' , routing Def :"+routingInfo.getRoutingDef()+". Routing Failed");
				}
			}else if("PACKAGE".equalsIgnoreCase(routingCategory)){
				List<String> routingDefinition=JocgString.convertStrToList(routingInfo.getRoutingDef(),"|");
				int packageId=0;
				for(String s:routingDefinition){
					if(s.toUpperCase().contains("PACKAGE"))
						packageId=JocgString.strToInt(s.substring(s.indexOf(":")+1), 0);
				}
				if(packageId>0){
					//Search package details
					PackageDetail pack=jocgCache.getPackageDetailsByID(packageId);
					if(pack!=null && pack.getPkgid()>0 && pack.getServiceid()!=newReq.getService().getServiceid()){
						Service newService=jocgCache.getServicesById(pack.getServiceid());
						if(newService!=null && newService.getServiceid()>0){
							newReq.setService(newService);
							newReq.setPackageDetail(pack);
							logger.debug(logHeader+" Service & package name changed by Routing Logic successfully!");
						}else{
							newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
							newReq.getResponseObject().setResponseDescription(" Failed to search service Id "+pack.getServiceid()+" defined in Routing Logic. Routing Failed");
						
						}
					}else{
						newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
						newReq.getResponseObject().setResponseDescription("Failed to search Package Id "+packageId+" defined in Routing Logic. Routing Failed");
					
					}
				}else{
					newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					newReq.getResponseObject().setResponseDescription("Invalid Routing Config for routing Category 'PACKAGE' ,routing Def :"+routingInfo.getRoutingDef()+"  Routing Failed");
				
				}
			}
			
			
		}catch(Exception e){
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			newReq.getResponseObject().setResponseDescription(" Exception occured while processing Routing logic, Exception :"+e);
			logger.error(logHeader+" Exception occured while processing Routing logic, Exception :"+e);
		}
		
		
		return newReq;
	}
	
	private JocgRequest applyWasteURLRouting(String logHeader,JocgRequest newReq,Routing routingInfo,HttpServletRequest requestContext){
		String wasteUrl=routingInfo.getRoutingDef();
		
		logger.debug(logHeader+"waste URL "+wasteUrl+", adToken "+newReq.getAdNetworkToken());
		wasteUrl=wasteUrl.replace("[ADTOKEN]", newReq.getAdNetworkToken());
		
		logger.debug(logHeader+"waste URL "+wasteUrl);
		routingInfo.setRoutingDef(wasteUrl);
		logger.debug(logHeader+"Applying URL Routing Rule "+routingInfo.getRoutingId());
		newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
		newReq.getResponseObject().setResponseDescription(routingInfo.getCasename()+" | Routing to WASTE URL against routing id "+routingInfo.getRoutingId());
	
		return newReq;
	}
	private JocgRequest applyCampaignRouting(String logHeader,JocgRequest newReq,Routing routingInfo,HttpServletRequest requestContext){
		logger.debug(logHeader+"Applying Campaign Routing Rule "+routingInfo.getRoutingId());
		newReq=getCampaignDetails(logHeader,newReq,JocgString.strToInt(routingInfo.getRoutingDef(),0),requestContext);
		logger.debug(logHeader+"Routed Campaign  "+newReq.getCampaign());
		if(newReq.getCampaign()==null && newReq.getCampaign().getCampaignid()<=0){
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS);
			newReq.getResponseObject().setResponseDescription("Routed Campaign Details Not found");
		}else if(newReq.getService()==null || newReq.getService().getServiceid()<=0){
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS);
			newReq.getResponseObject().setResponseDescription("Routed Service Details Not found");
		}else if(newReq.getPackageDetail()==null && newReq.getPackageDetail().getPkgid()<=0){
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS);
			newReq.getResponseObject().setResponseDescription("Routed Package Details Not found");
		}else if(newReq.getTrafficSource()==null && newReq.getTrafficSource().getTsourceid()<=0){
			newReq.getResponseObject().setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			newReq.getResponseObject().setResponseDescription("Routed AdNetwork/Traffic Source Details Not defined");
		}
		
		return newReq;
	}
	
	private JocgRequest getCampaignDetails(String logHeader,JocgRequest newReq,Integer campaignId,HttpServletRequest requestContext){
		try{
			
			TrafficCampaign tc=jocgCache.getTrafficCampaignById(campaignId);
			logger.debug("Searching Campaign for Id "+campaignId+", Found Object "+tc);
			newReq.setCampaign(tc);
			if(newReq.getCampaign()!=null && newReq.getCampaign().getCampaignid()>0){
				//Search Traffic Source Details
				newReq.setTrafficSource(jocgCache.getTrafficSourceById(newReq.getCampaign().getTsourceid()));
				if(newReq.getTrafficSource()!=null && newReq.getTrafficSource().getTsourceid()>0){
					//Search Package & Service Details
					newReq.setPackageDetail(jocgCache.getPackageDetailsByID(newReq.getCampaign().getPkgid()));
					if(newReq.getPackageDetail()!=null && newReq.getPackageDetail().getPkgid()>0){
						newReq.setService(jocgCache.getServicesById(newReq.getPackageDetail().getServiceid()));
						
					}
					
					//Fetch AdNetwork Token
					String tokenP=newReq.getTrafficSource().getTokenParams();
					String[] tokenParams=tokenP.split(",");
					StringBuilder sp=new StringBuilder();
					for(String t:tokenParams){
						sp.append(t).append("=").append(requestContext.getParameter(t)).append("&");
					}
					newReq.setAdNetworkToken(sp.toString());
					
					//if(newReq.getAdNetworkToken().endsWith("&")) newReq.setAdNetworkToken(newReq.getAdNetworkToken().substring(0,newReq.getAdNetworkToken().length()-1));
					
				}
				newReq.setChargingOperator(jocgCache.getChargingInterface(newReq.getCampaign().getChintfid()));
				
			}
		}catch(Exception e){}
		
		return newReq;
	}
	
	public JocgRequest fetchServiceDetails(String logHeader,JocgRequest newReq,HttpServletRequest requestContext){
		Integer processStatus=JocgStatusCodes.CHARGING_INPROCESS;
		String statusDescp="";
		if("C".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory())){
			//Search Campaign Detail
			newReq=getCampaignDetails(logHeader,newReq,newReq.getRequestParams().getCampaignId(),requestContext);
			if(newReq.getCampaign()==null && newReq.getCampaign().getCampaignid()<=0){
				processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
				statusDescp="Campaign Details Not found";
			}else if(newReq.getService()==null || newReq.getService().getServiceid()<=0){
				processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
				statusDescp="Service Details Not found";
			}else if(newReq.getPackageDetail()==null && newReq.getPackageDetail().getPkgid()<=0){
				processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
				statusDescp="Invalid Package Name";
			}else if(newReq.getTrafficSource()==null && newReq.getTrafficSource().getTsourceid()<=0){
				processStatus=JocgStatusCodes.CHARGING_FAILED;
				statusDescp="AdNetwork/Traffic Source Details Not defined";
			}
			
		}else if("V".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory())){
			newReq.setVoucher(jocgCache.getVoucherDetails(newReq.getRequestParams().getVoucherCode()));
			if(newReq.getVoucher()!=null && newReq.getVoucher().getCouponid()!=null && newReq.getVoucher().getCouponid()>0){
				if(newReq.getVoucher().getStatus()!=1 ){
					if(newReq.getVoucher().getStatus()==2){
						processStatus=JocgStatusCodes.CHARGING_FAILED;
						statusDescp="Voucher Already Used";
					}else{
						processStatus=JocgStatusCodes.CHARGING_FAILED;
						statusDescp="Invalid Voucher Code";
					}
				}else{
					newReq.setPackageDetail(jocgCache.getPackageDetailsByID(newReq.getVoucher().getPkgid()));
					if(newReq.getPackageDetail()!=null && newReq.getPackageDetail().getPkgid()>0){
						newReq.setService(jocgCache.getServicesById(newReq.getPackageDetail().getServiceid()));
						if(newReq.getService()==null || newReq.getService().getServiceid()<=0){
							processStatus=JocgStatusCodes.CHARGING_FAILED;
							statusDescp="Service Details Not found";
						}
					}else{
						processStatus=JocgStatusCodes.CHARGING_FAILED;
						statusDescp="Package Details not found";
					}
				}
			}else{
				processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
				statusDescp="Invalid Voucher Code";
			}
		}else{//W/OP
			newReq.setPackageDetail(jocgCache.getPackageDetailsByName(newReq.getRequestParams().getPackageName()));
			if(newReq.getPackageDetail()!=null && newReq.getPackageDetail().getPkgid()>0){
				newReq.setService(jocgCache.getServicesById(newReq.getPackageDetail().getServiceid()));
				if(newReq.getService()==null || newReq.getService().getServiceid()<=0){
					processStatus=JocgStatusCodes.CHARGING_FAILED;
					statusDescp="Service Details Not found";
				}
			}else{
				processStatus=JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS;
				statusDescp="Invalid Package Name";
			}
		}
		newReq.getResponseObject().setResponseCode(processStatus);
		newReq.getResponseObject().setResponseDescription(statusDescp);
		return newReq;
	}
	
	public CgimageSchedule getCGImageInfo(int pkgChintfMapId,int chargingInterfaceId,int trafficSourceId,int clickCount){
		return getCGImageInfo(pkgChintfMapId,trafficSourceId,clickCount);
	}
	
	public CgimageSchedule getCGImageInfo(int chargingInterfaceId,int trafficSourceId,int clickCount){
		CgimageSchedule imageSch=null;
		Map<Integer,CgimageSchedule> cgImageList=jocgCache.getScheduledImages(chargingInterfaceId, trafficSourceId);
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		int cdayOfWeek=cal.get(Calendar.DAY_OF_WEEK);
		int chourOfDay=cal.get(Calendar.HOUR_OF_DAY);
		Iterator<CgimageSchedule> i=cgImageList.values().iterator();
		while(i.hasNext()){
			imageSch=i.next();
			boolean scheduleMatched=false;
			logger.debug("Validating "+imageSch.getImgid()+",ScheduleType="+imageSch.getScheduleType()+" {DaysOfWeek="+imageSch.getDaysOfWeek()+"|hoursList="+imageSch.getHrsOfDay()+" },Current Day of week "+cdayOfWeek+", Hour :"+chourOfDay);
			if("DAYOFWEEK".equalsIgnoreCase(imageSch.getScheduleType()) || "DEFAULT".equalsIgnoreCase(imageSch.getScheduleType())){
				scheduleMatched=("SUNDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.SUNDAY)?true:
								("MONDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.MONDAY)?true:
								("TUESDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.TUESDAY)?true:
								("WEDNESDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.WEDNESDAY)?true:
								("THURSDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.THURSDAY)?true:
								("FRIDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.FRIDAY)?true:
								("SATURDAY".equalsIgnoreCase(imageSch.getDaysOfWeek()) && cdayOfWeek==Calendar.SATURDAY)?true:
								("ALLDAYS".equalsIgnoreCase(imageSch.getDaysOfWeek()))?true:false;
				String hrsOfDay=JocgString.formatString(imageSch.getHrsOfDay(),"NA");
				scheduleMatched=(scheduleMatched==true)?(("NA".equalsIgnoreCase(hrsOfDay) || hrsOfDay.length()<=0)?scheduleMatched:hrsOfDay.contains(","+chourOfDay+",")?scheduleMatched:false):scheduleMatched;
				
			}else if("DAILY".equalsIgnoreCase(imageSch.getScheduleType())){
				String hrsOfDay=JocgString.formatString(imageSch.getHrsOfDay(),"NA");
				scheduleMatched=("NA".equalsIgnoreCase(hrsOfDay) || hrsOfDay.length()<=0)?scheduleMatched:hrsOfDay.contains(","+chourOfDay+",")?true:false;
			}else if("CLICKCOUNT".equalsIgnoreCase(imageSch.getScheduleType())){
				int clickCountToValidate=JocgString.strToInt(imageSch.getHrsOfDay(), 0);
				scheduleMatched=(clickCountToValidate>0 && clickCountToValidate<clickCount)?true:false;
			}
			logger.debug("scheduleMatched "+scheduleMatched);
			if(!scheduleMatched) imageSch=null;
			else break;
		}
		return imageSch;
	}
	
	public int getClickCount(Long msisdn){
		int clickCount=0;
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			cal.set(Calendar.AM_PM,Calendar.AM);
			cal.set(Calendar.HOUR_OF_DAY,0);
			cal.set(Calendar.MINUTE,0);
			cal.set(Calendar.SECOND,0);
			java.util.Date today=cal.getTime();
			logger.debug("Searching clicks against "+msisdn+", Date "+today);
			List<ChargingCDR> existingCDR=chargingCDRDao.findByMsisdnAndRequestDate(msisdn,today);
			clickCount=existingCDR.size();
			existingCDR=null;
		}catch(Exception e){
			clickCount=0;
			e.printStackTrace();
		}
		return clickCount;
	}
	
	public Routing getRoutingInfo(Subscriber sub,JocgRequest req,Integer subStatus){
		Routing routing=null;
		String logHeader="getRoutingInfo() :: "+req.getMsisdn()+"::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			cal.set(Calendar.AM_PM,Calendar.AM);
			cal.set(Calendar.HOUR_OF_DAY,0);
			cal.set(Calendar.MINUTE,0);
			cal.set(Calendar.SECOND,0);
			java.util.Date today=cal.getTime();
			List<Routing> routingConfig=jocgCache.getRoutingConfig(req.getChargingOperator().getChintfid(),req.getTrafficSource().getTsourceid(),req.getCampaign().getCampaignid(),(req.getCircle()!=null?req.getCircle().getCircleid():0),req.getRequestParams().getPublisherId());
			logger.debug(logHeader+"Routing Query Params {chintfid="+req.getChargingOperator().getChintfid()+",TsourceId="+req.getTrafficSource().getTsourceid()+",}");
			
			//set current time again in calendar
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date ctime=cal.getTime();
			
			//logger.debug("Routing Config "+routingConfig);
			if((routingConfig!=null && routingConfig.size()>0) && "C".equalsIgnoreCase(req.getRequestParams().getRequestCategory())){
				
				//Find All Traffic Routing Config if applied
				for(Routing r:routingConfig){
					try{
					if(r.getCasename().equalsIgnoreCase("ALLTRAFFIC") && (r.getChintfid()==0 || req.getChargingOperator().getChintfid()==r.getChintfid()) && 
							(req.getTrafficSource().getTsourceid()==r.getTsourceid() && req.getCampaign().getCampaignid()==r.getCampaignid() && req.getRequestParams().getPublisherId().equalsIgnoreCase(r.getPubid()))) {routing=r;break;}
					}catch(Exception ee){}
				}
				
				if(req.getRequestParams().getHeaderMsisdn()<=0){
					//Missing Msisdn
					for(Routing r:routingConfig){
						if(r.getCasename().equalsIgnoreCase("MISSING MSISDN")) {routing=r;break;}
					}
				}
				
				if(routing==null && subStatus==1){
					//Already Subscriber
					for(Routing r:routingConfig){
						if(r.getCasename().equalsIgnoreCase("ALREADY SUB")) {routing=r;break;}
					}
				}
				
				//check for repeat click
				if(routing==null){
					
					logger.debug(logHeader+"Existing CDR Count "+req.getClickCount());
					for(Routing r:routingConfig){
						//Campaign Routing Fixes
						//Using otherCnfig value instead of RoutingDef for count check.
						//Routing Def contains where to route if click count is more than the configured count.
						//if(r.getCasename().equalsIgnoreCase("REPEAT CLICK") && req.getClickCount()>=JocgString.strToInt(r.getRoutingDef(), 5)) {routing=r;break;}
						if(r.getCasename().equalsIgnoreCase("REPEAT CLICK") && req.getClickCount()>=JocgString.strToInt(r.getOtherConfig(), 5)) {routing=r;break;}
					}
					
				}
				
				logger.debug(logHeader+"Validating against Same Day ACT/DCT , Search Routing Config on ACT/DCT Flag  "+((routing==null)?true:false)+", SubStatus "+sub.getStatus()+" match with "+JocgStatusCodes.UNSUB_SUCCESS);
				//Check for Routing Logic against deactivation expiry
				if(routing==null){
					//adding null check to avoid NullPointerException
					if(sub!=null && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
						logger.debug(logHeader+"Searching Routing For ACT/DCT  Sub Charge Date "+sub.getSubChargedDate()+", Unsub Date "+sub.getUnsubDate());
							for(Routing r:routingConfig){
								if(r.getCasename().equalsIgnoreCase("UNSUB ACTIVE")){
									logger.debug(logHeader+" Validating for Case "+r.getCasename());
									int subUnsubExpiryHours=JocgString.strToInt(r.getOtherConfig(), 0);
									java.util.Date subExpiryDate=null,unsubExpiryDate=null;
									if(sub.getSubChargedDate()!=null){
										cal.setTime(sub.getSubChargedDate());
										cal.add(Calendar.HOUR_OF_DAY, subUnsubExpiryHours);
										subExpiryDate=cal.getTime();
									}
									if(sub.getUnsubDate()!=null){
										cal.setTime(sub.getUnsubDate());
										cal.add(Calendar.HOUR_OF_DAY, subUnsubExpiryHours);
										unsubExpiryDate=cal.getTime();
									}
									long subExpiryTime=subExpiryDate.getTime();
									long unsubExpiryTime=unsubExpiryDate.getTime();
									long currentTime=System.currentTimeMillis();
									logger.debug(logHeader+"Case Name "+r.getCasename()+", subExpiryTime= "+subExpiryTime+",unsubExpiryTime "+unsubExpiryTime+", currentTime "+currentTime+"|c-subtime:"+(currentTime-subExpiryTime)+",c-unsubtime:"+(currentTime-unsubExpiryTime));
									if(((currentTime-subExpiryTime)<0 || (currentTime-unsubExpiryTime)<0)){//((subExpiryDate!=null && ctime.before(subExpiryDate)) || (unsubExpiryDate!=null && ctime.before(unsubExpiryDate)))) {
											routing=r;
											break;
									}
								}
							}//for
							
							logger.debug(logHeader+"Selected Routing Object  "+routing);
					}//if
					
				}//if routing
				
				
			}
			
			
		}catch(Exception e){
			
		}
		return routing;
	}
	
	public Integer validateTrafficStats(Subscriber sub,JocgRequest req){
		Integer validatorAction=RequestValidator.TPROCESS_REQ;
		String logHead="validateTrafficStats::{subid="+req.getSubscriberId()+",msisdn="+req.getMsisdn()+"} :: ";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			cal.set(Calendar.AM_PM,Calendar.AM);
			cal.set(Calendar.HOUR_OF_DAY,0);
			cal.set(Calendar.MINUTE,0);
			cal.set(Calendar.SECOND,0);
			String subControlLevel="OPERATOR";
		    List<NotificationConfig> notifyConfig=jocgCache.getNotificationConfig(req.getChargingOperator().getChintfid(),req.getTrafficSource().getTsourceid(), req.getCampaign().getCampaignid(), req.getRequestParams().getPublisherId());
			logger.debug(logHead+" Notification Config "+notifyConfig);
			NotificationConfig config=null;
			for(NotificationConfig nc:notifyConfig) if(req.getTrafficSource().getTsourceid()==nc.getTsourceid() && 
						req.getCampaign().getCampaignid()==nc.getCampaignid() && 
						req.getRequestParams().getPublisherId()!=null && 
						req.getRequestParams().getPublisherId().equalsIgnoreCase(nc.getPubid())){
					config=nc;subControlLevel="PUBLISHER";break;
				}
			
			if(config==null) for(NotificationConfig nc:notifyConfig) if(req.getTrafficSource().getTsourceid()==nc.getTsourceid() && 
					req.getCampaign().getCampaignid()==nc.getCampaignid()){
				config=nc;subControlLevel="CAMPAIGN";break;
			}
			if(config==null) for(NotificationConfig nc:notifyConfig) if(req.getTrafficSource().getTsourceid()==nc.getTsourceid()){
				config=nc;subControlLevel="OPERATOR";break;
			}
			
			logger.debug(logHead+" Applied Notification Config "+config);
			//Check for Subcontrol Config
			if(config!=null){
				JsonSubcontrolConfigConverter jscc=new JsonSubcontrolConfigConverter();
				SubcontrolConfig scc=jscc.convertToEntityAttribute(config.getSubcontrolConfig());
				logger.debug(logHead+" parsed subcontrol config "+scc+", subControlLevel : "+subControlLevel);
				//Fetch Todays stats 
				int todayChurn=0,today20MinChurn=0,todaysActivation=0;
				
				
			}else{
				logger.error(logHead+" Notification Config is NUll, No check on sub control will be applied!");
			}
			
		}catch(Exception e){
			
		}
		return validatorAction;
		
	}
	
	
	
	
	
	public Integer validateSubscription(Subscriber sub,JocgRequest req){
		Integer subStatus=new Integer(0);
		String logHead="RV::{subid="+req.getSubscriberId()+",msisdn="+req.getMsisdn()+"} :: ";
		try{
			if(sub!=null && sub.getId()!=null && (sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE ||
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE || 
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_GRACE || 
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING)){
				
				subStatus=1;//Already Subscribed
				
				logger.info(logHead+"Subscriber Account Found and Active with "+
					((sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE)?"ACTIVE":
						(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE)?"FREE ACTIVATION":
							(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE)?"GRACE ACTIVATION":"PARKING")+" Status");
				
				
			}else if(sub!=null && sub.getId()!=null && (sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_CG ||
					sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_PENDING)){
				 java.util.Date d=sub.getSubRequestDate();
				 Calendar cal=Calendar.getInstance();
				 logger.debug("Last transaction time "+d.getTime());
				 cal.setTimeInMillis(d.getTime());
				 transactionExpiryHours=(transactionExpiryHours==null || transactionExpiryHours.intValue()<0)?0:transactionExpiryHours;
				 cal.add(Calendar.HOUR, transactionExpiryHours);
				 
				 java.util.Date expiryTime=cal.getTime();
				 cal.setTimeInMillis(System.currentTimeMillis());
				 java.util.Date currentTime=cal.getTime();
				 logger.debug("Last transaction Expiry time "+expiryTime.getTime()+", current time "+currentTime);
				 if(currentTime.before(expiryTime)){
					 subStatus=0;
					 logger.info(logHead+"Previous Subscriber Request is in "+
								((sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_CG)?"CG SUCCESS":
									"PENDING")+" Status and not yet expired");
				 }else{
					 subStatus=-1;//Exists but expired
					 logger.info(logHead+"Previous Subscriber Request is in "+
								((sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_CG)?"CG SUCCESS":
									"PENDING")+" Status and but expired");
				 }
			}else if(sub!=null && sub.getId()!=null){
				 java.util.Date d=(sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS)?sub.getUnsubDate():
					 (sub.getStatus()==JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE || 
					 sub.getStatus()==JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING)?sub.getSuspendDate():
						 sub.getUnsubDate();
				 Calendar cal=Calendar.getInstance();
				 cal.setTimeInMillis(d.getTime());
				 transactionExpiryHours=(unsubExpiryHours==null || unsubExpiryHours.intValue()<0)?0:unsubExpiryHours;
				 cal.add(Calendar.HOUR, unsubExpiryHours);
				 java.util.Date expiryTime=cal.getTime();
				if(d.before(expiryTime)){
					subStatus=0;//Exists and unsubscribed/suspended state not expired 
				}else{
					subStatus=-1;//Exists and unsubscribed/suspended state is expired 
				}
				logger.info(logHead+"Previous Subscriber Request exists and in unsubscribed/suspended status");
			}else{
				subStatus=-2;//Not exists
				logger.info(logHead+"Previous Subscriber Request does not exists");
			}
		}catch(Exception e){
			subStatus=-3;
		}
		return subStatus;
	}
	
	public boolean validateChecksum(String checkSumStr,Long checksum,String checksumKey){
		Long csValue=JocgString.generateChecksum(checkSumStr, checksumKey);
		logger.info(checkSumStr+"==> Generated Checksum "+csValue+", received checksum "+checksum);
		return (csValue.longValue()==checksum.longValue())?true:false;
			
	}
	public String prepareSubscriberId(String subscriberId,Long msisdn){
		if(subscriberId==null || "NA".equalsIgnoreCase(subscriberId) || subscriberId.length()<=0){
			return (msisdn>0)?""+msisdn.longValue():"NA";
		}else{
			return subscriberId;
		}
	}
	
	public Long selectMsisdn(Long headerMsisdn,Long requestMsisdn){
		return headerMsisdn>0?headerMsisdn:requestMsisdn>0?requestMsisdn:0;
	}
	
	public Long selectMsisdn(String requestCatg,Long headerMsisdn,Long requestMsisdn){
		if("OP".equalsIgnoreCase(requestCatg)){
			return headerMsisdn>0?headerMsisdn:requestMsisdn>0?requestMsisdn:0;	
		}else{
			return requestMsisdn>0?requestMsisdn:headerMsisdn>0?headerMsisdn:0;
		}
		
	}
	
	public boolean validateEmail(String emailId){
		boolean status=false;
		try{
		   status=(emailId==null||emailId.length()<=0)?false:
			   (emailId.indexOf("@")>0 && emailId.lastIndexOf(".")>0 && emailId.lastIndexOf(".")>emailId.indexOf("@") && emailId.lastIndexOf("@")==emailId.indexOf("@"))?true:
				   false;
		}catch(Exception e){status=false;}
		return status;
	}
	public boolean validateMsisdn(String msisdn){
		boolean status=false;
		try{
		   status=(msisdn==null||msisdn.length()<=0)?false:
			   (Long.parseLong(msisdn)>0 && msisdn.length()>=10)?true:
				   false;
		}catch(Exception e){status=false;}
		return status;
	}
	
	public ValidationResponse validateRequest(JocgRequestParameters rp){
		ValidationResponse vr=new ValidationResponse();
		vr.setValidationStatus(
		validateRequest(rp.getRequestCategory(),rp.getSourceOperator(),rp.getSubscriberId(),
				rp.getHeaderMsisdn(),rp.getRequestMsisdn(),rp.getPlatformName(),
				rp.getPackageName(),rp.getPackagePrice(),rp.getPackageValidity(),
				rp.getNetworkType(),rp.getVoucherVendor(),rp.getAmountToBeCharged(),rp.getVoucherCode(),rp.getCampaignId(),
				rp.getCheckSum()));
		vr.setStatusDescription(getStatusDescription(vr.getValidationStatus()));
		return vr;
	}
	
	
	public Integer validateRequest(String requestCategory, String sourceOperator,
			String subscriberId,Long headerMsisdn,Long requestMsisdn,String platformName,
			 String packageName,Double packagePrice,Integer packageValidity,
			String networkType,String voucherVendor, Double amountToBeCharged,
			String voucherCode,Integer campaignId,Long checkSum){
		Integer validationCode=new Integer(0);
		logger.info("Processing for Request Category "+requestCategory);
		if("NA".equalsIgnoreCase(requestCategory)){
			validationCode=-101;
		}else{
			if("V".equalsIgnoreCase(requestCategory)){
				return validateVoucherRequest(subscriberId,headerMsisdn,requestMsisdn,platformName,
						 packageName,packagePrice,packageValidity,
							networkType,voucherVendor,amountToBeCharged,
							voucherCode,checkSum);
			}else if("W".equalsIgnoreCase(requestCategory)){
				return validateWalletRequest(sourceOperator,subscriberId,headerMsisdn,requestMsisdn,platformName,
						 packageName,packagePrice,packageValidity,
							networkType,voucherVendor,amountToBeCharged,
							voucherCode,checkSum);
			}else if("OP".equalsIgnoreCase(requestCategory) || "TEST".equalsIgnoreCase(requestCategory)){
				logger.info("Validating for Operator Charging....");
				
				return validateOperatorRequest(sourceOperator,subscriberId,headerMsisdn,requestMsisdn,platformName,
						 packageName,packagePrice,packageValidity,
							networkType,voucherVendor,amountToBeCharged,
							voucherCode,checkSum);
			}else if("C".equalsIgnoreCase(requestCategory)){
				return validateCampaignRequest(campaignId);
			}else{
				validationCode=-101;
			}
		}
		return validationCode;
	}
	
	public Integer validateCampaignRequest(Integer campaignId){
		Integer validationCode=new Integer(0);
		if(campaignId<=0){
			validationCode=-109;
		}
		return validationCode;
		
	}
	public Integer validateWalletRequest(String sourceOperator,String subscriberId,Long headerMsisdn,Long requestMsisdn,String platformName,
			 String packageName,Double packagePrice,Integer packageValidity,
			String networkType,String voucherVendor, Double amountToBeCharged,
			String voucherCode,Long checkSum){
		Integer validationCode=new Integer(0);
		//Validate subscriberId, Header Msisdn, requestMsisdn
		subscriberId=("NA".equalsIgnoreCase(subscriberId))?
				(("M".equalsIgnoreCase(networkType))?(headerMsisdn>0?""+headerMsisdn.longValue():subscriberId)
						:(requestMsisdn>0?""+requestMsisdn:subscriberId))
				:(subscriberId.length()>0?subscriberId:
					headerMsisdn>0?""+headerMsisdn:
						requestMsisdn>0?""+requestMsisdn:subscriberId);
				if("NA".equalsIgnoreCase(subscriberId)){
					validationCode=-102;
				}else if("NA".equalsIgnoreCase(platformName)){
					validationCode=-103;
				}else if("NA".equalsIgnoreCase(packageName)){
					validationCode=-104;
				}else if(packagePrice<=0){
					validationCode=-106;
				}else if(packageValidity<=0){
					validationCode=-108;
				}else if("NA".equalsIgnoreCase(sourceOperator)){
					validationCode=-107;
				}				
		return validationCode;
	}
	
	public Integer validateOperatorRequest(String sourceOperator,String subscriberId,Long headerMsisdn,Long requestMsisdn,String platformName,
			 String packageName,Double packagePrice,Integer packageValidity,
			String networkType,String voucherVendor, Double amountToBeCharged,
			String voucherCode,Long checkSum){
		Integer validationCode=new Integer(0);
		//Validate subscriberId, Header Msisdn, requestMsisdn
		subscriberId=("NA".equalsIgnoreCase(subscriberId))?
				(("M".equalsIgnoreCase(networkType))?(headerMsisdn>0?""+headerMsisdn.longValue():subscriberId)
						:(requestMsisdn>0?""+requestMsisdn:subscriberId))
				:(subscriberId.length()>0?subscriberId:
					headerMsisdn>0?""+headerMsisdn:
						requestMsisdn>0?""+requestMsisdn:subscriberId);
	   
		if("NA".equalsIgnoreCase(subscriberId)){
			validationCode=-102;
		}else if("NA".equalsIgnoreCase(platformName)){
			validationCode=-103;
		}else if("NA".equalsIgnoreCase(packageName)){
			validationCode=-104;
		}else if(packagePrice<=0){
			validationCode=-106;
		}else if("NA".equalsIgnoreCase(sourceOperator)){
			validationCode=-107;
		}else if(!"M".equalsIgnoreCase(networkType)){
			validationCode=-110;
		}
		
		return validationCode;
	}
	public Integer validateVoucherRequest(String subscriberId,Long headerMsisdn,Long requestMsisdn,String platformName,
			 String packageName,Double packagePrice,Integer packageValidity,
			String networkType,String voucherVendor, Double amountToBeCharged,
			String voucherCode,Long checkSum){
		Integer validationCode=new Integer(0);
		//Validate subscriberId, Header Msisdn, requestMsisdn
		subscriberId=("NA".equalsIgnoreCase(subscriberId))?
				(("M".equalsIgnoreCase(networkType))?(headerMsisdn>0?""+headerMsisdn.longValue():subscriberId)
						:(requestMsisdn>0?""+requestMsisdn:subscriberId))
				:(subscriberId.length()>0?subscriberId:
					headerMsisdn>0?""+headerMsisdn:
						requestMsisdn>0?""+requestMsisdn:subscriberId);
		
		if("NA".equalsIgnoreCase(subscriberId)){
			validationCode=-102;
		}else if("NA".equalsIgnoreCase(voucherCode)){
			validationCode=-105;
		}
		return validationCode;
	}
	
	public String getStatusDescription(Integer errorCode){
		if(errorCode==0) return "VALIDATION SUCCESSFUL";
		else if(errorCode==-100) return "INVALID MSISDN";
		else if(errorCode==-101) return "INVALID PATH REQUEST CATEGORY";
		else if(errorCode==-102) return "INVALID CUSTOMER ID SUBID/MSISDN";
		else if(errorCode==-103) return "INVALID PLATFORM";
		else if(errorCode==-104) return "INVALID PACKAGE NAME";
		else if(errorCode==-105) return "INVALID VOUCHER CODE";
		else if(errorCode==-106) return "INVALID PACKAGE PRICE";
		else if(errorCode==-107) return "INVALID SOURCE OPERATOR";
		else if(errorCode==-108) return "INVALID PACKAGE Validity";
		else if(errorCode==-109) return "INVALID CAMPAIGN REQUEST";
		else if(errorCode==-110) return "Please use this service through operator sim in order to purchase the pack.<br/> It cannot be purchased through wifi network";
		
		else return "VALIDATION ERROR";
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public class ValidationResponse{
		Integer validationStatus;
		String statusDescription;
		public ValidationResponse(){}
		public Integer getValidationStatus() {
			return validationStatus;
		}
		public void setValidationStatus(Integer validationStatus) {
			this.validationStatus = validationStatus;
		}
		public String getStatusDescription() {
			return statusDescription;
		}
		public void setStatusDescription(String statusDescription) {
			this.statusDescription = statusDescription;
		}
		
	}
}
