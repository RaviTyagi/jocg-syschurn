package com.jss.jocg.services.bo.airtel;

public class ActivationApiResponse {
	 String seTransactionId;
	    
	    public ActivationApiResponse(){}

	    public String getSeTransactionId() {
	        return seTransactionId;
	    }

	    public void setSeTransactionId(String seTransactionId) {
	        this.seTransactionId = seTransactionId;
	    }
	    
}
