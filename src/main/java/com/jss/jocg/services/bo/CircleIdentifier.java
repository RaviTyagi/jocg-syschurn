package com.jss.jocg.services.bo;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.util.IPMask;

public class CircleIdentifier {
	private static final Logger logger = LoggerFactory
			.getLogger(CircleIdentifier.class);
	
	@Autowired JocgCacheManager jocgCache;
	
	public CircleIdentifier(){}
	
	public MsisdnSeries identifyMsisdnSeriesByMsisdn(Long msisdn){
		MsisdnSeries ms=null;
		try{
			Iterator<MsisdnSeries> i=jocgCache.getMsisdnSeries().values().iterator();
			
			while(i.hasNext()){
				ms=i.next();
				if(ms.getMsisdnStart()<=msisdn.longValue() && ms.getMsisdnEnd()>=msisdn.longValue()) break;
				else ms=null;
			}
			
		}catch(Exception e){}
		return ms;
	}
	public Circleinfo identifyCircleByMsisdn(Long msisdn){
		Circleinfo circleInfo=null;
		try{
			Iterator<MsisdnSeries> i=jocgCache.getMsisdnSeries().values().iterator();
			MsisdnSeries ms=null;
			while(i.hasNext()){
				ms=i.next();
				if(ms.getMsisdnStart()<=msisdn.longValue() && ms.getMsisdnEnd()>=msisdn.longValue()) break;
				else ms=null;
			}
			if(ms!=null){
				circleInfo=jocgCache.getCircleInfo(ms.getCircleid());
			}
		}catch(Exception e){}
		return circleInfo;
	}
	public Circleinfo identifyCircle(String sourceIp,String imsi,Long msisdn){
		Circleinfo circleInfo=null;
		//Identify Circle By IP
		
		IPSeries ipSeries=searchIPSeries(sourceIp);
		//Identify Circle by Imsi
		ImsiSeries imsiSeries=searchImsiSeries(imsi);
		
		//Identify Circle by msisdn Series
		MsisdnSeries msiSeries=searchMsisdnSeries(""+msisdn.longValue());
		
		if(imsiSeries!=null && imsiSeries.getImsiSeriesId()>0){
			if(ipSeries.getChintfid()==imsiSeries.getChintfid() || ipSeries.getIpSeriesId()<=0){
				circleInfo=jocgCache.getCircleInfo(imsiSeries.getCircleid());
			}else {
				circleInfo=jocgCache.getCircleInfo(ipSeries.getCircleid());
			}
		}else if(msiSeries!=null && msiSeries.getMsisdnSeriesId()>0){
			if(ipSeries.getChintfid()==msiSeries.getChintfid() || ipSeries.getIpSeriesId()<=0){
				circleInfo=jocgCache.getCircleInfo(msiSeries.getCircleid());
			}else{
				circleInfo=jocgCache.getCircleInfo(ipSeries.getCircleid());
			}
		}else if(ipSeries.getIpSeriesId()>0){
			//Identify circle based on IP
			circleInfo=jocgCache.getCircleInfo(ipSeries.getCircleid());
		}else
			circleInfo=new Circleinfo();
		
		
		return circleInfo;
	}
	
	public IPSeries searchIPSeries(String remoteIP){
		IPSeries ipSeries=new IPSeries();
		try{
			Iterator<String> i = jocgCache.getIPSeries().keySet().iterator();
			String nextKey="";
			 IPMask ipMask = null;
			while(i.hasNext()){
				nextKey=i.next();
				try{
					ipMask = IPMask.getIPMask(nextKey);
					 if (IPMask.validate(ipMask, remoteIP)) {
						 ipSeries = jocgCache.getIPSeries().get(nextKey);
		                    break;
		              }
				}catch(Exception ee){}
			}
			if(ipSeries==null) ipSeries=new IPSeries();
		}catch(Exception e){
			ipSeries=new IPSeries();
		}
		return ipSeries;
	}
	
	private MsisdnSeries searchMsisdnSeries(String msisdn){
		MsisdnSeries msiSeries=new MsisdnSeries();
		try{
			msisdn=msisdn.trim();
			int i=0;int initLen=msisdn.length();
			while(msisdn.length()>2){
				msisdn=msisdn.substring(0,initLen-i);
				msiSeries=jocgCache.getMsisdnSeries(msisdn);
				if(msiSeries!=null && msiSeries.getMsisdnSeriesId()>0) break;
				i++;
			}
			if(msiSeries==null) msiSeries=new MsisdnSeries();
		}catch(Exception e){
			msiSeries=new MsisdnSeries();
		}
		return msiSeries;
	}
	
	private ImsiSeries searchImsiSeries(String imsiSeries){
		ImsiSeries foundimsi=new ImsiSeries();
		try{
			String keyToSearch="";
			 if (imsiSeries.length() >= 5) {
                 String f3d = imsiSeries.substring(0, 3);
                 String fs2d = imsiSeries.substring(3, 5);
                 String fs3d = imsiSeries.substring(3, 6);
                 keyToSearch = ("404".equalsIgnoreCase(f3d)) ? (f3d + "-" + fs2d) : ("405".equalsIgnoreCase(f3d)) ? (f3d + "-" + fs3d) : "";
             }else
            	 keyToSearch=imsiSeries;
			  if (keyToSearch.length() > 0) {
				  foundimsi=jocgCache.getImsiSeries().get(keyToSearch);
			  }
			  if(foundimsi==null) foundimsi=new ImsiSeries();
		}catch(Exception e){
			foundimsi=new ImsiSeries();
		}
		return foundimsi;
	}
	
}
