package com.jss.jocg.services.bo.airtel;

import java.lang.reflect.Field;
import java.sql.Timestamp;

public class ActivationCallback {
	 private String airtelCallbackId;
	    private String amount;
	    private Timestamp chargigTime;
	    private String errorCode;
	    private String errorMsg;
	    private String lowBalance;
	    private String msisdn;
	    private String productId;
	    private String temp1;
	    private String temp2;
	    private String id;
	    private String chargingType;
	    private String vCode;
	    private String partyB;
	    private String channelName;
	    private String xactionId;
	    private String callbackResp;
	    private int process;
	    String referer;
	    String systemId;
	    String adnetworkName;
	    public ActivationCallback(){
	    	this.referer="NA";
	    }
	    
	    @Override
		public String toString(){
			Field[] fields = this.getClass().getDeclaredFields();
		    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
		    try {
		        for (Field field : fields) {
		        	
		            str.append( field.getName()).append("=").append(field.get(this)).append(",");
		        }
		    } catch (IllegalArgumentException ex) {
		        System.out.println(ex);
		    } catch (IllegalAccessException ex) {
		        System.out.println(ex);
		    }
		    str.append("}");
		    return str.toString();
		}
	    /**
	     * @return the amount
	     */
	    public String getAmount() {
	        return amount;
	    }

	    /**
	     * @param amount the amount to set
	     */
	    public void setAmount(String amount) {
	        this.amount = amount;
	    }

	  

	    /**
	     * @return the errorCode
	     */
	    public String getErrorCode() {
	        return errorCode;
	    }

	    /**
	     * @param errorCode the errorCode to set
	     */
	    public void setErrorCode(String errorCode) {
	        this.errorCode = errorCode;
	    }

	    /**
	     * @return the errorMsg
	     */
	    public String getErrorMsg() {
	        return errorMsg;
	    }

	    /**
	     * @param errorMsg the errorMsg to set
	     */
	    public void setErrorMsg(String errorMsg) {
	        this.errorMsg = errorMsg;
	    }

	    /**
	     * @return the lowBalance
	     */
	    public String getLowBalance() {
	        return lowBalance;
	    }

	    /**
	     * @param lowBalance the lowBalance to set
	     */
	    public void setLowBalance(String lowBalance) {
	        this.lowBalance = lowBalance;
	    }

	    /**
	     * @return the msisdn
	     */
	    public String getMsisdn() {
	        return msisdn;
	    }

	    /**
	     * @param msisdn the msisdn to set
	     */
	    public void setMsisdn(String msisdn) {
	        this.msisdn = msisdn;
	    }

	    /**
	     * @return the productId
	     */
	    public String getProductId() {
	        return productId;
	    }

	    /**
	     * @param productId the productId to set
	     */
	    public void setProductId(String productId) {
	        this.productId = productId;
	    }

	    /**
	     * @return the temp1
	     */
	    public String getTemp1() {
	        return temp1;
	    }

	    /**
	     * @param temp1 the temp1 to set
	     */
	    public void setTemp1(String temp1) {
	        this.temp1 = temp1;
	    }

	    /**
	     * @return the temp2
	     */
	    public String getTemp2() {
	        return temp2;
	    }

	    /**
	     * @param temp2 the temp2 to set
	     */
	    public void setTemp2(String temp2) {
	        this.temp2 = temp2;
	    }

	  
	    /**
	     * @return the partyB
	     */
	    public String getPartyB() {
	        return partyB;
	    }

	    /**
	     * @param partyB the partyB to set
	     */
	    public void setPartyB(String partyB) {
	        this.partyB = partyB;
	    }

	    /**
	     * @return the channelName
	     */
	    public String getChannelName() {
	        return channelName;
	    }

	    /**
	     * @param channelName the channelName to set
	     */
	    public void setChannelName(String channelName) {
	        this.channelName = channelName;
	    }

	    /**
	     * @return the xactionId
	     */
	    public String getXactionId() {
	        return xactionId;
	    }

	    /**
	     * @param xactionId the xactionId to set
	     */
	    public void setXactionId(String xactionId) {
	        this.xactionId = xactionId;
	    }

	    /**
	     * @return the id
	     */
	    public String getId() {
	        return id;
	    }

	    /**
	     * @param id the id to set
	     */
	    public void setId(String id) {
	        this.id = id;
	    }

	    /**
	     * @return the chargingType
	     */
	    public String getChargingType() {
	        return chargingType;
	    }

	    /**
	     * @param chargingType the chargingType to set
	     */
	    public void setChargingType(String chargingType) {
	        this.chargingType = chargingType;
	    }

	    /**
	     * @return the vCode
	     */
	    public String getvCode() {
	        return vCode;
	    }

	    /**
	     * @param vCode the vCode to set
	     */
	    public void setvCode(String vCode) {
	        this.vCode = vCode;
	    }

	    /**
	     * @return the callbackResp
	     */
	    public String getCallbackResp() {
	        return callbackResp;
	    }

	    /**
	     * @param callbackResp the callbackResp to set
	     */
	    public void setCallbackResp(String callbackResp) {
	        this.callbackResp = callbackResp;
	    }

	    /**
	     * @return the chargigTime
	     */
	    public Timestamp getChargigTime() {
	        return chargigTime;
	    }

	    /**
	     * @param chargigTime the chargigTime to set
	     */
	    public void setChargigTime(Timestamp chargigTime) {
	        this.chargigTime = chargigTime;
	    }

	    /**
	     * @return the process
	     */
	    public int getProcess() {
	        return process;
	    }

	    /**
	     * @param process the process to set
	     */
	    public void setProcess(int process) {
	        this.process = process;
	    }

	    /**
	     * @return the airtelCallbackId
	     */
	    public String getAirtelCallbackId() {
	        return airtelCallbackId;
	    }

	    /**
	     * @param airtelCallbackId the airtelCallbackId to set
	     */
	    public void setAirtelCallbackId(String airtelCallbackId) {
	        this.airtelCallbackId = airtelCallbackId;
	    }
		public String getReferer() {
			return referer;
		}
		public void setReferer(String referer) {
			this.referer = referer;
		}

		public String getSystemId() {
			return systemId;
		}

		public void setSystemId(String systemId) {
			this.systemId = systemId;
		}

		public String getAdnetworkName() {
			return adnetworkName;
		}

		public void setAdnetworkName(String adnetworkName) {
			this.adnetworkName = adnetworkName;
		}
	    
	    
	    
	    
}
