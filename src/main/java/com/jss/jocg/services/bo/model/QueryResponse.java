package com.jss.jocg.services.bo.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QueryResponse implements Serializable{

	String subscriberId;
	Long msisdn;
	Integer returnCode;//0=success, 1=failed
    String returnMessage;
    String serviceId;
    String transactionID;
	List<QRSubscribedPack> subscribedPacks;
	List<QRSubscribedPack> pendingPacks;
	QRCustomerStatus customerStatus;
	QRLastTransaction lastTransactionDetails;
    	
	
	public QueryResponse(){
		subscribedPacks=new ArrayList<>();
		pendingPacks=new ArrayList<>();
		
	}


	public Long getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}


	public Integer getReturnCode() {
		return returnCode;
	}


	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}


	public String getReturnMessage() {
		return returnMessage;
	}


	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}


	public String getServiceId() {
		return serviceId;
	}


	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}


	public String getTransactionID() {
		return transactionID;
	}


	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}


	public List<QRSubscribedPack> getSubscribedPacks() {
		return subscribedPacks;
	}


	public void setSubscribedPacks(List<QRSubscribedPack> subscribedPacks) {
		this.subscribedPacks = subscribedPacks;
	}

	public void addSubscribedPack(QRSubscribedPack subscribedPack) {
		if(this.subscribedPacks==null) this.subscribedPacks=new ArrayList<>();
		this.subscribedPacks.add(subscribedPack);
	}


	public List<QRSubscribedPack> getPendingPacks() {
		return pendingPacks;
	}


	public void setPendingPacks(List<QRSubscribedPack> pendingPacks) {
		this.pendingPacks = pendingPacks;
	}

	public void addPendingPacks(QRSubscribedPack pendingPacks) {
		if(this.pendingPacks==null) this.pendingPacks=new ArrayList<>();
		this.pendingPacks.add(pendingPacks);
	}

	public QRCustomerStatus getCustomerStatus() {
		return customerStatus;
	}


	public void setCustomerStatus(QRCustomerStatus customerStatus) {
		this.customerStatus = customerStatus;
	}


	public QRLastTransaction getLastTransactionDetails() {
		return lastTransactionDetails;
	}


	public void setLastTransactionDetails(QRLastTransaction lastTransactionDetails) {
		this.lastTransactionDetails = lastTransactionDetails;
	}
		
	
	
}
