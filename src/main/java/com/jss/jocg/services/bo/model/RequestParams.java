package com.jss.jocg.services.bo.model;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;

import com.jss.jocg.util.JocgString;

public class RequestParams implements Serializable {
	
	public static final int REQTYPE_QUERY=0;
	public static final int REQTYPE_VOUCHERCHARGE=1;
	public static final int REQTYPE_OPERATORCHARGE=2;
	public static final int REQTYPE_WALLETCHARGE=3;
	public static final int REQTYPE_DEACTIVATE=4;
	public static final int REQTYPE_ACTIVATEFREE=5;
	
	
	
	//Params for Pack Check Status : UnifiedApp|9643467350|MOBTV|<CheckSum>
	String platform;
	Long msisdn;
	String subscriberId;
	String serviceName;
	
	//Params for Pack Activation Operator Charging:  UnifiedApp|9643467350|OVOD|PK434|5|1|M|2357045061
	String operator;
	String packageName;
	Integer price;
	Integer validity;
	String networkType;
	
	//Params for Voucher Activation-UnifiedApp|9643467350|OVOD|PK434|5|1|M|2357045061
	
	
	String checksumKey;
	Integer requestType;
	
	Integer status;
	String statusDescp;
	
	public RequestParams(){
		this.requestType=REQTYPE_QUERY;
	}
	
	public boolean validateKey(String validationkey){
		Long receivedCheckSum=JocgString.strToLong(this.getChecksumKey(), 0);
		Long generatedChecksum=JocgString.generateChecksum(generateChecksumString(), validationkey);
		System.out.println(this.getMsisdn()+"-Received Checksum "+receivedCheckSum+", generated checkSum "+generatedChecksum);
		return ( receivedCheckSum.longValue() == generatedChecksum.longValue())?true:false;
	}
	
	private String generateChecksumString(){
		StringBuilder sb=new StringBuilder();
		try{
			switch(this.getRequestType()){
			case REQTYPE_QUERY://Params for Pack Check Status-UnifiedApp|9643467350|MOBTV|<CheckSum>
								sb.append(this.getPlatform()).append("|");
								sb.append(this.getSubscriberId()).append("|");
								sb.append(this.getServiceName()).append("|");
								break;
			case REQTYPE_VOUCHERCHARGE:
								
								break;
			case REQTYPE_OPERATORCHARGE:
				break;
			case REQTYPE_WALLETCHARGE:
				break;
			case REQTYPE_DEACTIVATE:
				break;
			}
		}catch(Exception e){
			
			e.printStackTrace();
		}
		return sb.toString();
	}

	public String getPlatform() {
		return platform;
	}


	public void setPlatform(String platform) {
		this.platform = platform;
	}


	public Long getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}


	public String getSubscriberId() {
		return subscriberId;
	}


	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}


	public String getServiceName() {
		return serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getOperator() {
		return operator;
	}


	public void setOperator(String operator) {
		this.operator = operator;
	}


	public String getPackageName() {
		return packageName;
	}


	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}


	public Integer getPrice() {
		return price;
	}


	public void setPrice(Integer price) {
		this.price = price;
	}


	public Integer getValidity() {
		return validity;
	}


	public void setValidity(Integer validity) {
		this.validity = validity;
	}


	public String getNetworkType() {
		return networkType;
	}


	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}


	public String getChecksumKey() {
		return checksumKey;
	}


	public void setChecksumKey(String checksumKey) {
		this.checksumKey = checksumKey;
	}


	public Integer getRequestType() {
		return requestType;
	}


	public void setRequestType(Integer requestType) {
		this.requestType = requestType;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getStatusDescp() {
		return statusDescp;
	}


	public void setStatusDescp(String statusDescp) {
		this.statusDescp = statusDescp;
	}
	
	
	
	
}
