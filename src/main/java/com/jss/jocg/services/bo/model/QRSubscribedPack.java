package com.jss.jocg.services.bo.model;

import org.springframework.format.annotation.DateTimeFormat;

public class QRSubscribedPack {
	/**
                 [expiryDate] => 2017-11-17
            [operatorId] => OVOD
            [packId] => VF_FOC
            [packPeriod] => 
            [packType] => 
            [packValidity] => 90
            [periodDate] => 
            [purchasePrice] => 0
            [purchaseTime] => 2017-08-19 20:29:07
            [renewalDate] => 2017-11-17
            [renewalYesNo] => Y

	 */
	String subRegId;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date expiryDate;
	String operatorId;
	String packId;
	Integer packValidity;
	String validityUnit;
	Double purchasePrice;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date purchaseTime;
	@DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss")
	java.util.Date renewalDate;
	String renewalYesNo;
	
	public java.util.Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(java.util.Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getOperatorId() {
		return operatorId;
	}
	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}
	public String getPackId() {
		return packId;
	}
	public void setPackId(String packId) {
		this.packId = packId;
	}
	public Integer getPackValidity() {
		return packValidity;
	}
	public void setPackValidity(Integer packValidity) {
		this.packValidity = packValidity;
	}
	public Double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public java.util.Date getPurchaseTime() {
		return purchaseTime;
	}
	public void setPurchaseTime(java.util.Date purchaseTime) {
		this.purchaseTime = purchaseTime;
	}
	public java.util.Date getRenewalDate() {
		return renewalDate;
	}
	public void setRenewalDate(java.util.Date renewalDate) {
		this.renewalDate = renewalDate;
	}
	public String getRenewalYesNo() {
		return renewalYesNo;
	}
	public void setRenewalYesNo(String renewalYesNo) {
		this.renewalYesNo = renewalYesNo;
	}
	public String getSubRegId() {
		return subRegId;
	}
	public void setSubRegId(String subRegId) {
		this.subRegId = subRegId;
	}
	public String getValidityUnit() {
		return validityUnit;
	}
	public void setValidityUnit(String validityUnit) {
		this.validityUnit = validityUnit;
	}
	
}
