package com.jss.jocg.services.bo;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.charging.handlers.PayTMIndiaChargingHandler;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class CDRBuilder {
	private static final Logger logger = LoggerFactory
			.getLogger(CDRBuilder.class);
	
	 @Autowired public SubscriptionManager subMgr;
	 @Autowired public JocgCacheManager jocgCache;
	 
	 @Autowired ChargingCDRRepository chargingCDRDao;
	 @Autowired SubscriberRepository subscriberDao;
	 
	public CDRBuilder(){
		
	}
	
	public Subscriber buildSubscriber(ChargingCDR newCDR,JocgRequest newReq,HttpServletRequest requestContext){
		String logHeader="buildSubscriber()::{"+newReq.getSubscriberId()+","+newReq.getMsisdn()+"} ::";
		Subscriber sub=null;
		try{
		if(newReq.getSubscriber()!=null && newReq.getSubscriber().getId()!=null){
			sub=newReq.getSubscriber();
		}else{
			if(newCDR.getMsisdn()!=null && newCDR.getMsisdn().intValue()>910){
			List<Subscriber> subList=subscriberDao.findByMsisdnAndServiceId(newCDR.getMsisdn(), newCDR.getServiceId());
			for(Subscriber s:subList){
				if(sub==null) sub=s;
				else if(sub.getSubRequestDate().before(s.getSubRequestDate())) sub=s;
			}
			}else if(newCDR.getSubscriberId()!=null && !"NA".equalsIgnoreCase(newCDR.getSubscriberId()) && newCDR.getSubscriberId().length()>0){
				List<Subscriber> subList=subscriberDao.findBySubscriberIdAndServiceName(newCDR.getSubscriberId(), newCDR.getServiceName());
				for(Subscriber s:subList){
					if(sub==null) sub=s;
					else if(sub.getSubRequestDate().before(s.getSubRequestDate())) sub=s;
				}
			}
			if(sub==null || sub.getId()==null){
				sub=new Subscriber();
			}
		}
		}catch(Exception e){
			logger.error(logHeader+" Error occured while searching subscriber. Creating new Subscriber entry.");
			sub=new Subscriber();
		}
		//Subscriber sub=(newReq.getSubscriber()!=null)?newReq.getSubscriber():new Subscriber();
		
		try{
			//logger.debug(logHeader+" Step 1 "+newCDR);
			sub.setSubscriberId(newCDR.getSubscriberId());
			sub.setMsisdn(newCDR.getMsisdn());
			sub.setTransactionCategory(newReq.getChargingCategory());
			//logger.debug(logHeader+" Step 2");
			if(newReq.getSourceInterface()!=null && newReq.getSourceInterface().getChintfid()>0){
				sub.setSourceNetworkId(newReq.getSourceInterface().getChintfid());
				sub.setSourceNetworkCode(newReq.getSourceInterface().getOpcode());
				sub.setSourceNetworkName(newReq.getSourceInterface().getName());
			}
			//logger.debug(logHeader+" Step 3");
			if(newReq.getCircle()!=null && newReq.getCircle().getCircleid()>0){
				sub.setCircleId(newReq.getCircle().getCircleid());
				sub.setCircleCode(newReq.getCircle().getCirclecode());
				sub.setCircleName(newReq.getCircle().getCirclename());
			}
			//logger.debug(logHeader+"Step 1");
			sub.setRequestIP(newReq.getRequestParams().getSourceIP());
			if(newReq.getChargingOperator()!=null && newReq.getChargingOperator().getChintfid()>0){
				sub.setParentChintfId(newReq.getChargingOperator().getPchintfid());
				sub.setUseParentChargingHandler(newReq.getChargingOperator().getUseparenthandler());
				sub.setChargingInterfaceId(newReq.getChargingOperator().getChintfid());
				sub.setOperatorName(newReq.getChargingOperator().getName());
				sub.setCountryName(newReq.getChargingOperator().getCountry());
				sub.setCurrency(newCDR.getCurrencyName());
				sub.setGmtDifference(newReq.getChargingOperator().getGmtDiff());
				sub.setChargingInterfaceType(newReq.getChargingOperator().getChintfType());
				sub.setRenewalCategory(newReq.getChargingOperator().getRenewalType());
				sub.setRenewalRequired(newReq.getChargingOperator().getRenewalRequired()>0?true:false);
			}
			//logger.debug(logHeader+"Step 2");
			if(newReq.getPackageDetail()!=null && newReq.getPackageDetail().getPkgid()>0){
				sub.setPackageId(newReq.getPackageDetail().getPkgid());
				sub.setPackageName(newReq.getPackageDetail().getPkgname());
				sub.setServiceId(newReq.getPackageDetail().getServiceid());
				sub.setPackagePrice(newReq.getPackageDetail().getPricepoint());
				sub.setValidityCategory(newReq.getPackageDetail().getValidityCatg());
				//Search Service by Id
				Service service=jocgCache.getServicesById(sub.getServiceId());
				if(service!=null && service.getServiceid()>0){
					sub.setServiceName(service.getServiceName());
					sub.setServiceCategory(service.getServiceCatg());
				}else{
					sub.setServiceName("NA");
					sub.setServiceCategory("NA");
				}
			}
			//logger.debug(logHeader+"Step 3");
			if(newReq.getPkgChintfMap()!=null && newReq.getPkgChintfMap().getPkgchintfid()>0){
				sub.setPackageChargingInterfaceMapId(newReq.getPkgChintfMap().getPkgchintfid());
				if(newReq.getChargingPrice()!=null && newReq.getChargingPrice().getPpid()>0){
					sub.setPricepointId(newReq.getChargingPrice().getPpid());
					sub.setOperatorPricePoint(newReq.getChargingPrice().getPricePoint());
					sub.setValidity(newReq.getChargingPrice().getValidity());
					sub.setValidityUnit(newReq.getChargingPrice().getValidityType());
					sub.setOperatorServiceKey(newReq.getChargingPrice().getOperatorKey());
					sub.setOperatorParams(newReq.getChargingPrice().getOpParams());
					sub.setContentType(newReq.getChargingPrice().getCtype());
				
				}else{
					sub.setPricepointId(newReq.getPkgChintfMap().getOpPpid());
				}
			}
			//logger.debug(logHeader+"Step 4");
			if(newCDR.getChargedAmount()!=null){
				sub.setChargedAmount(newCDR.getChargedAmount());
			}else{
				sub.setChargedAmount(0D);
			}
			sub.setChargingCategory(newCDR.getChargingCategory());
			sub.setSubRequestDate(newCDR.getRequestDate());
			sub.setSubChargedDate(newCDR.getChargingDate());
			sub.setValidityStartDate(newCDR.getValidityStartDate());
			sub.setValidityEndDate(newCDR.getValidityEndDate());
			sub.setPendingDate(newCDR.getRequestDate());
			sub.setSubSessionRevenue(sub.getChargedAmount());
			sub.setJocgCDRId(newCDR.getId());
			sub.setJocgTransId(newCDR.getJocgTransId());
			sub.setCgTransactionId(newCDR.getCgTransactionId());
			sub.setCgStatusCode(newCDR.getCgStatusCode());
			sub.setCgResponseTime(newCDR.getCgResponseTime());
			//Set charged operator key to the price point requested by default, shall be updated on callback
			sub.setChargedOperatorKey(sub.getOperatorServiceKey());
			sub.setChargedOperatorParams(sub.getOperatorParams());
			sub.setStatus(getSubscriberStatus(newCDR.getStatus()));
			sub.setChargingMode(newReq.getRequestParams().getRequestCategory());
			sub.setActivationSource(newReq.getRequestParams().getRequestCategory());
			//logger.debug(logHeader+"Step 5");
			if(newReq.getCampaign()!=null && newReq.getCampaign().getCampaignid()>0){
				sub.setCampaignId(newReq.getCampaign().getCampaignid());
				sub.setCampaignName(newReq.getCampaign().getCampaignname());
				sub.setPublisherId(newReq.getRequestParams().getPublisherId());
				TrafficSource ts=jocgCache.getTrafficSourceById(newReq.getCampaign().getTsourceid());
				if(ts!=null && ts.getTsourceid()>0){
					sub.setTrafficSourceId(newReq.getCampaign().getTsourceid());
					sub.setTrafficSourceName(ts.getSourcename());
				}else{
					sub.setTrafficSourceId(newReq.getCampaign().getTsourceid());
					sub.setTrafficSourceName("NA");
				}
			}else{
				sub.setCampaignId(0);
			}
			//logger.debug(logHeader+"Step 6");
			try{
				if(newReq.getVoucher()!=null && newReq.getVoucher().getCouponid()>0){
					sub.setVoucherId(newReq.getVoucher().getCouponid());
					sub.setVoucherName(newReq.getVoucher().getVoucherId());
					sub.setVoucherVendor(""+newReq.getVoucher().getVendorId());
					sub.setVoucherType(newReq.getVoucher().getVoucherType());
					sub.setVoucherDiscountType(newReq.getVoucher().getDiscountType());
					sub.setVoucherDiscount((double)newReq.getVoucher().getDiscuountValue());
					sub.setVoucherChargedAmount(newCDR.getChargedAmount());
				}
			}catch(Exception ee){}
			//logger.debug(logHeader+"Step 7");
			Platform plt=jocgCache.getPlatformConfig(newReq.getRequestParams().getPlatformName());
			if(plt!=null && plt.getPlatformid()>0){
				sub.setPlatformId(plt.getPlatform());
				sub.setSystemId(plt.getSystemid());
			}else{
				sub.setPlatformId(newReq.getRequestParams().getPlatformName());
				sub.setSystemId("NA");
			}
			plt=null;
			//logger.debug(logHeader+"Step 8");
			sub.setUsedContentCount(0);
			sub.setAllowedContentCount(1);
			sub.setContentProffileType("UNLIMITED");
			if(sub.getId()!=null)
				sub=subMgr.save(sub);
			else
				sub=subMgr.insert(sub);
			//logger.debug(logHeader+"Step 9");
			}catch(Exception e){
				e.printStackTrace();
			}
		return sub;
		
	}
	public Subscriber buildSubscriber(ChargingCDR newCDR){
		String logHeader="buildSubscriber(ChargingCDR)::{"+newCDR.getSubscriberId()+","+newCDR.getMsisdn()+"} ::";
		Subscriber sub=null;
		try{
			if(newCDR.getMsisdn()!=null && newCDR.getMsisdn().intValue()>910){
			List<Subscriber> subList=subscriberDao.findByMsisdnAndServiceId(newCDR.getMsisdn(), newCDR.getServiceId());
			for(Subscriber s:subList){
				if(sub==null) sub=s;
				else if(sub.getSubRequestDate().before(s.getSubRequestDate())) sub=s;
			}
			}else if(newCDR.getSubscriberId()!=null && !"NA".equalsIgnoreCase(newCDR.getSubscriberId()) && newCDR.getSubscriberId().length()>0){
				List<Subscriber> subList=subscriberDao.findBySubscriberIdAndServiceName(newCDR.getSubscriberId(), newCDR.getServiceName());
				for(Subscriber s:subList){
					if(sub==null) sub=s;
					else if(sub.getSubRequestDate().before(s.getSubRequestDate())) sub=s;
				}
			}
			if(sub==null || sub.getId()==null){
				sub=new Subscriber();
			}
		
		}catch(Exception e){
			logger.error(logHeader+" Error occured while searching subscriber. Creating new Subscriber entry.");
			sub=new Subscriber();
		}
		//Subscriber sub=(newReq.getSubscriber()!=null)?newReq.getSubscriber():new Subscriber();
		
		try{
			//logger.debug(logHeader+" Step 1 "+newCDR);
			sub.setSubscriberId(newCDR.getSubscriberId());
			sub.setMsisdn(newCDR.getMsisdn());
			sub.setTransactionCategory(newCDR.getChargingCategory());
			//logger.debug(logHeader+" Step 2");
			if(newCDR.getSourceNetworkId()!=null && newCDR.getSourceNetworkId()>0){
				sub.setSourceNetworkId(newCDR.getSourceNetworkId());
				sub.setSourceNetworkCode(newCDR.getSourceNetworkCode());
				sub.setSourceNetworkName(newCDR.getSourceNetworkName());
			}
			//logger.debug(logHeader+" Step 3");
			if(newCDR.getCircleId()!=null && newCDR.getCircleId()>0){
				sub.setCircleId(newCDR.getCircleId());
				sub.setCircleCode(newCDR.getCircleCode());
				sub.setCircleName(newCDR.getCircleName());
			}
			//logger.debug(logHeader+"Step 1");
			sub.setRequestIP("");
			if(newCDR.getChargingInterfaceId()!=null && newCDR.getChargingInterfaceId()>0){
				//sub.setParentChintfId();
				sub.setUseParentChargingHandler(JocgString.boolToInt(newCDR.getUseParentHandler()));
				sub.setChargingInterfaceId(newCDR.getChargingInterfaceId());
				sub.setOperatorName(newCDR.getChargingInterfaceName());
				sub.setCountryName(newCDR.getCountryName());
				sub.setCurrency(newCDR.getCurrencyName());
				sub.setGmtDifference(newCDR.getGmtDiff());
				sub.setChargingInterfaceType(newCDR.getChargingInterfaceType());
				sub.setRenewalCategory(newCDR.getRenewalCategory());
				sub.setRenewalRequired(newCDR.getRenewalRequired());
			}
			//logger.debug(logHeader+"Step 2");
			if(newCDR.getPackageId()!=null && newCDR.getPackageId()>0){
				sub.setPackageId(newCDR.getPackageId());
				sub.setPackageName(newCDR.getPackageName());
				sub.setServiceId(newCDR.getServiceId());
				sub.setPackagePrice(newCDR.getPackagePrice());
				sub.setValidityCategory(newCDR.getValidityCategory());
				//Search Service by Id
				
				sub.setServiceName(newCDR.getServiceName());
				sub.setServiceCategory(newCDR.getServiceCategory());
				
			}
			//logger.debug(logHeader+"Step 3");
			if(newCDR.getPackageChintfId()!=null && newCDR.getPackageChintfId()>0){
				sub.setPackageChargingInterfaceMapId(newCDR.getPackageChintfId());
				if(newCDR.getChargedPpid()!=null &&newCDR.getChargedPpid()>0){
					sub.setPricepointId(newCDR.getPricePointId());
					sub.setOperatorPricePoint(newCDR.getOperatorPricePoint());
					sub.setValidity(newCDR.getValidityPeriod());
					sub.setValidityUnit(newCDR.getValidityUnit());
					sub.setOperatorServiceKey(newCDR.getMppOperatorKey());
					sub.setOperatorParams(newCDR.getMppOperatorParams());
					sub.setContentType(newCDR.getContentType());
				
				}else{
					sub.setPricepointId(newCDR.getPricePointId());
				}
			}
			//logger.debug(logHeader+"Step 4");
			if(newCDR.getChargedAmount()!=null){
				sub.setChargedAmount(newCDR.getChargedAmount());
			}else{
				sub.setChargedAmount(0D);
			}
			sub.setChargingCategory(newCDR.getChargingCategory());
			sub.setSubRequestDate(newCDR.getRequestDate());
			sub.setSubChargedDate(newCDR.getChargingDate());
			sub.setValidityStartDate(newCDR.getValidityStartDate());
			sub.setValidityEndDate(newCDR.getValidityEndDate());
			sub.setPendingDate(newCDR.getRequestDate());
			sub.setSubSessionRevenue(sub.getChargedAmount());
			sub.setJocgCDRId(newCDR.getId());
			sub.setJocgTransId(newCDR.getJocgTransId());
			sub.setCgTransactionId(newCDR.getCgTransactionId());
			sub.setCgStatusCode(newCDR.getCgStatusCode());
			sub.setCgResponseTime(newCDR.getCgResponseTime());
			//Set charged operator key to the price point requested by default, shall be updated on callback
			sub.setChargedOperatorKey(sub.getOperatorServiceKey());
			sub.setChargedOperatorParams(sub.getOperatorParams());
			sub.setStatus(getSubscriberStatus(newCDR.getStatus()));
			sub.setChargingMode(newCDR.getRequestCategory());
			sub.setActivationSource(newCDR.getRequestCategory());
			//logger.debug(logHeader+"Step 5");
			if(newCDR.getCampaignId()!=null && newCDR.getCampaignId()>0){
				sub.setCampaignId(newCDR.getCampaignId());
				sub.setCampaignName(newCDR.getCampaignName());
				sub.setPublisherId(newCDR.getPublisherId());
			}else{
				sub.setCampaignId(0);
				sub.setCampaignName("NA");
			}
			if(newCDR.getTrafficSourceId()!=null && newCDR.getTrafficSourceId()>0){
				sub.setTrafficSourceId(newCDR.getTrafficSourceId());
				sub.setTrafficSourceName(newCDR.getTrafficSourceName());
			}else{
				sub.setTrafficSourceId(0);
				sub.setTrafficSourceName("NA");
			}
			
			
			//logger.debug(logHeader+"Step 6");
			try{
				if(newCDR.getVoucherId()!=null && newCDR.getVoucherId()>0L){
					sub.setVoucherId(newCDR.getVoucherId());
					sub.setVoucherName(newCDR.getVoucherName());
					sub.setVoucherVendor(""+newCDR.getVoucherVendorName());
					sub.setVoucherType(newCDR.getVoucherType());
					sub.setVoucherDiscountType(newCDR.getVoucherDiscountType());
					sub.setVoucherDiscount((double)newCDR.getVoucherDiscount());
					sub.setVoucherChargedAmount(newCDR.getChargedAmount());
				}
			}catch(Exception ee){}
		
				sub.setPlatformId(newCDR.getPlatformName());
				sub.setSystemId(newCDR.getSystemId());
			
			//logger.debug(logHeader+"Step 8");
			sub.setUsedContentCount(0);
			sub.setAllowedContentCount(1);
			sub.setContentProffileType("UNLIMITED");
			if(sub.getId()!=null)
				sub=subMgr.save(sub);
			else
				sub=subMgr.insert(sub);
			//logger.debug(logHeader+"Step 9");
			}catch(Exception e){
				e.printStackTrace();
			}
		return sub;
		
	}
	
	public Subscriber updateSubscriber(ChargingCDR newCDR,Subscriber sub){
		String logHeader="buildSubscriber(ChargingCDR)::{"+newCDR.getSubscriberId()+","+newCDR.getMsisdn()+"} ::";
				
		try{
			//logger.debug(logHeader+" Step 1 "+newCDR);
			sub.setSubscriberId(newCDR.getSubscriberId());
			sub.setMsisdn(newCDR.getMsisdn());
			sub.setTransactionCategory(newCDR.getChargingCategory());
			//logger.debug(logHeader+" Step 2");
			if(newCDR.getSourceNetworkId()!=null && newCDR.getSourceNetworkId()>0){
				sub.setSourceNetworkId(newCDR.getSourceNetworkId());
				sub.setSourceNetworkCode(newCDR.getSourceNetworkCode());
				sub.setSourceNetworkName(newCDR.getSourceNetworkName());
			}
			//logger.debug(logHeader+" Step 3");
			if(newCDR.getCircleId()!=null && newCDR.getCircleId()>0){
				sub.setCircleId(newCDR.getCircleId());
				sub.setCircleCode(newCDR.getCircleCode());
				sub.setCircleName(newCDR.getCircleName());
			}
			//logger.debug(logHeader+"Step 1");
			sub.setRequestIP("");
			if(newCDR.getChargingInterfaceId()!=null && newCDR.getChargingInterfaceId()>0){
				//sub.setParentChintfId();
				sub.setUseParentChargingHandler(JocgString.boolToInt(newCDR.getUseParentHandler()));
				sub.setChargingInterfaceId(newCDR.getChargingInterfaceId());
				sub.setOperatorName(newCDR.getChargingInterfaceName());
				sub.setCountryName(newCDR.getCountryName());
				sub.setCurrency(newCDR.getCurrencyName());
				sub.setGmtDifference(newCDR.getGmtDiff());
				sub.setChargingInterfaceType(newCDR.getChargingInterfaceType());
				sub.setRenewalCategory(newCDR.getRenewalCategory());
				sub.setRenewalRequired(newCDR.getRenewalRequired());
			}
			//logger.debug(logHeader+"Step 2");
			if(newCDR.getPackageId()!=null && newCDR.getPackageId()>0){
				sub.setPackageId(newCDR.getPackageId());
				sub.setPackageName(newCDR.getPackageName());
				sub.setServiceId(newCDR.getServiceId());
				sub.setPackagePrice(newCDR.getPackagePrice());
				sub.setValidityCategory(newCDR.getValidityCategory());
				//Search Service by Id
				
				sub.setServiceName(newCDR.getServiceName());
				sub.setServiceCategory(newCDR.getServiceCategory());
				
			}
			//logger.debug(logHeader+"Step 3");
			if(newCDR.getPackageChintfId()!=null && newCDR.getPackageChintfId()>0){
				sub.setPackageChargingInterfaceMapId(newCDR.getPackageChintfId());
				if(newCDR.getChargedPpid()!=null &&newCDR.getChargedPpid()>0){
					sub.setPricepointId(newCDR.getPricePointId());
					sub.setOperatorPricePoint(newCDR.getOperatorPricePoint());
					sub.setValidity(newCDR.getValidityPeriod());
					sub.setValidityUnit(newCDR.getValidityUnit());
					sub.setOperatorServiceKey(newCDR.getMppOperatorKey());
					sub.setOperatorParams(newCDR.getMppOperatorParams());
					sub.setContentType(newCDR.getContentType());
				
				}else{
					sub.setPricepointId(newCDR.getPricePointId());
				}
			}
			//logger.debug(logHeader+"Step 4");
			if(newCDR.getChargedAmount()!=null){
				sub.setChargedAmount(newCDR.getChargedAmount());
			}else{
				sub.setChargedAmount(0D);
			}
			sub.setChargingCategory(newCDR.getChargingCategory());
			sub.setSubRequestDate(newCDR.getRequestDate());
			sub.setSubChargedDate(newCDR.getChargingDate());
			sub.setValidityStartDate(newCDR.getValidityStartDate());
			sub.setValidityEndDate(newCDR.getValidityEndDate());
			sub.setPendingDate(newCDR.getRequestDate());
			sub.setSubSessionRevenue(sub.getChargedAmount());
			sub.setJocgCDRId(newCDR.getId());
			sub.setJocgTransId(newCDR.getJocgTransId());
			sub.setCgTransactionId(newCDR.getCgTransactionId());
			sub.setCgStatusCode(newCDR.getCgStatusCode());
			sub.setCgResponseTime(newCDR.getCgResponseTime());
			//Set charged operator key to the price point requested by default, shall be updated on callback
			sub.setChargedOperatorKey(sub.getOperatorServiceKey());
			sub.setChargedOperatorParams(sub.getOperatorParams());
			sub.setStatus(getSubscriberStatus(newCDR.getStatus()));
			sub.setChargingMode(newCDR.getRequestCategory());
			sub.setActivationSource(newCDR.getRequestCategory());
			//logger.debug(logHeader+"Step 5");
			if(newCDR.getCampaignId()!=null && newCDR.getCampaignId()>0){
				sub.setCampaignId(newCDR.getCampaignId());
				sub.setCampaignName(newCDR.getCampaignName());
				sub.setPublisherId(newCDR.getPublisherId());
			}else{
				sub.setCampaignId(0);
				sub.setCampaignName("NA");
			}
			if(newCDR.getTrafficSourceId()!=null && newCDR.getTrafficSourceId()>0){
				sub.setTrafficSourceId(newCDR.getTrafficSourceId());
				sub.setTrafficSourceName(newCDR.getTrafficSourceName());
			}else{
				sub.setTrafficSourceId(0);
				sub.setTrafficSourceName("NA");
			}
			
			
			//logger.debug(logHeader+"Step 6");
			try{
				if(newCDR.getVoucherId()!=null && newCDR.getVoucherId()>0L){
					sub.setVoucherId(newCDR.getVoucherId());
					sub.setVoucherName(newCDR.getVoucherName());
					sub.setVoucherVendor(""+newCDR.getVoucherVendorName());
					sub.setVoucherType(newCDR.getVoucherType());
					sub.setVoucherDiscountType(newCDR.getVoucherDiscountType());
					sub.setVoucherDiscount((double)newCDR.getVoucherDiscount());
					sub.setVoucherChargedAmount(newCDR.getChargedAmount());
				}
			}catch(Exception ee){}
		
				sub.setPlatformId(newCDR.getPlatformName());
				sub.setSystemId(newCDR.getSystemId());
			
			//logger.debug(logHeader+"Step 8");
			sub.setUsedContentCount(0);
			sub.setAllowedContentCount(1);
			sub.setContentProffileType("UNLIMITED");
			if(sub.getId()!=null)
				sub=subMgr.save(sub);
			else
				sub=subMgr.insert(sub);
			//logger.debug(logHeader+"Step 9");
			}catch(Exception e){
				e.printStackTrace();
			}
		return sub;
		
	}
	
	public ChargingCDR buildCDR(JocgRequest newReq,HttpServletRequest requestContext){
		String logHeader="buildCDR()::{"+newReq.getSubscriberId()+","+newReq.getMsisdn()+"} ::";
		ChargingCDR newCDR=new ChargingCDR();
		try{
			//newCDR.setDeviceDetails(parseDevice(logHeader,model));
			//logger.debug(logHeader+" Step 1");
			newCDR.setJocgTransId(newReq.getJocgTransactionId());
			if(requestContext!=null){
					newCDR.setUserAgent(requestContext.getHeader("user-agent"));
		    }
			newCDR.setRequestCategory(newReq.getRequestParams().getRequestCategory());
			newCDR.setRequestType(newReq.getRequestType());
			newCDR.setSubscriberId(newReq.getSubscriberId());
			newCDR.setMsisdn(newReq.getMsisdn());
			Platform plt=null;
			try{
				plt=jocgCache.getPlatformConfig(newReq.getRequestParams().getPlatformName());
			}catch(Exception e){}
			if(plt==null){
				try{
					Iterator<String> i=jocgCache.getPlatformConfig().keySet().iterator();
					String key="";
					while(i.hasNext()){
						key=i.next();
						if(key.equalsIgnoreCase(newReq.getRequestParams().getPlatformName())){
							plt=jocgCache.getPlatformConfig(key);
							break;
						}
					}
				}catch(Exception e){}
			}
			//logger.debug(logHeader+" Step 2");
			if(plt!=null){
				newCDR.setPlatformName(plt.getPlatform());
				newCDR.setSystemId(plt.getSystemid());
			}else
				newCDR.setPlatformName(newReq.getRequestParams().getPlatformName());
			//logger.debug(logHeader+" Step 3");
			newCDR.setRqPackageName(newReq.getRequestParams().getPackageName());
			newCDR.setRqPackagePrice(newReq.getRequestParams().getPackagePrice());
			newCDR.setRqNetworkType(newReq.getRequestParams().getNetworkType());
			newCDR.setRqVoucherCode(newReq.getRequestParams().getVoucherCode());
			newCDR.setRqAmountToBeCharged(newReq.getRequestParams().getAmountToBeCharged());
			newCDR.setRqVoucherVendor(newReq.getRequestParams().getVoucherVendor());
			//logger.debug(logHeader+" Step 4");
			if(newReq.getSourceInterface()!=null){
				newCDR.setSourceNetworkId(newReq.getSourceInterface().getChintfid());
				newCDR.setSourceNetworkName(newReq.getSourceInterface().getName());
				newCDR.setSourceNetworkCode(newReq.getSourceInterface().getOpcode());
			}
			//logger.debug(logHeader+" Step 5");
			if(newReq.getChargingOperator()!=null){
				newCDR.setChargingInterfaceId(newReq.getChargingOperator().getChintfid());
				newCDR.setChargingInterfaceName(newReq.getChargingOperator().getName());
				newCDR.setCountryName(newReq.getChargingOperator().getCountry());
				newCDR.setCurrencyName("INR");
				newCDR.setGmtDiff(newReq.getChargingOperator().getGmtDiff());
				newCDR.setChargingInterfaceType(newReq.getChargingOperator().getChintfType());
				
				if(newReq.getChargingOperator().getUseparenthandler()>0){
					newCDR.setUseParentHandler(true);
					newCDR.setAggregatorId(newReq.getChargingOperator().getPchintfid());
				}else{
					newCDR.setUseParentHandler(false);
					newCDR.setAggregatorId(0);
				}
				
			}
			//logger.debug(logHeader+" Step 6");
			if(newReq.getCircle()!=null){
				newCDR.setCircleId(newReq.getCircle().getCircleid());
				newCDR.setCircleName(newReq.getCircle().getCirclename());
				newCDR.setCircleCode(newReq.getCircle().getCirclecode());
			}
			//logger.debug(logHeader+" Step 7");
			if(newReq.getPackageDetail()!=null){
				newCDR.setPackageId(newReq.getPackageDetail().getPkgid());
				newCDR.setPackageName(newReq.getPackageDetail().getPkgname());
				newCDR.setPackagePrice(newReq.getPackageDetail().getPricepoint());
			}
			//logger.debug(logHeader+" Step 8");
			if(newReq.getService()!=null){
				newCDR.setServiceId(newReq.getService().getServiceid());
				newCDR.setServiceName(newReq.getService().getServiceName());
				newCDR.setServiceCategory(newReq.getService().getServiceCatg());
				
			}
			if(newReq.getPkgChintfMap()!=null){
				newCDR.setPackageChintfId(newReq.getPkgChintfMap().getPkgchintfid());
			}
			
			//logger.debug(logHeader+" Step 9");
			if(newReq.getChargingPrice()!=null){
				
				newCDR.setPricePointId(newReq.getChargingPrice().getPpid());
				newCDR.setOperatorPricePoint(newReq.getChargingPrice().getPricePoint());
				newCDR.setValidityCategory(newReq.getChargingPrice().getValidityType());
				newCDR.setValidityUnit(newReq.getChargingPrice().getValidityType());
				newCDR.setValidityPeriod(newReq.getChargingPrice().getValidity());
				newCDR.setMppOperatorKey(newReq.getChargingPrice().getOperatorKey());
				newCDR.setMppOperatorParams(newReq.getChargingPrice().getOpParams());
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(System.currentTimeMillis());
				newCDR.setRequestDate(cal.getTime());
				newCDR.setChargingDate(cal.getTime());
				newCDR.setSubRegDate(cal.getTime());
				newCDR.setValidityStartDate(cal.getTime());
				cal.add(Calendar.DAY_OF_MONTH, newCDR.getValidityPeriod());
				newCDR.setValidityEndDate(cal.getTime());
				//newCDR.setChargedAmount(newReq.getChargingPrice().getPricePoint());
			}
			
			//logger.debug(logHeader+" Step 10");
			try{
				logger.debug("Request Category "+newReq.getRequestParams().getRequestCategory()+", Voucher "+newReq.getVoucher()+", Voucher Id "+newReq.getVoucher().getCouponid());
				if(newReq.getVoucher()!=null && newReq.getVoucher().getCouponid()!=null && newReq.getVoucher().getCouponid()>0){
					newCDR.setVoucherId(newReq.getVoucher().getCouponid());
					newCDR.setVoucherName(newReq.getVoucher().getVoucherId());
					//this.voucherVendorId=0;
					newCDR.setVoucherVendorId(newReq.getVoucher().getVendorId());
					newCDR.setVoucherVendorName(""+newReq.getVoucher().getVendorId());//replace with voucher vendor name
					newCDR.setVoucherType(newReq.getVoucher().getVoucherType());
					newCDR.setVoucherDiscount((double)newReq.getVoucher().getDiscuountValue());
					newCDR.setVoucherDiscountType(newReq.getVoucher().getDiscountType());
					newCDR.setDiscountGiven(newReq.getChargingPrice().getPricePoint()-newReq.getChargedAmount());
					newCDR.setChargedAmount(newReq.getChargedAmount());
					
					if(newReq.getChargedPrice()!=null && newReq.getChargedPrice().getPpid()>0){
						newCDR.setChargedPpid(newReq.getChargedPrice().getPpid());
						newCDR.setChargedOperatorKey(newReq.getChargedPrice().getOperatorKey());
						newCDR.setChargedOperatorParams(newReq.getChargedPrice().getOpParams());
						newCDR.setChargedPricePoint(newReq.getChargedPrice().getPricePoint());
						newCDR.setChargedValidityPeriod(""+newReq.getChargedPrice().getValidity());
						newCDR.setFallbackCharged(newReq.getChargedPrice().getIsfallback()>0?true:false);
						
					}
					
				}else if("W".equalsIgnoreCase(newReq.getRequestParams().getRequestCategory())){
					//logger.debug("Setting charged Price point details "+newReq.getChargingPrice());
					newCDR.setChargedPpid(newReq.getChargingPrice().getPpid());
					newCDR.setChargedAmount(newReq.getChargingPrice().getPricePoint());
					newCDR.setChargedOperatorKey(newReq.getChargingPrice().getOperatorKey());
					newCDR.setChargedOperatorParams(newReq.getChargingPrice().getOpParams());
					newCDR.setChargedPricePoint(newReq.getChargingPrice().getPricePoint());
					newCDR.setChargedValidityPeriod(""+newReq.getChargingPrice().getValidity());
					newCDR.setFallbackCharged(newReq.getChargingPrice().getIsfallback()>0?true:false);
					//logger.debug("Charged Amount "+newCDR.getChargedAmount());
				}
			}catch(Exception ee){
				ee.printStackTrace();
			}
			//logger.debug(logHeader+" Step 11");
			newCDR.setRenewalRequired(newReq.getChargingOperator().getRenewalRequired()>0?true:false);
			newCDR.setRenewalCategory(newReq.getChargingOperator().getRenewalType());
			//logger.debug(logHeader+" Step 12");
			try{
				if(newReq.getCampaign()!=null && newReq.getCampaign().getCampaignid()>0){
					newCDR.setCampaignId(newReq.getCampaign().getCampaignid());
					newCDR.setCampaignName(newReq.getCampaign().getCampaignname());
					newCDR.setTrafficSourceToken(newReq.getAdNetworkToken());
					newCDR.setPublisherId(newReq.getRequestParams().getPublisherId());
				}
			
				//logger.debug(logHeader+" Step 13");
				if(newReq.getTrafficSource()!=null){
					newCDR.setTrafficSourceId(newReq.getTrafficSource().getTsourceid());
					newCDR.setTrafficSourceName(newReq.getTrafficSource().getSourcename());
				}
			}catch(Exception ee){}
			//logger.debug(logHeader+" Step 14");
			newCDR.setChargingCategory(newReq.getChargingCategory());
			newCDR.setDiscountGiven(newReq.getDiscountGiven());
			newCDR.setChargedAmount(newReq.getChargedAmount());
			//logger.debug(logHeader+" Step 15");
			newCDR.setStatus(newReq.getResponseObject().getResponseCode());
			newCDR.setStatusDescp(newReq.getResponseObject().getResponseDescription());
			newCDR.setActivatedFromZero(false);
			newCDR.setActivatedFromParking(false);
			newCDR.setActivatedFromGrace(false);
			if(newReq.getLandingPage()!=null){
				newCDR.setLandingPageScheduleId(newReq.getLandingPage().getLandingPageId());
			}
			if(newReq.getCgimageSchedule()!=null){
				newCDR.setCgImageId(newReq.getCgimageSchedule().getImgid());
			}
			newCDR.setChurn20Min(newReq.getChurn20Min());
			newCDR.setChurnSameDay(newReq.getChurnSameDay());
			newCDR.setNotifyTimeChurn(null);
			try{
				if(newReq.getResponseObject().getOtpPin()!=null){
					newCDR.setOtpPin(newReq.getResponseObject().getOtpPin());
					newCDR.setOtpStatus(1);
				}else{
					newCDR.setOtpPin("NA");
					newCDR.setOtpStatus(-1);
					
				}
			}catch(Exception ee){
				newCDR.setOtpPin("NA");
				newCDR.setOtpStatus(-1);
			}
			//logger.debug(logHeader+" Step 16 "+chargingCDRDao);
			newCDR=this.insert(newCDR);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return newCDR;
	}
	
	public Integer getSubscriberStatus(Integer cdrStatus){
		Integer subStatus=(cdrStatus==JocgStatusCodes.CHARGING_CGSUCCESS)?JocgStatusCodes.SUB_SUCCESS_CG:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_ALYSUBSCRIBED)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_CGCONSENT)?JocgStatusCodes.SUB_FAILED_CG:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_DUPLICATEREQUEST)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_EXCEPTION)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVCHINTF)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVCHPARAMS)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVCHURL)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVHANDLER)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVOPERATOR)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_LOWBALANCE)?JocgStatusCodes.SUB_FAILED_SDPCHARGING:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_PREVTRANSNOTEXPIRED)?JocgStatusCodes.SUB_FAILED_TRANS:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_SDPGRACE)?JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE:
			(cdrStatus==JocgStatusCodes.CHARGING_FAILED_SDPPARKING)?JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING:
			(cdrStatus==JocgStatusCodes.CHARGING_INPROCESS)?JocgStatusCodes.SUB_SUCCESS_CG:
			(cdrStatus==JocgStatusCodes.CHARGING_SDPGRACE)?JocgStatusCodes.SUB_SUCCESS_GRACE:
			(cdrStatus==JocgStatusCodes.CHARGING_SDPPARKING)?JocgStatusCodes.SUB_SUCCESS_PARKING:
				(cdrStatus==JocgStatusCodes.CHARGING_SUCCESS)?JocgStatusCodes.SUB_SUCCESS_ACTIVE:
					JocgStatusCodes.SUB_FAILED_TRANS;
						
				return subStatus;
	}
	
	
	public ChargingCDR getNewCDRInstance(String subregId,String jocgTransId,String requestCategory,String requestType,String subscriberId,Long msisdn,String platformName,String systemId,
			JocgRequest reqParams,ChargingInterface sourceNetwork,ChargingInterface chargingNetwork,PackageChintfmap pkgchintfMap,Circleinfo circle,
			PackageDetail pkg,Service service,ChargingPricepoint masterPP,ChargingPricepoint chargedPP,VoucherDetail voucher,
			String chargingCategory,Double discountAmount,Double chargedAmount,String referer,
			java.util.Date chargingDate,java.util.Date validityStartDate,java.util.Date validityEndDate,
			String deviceDetails,String deviceOs,String userAgent,TrafficSource adNetwork,TrafficCampaign campaign,String adNetworkToken,String publisherId,
			Integer notifyStatus,String notifyDescp,java.util.Date notifyTime,Integer notifyChurnStatus,java.util.Date notifyTimeChurn,
			java.util.Date notifyScheduleTime,Boolean churn20Min,Boolean churnSameDay,Boolean notifyChurn,LandingPageSchedule landingPage,Routing routingInfo,
			CgimageSchedule cgImage,Integer otpStatus,String otpPin,Integer status,String statusDescp,java.util.Date requestDate,
			String cgStatusCode,String cgStatusDescp,String cgTransactionId,java.util.Date cgResponseTime,
			String sdpStatusCode,String sdpStatusDescp,String sdpTransactionId,String sdpLifeCycleId,java.util.Date sdpResponseTime,
			java.util.Date parkingDate,java.util.Date parkingToActivationDate,
			Boolean activatedFromZero,Boolean activatedFromParking,Boolean activatedFromGrace){
			
		
		ChargingCDR cdr=new ChargingCDR();
		try{
		cdr.setSubRegId(JocgString.formatString(subregId,"NA"));
		cdr.setJocgTransId(JocgString.formatString(jocgTransId,"NA"));
		cdr.setRequestCategory(JocgString.formatString(requestCategory,"NA"));
		cdr.setRequestType(JocgString.formatString(requestType,"NA"));
		cdr.setSubscriberId(JocgString.formatString(subscriberId,"NA"));
		cdr.setMsisdn(msisdn==null?0:msisdn);
		cdr.setPlatformName(JocgString.formatString(platformName,"NA"));
		cdr.setSystemId(JocgString.formatString(systemId,"NA"));
		
		cdr.setRqPackageName(JocgString.formatString(reqParams.getRequestParams().getPackageName(),"NA"));
		cdr.setRqPackagePrice(reqParams.getRequestParams().getPackagePrice()==null?0:reqParams.getRequestParams().getPackagePrice());
		cdr.setRqPackageValidity(reqParams.getRequestParams().getPackageValidity()==null?0:reqParams.getRequestParams().getPackageValidity());
		cdr.setRqNetworkType(JocgString.formatString(reqParams.getRequestParams().getNetworkType(),"M"));
		cdr.setRqVoucherVendor(JocgString.formatString(reqParams.getRequestParams().getVoucherVendor(),"NA"));
		cdr.setRqAmountToBeCharged(reqParams.getRequestParams().getAmountToBeCharged()==null?0:reqParams.getRequestParams().getAmountToBeCharged());
		cdr.setRqVoucherCode(JocgString.formatString(reqParams.getRequestParams().getVoucherCode(),"NA"));

		if(sourceNetwork!=null){
			cdr.setSourceNetworkId(sourceNetwork.getChintfid());
			cdr.setSourceNetworkName(JocgString.formatString(sourceNetwork.getName(),"NA"));
			cdr.setSourceNetworkCode(JocgString.formatString(sourceNetwork.getOpcode(),"NA"));
		}else{
			cdr.setSourceNetworkId(0);
			cdr.setSourceNetworkName("NA");
			cdr.setSourceNetworkCode("NA");
		}
		
		if(chargingNetwork!=null){
			ChargingInterface aggregator=jocgCache.getChargingInterface(chargingNetwork.getPchintfid());
			cdr.setAggregatorId(aggregator!=null?aggregator.getChintfid():chargingNetwork.getPchintfid());
			cdr.setAggregatorName(aggregator!=null?JocgString.formatString(aggregator.getName(),"NA"):"NA");
			cdr.setUseParentHandler(JocgString.intToBool(chargingNetwork.getUseparenthandler()));
			cdr.setChargingInterfaceId(chargingNetwork.getChintfid());
			cdr.setChargingInterfaceName(JocgString.formatString(chargingNetwork.getName(),"NA"));
			cdr.setCountryName(JocgString.formatString(chargingNetwork.getCountry(),"NA"));
			cdr.setGmtDiff(JocgString.formatString(chargingNetwork.getGmtDiff(),"+05:30"));
			cdr.setChargingInterfaceType(JocgString.formatString(chargingNetwork.getChintfType(),"NA"));
			cdr.setRenewalRequired(JocgString.intToBool(chargingNetwork.getRenewalRequired()));
			cdr.setRenewalCategory(JocgString.formatString(chargingNetwork.getRenewalType(),"NA"));
		}else{
			cdr.setAggregatorId(0);
			cdr.setAggregatorName("NA");
			cdr.setUseParentHandler(false);
			cdr.setChargingInterfaceId(0);
			cdr.setChargingInterfaceName("NA");
			cdr.setCountryName("NA");
			cdr.setGmtDiff("+05:30");
			cdr.setChargingInterfaceType("NA");
			cdr.setRenewalRequired(false);
			cdr.setRenewalCategory("NA");
		}
		cdr.setPackageChintfId(pkgchintfMap==null?0:pkgchintfMap.getPkgchintfid());

		if(circle!=null){
			cdr.setCircleId(circle.getCircleid());
			cdr.setCircleName(JocgString.formatString(circle.getCirclename(),"NA"));
			cdr.setCircleCode(JocgString.formatString(circle.getCirclecode(),"NA"));
		}else{
			cdr.setCircleId(0);
			cdr.setCircleName("NA");
			cdr.setCircleCode("NA");
		}

		if(pkg!=null){
			cdr.setPackageId(pkg.getPkgid());
			cdr.setPackageName(JocgString.formatString(pkg.getPkgname(),"NA"));
			cdr.setPackagePrice(pkg.getPricepoint());
			cdr.setValidityCategory(JocgString.formatString(pkg.getValidityCatg(),"NA"));
		}else{
			cdr.setPackageId(0);
			cdr.setPackageName("NA");
			cdr.setPackagePrice(0D);
			cdr.setValidityCategory("NA");
		}
		
		if(service!=null){
			cdr.setServiceId(service.getServiceid());
			cdr.setServiceName(JocgString.formatString(service.getServiceName(),"NA"));
			cdr.setServiceCategory(JocgString.formatString(service.getServiceCatg(),"NA"));
		}else{
			cdr.setServiceId(0);
			cdr.setServiceName("NA");
			cdr.setServiceCategory("NA");
		}

		if(masterPP!=null){
			cdr.setPricePointId(masterPP.getPpid());
			cdr.setOperatorPricePoint(masterPP.getPricePoint());
			cdr.setValidityUnit(JocgString.formatString(masterPP.getValidityType(),"NA"));
			cdr.setValidityPeriod(masterPP.getValidity());
			cdr.setMppOperatorKey(JocgString.formatString(masterPP.getOperatorKey(),"NA"));
			cdr.setMppOperatorParams(JocgString.formatString(masterPP.getOpParams(),"NA"));
			cdr.setContentType(JocgString.formatString(masterPP.getCtype(),"VIDEOS"));
			
		}else{
			cdr.setPricePointId(0);
			cdr.setOperatorPricePoint(0D);
			cdr.setValidityUnit("NA");
			cdr.setValidityPeriod(0);
			cdr.setMppOperatorKey("NA");
			cdr.setMppOperatorParams("NA");
			cdr.setContentType("VIDEOS");
		}
		
		if(chargedPP!=null){
			cdr.setChargedPpid(chargedPP.getPpid());
			cdr.setChargedPricePoint(chargedPP.getPricePoint());
			cdr.setFallbackCharged(JocgString.intToBool(chargedPP.getIsfallback()));
			cdr.setChargedValidityPeriod(""+chargedPP.getValidity());
			cdr.setChargedOperatorKey(JocgString.formatString(chargedPP.getOperatorKey(),"NA"));
			cdr.setChargedOperatorParams(JocgString.formatString(chargedPP.getOpParams(),"NA"));
		}else{
			cdr.setPricePointId(0);
			cdr.setOperatorPricePoint(0D);
			cdr.setValidityUnit("NA");
			cdr.setValidityPeriod(0);
			cdr.setMppOperatorKey("NA");
			cdr.setMppOperatorParams("NA");
		}
		
		if(voucher!=null){
			cdr.setVoucherId(voucher.getCouponid());
			cdr.setVoucherName(JocgString.formatString(voucher.getVoucherId(),"NA"));
			cdr.setVoucherVendorId(voucher.getVendorId());
			cdr.setVoucherVendorName("NA");
			cdr.setVoucherType(JocgString.formatString(voucher.getVoucherType(),"NA"));
			cdr.setVoucherDiscount((double)voucher.getDiscuountValue());
			cdr.setVoucherDiscountType(JocgString.formatString(voucher.getDiscountType(),"NA"));
		}else{
			cdr.setVoucherId(0L);
			cdr.setVoucherName("NA");
			cdr.setVoucherVendorId(0);
			cdr.setVoucherVendorName("NA");
			cdr.setVoucherType("NA");
			cdr.setVoucherDiscount(0D);
			cdr.setVoucherDiscountType("NA");
		}
		cdr.setChargingCategory(JocgString.formatString(chargingCategory, "NA"));
		cdr.setDiscountGiven(discountAmount);
		cdr.setChargedAmount(chargedAmount);
		cdr.setReferer(referer);
		cdr.setChargingDate(chargingDate);
		cdr.setSubRegDate(chargingDate);
		cdr.setValidityStartDate(validityStartDate);
		cdr.setValidityEndDate(validityEndDate);
		cdr.setDeviceDetails(JocgString.formatString(deviceDetails, "NA"));
		cdr.setUserAgent(JocgString.formatString(userAgent, "NA"));
		cdr.setDeviceOS(JocgString.formatString(deviceOs, "NA"));
		
		if(adNetwork!=null){
			cdr.setTrafficSourceId(adNetwork.getTsourceid());
			cdr.setTrafficSourceName(adNetwork.getSourcename());
			cdr.setTrafficSourceToken(JocgString.formatString(adNetworkToken, "NA"));
		}else{
			cdr.setTrafficSourceId(0);
			cdr.setTrafficSourceName("NA");
			cdr.setTrafficSourceToken("NA");
		}
		
		if(campaign!=null){
			cdr.setCampaignId(campaign.getCampaignid());
			cdr.setCampaignName(JocgString.formatString(campaign.getCampaignname(), "NA"));
		}else{
			cdr.setCampaignId(0);
			cdr.setCampaignName("NA");
		}
		cdr.setPublisherId(publisherId);
		cdr.setNotifyStatus(notifyStatus);
		cdr.setNotifyDescp(JocgString.formatString(notifyDescp, "NA"));
		cdr.setNotifyTime(notifyTime);
		cdr.setNotifyScheduleTime(notifyScheduleTime);
		cdr.setNotifyChurnStatus(notifyChurnStatus);
		cdr.setNotifyTimeChurn(notifyTimeChurn);
		cdr.setChurn20Min(churn20Min);
		cdr.setChurnSameDay(churnSameDay);
		cdr.setChurn24Hour(false);
		cdr.setNotifyChurn(notifyChurn);
		
		if(landingPage!=null){
			cdr.setLandingPageId(landingPage.getLandingPageId());
			cdr.setLandingPageScheduleId(landingPage.getLandingPageId());
		}else{
			cdr.setLandingPageId(0);
			cdr.setLandingPageScheduleId(0);
		}
		
		if(cgImage!=null){
			cdr.setCgImageId(cgImage.getImgid());
		}else{
			cdr.setCgImageId(0);
		}
		
		if(routingInfo!=null){
			cdr.setRoutingScheduleId(routingInfo.getRoutingId());
		}else{
			cdr.setRoutingScheduleId(0);
		}
		
	
		cdr.setOtpStatus(otpStatus);
		cdr.setOtpPin(JocgString.formatString(otpPin, "NA"));
		cdr.setStatus(status);
		cdr.setStatusDescp(JocgString.formatString(statusDescp, "NA"));
		cdr.setRequestDate(requestDate);
	
		
		cdr.setCgStatusCode(JocgString.formatString(cgStatusCode, "NA"));
		cdr.setCgStatusDescp(JocgString.formatString(cgStatusDescp, "NA"));
		cdr.setCgTransactionId(JocgString.formatString(cgTransactionId, "NA"));
		cdr.setCgResponseTime(cgResponseTime);
		
		cdr.setSdpStatusCode(JocgString.formatString(sdpStatusCode, "NA"));
		cdr.setSdpStatusDescp(JocgString.formatString(sdpStatusDescp, "NA"));
		cdr.setSdpTransactionId(JocgString.formatString(sdpTransactionId, "NA"));
		cdr.setSdpLifeCycleId(JocgString.formatString(sdpLifeCycleId, "NA"));
		cdr.setSdpResponseTime(sdpResponseTime);
	
		cdr.setParkingDate(parkingDate);
		cdr.setParkingToActivationDate(parkingToActivationDate);
		cdr.setActivatedFromZero(activatedFromZero);
		cdr.setActivatedFromGrace(activatedFromGrace);
		cdr.setActivatedFromParking(activatedFromParking);
		}catch(Exception e){
			logger.error("Exception "+e);
		}
		return cdr;
	}
	
	public ChargingCDR insert(ChargingCDR cdr){
		if(cdr.getRequestCategory()!=null && ("C".equalsIgnoreCase(cdr.getRequestCategory()) || "W".equalsIgnoreCase(cdr.getRequestCategory()) || "V".equalsIgnoreCase(cdr.getRequestCategory()) || "OP".equalsIgnoreCase(cdr.getRequestCategory()) ) )
			cdr.setReferer("JOCG");
		cdr.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
		cdr.setReportGenerated(false);
		return chargingCDRDao.insert(cdr);
	
	}
	
	public ChargingCDR save(ChargingCDR cdr){
		cdr.setLastUpdated(new java.util.Date(System.currentTimeMillis()));
		cdr.setReportGenerated(false);
		return chargingCDRDao.save(cdr);
	
	}
}
