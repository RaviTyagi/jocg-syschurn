package com.jss.jocg.services.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.charging.handlers.JocgChargingHandler;
import com.jss.jocg.charging.model.AirtelIndiaChargingParams;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.services.bo.airtel.AirtelActivationWrapper;
import com.jss.jocg.services.bo.airtel.AirtelApiResponseWraper;
import com.jss.jocg.services.bo.airtel.AirtelSmartApi;
import com.jss.jocg.services.dao.AirtelIndiaPendingTransactionsDao;
import com.jss.jocg.services.data.AirtelIndiaPendingTransactions;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;

public class ChargingProcessor  {

	private static final Logger logger = LoggerFactory
			.getLogger(ChargingProcessor.class);
	
	@Value("${jocg.transid.template}")
	public String jocgTransIdTemplate;
	
	@Value("${ssl.keystore}")
	public String keyStoreFile;
	
	@Value("${keystore.pwd}")
	public String keyStorePassword;

	
	@Autowired JocgCacheManager jocgCache;
	@Autowired NotificationProcessor notifyProcessor;
	SimpleDateFormat idformat=new SimpleDateFormat("ddMM");
	@Autowired RequestValidator requestValidator;
	@Autowired AirtelIndiaPendingTransactionsDao pendingTransDao;
	 @Autowired public JmsTemplate jmsTemplate;
	
	public JocgRequest processOperatorCharging(JocgRequest cr,HttpServletRequest requestContext){
		String logHeader="processOperatorCharging()::{subid="+cr.getSubscriberId()+",msisdn="+cr.getMsisdn()+"} ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			//Search Source Operator
			cr.setSourceInterface(jocgCache.getChargingInterfaceByOpcode(cr.getRequestParams().getSourceOperator()));
			if(cr.getSourceInterface()==null || cr.getSourceInterface().getChintfid()<=0){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVOPERATOR);
				cresp.setResponseDescription("Invalid Source Network");
			}
			logger.debug(logHeader+"Found Source Network "+cr.getSourceInterface());
			//Search Price Point Mapping
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				cr.setPkgChintfMap(jocgCache.getPackageChargingInterfaceMap(cr.getSourceInterface().getChintfid(),cr.getPackageDetail().getPkgid(),cr.getRequestParams().getPackagePrice()));
				logger.debug(logHeader+"Found PackageChintfMap "+cr.getPkgChintfMap());
				if(cr.getPkgChintfMap()==null || cr.getPkgChintfMap().getPkgchintfid()<=0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
					cresp.setResponseDescription("Price Point Mapping not defined!");
				}else{
					//Search Price Point
					cr.setChargingPrice(jocgCache.getPricePointListById(cr.getPkgChintfMap().getOpPpid()));
					logger.debug(logHeader+"Found Chargign Price Point  "+cr.getChargingPrice());
					if(cr.getChargingPrice()==null || cr.getChargingPrice().getPpid()<=0){
						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
						cresp.setResponseDescription("Price Point Id "+cr.getPkgChintfMap().getOpPpid()+" not defined!");
					}else{
						//Search Charging Operator details
						cr.setChargingOperator(jocgCache.getChargingInterface(cr.getPkgChintfMap().getChintfid()));
						logger.debug(logHeader+"Found Charging Interface "+cr.getChargingOperator());
						if(cr.getChargingOperator()==null || cr.getChargingOperator().getChintfid()<=0){
							cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVCHINTF);
							cresp.setResponseDescription("Charging Interface id "+cr.getPkgChintfMap().getChintfid()+"  not defined!");
						}
					}
					
				}
			}
			//Initialize Campaign if not a campaign request
			if(cr.getCampaign()==null) cr.setCampaign(new TrafficCampaign());
			
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				//generate Unique Id
				cr.setJocgTransactionId(generateUniqueId(cr.getChargingOperator().getChintfid(),cr.getCampaign().getTsourceid()));
				logger.error(logHeader+" Generated TransId :"+cr.getJocgTransactionId()+", Processing for charging.....");
				cr=processCharging(cr, requestContext);
				cresp=cr.getResponseObject();
			}else{
				logger.error(logHeader+" Request Failed, Response code"+cresp.getResponseCode()+", Descp :"+cresp.getResponseDescription());
			}
			
		}catch(Exception e){
			
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	public JocgRequest processCampaignCharging(JocgRequest cr,HttpServletRequest requestContext){
		String logHeader="processCampaignCharging()::{subid="+cr.getSubscriberId()+",msisdn="+cr.getMsisdn()+"} ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			logger.debug("Validating Traffic stats before charging");
			//Validate Traffic Stats
			
			Integer trafficValidaterStatus=requestValidator.validateTrafficStats(cr.getSubscriber(),cr);
			
//			if(trafficValidaterStatus.intValue()!=RequestValidator.TPROCESS_REQ){
//				//Apply logic based on Traffic 
//				processStatus=JocgStatusCodes.CHARGING_FAILED;
//				statusDescp=requestValidator.getErrorMessage();
//			}
			
		
			
			
			
			cr.setSourceInterface(jocgCache.getChargingInterfaceByOpcode(cr.getRequestParams().getSourceOperator()));
			if(cr.getSourceInterface()==null || cr.getSourceInterface().getChintfid()<=0){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVOPERATOR);
				cresp.setResponseDescription("Invalid Source Network");
			}
			logger.debug(logHeader+"Found Source Network "+cr.getSourceInterface());
			
			//Search Price Point Mapping
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				
				cr.setPkgChintfMap(jocgCache.getPackageChargingInterfaceMap(cr.getSourceInterface().getChintfid(),cr.getPackageDetail().getPkgid(),cr.getCampaign().getPackagePrice()));
				logger.debug(logHeader+"Found PackageChintfMap "+cr.getPkgChintfMap());
				if(cr.getPkgChintfMap()==null || cr.getPkgChintfMap().getPkgchintfid()<=0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
					cresp.setResponseDescription("Price Point Mapping not defined!");
				}else{
					//Search Price Point
					cr.setChargingPrice(jocgCache.getPricePointListById(cr.getPkgChintfMap().getOpPpid()));
					logger.debug(logHeader+"Found Chargign Price Point  "+cr.getChargingPrice());
					if(cr.getChargingPrice()==null || cr.getChargingPrice().getPpid()<=0){
						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
						cresp.setResponseDescription("Price Point Id "+cr.getPkgChintfMap().getOpPpid()+" not defined!");
					}else{
						//Search Charging Operator details
						cr.setChargingOperator(jocgCache.getChargingInterface(cr.getPkgChintfMap().getChintfid()));
						logger.debug(logHeader+"Found Charging Interface "+cr.getChargingOperator());
						if(cr.getChargingOperator()==null || cr.getChargingOperator().getChintfid()<=0){
							cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVCHINTF);
							cresp.setResponseDescription("Charging Interface id "+cr.getPkgChintfMap().getChintfid()+"  not defined!");
						}else{
							try{
								logger.debug("Validating Landing Page config before charging");
								cr.setLandingPage(selectLandingPageSchedule(cr));
								logger.debug("Selected Landing Page Schedule Id"+(cr.getLandingPage()==null?0:cr.getLandingPage().getLandingPageId()));
								
								logger.debug("Validating CG Image Schedule before charging");
								cr.setCgimageSchedule(requestValidator.getCGImageInfo(cr.getPkgChintfMap().getPkgchintfid(),cr.getTrafficSource().getTsourceid(),cr.getClickCount()));
								//cr.setCgimageSchedule(requestValidator.getCGImageInfo(cr.getChargingOperator().getChintfid(),cr.getTrafficSource().getTsourceid(),cr.getClickCount()));
								logger.debug("Selected CG Image Schedule Id"+(cr.getCgimageSchedule()==null?0:cr.getCgimageSchedule().getImgid()));
							}catch(Exception ee){}
							//Routinginfo routingInfo=requestValidator.getRoutingInfo(cr.getSubscriber(),cr,JocgStatusCodes.SUB_SUCCESS_PENDING);
						
						}
					}
					
					if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
						//generate Unique Id
						cr.setJocgTransactionId(generateUniqueId(cr.getChargingOperator().getChintfid(),cr.getCampaign().getTsourceid()));
						logger.error(logHeader+" Generated TransId :"+cr.getJocgTransactionId()+", Processing for charging.....");
						cr=processCharging(cr, requestContext);
						cresp=cr.getResponseObject();
					}else{
						logger.error(logHeader+" Request Failed, Response code"+cresp.getResponseCode()+", Descp :"+cresp.getResponseDescription());
					}
					
				}
				
			}
			
		}catch(Exception e){
			
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	public JocgRequest processVoucherActivation(JocgRequest cr,HttpServletRequest requestContext){
		String logHeader="processVoucherActivation()::{subid="+cr.getSubscriberId()+",msisdn="+cr.getMsisdn()+"} ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			//Search Source Operator
			logger.debug(logHeader+"Searching Source Network.....");
			cr.setSourceInterface(jocgCache.getChargingInterfaceByOpcode(cr.getRequestParams().getSourceOperator()));
			if(cr.getSourceInterface()==null || cr.getSourceInterface().getChintfid()<=0){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVOPERATOR);
				cresp.setResponseDescription("Invalid Source Network");
			}
			logger.debug(logHeader+"Found Source Network "+cr.getSourceInterface()+"\n Searching Voucher......");
			
			cr.setVoucher(jocgCache.getVoucherDetails(cr.getRequestParams().getVoucherCode()));
			if(cr.getVoucher()==null || cr.getVoucher().getCouponid()<=0){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVREQPARAMS);
				cresp.setResponseDescription("Invalid Voucher Code");
			}
			logger.debug(logHeader+"Found Voucher "+cr.getVoucher()+"\n Searching Price point......");
			
			//Search Price Point Mapping
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				//Search Package using voucher config
				cr.setPkgChintfMap(jocgCache.getPackageChargingInterfaceMapById(cr.getVoucher().getPkgchintfid()));
				logger.debug(logHeader+"Found PackageChintfMap "+cr.getPkgChintfMap());
				if(cr.getPkgChintfMap()==null || cr.getPkgChintfMap().getPkgchintfid()<=0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
					cresp.setResponseDescription("Price Point Mapping not defined!");
				}else{
					//Search Price Point
					//cr.setChargingPrice(jocgCache.getPricePointListById(cr.getPkgChintfMap().getOpPpid()));
					cr.setChargingPrice(jocgCache.getPricePointListById(cr.getVoucher().getChargedPPId()));
					logger.debug(logHeader+"Found Chargign Price Point  "+cr.getChargingPrice());
					if(cr.getChargingPrice()==null || cr.getChargingPrice().getPpid()<=0){
						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
						cresp.setResponseDescription("Price Point Id "+cr.getPkgChintfMap().getOpPpid()+" not defined!");
					}else{
						//Search Charging Operator details
						cr.setChargingOperator(jocgCache.getChargingInterface(cr.getPkgChintfMap().getChintfid()));
						logger.debug(logHeader+"Found Charging Interface "+cr.getChargingOperator());
						if(cr.getChargingOperator()==null || cr.getChargingOperator().getChintfid()<=0){
							cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVCHINTF);
							cresp.setResponseDescription("Charging Interface id "+cr.getPkgChintfMap().getChintfid()+"  not defined!");
						}
						cr.setChargedPrice(cr.getChargingPrice());
						cr.setChargedAmount(cr.getChargingPrice().getPricePoint());
					}
					
//					//Selected Charged Price Point
//					cr.setChargedPrice(jocgCache.getPricePointListById(cr.getVoucher().getChargedPPId()));
//					if(cr.getChargedPrice()==null || cr.getChargedPrice().getPpid()<=0){
//						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
//						cresp.setResponseDescription("Charged Price Point Id "+cr.getVoucher().getChargedPPId()+" not defined!");
//					}else{
//						cr.setChargedAmount(cr.getChargedPrice().getPricePoint());
//					}
					
					
				}
			}
				
		//Initialize Campaign if not a campaign request
			if(cr.getCampaign()==null) cr.setCampaign(new TrafficCampaign());
			
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				//generate Unique Id
				cresp.setResponseCode(JocgStatusCodes.CHARGING_SUCCESS);
				cresp.setResponseDescription("Voucher Applied, No Charging done");
			}else{
				logger.error(logHeader+" Request Failed, Response code"+cresp.getResponseCode()+", Descp :"+cresp.getResponseDescription());
			}
			
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception while processing "+e.getMessage());
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	public JocgRequest processWalletCharging(JocgRequest cr,HttpServletRequest requestContext){
		String logHeader="processWalletCharging()::{subid="+cr.getSubscriberId()+",msisdn="+cr.getMsisdn()+"} ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			//Search Source Operator
			cr.setSourceInterface(jocgCache.getChargingInterfaceByOpcode(cr.getRequestParams().getSourceOperator()));
			if(cr.getSourceInterface()==null || cr.getSourceInterface().getChintfid()<=0){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVOPERATOR);
				cresp.setResponseDescription("Invalid Source Network");
			}
			logger.debug(logHeader+"Found Source Network "+cr.getSourceInterface());
			
			//Search Price Point Mapping
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				cr.setPkgChintfMap(jocgCache.getPackageChargingInterfaceMapByPriceAndValidity(cr.getSourceInterface().getChintfid(),cr.getPackageDetail().getPkgid(),cr.getRequestParams().getPackagePrice(),cr.getRequestParams().getPackageValidity()));
				//Search based on package Price
				//cr.setPkgChintfMap(jocgCache.getPackageChargingInterfaceMap(cr.getSourceInterface().getChintfid(),cr.getPackageDetail().getPkgid(),cr.getPackageDetail().getPricepoint()));
				logger.debug(logHeader+"Found PackageChintfMap "+cr.getPkgChintfMap());
				if(cr.getPkgChintfMap()==null || cr.getPkgChintfMap().getPkgchintfid()<=0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
					cresp.setResponseDescription("Price Point Mapping not defined!");
				}else{
					//Search Price Point
					cr.setChargingPrice(jocgCache.getPricePointListById(cr.getPkgChintfMap().getOpPpid()));
					logger.debug(logHeader+"Found Charging Price Point  "+cr.getChargingPrice());
					if(cr.getChargingPrice()==null || cr.getChargingPrice().getPpid()<=0){
						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVPRICEPOINT);
						cresp.setResponseDescription("Price Point Id "+cr.getPkgChintfMap().getOpPpid()+" not defined!");
					}else{
						//Search Charging Operator details
						cr.setChargingOperator(jocgCache.getChargingInterface(cr.getPkgChintfMap().getChintfid()));
						logger.debug(logHeader+"Found Charging Interface "+cr.getChargingOperator());
						if(cr.getChargingOperator()==null || cr.getChargingOperator().getChintfid()<=0){
							cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVCHINTF);
							cresp.setResponseDescription("Charging Interface id "+cr.getPkgChintfMap().getChintfid()+"  not defined!");
						}
					}
					
				}
			}
			logger.debug("Charging Price "+cr.getChargingPrice());
			
			//Apply Voucher discount if applicable
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				VoucherDetail vd=jocgCache.getVoucherDetails(cr.getRequestParams().getVoucherCode());
				if(vd==null || vd.getCouponid()<=0){
					logger.debug(logHeader+"Voucher Code not applicable or failed to search voucher code  "+cr.getRequestParams().getVoucherCode());
				}else{
					//calculate discount
					if("PERCENTAGE".equalsIgnoreCase(vd.getDiscountType()) || "FIX-AMOUNT".equalsIgnoreCase(vd.getDiscountType())){
						
						//MULTIPLE-TIME-ANYUSER
						int vdiscount=(int)(("PERCENTAGE".equalsIgnoreCase(vd.getDiscountType()))?(vd.getDiscuountValue()*cr.getChargingPrice().getPricePoint())/100:
							vd.getDiscuountValue());
						
						logger.debug("Discount Amount "+vdiscount);
						if(vdiscount>0){
							cr.setChargedAmount(cr.getChargingPrice().getPricePoint()-vdiscount);
							logger.debug("Amount after Discount "+cr.getChargedAmount());
							logger.debug(logHeader+"Voucher Code Applied, Discount Type "+vd.getDiscountType()+",Discount Value "+vd.getDiscuountValue()+", Amount to be charged "+cr.getChargedAmount());
						}else{
							logger.debug(logHeader+"Voucher Code Applied, Discount Type "+vd.getDiscountType()+",Discount Value "+vd.getDiscuountValue()+", Ingnored Negative or Zero discount");
						}
						cr.setVoucher(vd);
					}else{
						logger.debug(logHeader+"Voucher Code not applicable as only Discount vouchers are allowed to be used for Wallet Charging.");
					}
				}
			}
			
			
			//Initialize Campaign if not a campaign request
			if(cr.getCampaign()==null) cr.setCampaign(new TrafficCampaign());
			
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				//generate Unique Id
				cr.setJocgTransactionId(generateUniqueId(cr.getChargingOperator().getChintfid(),cr.getCampaign().getTsourceid()));
				logger.info(logHeader+" Generated TransId :"+cr.getJocgTransactionId()+", Processing for charging.....");
				cr=processCharging(cr, requestContext);
				cresp=cr.getResponseObject();
			}else{
				logger.error(logHeader+" Request Failed, Response code"+cresp.getResponseCode()+", Descp :"+cresp.getResponseDescription());
			}
			
		}catch(Exception e){
			
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	/**
	 * 
	 * @param ChargingRequest cr
	 * @param HttpServletRequest requestContext
	 * @return
	 */
	public JocgRequest processSTVCharging(JocgRequest cr,HttpServletRequest requestContext){
	
		
		
		return cr;
	}
	
	/**
	 * 
	 * @param cr
	 * @param requestContext
	 * @return
	 */
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead="processCharging()::{subid="+cr.getSubscriberId()+",msisdn="+cr.getMsisdn()+"} :: ";
		JocgResponse cresp=cr.getResponseObject();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			String handlerClass="NA";
			List<ChargingUrl> urlList=null;
			cr.setSslKeyStoreFile(keyStoreFile);
			cr.setSslKeystorePwd(keyStorePassword);
			//Search Handler Class to be invoked
			if(cr.getChargingOperator().getUseparenthandler()>0 && cr.getChargingOperator().getPchintfid()>0){
				logger.debug(loggerHead+"Using Parent Handler on priority....Parent id "+cr.getChargingOperator().getPchintfid());
				//Use Parent handler Class
				ChargingInterface pchintf=jocgCache.getChargingInterface(cr.getChargingOperator().getPchintfid());
				logger.debug(loggerHead+"Parent Chintf  "+pchintf);
				if(pchintf!=null && pchintf.getChintfid()>0){
					cr.setUseParentHandler(true);
					cr.setParentChintfId(cr.getChargingOperator().getPchintfid());
					cr.setChargingHandlerClass(pchintf.getHandlerClass());
					cr.setUrlList(jocgCache.getChargingUrlByChintfId(pchintf.getChintfid()));
					
				}else{
					//Parent handler not defined, use charging operator handler class
					logger.debug(loggerHead+"Parent Handler not defined , using chargign operator handler instead.... ");
					cr.setChargingHandlerClass(cr.getChargingOperator().getHandlerClass());
					cr.setUrlList(jocgCache.getChargingUrlByChintfId(cr.getChargingOperator().getChintfid()));
				
				}
			}else{
				//Use charging operator handler class
				logger.debug(loggerHead+"Picking Handler from Charging INterface   "+cr.getChargingOperator()+"\n Handler class "+cr.getChargingOperator().getHandlerClass());
				cr.setChargingHandlerClass(cr.getChargingOperator().getHandlerClass());
				//logger.debug(loggerHead+"Handler Class "+cr.getChargingHandlerClass());
				cr.setUrlList(jocgCache.getChargingUrlByChintfId(cr.getChargingOperator().getChintfid()));
			}
			cr.setChargingHandlerClass((cr.getChargingHandlerClass()==null || cr.getChargingHandlerClass().length()<=0)?"NA":cr.getChargingHandlerClass().trim());
			logger.debug(loggerHead+"Using Handler class  "+cr.getChargingHandlerClass());
			if("NA".equalsIgnoreCase(cr.getChargingHandlerClass())){
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_INVHANDLER);
				cresp.setResponseDescription("Invalid Charging Handler Class "+handlerClass);
			}
			//Populate URL Params
			logger.debug(loggerHead+"Searching URL Parameters ........, URL List Count "+cr.getUrlList().size());
			if(cr.getUrlList()!=null && cr.getUrlList().size()>0){
				Map<Integer,List<ChargingUrlParam>> urlParams=new HashMap<>();
				for(ChargingUrl churl:cr.getUrlList()){
					
					urlParams.put(churl.getChurlId(), jocgCache.getChargingUrlParams(churl.getChurlId()));
				}
				cr.setUrlParams(urlParams);
				
			}
			logger.debug(loggerHead+"URL Param List "+cr.getUrlParams().size());
			
			logger.debug(loggerHead+"cresp  "+cresp.getResponseCode());
			//Invoke Charging Handler if successful processing till here
			if(cresp.getResponseCode()==JocgStatusCodes.CHARGING_INPROCESS){
				logger.debug(loggerHead+"Loading Handler Class .......");
				JocgChargingHandler chHand=(JocgChargingHandler)Class.forName(cr.getChargingHandlerClass()).newInstance();
			    logger.info(loggerHead+" chHand object "+chHand);
			    //cr.setLoggerHead(loggerHead);
			    cr=chHand.processCharging(cr,requestContext);
			    cresp=cr.getResponseObject();
			    logger.debug(loggerHead+"Response Type "+cresp.getRespType());
			}
			logger.debug(loggerHead+"Response Code "+cresp.getResponseCode()+", Descp :"+cresp.getResponseDescription());
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e.getMessage());
			logger.error(loggerHead+" Exception "+e.getMessage());
		}
	
		cr.setResponseObject(cresp);
		return cr;
	}
	
	
	
	
	public ChargingCDR processAirtelIndiaCGRedirect(ChargingCDR cdr,String productName,String cgAuthCode,String jocgTransId){
		
		String logHeader="handleAirtelCGRedirect():: "+jocgTransId+":: ";
		try{
		if(cdr!=null && cdr.getId()!=null){
			//CDR Found
			
			logger.debug(logHeader+"Processing code "+cgAuthCode+", cdr id "+cdr.getId());
			
			ChargingInterface chintf=jocgCache.getChargingInterface(cdr.getChargingInterfaceId());
			logger.debug(logHeader+"Charging Interface "+chintf);
			
			List<ChargingUrl> urlList=null;
			if(chintf!=null && chintf.getChintfid()>0)
				urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
			//logger.debug("Charging URL List "+urlList);
			
			if(cdr.getStatus()==JocgStatusCodes.CHARGING_INPROCESS && chintf.getChintfid()>0 && urlList!=null && urlList.size()>0){
				String operatorParams=cdr.getMppOperatorParams();
				AirtelIndiaChargingParams acp=new AirtelIndiaChargingParams();
				acp.decodeParams(operatorParams);
				//logger.debug("operatorParams "+operatorParams);
				
				ChargingUrl churl=null;
				for(ChargingUrl c:urlList){
					if("SDP-AUTHURL".equalsIgnoreCase(c.getUrlCatg())){ 
						churl=c; break;
					}
				}
				
				logger.debug(logHeader+"Auth URL  "+churl);
				
				List<ChargingUrlParam> urlParams=jocgCache.getChargingUrlParams(churl.getChurlId());
				logger.debug(logHeader+"Auth URL Params  "+urlParams);
				if(churl!=null && churl.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
					
					String urlToInvoke=churl.getUrl();
					logger.debug(logHeader+"urlToInvoke  "+urlToInvoke);
					
					String requestData = churl.getDataTemplate();
					logger.debug(logHeader+"requestDataTemplate  "+requestData);
					
					//grant_type=authorization_code&client_id=<CLIENT_ID>&redirect_uri=<REDIRECT_URL>&code=<AUTHCODE>
					requestData=requestData.replaceAll("<AUTHCODE>", cgAuthCode);
					requestData=requestData.replaceAll("<CLIENT_ID>", acp.getClientId());
					String redirectUrl="";
					for(ChargingUrlParam urlParam:urlParams){
						if("REDIRECT_URL".equalsIgnoreCase(urlParam.getParamKey())) redirectUrl=urlParam.getDefaultValue();
					}
					
					requestData=requestData.replaceAll("<REDIRECT_URL>", redirectUrl);
					//grant_type=authorization_code&client_id=<CLIENT_ID>&redirect_uri=REDIRECT_URL&code=<AUTHCODE>"
		            		
					logger.debug(logHeader+"requestData  "+requestData);
					JcmsSSLClientResponse urlResp=invokeAirtelOuthURL(logHeader,urlToInvoke,requestData, "application/x-www-form-urlencoded", "application/json", acp.getClientId(), acp.getSecretKey());
					
					String respData = urlResp.getResponseData();//"{\"access_token\":\"fc79ace7-307a-4a0c-85be-8baa39bcb8e1\",\"token_type\":\"bearer\",\"expires_in\":43199,\"scope\":\"subscription\"}";//JocgString.formatString(urlResp.getResponseData(), "NA");
					logger.debug(logHeader+"tokenResp  "+respData);
				
				
					if (respData.contains("access_token")) {
						logger.debug(logHeader+"Token received.. Prsing Token  ");
					        AirtelApiResponseWraper jr = new AirtelApiResponseWraper();
					        AirtelSmartApi apiResp = jr.parseResponse(respData);
					        logger.debug(logHeader+"Parsed Response Object  "+apiResp);
					        if (apiResp != null) {
					            //Process for Activation 
					        	churl=null;
								for(ChargingUrl c:urlList){
									if("SDP-CHARGE".equalsIgnoreCase(c.getUrlCatg())){ 
										churl=c; break;
									}
								}
								urlResp=null;
							
								

								StringBuilder activationDataTemplate=new StringBuilder();
								activationDataTemplate.append("{").append('"').append("channel").append('"').append(":").append('"').append("WAP").append('"').append("}");
								logger.debug(logHeader+" 	airtelActivationDataTemplate " +activationDataTemplate);
								
								
								urlResp= invokeAirtelActivationUrl(churl.getUrl(), "smartapi.airtel.in", 
										activationDataTemplate.toString(), apiResp.getAccess_token(), "application/json", "application/json", acp.getClientId(), acp.getSecretKey());
								String actRespData = urlResp.getResponseData();//"{\"object\":{\"cpId\":\"150027\",\"productId\":\"546323\",\"msisdn\":\"7428837428\",\"seTransactionId\":\"41530810290\",\"seTransactionTime\":\"2016-07-19T12:30:12.859Z\",\"xActionId\":\"-1418862670\",\"lifecycleId\":\"736892789\"},\"response\":{\"code\":\"activateservice-0000-S\",\"message\":\"Request processed successfully\"}}";
								logger.debug(logHeader+"activation response data "+actRespData);
								if(urlResp.getResponseCode()==500 || actRespData.contains("SAS-DP-01") || actRespData.contains("Internal Server Error")){
									logger.debug(logHeader+"Received Internal Server error, Retrial -1");
									urlResp= invokeAirtelActivationUrl(churl.getUrl(), "smartapi.airtel.in", 
											activationDataTemplate.toString(), apiResp.getAccess_token(), "application/json", "application/json", acp.getClientId(), acp.getSecretKey());
									actRespData = urlResp.getResponseData();
									logger.debug(logHeader+"activation response data "+actRespData);
								}
								
								if(urlResp.getResponseCode()==500 || actRespData.contains("SAS-DP-01") || actRespData.contains("Internal Server Error")){
									logger.debug(logHeader+"Failed in two retrials, Registering transaction for lateral processing....");
									AirtelIndiaPendingTransactions pendingTrans=new AirtelIndiaPendingTransactions();
									pendingTrans.setAccessTokenStr(respData);
									pendingTrans.setCdrId(cdr.getId());
									pendingTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
									pendingTrans.setRetryCount(0);
									pendingTrans.setStatus(0);
									pendingTrans.setStatusDescp("NEW");
									pendingTrans.setTransTime(new java.util.Date(System.currentTimeMillis()));
									pendingTrans=pendingTransDao.save(pendingTrans);
									
									cdr.setStatus(JocgStatusCodes.CHARGING_CGSUCCESS);
									cdr.setStatusDescp(logHeader+"Access Token Generated against code="+cgAuthCode+", Pending Trans id "+pendingTrans.getId());
									cdr.setCgStatusCode(logHeader+"ACCESSTOKEN_SUCCESS");
									cdr.setCgStatusDescp(respData);
									cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
									cdr.setSubRegDate(new java.util.Date(System.currentTimeMillis()));

								}else{
									//Valid Activation API response, No need to reprocess
									//String actRespData = JocgString.formatString(urlResp.getResponseData(), "NA");
									logger.debug(logHeader+"Parsing activation response "+actRespData);
									//Parse Activation response
									AirtelActivationWrapper airtelActivationWrapper = jr.parseActivationResponseNew(actRespData);
									logger.debug(logHeader+"Object After parsing  "+airtelActivationWrapper);
									Integer transStatus=0;
									String transStatusDescp="",seTransId="";
									if (airtelActivationWrapper!=null && airtelActivationWrapper.getActivationObject()!=null &&
						                    airtelActivationWrapper.getActivationObject().getxActionId()!=null) {
										logger.debug(logHeader+"Updating successful transaction "+airtelActivationWrapper.getActivationObject());
						                cdr.setCgTransactionId(airtelActivationWrapper.getActivationObject().getxActionId());
						                cdr.setSdpTransactionId(airtelActivationWrapper.getActivationObject().getSeTransactionId());
						                String msisdnStr=""+airtelActivationWrapper.getActivationObject().getMsisdn();
						                msisdnStr=(msisdnStr==null)?"NA":msisdnStr.length()<=10?"91"+msisdnStr:msisdnStr;
						                cdr.setMsisdn(JocgString.strToLong(msisdnStr, cdr.getMsisdn()));
						                
						                
						                cdr.setSdpLifeCycleId(airtelActivationWrapper.getActivationObject().getLifecycleId());
						                String chargedCode=airtelActivationWrapper.getActivationObject().getProductId();
						                cdr.setChargedOperatorKey(chargedCode);
						               
						                logger.debug(logHeader+"cdr new "+cdr.getId()+", msisdn "+cdr.getMsisdn());
						                
						                ChargingPricepoint chpp=jocgCache.getPricePointListByOperatorKey(chargedCode);
						                Calendar cal=Calendar.getInstance();
					                	cal.setTimeInMillis(System.currentTimeMillis());
						                logger.debug(logHeader+"Pricepoint "+chpp);
						                if(chpp!=null && chpp.getPpid()>0){
						                	cdr.setChargedAmount(chpp.getPricePoint());
						                	cdr.setChargedPpid(chpp.getPpid());
						                	cdr.setChargedPricePoint(chpp.getPricePoint());
						                	cdr.setChargedValidityPeriod(""+chpp.getValidity());
						                	
						                	cdr.setChargingDate(cal.getTime());
						                	cdr.setSubRegDate(cal.getTime());
						                	cdr.setValidityStartDate(cal.getTime());
						                	cal.add(Calendar.DATE, chpp.getValidity());
						                	cdr.setValidityEndDate(cal.getTime());
						                	cdr.setFallbackCharged(chpp.getIsfallback()>0?true:false);
						                	cdr.setChargedOperatorParams(chpp.getOpParams());
						                	cdr.setChargedOperatorKey(chpp.getOperatorKey());
						                	//Setting Notification
											cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
											cdr.setNotifyScheduleTime(new java.util.Date());
											cdr.setNotifyChurn(true);
											logger.debug(logHeader+" S2S Notification Queued, Churn Status Enabled!");
						                	
						                }else{
						                	cdr.setChargingDate(cal.getTime());
						                	cdr.setSubRegDate(cal.getTime());
						                	cdr.setChargedAmount(0D);
						                	cdr.setChargedOperatorKey(chargedCode);
						                }
						                
						                transStatus = JocgStatusCodes.CHARGING_SUCCESS;
						                transStatusDescp = "Activation Resp=" + actRespData + "|TokenResp=" + respData;
						                //seTransId = activationRespData.substring(activationRespData.indexOf(":") + 1);
						                //seTransId = seTransId.trim();
						                //seTransId = seTransId.substring(0, seTransId.length() - 1);
						                seTransId=airtelActivationWrapper.getActivationObject().getxActionId();
						                cdr.setStatus(transStatus);
						                cdr.setStatusDescp(transStatusDescp);
						                cdr.setSdpStatusCode(airtelActivationWrapper.getActivationObject().getxActionId());
						                cdr.setCgTransactionId(airtelActivationWrapper.getActivationObject().getSeTransactionId());
						                
						                //S2S to Suntech URL
						                try{
//											String urlStr="http://119.252.216.58:8088/SdplService/HttpPostCollector/OAIRT?"
//													+ "RefID="+jocgTransId+"&EXT_TransID="+seTransId+"&CPID=118&msisdn="+airtelActivationWrapper.getActivationObject().getMsisdn()+"&"
//															+ "Product_ID="+airtelActivationWrapper.getActivationObject().getProductId()+"&Charging_Time="+airtelActivationWrapper.getActivationObject().getSeTransactionTime()+"&"
//																	+ "Response_Code=1&Response_Message=Success&Mode=6WHMM&Vendor_ID="+cdr.getTrafficSourceName()+"&USER_FIELD_1="+cdr.getPackageName()+"&USER_FIELD_2="+cdr.getSourceNetworkCode()+"&USER_FIELD_3="+cdr.getSdpLifeCycleId()+"&USER_FIELD_4=null&USER_FIELD_5=null";
//											jmsTemplate.convertAndSend("callback.airtelincg", urlStr);
						                
						                }catch(Exception ee){
											
										}
						                
						                
						                
						            } else {
						                //Activation Error
						            	 cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
							             cdr.setStatusDescp("Failed on CG Consent");
						                transStatus = JocgStatusCodes.CHARGING_FAILED;
						                transStatusDescp = "Activation Resp=" + actRespData + "|TokenResp=" + respData;

						            }
									
									
									//Update Transaction accordingly now
									cdr.setCgStatusCode(""+transStatus.intValue());
									cdr.setCgStatusDescp(transStatusDescp);

								}
																
								
								
					        }else{
					        	cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
								cdr.setCgStatusDescp("Unparseable Access Token");
					        	cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					        	cdr.setStatusDescp("Unparseable Token, resp "+respData);
					        }
					 }else{
						 //Invalid Access Token
						 cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
							cdr.setCgStatusDescp("Invalid Access Token");
							cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				             cdr.setStatusDescp("Failed on CG Consent");
					 }
				  
				}else{
					//Failed to find Auth URL Info
					cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cdr.setCgStatusDescp("TOKEN URL Not Registered");
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
		             cdr.setStatusDescp("Failed on CG Consent");
				}
				
				
				
			}else{
				cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cdr.setCgStatusDescp("Invalid Transaction State");
				cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
	             cdr.setStatusDescp("Failed on CG Consent");
			}
			
		}else{
			//Transaction id not found
			cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cdr.setCgStatusDescp("Invalid Transaction Id");
			cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
            cdr.setStatusDescp("Failed on CG Consent");
		}
		}catch(Exception e){
			e.printStackTrace();
			cdr.setCgStatusCode(""+JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cdr.setCgStatusDescp("Exception Post CG Redirect "+e);
			cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
            cdr.setStatusDescp("Failed on CG Consent");
			
		}
		cdr.setCgResponseTime(new java.util.Date(System.currentTimeMillis()));
		//Notification to be handled by S2S Handler only
		cdr=notifyProcessor.processNotification(cdr);
		return cdr;
	}
	
	
	 private JcmsSSLClientResponse invokeAirtelOuthURL(String logHeader,String url1, String requestData, String contentType, String acceptFormat, String user, String pwd) {
	        JcmsSSLClientResponse resp = null;
	        try {
	            JcmsSSLClient sslClient = new JcmsSSLClient();
	            sslClient.setDestURL(url1);
	            sslClient.setUseBasicAuthentication(true);
	            sslClient.setAuthUser(user);
	            sslClient.setAuthPwd(pwd);
	            sslClient.setRequestData(requestData);
	            sslClient.setAcceptDataFormat(acceptFormat);
	            sslClient.setContentType(contentType);
	            sslClient.setHttpMethod("POST");
	            sslClient.setConnectionTimeoutInSecond(30);
	            sslClient.setReadTimeoutInSecond(30);
	            resp = sslClient.connectURLViaSSLIgnore(logHeader);
//	            System.out.println("Airtel Response code " + resp.getResponseCode());
//	            System.out.println("Airtel Response Data " + resp.getResponseData());
//	            System.out.println("Airtel Response Error " + resp.getErrorMessage());
	        } catch (Exception e) {
	            System.out.println(" Airtel  Exception " + e);
	        }
	        return resp;
	    }
	 
	
	 private JcmsSSLClientResponse invokeAirtelActivationUrl(String url1, String hostName, String requestData, String authToken, String contentType, 
             String acceptFormat, String user, String pwd) {
		 JcmsSSLClientResponse resp = null;
		 try {
		     JcmsSSLClient sslClient = new JcmsSSLClient();
		     sslClient.setDestURL(url1);
		     sslClient.setUseBasicAuthentication(true);
		     sslClient.setAuthUser(user);
		     sslClient.setAuthPwd(pwd);
		
		     sslClient.setRequestData(requestData);
		     sslClient.setAcceptDataFormat(acceptFormat);
		     sslClient.setContentType(contentType);
		     sslClient.setAuthToken(authToken);
		     sslClient.setUseAuthTokenInBasicAuth(true);
		     sslClient.setHttpMethod("POST");
		     sslClient.setUseHostNameInHeader(true);
		     sslClient.setHostName(hostName);
		     sslClient.setConnectionTimeoutInSecond(30);
		     sslClient.setReadTimeoutInSecond(30);
		     resp = sslClient.connectInsecureURLBasicAuth("Airtel Smart API :: ");
		     
		   
		     System.out.println("Airtel Response code " + resp.getResponseCode());
		     System.out.println("Airtel Response Data " + resp.getResponseData());
		     System.out.println("Airtel Response Error " + resp.getErrorMessage());
		 } catch (Exception e) {
		     System.out.println(" Airtel  Exception " + e);
		 }
		 return resp;
		}
	 
	 protected String generateUniqueId(Integer operatorId,Integer adnetworkId){
			String idTemplate="";
			try{
			idTemplate=this.jocgTransIdTemplate;
			String uniqNumber=idformat.format(new java.util.Date(System.currentTimeMillis()))+jocgCache.getTransactionSequence(operatorId, 7);
			String adnStr=""+adnetworkId.intValue();
			while(adnStr.length()<3) adnStr="0"+adnStr;
		
			idTemplate=idTemplate.replaceAll("<ADNETWORK>", adnStr);
			
			idTemplate=idTemplate.replaceAll("<TRANSID>", uniqNumber);
			idTemplate=idTemplate.replaceAll("<SERVERID>",""+jocgCache.getServerId());
			}catch(Exception e){
				e.printStackTrace();
				
			}
			
			System.out.println("Unique Id Generated "+idTemplate);
			return idTemplate;
		}
	 
	/* public CgimageSchedule selectCGPageSchedule(JocgRequest cr){
		 CgimageSchedule appliedImage=null,clickCountImage=null,dailyImage=null,defaultimage=null,allDayPresentHour=null,dayMatchedAnyHour=null,allDayAllHour=null;
		 try{
			 Integer clickCount=cr.getClickCount();
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			int dayOfWeek= cal.get(Calendar.DAY_OF_WEEK);
			int cHour=cal.get(Calendar.HOUR_OF_DAY);
			 Map<Integer,CgimageSchedule> retr= jocgCache.getScheduledImages(cr.getChargingOperator().getChintfid(),cr.getTrafficSource().getTsourceid());
			Iterator<CgimageSchedule> i=retr.values().iterator();
			CgimageSchedule imgschedule=null;
			while(i.hasNext()){
				imgschedule=i.next();
				if("CLICKCOUNT".equalsIgnoreCase(imgschedule.getScheduleType())){
					if((clickCountImage==null || clickCount> JocgString.strToInt(imgschedule.getHrsOfDay(),0)) || 
							(clickCountImage!=null && JocgString.strToInt(clickCountImage.getHrsOfDay(),0) < JocgString.strToInt(imgschedule.getHrsOfDay(),0)
							 && clickCount> JocgString.strToInt(imgschedule.getHrsOfDay(),0))){
						clickCountImage=imgschedule;
					}
				}else if("DAILY".equalsIgnoreCase(imgschedule.getScheduleType()) && (imgschedule.getHrsOfDay()==null || imgschedule.getHrsOfDay().contains(","+dayOfWeek+",") || "NA".equalsIgnoreCase(imgschedule.getHrsOfDay()))){
					dailyImage=imgschedule;
				}else if("DAYOFWEEK".equalsIgnoreCase(imgschedule.getScheduleType())){
					int sDayOfWeek=getDayOfWeek(imgschedule.getDaysOfWeek());
					if(sDayOfWeek==dayOfWeek && imgschedule.getHrsOfDay().contains(","+cHour+",")){
						//Both Day and Hour matched
						appliedImage=imgschedule;
					}else if(sDayOfWeek==0 && imgschedule.getHrsOfDay().contains(","+cHour+",")){
						//Day of week is All Day and hour is matched
						allDayPresentHour=imgschedule;
					}else if(sDayOfWeek==dayOfWeek){
						dayMatchedAnyHour=imgschedule;
					}else if(sDayOfWeek==0){
						allDayAllHour=imgschedule;
					}
					appliedImage=(appliedImage==null || appliedImage.getImgid()<=0)?((allDayPresentHour!=null && allDayPresentHour.getImgid()>0)?allDayPresentHour:
						(dayMatchedAnyHour!=null && dayMatchedAnyHour.getImgid()>0)?dayMatchedAnyHour:	
							(allDayAllHour!=null && allDayAllHour.getImgid()>0)?allDayAllHour:appliedImage):appliedImage;
				}else if("DEFAULT".equalsIgnoreCase(imgschedule.getScheduleType())){
					defaultimage=imgschedule;
				}
			}
			appliedImage=(appliedImage==null || appliedImage.getImgid()<=0)?(
							(clickCountImage!=null && clickCountImage.getImgid()>0)?clickCountImage:
							(dailyImage!=null && dailyImage.getImgid()>0)?dailyImage:defaultimage
					):appliedImage;

		 }catch(Exception e){
			 logger.error("selectLandingPageSchedule() :: Exception "+e);
		 }
		 clickCountImage=null;dailyImage=null;defaultimage=null;allDayPresentHour=null;dayMatchedAnyHour=null;allDayAllHour=null;
		 appliedImage=(appliedImage==null)?new CgimageSchedule():appliedImage;
		 return appliedImage;
	 }*/
	 
	 
	 public LandingPageSchedule selectLandingPageSchedule(JocgRequest cr){
		 LandingPageSchedule appliedLps=null,dayMatchedAnyHour=null,allDayAllHour=null,allDayPresentHour=null;
		 try{
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			int dayOfWeek= cal.get(Calendar.DAY_OF_WEEK);
			int cHour=cal.get(Calendar.HOUR_OF_DAY);
			List<LandingPageSchedule> retr=jocgCache.getLandingPageSchedule(cr.getChargingOperator().getChintfid(), cr.getTrafficSource().getTsourceid());
			for(LandingPageSchedule schedule:retr){
				int sDayOfWeek=getDayOfWeek(schedule.getDaysOfWeek());
				if("LANDING".equalsIgnoreCase(schedule.getPageType())){
					if(sDayOfWeek==dayOfWeek && schedule.getHrsOfDay().contains(","+cHour+",")){
						//Both Day and Hour matched
						appliedLps=schedule;
					}else if(sDayOfWeek==0 && schedule.getHrsOfDay().contains(","+cHour+",")){
						//Day of week is All Day and hour is matched
						allDayPresentHour=schedule;
					}else if(sDayOfWeek==dayOfWeek){
						dayMatchedAnyHour=schedule;
					}else if(sDayOfWeek==0){
						allDayAllHour=schedule;
					}
				}
			}
			appliedLps=(appliedLps==null || appliedLps.getLandingPageId()<=0)?((allDayPresentHour!=null && allDayPresentHour.getLandingPageId()>0)?allDayPresentHour:
							(dayMatchedAnyHour!=null && dayMatchedAnyHour.getLandingPageId()>0)?dayMatchedAnyHour:	
								(allDayAllHour!=null && allDayAllHour.getLandingPageId()>0)?allDayAllHour:appliedLps):appliedLps;
			dayMatchedAnyHour=null;allDayAllHour=null;allDayPresentHour=null;
		 }catch(Exception e){
			 logger.error("selectLandingPageSchedule() :: Exception "+e);
		 }
		 appliedLps=(appliedLps==null)?new LandingPageSchedule():appliedLps;
		 return appliedLps;
	 }
	 
	 public int getDayOfWeek(String dayName){
		 return "SUNDAY".equalsIgnoreCase(dayName)?Calendar.SUNDAY:
			 "MONDAY".equalsIgnoreCase(dayName)?Calendar.MONDAY:
				 "TUESDAY".equalsIgnoreCase(dayName)?Calendar.TUESDAY: 
					 "WEDNESDAY".equalsIgnoreCase(dayName)?Calendar.WEDNESDAY: 
						 "THURSDAY".equalsIgnoreCase(dayName)?Calendar.THURSDAY:	 
							 "FRIDAY".equalsIgnoreCase(dayName)?Calendar.FRIDAY:
								 "SATURDAY".equalsIgnoreCase(dayName)?Calendar.SATURDAY:0;
	 }
	
}