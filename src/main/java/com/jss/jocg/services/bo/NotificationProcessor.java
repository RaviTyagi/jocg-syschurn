package com.jss.jocg.services.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JocgStatusCodes;

public class NotificationProcessor {
	private static final Logger logger = LoggerFactory
			.getLogger(NotificationProcessor.class);
	
	@Autowired JocgCacheManager jocgCache;
	@Autowired JmsTemplate jmsTemplate;
	
	public ChargingCDR processNotification(ChargingCDR cdr){
		Integer notifyStatus=JocgStatusCodes.NOTIFICATION_NOTSENT;
		try{
			if(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS){
				//Implement Notification Logic here
				//Temporarily set to not sent for all transactions
				cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
				cdr.setNotifyDescp("Notification Queued");
				cdr.setNotifyScheduleTime(new java.util.Date(System.currentTimeMillis()));
			//	Not to be sent from app servers
			//	jmsTemplate.convertAndSend("notify.adnetwork", cdr);
			}else{
				cdr.setNotifyStatus(notifyStatus);
				cdr.setNotifyDescp("Notification not to be sent");
				cdr.setNotifyScheduleTime(new java.util.Date(System.currentTimeMillis()));
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Exception "+e+" while processing Notification for cdr "+cdr);
		}
		return cdr;
	}
	
	
}
