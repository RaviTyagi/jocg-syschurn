package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgString;
import com.jss.jocg.util.SubscriberUpdateRecord;
import com.jss.jocg.util.SubscriberVendorUpdateRecord;


@Component
public class SubscriberUpdateProcess {
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriberUpdateProcess.class);
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	
	@Value("${jocg.offlinecdr.subscriber.update}")
	String offlineCDRFolder;
	
	//@Scheduled(initialDelay=1, fixedRate=86400000)
	 public void processFiles(){
		String logHeader="SUBSCRIBER-UPDATE :: ";
		if(jocgCache.isOfflineProcessEnabled()){
				
				 try{
					 logger.debug(logHeader+" PROCESSOR Invoked ");
					 File fd=new File(offlineCDRFolder);
					 logger.debug(logHeader+"  Base folder "+offlineCDRFolder);
					 if(fd.exists() && fd.isDirectory()){
							File[] flist=fd.listFiles();
							if(flist!=null && flist.length>0){
								for(File f:flist){
									 logger.debug(logHeader+" Processing file  "+f.getAbsolutePath());
									 
									 if(!f.isDirectory() && ((System.currentTimeMillis()-f.lastModified())/1000)>5){
										 long processStartTime=System.currentTimeMillis();	
										 BufferedReader br=new BufferedReader(new FileReader(f));
							                    String newLine="";int processedRecords=0;
							                    while((newLine=br.readLine())!=null){
							                    	   //processRecord(logHeader,newLine,f.getName());
							                    		//processVenderRecord(logHeader,newLine,f.getName());
							                    		processBLANKRecord(logHeader,newLine,f.getName());
							                           processedRecords++;
							                            WAIT(10);
							                    }
							                    
							                    logger.info(logHeader+"  Processed Count in ("+f.getCanonicalPath()+") :"+processedRecords+", Time Taken "+((System.currentTimeMillis()-processStartTime)/60000)+" minutes.");
							                    br.close();
							                    br=null;
							                    try{
							                    	f.renameTo(new File(offlineCDRFolder+"/processed/"+f.getName()));
							                    }catch(Exception ee){
							                    	f.delete();
							                    }
							                  WAIT(50);
							                  
										}//if
										else  
											logger.debug(logHeader+" Skipped File "+f.getAbsolutePath()+"|IsDirectory="+f.isDirectory());
										f=null;
								}//for
								
							}//if
					 }else  
						 logger.debug(logHeader+" Invalid Folder "+offlineCDRFolder);
					 
					 
				 }catch(Exception e){
					 
				 }
		 } else  logger.debug(logHeader+":: Offline Processes are disabled on this server ");
		
	}
	
	public synchronized void WAIT(int ms){
		 try{
			this.wait(ms);
		 }catch(Exception e){}
		 
	 }
	
	public void  processBLANKRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		try{
			//newLine=newLine.substring(newLine.indexOf("{")+1);
			//newLine=newLine.substring(0, newLine.indexOf("}"));
			String[] strArr=newLine.split(",");
			String seTranId="NA",adNetworkName="NA",systemId="NA";
			for(String s:strArr){
				if(s.contains("temp2")){
					seTranId=s.substring(s.indexOf("=")+1);
				}else if(s.contains("systemId")){
					systemId=s.substring(s.indexOf("=")+1);
				}else if(s.contains("adnetworkName")){
					adNetworkName=s.substring(s.indexOf("=")+1);
				}
			}
			
			if(!"NA".equalsIgnoreCase(seTranId) && !"NA".equalsIgnoreCase(adNetworkName) && !"NA".equalsIgnoreCase(systemId)){
				processVenderRecord(logHeader,fileName,newLine,seTranId,adNetworkName,systemId);
			}else{
				erroFlag=true;
				erroMessage="Failed to Parse. seTranId="+seTranId+",adNetworkName="+adNetworkName+",systemId="+systemId;
			}
		}catch(Exception e){
			
		}finally{
			try{
				if(erroFlag==true){
					String fileNameStatus=offlineCDRFolder+"/processed/"+fileName+".failed";
					FileWriter fw=new FileWriter(fileNameStatus,true);
					fw.write("\n"+newLine+"|"+erroMessage);
					fw.flush();
					fw.close();
				}
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}
	
	public void  processVenderRecord(String logHeader,String fileName,String newLine,String seTransId,String adNetworkName,String systemId){
		boolean erroFlag=false;
		String erroMessage="";
		try{
			TrafficSource adNetwork=getTrafficSourceByName(logHeader,JocgString.formatString(adNetworkName, "NA"));
			Platform platform=getPlatformBySystemId(logHeader,systemId);
			
			if(adNetwork!=null && adNetwork.getTsourceid()>0 && platform!=null && platform.getPlatformid()>0){
				ChargingCDR cdr=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(2, seTransId);
				String cdrId="NA";
				if(cdr!=null && cdr.getId()!=null){
					cdrId=cdr.getId();
					cdr.setSystemId(platform.getSystemid());
					cdr.setPlatformName(platform.getPlatform());
					cdr.setTrafficSourceId(adNetwork.getTsourceid());
					cdr.setTrafficSourceName(adNetwork.getSourcename());
					cdr=cdrBuilder.save(cdr);
					logger.debug(logHeader+" Subscriber "+cdr.getId()+" updated with Adnetwork "+cdr.getTrafficSourceName()+" and system id "+cdr.getSystemId());
				}else{
					erroFlag=true;
					erroMessage +="CDR Not found for seTransId  "+seTransId;
				}
				cdr=null;
				Subscriber sub=subscriberDao.findByJocgCDRId(cdrId);
				if(sub!=null && sub.getId()!=null){
					sub.setSystemId(platform.getSystemid());
					sub.setPlatformId(platform.getPlatform());
					sub.setTrafficSourceId(adNetwork.getTsourceid());
					sub.setTrafficSourceName(adNetwork.getSourcename());
					sub=subMgr.save(sub);
					logger.debug(logHeader+" Subscriber "+sub.getId()+" updated with Adnetwork "+sub.getTrafficSourceName()+" and system id "+sub.getSystemId());
				}else{
					erroFlag=true;
					erroMessage +="Susbcriber Not found for CDRId  "+cdrId;
				}
				sub=null;
			}else if (adNetwork!=null && adNetwork.getTsourceid()>0){
				//Invalid System id
				erroFlag=true;
				erroMessage="Invalid SystemId "+systemId;
			}else{
				//Invalid AdNetwork
				erroFlag=true;
				erroMessage="Invalid adNetwork "+adNetwork;
			}
			
			
			
		}catch(Exception e){
			erroFlag=true;
			erroMessage="Exception "+e.getMessage()+","+newLine;
		}finally{
			try{
				String fileNameStatus=offlineCDRFolder+"/processed/"+fileName+((erroFlag==true)?"failed":"success");
				FileWriter fw=new FileWriter(fileNameStatus,true);
				fw.write("\n"+newLine+"|"+erroMessage);
				fw.flush();
				fw.close();
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}
	
	/*public void  processVenderRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		try{
			String[] strArr=newLine.split(",");
			if(strArr.length>9){
				String subscriberid=JocgString.formatString(strArr[0], "NA");
				TrafficSource adNetwork=getTrafficSourceByName(logHeader,JocgString.formatString(strArr[9], "NA"));
				Platform platform=null;
				if(strArr.length>10){
					platform=getPlatformBySystemId(logHeader,JocgString.formatString(strArr[10], "NA"));
				}else if(adNetwork!=null && adNetwork.getSourcename().contains("sapioffline")){
					platform=getPlatformBySystemId(logHeader,adNetwork.getSourcename());
				}else{
					erroFlag=true;
					erroMessage="Invalid SystemId";
				}
				if(!"NA".equalsIgnoreCase(subscriberid)){
					String cdrId="";
					try{
						Subscriber sub=subscriberDao.findById(subscriberid);
						if(sub!=null && sub.getId()!=null){
							if(adNetwork!=null && adNetwork.getTsourceid()>=0 ){
								sub.setTrafficSourceId(adNetwork.getTsourceid());
								sub.setTrafficSourceName(adNetwork.getSourcename());
							}
							if(platform!=null && platform.getPlatformid()>0){
								sub.setPlatformId(platform.getPlatform());
								sub.setSystemId(platform.getSystemid());
							}
							cdrId=sub.getJocgCDRId();
							sub=subMgr.save(sub);
							logger.debug(logHeader+" Subscriber "+sub.getId()+" updated with Adnetwork "+sub.getTrafficSourceName()+" and system id "+sub.getSystemId());
							sub=null;
						}else{
							erroFlag=true;
							erroMessage="Subscriber not found for Customer id "+subscriberid;
						}
						
					}catch(Exception ee){
						erroFlag=true;
						erroMessage="Subscriber not found for Subscriber id "+subscriberid;
					}
					
					try{
						if("NA".equalsIgnoreCase(cdrId)){
							ChargingCDR cdr=chargingCDRDao.findById(cdrId);
							if(cdr!=null && cdr.getId()!=null){
								if(adNetwork!=null && adNetwork.getTsourceid()>=0 ){
									cdr.setTrafficSourceId(adNetwork.getTsourceid());
									cdr.setTrafficSourceName(adNetwork.getSourcename());
								}
								if(platform!=null && platform.getPlatformid()>0){
									cdr.setPlatformName(platform.getPlatform());
									cdr.setSystemId(platform.getSystemid());
								}
								cdr=cdrBuilder.save(cdr);
								logger.debug(logHeader+" CDR "+cdr.getId()+" updated with Adnetwork "+adNetwork.getSourcename());
								
							}else{
								newLine +=",subscriber Updated but CDR not found for id "+cdrId;
							}
							cdr=null;
						}
						
					}catch(Exception e1){
						erroFlag=true;
						erroMessage="Exception while updating CDR id "+cdrId+" , "+e1.getMessage();
					}
					
					
				}else{
					erroFlag=true;
					erroMessage="Invalid Subscriber Id "+subscriberid;
				}
			}else{
				erroFlag=true;
				erroMessage="Invalid VendorName and SystemId";
			}
		}catch(Exception e){
			erroFlag=true;
			erroMessage="Exception "+e.getMessage()+","+newLine;
		}finally{
			try{
				String fileNameStatus=offlineCDRFolder+"/processed/vu."+fileName+((erroFlag==true)?"failed":"success");
				FileWriter fw=new FileWriter(fileNameStatus,true);
				fw.write("\n"+newLine+"|"+erroMessage);
				fw.flush();
				fw.close();
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}*/
	
	/*
	public void  processVenderRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		SubscriberVendorUpdateRecord newRecord=null;
		try{
			newRecord=new SubscriberVendorUpdateRecord();
			newRecord.parseRecord(logger, logHeader, newLine);
			if(newRecord.getCustomerId()!=null && !"NA".equalsIgnoreCase(newRecord.getCustomerId())){
				if(newRecord.getAdNetworkName()!=null && !"NA".equalsIgnoreCase(newRecord.getAdNetworkName())){
					if(newRecord.getFirstSubscribeDate()!=null){
						TrafficSource adNetwork=getTrafficSourceByName(logHeader,newRecord.getAdNetworkName());
						if(adNetwork!=null && adNetwork.getTsourceid()>=0 ){
							String cdrId="NA";
							try{
								Subscriber sub=subscriberDao.findByCustomerId(newRecord.getCustomerId());
								if(sub!=null && sub.getId()!=null){
									sub.setTrafficSourceId(adNetwork.getTsourceid());
									sub.setTrafficSourceName(adNetwork.getSourcename());
									if(newRecord.getFirstSubscribeDate()!=null)
										sub.setValidityStartDate(newRecord.getFirstSubscribeDate());
									cdrId=sub.getJocgCDRId();
									sub=subMgr.save(sub);
									logger.debug(logHeader+" Subscriber "+sub.getId()+" updated with Adnetwork "+sub.getTrafficSourceName()+" and system id "+sub.getSystemId());
									sub=null;
								}else{
									erroFlag=true;
									erroMessage="Subscriber not found for Customer id "+newRecord.getCustomerId();
								}
							}catch(Exception e1){
								erroFlag=true;
								erroMessage="Exception while updating Customer id "+newRecord.getCustomerId()+" , "+e1.getMessage();
							}
							try{
								if("NA".equalsIgnoreCase(cdrId)){
									ChargingCDR cdr=chargingCDRDao.findById(cdrId);
									if(cdr!=null && cdr.getId()!=null){
										cdr.setTrafficSourceId(adNetwork.getTsourceid());
										cdr.setTrafficSourceName(adNetwork.getSourcename());
										cdr=cdrBuilder.save(cdr);
										logger.debug(logHeader+" CDR "+cdr.getId()+" updated with Adnetwork "+newRecord.getAdNetworkName());
										
									}else{
										newLine +=",subscriber Updated but CDR not found for id "+cdrId;
									}
									cdr=null;
								}
								
							}catch(Exception e1){
								erroFlag=true;
								erroMessage="Exception while updating CDR id "+cdrId+" , "+e1.getMessage();
							}
							
							adNetwork=null;
						}else{
							erroFlag=true;
							erroMessage="Adnetwork not registered";
						}
						
					}else{
						erroFlag=true;
						erroMessage="Invalid First Subscribe Date";
					}
				}else{
					erroFlag=true;
					erroMessage="Invalid AdNetwork Name";
				}
				
			}else{
				erroFlag=true;
				erroMessage="Invalid Customer Id";
			}
		}catch(Exception e){
			erroFlag=true;
			erroMessage="Exception "+e.getMessage()+","+newRecord;
		}finally{
			try{
				String fileNameStatus=offlineCDRFolder+"/processed/vu."+fileName+((erroFlag==true)?"failed":"success");
				FileWriter fw=new FileWriter(fileNameStatus,true);
				fw.write("\n"+newLine+"|"+erroMessage);
				fw.flush();
				fw.close();
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}*/
	
	/*public void  processRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		SubscriberUpdateRecord newRecord=null;
		try{
			newRecord=new SubscriberUpdateRecord();
			newRecord.parseRecord(logger, logHeader, newLine);
			if(newRecord.getSubscriberid()!=null && !"NA".equalsIgnoreCase(newRecord.getSubscriberid())){
				if(newRecord.getAdNetworkName()!=null && !"NA".equalsIgnoreCase(newRecord.getAdNetworkName())){
					if(newRecord.getSystemId()!=null && !"NA".equalsIgnoreCase(newRecord.getSystemId())){
						Platform platform=getPlatformBySystemId(logHeader,newRecord.getSystemId());
						TrafficSource adNetwork=getTrafficSourceByName(logHeader,newRecord.getAdNetworkName());
						if(platform!=null && adNetwork!=null && adNetwork.getTsourceid()>=0 && platform.getPlatformid()>0){
							String cdrId="NA";
							try{
								Subscriber sub=subscriberDao.findById(newRecord.getSubscriberid());
								
								if(sub!=null && sub.getId()!=null){
									sub.setPlatformId(platform.getPlatform());
									sub.setSystemId(platform.getSystemid());
									sub.setTrafficSourceId(adNetwork.getTsourceid());
									sub.setTrafficSourceName(adNetwork.getSourcename());
									cdrId=sub.getJocgCDRId();
									sub=subMgr.save(sub);
									logger.debug(logHeader+" Subscriber "+sub.getId()+" updated with Adnetwork "+sub.getTrafficSourceName()+" and system id "+sub.getSystemId());
									sub=null;
								}else{
									erroFlag=true;
									erroMessage="Subscriber not found for id "+newRecord.getSubscriberid();
								}
							}catch(Exception e1){
								erroFlag=true;
								erroMessage="Exception while updating subscribed id "+newRecord.getSubscriberid()+" , "+e1.getMessage();
							}
							try{
								ChargingCDR cdr=chargingCDRDao.findById(cdrId);
								if(cdr!=null && cdr.getId()!=null){
									cdr.setPlatformName(platform.getPlatform());
									cdr.setSystemId(platform.getSystemid());
									cdr.setTrafficSourceId(adNetwork.getTsourceid());
									cdr.setTrafficSourceName(adNetwork.getSourcename());
									cdr=cdrBuilder.save(cdr);
									logger.debug(logHeader+" CDR "+cdr.getId()+" updated with Adnetwork "+newRecord.getAdNetworkName()+" and system id "+newRecord.getSystemId());
									
								}else{
									newLine +=",subscriber Updated but CDR not found for id "+newRecord.getJocgCDRId();
								}
								cdr=null;
								if(newRecord.getJocgCDRId()!=null && !"NA".equalsIgnoreCase(newRecord.getJocgCDRId()) && !cdrId.equalsIgnoreCase(newRecord.getJocgCDRId()) ){
									cdr=chargingCDRDao.findById(newRecord.getJocgCDRId());
									if(cdr!=null && cdr.getId()!=null){
										cdr.setPlatformName(platform.getPlatform());
										cdr.setSystemId(platform.getSystemid());
										cdr.setTrafficSourceId(adNetwork.getTsourceid());
										cdr.setTrafficSourceName(adNetwork.getSourcename());
										cdr=cdrBuilder.save(cdr);
										logger.debug(logHeader+" CDR "+cdr.getId()+" updated with Adnetwork "+newRecord.getAdNetworkName()+" and system id "+newRecord.getSystemId());
										
									}else{
										newLine +=",subscriber Updated but CDR not found for id "+newRecord.getJocgCDRId();
									}
									cdr=null;
								}
							}catch(Exception e1){
								erroFlag=true;
								erroMessage="Exception while updating subscribed id "+newRecord.getSubscriberid()+" , "+e1.getMessage();
							}
							platform=null;
							adNetwork=null;
						}else{
							erroFlag=true;
							erroMessage="Either System Id or Adnetwork not registered";
						}
						
					}else{
						erroFlag=true;
						erroMessage="Invalid System id";
					}
				}else{
					erroFlag=true;
					erroMessage="Invalid Subscriber id";
				}
				
			}else{
				erroFlag=true;
				erroMessage="Invalid Subscriber id";
			}
		}catch(Exception e){
			erroFlag=true;
			erroMessage="Exception "+e.getMessage()+","+newRecord;
		}finally{
			try{
				String fileNameStatus=offlineCDRFolder+"/processed/"+fileName+"."+((erroFlag==true)?"failed":"success");
				FileWriter fw=new FileWriter(fileNameStatus,true);
				fw.write("\n"+newLine+"|"+erroMessage);
				fw.flush();
				fw.close();
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}*/
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
	public Platform getPlatformBySystemId(String logHeader,String systemId){
		 Platform pl=null;
		 try{
			Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
			Iterator<Platform> i=platfromList.values().iterator();
			while(i.hasNext()){
				pl=i.next();
				if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
				else pl=null;
			}
		 }catch(Exception e){
			 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
		 }
		 return pl;
	 }
	
}
