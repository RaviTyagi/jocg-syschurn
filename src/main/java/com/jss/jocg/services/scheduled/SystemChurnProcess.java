package com.jss.jocg.services.scheduled;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;

@Component
public class SystemChurnProcess {
	
	private static final Logger logger = LoggerFactory
			.getLogger(SystemChurnProcess.class);
	
@Autowired SubscriberRepository subscriberDao;
@Autowired JocgCacheManager jocgCache;
@Autowired ChargingCDRRepository chargingCDRDao;
@Autowired CDRBuilder cdrBuilder;
@Autowired SubscriptionManager subMgr;
	
	@SuppressWarnings("deprecation")
	@Scheduled(initialDelay=1, fixedRate=86400000)
	public void schedularThread() {
		String logHead="SYSTEMCHURN::";
		String operatorParmas="";
		Subscriber sub=null;
		int ppId=0;
		if(jocgCache.isOfflineProcessEnabled()){
			Collection<ChargingInterface> chintfList=jocgCache.getChargingInterface().values();
			for(ChargingInterface chintf:chintfList){
				if(chintf.getChintfid()!=25 && chintf.getChintfid()!=34 ){
				logger.debug(logHead+" Processing for Charging Interface "+chintf.getName()+"--->"+chintf.getChintfid());
				List<ChargingPricepoint> chppList=jocgCache.getPricePointListByChargingInterface(chintf.getChintfid());		
				boolean changePricePoint=false;
				for(ChargingPricepoint ch:chppList){
					changePricePoint=false;
					  if(ch.getChintfid()==chintf.getChintfid())
						  operatorParmas=ch.getOpParams();	
					  		ppId=ch.getPpid();
					  		if(ppId>=630 && ppId <=632){
					  			
					  			 logger.debug(logHead+"Fetched ppID :"+ppId+" is barred for System Churn Process");
					  		}
					  		else{
					  logger.debug(logHead+" Fetched OperatorParams "+operatorParmas+" For pricePointId "+ppId+ " For Operator "+chintf.getName());
					  
					  while(!changePricePoint){
						  List<Subscriber> subList=subscriberDao.searchSystemChurnSubscribers(ppId);					  
							 
							int parkingPeriod = 0;
							int gracePeriod = 0;
							int suspensionPeriod = 0;
				 
						 java.util.Date d=new java.util.Date(System.currentTimeMillis());				 				 
						 java.util.Calendar parkCal = Calendar.getInstance();
						 java.util.Calendar graceCal = Calendar.getInstance();
						 java.util.Calendar suspenCalFromGrace = Calendar.getInstance();
						 java.util.Calendar suspenCalFromPark = Calendar.getInstance();
					     String oppParmTemp=operatorParmas.replace("|", "<>");
					     java.util.Date parkDeactivationDate=parkCal.getTime();
					     java.util.Date graceDeactivationDate=parkCal.getTime();
					     java.util.Date suspendParkDeactivationDate=parkCal.getTime();
					     java.util.Date suspendGraceDeactivationDate=parkCal.getTime();

					     
					        if(!subList.isEmpty()){
									 for(Subscriber s:subList){
										 s.setReportGenerated(true);
										 subscriberDao.save(s);
										 logger.debug(logHead+" Processing Subscriber id  "+s.getId()+" msisdn  "+s.getMsisdn()+"");
										 StringBuilder sb=new StringBuilder();
										 try{
										  String opParamValues[] = oppParmTemp.split("<>");
									        for (String a : opParamValues) {
										 if (a.contains("PARKING")) {
								                String temp[] = a.split(":");
								                parkingPeriod = Integer.valueOf(temp[1]);
								                parkCal.setTime(s.getValidityEndDate());
								                parkCal.add(Calendar.DATE, +parkingPeriod);
								                parkDeactivationDate = parkCal.getTime();
								            } else if (a.contains("GRACE")) {
								                String temp[] = a.split(":");						                
								                gracePeriod = Integer.valueOf(temp[1]);
								                graceCal.setTime(s.getValidityEndDate());						                
								                graceCal.add(Calendar.DATE, +gracePeriod);
								                graceDeactivationDate = graceCal.getTime();
								            } else if (a.contains("SUSPENSION")) {
								                String temp[] = a.split(":");
								                suspensionPeriod = Integer.valueOf(temp[1]);
								                suspenCalFromGrace.setTime(s.getValidityEndDate());
								                suspenCalFromGrace.add(Calendar.DATE, +suspensionPeriod+gracePeriod);
								                suspendGraceDeactivationDate = suspenCalFromGrace.getTime();
								                suspenCalFromPark.setTime(s.getValidityEndDate());
								                suspenCalFromPark.add(Calendar.DATE, +suspensionPeriod+parkingPeriod);
								                suspendParkDeactivationDate = suspenCalFromPark.getTime();
								            	}
									        }	
									        sb.append("params extracted");
										 if((s.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE ||  s.getStatus()==JocgStatusCodes.SUB_FAILED_TRANS ||  s.getStatus()==JocgStatusCodes.SUB_FAILED_CG ||s.getStatus()==JocgStatusCodes.SUB_SUCCESS_CG || s.getStatus()==JocgStatusCodes.SUB_SUCCESS_PENDING)&& (s.getValidityEndDate()==null || d.after(suspendGraceDeactivationDate))){
											 s.setUnsubDate(d);
												s.setLastUpdated(d);
												s.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
												s.setStatusDescp("System Churn");
												s.setChurnProcessed(true);
												s=subMgr.save(s);
												logger.debug(logHead+"Updated Subscriber "+s);
												 buildCDR(logHead,s);
												
										 }
										 else if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_PARKING && (s.getValidityEndDate()==null ||  d.after(suspendGraceDeactivationDate))){
												logger.debug(logHead+"AutoChurn for ActiveUser With Expire Validity+Parking "+s.getMsisdn()+" ppId "+s.getPricepointId());	
												logger.debug(logHead+" Validity+Parking  "+parkDeactivationDate+" less then Current Time : "+d+ " So Pushed to InVolChurn");
											    s.setUnsubDate(d);
												s.setLastUpdated(d);
												s.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
												s.setStatusDescp("System Churn");
												s.setChurnProcessed(true);
												s=subMgr.save(s);
												logger.debug(logHead+"Updated Subscriber "+s);
												 buildCDR(logHead,s);
																	 
										 }
										 else if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_GRACE && (s.getValidityEndDate()==null ||  d.after(suspendGraceDeactivationDate))){
												logger.debug(logHead+"AutoChurn for GraceUser With Expire Validity+Grace "+s.getMsisdn()+" ppId "+s.getPricepointId());	
												logger.debug(logHead+" Validity+Grace  "+graceDeactivationDate+" less then Current Time : "+d+ " So Pushed to InVolChurn");
											    s.setUnsubDate(d);
												s.setLastUpdated(d);
												s.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
												s.setStatusDescp("System Churn");
												s.setChurnProcessed(true);
												s=subMgr.save(s);
												logger.debug(logHead+"Updated Subscriber "+s);
												 buildCDR(logHead,s);
											
										 }
										 else if(s.getStatus()==JocgStatusCodes.SUB_FAILED_SUSPENDFROMGRACE && (s.getValidityEndDate()==null ||  d.after(suspendGraceDeactivationDate))){
												logger.debug(logHead+"AutoChurn for SUSPEND-From-Grace With Expire Validity+Grace+Suspend "+s.getMsisdn()+" ppId "+s.getPricepointId());	
												logger.debug(logHead+" Validity+Grace+Suspend  "+suspendGraceDeactivationDate+" less then Current Time : "+d+ " So Pushed to InVolChurn");
											    s.setUnsubDate(d);
												s.setLastUpdated(d);
												s.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
												s.setStatusDescp("System Churn");
												s.setChurnProcessed(true);
												s=subMgr.save(s);
												logger.debug(logHead+"Updated Subscriber "+s);
												 buildCDR(logHead,s);
											
										 }
										 else if(s.getStatus()==JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING && (s.getValidityEndDate()==null ||  s.getNextRenewalDate()==null || s.getValidityEndDate().before(suspendParkDeactivationDate))){
												logger.debug(logHead+"AutoChurn for SUSPEND-From-Parking With Expire Validity+Parking+Suspend "+s.getMsisdn()+" ppId "+s.getPricepointId());	
												logger.debug(logHead+" Validity+Parking+Suspend  "+suspendParkDeactivationDate+" less then Current Time : "+d+ " So Pushed to InVolChurn");
											    s.setUnsubDate(d);
												s.setLastUpdated(d);
												s.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
												s.setStatusDescp("System Churn");
												s.setChurnProcessed(true);
												s=subMgr.save(s);
												logger.debug(logHead+"Updated Subscriber "+s);
												 buildCDR(logHead,s);
											 
										 }else{
											 	s.setChurnProcessed(true);
											 	s=subMgr.save(s);												
												logger.info("No change for subscriber id :"+s.getId()+" s.getValidityEndDate() :"+s.getValidityEndDate()+" s.getStatus() :"+s.getStatus()+" SuspendGraceDeactivationDate :"+suspendGraceDeactivationDate+" SuspendParkDeaxctivationDate :"+suspendParkDeactivationDate+"   D "+d);
										 }
										 sb.append("|processcompleted");
										 }catch(Exception e){
											 logger.error("Exception while processing subscriber "+e+", Exception "+e+", Step "+sb.toString());
										 }
									 }
					        }else changePricePoint=true;
						  
						  
						  
					  }				  
					  ch=null;
				} // End Else
				  }
			} // End Else of Chintf Check 
				else{
					logger.debug(logHead+"Fetched ChargingInterface Id :"+chintf.getChintfid()+" is barred for System Churn Process");
				}
			}
		}
	}
	public void buildCDR(String logHeader,Subscriber sub){
		logHeader +="buildCDR ::";
		try{
			logger.debug(logHeader+"Method Invoked ..");
		ChargingInterface sourceNetwork=jocgCache.getChargingInterface(sub.getSourceNetworkId());
		logger.debug(logHeader+"sourceNetwork "+sourceNetwork);
		ChargingInterface chintf=jocgCache.getChargingInterface(sub.getChargingInterfaceId());
		logger.debug(logHeader+"chintf "+chintf);
		PackageChintfmap pkgchintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
		logger.debug(logHeader+"pkgchintf "+pkgchintf);
		Circleinfo circle=jocgCache.getCircleInfo(sub.getCircleId());
		if(circle==null) circle=new Circleinfo();
		logger.debug(logHeader+"circle "+circle);
		PackageDetail pkg=jocgCache.getPackageDetailsByName(sub.getPackageName());
		logger.debug(logHeader+"pkg "+pkg);
		Service service=jocgCache.getServicesById(pkg.getServiceid());
		logger.debug(logHeader+"service "+service);
		ChargingPricepoint chppmaster=jocgCache.getPricePointListById(sub.getPricepointId());
		logger.debug(logHeader+"service "+chppmaster);
		ChargingPricepoint chpp=chppmaster;//must be changed when fallback is implemented
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		java.util.Date chargingDate=cal.getTime();
		cal.add(Calendar.DATE, chpp.getValidity());
		java.util.Date validityEndDate=cal.getTime();
		TrafficSource ts=null;TrafficCampaign tc=null;
		try{
		ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
		logger.debug(logHeader+"traffic Source "+ts);
		tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
		logger.debug(logHeader+"traffic Campaign "+tc);
		}catch(Exception e1){
			logger.error(logHeader+"Exception while searching traffic source id "+sub.getTrafficSourceId()+", and campaign "+sub.getCampaignId());
		}
		ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), "SYSTEM-CHURN", "DCT", sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
				new JocgRequest(), sourceNetwork, chintf, pkgchintf, circle, pkg, service, chppmaster, chpp, null, sub.getChargingCategory(), 0D, chpp.getPricePoint(), "SYSTEM-CHURN", 
				chargingDate, sub.getValidityStartDate(), validityEndDate, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
				null, null, null, -1, "NA", JocgStatusCodes.UNSUB_SUCCESS, "System Churn Successful", new java.util.Date(System.currentTimeMillis()), "NA",
				"NA", "NA", new java.util.Date(System.currentTimeMillis()), "", "", "NA", ""+sub.getMsisdn(), new java.util.Date(), 
				null, null, false, false, false);
		logger.debug(logHeader+"Inserting new InVolChurn CDR......");
		newCDR=cdrBuilder.insert(newCDR);
		logger.info(logHeader+"buildCDR():: InVolChurn CDR Created with ID "+newCDR.getId());
		}catch(Exception e){
			logger.error(logHeader+"buildCDR()::"+e.getMessage());
		}
	
	}
	
	
	
}
