package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cdr.dao.EventCDRRepository;
import com.jss.jocg.cdr.entities.EventCDR;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.ChargingCDRRepositoryImpl;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JocgString;

@Component
public class SubscriberReportingService {
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriberReportingService.class);
	@Autowired ChargingCDRRepository notifyCDRDao;
	@Autowired EventCDRRepository eventCDRDao;
	@Autowired JocgCacheManager jocgCache;
	
	@Value("${jocg.reports.triggerfile}")
	String triggerFile;
	
 //@Scheduled(initialDelay=1, fixedRate=60000)
	   public void schedularThread() {
	 
	 if(jocgCache.isOfflineProcessEnabled()){
	 		try{
	 			
			 	String logHeader="REPORTING :: ";	
			 	ReportTriggers triggers=new ReportTriggers();
			 	triggers.extractTriggers(logHeader, triggerFile);
			 	logger.debug("Triggers "+triggers+", enabled :"+triggers.getEnabled()+", LastUpdate "+triggers.getLastUpdate()+", Start from Init "+triggers.getStartFromInit() );
			 	
			 	if(triggers.getEnabled()==true){
			 		if(triggers.getStartFromInit()){
			 			triggers.setStartFromInit(false);
			 			boolean startProcess=true;java.util.Date lastPickedDate=null;
			 			logger.debug(logHeader+"Starting from INIT");
			 			List<ChargingCDR> cdrList=null;
			 			while(startProcess){
			 				cdrList=notifyCDRDao.findQueuedReportCDR("REPORTING", 1000, new java.util.Date(), true);
			 				if(cdrList.isEmpty()) startProcess=false;
			 				else{
			 					lastPickedDate=cdrList.get(cdrList.size()-1).getLastUpdated();
			 					triggers.setLastUpdate(lastPickedDate);
			 					triggers.saveTriggers(logHeader, triggerFile);
			 				}
			 				
			 				int updatedCount=updateCDRList(cdrList);
			 				logger.debug(logHeader+"INIT-Picked CDR Count "+cdrList.size()+" Updated Count "+updatedCount);
			 				cdrList=null;
			 				WAIT(10);
			 			}
			 			
			 			
			 		}else{ 
			 			Calendar cal=Calendar.getInstance();
			 			cal.setTimeInMillis(System.currentTimeMillis());
			 			cal.set(Calendar.HOUR, 0);
			 			cal.set(Calendar.MINUTE, 0);
			 			cal.set(Calendar.SECOND, 0);
			 			if(triggers.getLastUpdate()==null) triggers.setLastUpdate(cal.getTime());
			 			logger.debug(logHeader+"Picking Records after "+triggers.getLastUpdate());
			 			boolean startProcess=true;java.util.Date lastPickedDate=null;
			 			List<ChargingCDR> cdrList=null;
			 			while(startProcess){
				 			cdrList=notifyCDRDao.findQueuedReportCDR("REPORTING", 1000, triggers.getLastUpdate(), false);
				 			if(cdrList.isEmpty()) startProcess=false;
			 				else{
			 					lastPickedDate=cdrList.get(cdrList.size()-1).getLastUpdated();
			 					triggers.setLastUpdate(lastPickedDate);
			 					triggers.saveTriggers(logHeader, triggerFile);
			 				}
				 			int updatedCount=updateCDRList(cdrList);
			 				logger.debug(logHeader+"NON-INIT-Picked CDR Count "+cdrList.size()+" Updated Count "+updatedCount);
			 				cdrList=null;
			 				WAIT(10);
			 			}
			 			
			 		}
			 	}
	 		}catch(Exception e){
	 			e.printStackTrace();
	 		}
	 } else  logger.debug("REPORTING PROCESS :: Offline Processes are disabled on this server ");
		 	
		 	
	 }
	 
 
	 public synchronized void WAIT(int ms){
		 try{
			 this.wait(ms);
		 }catch(Exception e){}
	 }
	 
	public int updateCDRList(List<ChargingCDR> cdrList){
		int updatedCount=0;
		try{
			for(ChargingCDR cdr:cdrList){
				EventCDR eventCDR=getEventCDR(cdr);
				try{
					eventCDR.setId(cdr.getId());
					eventCDRDao.save(eventCDR);
					updatedCount++;
				}catch(Exception e1){
					
					logger.error("Failed to Save CDR "+e1);
				}
				WAIT(2);
			}
		}catch(Exception e){
			logger.error("Failed to Convert CDR "+e);
		}
		return updatedCount;
	}
	 
	public EventCDR getEventCDR(ChargingCDR cdr){
		EventCDR eCdr=null;
		try{
			eCdr=new EventCDR(cdr.getId(),cdr.getSubRegId(),cdr.getLandingPageId(),
					cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),
					cdr.getRequestType(),cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),
					cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),
					cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),
					cdr.getServiceCategory(),cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),
					cdr.getMppOperatorKey(),"",cdr.getChargedPpid(),cdr.getChargedPricePoint(),
					cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),""
					,cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),
					cdr.getChargingDate(),cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),
					cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),
					cdr.getPublisherId(),cdr.getNotifyStatus(),"",cdr.getNotifyTime(),cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),
					cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),"",cdr.getRequestDate(),cdr.getCgStatusCode(),"",
					cdr.getCgTransactionId(),cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),
					cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),cdr.getLastUpdated(),true,cdr.getActivatedFromZero(),cdr.getActivatedFromGrace(),cdr.getActivatedFromParking(),
					cdr.getSubRegDate(),cdr.getChurn24Hour());
			
			System.out.println("Reporting: ActivatedFromZero : Source Value  "+cdr.getActivatedFromZero()+", Dest Value "+eCdr.getActivatedFromZero());
		}catch(Exception e){
			e.printStackTrace();
		}
		return eCdr;
	}
	
	public EventCDR getEventCDROld(ChargingCDR cdr){
		EventCDR eCdr=null;
		try{
			eCdr=new EventCDR(cdr.getId(),cdr.getSubRegId(),cdr.getLandingPageId(),
					cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),
					cdr.getRequestType(),cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),
					cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),
					cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),
					cdr.getServiceCategory(),cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),
					cdr.getMppOperatorKey(),cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),
					cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),cdr.getChargedOperatorParams()
					,cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),
					cdr.getChargingDate(),cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),
					cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),
					cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),
					cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),
					cdr.getCgTransactionId(),cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),
					cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),cdr.getLastUpdated(),true,cdr.getActivatedFromZero(),cdr.getActivatedFromGrace(),cdr.getActivatedFromParking(),
					cdr.getSubRegDate(),cdr.getChurn24Hour());
			
			System.out.println("Reporting: ActivatedFromZero : Source Value  "+cdr.getActivatedFromZero()+", Dest Value "+eCdr.getActivatedFromZero());
		}catch(Exception e){
			e.printStackTrace();
		}
		return eCdr;
	}
	public class ReportTriggers{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		java.util.Date lastUpdate;
		Boolean startFromInit;
		Boolean enabled;
		public ReportTriggers(){}
		public void extractTriggers(String logHeader,String triggerFileName){
			String triggerData="NA";
			logger.debug(logHeader+"Trigger File "+triggerFileName);
			BufferedReader br=null;
			try{
				br=new BufferedReader(new FileReader(triggerFile));
				triggerData=br.readLine();
				
			}catch(Exception e){ triggerData="NA";}
			finally{
				try{if(br!=null) br.close(); }catch(Exception e){}
				br=null;
				if("NA".equalsIgnoreCase(triggerData)) triggerData="lastupdate=NA,init=true,enabled=false";
			}
			
			try{
				//Parse Data
				String[] str=triggerData.split(",");
				String[] str1=null;
				for(String s:str){
					logger.debug(logHeader+"Processing "+s);
					str1=s.split("=");
					if(str1!=null && str1.length>=2){
						logger.debug(logHeader+str1[0]+"="+str1[1]);
						if("init".equalsIgnoreCase(str1[0])){
							this.startFromInit=JocgString.strToBool(str1[1], false);
						}else if("enabled".equalsIgnoreCase(str1[0])){
							this.enabled=JocgString.strToBool(str1[1], false);
						}else if("lastupdate".equalsIgnoreCase(str1[0]) && !"NA".equalsIgnoreCase(str1[1])){
							this.lastUpdate=sdf.parse(str1[1]);
						}
					}
				}
				logger.debug("Processed Successfully! "+this.startFromInit);
			}catch(Exception e){}
			
		}
		
		public void saveTriggers(String logHeader,String triggerFileName){
			StringBuilder sb=new StringBuilder();
			sb.append("init=").append(this.startFromInit.booleanValue()).append(",");
			sb.append("enabled").append("=").append(this.enabled.booleanValue()).append(",");
			sb.append("lastUpdate").append("=").append(sdf.format(this.lastUpdate));
			try{
				FileWriter bw=new FileWriter(triggerFileName);
				bw.write(sb.toString());
				bw.flush();
				bw=null;
			}catch(Exception e){
				
			}
		}
		public SimpleDateFormat getSdf() {
			return sdf;
		}
		public void setSdf(SimpleDateFormat sdf) {
			this.sdf = sdf;
		}
		public java.util.Date getLastUpdate() {
			return lastUpdate;
		}
		public void setLastUpdate(java.util.Date lastUpdate) {
			this.lastUpdate = lastUpdate;
		}
		public Boolean getStartFromInit() {
			return startFromInit;
		}
		public void setStartFromInit(Boolean startFromInit) {
			this.startFromInit = startFromInit;
		}
		public Boolean getEnabled() {
			return enabled;
		}
		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}
		
		
	}
}
