package com.jss.jocg.services.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cdr.dao.EventCDRRepository;
import com.jss.jocg.services.dao.ChargingCDRRepository;

@Component
public class SMSAlertHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(SMSAlertHandler.class);

	@Autowired JocgCacheManager jocgCache;
	
	//@Scheduled(initialDelay=1, fixedRate=3600000)
	public void schedularThread() {
		String logHeader="SMSALERT::";
		if(jocgCache.isOfflineProcessEnabled()){
			TrafficStats ts=jocgCache.getTrafficStats(2, 2, 24, "NA");
			logger.debug(logHeader+" Traffic Stats "+ts);
			logger.debug(logHeader+" Sending SMS  TotalCount "+ts.getTotalCount()+", SuccessCount "+ts.getSuccessCount()+",Sent Count "+ts.getSentCount());
		}
	}
	
	
}
