package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.callback.data.IdeaSdpCallback;
import com.jss.jocg.callback.data.bsnl.NetExcelBSNLSDPCallback;
import com.jss.jocg.callback.data.bsnl.PyroBSNLSDPCallback;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.sms.data.JocgSMSMessage;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;

@Component
public class BSNLSTVOfflineCallbackProcessor extends JocgService{
	private static final Logger logger = LoggerFactory
			.getLogger(BSNLSTVOfflineCallbackProcessor.class);
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMdd");
	@Value("${jocg.offlinecdr.bsnlstv.folder}")
	String offlineCDRFolder;
	
	
	
// @Scheduled(initialDelay=1, fixedRate=700000)
 public void processFiles(){
	 if(jocgCache.isOfflineProcessEnabled()){
		 try{
			 logger.debug("BSNLSTV-OFFLINE-PROCESSOR Invoked ");
			 File fd=new File(offlineCDRFolder);
			 logger.debug("BSNLSTV-OFFLINE-PROCESSOR : Base folder "+offlineCDRFolder);
			 
			 logger.debug("BSNLSTV-OFFLINE-PROCESSOR :  fd.exists() : "+fd.exists());
			 logger.debug("BSNLSTV-OFFLINE-PROCESSOR :  fd.isDirectory() : "+fd.isDirectory());
			 
			 if(fd.exists() && fd.isDirectory()){
				File[] flist=fd.listFiles();
				for(File f:flist){
					 logger.debug("BSNLSTV-OFFLINE-PROCESSOR : Processing file  "+f.getAbsolutePath());
					if(!f.isDirectory() && ((System.currentTimeMillis()-f.lastModified())/1000)>60){
						 BufferedReader br=new BufferedReader(new FileReader(f));
		                    String newLine="";int processedRecords=0;
		                    while((newLine=br.readLine())!=null){		                        
		                           processRecord(newLine);
		                            processedRecords++;
		                      
		                            WAIT(100);
		                    }
		                    logger.info("BSNLSTV-OFFLINE-PROCESSOR :: Total Record Count in ("+f.getCanonicalPath()+") :"+processedRecords);
		                    br.close();
		                    br=null;
		                    try{
		                    	f.renameTo(new File(offlineCDRFolder+"/processed/"+f.getName()));
		                    }catch(Exception ee){
		                    	f.delete();
		                    }
		                  WAIT(2000);
		                  
					}//if
					else  logger.debug("BSNLSTV-OFFLINE-PROCESSOR : File Skipped.");
					f=null;
				}//for
			 }//if
			 fd=null;
		 }catch(Exception e){
			 logger.debug("BSNLSTV-OFFLINE-PROCESSOR:: Exception  : "+e);
		 }
	 } else  logger.debug("BSNLSTV-OFFLINE-PROCESSOR:: Offline Processes are disabled on this server ");
 }
 
 public synchronized void WAIT(int ms){
	 try{
		this.wait(ms);
	 }catch(Exception e){}
	 
 }
 
 public void processRecord(String newRecord){	 
     try{
    	 Double chargedPrice=0D;
    	 String serviceKey="";
         if(newRecord.contains(",") && newRecord.contains("DigiVive")){
        	 int denomination=0;
        	 NetExcelBSNLSDPCallback callbackData=new NetExcelBSNLSDPCallback();
        	 if(!newRecord.contains("TRANSDATE")){
        		 //TRANSDATE,ZONENAME,CIRCLENAME,SOURCE_MSISDN,DMSISDN,DENOMINATION,CP_NAME,STATUS
        		 //08-NOV-2017,West,Madhya Pradesh,3300000002,9407138093,103,DigiVive,SUCCESS
        		 SimpleDateFormat sdfdate=new SimpleDateFormat("dd-MMM-yy");
        		 /*
        		  	SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yy");
        			java.util.Date startDate=sdf.parse("12-NOV-17");
        			System.out.println(">"+startDate);        
        		  */
        		 String[] strArr=newRecord.split(",");
        		 denomination=JocgString.strToInt(strArr[5], 0);
        		 logger.debug("DEBUG Logger : BSNLSTVOffline Callback for Date : "+strArr[0]);
        		 java.util.Date startDate=sdfdate.parse(strArr[0]);        		 
        		 callbackData.setStvPackStartDate(startDate);
        		 callbackData.setZoneName("East".equalsIgnoreCase(strArr[1])?"BE_OF":
        			 "West".equalsIgnoreCase(strArr[1])?"BW_OF":
        				 "North".equalsIgnoreCase(strArr[1])?"BN_OF":"BS_OF");
        		 String destMsisdn=JocgString.formatString(strArr[4], "0");        	        		 
        		 destMsisdn=(destMsisdn.length()<=10)?"91"+destMsisdn:destMsisdn;
        		 int operatorId=9;
        		 String validity=denomination==373?"56":(denomination==179)?"22":"15";
        		 MsisdnSeries circle=circleIndentifier.identifyMsisdnSeriesByMsisdn(Long.valueOf(destMsisdn));
//        		 logger.debug("BSNLSTV-OFFLINE-PROCESSOR : Circle : "+circle.getCircleid());     		         		 
        		 
        			if(circle!=null && circle.getCircleid()>0){
        				operatorId=circle.getChintfid(); 
        			// Ravi Here 
        			}
        			if(circle==null ){
        				operatorId=callbackData.getZoneName().equals("BN_OF")?9:callbackData.getZoneName().equals("BE_OF")?8:callbackData.getZoneName().equals("BS_OF")?11:10;
        			}
        			logger.debug("BSNLSTV-OFFLINE-PROCESSOR : chintf : "+operatorId);
        			String bsnlStvPacks=jocgCache.getBsnlSTVPacks();	
        			logger.debug("BSNLSTV-OFFLINE-PROCESSOR : triggers property : "+bsnlStvPacks);
        			List<String> packList=JocgString.convertStrToList(bsnlStvPacks, "|");
        			String keyToSearch=""+operatorId+"_"+validity+"DYS";        			
        			String key="",value="";int chppId=0;
        			for(String s:packList){
        				key=s.substring(0,s.indexOf("="));
        				value=s.substring(s.indexOf("=")+1);        				
        				if(key.equalsIgnoreCase(keyToSearch)){
        					logger.debug("BSNLSTV-OFFLINE-PROCESSOR : key matched with : "+s);
        					chppId=JocgString.strToInt(value, 0);
        					if(chppId>0) break;
        				}
        			}
        			logger.debug("BSNLSTV-OFFLINE-PROCESSOR : chppId : "+chppId);
        			if(chppId>0){
        				ChargingPricepoint chpp=jocgCache.getPricePointListById(chppId);
        				logger.debug("BSNLSTV-OFFLINE-PROCESSOR : chpp : "+chpp);
        				PackageChintfmap chintfMap=jocgCache.getPackageChargingInterfaceMap(chpp.getChintfid(),chpp.getChintfid(), chpp.getPpid());
        				logger.debug("BSNLSTV-OFFLINE-PROCESSOR : chintfMap : "+chintfMap);
        				PackageDetail pkgDetail=jocgCache.getPackageDetailsByID(chintfMap.getPkgid());
        				logger.debug("BSNLSTV-OFFLINE-PROCESSOR : pkgDetail : "+pkgDetail);
        				//Now process for the Package subscription			
        				
        			}else{
        				logger.error("BSNL STV Packs : Invalid Price point Validity |msisdn="+destMsisdn+",VASID=RAVI"+System.currentTimeMillis()+"TEST,validity="+validity+", Status=Success");
        			} 
        		  if(chppId!=0){
        			ChargingPricepoint chpp=jocgCache.getPricePointListById(chppId);
        			chargedPrice=chpp.getPricePoint();
        			serviceKey=chpp.getOperatorKey();
        		    			}
        			callbackData.setMsisdn(JocgString.strToLong(destMsisdn, 0L));
        			callbackData.setPpid(chppId);
        			callbackData.setPrice(chargedPrice);
        			callbackData.setServiceId(serviceKey);        		 
        			callbackData.setReq_type("ACT");
        			callbackData.setConsent_Status(strArr[7]);        			
        			int msisdnlastDigit=(int)(JocgString.strToInt(destMsisdn, 789571371)%10000);
        			callbackData.setCg_id("DIGIV"+System.currentTimeMillis()+msisdnlastDigit);
        	 
        	 receiveMessage(callbackData);
        	 }
            
         }
     }catch(Exception e){
			logger.error("BSNL STV Packs : Exception :- "+Thread.currentThread().getStackTrace()[2].getLineNumber());
     }
 }
 
 public void receiveMessage(NetExcelBSNLSDPCallback callbackData) {
		String loggerHead="msisdn="+callbackData.getMsisdn()+"| BsnlSTVOfflineCallbackReceiver | ";	
		logger.debug("BsnlSTVOfflineCallbackReceiver :: Received ||| <" + callbackData + ">");
		try{
			
			if(callbackData!=null && callbackData.getMsisdn()>0){	
				logger.debug(loggerHead+" callbackData is valid for msisdn "+callbackData.getMsisdn()+", Key "+callbackData.getServiceId()+", Price "+callbackData.getPrice());
				ChargingPricepoint cpp=jocgCache.getPricePointListById(callbackData.getPpid());
				if(cpp==null || cpp.getPpid()<=0) {
					logger.debug(loggerHead+"Price point not found for OpKey "+callbackData.getServiceId()+", Price "+callbackData.getPrice()+", Searching against opkey only.....");
					
					cpp=jocgCache.getPricePointListById(callbackData.getPpid());
					
				}
//				ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId()):(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
				logger.debug(loggerHead+" callbackData.getPpid() : "+callbackData.getPpid());//478
				logger.debug(loggerHead+" callbackData.getServiceId(): "+callbackData.getServiceId());//478
				logger.debug(loggerHead+"cpp.getIsfallback(): "+cpp.getIsfallback());
				logger.debug(loggerHead+"cpp.getIsfallback(): 1 "+cpp.getPpid());
				ChargingPricepoint cppmaster= new ChargingPricepoint();
				if(callbackData.getPpid()!=0){
					 cppmaster=(cpp==null || cpp.getPpid()<=0)?jocgCache.getPricePointListByOperatorKey(callbackData.getServiceId()):(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
					}
					else{
						cppmaster=jocgCache.getPricePointListById(callbackData.getPpid());
					}
				
				
				logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
				
				if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
					String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
					String msisdnStr=""+callbackData.getMsisdn().longValue();
					if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+callbackData.getMsisdn().longValue(),callbackData.getMsisdn()));
					
					logger.debug(loggerHead+"Searching Subscriber for msisdn  "+callbackData.getMsisdn().longValue()+", Operator Service Key "+serviceKey);
					//To be checked-Rishi
					Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(callbackData.getMsisdn(),serviceKey);	
					ChargingCDR cdr=null;
					if(sub!=null && sub.getId()!=null){
						logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
						java.util.Date lastRenewaldate=sub.getLastRenewalDate();
						java.util.Date callbackRenewaldate=callbackData.getStvPackStartDate();
						if(("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())) && lastRenewaldate!=null && sdfDate.format(lastRenewaldate).equalsIgnoreCase(sdfDate.format(callbackRenewaldate))){
							logger.debug(loggerHead+" Duplicate Renewal Callback, Hence Ignored");
						}else if(("DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type())) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
							logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
						}else if("ACT".equalsIgnoreCase(callbackData.getReq_type()) && DateUtils.isSameDay(sub.getSubChargedDate(),callbackData.getStvPackStartDate()) && callbackData.getPpid()==sub.getPricepointId() ){
							handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
//							logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored"+callbackData.getStvPackStartDate()+"   |  "+sub.getSubChargedDate());
						}else{
							cdr=chargingCDRDao.findById(sub.getJocgCDRId());
							if(cdr!=null && cdr.getId()!=null && DateUtils.isSameDay(cdr.getChargingDate(),callbackData.getStvPackStartDate())){
								logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode());
//								handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr);
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}else{
								logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
								handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub);
							}
						}
					}else{
						logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
						handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster);						
					}
					
				}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");				
			}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");	
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			
		}
     
 }
	
	
	public Subscriber handleExistingSubscriber(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr){
		loggerHead +="HES::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false);
			subMgr.save(sub);
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) ){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>1.0){
//					JocgSMSMessage smsMessage=new JocgSMSMessage();
//					smsMessage.setMsisdn(callbackData.getMsisdn());
//					smsMessage.setSmsInterface("smsnetexcelbsnl");
//					smsMessage.setChintfId(1);
//					smsMessage.setTextMessage("Successful Activation");
//					jmsTemplate.convertAndSend("notify.sms", smsMessage);
//					//Setting Notification
//					cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//					Calendar cal=Calendar.getInstance();
//					cal.setTimeInMillis(System.currentTimeMillis());
//					cal.add(Calendar.MINUTE, 60);
//					cdr.setNotifyScheduleTime(cal.getTime());
					
				}
				cdrBuilder.save(cdr);
			}else if("DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type())){
				if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
					cdr.setNotifyChurn(true);
					cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
					cdr.setNotifyTimeChurn(new java.util.Date());
				}
				cdrBuilder.save(cdr);
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		
	
		return sub;
	}
	
	public void handleExistingSubscriberNonExistingCDR(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub){
		loggerHead +="HESNEC::";
		try{
			sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
			Circleinfo circle=new Circleinfo();
			PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
			Service service=jocgCache.getServicesById(sub.getServiceId());
			SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy");
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=callbackData.getStvPackStartDate();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			String requestType=("ACT".equalsIgnoreCase(callbackData.getReq_type()))?"ACT":
				("DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()))?"DCT":
					("REACT".equalsIgnoreCase(callbackData.getReq_type()) || "REN".equalsIgnoreCase(callbackData.getReq_type()))?"REN":callbackData.getReq_type();
			
			ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), "SDP-VASAPI"+System.currentTimeMillis(), requestCategory, requestType, sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), "MJB8Q", 
					new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
					startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
					null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated",startDate, "NA", "NA", "NA",startDate, callbackData.getConsent_Status(), callbackData.toString(), callbackData.getCg_id(), ""+callbackData.getMsisdn(), startDate, 
					null, null, false, false, false);
			
			newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
			newCDR=cdrBuilder.insert(newCDR);
			logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
			if("ACT".equalsIgnoreCase(callbackData.getReq_type())) sub.setJocgCDRId(newCDR.getId());
			sub=subMgr.save(sub);
			logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
			newCDR=null;
			sub=null;
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public void handleNonExistingSubscriber(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster){
		loggerHead +="HNES::";
		try{
			String requestCategory="OP-SDP";
			ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
			PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
			Circleinfo circle=new Circleinfo();
			PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
			Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=callbackData.getStvPackStartDate();
			cal.add(Calendar.DAY_OF_MONTH,cpp.getValidity());
			java.util.Date endDate1=cal.getTime();
			String requestType=("ACT".equalsIgnoreCase(callbackData.getReq_type()))?"ACT":
				("DECT".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()))?"DCT":
					("REACT".equalsIgnoreCase(callbackData.getReq_type()) || "REN".equalsIgnoreCase(callbackData.getReq_type()))?"REN":callbackData.getReq_type();
			if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "STV-"+callbackData.getCg_id(), requestCategory, requestType, ""+callbackData.getMsisdn(), callbackData.getMsisdn(), "NA", "MJB8Q", 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), "SDP-CALLBACK", 
						startDate, startDate, endDate1, "NA", "NA", "NA", null, null, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", startDate, "NA", "NA", "NA",startDate, callbackData.getConsent_Status(), callbackData.toString(), callbackData.getCg_id(), ""+callbackData.getMsisdn(), startDate, 
						null, null, false, false, false);
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true);
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
				
				Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+callbackData.getMsisdn(),""+callbackData.getMsisdn(),callbackData.getMsisdn(),
						"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
						chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
						pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
						cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",startDate,
						startDate,startDate,startDate,chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
						null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
						1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
						null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
						JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-CALLBACK","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
						newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
						newCDR.getVoucherDiscount(),0D,"MJB8Q","NA","OP","NA","NA",
						"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),startDate);
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster);
				sub=subMgr.save(sub);				
				logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);			
				logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(10);
				int esmeID=callbackData.getZoneName().equals("BE_OF")?2:callbackData.getZoneName().equals("BW_OF")?1:callbackData.getZoneName().equals("BN_OF")?3:4;									
				String messageToSend=jocgCache.getBsnlMessage();
				messageToSend=URLEncoder.encode(messageToSend);
				String msisdn1=Long.toString(callbackData.getMsisdn());
				String msisdn=(msisdn1.length()<=10)?"91"+msisdn1:msisdn1;
				String smsURL="http://172.31.22.128/DigiSMS/scheduler.jiffy?msisdn="+msisdn+"&sms="+messageToSend+"&esmeID="+esmeID;
				logger.debug(loggerHead+" SMS-URL"+smsURL);
				JcmsSSLClient.JcmsSSLClientResponse resp1=sslClient.invokeURL("SMS URL Invoked ",smsURL);
				logger.debug(loggerHead+" JiffySMS-URL:"+smsURL+"::Resp="+resp1.getResponseData()+":Error="+resp1.getErrorMessage()+"::RespTime="+resp1.getResponseTime());				

																		
			}else{
				logger.error("Failed to search Configurations for Callback "+callbackData);
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
	}
	
	public ChargingCDR updateCDRStatus(String loggerHead,NetExcelBSNLSDPCallback callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR){
		loggerHead +="UCS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(System.currentTimeMillis());
			java.util.Date startDate=callbackData.getStvPackStartDate();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) || "LOWBAL".equalsIgnoreCase(callbackData.getReq_type()) || "FAILED".equalsIgnoreCase(callbackData.getReq_type())){
				if("ACT".equalsIgnoreCase(callbackData.getReq_type()) && "SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>1.0) {
					logger.debug("Updating CDR for "+chargedPricePoint);
					if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
						cdr.setParkingToActivationDate(startDate);
						cdr.setActivatedFromParking(true);
					}
					
					cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					cdr.setStatusDescp("Customer Activated");
					cdr.setChargedPpid(chargedPricePoint.getPpid());
					cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
					cdr.setChargedAmount(chargedPricePoint.getPricePoint());
					cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
					cdr.setChargingDate(startDate);
					cdr.setValidityStartDate(startDate);
					cdr.setValidityEndDate(endDate1);
				
					logger.debug("Charged PpId "+cdr.getChargedPpid());
				}else if(callbackData.getPrice()<1.0){
					if("LOWBAL".equalsIgnoreCase(callbackData.getReq_type())){
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPPARKING);
						cdr.setStatusDescp("Customer Moved To Parking");
						cdr.setChargedAmount(0D);
						cdr.setChargingDate(startDate);
						cdr.setParkingDate(startDate);
					}else{
						cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
						cdr.setStatusDescp("Charging Failed at SDP");
						cdr.setChargedAmount(0D);
					}
				}else{
					cdr.setStatus(JocgStatusCodes.CHARGING_FAILED);
					cdr.setStatusDescp("Charging Failed at SDP");
					cdr.setChargedAmount(0D);
				}
				cdr.setSdpStatusCode(callbackData.getConsent_Status());
				cdr.setSdpStatusDescp(callbackData.toString());
				cdr.setSdpResponseTime(startDate);
				
			}else if("desubscription".equalsIgnoreCase(callbackData.getReq_type()) || "DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "STOP".equalsIgnoreCase(callbackData.getReq_type()) || "UNSUB".equalsIgnoreCase(callbackData.getReq_type())){
				
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(cdr.getChargingDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn=cal1.getTime();
					cal1.setTime(cdr.getRequestDate());
					cal1.add(Calendar.MINUTE,20);
					java.util.Date expiry20MinChurn1=cal1.getTime();
					cal1.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal1.getTime();
					if(cDate.before(expiry20MinChurn) || cDate.before(expiry20MinChurn1)){
						cdr.setChurn20Min(true);
					}
					if(cDate.getDate()==cdr.getRequestDate().getDay() && cDate.getMonth()==cdr.getRequestDate().getMonth() && cDate.getYear()==cdr.getRequestDate().getYear()){
						cdr.setChurnSameDay(true);
					}
					
					//Create New CDR For DCT
					logger.debug(loggerHead+" Activation CDR updated for churn status, now creating DCT CDR....");
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("DCT");
						cdr1.setRequestDate(new java.util.Date());
						cdr1.setSdpResponseTime(new java.util.Date());
						cdr1.setSdpStatusCode(callbackData.getConsent_Status());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setChargedAmount(0D);
						cdr1.setChargedPpid(0);
						cdr1.setChargedOperatorKey("NA");
						cdr1.setChargedOperatorParams("NA");
						cdr1.setChargedPricePoint(0D);
						cdr1.setChargedValidityPeriod("NA");
						cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("DCT");
						cdr.setRequestDate(new java.util.Date());
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getConsent_Status());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setChargedAmount(0D);
						cdr.setChargedPpid(0);
						cdr.setChargedOperatorKey("NA");
						cdr.setChargedOperatorParams("NA");
						cdr.setChargedPricePoint(0D);
						cdr.setChargedValidityPeriod("NA");
					}
				
			}else if("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					
					if(updateSameCDR==false){
					ChargingCDR cdr1=copyCDR(loggerHead,cdr);
					cdr1.setRequestType("REN");
					cdr1.setSdpResponseTime(new java.util.Date());
					cdr1.setSdpStatusCode(callbackData.getConsent_Status());
					cdr1.setSdpStatusDescp(callbackData.toString());
					cdr1.setRequestDate(new java.util.Date());
					cdr1.setChargingDate(startDate);
					if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
						cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr1.setChargedPpid(chargedPricePoint.getPpid());
						cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
						cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
						
					}
					cdrBuilder.insert(cdr1);
					}else{
						cdr.setRequestType("REN");
						cdr.setSdpResponseTime(new java.util.Date());
						cdr.setSdpStatusCode(callbackData.getConsent_Status());
						cdr.setSdpStatusDescp(callbackData.toString());
						cdr.setRequestDate(new java.util.Date());
						cdr.setChargingDate(startDate);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr.setChargedAmount(chargedPricePoint.getPricePoint());
							cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
							cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
							cdr.setChargedPpid(chargedPricePoint.getPpid());
							cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
							cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
							cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
							
						}
					}
				}
			}
			
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return cdr;
		
	}
	
	public Subscriber updateSubscriberStatus(String loggerHead,NetExcelBSNLSDPCallback callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint){
		loggerHead +="USS::";
		try{
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(callbackData.getStvPackStartDate().getTime());
			java.util.Date startDate=cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH,chargedPricePoint.getValidity());
			java.util.Date endDate1=cal.getTime();
			if("ACT".equalsIgnoreCase(callbackData.getReq_type()) || "LOWBAL".equalsIgnoreCase(callbackData.getReq_type()) || "FAILED".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					logger.debug("Updating Success callback for chargedPricePoint "+chargedPricePoint.getPpid()+", Master PP "+masterPricePoint.getPpid()+", Callback Price "+callbackData.getPrice()+", Callback Mode "+callbackData.getMode());
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(endDate1);
					cal1.set(Calendar.HOUR_OF_DAY,23);
					cal1.set(Calendar.MINUTE,59);
					cal1.set(Calendar.SECOND,59);
//					cal.setTime(startDate);
//					cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Activated");
					double subSRevenue=sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint();
					logger.debug("SubSession Revenue "+subSRevenue);
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setSubChargedDate(startDate);
					sub.setLastRenewalDate(startDate);
					sub.setValidityEndDate(endDate);
					sub.setSystemId("MJB8Q");
					sub.setNextRenewalDate(endDate);
				}else if(callbackData.getPrice()<1.0){
					if("LOWBAL".equalsIgnoreCase(callbackData.getReq_type())){
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_PARKING);
						sub.setChargedAmount(0D);
						sub.setParkingDate(startDate);
					}else{
						sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
						sub.setChargedAmount(0D);
						sub.setStatusDescp("Charging Failed at SDP|SDP Status="+callbackData.getConsent_Status()+",Mode="+callbackData.getMode()+",Amount="+callbackData.getPrice());
					}
				}else{
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SDPCHARGING);
					sub.setChargedAmount(0D);
					sub.setParkingDate(startDate);
				}
				
			}else if("DCT".equalsIgnoreCase(callbackData.getReq_type()) || "DEACT".equalsIgnoreCase(callbackData.getReq_type()) || "desubscription".equalsIgnoreCase(callbackData.getReq_type())){
						sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
						sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
						//sub.setChargedAmount(0D);
						sub.setUserField_0("UNSUBMODE-"+callbackData.getMode());
					logger.debug(callbackData.getMsisdn()+" :: DCT :: Subscriber Status set to "+sub.getStatus());
			}else if("REN".equalsIgnoreCase(callbackData.getReq_type()) || "REACT".equalsIgnoreCase(callbackData.getReq_type())){
				if("SUCCESS".equalsIgnoreCase(callbackData.getConsent_Status()) && callbackData.getPrice()>=1.0){
					Calendar cal1=Calendar.getInstance();
					cal1.setTime(startDate);
					cal1.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
					java.util.Date endDate=cal1.getTime();
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp("Customer Renewed");
					sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate);
					sub.setNextRenewalDate(endDate);
					sub.setLastRenewalDate(startDate);
				}
			}
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return sub;
	}
	
	
	public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
		loggerHead +="CopyCDR::";
		ChargingCDR newCDR=null;
		
		try{
			newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
					cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
					cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
					cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
					cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
					cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
					cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
					cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
					cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
					cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
					cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
					cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
					cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
					cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
					cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
		}catch(Exception e){
			logger.error(loggerHead+" Exception "+e);
		}
		return newCDR;
	}
 
	private void sendBSNLSTVSms(String url,Long msisdn){
		String logHeader="Send-BSNLSTV-Sms()::{"+msisdn+"}";
		try{
		JcmsSSLClient sslClient=new JcmsSSLClient();		
		sslClient.setContentType("text/plain");
		sslClient.setAcceptDataFormat("text/plain");
		sslClient.setHttpMethod("GET");
		sslClient.setDestURL(url);		
		JcmsSSLClientResponse urlResp=sslClient.connectURL();
		logger.debug(logHeader+" URL Response Code : "+urlResp.getResponseCode());
		logger.debug(logHeader+" URL Response Data : "+urlResp.getResponseData());
		logger.debug(logHeader+" URL Response Error : "+urlResp.getErrorMessage());
		}catch(Exception e){
			logger.error(logHeader+" Exception "+e);
		}
	
	}

}
