package com.jss.jocg.services.scheduled;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;

import com.jss.jocg.charging.model.AirtelIndiaChargingParams;
import com.jss.jocg.services.bo.airtel.AirtelActivationWrapper;
import com.jss.jocg.services.bo.airtel.AirtelApiResponseWraper;
import com.jss.jocg.services.bo.airtel.AirtelSmartApi;
import com.jss.jocg.services.dao.AirtelIndiaPendingTransactionsDao;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.AirtelIndiaPendingTransactions;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.jss.jocg.util.JcmsSSLClient.JcmsSSLClientResponse;

@Component
public class AirtelFailedTransactionProcessor {
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelFailedTransactionProcessor.class);
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired AirtelIndiaPendingTransactionsDao pendingTransDao;
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	
	@Value("{$airtelin.activation.template}")
	String airtelActivationDataTemplate;
	
	 @Scheduled(initialDelay=1, fixedRate=600000)
	 public void processPendingRecords(){
		 try{
			 if(jocgCache.isAirtelPendingEnabled()){
				 List<AirtelIndiaPendingTransactions> pendingTransList=pendingTransDao.findByStatus(new Integer(0));
				 for(AirtelIndiaPendingTransactions newTrans:pendingTransList){
					 logger.debug("Processing Transaction "+newTrans.getId());
					 String logHeader=""+newTrans.getId()+"::";
					 if(newTrans.getRetryCount()<5){
						 AirtelApiResponseWraper jr = new AirtelApiResponseWraper();
						 logger.debug(logHeader+"Parseing Token from Object  "+newTrans.getAccessTokenStr());
					        AirtelSmartApi apiResp = jr.parseResponse(newTrans.getAccessTokenStr());
					        logger.debug(logHeader+"Parsed Response Object  "+apiResp);
					        if (apiResp != null) {
					        	logger.debug(logHeader+"Token Parsed for "+newTrans.getId());
					        	ChargingCDR cdr=chargingCDRDao.findById(newTrans.getCdrId());
					        	
					        	if(cdr!=null && cdr.getId()!=null){
					        		logger.debug(logHeader+"CDR "+cdr.getId());
					        		Subscriber sub=subscriberDao.findByJocgCDRId(cdr.getId());
					            	if(sub!=null && sub.getId()!=null){
					        			logger.debug(logHeader+"Invoking process for  "+newTrans.getId()+", subId "+sub.getId());
						        		newTrans=processRecord(cdr,sub,newTrans,apiResp);
					        		}else{
					        			logger.debug(logHeader+"Subscriber Not found against cdr id  "+cdr.getId());
						        		newTrans.setStatus(-1);
						        		newTrans.setStatusDescp("Failed to Search Mapped Subscriber");
						        		newTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
						        		newTrans.setRetryCount(newTrans.getRetryCount()+1);
					        		}
					        	}else{
					        		logger.debug(logHeader+"CDR Not found against id  "+newTrans.getCdrId());
					        		newTrans.setStatus(-1);
					        		newTrans.setStatusDescp("Failed to Search Mapped CDR ");
					        		newTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
					        		newTrans.setRetryCount(newTrans.getRetryCount()+1);
					        	}
					        	
					        }else{
					        	newTrans.setStatus(-1);
				        		newTrans.setStatusDescp("Failed to Parse Token String");
				        		newTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
				            }
					 }else{
				        	newTrans.setStatus(-1);
			        		newTrans.setStatusDescp("Retry Count Exceeded");
			        		newTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
			        }
					 pendingTransDao.save(newTrans);
					 logger.debug(logHeader+"Pending Trans Id "+newTrans.getId()+" updated with status "+newTrans.getStatus());
					 
				 }
			 } else logger.debug(" AIRTEL PENDING PROCESS :: Disabled on this server.");
		 }catch(Exception e){
			 logger.error("Exception "+e);
		 }
	 }
	
	 private AirtelIndiaPendingTransactions processRecord(ChargingCDR cdr,Subscriber sub,AirtelIndiaPendingTransactions newTrans,AirtelSmartApi apiResp){
		 try{
			 ChargingInterface chintf=jocgCache.getChargingInterface(cdr.getChargingInterfaceId());
				logger.debug(newTrans.getId()+" :: Charging Interface "+chintf);
				
				List<ChargingUrl> urlList=null;
				if(chintf!=null && chintf.getChintfid()>0)
					urlList=jocgCache.getChargingUrlByChintfId(chintf.getChintfid());
				logger.debug(newTrans.getId()+" :: Charging URL List "+urlList);
				if((cdr.getStatus()==JocgStatusCodes.CHARGING_INPROCESS || cdr.getStatus()==JocgStatusCodes.CHARGING_CGSUCCESS) && chintf.getChintfid()>0 && urlList!=null && urlList.size()>0){
					String operatorParams=cdr.getMppOperatorParams();
					AirtelIndiaChargingParams acp=new AirtelIndiaChargingParams();
					acp.decodeParams(operatorParams);
					logger.debug(newTrans.getId()+" :: operatorParams "+operatorParams);
					
					ChargingUrl churl=null;
					for(ChargingUrl c:urlList){
						if("SDP-CHARGE".equalsIgnoreCase(c.getUrlCatg())){ 
							churl=c; break;
						}
					}
					StringBuilder activationDataTemplate=new StringBuilder();
					activationDataTemplate.append("{").append('"').append("channel").append('"').append(":").append('"').append("WAP").append('"').append("}");
					
					
					JcmsSSLClientResponse urlResp= invokeAirtelActivationUrl(churl.getUrl(), "smartapi.airtel.in", 
							activationDataTemplate.toString(), apiResp.getAccess_token(), "application/json", "application/json", acp.getClientId(), acp.getSecretKey());
					String actRespData = urlResp.getResponseData();
					logger.debug(newTrans.getId()+" :: actRespData "+actRespData);
					
					if(urlResp.getResponseCode()==500 || actRespData.contains("SAS-DP-01") || actRespData.contains("Internal Server Error")){
						newTrans.setRetryCount(newTrans.getRetryCount()+1);
						newTrans.setLastprocessTime(new java.util.Date(System.currentTimeMillis()));
					}else{
						AirtelApiResponseWraper jr = new AirtelApiResponseWraper();
						AirtelActivationWrapper airtelActivationWrapper = jr.parseActivationResponseNew(actRespData);
						logger.debug(newTrans.getId()+" :: Object After parsing  "+airtelActivationWrapper);
						Integer transStatus=0;
						String transStatusDescp="",seTransId="";
						if (airtelActivationWrapper!=null && airtelActivationWrapper.getActivationObject()!=null &&
			                    airtelActivationWrapper.getActivationObject().getxActionId()!=null) {
							logger.debug(newTrans.getId()+" :: Updating successful transaction "+airtelActivationWrapper.getActivationObject());
			                cdr.setCgTransactionId(airtelActivationWrapper.getActivationObject().getSeTransactionId());
			                cdr.setSdpTransactionId(airtelActivationWrapper.getActivationObject().getxActionId());
			                String msisdnStr=""+airtelActivationWrapper.getActivationObject().getMsisdn();
			                msisdnStr=(msisdnStr==null)?"NA":msisdnStr.length()<=10?"91"+msisdnStr:msisdnStr;
			                cdr.setMsisdn(JocgString.strToLong(msisdnStr, cdr.getMsisdn()));
			                cdr.setSdpLifeCycleId(airtelActivationWrapper.getActivationObject().getLifecycleId());
			                String chargedCode=airtelActivationWrapper.getActivationObject().getProductId();
			                cdr.setChargedOperatorKey(chargedCode);
			                logger.debug(newTrans.getId()+" :: cdr new "+cdr);
			                
			                ChargingPricepoint chpp=jocgCache.getPricePointListByOperatorKey(chargedCode);
			                logger.debug(newTrans.getId()+" :: Pricepoint "+chpp);
			                if(chpp!=null && chpp.getPpid()>0){
			                	cdr.setChargedAmount(chpp.getPricePoint());
			                	cdr.setChargedPpid(chpp.getPpid());
			                	cdr.setChargedPricePoint(chpp.getPricePoint());
			                	cdr.setChargedValidityPeriod(""+chpp.getValidity());
			                	sub.setChargedAmount(chpp.getPricePoint());
			                	sub.setChargedOperatorKey(chpp.getOperatorKey());
			                	sub.setChargedOperatorParams(chpp.getOpParams());
			                	sub.setSubSessionRevenue(chpp.getPricePoint());
			                	sub.setValidity(chpp.getValidity());
			                	
			                	Calendar cal=Calendar.getInstance();
			                	cal.setTimeInMillis(System.currentTimeMillis());
			                	cdr.setChargingDate(cal.getTime());
			                	sub.setSubChargedDate(cal.getTime());
			                	cdr.setValidityStartDate(cal.getTime());
			                	sub.setValidityStartDate(cal.getTime());
			                	
			                	cal.add(Calendar.DATE, chpp.getValidity());
			                	cdr.setValidityEndDate(cal.getTime());
			                	sub.setValidityEndDate(cal.getTime());
			                	cdr.setFallbackCharged(chpp.getIsfallback()>0?true:false);
			                	cdr.setChargedOperatorParams(chpp.getOpParams());
			                	
			                	//Setting Notification
								cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
								cdr.setNotifyScheduleTime(new java.util.Date());
								cdr.setNotifyChurn(true);
								logger.debug(newTrans.getId()+" ::  S2S Notification Queued, Churn Status Enabled!");
			                	
			                	
			                	
			                }
			                
			                transStatus = JocgStatusCodes.CHARGING_SUCCESS;
			                transStatusDescp = "Activation Resp=" + actRespData ;
			                //seTransId = activationRespData.substring(activationRespData.indexOf(":") + 1);
			                //seTransId = seTransId.trim();
			                //seTransId = seTransId.substring(0, seTransId.length() - 1);
			                seTransId=airtelActivationWrapper.getActivationObject().getxActionId();
			                cdr.setStatus(transStatus);
			                cdr.setStatusDescp(transStatusDescp);
			                cdr.setSdpStatusCode(airtelActivationWrapper.getActivationObject().getxActionId());
			                cdr.setCgTransactionId(airtelActivationWrapper.getActivationObject().getSeTransactionId());
			                
			                sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
			                sub.setLifecycleId(airtelActivationWrapper.getActivationObject().getLifecycleId());
			                sub.setMsisdn(JocgString.strToLong(airtelActivationWrapper.getActivationObject().getMsisdn(),0L));
			                sub.setCustomerId(airtelActivationWrapper.getActivationObject().getLifecycleId());
			                sub.setUserField_0("xactionid:"+airtelActivationWrapper.getActivationObject().getxActionId());
			                sub.setUserField_1("setransid:"+airtelActivationWrapper.getActivationObject().getSeTransactionId());
			                newTrans.setStatus(1);
							newTrans.setStatusDescp(transStatusDescp);
			                
			            }else {
			                //Activation Error
			            	 cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				             cdr.setStatusDescp("Failed to Parse Activation Resp");
				             sub.setStatus(JocgStatusCodes.SUB_FAILED_CG);
				             
			           }
						
					}
					
				}else{
					logger.debug(newTrans.getId()+" Invalid URL List or CDR Status "+cdr.getStatus());
					newTrans.setStatus(-1);
					newTrans.setStatusDescp("Invalid URL List or CDR Status "+cdr.getStatus());
				}
				
				
		 }catch(Exception e){
		 }finally{
			 try{
				 sub.setLastUpdated(new java.util.Date());
				 cdr.setLastUpdated(new java.util.Date());
				 cdr.setReportGenerated(false);
				 subscriberDao.save(sub);
				 chargingCDRDao.save(cdr);
			 }catch(Exception e){}
		 }
		 return newTrans;
	 }
	 
	 private JcmsSSLClientResponse invokeAirtelActivationUrl(String url1, String hostName, String requestData, String authToken, String contentType, 
             String acceptFormat, String user, String pwd) {
		 JcmsSSLClientResponse resp = null;
		 try {
		     JcmsSSLClient sslClient = new JcmsSSLClient();
		     sslClient.setDestURL(url1);
		     sslClient.setUseBasicAuthentication(true);
		     sslClient.setAuthUser(user);
		     sslClient.setAuthPwd(pwd);
		
		     sslClient.setRequestData(requestData);
		     sslClient.setAcceptDataFormat(acceptFormat);
		     sslClient.setContentType(contentType);
		     sslClient.setAuthToken(authToken);
		     sslClient.setUseAuthTokenInBasicAuth(true);
		     sslClient.setHttpMethod("POST");
		     sslClient.setUseHostNameInHeader(true);
		     sslClient.setHostName(hostName);
		     sslClient.setConnectionTimeoutInSecond(30);
		     sslClient.setReadTimeoutInSecond(30);
		     resp = sslClient.connectInsecureURLBasicAuth("Airtel Smart API :: ");
		     
		   
		     System.out.println("Airtel Response code " + resp.getResponseCode());
		     System.out.println("Airtel Response Data " + resp.getResponseData());
		     System.out.println("Airtel Response Error " + resp.getErrorMessage());
		 } catch (Exception e) {
		     System.out.println(" Airtel  Exception " + e);
		 }
		 return resp;
		}
	
}
