package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.ChargingCDRUpdateRecord;
import com.jss.jocg.util.SubscriberUpdateRecord;

@Component
public class ChargingCDRDateCorrection {
	private static final Logger logger = LoggerFactory
			.getLogger(ChargingCDRDateCorrection.class);
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	
	@Value("${jocg.offlinecdr.chargingcdr.update}")
	String offlineCDRFolder;
	//@Scheduled(initialDelay=1, fixedRate=86400000)
	 public void processFiles(){
		String logHeader="CDR-UPDATE :: ";
		if(jocgCache.isOfflineProcessEnabled()){
				
				 try{
					 logger.debug(logHeader+" PROCESSOR Invoked ");
					 File fd=new File(offlineCDRFolder);
					 logger.debug(logHeader+"  Base folder "+offlineCDRFolder);
					 if(fd.exists() && fd.isDirectory()){
							File[] flist=fd.listFiles();
							if(flist!=null && flist.length>0){
								for(File f:flist){
									 logger.debug(logHeader+" Processing file  "+f.getAbsolutePath());
									 
									 if(!f.isDirectory() && ((System.currentTimeMillis()-f.lastModified())/1000)>5){
										 long processStartTime=System.currentTimeMillis();	
										 BufferedReader br=new BufferedReader(new FileReader(f));
							                    String newLine="";int processedRecords=0;
							                    while((newLine=br.readLine())!=null){
							                    	   processRecord(logHeader,newLine,f.getName());
							                           processedRecords++;
							                            WAIT(10);
							                    }
							                    
							                    logger.info(logHeader+"  Processed Count in ("+f.getCanonicalPath()+") :"+processedRecords+", Time Taken "+((System.currentTimeMillis()-processStartTime)/60000)+" minutes.");
							                    br.close();
							                    br=null;
							                    try{
							                    	f.renameTo(new File(offlineCDRFolder+"/processed/"+f.getName()));
							                    }catch(Exception ee){
							                    	f.delete();
							                    }
							                  WAIT(50);
							                  
										}//if
										else  
											logger.debug(logHeader+" Skipped File "+f.getAbsolutePath()+"|IsDirectory="+f.isDirectory());
										f=null;
								}//for
								
							}//if
					 }else  
						 logger.debug(logHeader+" Invalid Folder "+offlineCDRFolder);
					 
					 
				 }catch(Exception e){
					 
				 }
		 } else  logger.debug(logHeader+":: Offline Processes are disabled on this server ");
		
	}
	
	public synchronized void WAIT(int ms){
		 try{
			this.wait(ms);
		 }catch(Exception e){}
		 
	 }
	
	public void  processRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		ChargingCDRUpdateRecord newRecord=null;
		try{
			newRecord=new ChargingCDRUpdateRecord();
			newRecord.parseRecord(logger, logHeader, newLine);
			if(newRecord.getChardingCDRid()!=null && !"NA".equalsIgnoreCase(newRecord.getChardingCDRid())){
				SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				ChargingCDR cdr=chargingCDRDao.findById(newRecord.getChardingCDRid());
				if(cdr!=null && cdr.getId()!=null){
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(cdr.getChargingDate().getTime());
					java.util.Date dold=cal.getTime();
					cal.add(Calendar.HOUR,5);
					cal.add(Calendar.MINUTE, 30);
					java.util.Date d=cal.getTime();
					cdr.setChargingDate(d);
					cdr=cdrBuilder.save(cdr);
					logger.debug(logHeader+" CDR "+cdr.getId()+"|OldDate= "+sdf.format(dold)+"|NewDate= "+sdf.format(d));
					cdr=null;
				}
			}else{
				erroFlag=true;
				erroMessage="CDR id Not Found";
			}
		}catch(Exception e){
			erroFlag=true;
			erroMessage="Exception "+e.getMessage()+","+newRecord;
		}finally{
			try{
				String fileNameStatus=offlineCDRFolder+"/processed/"+fileName+"."+((erroFlag==true)?"failed":"success");
				FileWriter fw=new FileWriter(fileNameStatus,true);
				fw.write("\n"+newLine+"|"+erroMessage);
				fw.flush();
				fw.close();
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}
	
	public TrafficSource getTrafficSourceByName(String logHeader,String trafficSourceName){
		TrafficSource ts=null;
		try{
			Map<Integer,TrafficSource> tsList=jocgCache.getTrafficSources();
			Iterator<TrafficSource> i=tsList.values().iterator();
			while(i.hasNext()){
				ts=i.next();
				if(ts.getSourcename().equalsIgnoreCase(trafficSourceName)) break;
				else ts=null;
			}
		}catch(Exception e){
			logger.error(logHeader+" Error while searching trafficSourceName "+trafficSourceName+"|"+e.getMessage());
		}
		return ts;
	}
	
	public Platform getPlatformBySystemId(String logHeader,String systemId){
		 Platform pl=null;
		 try{
			Map<String,Platform> platfromList=jocgCache.getPlatformConfig();
			Iterator<Platform> i=platfromList.values().iterator();
			while(i.hasNext()){
				pl=i.next();
				if(pl.getSystemid().equalsIgnoreCase(systemId)) break;
				else pl=null;
			}
		 }catch(Exception e){
			 logger.error(logHeader+" Error while searching systemId "+systemId+"|"+e.getMessage());
		 }
		 return pl;
	 }
	
}
