package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.callback.bo.AirtelIndiaSDPOfflineCallbackReceiver;
import com.jss.jocg.callback.data.AirtelIndiaOfflineDumpRecord;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class AirtelOfflineDumpProcessor {
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	@Autowired JmsTemplate jmsTemplate;
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelOfflineDumpProcessor.class);
	
	
	SimpleDateFormat sdfDate=new SimpleDateFormat("yyyyMMddHHmmss");
	@Value("${jocg.offlinecdr.airtel.folder}")
	String offlineCDRFolder;
	
	//@Scheduled(initialDelay=1, fixedRate=600000)
	 public void processFiles(){
		if(jocgCache.isOfflineProcessEnabled()){
				String logHeader="AIRTEL-OFFLINE :: ";
				 try{
					 logger.debug(logHeader+" PROCESSOR Invoked ");
					 File fd=new File(offlineCDRFolder);
					 logger.debug(logHeader+"  Base folder "+offlineCDRFolder);
					 if(fd.exists() && fd.isDirectory()){
							File[] flist=fd.listFiles();
							if(flist!=null && flist.length>0){
								for(File f:flist){
									 logger.debug(logHeader+" Processing file  "+f.getAbsolutePath());
									 
									 if(!f.isDirectory() && ((System.currentTimeMillis()-f.lastModified())/1000)>5){
										 long processStartTime=System.currentTimeMillis();	
										 BufferedReader br=new BufferedReader(new FileReader(f));
							                    String newLine="";int processedRecords=0;
							                    while((newLine=br.readLine())!=null){
							                    	
//To be enabled after cleaning data
							                           processRecord(logHeader,newLine);
							                           processedRecords++;
							                            WAIT(10);
							                    }
							                    
							                    logger.info(logHeader+"  Processed Count in ("+f.getCanonicalPath()+") :"+processedRecords+", Time Taken "+((System.currentTimeMillis()-processStartTime)/60000)+" minutes.");
							                    br.close();
							                    br=null;
							                    try{
							                    	f.renameTo(new File(offlineCDRFolder+"/processed/"+f.getName()));
							                    }catch(Exception ee){
							                    	f.delete();
							                    }
							                  WAIT(50);
							                  
										}//if
										else  
											logger.debug(logHeader+" Skipped File "+f.getAbsolutePath()+"|IsDirectory="+f.isDirectory());
										f=null;
								}//for
								
							}//if
					 }else  
						 logger.debug(logHeader+" Invalid Folder "+offlineCDRFolder);
					 
					 
				 }catch(Exception e){
					 
				 }
		 } else  logger.debug("AIRTEL-OFFLINE :: Offline Processes are disabled on this server ");
		
	}
	
	public synchronized void WAIT(int ms){
		 try{
			this.wait(ms);
		 }catch(Exception e){}
		 
	 }
	 
	
	
	 public void processRecord(String logHeader,String newRecord){
		 if(!newRecord.contains("TRANSACTION_ID")){
			 AirtelIndiaOfflineDumpRecord offlineRecord=new AirtelIndiaOfflineDumpRecord();
			boolean flag= offlineRecord.parseRecord(newRecord);
			 if(flag){
				
				 
				 
				 if(offlineRecord.getTransType().equalsIgnoreCase("New-Subscription")){
					 if("1000".equalsIgnoreCase(offlineRecord.getErrorCode()) && offlineRecord.getStatus().contains("Success")){
						 //Successful Activation
						 processOfflineDumpMessage(offlineRecord);
					 }else{
						 //Activation Failed
						 processOfflineDumpMessage(offlineRecord);
					 }
					 
				 }else if(offlineRecord.getTransType().equalsIgnoreCase("Re-Subscription")){
					 if("1000".equalsIgnoreCase(offlineRecord.getErrorCode()) && offlineRecord.getStatus().contains("Success")){
						 //Successful Renewal
						 processOfflineDumpMessage(offlineRecord);
					 }else if("3027".equalsIgnoreCase(offlineRecord.getErrorCode()) || "3404".equalsIgnoreCase(offlineRecord.getErrorCode())){
						 //Renewal Failed
						 logger.debug(logHeader+" Ignored Renewal Failed "+offlineRecord);
					 }else{
						 logger.debug(logHeader+" Ignored REN Failed -other reason "+offlineRecord);
					 }
				 }else if(offlineRecord.getTransType().contains("De-Subscription")){
					 if("1001".equalsIgnoreCase(offlineRecord.getErrorCode())){
						 //Successful Deactivation
						 processOfflineDumpMessage(offlineRecord);
					 }else{
						// De-activation Failed
						 logger.debug(logHeader+" Ignored DCT Failed "+offlineRecord);
					 }
				 }
				 
			 }else{
				 logger.debug(logHeader+" Parsing Failed , Error "+offlineRecord);
			 }
		 }
		 
	 }
	 
	 
	 
	 public void processOfflineDumpMessage(AirtelIndiaOfflineDumpRecord callbackData) {
			String loggerHead="AirtelOfflineDumpProcessor::msisdn="+callbackData.getMsisdn()+"|";
		
			logger.debug(loggerHead+" :: Received ||| <" + callbackData + ">");
			try{
				Long msisdn=callbackData.getMsisdn();
				if(callbackData!=null && msisdn>0){	
					logger.debug(loggerHead+" callbackData is valid for msisdn "+msisdn+", Key "+callbackData.getProductId()+", Amount "+callbackData.getAmount()+", Balance "+callbackData.getBalance());
					ChargingPricepoint cpp=jocgCache.getPricePointListByOperatorKey(callbackData.getProductId(),callbackData.getAmount());
					//getPricePointListByOperatorKey(callbackData.getProductId());
					
					ChargingPricepoint cppmaster=(cpp==null || cpp.getPpid()<=0)?null:(cpp.getIsfallback()>0?jocgCache.getPricePointListById(cpp.getMppid()):cpp);
					logger.debug(loggerHead+" Charged Price Point "+cpp+", Master Price Point "+cppmaster);
					
					if(cpp!=null && cpp.getPpid()>0 && cppmaster!=null){
						String serviceKey=(cppmaster!=null && cppmaster.getPpid()>0)?cppmaster.getOperatorKey():cpp.getOperatorKey();
						String msisdnStr=""+callbackData.getMsisdn();
						if(msisdnStr.length()<=10) callbackData.setMsisdn(JocgString.strToLong("91"+msisdn.longValue(),msisdn));
						
						logger.debug(loggerHead+"Searching Subscriber for msisdn  "+msisdn+", Operator Service Key "+serviceKey);
						//To be checked-Rishi
						Subscriber sub=subscriberDao.findByMsisdnAndOperatorServiceKey(msisdn,serviceKey);	
						if(sub==null || sub.getId()==null){
							ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
							PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
							Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
							PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
							Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
							if(service!=null && service.getServiceid()>0){
								sub=subscriberDao.searchSubscriberByChintfAndServiceId(chintf.getChintfid(),""+msisdn.longValue(),msisdn,service.getServiceid(),service.getServiceName());
							}
							
						}
						ChargingCDR cdr=null;
						
						String action=(callbackData.getTransType().contains("New-Subscription") && "1000".equalsIgnoreCase(callbackData.getErrorCode()) && callbackData.getStatus().contains("Success"))?"ACT":
							(callbackData.getTransType().contains("Re-Subscription") && "1000".equalsIgnoreCase(callbackData.getErrorCode()) && callbackData.getStatus().contains("Success"))?"REN":
							(callbackData.getTransType().contains("Re-Subscription") && "3027".equalsIgnoreCase(callbackData.getErrorCode()))?"SUSPEND":
									(callbackData.getTransType().contains("Re-Subscription") && "3404".equalsIgnoreCase(callbackData.getErrorCode()))?"GRACE":
								(callbackData.getTransType().contains("De-Subscription") && ("1001".equalsIgnoreCase(callbackData.getErrorCode()) || "1013".equalsIgnoreCase(callbackData.getErrorCode()) ))?"DCT":
									callbackData.getTransType().contains("Re-Subscription")?"REN-FAILED":
										callbackData.getTransType().contains("New-Subscription")?"ACT-FAILED":
											callbackData.getTransType().contains("De-Subscription")?"DCT-FAILED":"OTHER";
						
						if("OTHER".equalsIgnoreCase(action) || "REN-FAILED".equalsIgnoreCase(action) ||  "DCT-FAILED".equalsIgnoreCase(action)){
							logger.debug(loggerHead+"ACTION-"+action+" NOT TO BE LOGGED");
						}else{
							//Check is seTransId is already processed
							cdr=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(cppmaster.getChintfid(), callbackData.getTransactionId());
							if(cdr!=null && cdr.getId()!=null && cdr.getRequestType().equalsIgnoreCase(action)){
								logger.debug(loggerHead+"Duplicate Transaction Id  "+callbackData.getTransactionId()+", Already Registered against CDR ID "+cdr.getId()+" Offline Callback Ignored!");
							}else if("ACT".equalsIgnoreCase(action)){
								if(sub!=null) handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
								else handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
							}else if("REN".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
								if(sub!=null && sub.getId()!=null){
									if(sub!=null) handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
									else handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
								}else{
									handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
								}
							
							}else{
								logger.debug(loggerHead+"Unknown action  "+action+", Callback Ignored!");
							}
								
							
							
						}
						
//						if("OTHER".equalsIgnoreCase(action) || "REN-FAILED".equalsIgnoreCase(action) ||  "DCT-FAILED".equalsIgnoreCase(action)){
//							logger.debug(loggerHead+"ACTION-"+action+" NOT TO BE LOGGED");
//						}else if(sub!=null && sub.getId()!=null){
//							logger.debug(loggerHead+" Found subscriber id "+sub.getId()+", Status "+sub.getStatus()+", CGStatus "+sub.getCgStatusCode()+", Last Renewal date "+sub.getLastRenewalDate());
//							java.util.Date lastRenewaldate=sub.getLastRenewalDate();
//							java.util.Date callbackDate=callbackData.getTransTime();
//							String sdpTransId=callbackData.getTransactionId();
//							boolean duplicateActOrRenewal=false;
//							if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action)){
//								cdr=chargingCDRDao.findByChargingInterfaceIdAndSdpTransactionId(cppmaster.getChintfid(),callbackData.getTransactionId());
//								if(cdr!=null && cdr.getId()!=null && action.equalsIgnoreCase(cdr.getRequestType())){
//									duplicateActOrRenewal=true;
//								}
//							}
//							
//							
//							System.out.println("action "+action+", sub :"+sub+"|last RenewalDate "+lastRenewaldate+"|Processing Date "+sdfDate.format(callbackDate));
//							if(("REN".equalsIgnoreCase(action) || "ACT".equalsIgnoreCase(action)) && duplicateActOrRenewal==true){
//								logger.debug(loggerHead+" Duplicate ACT/Renewal Callback ("+sdpTransId+"), Hence Ignored");
//							}else if("DCT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.UNSUB_SUCCESS){
//								logger.debug(loggerHead+" Duplicate Deactivation Callback, Hence Ignored");
//							}else if("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
//								cdr=chargingCDRDao.findById(sub.getJocgCDRId());
//								//chargingCDRDao.findBy
//								if(cdr!=null && cdr.getId()!=null){
//									/*if(sub.getChargedAmount()>0 && "ACT".equalsIgnoreCase(cdr.getRequestType()) && "ACT".equalsIgnoreCase(action) && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE){
//										logger.debug(loggerHead+" Duplicate Activation Callback, Hence Ignored");
//									}else{*/
//										if(sub.getStatus()!=JocgStatusCodes.SUB_SUCCESS_ACTIVE && ("ACT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action))){
//											logger.debug(loggerHead+"  CDR found but subscriber is not active, Process for Existing Subscriber and Non Existing CDR...");
//											handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
//										}else{
//											logger.debug(loggerHead+"  Found CDR id "+cdr.getId()+", Status "+cdr.getStatus()+", CGStatus "+cdr.getCgStatusCode()+"");
//											handleExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,sub,cdr,action);
//										}
//									//}
//								}else{
//									logger.debug(loggerHead+"  CDR Not found, Process for Existing Subscriber and Non Existing CDR...");
//									handleExistingSubscriberNonExistingCDR(loggerHead,callbackData,cpp,cppmaster,sub,action);
//								}
//							}else{
//								logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
//							}
//						}else{
//							if("ACT".equalsIgnoreCase(action) || "DCT".equalsIgnoreCase(action) || "REN".equalsIgnoreCase(action) || "GRACE".equalsIgnoreCase(action) || "SUSPEND".equalsIgnoreCase(action)){
//								logger.debug(loggerHead+"  Subscriber Not found, Process for Non Existing Subscriber...");
//								handleNonExistingSubscriber(loggerHead,callbackData,cpp,cppmaster,action,msisdn);
//							}else{
//								logger.debug(loggerHead+" Unsupported Action "+action+", Notification Ignored!");
//							}
//							
//						}
						
					}else logger.error(loggerHead+" Invalid serviceKey or Pricepint, Callback Ignored!");
					
				}else logger.error(loggerHead+" Invalid Msisdn in callback, Callback Ignored!");
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				
				
			}
	        
	    }
		
		
		
		public void handleExistingSubscriber(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,ChargingCDR cdr,String actionName){
			loggerHead +="HES::";
			try{
				logger.debug(loggerHead+" Processing for existing custmer CDR ......."+actionName+", status "+callbackData.getStatus());
				boolean activatedFromZero=(cdr.getStatus()==JocgStatusCodes.CHARGING_SUCCESS && 
						cdr.getChargedPricePoint().doubleValue()==0D && cpp.getPricePoint()>0 && 
						sub.getChargedAmount().doubleValue()==0D)?true:false;
				
				logger.debug(loggerHead+" activatedFromZero "+activatedFromZero);
				boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
				if("DCT".equalsIgnoreCase(actionName)){
					SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
					String dateAct=sdfdate.format(sub.getSubChargedDate());
					String dateDct=sdfdate.format(callbackData.getTransTime());
					sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
					logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
					try{
						java.util.Date dsubCharged=sub.getSubChargedDate();
						if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
						if(dsubCharged!=null){
							Calendar cal=Calendar.getInstance();
							cal.setTimeInMillis(dsubCharged.getTime());
							cal.add(Calendar.MINUTE, 20);
							java.util.Date expiry20Minchurn=cal.getTime();
							cal.setTimeInMillis(dsubCharged.getTime());
							cal.add(Calendar.HOUR, 24);
							java.util.Date expiry24Hourchurn=cal.getTime();
							churn20Min=(expiry20Minchurn.after(callbackData.getTransTime())|| expiry20Minchurn.equals(callbackData.getTransTime()))?true:false;
							churn24Hour=(expiry24Hourchurn.after(callbackData.getTransTime())|| expiry24Hourchurn.equals(callbackData.getTransTime()))?true:false;
							logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDate.format(sub.getSubChargedDate())
							+",ValidityStart : "+sdfDate.format(sub.getValidityStartDate())+", DCT Time :"+sdfDate.format(callbackData.getTransTime())+",Expiry20MinChurn : "+
							sdfDate.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDate.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
						}else{
							logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
						}
					}catch(Exception e){
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
					}
				
				}
				
				cdr=updateCDRStatus(loggerHead,callbackData,cdr,cpp,cppmaster,false,actionName,sub,activatedFromZero,sameDayChurn,churn20Min,churn24Hour);
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
				subMgr.save(sub);
				if("ACT".equalsIgnoreCase(actionName) ){
					if("1000".equalsIgnoreCase(callbackData.getErrorCode())){
						//Disabled sms and notification in case of offline dump activation
//						logger.debug(loggerHead+" Sending Activation SMS .......");
//						JocgSMSMessage smsMessage=new JocgSMSMessage();
//						smsMessage.setMsisdn(callbackData.getMsisdn());
//						smsMessage.setSmsInterface("smsjust");
//						smsMessage.setChintfId(1);
//						smsMessage.setTextMessage("Successful Activation");
//						jmsTemplate.convertAndSend("notify.sms", smsMessage);
//						logger.debug(loggerHead+" Activation SMS Queued .......SMSData="+smsMessage);
						//Setting Notification
//						cdr.setNotifyStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
//						Calendar cal=Calendar.getInstance();
//						cal.setTimeInMillis(System.currentTimeMillis());
//						cal.add(Calendar.MINUTE, 60);
//						cdr.setNotifyScheduleTime(cal.getTime());
						//cdr.setNotifyChurn(true);
//						logger.debug(loggerHead+" S2S Notification Queued, Churn Status Enabled!");
						
					}
					cdrBuilder.save(cdr);
				}else if("DCT".equalsIgnoreCase(actionName)){
					if(cdr.getChurnSameDay()==true && cdr.getNotifyStatus()==JocgStatusCodes.NOTIFICATION_SENT){
						cdr.setNotifyChurn(true);
						cdr.setNotifyChurnStatus(JocgStatusCodes.NOTIFICATION_QUEUED);
						cdr.setNotifyTimeChurn(new java.util.Date());
					}
					cdrBuilder.save(cdr);
				}
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
		}
		
		public void handleExistingSubscriberNonExistingCDR(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,Subscriber sub,String actionName){
			loggerHead +="HESNEC::";
			try{
				
				String requestCategory="OP-OFFLINEDUMP";
				
				boolean activatedFromZero=("REN".equalsIgnoreCase(actionName) && cpp.getPricePoint()>0 && 
						sub.getChargedAmount().doubleValue()==0D && sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE)?true:false;
				
				logger.debug(loggerHead+" activatedFromZero "+activatedFromZero);
				boolean sameDayChurn=false,churn20Min=false,churn24Hour=false;
				if("DCT".equalsIgnoreCase(actionName)){
					SimpleDateFormat sdfdate=new SimpleDateFormat("yyyyMMdd");
					String dateAct=sdfdate.format(sub.getSubChargedDate());
					String dateDct=sdfdate.format(callbackData.getTransTime());
					sameDayChurn=(sub.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && dateAct.equalsIgnoreCase(dateDct))?true:false;
					logger.debug(loggerHead+"DIGIVIVE-DCT-PUSH:Validation {subId :"+sub.getId()+",subDate:"+dateAct+",dctDate:"+dateDct+",status:"+sub.getStatus()+",sameDayChurn:"+sameDayChurn+"}");
					try{
						java.util.Date dsubCharged=sub.getSubChargedDate();
						if(dsubCharged==null) dsubCharged=sub.getValidityStartDate();
						if(dsubCharged!=null){
							Calendar cal=Calendar.getInstance();
							cal.setTimeInMillis(dsubCharged.getTime());
							cal.add(Calendar.MINUTE, 20);
							java.util.Date expiry20Minchurn=cal.getTime();
							cal.setTimeInMillis(dsubCharged.getTime());
							cal.add(Calendar.HOUR, 24);
							java.util.Date expiry24Hourchurn=cal.getTime();
							churn20Min=(expiry20Minchurn.after(callbackData.getTransTime())|| expiry20Minchurn.equals(callbackData.getTransTime()))?true:false;
							churn24Hour=(expiry24Hourchurn.after(callbackData.getTransTime())|| expiry24Hourchurn.equals(callbackData.getTransTime()))?true:false;
							logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {subId :"+sub.getId()+",SubChargedTime:"+sdfDate.format(sub.getSubChargedDate())
							+",ValidityStart : "+sdfDate.format(sub.getValidityStartDate())+", DCT Time :"+sdfDate.format(callbackData.getTransTime())+",Expiry20MinChurn : "+
							sdfDate.format(expiry20Minchurn)+",Expiry24HourChurn:"+sdfDate.format(expiry24Hourchurn)+"},{sameDayChurn="+sameDayChurn+",churn20Min="+churn20Min+",churn24Hour="+churn24Hour+"}");
						}else{
							logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {dsubcharged date is null}");
						}
					}catch(Exception e){
						logger.debug(loggerHead+"DIGIVIVE-(20Min-24Hour)DCT:Validation {Exception "+e+"}");
					}
				
				}
				
				
				ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
				PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
				Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
				PackageDetail pkg=jocgCache.getPackageDetailsByID(sub.getPackageId());
				Service service=jocgCache.getServicesById(sub.getServiceId());
				//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				if(pkg.getPkgid()!=sub.getPackageId()){
					sub.setPackageId(pkg.getPkgid());
					sub.setPackageName(pkg.getPkgname());
					sub.setPackagePrice(pkg.getPricepoint());
					if(service!=null){
						sub.setServiceId(service.getServiceid());
						sub.setServiceName(service.getServiceName());
					
					}
					if(sub.getPackageChargingInterfaceMapId()!=pkgChintf.getPkgchintfid()){
						sub.setPackageChargingInterfaceMapId(pkgChintf.getPkgchintfid());
					}
					
				}
				
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(callbackData.getTransTime().getTime());
				java.util.Date startDate=cal.getTime();
				cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
				java.util.Date endDate1=cal.getTime();
				
				Platform platform=jocgCache.getPlatformConfig(sub.getPlatformId());
				if(platform==null || platform.getPlatformid()<=0) platform=jocgCache.getPlatformConfig("BLANK-1");
				TrafficSource ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
				TrafficCampaign tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
				
				ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), sub.getJocgTransId(), requestCategory, actionName, sub.getSubscriberId(), sub.getMsisdn(), platform.getPlatform(), platform.getSystemid(), 
						new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
						startDate, startDate, endDate1, "NA", "NA", "NA",  ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
						null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "To be updated", callbackData.getTransTime(), "NA", "NA", "NA", callbackData.getTransTime(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getTransactionId(), ""+callbackData.getMsisdn(), callbackData.getTransTime(), 
						null, null, false, false, false);
				
				
				newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,sub,activatedFromZero,sameDayChurn,churn20Min,churn24Hour);
				
				
				if("ACT".equalsIgnoreCase(actionName) || "REN".equalsIgnoreCase(actionName)){
					//Update in case of non existing CDR
					newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
					newCDR.setStatusDescp("Customer Activated");
					newCDR.setChargedPpid(cpp.getPpid());
					newCDR.setChargedPricePoint(cpp.getPricePoint());
					newCDR.setChargedAmount(cpp.getPricePoint());
					newCDR.setChargedOperatorKey(cpp.getOperatorKey());
					newCDR.setChargedOperatorParams(cpp.getOpParams());
					newCDR.setChargingDate(startDate);
					newCDR.setValidityStartDate(sub.getValidityStartDate());
					newCDR.setValidityEndDate(endDate1);
					logger.debug("Charged PpId "+newCDR.getChargedPpid());
				}
				
				
				sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
				//Update SubCharged Date
				try{
					if(sub.getSubChargedDate()!=null) newCDR.setSubRegDate(sub.getSubChargedDate());
					else if(sub.getValidityStartDate()!=null) newCDR.setSubRegDate(sub.getValidityStartDate());
				}catch(Exception e){}
				if(sub!=null && sub.getValidityStartDate()!=null) newCDR.setValidityStartDate(sub.getValidityStartDate());
				
				newCDR=cdrBuilder.insert(newCDR);
				logger.debug(loggerHead+"Created new CDR for Existing Subscriber but non existing CDR "+newCDR);
				if("ACT".equalsIgnoreCase(actionName)) sub.setJocgCDRId(newCDR.getId());
				sub=subMgr.save(sub);
				logger.debug(loggerHead+"Subscriber Updated with status "+sub.getStatus()+" against Id "+sub.getId());
				newCDR=null;
				sub=null;
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
		}
		
		public void handleNonExistingSubscriber(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingPricepoint cpp,ChargingPricepoint cppmaster,String actionName,long msisdn){
			loggerHead +="HNES::";
			try{
				String requestCategory="SDP-OFFLINEDUMP";
				ChargingInterface chintf=jocgCache.getChargingInterface(cppmaster.getChintfid());
				PackageChintfmap pkgChintf=jocgCache.getPackageChargingInterfaceMap(chintf.getChintfid(), chintf.getChintfid(), cppmaster.getPpid());
				Circleinfo circle=new Circleinfo();//callbackData.getCircleId()>0?jocgCache.getCircleInfo(chintf.getChintfid(), (callbackData.getCircleId()<10?"0"+callbackData.getCircleId():""+callbackData.getCircleId())):jocgCache.getCircleInfo(chintf.getChintfid(), "99");
				PackageDetail pkg=jocgCache.getPackageDetailsByID(pkgChintf!=null?pkgChintf.getPkgid():0);
				Service service=jocgCache.getServicesById(pkg!=null?pkg.getServiceid():0);
				//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(callbackData.getTransTime().getTime());;
				java.util.Date startDate=cal.getTime();
				cal.add(Calendar.DAY_OF_MONTH, cpp.getValidity());
				java.util.Date endDate1=cal.getTime();
				if(chintf!=null && pkgChintf!=null && pkg!=null && service!=null){
				
					Platform platform=jocgCache.getPlatformConfig("BLANK");
					TrafficSource ts=null;
					TrafficCampaign tc=null;
					
					ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(""+callbackData.getMsisdn(), "SDP-"+callbackData.getTransactionId(), requestCategory, actionName, ""+msisdn, msisdn, platform.getPlatform(), platform.getSystemid(), 
							new JocgRequest(), chintf, chintf, pkgChintf, circle, pkg, service, cppmaster, cpp, null, "OPERATOR", 0D, cpp.getPricePoint(), callbackData.getReferer(), 
							startDate, startDate, endDate1, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
							null, null, null, -1, "NA", JocgStatusCodes.CHARGING_INPROCESS, "Callback Processed", callbackData.getTransTime(), "NA", "NA", "NA", callbackData.getTransTime(), callbackData.getErrorCode(), callbackData.toString(), callbackData.getTransactionId(), ""+callbackData.getMsisdn(), callbackData.getTransTime(), 
							null, null, false, false, false);
					
					newCDR=updateCDRStatus(loggerHead,callbackData,newCDR,cpp,cppmaster,true,actionName,null,false,false,false,false);
					newCDR.setSubRegDate(startDate);
					if("ACT".equalsIgnoreCase(actionName)){
						//Update in case of non existing CDR
						newCDR.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						newCDR.setStatusDescp("Customer Activated");
						newCDR.setChargedPpid(cpp.getPpid());
						newCDR.setChargedPricePoint(cpp.getPricePoint());
						newCDR.setChargedAmount(cpp.getPricePoint());
						newCDR.setChargedOperatorKey(cpp.getOperatorKey());
						newCDR.setChargedOperatorParams(cpp.getOpParams());
						newCDR.setChargingDate(startDate);
						newCDR.setValidityStartDate(startDate);
						newCDR.setValidityEndDate(endDate1);
						logger.debug("Charged PpId "+newCDR.getChargedPpid());
					}
					
					newCDR=cdrBuilder.insert(newCDR);
					logger.debug(loggerHead+"Created new CDR for Non Existing Subscriber "+newCDR);
					
					Subscriber sub=new Subscriber(null,newCDR.getCircleId(),newCDR.getCircleName(),newCDR.getCircleCode(),"SDPIP",""+msisdn,""+msisdn,msisdn,
							"OP",chintf.getPchintfid(),chintf.getUseparenthandler(),chintf.getChintfid(),chintf.getOpcode(),chintf.getName(),
							chintf.getChintfid(),chintf.getName(),chintf.getCountry(),cppmaster.getCurrency(),chintf.getGmtDiff(),chintf.getChintfType(),pkg.getPkgid(),
							pkg.getPkgname(),pkg.getServiceid(),service.getServiceName(),service.getServiceCatg(),pkg.getPricepoint(),pkgChintf.getPkgchintfid(),cppmaster.getPpid(),
							cppmaster.getPricePoint(),pkg.getValidityCatg(),cppmaster.getValidityType(),cppmaster.getValidity(),newCDR.getChargedAmount(),"OP",callbackData.getTransTime(),
							callbackData.getTransTime(),callbackData.getTransTime(),callbackData.getTransTime(),chintf.getRenewalType(),JocgString.intToBool(chintf.getRenewalRequired()),JocgStatusCodes.REN_NOT_REQUIRED,null,
							null,null,null,null,null,null,cppmaster.getCtype(),"UNLIMITED",
							1000,0,newCDR.getId(),newCDR.getJocgTransId(),"NA","NA","NA",
							null,cppmaster.getOperatorKey(),cppmaster.getOpParams(),cpp.getOperatorKey(),cpp.getOpParams(),0D,
							JocgStatusCodes.SUB_SUCCESS_PENDING,"Callback In Process","SDP-OFFLINEDUMP","OPERATOR",newCDR.getCampaignId(),newCDR.getCampaignName(),
							newCDR.getTrafficSourceId(),newCDR.getTrafficSourceName(),"NA",newCDR.getVoucherId(),newCDR.getVoucherName(),newCDR.getVoucherVendorName(),newCDR.getVoucherType(),newCDR.getVoucherDiscountType(),
							newCDR.getVoucherDiscount(),0D,platform.getSystemid(),platform.getPlatform(),"OP","NA","NA",
							"NA","NA","NA","NA","NA","NA","NA","NA",""+callbackData.getMsisdn(),callbackData.getTransTime());
					sub=updateSubscriberStatus(loggerHead,callbackData,sub,cpp,cppmaster,actionName);
					sub=subMgr.save(sub);
					logger.debug(loggerHead+"Subscriber Created with status "+sub.getStatus()+" against Id "+sub.getId());
					
				}else{
					logger.error("Failed to search Configurations for Callback "+callbackData);
				}
				
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
		}
		
		public ChargingCDR updateCDRStatus(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,ChargingCDR cdr,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,boolean updateSameCDR,String actionName,Subscriber sub,
				boolean activatedFromZero,boolean sameDayChurn,boolean churn20Min,boolean churn24Hour){
			loggerHead +="UCS::";
			try{
				//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(callbackData.getTransTime().getTime());
				java.util.Date startDate=cal.getTime();
				cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
				java.util.Date endDate1=cal.getTime();
				System.out.println(loggerHead+" action "+actionName);
				if("ACT".equalsIgnoreCase(actionName)){
						logger.debug("Updating CDR for "+chargedPricePoint);
						System.out.println(loggerHead+" Updating Successful Activation for "+chargedPricePoint.getPricePoint());
						if(cdr.getStatus()==JocgStatusCodes.CHARGING_FAILED_SDPPARKING){
							cdr.setParkingToActivationDate(startDate);
							cdr.setActivatedFromParking(true);
						}
						cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
						cdr.setStatusDescp("Customer Activated");
						cdr.setChargedPpid(chargedPricePoint.getPpid());
						cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
						cdr.setChargedAmount(chargedPricePoint.getPricePoint());
						cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
						cdr.setChargingDate(startDate);
						cdr.setValidityStartDate(startDate);
						cdr.setValidityEndDate(endDate1);
						logger.debug("Charged PpId "+cdr.getChargedPpid());
					cdr.setSdpStatusCode(callbackData.getErrorCode());
					cdr.setSdpStatusDescp(callbackData.toString());
					cdr.setSdpResponseTime(callbackData.getTransTime());
					cdr.setSdpTransactionId(callbackData.getTransactionId());
					System.out.println(loggerHead+" CDR Updated for Successful Activation ");
					
				}else if("DCT".equalsIgnoreCase(actionName) || "SUSPEND".equalsIgnoreCase(actionName)){
						cdr.setChurn20Min(churn20Min);
						cdr.setChurn24Hour(churn24Hour);
						cdr.setChurnSameDay(sameDayChurn);
						logger.debug(loggerHead+" DCT 20minchurnflag :"+cdr.getChurn20Min()+"|samedaychurn "+cdr.getChurnSameDay()+"|24hourchurn "+cdr.getChurn24Hour());
						//Create New CDR For DCT
						logger.debug(loggerHead+" Deactivation CDR updated for "+actionName+" status, now creating "+actionName+" CDR....");
						if(updateSameCDR==false){
							ChargingCDR cdr1=copyCDR(loggerHead,cdr);
							cdr1.setRequestType(actionName);
							cdr1.setRequestDate(callbackData.getTransTime());
							cdr1.setSdpResponseTime(callbackData.getTransTime());
							cdr1.setSdpStatusCode(callbackData.getErrorCode());
							cdr1.setSdpStatusDescp(callbackData.toString());
							cdr1.setChargedAmount(0D);
							cdr1.setChargedPpid(0);
							cdr1.setChargedOperatorKey("NA");
							cdr1.setChargedOperatorParams("NA");
							cdr1.setChargedPricePoint(0D);
							cdr1.setChargedValidityPeriod("NA");
							cdr1.setChargingDate(startDate);
							cdr1.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
							if("SUSPEND".equalsIgnoreCase(actionName)){
								cdr1.setStatusDescp("SUSPENDED");
							}else{
								cdr1.setStatusDescp("Deactivated");
							}
							cdr1.setSdpTransactionId(callbackData.getTransactionId());
							cdr1=cdrBuilder.insert(cdr1);
							System.out.println(loggerHead+" "+actionName+" new CDR created with id  :"+cdr1.getId()+", status "+cdr1.getStatus());
						}else{
							cdr.setRequestType(actionName);
							cdr.setRequestDate(callbackData.getTransTime());
							cdr.setSdpResponseTime(callbackData.getTransTime());
							cdr.setSdpStatusCode(callbackData.getErrorCode());
							cdr.setSdpStatusDescp(callbackData.toString());
							cdr.setChargedAmount(0D);
							cdr.setChargedPpid(0);
							cdr.setChargedOperatorKey("NA");
							cdr.setChargedOperatorParams("NA");
							cdr.setChargedPricePoint(0D);
							cdr.setChargedValidityPeriod("NA");
							cdr.setChargingDate(startDate);
							cdr.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
							if("SUSPEND".equalsIgnoreCase(actionName)){
								cdr.setStatusDescp("SUSPENDED");
							}else{
								cdr.setStatusDescp("Deactivated");
							}
							cdr.setSdpTransactionId(callbackData.getTransactionId());
							System.out.println(loggerHead+" "+actionName+" CDR updated with id  :"+cdr.getId()+", status "+cdr.getStatus());
						}
					
				}else if("REN".equalsIgnoreCase(actionName)){
					System.out.println(loggerHead+" "+actionName+" updateSameCDR : "+updateSameCDR+", activatedFromZero "+activatedFromZero);
						if(updateSameCDR==false){
							ChargingCDR cdr1=copyCDR(loggerHead,cdr);
							cdr1.setRequestType("REN");
							cdr1.setSdpResponseTime(callbackData.getTransTime());
							cdr1.setChargingDate(callbackData.getTransTime());
							cdr1.setSdpStatusCode(callbackData.getErrorCode());
							cdr1.setSdpStatusDescp(callbackData.toString());
							cdr1.setRequestDate(callbackData.getTransTime());
							cdr1.setChargingDate(startDate);
							cdr1.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr1.setStatusDescp("Renewal Successful");
							if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
								cdr1.setChargedAmount(chargedPricePoint.getPricePoint());
								cdr1.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
								cdr1.setChargedOperatorParams(chargedPricePoint.getOpParams());
								cdr1.setChargedPpid(chargedPricePoint.getPpid());
								cdr1.setChargedPricePoint(chargedPricePoint.getPricePoint());
								cdr1.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
								cdr1.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
								cdr1.setActivatedFromZero(activatedFromZero);
							}
							cdr1.setSdpTransactionId(callbackData.getTransactionId());
							cdr1=cdrBuilder.insert(cdr1);
							System.out.println(loggerHead+" "+actionName+" new CDR Created  : id "+cdr1.getId()+", status "+cdr1.getStatus()+", activatedFromZero "+cdr1.getActivatedFromZero());
						}else{
							cdr.setRequestType("REN");
							cdr.setSdpResponseTime(callbackData.getTransTime());
							cdr.setSdpStatusCode(callbackData.getErrorCode());
							cdr.setSdpStatusDescp(callbackData.toString());
							cdr.setRequestDate(callbackData.getTransTime());
							cdr.setChargingDate(startDate);
							cdr.setStatus(JocgStatusCodes.CHARGING_SUCCESS);
							cdr.setStatusDescp("Renewal Successful");
							if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
								cdr.setChargedAmount(chargedPricePoint.getPricePoint());
								cdr.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
								cdr.setChargedOperatorParams(chargedPricePoint.getOpParams());
								cdr.setChargedPpid(chargedPricePoint.getPpid());
								cdr.setChargedPricePoint(chargedPricePoint.getPricePoint());
								cdr.setChargedValidityPeriod(chargedPricePoint.getValidity()+chargedPricePoint.getValidityType());
								cdr.setFallbackCharged(chargedPricePoint.getIsfallback()>0?true:false);
								cdr.setActivatedFromZero(activatedFromZero);
							}
							cdr.setSdpTransactionId(callbackData.getTransactionId());
							System.out.println(loggerHead+" "+actionName+"  CDR updated  : id "+cdr.getId()+", status "+cdr.getStatus()+", activatedFromZero "+cdr.getActivatedFromZero());
						}
					
				}else if("GRACE".equalsIgnoreCase(actionName)){
					if(updateSameCDR==false){
						ChargingCDR cdr1=copyCDR(loggerHead,cdr);
						cdr1.setRequestType("GRACE");
						cdr1.setSdpResponseTime(callbackData.getTransTime());
						cdr1.setSdpStatusCode(callbackData.getErrorCode());
						cdr1.setSdpStatusDescp(callbackData.toString());
						cdr1.setRequestDate(callbackData.getTransTime());
						cdr1.setChargingDate(startDate);
						cdr1.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
						if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
							cdr1.setChargedAmount(0D);
							cdr1.setChargedOperatorKey("NA");
							cdr1.setChargedOperatorParams("NA");
							cdr1.setChargedPpid(0);
							cdr1.setChargedPricePoint(0D);
							cdr1.setChargedValidityPeriod("NA");
							cdr1.setFallbackCharged(false);
							
						}
						cdr1.setSdpTransactionId(callbackData.getTransactionId());
						cdrBuilder.insert(cdr1);
					}else{
							cdr.setRequestType("GRACE");
							cdr.setSdpResponseTime(callbackData.getTransTime());
							cdr.setSdpStatusCode(callbackData.getErrorCode());
							cdr.setSdpStatusDescp(callbackData.toString());
							cdr.setRequestDate(callbackData.getTransTime());
							cdr.setChargingDate(startDate);
							cdr.setStatus(JocgStatusCodes.CHARGING_FAILED_SDPGRACE);
							if(chargedPricePoint!=null && chargedPricePoint.getPpid()>0){
								cdr.setChargedAmount(0D);
								cdr.setChargedOperatorKey("NA");
								cdr.setChargedOperatorParams("NA");
								cdr.setChargedPpid(0);
								cdr.setChargedPricePoint(0D);
								cdr.setChargedValidityPeriod("NA");
								cdr.setFallbackCharged(false);
							}
							cdr.setSdpTransactionId(callbackData.getTransactionId());
						}
					
				}
				
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
			return cdr;
			
		}
		public Subscriber updateSubscriberStatus(String loggerHead,AirtelIndiaOfflineDumpRecord callbackData,Subscriber sub,ChargingPricepoint chargedPricePoint,ChargingPricepoint masterPricePoint,String actionName){
			loggerHead +="USS::";
			try{
				//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
				Calendar cal=Calendar.getInstance();
				cal.setTimeInMillis(callbackData.getTransTime().getTime());
				java.util.Date startDate=cal.getTime();
				cal.add(Calendar.DAY_OF_MONTH, chargedPricePoint.getValidity());
				java.util.Date endDate1=cal.getTime();
				if("ACT".equalsIgnoreCase(actionName)){
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
					sub.setStatusDescp(callbackData.getStatus());
					sub.setSubSessionRevenue(callbackData.getAmount());
					sub.setChargedAmount(chargedPricePoint.getPricePoint());
					sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
					sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
					sub.setValidityEndDate(endDate1);
					sub.setNextRenewalDate(endDate1);
					sub.setLastRenewalDate(callbackData.getTransTime());
					sub.setActivationSource(callbackData.getReferer());
				}else if("DCT".equalsIgnoreCase(actionName)){
					sub.setStatus(JocgStatusCodes.UNSUB_SUCCESS);
					sub.setUnsubDate(new java.util.Date(System.currentTimeMillis()));
					//sub.setChargedAmount(0D);
					sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
				}else if("SUSPEND".equalsIgnoreCase(actionName)){
					sub.setStatus(JocgStatusCodes.SUB_FAILED_SUSPENDFROMPARKING);
					sub.setSuspendDate(new java.util.Date(System.currentTimeMillis()));
							//sub.setChargedAmount(0D);
					sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
					logger.debug(loggerHead+" :: SUSPEND :: Subscriber Status set to "+sub.getStatus());
				
				}else if("REN".equalsIgnoreCase(actionName)){
						Calendar cal1=Calendar.getInstance();
						cal.setTime(startDate);
						cal.add(Calendar.DATE, chargedPricePoint.getValidity());
						java.util.Date endDate=cal.getTime();
						sub.setStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
						sub.setStatusDescp("Customer Renewed");
						sub.setSubSessionRevenue(sub.getSubSessionRevenue()+chargedPricePoint.getPricePoint());
						sub.setChargedAmount(chargedPricePoint.getPricePoint());
						sub.setChargedOperatorKey(chargedPricePoint.getOperatorKey());
						sub.setChargedOperatorParams(chargedPricePoint.getOpParams());
						sub.setValidityEndDate(endDate);
						sub.setNextRenewalDate(endDate);
						sub.setLastRenewalDate(callbackData.getTransTime());
						logger.debug(loggerHead+" :: REN :: Subscriber Status set to "+sub.getStatus());
				}else if("GRACE".equalsIgnoreCase(actionName)){
					sub.setStatus(JocgStatusCodes.SUB_SUCCESS_GRACE);
					sub.setGraceDate(callbackData.getTransTime());
							//sub.setChargedAmount(0D);
					sub.setUserField_0("UNSUBMODE-"+callbackData.getChannelName());
					logger.debug(loggerHead+" :: GRACE :: Subscriber Status set to "+sub.getStatus());
				
				}
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
			return sub;
		}
		
		public ChargingCDR copyCDR(String loggerHead,ChargingCDR cdr){
			loggerHead +="CopyCDR::";
			ChargingCDR newCDR=null;
			
			try{
				newCDR=new ChargingCDR(null,cdr.getSubRegId(),cdr.getLandingPageId(),cdr.getRoutingScheduleId(),cdr.getJocgTransId(),cdr.getRequestCategory(),cdr.getRequestType(),
						cdr.getSubscriberId(),cdr.getMsisdn(),cdr.getPlatformName(),cdr.getRqPackageName(),cdr.getRqPackagePrice(),cdr.getRqPackageValidity(),
						cdr.getRqNetworkType(),cdr.getRqVoucherVendor(),cdr.getRqAmountToBeCharged(),cdr.getRqVoucherCode(),cdr.getSystemId(),cdr.getSourceNetworkId(),
						cdr.getSourceNetworkName(),cdr.getSourceNetworkCode(),cdr.getAggregatorId(),cdr.getAggregatorName(),cdr.getUseParentHandler(),cdr.getChargingInterfaceId(),
						cdr.getChargingInterfaceName(),cdr.getCountryName(),cdr.getCurrencyName(),cdr.getGmtDiff(),cdr.getChargingInterfaceType(),cdr.getPackageChintfId(),cdr.getCircleId(),
						cdr.getCircleName(),cdr.getCircleCode(),cdr.getPackageId(),cdr.getPackageName(),cdr.getPackagePrice(),cdr.getServiceId(),cdr.getServiceName(),cdr.getServiceCategory(),
						cdr.getPricePointId(),cdr.getOperatorPricePoint(),cdr.getValidityCategory(),cdr.getValidityUnit(),cdr.getValidityPeriod(),cdr.getMppOperatorKey(),
						cdr.getMppOperatorParams(),cdr.getChargedPpid(),cdr.getChargedPricePoint(),cdr.getFallbackCharged(),cdr.getChargedValidityPeriod(),cdr.getChargedOperatorKey(),
						cdr.getChargedOperatorParams(),cdr.getVoucherId(),cdr.getVoucherName(),cdr.getVoucherVendorId(),cdr.getVoucherVendorName(),cdr.getVoucherType(),cdr.getVoucherDiscount(),
						cdr.getVoucherDiscountType(),cdr.getChargingCategory(),cdr.getDiscountGiven(),cdr.getChargedAmount(),cdr.getRenewalRequired(),cdr.getRenewalCategory(),cdr.getChargingDate(),
						cdr.getValidityStartDate(),cdr.getValidityEndDate(),cdr.getDeviceDetails(),cdr.getUserAgent(),cdr.getReferer(),cdr.getTrafficSourceId(),cdr.getTrafficSourceName(),
						cdr.getTrafficSourceToken(),cdr.getCampaignId(),cdr.getCampaignName(),cdr.getContentType(),cdr.getPublisherId(),cdr.getNotifyStatus(),cdr.getNotifyDescp(),cdr.getNotifyTime(),
						cdr.getNotifyScheduleTime(),cdr.getChurn20Min(),cdr.getChurnSameDay(),cdr.getNotifyChurn(),cdr.getNotifyChurnStatus(),cdr.getNotifyTimeChurn(),cdr.getLandingPageId(),cdr.getCgImageId(),
						cdr.getOtpStatus(),cdr.getOtpPin(),cdr.getStatus(),cdr.getStatusDescp(),cdr.getRequestDate(),cdr.getCgStatusCode(),cdr.getCgStatusDescp(),cdr.getCgTransactionId(),
						cdr.getCgResponseTime(),cdr.getSdpStatusCode(),cdr.getSdpStatusDescp(),cdr.getSdpTransactionId(),cdr.getSdpLifeCycleId(),cdr.getSdpResponseTime(),new java.util.Date(),false,cdr.getChurn24Hour(),cdr.getSubRegDate());
			}catch(Exception e){
				logger.error(loggerHead+" Exception "+e);
			}
			return newCDR;
		}
	
	
}
