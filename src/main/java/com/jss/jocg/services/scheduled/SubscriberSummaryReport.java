package com.jss.jocg.services.scheduled;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cdr.dao.SubscriberStatsDao;
import com.jss.jocg.cdr.entities.SubscriberStats;
import com.jss.jocg.services.dao.SubscriberRepository;

@Component
public class SubscriberSummaryReport {
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriberSummaryReport.class);
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired SubscriberStatsDao substatsDao;

	
	//@Scheduled(cron = "03 05 * * * *")
	//@Scheduled(initialDelay=1, fixedRate=300000)
	 public void schedularThread() {
		String loggerHead="SUBSUMMARY-REPORT::";
		 if(jocgCache.isOfflineProcessEnabled()){
			 logger.info(loggerHead+" Schedular Invoked.");
				try{
					
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
				//	cal.add(Calendar.DATE, -1);
					cal.set(Calendar.HOUR, 0);
					cal.set(Calendar.MINUTE, 0);
					cal.set(Calendar.SECOND, 0);
					
					java.util.Date startDate=cal.getTime();
					cal.set(Calendar.HOUR, 23);
					cal.set(Calendar.MINUTE, 59);
					cal.set(Calendar.SECOND, 59);
					java.util.Date endDate=cal.getTime();
					cal.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cDate=cal.getTime();
					
					 logger.info(loggerHead+" Searching Existing stats data for "+cDate);
					//List<SubscriberStats> existingList=substatsDao.findByDatestr(cDate);
					 //logger.info(loggerHead+" Existing Stats  "+existingList);
					
					 List<SubscriberStats> subStats=subscriberDao.getSubscriberStatsForTheDay();
					logger.info(loggerHead+" New Stats  count "+subStats.size());
					
					if(subStats!=null && subStats.size()>0){
						 logger.info(loggerHead+" Replacing Stats..... ");
						//if(existingList==null || existingList.size()<=0) 
							
							//substatsDao.delete(existingList);
						substatsDao.deleteByDatestrBetween(startDate,endDate);
						
						SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					   String datestr= sdf.format(cDate);
						for(SubscriberStats s:subStats){
							s.setDatestr(sdf.parse(datestr));
							s=substatsDao.save(s);
							 logger.debug(loggerHead+" NewObject Inserted "+s);
						}
						logger.info(loggerHead+" New Stats Updated for date "+datestr);
					}
				
					
					
				}catch(Exception e){
					 logger.error(loggerHead+" Exception "+e.getMessage());
				}
		 }
		
		 
	 }

}
