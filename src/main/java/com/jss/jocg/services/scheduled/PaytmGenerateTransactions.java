package com.jss.jocg.services.scheduled;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;



@Component
public class PaytmGenerateTransactions {
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriberUpdateProcess.class);
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired CDRBuilder cdrBuilder;
	@Autowired SubscriptionManager subMgr;
	
	@Value("${jocg.offlinecdr.paytm.update}")
	String offlineCDRFolder;
	
	//@Scheduled(initialDelay=1, fixedRate=86400000)
	 public void processFiles(){
		String logHeader="PAYTM-REGEN-REPORT :: ";
		if(jocgCache.isOfflineProcessEnabled()){
				
				 try{
					 logger.debug(logHeader+" PROCESSOR Invoked ");
					 File fd=new File(offlineCDRFolder);
					 logger.debug(logHeader+"  Base folder "+offlineCDRFolder);
					 if(fd.exists() && fd.isDirectory()){
							File[] flist=fd.listFiles();
							if(flist!=null && flist.length>0){
								for(File f:flist){
									 logger.debug(logHeader+" Processing file  "+f.getAbsolutePath());
									 
									 if(!f.isDirectory() && ((System.currentTimeMillis()-f.lastModified())/1000)>5){
										 long processStartTime=System.currentTimeMillis();	
										 BufferedReader br=new BufferedReader(new FileReader(f));
							                    String newLine="";int processedRecords=0;
							                    while((newLine=br.readLine())!=null){
							                    	   
							                    		processRecord(logHeader,newLine,f.getName());
							                           processedRecords++;
							                            WAIT(10);
							                    }
							                    
							                    logger.info(logHeader+"  Processed Count in ("+f.getCanonicalPath()+") :"+processedRecords+", Time Taken "+((System.currentTimeMillis()-processStartTime)/60000)+" minutes.");
							                    br.close();
							                    br=null;
							                    try{
							                    	f.renameTo(new File(offlineCDRFolder+"/processed/"+f.getName()));
							                    }catch(Exception ee){
							                    	f.delete();
							                    }
							                  WAIT(50);
							                  
										}//if
										else  
											logger.debug(logHeader+" Skipped File "+f.getAbsolutePath()+"|IsDirectory="+f.isDirectory());
										f=null;
								}//for
								
							}//if
					 }else  
						 logger.debug(logHeader+" Invalid Folder "+offlineCDRFolder);
					 
					 
				 }catch(Exception e){
					 
				 }
		 } else  logger.debug(logHeader+":: Offline Processes are disabled on this server ");
		
	}
	
	public synchronized void WAIT(int ms){
		 try{
			this.wait(ms);
		 }catch(Exception e){}
		 
	 }
	
	public void  processRecord(String logHeader,String newLine,String fileName){
		boolean erroFlag=false;
		String erroMessage="";
		try{
			SimpleDateFormat sdfTransTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//2017-12-06 14:55:5
			logger.debug("Processing Record "+newLine);
			String transTime=newLine.substring(0,newLine.indexOf(","));
			java.util.Date renewalTime=sdfTransTime.parse(transTime);
			newLine=newLine.substring(newLine.indexOf("subId")+5);
			String subscriberId=newLine.substring(0,newLine.indexOf(","));
			newLine=newLine.substring(newLine.indexOf("{"));
			newLine=newLine.substring(0,newLine.indexOf("}"));
			String transId=parseTransId(newLine);
			createRenewalTransaction(renewalTime,subscriberId,newLine,transId);
			logger.debug("Details to Process TransTime="+transTime+"|ParsedDate:"+renewalTime+"|TransId="+transId+"|SubscriberId="+subscriberId+"|PaytemResp="+newLine);
			
		}catch(Exception e){
			
		}finally{
			try{
				if(erroFlag==true){
					String fileNameStatus=offlineCDRFolder+"/processed/"+fileName+".failed";
					FileWriter fw=new FileWriter(fileNameStatus,true);
					fw.write("\n"+newLine+"|"+erroMessage);
					fw.flush();
					fw.close();
				}
			}catch(Exception e){
				logger.error(logHeader+"Exception while writing status "+e.getMessage());
			}
		}
	}
	
	public boolean createRenewalTransaction(java.util.Date transTime,String subscriberId,String paytmResponse,String transId){
		boolean status=true;
		String logHeader="PAYTEM-REGEN-RENTRANS::";
		try{
			Subscriber sub=subscriberDao.findById(subscriberId);
			if(sub!=null && sub.getId()!=null){
				logHeader +="{subId="+sub.getSubscriberId()+",msisdn="+sub.getMsisdn().longValue()+"} ::";
				logger.debug("Found Subscriber Id "+sub.getId());
				buildCDR(logHeader,sub,transId,JocgStatusCodes.CHARGING_SUCCESS,paytmResponse,transTime);
			}
			
			
		}catch(Exception e){
			status=false;
		}
		return status;
	}
	
	public String parseTransId(String str){
		String transId="";
		try{
			str=str.replaceAll("\"", "");
			String[] strArr=str.split(",");
			for(String s:strArr){
				if(s.contains("ORDERID")){
					transId=s.substring(s.indexOf(":")+1);
					break;
				}
			}
			//"ORDERID":"JOCGREN021241260"
		}catch(Exception e){
			
		}
		return transId;
		
	}
	
	public void buildCDR(String logHeader,Subscriber sub,String transId,Integer status,String statusDescp,java.util.Date transTime){
		logHeader +="buildCDR ::";
		try{
			logger.debug(logHeader+"Method Invoked ..");
		ChargingInterface sourceNetwork=jocgCache.getChargingInterface(sub.getSourceNetworkId());
		logger.debug(logHeader+"sourceNetwork "+sourceNetwork);
		ChargingInterface chintf=jocgCache.getChargingInterface(sub.getChargingInterfaceId());
		logger.debug(logHeader+"chintf "+chintf);
		PackageChintfmap pkgchintf=jocgCache.getPackageChargingInterfaceMapById(sub.getPackageChargingInterfaceMapId());
		logger.debug(logHeader+"pkgchintf "+pkgchintf);
		Circleinfo circle=jocgCache.getCircleInfo(sub.getCircleId());
		if(circle==null) circle=new Circleinfo();
		logger.debug(logHeader+"circle "+circle);
		PackageDetail pkg=jocgCache.getPackageDetailsByName(sub.getPackageName());
		logger.debug(logHeader+"pkg "+pkg);
		Service service=jocgCache.getServicesById(pkg.getServiceid());
		logger.debug(logHeader+"service "+service);
		ChargingPricepoint chppmaster=jocgCache.getPricePointListById(sub.getPricepointId());
		logger.debug(logHeader+"service "+chppmaster);
		ChargingPricepoint chpp=chppmaster;//must be changed when fallback is implemented
		Calendar cal=Calendar.getInstance();
		cal.setTimeInMillis(transTime.getTime());
		java.util.Date chargingDate=cal.getTime();
		cal.add(Calendar.DATE, chpp.getValidity());
		java.util.Date validityEndDate=cal.getTime();
		TrafficSource ts=null;TrafficCampaign tc=null;
		try{
		ts=jocgCache.getTrafficSourceById(sub.getTrafficSourceId());
		logger.debug(logHeader+"traffic Source "+ts);
		tc=jocgCache.getTrafficCampaignById(sub.getCampaignId());
		logger.debug(logHeader+"traffic Campaign "+tc);
		}catch(Exception e1){
			logger.error(logHeader+"Exception while searching traffic source id "+sub.getTrafficSourceId()+", and campaign "+sub.getCampaignId());
		}
		ChargingCDR newCDR=cdrBuilder.getNewCDRInstance(sub.getSubscriberId(), transId, "SYSTEM-REN", "REN", sub.getSubscriberId(), sub.getMsisdn(), sub.getPlatformId(), sub.getSystemId(), 
				new JocgRequest(), sourceNetwork, chintf, pkgchintf, circle, pkg, service, chppmaster, chpp, null, "WALLET", 0D, chpp.getPricePoint(), "SYSTEM-RENEWAL", 
				chargingDate, sub.getValidityStartDate(), validityEndDate, "NA", "NA", "NA", ts, tc, "NA", "NA", JocgStatusCodes.NOTIFICATION_NOTSENT, "NOT To BE SENT", null, JocgStatusCodes.NOTIFICATION_NOTSENT, null, null, false, false, false, 
				null, null, null, -1, "NA", JocgStatusCodes.CHARGING_SUCCESS, "Renewal Successful", new java.util.Date(System.currentTimeMillis()), "NA", "NA", "NA", new java.util.Date(System.currentTimeMillis()), ""+status, statusDescp, "NA", ""+sub.getMsisdn(), new java.util.Date(), 
				null, null, false, false, false);
		logger.debug(logHeader+"Inserting new Renewal CDR......");
		newCDR=cdrBuilder.insert(newCDR);
		logger.info(logHeader+"buildCDR():: Renewal CDR Created with ID "+newCDR.getId());
		}catch(Exception e){
			logger.error(logHeader+"buildCDR()::"+e.getMessage());
		}
	
	}
		
}
