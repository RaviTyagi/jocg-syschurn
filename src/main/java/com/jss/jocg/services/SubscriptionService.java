package com.jss.jocg.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.SubscriptionManager;
import com.jss.jocg.services.bo.airtel.ActivationCallback;
import com.jss.jocg.services.bo.airtel.AirtelApiResponseWraper;
import com.jss.jocg.services.bo.model.QRCustomerStatus;
import com.jss.jocg.services.bo.model.QRSubscribedPack;
import com.jss.jocg.services.bo.model.QueryResponse;
import com.jss.jocg.services.bo.model.RequestParams;
import com.jss.jocg.services.dao.SubscriberTestRepository;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.services.data.SubscriberTest;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;


@RestController
@RequestMapping("/")
public class SubscriptionService extends JocgService{
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriptionService.class);
	
	//@Autowired MongoTemplate mongoTemplate;
	
	@Value("${jocg.checksumkey}")
	public String validationKey="";
	@RequestMapping("jocg/sub/tbms/remove")
	public @ResponseBody String removeFromTBMS(@RequestParam(value="platform",required=false) String validity,
			@RequestParam(value="subid",required=false) String subscriberId,@RequestParam(value="service",required=false) String serviceName,
			@RequestParam(value="msisdn",required=false) Long requestMsisdn,@RequestParam(value="operator",required=false) String operator,@RequestParam(value="pkgname",required=false) String packageName){
		String outputString="",responseString="";
		String logHeader="PackRemove::fromSuntec() :: ";
		 String wsURL="";String queryData="";
		try{
			queryData="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.sdpl.tbms.suntecgroup.com\">"
					 +"<soapenv:Header/>"
					 +"<soapenv:Body>"
					 +"<ser:packRemove>"
					 +"<!--Optional:-->"
					 +"<ser:msisdn>"+requestMsisdn+"</ser:msisdn>"
					 +"<!--Optional:-->"
					 +"<ser:subscriberId>"+subscriberId+"</ser:subscriberId>"
					 +"<!--Optional:-->"
					 +"<ser:serviceId>"+serviceName+"</ser:serviceId>"
					 +"<!--Optional:-->"
					 +"<ser:systemId>1Y</ser:systemId>"
					 +"<!--Optional:-->"
					 +"<ser:password>W@P123</ser:password>"
					 +"<!--Optional:-->"
					 +"<ser:date></ser:date>"
					 +"<!--Optional:-->"
					 +"<ser:operatorId>"+operator+"</ser:operatorId>"
					 +"<!--Optional:-->"
					 +"<ser:validity>"+validity+"</ser:validity>"
					 +"<!--Optional:-->"
					 +"<ser:packCode>"+packageName+"</ser:packCode>"
					 +"<!--Optional:-->"
					 +"<ser:UserField1></ser:UserField1>"
					 +"<!--Optional:-->"
					 +"<ser:UserField2></ser:UserField2>"
					 +"<!--Optional:-->"
					 +"<ser:UserField3></ser:UserField3>"
					 +"<!--Optional:-->"
					 +"<ser:UserField4></ser:UserField4>"
					 +"<!--Optional:-->"
					 +"<ser:UserField5>0</ser:UserField5>"
					 +"<!--Optional:-->"
					 +"<ser:UserField6>0</ser:UserField6>"
					 +"<!--Optional:-->"
					 +"<ser:UserField7>0</ser:UserField7>"
					 +"<!--Optional:-->"
					 +"<ser:UserField8>0</ser:UserField8>"
					 +"<!--Optional:-->"
					 +"<ser:UserField9>0</ser:UserField9>"
					 +"<!--Optional:-->"
					 +"<ser:UserField10>0</ser:UserField10>"
					 +"</ser:packRemove>"
					 +"</soap:Body>"
					+"</soap:Envelope>";
			System.out.println("Query Data "+queryData);
			URL url = new URL(wsURL);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection)connection;
			
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buffer = new byte[queryData.length()];
			buffer = queryData.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			System.out.println("Data Length "+b.length);
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty("Content-Length",
			String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("Cache-Control", "no-cache");
			httpConn.setRequestProperty("Pragma", "no-cache");
			httpConn.setRequestProperty("SOAPAction", "customerEnquiryGen");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			//httpConn.connect();
			//int responseCode=httpConn.getResponseCode();
			//System.out.println("Response Code "+responseCode);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			System.out.println("Out Stream "+out);
			out.write(b);
			out.flush();
			out.close();
			System.out.println("Out Stream Data flushed with  "+b.length+" bytes");
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			System.out.println("In Stream "+in);
			System.out.println("Reading Input");
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				System.out.println(">>received data "+responseString);
				outputString = outputString + responseString;
			}
			
			in.close();
			in=null;
			httpConn.disconnect();
			httpConn=null;
			url=null;
			
		}catch(Exception e){
			System.out.println("Exception "+e);
			e.printStackTrace();
			
		}
		finally{
			try{
				String dataToWrite="\n"+requestMsisdn+"|"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+queryData+"|Response Data |"+outputString;
				SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
				FileWriter fw=new FileWriter("/home/jocg/logs/packremovequery_"+sdf.format(new java.util.Date())+".txt",true);
				fw.write(dataToWrite);
				fw.flush();
				fw.close();
			}catch(Exception e){}
		}
		
		return outputString;
	}
	
	@RequestMapping("jocg/sub/tbms/status")
	public @ResponseBody String checkStatusTBMS(@RequestParam(value="platform",required=false) String platform,
			@RequestParam(value="subid",required=false) String subscriberId,@RequestParam(value="service",required=false) String serviceName,
			@RequestParam(value="msisdn",required=false) Long requestMsisdn,@RequestParam(value="operator",required=false) String operator){
		String outputString="",responseString="";
		String logHeader="STATUS::processStatusToSuntec() :: ";
		 String wsURL="";String queryData="";
		try{
			
			int modeVal=(int)(System.currentTimeMillis()%4);
			wsURL =(modeVal<=0)?"http://172.31.19.172:8082/tbms/services/SDPLWebService?wsdl"
					:(modeVal<=1)?"http://172.31.19.172:8083/tbms/services/SDPLWebService?wsdl"
					:(modeVal<=2)?"http://172.31.19.175:8082/tbms/services/SDPLWebService?wsdl"
					:"http://172.31.19.175:8083/tbms/services/SDPLWebService?wsdl";
			//2012-01-18T14:48:00Z
			queryData="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.sdpl.tbms.suntecgroup.com\">"+
					"<soapenv:Header/>"+
					"<soapenv:Body> "+
					"<ser:customerEnquiry>"+
					"<!--Optional:--> "+
					"<ser:msisdn>"+requestMsisdn+"</ser:msisdn>"+
					"<!--Optional:--> "+
					"<ser:subscriberId>"+subscriberId+"</ser:subscriberId>"+
					"<!--Optional:-->"+
					"<ser:serviceId>"+serviceName+"</ser:serviceId>"+
					"<!--Optional:-->"+
					"<ser:operatorID>"+operator+"</ser:operatorID>"+
					"<!--Optional:--> "+
					"<ser:systemId>1Y</ser:systemId>"+
					"<!--Optional:--> "+
					"<ser:password>W@P123</ser:password>"+
					"<!--Optional:--> "+
					"<ser:date></ser:date>"+
					"<!--Optional:--> "+
					"<ser:UserField1></ser:UserField1>"+
					"<!--Optional:--> "+
					"<ser:UserField2></ser:UserField2>"+
					"<!--Optional:--> "+
					"<ser:UserField3></ser:UserField3>"+
					"<!--Optional:--> "+
					"<ser:UserField4></ser:UserField4>"+
					"<!--Optional:--> "+
					"<ser:UserField5></ser:UserField5>"+
					"<!--Optional:--> "+
					"<ser:UserField6>0</ser:UserField6>"+
					"<!--Optional:-->"+
					"<ser:UserField7>0</ser:UserField7>"+
					"<!--Optional:-->"+
					"<ser:UserField8>0</ser:UserField8>"+
					"<!--Optional:-->"+
					"<ser:UserField9>0</ser:UserField9>"+
					"<!--Optional:-->"+
					"<ser:UserField10>0</ser:UserField10>"+
					"</ser:customerEnquiry>"+
					"</soapenv:Body>"+
					"</soapenv:Envelope>";

			
			System.out.println("Query Data "+queryData);
			URL url = new URL(wsURL);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection)connection;
			
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buffer = new byte[queryData.length()];
			buffer = queryData.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			System.out.println("Data Length "+b.length);
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty("Content-Length",
			String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("Cache-Control", "no-cache");
			httpConn.setRequestProperty("Pragma", "no-cache");
			httpConn.setRequestProperty("SOAPAction", "customerEnquiryGen");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			//httpConn.connect();
			//int responseCode=httpConn.getResponseCode();
			//System.out.println("Response Code "+responseCode);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			System.out.println("Out Stream "+out);
			out.write(b);
			out.flush();
			out.close();
			System.out.println("Out Stream Data flushed with  "+b.length+" bytes");
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			System.out.println("In Stream "+in);
			System.out.println("Reading Input");
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				System.out.println(">>received data "+responseString);
				outputString = outputString + responseString;
			}
			
			in.close();
			in=null;
			httpConn.disconnect();
			httpConn=null;
			url=null;
			
			
		}catch(Exception e){
			System.out.println("Exception "+e);
			e.printStackTrace();
		}
		finally{
			try{
				String dataToWrite="\n"+requestMsisdn+"|"+new java.sql.Timestamp(System.currentTimeMillis())+"|"+queryData+"|Response Data |"+outputString;
				SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
				FileWriter fw=new FileWriter("/home/jocg/logs/statusquery_"+sdf.format(new java.util.Date())+".txt",true);
				fw.write(dataToWrite);
				fw.flush();
				fw.close();
			}catch(Exception e){}
		}
		
		
		return outputString;
	}
	
	
	
	/**
	 * Request Map : /sub/status
	 * @param platfrom
	 * @param subid : subscriberid or mobile number 
	 * @param service : service Name like MOBTV
	 * @param key : checksumKey
	 * @return
	 */
	
	@RequestMapping("jocg/sub/status")
	public @ResponseBody QueryResponse checkStatus(@RequestParam(value="platform",required=false) String platform,
			@RequestParam(value="subid",required=false) String subscriberId,@RequestParam(value="service",required=false) String serviceName,
			@RequestParam(value="msisdn",required=false) String msisdnStr){
		QueryResponse resp=new QueryResponse();
		Long msisdn=0L;
		String queryTransId="JOCGQR-"+System.currentTimeMillis();
		try{
				msisdn=parseMsisdn();
				Long requestMsisdn=JocgString.strToLong(msisdnStr, 0L);
				requestMsisdn=(requestMsisdn==null)?0L:requestMsisdn;
				if((msisdn==null || msisdn<=0) && requestMsisdn>0) msisdn=requestMsisdn;
				
				RequestParams params=new RequestParams();
				params.setPlatform(platform);
				params.setSubscriberId(subscriberId);
				params.setMsisdn(msisdn);
				params.setServiceName(serviceName);
				Subscriber sub=subMgr.checkStatus(subscriberId, msisdn, serviceName);
				
				logger.info("Sub Search Query(subscriberId="+subscriberId+",msisdn="+msisdn+",serviceName="+serviceName+") Found "+sub);
				if(sub==null || (sub.getMsisdn()<=0 && (sub.getSubscriberId()==null || sub.getSubscriberId().length()<=0 || "NA".equalsIgnoreCase(sub.getSubscriberId())) )){
					resp.setMsisdn(msisdn);
					resp.setServiceId(serviceName);
					resp.setTransactionID(queryTransId);
					resp.setReturnCode(0);
					resp.setReturnMessage("CUSTOMER ENQUIRY SUCCESSFUL");
					
					QRCustomerStatus customerStatus=new QRCustomerStatus();
					customerStatus.setCloseDate(null);
					customerStatus.setCustomerCategory("GEN");
					customerStatus.setCustomerStatus("N");
					customerStatus.setOpenDate(null);
					customerStatus.setSubRegId("");
					customerStatus.setSubscriberID(subscriberId);
				}else{
					resp.setMsisdn(msisdn);
					resp.setServiceId(serviceName);
					resp.setTransactionID(queryTransId);
					resp.setReturnCode(0);
					resp.setReturnMessage("CUSTOMER ENQUIRY SUCCESSFUL");
					String statusCode=subMgr.parseSubscriberStatus(sub.getStatus(),((sub.getValidityEndDate()==null)?sub.getNextRenewalDate():sub.getValidityEndDate()));
					//Populate Present Status of the Subscriber
							QRCustomerStatus customerStatus=new QRCustomerStatus();
							//Set Unsub Date
							if("D".equalsIgnoreCase(statusCode)){
								customerStatus.setCloseDate(sub.getUnsubDate());
								customerStatus.setCustomerStatus("D");
							}else if("A".equalsIgnoreCase(statusCode)){
								customerStatus.setCloseDate(null);
								customerStatus.setCustomerStatus("A");
							}else{
								customerStatus.setCloseDate(null);
								customerStatus.setCustomerStatus("P");
							}
							customerStatus.setCustomerCategory(sub.getCustomerCategory());
							customerStatus.setOpenDate(sub.getSubChargedDate());
							customerStatus.setSubRegId(sub.getId());
							customerStatus.setSubscriberID(sub.getSubscriberId());
					resp.setCustomerStatus(customerStatus);
					logger.info("statusCode "+statusCode);
					//Populate Active/Pending Pack Details
					if("A".equalsIgnoreCase(statusCode)){
						QRSubscribedPack subPack=new QRSubscribedPack();
						//Calculate Expiry & Validity
						sub.getChargedOperatorKey();
						ChargingPricepoint chpp=(sub.getChargingInterfaceId()==13)?jocgCache.getPricePointListById(sub.getPricepointId()):jocgCache.getPricePointListByChargingInterface(sub.getChargingInterfaceId(), sub.getChargedAmount());
						
						java.util.Date dExpiry=(sub.getValidityEndDate()==null)?sub.getNextRenewalDate():sub.getValidityEndDate();
						
						java.util.Date dCurrent=new java.util.Date(System.currentTimeMillis());
						long dayMillis=(1000 * 60 * 60 * 24);
						long timeDiff=(dExpiry.getTime()-dCurrent.getTime());
						int dayDiff=(int)(timeDiff/dayMillis);
						logger.debug(sub.getMsisdn()+"dexpiryTime "+dExpiry.getTime()+", dcurrentTime "+dCurrent.getTime()+", difference in ms "+timeDiff+", packValidity:"+dayDiff);
						
						int packValidity=("WALLET".equalsIgnoreCase(sub.getChargingInterfaceType()) || sub.getChargingInterfaceId()==13)?sub.getValidity():
							(chpp!=null && chpp.getPpid()>0)?chpp.getValidity():(dayDiff>0)?(dayDiff+1):0;
						int validity=(dayDiff>0)?dayDiff:0;
						logger.debug(sub.getMsisdn()+" dExpiry "+dExpiry+", dayDiff "+dayDiff+", packValidity "+packValidity+", chpp "+chpp+", validity "+sub.getValidity());
//						SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
//						 String cDateStr=sdf.format(new java.util.Date());
//						 String cExpiryStr=sdf.format(sub.getValidityEndDate());
//						int currentDate=JocgString.strToInt(cDateStr, 0);
//						int expiryDate=JocgString.strToInt(cExpiryStr, 0);
//						
//						int packValidity=(chpp!=null && chpp.getPpid()>0)?chpp.getValidity():(expiryDate>0 && currentDate>0)?(expiryDate-currentDate)>=0?((expiryDate-currentDate)+1):0:0;
//						
//						int validity=(expiryDate>0 && currentDate>0)?(expiryDate-currentDate)>=0?((expiryDate-currentDate)):0:0;
						
						
						logger.debug(sub.getMsisdn()+" :: 1");
						Calendar cal=Calendar.getInstance();
						cal.setTimeInMillis(System.currentTimeMillis());
						if(validity>0)cal.add(Calendar.DAY_OF_MONTH, validity);
						dExpiry=cal.getTime();
						subPack.setExpiryDate(dExpiry);
						logger.debug(sub.getMsisdn()+" :: 2");
						if("CMOF".equalsIgnoreCase(sub.getSourceNetworkCode()))
							subPack.setOperatorId("CM_OF");
						else
							subPack.setOperatorId(sub.getSourceNetworkCode());
						subPack.setPackId(sub.getPackageName());
						subPack.setPackValidity(packValidity);
						logger.debug(sub.getMsisdn()+" :: 3 : "+subPack.getPackValidity());
						subPack.setValidityUnit(sub.getValidityUnit());
						subPack.setPurchasePrice(sub.getChargedAmount());
						logger.debug(sub.getMsisdn()+" :: 4");
						subPack.setPurchaseTime(sub.getSubChargedDate());
						subPack.setRenewalDate(sub.getLastRenewalDate());
						logger.debug(sub.getMsisdn()+" :: 5");
						subPack.setRenewalYesNo(sub.getRenewalRequired().booleanValue()?"Y":"N");
						subPack.setSubRegId(sub.getId());
						logger.debug(sub.getMsisdn()+" :: 6");
						resp.addSubscribedPack(subPack);
					}else if("P".equalsIgnoreCase(statusCode)){
						QRSubscribedPack subPack=new QRSubscribedPack();
						subPack.setExpiryDate(sub.getValidityEndDate());
						if("CMOF".equalsIgnoreCase(sub.getSourceNetworkCode()))
							subPack.setOperatorId("CM_OF");
						else
							subPack.setOperatorId(sub.getSourceNetworkCode());
						//subPack.setOperatorId(sub.getSourceNetworkCode());
						subPack.setPackId(sub.getPackageName());
						subPack.setPackValidity(sub.getValidity());
						subPack.setValidityUnit(sub.getValidityUnit());
						subPack.setPurchasePrice(sub.getChargedAmount());
						subPack.setPurchaseTime(sub.getSubChargedDate());
						subPack.setRenewalDate(sub.getLastRenewalDate());
						subPack.setRenewalYesNo(sub.getRenewalRequired().booleanValue()?"Y":"N");
						subPack.setSubRegId(sub.getId());
						resp.addPendingPacks(subPack);
					}
					
					
					
				}
				logger.info("Resp Object "+resp);
		}catch(Exception e){
			resp.setMsisdn(msisdn);
			resp.setServiceId(serviceName);
			resp.setTransactionID(queryTransId);
			resp.setReturnCode(1);
			resp.setReturnMessage("CUSTOMER ENQUIRY FAILED");
			QRCustomerStatus customerStatus=new QRCustomerStatus();
			customerStatus.setCloseDate(null);
			customerStatus.setCustomerCategory("GEN");
			customerStatus.setCustomerStatus("N");
			customerStatus.setOpenDate(null);
			customerStatus.setSubRegId("");
			customerStatus.setSubscriberID(subscriberId);
			logger.error("Resp Object "+resp+", Exception "+e);
		}
		
		return resp;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@RequestMapping("jocg/sub/sub/test/{act}/{name}")
	public @ResponseBody SubscriberTest testSub(@PathVariable String act,@PathVariable String name,
			@RequestParam String subscriberid,@RequestParam Integer age){
		act=(act==null || act.length()<=0)?"FIND":act.toUpperCase().trim();
		if("FIND".equalsIgnoreCase(act)){
			return subtestDao.getById(subscriberid);
		}else if("UPD".equalsIgnoreCase(act)){
			SubscriberTest subtest=subtestDao.getById(subscriberid);
			try{
			subtest.setName(name);
			subtest.setAge(age);
			subtestDao.save(subtest);
			}catch(Exception e){
				System.out.println("Exception "+e);
				
			}
			return subtestDao.getById(subscriberid);
		}else{
			//Create
			SubscriberTest subtest=new SubscriberTest(subscriberid,name,age);
			try{
				subtestDao.insert(subtest);
			}catch(Exception e){
				System.out.println("Exception "+e);
			}
			return subtestDao.getById(subscriberid);
		}
	}
	
	
	
	
//	@RequestMapping("/sub/find/{msisdn}")
//	public @ResponseBody List<Subscriber> findBySubscriberId(@PathVariable Long msisdn,@RequestParam String subscriberId){
//		return subMgr.searchSubscriber(subscriberId,msisdn);
//	}
//	
//	@RequestMapping("/sub/update/{subscriberId}")
//	public @ResponseBody Subscriber updateBySubscriberId(@PathVariable String subscriberId,@RequestParam Long subRegId,@RequestParam Long msisdn){
//			Subscriber subObj=null;
//		List<Subscriber> subList=subMgr.searchSubscriber(subscriberId,msisdn);
//		if(subList!=null && subList.get(0)!=null){
//			Subscriber s=subList.get(0);
//			s.setSubregId(subRegId);
//			Boolean status=subMgr.updateSubscriber(s);
//			if(status){
//				subObj=s;
//			}else{
//				subObj=new Subscriber();
//			}
//		}
//		return subObj;
//	}
	/*@RequestMapping("/sub/create/{msisdn}")
	public @ResponseBody Subscriber createSubscriber(@PathVariable Long msisdn,@RequestParam String subscriberId,
			@RequestParam String packageName,String operator,String voucherCode){
		//http://localhost:8101/jocg-services/sub/create/919999631990?subscriberId=rishi@jiffysoftwares.in&packageName=PKGM90&operator=Aircel&voucherCode=V1
		String remoteIp=requestContext.getRemoteAddr();
		Subscriber newSub=subMgr.createSubscriber(packageName,subscriberId,remoteIp,msisdn,operator,voucherCode);
		
		return newSub;
	}*/
	
}
