package com.jss.jocg.services;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.services.dao.AdnetworkRepository;
import com.jss.jocg.services.dao.AdnetworkRepositoryImpl;
import com.jss.jocg.services.data.Adnetwork;



@RestController
@RequestMapping("simulator/adn")
public class AdnSimulator {
	private static final Logger logger = LoggerFactory.getLogger(AdnSimulator.class);
	@Autowired AdnetworkRepository adnRepo;
	@Autowired AdnetworkRepositoryImpl adnRepoImpl;
	@RequestMapping("/{cmp}/{pubid}/")
	public  ModelAndView generateClick(@PathVariable(value="cmp") String campaignId,
			@PathVariable(value="pubid") String publisherId,HttpServletRequest request){
		ModelAndView mv=null;
		campaignId=(campaignId==null)?"NA":campaignId.trim();
		publisherId=(publisherId==null)?"NA":publisherId.trim();
		if("NA".equalsIgnoreCase(campaignId)){
			mv=new ModelAndView();
			mv.setViewName("status");
			mv.addObject("message", "Invalid Campaign Id");
		}else{
			try{
				String redirectURL="/jocg/charge/C/OVOD/?cmp="+campaignId+"&pubid="+publisherId+"&clickid=";
				Adnetwork adn=new Adnetwork();
				String clickId=request.getHeader("msisdn");
				clickId=(clickId==null)?request.getHeader("X-Nokia-msisdn"):clickId;
				clickId +=""+System.currentTimeMillis();
				
			//	adn.setClickId(clickId);
				adn.setCampaignId(campaignId);
				adn.setPubId(publisherId);
				adn.setRedirectUrl(redirectURL);
				adn.setRequestTime(new java.util.Date(System.currentTimeMillis()));
				adn.setStatus(1);
				adn.setCallbackStatus(0);
				adn=adnRepo.insert(adn);
				redirectURL+=adn.getId();
				//logger.debug("ADN : Redirecting to "+redirectURL);
				mv=new ModelAndView("redirect:"+redirectURL);
//				mv=new ModelAndView("redirect:"+redirectURL);
//				mv=new ModelAndView();
//				mv.setViewName("status");
//				mv.addObject("message", "Redirecting to URL : "+redirectURL);
			}catch(Exception e){
				mv=new ModelAndView();
				mv.setViewName("status");
				mv.addObject("message", "Exception "+e);
				
			}
		}
		
		
		return mv;
	}
	
	@RequestMapping("/s2s")
	public  @ResponseBody String receiveS2s(@RequestParam(value="clickid") String clickid,
			HttpServletRequest request){
		String resp="";
		try{
			

			Adnetwork adn=adnRepo.findById(clickid);
			if(adn!=null && adn.getId()!=null && adn.getCallbackStatus()<=0){
				adn.setCallbackStatus(2);
				adn.setCallbackTime(new java.util.Date(System.currentTimeMillis()));
				adnRepo.save(adn);
				resp="Received S2S successfully!";
			}else if(adn!=null && adn.getId()!=null && adn.getCallbackStatus()>0){
				logger.error("Duplicate  "+clickid+" received");
			}else{
				logger.error("clickid "+clickid+" not found");
			}
			resp="S2S Received!";
		}catch(Exception e){
			resp="ERROR";
			e.printStackTrace();
		}
		logger.debug("ADN : S2S Status  "+resp);
		return resp;
	}

	@RequestMapping("/waste")
	public  @ResponseBody String receiveWaste(@RequestParam(value="clickid") String clickid,
			HttpServletRequest request){
		String resp="";
		try{
			Adnetwork adn=adnRepo.findById(clickid);
			if(adn!=null && adn.getId()!=null){
				adn.setWasteStatus(1);;
				adn.setWasteCallbackTime(new java.util.Date(System.currentTimeMillis()));
				adnRepo.save(adn);
				resp="Welcome to Waste URL Page";
			}
			resp="Welcome to Waste URL Page";
		}catch(Exception e){
			resp="ERROR";
			e.printStackTrace();
		}
		logger.debug("ADN : Waste Click Received  "+resp);
		return resp;
	}
	
	@RequestMapping("/churn")
	public  @ResponseBody String receiveChurn(@RequestParam(value="clickid") String clickid,
			HttpServletRequest request){
		String resp="";
		try{
			Adnetwork adn=adnRepo.findById(clickid);
			if(adn!=null && adn.getId()!=null){
				adn.setChurnStatus(1);;
				adn.setChurnCallbackTime(new java.util.Date(System.currentTimeMillis()));
				adnRepo.save(adn);
				resp="SUCCESS";
			}
			resp="SUCCESS";
		}catch(Exception e){
			resp="ERROR";
			e.printStackTrace();
		}
		logger.debug("ADN : S2S Status  "+resp);
		return resp;
	}


}
