package com.jss.jocg.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.cache.entities.config.json.JsonNotifyConfigConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyPercentageConverter;
import com.jss.jocg.cache.entities.config.json.JsonNotifyUrlsConverter;
import com.jss.jocg.cache.entities.config.json.JsonSubcontrolConfigConverter;
import com.jss.jocg.cache.entities.config.json.NotifyConfig;
import com.jss.jocg.cache.entities.config.json.NotifyPercentage;
import com.jss.jocg.cache.entities.config.json.NotifyUrls;
import com.jss.jocg.cache.entities.config.json.SubcontrolConfig;
import com.jss.jocg.services.bo.CDRBuilder;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.ChargingCDRRepositoryImpl;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

@Component
public class S2SNotifierService {

	private static final Logger logger = LoggerFactory
			.getLogger(S2SNotifierService.class);
	
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired JmsTemplate jmsTemplate;
	@Autowired ChargingCDRRepositoryImpl notifyCDRImpl;
	@Autowired CDRBuilder cdrBuilder;
	Map<String,TrafficStats> trafficStats=null;
	// @Scheduled(initialDelay=1, fixedRate=120000)
	   public void schedularThread() {
		 	String logHeader="S2SNotifySchedular :: ";	
		 	 if(jocgCache.isNotifyS2SEnabled()){
				//List<Subscriber> subList= subscriberDao.findFirst100ByStatusAndRenewalCategoryAndRenewalRequiredAndRenewalStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE,"SYSTEM",new Boolean(true),JocgStatusCodes.REN_NOT_INITIATED);
			 	//PageRequest request = new PageRequest(0, 100, new Sort(Sort.Direction.ASC, "notifyTime"));
			 	Calendar cal=Calendar.getInstance();
			 	cal.setTimeInMillis(System.currentTimeMillis());
			 	logger.debug(logHeader+"schedularThread() :: Reloading Traffic Stats.... ");
			 	jocgCache.reloadStats();
			 	trafficStats=jocgCache.getTrafficStats();
			 
			 	//logger.debug(logHeader+" ::schedularThread() :: Searching queued Notification on NotificationTime>="+cal.getTime()+", Status "+JocgStatusCodes.CHARGING_SUCCESS+", NotifyStatus "+JocgStatusCodes.NOTIFICATION_QUEUED+", page "+request);
			 	List<ChargingCDR> subList= notifyCDRImpl.findQueuedNotifications(logHeader, JocgStatusCodes.CHARGING_SUCCESS, JocgStatusCodes.NOTIFICATION_QUEUED, 1000);//chargingCDRDao.findByRequestDateAndStatus(cal.getTime(),JocgStatusCodes.CHARGING_SUCCESS,JocgStatusCodes.NOTIFICATION_QUEUED,request);
				if(subList!=null){
				 	logger.debug(logHeader+"schedularThread() :: Picked CDR count "+subList.size());
				 	handleActNotification(subList);
				 	subList=null;
				}
		 	 }
		 	 if(jocgCache.isNotifyS2SChurnEnabled()){	
				//Run Notified for Churn
		 		List<ChargingCDR> subList= notifyCDRImpl.findQueuedChurnNotifications(logHeader, JocgStatusCodes.CHARGING_SUCCESS, JocgStatusCodes.NOTIFICATION_QUEUED, 100);//chargingCDRDao.findByRequestDateAndStatus(cal.getTime(),JocgStatusCodes.CHARGING_SUCCESS,JocgStatusCodes.NOTIFICATION_QUEUED,request);
				if(subList!=null){
					logger.debug(logHeader+"churnSchedularThread() :: Picked NUmber count "+subList.size());
					for(ChargingCDR cdr:subList){
						jmsTemplate.convertAndSend("notify.adnetworkchurn", cdr);
						logger.debug(logHeader+" Churn Notification for CDR Id "+cdr.getId()+", msisdn "+cdr.getMsisdn()+" sent to S2S Churn JMS Queue");
						WAIT(10);
					}
					subList=null;
				}
		 	 }
	
				
		 
	 }
	 
	 public void  handleActNotification(List<ChargingCDR> cdrList){
		 String logHeader="S2SNotifySchedular :: handleActNotification()::";	
		 try{
			 for(ChargingCDR cdr:cdrList){
				 NotificationConfig appliedConfig=getAppliedNotificationConfig(cdr);
				 logger.debug(logHeader+cdr.getMsisdn()+" :: Applied Notification Config for "+appliedConfig);
				 sendNotification(appliedConfig,cdr);
				
			 }
		 }catch(Exception e){
			 logger.error(logHeader+" :: Exception "+e);
		 }
	 }
	
	 public void sendNotification(NotificationConfig appliedConfig,ChargingCDR notificationCDR){
		 String logHeader="S2SNotifySchedular :: sendNotification()::"+notificationCDR.getMsisdn()+"::";	
		 try{
			 if(appliedConfig==null){
					notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
					notificationCDR.setNotifyDescp("S2SNotification: Notification Config not defined for this case.");
					notificationCDR.setNotifyTime(new java.util.Date());
					logger.error(logHeader+"Notification configuration not found for cdr id "+notificationCDR.getId());
			}else if(notificationCDR.getStatus()!=JocgStatusCodes.CHARGING_SUCCESS){
				notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
				notificationCDR.setNotifyDescp("S2SNotification: Notification Ignored for failed transaction.");
				notificationCDR.setNotifyTime(new java.util.Date());
				logger.error(logHeader+"Notification Ignored for failed transaction cdr id "+notificationCDR.getId());
			}else if(notificationCDR.getNotifyStatus()!=JocgStatusCodes.NOTIFICATION_QUEUED){
				notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
				notificationCDR.setNotifyDescp(" Notification Ignored for Invalid Notify Status "+notificationCDR.getNotifyStatus());
				notificationCDR.setNotifyTime(new java.util.Date());
				logger.error(logHeader+" Notification Ignored for Invalid Notify Status "+notificationCDR.getNotifyStatus());
			}else{
				
					 NotifyConfig notifyConfig=null;NotifyPercentage notifyPercentage=null;
					 NotifyUrls notifyUrls=null;SubcontrolConfig sc=null;TrafficStats ts=null;int percentageSent=0;
					 JsonNotifyConfigConverter jncc=new JsonNotifyConfigConverter();
					 notifyConfig=jncc.convertToEntityAttribute(appliedConfig.getNotifyConfig());
					
					 JsonNotifyPercentageConverter jnpc=new JsonNotifyPercentageConverter();
					 notifyPercentage=jnpc.convertToEntityAttribute(appliedConfig.getNotifyPercentage());
					
					 JsonNotifyUrlsConverter jnuc=new JsonNotifyUrlsConverter();
					 notifyUrls=jnuc.convertToEntityAttribute(appliedConfig.getNotifyUrls());
					
					 JsonSubcontrolConfigConverter jscc=new JsonSubcontrolConfigConverter();
					 sc=jscc.convertToEntityAttribute(appliedConfig.getSubcontrolConfig());
					 ts=getTrafficStats(notificationCDR.getChargingInterfaceId(), 
								notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), 
								notificationCDR.getPublisherId());
					
					logger.debug(logHeader+"Configurations Extracted successfully. TrafficStats "+ts);
					
					if(ts!=null && ts.getSuccessCount()>0){
						
						//Deduct Minimum same day activation from sent Count
						int sentToCompare=ts.getSentCount()-sc.getMinsda();
						sentToCompare=(sentToCompare<=0)?0:sentToCompare;
						int successCountToCompare=ts.getSuccessCount()-sc.getMinsda();
						
						if(successCountToCompare>0)
							percentageSent=sentToCompare*100/successCountToCompare;
						else
							percentageSent=0;
						logger.debug(logHeader+"Notify Percentage Check sent:"+ts.getSentCount()+
								",successCount:"+ts.getSuccessCount()+", sentToCompare :"+sentToCompare+
								",successCountToCompare:"+successCountToCompare+",percentageSent="+percentageSent);
						
						String skipPPType=notifyConfig.getSkippp().getType();
						Double skipPPValue=JocgString.strToDouble(notifyConfig.getSkippp().getValue(),0);
						
						//Validate for percentage Check
						if(percentageSent>=notifyPercentage.getCharging()){
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
							notificationCDR.setNotifyDescp("Bolcked-Percentage Check , Percentage found "+percentageSent);
							notificationCDR.setNotifyTime(new java.util.Date());
							logger.debug(logHeader+" S2SNotification Blocked due to Percentage check. percentageSent:"+percentageSent+",notifyPercentage.getCharging() "+notifyPercentage.getCharging());
						}else if(notifyConfig.getChurn20min()==false && notificationCDR.getChurn20Min()==true){ 
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
							notificationCDR.setNotifyDescp("Bolcked-20MinChurn");
							notificationCDR.setNotifyTime(new java.util.Date());
							logger.debug(logHeader+" Bolcked-20MinChurn");
						}else if((skipPPType.equalsIgnoreCase("BELOW") && notificationCDR.getChargedAmount()<=skipPPValue) ||
								(skipPPType.equalsIgnoreCase("ABOVE") && notificationCDR.getChargedAmount()<=skipPPValue) ||
								(skipPPType.equalsIgnoreCase("EQUALTO") && notificationCDR.getChargedAmount()==skipPPValue)) {
							notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_NOTSENT);
							notificationCDR.setNotifyDescp("Bolcked-SKIP Price Point Validation");
							notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
							logger.debug(logHeader+" Bolcked-SKIP Price Point Validation ");
						}else{
							//Send Notification Now
							String url=notifyUrls.getCharging();
							logger.debug(logHeader+" ACT Notification URL Template "+url);
							if(url!=null && url.startsWith("http://")){
								url=url+"?"+notificationCDR.getTrafficSourceToken();
								logger.debug(logHeader+" Final Notification URL "+url);
								
								JcmsSSLClient sslClient=new JcmsSSLClient();
								JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(logHeader, url);
								
								logger.debug(logHeader+" Notification URL Response "+resp.getResponseData()+", response time "+resp.getResponseTime()+"ms");
								logger.debug(logHeader+"Detailed Notification process Logs "+resp.getProcessLogs());
								if(resp.getResponseCode()==200){
									notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_SENT);
									notificationCDR.setNotifyDescp("S2S Pushed Successfully");
									logger.debug(logHeader+"Notification sent Successfully");
								}else{
									notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
									notificationCDR.setNotifyDescp("S2S Push Failed ");
									logger.debug(logHeader+"Notification failed as remote url not working.");
								}
								
								notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
								ts.setSentCount(ts.getSentCount()+1);
								
							}else{
								
								notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
								notificationCDR.setNotifyDescp("Invalid Notification URL");
								notificationCDR.setNotifyTime(new java.util.Date(System.currentTimeMillis()));
								logger.debug(logHeader+"Notification Ignored! Invalid Notification URL Template '"+url+"'");
							}
							
							
						}
						trafficStats.put(ts.getConfigKey(), ts);
						logger.debug(logHeader+"Updated Traffic Stats  "+ts);
						
					}else{
						logger.debug(logHeader+" Notify Percentage Check Failed, traffic stats is null or success_count<=0");
					}
					
					
			}
			
		 }catch(Exception e){
			 notificationCDR.setNotifyStatus(JocgStatusCodes.NOTIFICATION_FAILED);
			notificationCDR.setNotifyDescp("S2SNotification: Exception "+e.getMessage());
			notificationCDR.setNotifyTime(new java.util.Date());
			logger.error(logHeader+"Exception while processing for cdr id "+notificationCDR.getId()+", Exception "+e.getMessage());
		 }finally{
			 cdrBuilder.save(notificationCDR);
		 }
		 
		 
	 }
	 
	
	 public NotificationConfig getAppliedNotificationConfig(ChargingCDR notificationCDR){
		 String logHeader="S2SNotifySchedular :: getAppliedNotificationConfig()::"+notificationCDR.getMsisdn()+"::";	
			NotificationConfig appliedConfig=null;
			try{
			List<NotificationConfig> nc=jocgCache.getNotificationConfig(notificationCDR.getChargingInterfaceId(),notificationCDR.getTrafficSourceId(), notificationCDR.getCampaignId(), notificationCDR.getPublisherId());
			logger.debug(logHeader+" Searching Notification Config...");
			
			//Search Exact Match including publisher id
			for(NotificationConfig c:nc){
				appliedConfig=(c.getPubid().equalsIgnoreCase(notificationCDR.getPublisherId()) && 
						c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
						c.getTsourceid()==notificationCDR.getTrafficSourceId() && 
						c.getCampaignid()==notificationCDR.getCampaignId())?c:null;
				
				if(appliedConfig!=null) break;
			}
			//Search Exact Match excluding publisher id
			for(NotificationConfig c:nc){
				appliedConfig=(appliedConfig==null &&
						c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
						c.getTsourceid()==notificationCDR.getTrafficSourceId() && 
						c.getCampaignid()==notificationCDR.getCampaignId())?c:appliedConfig;
				if(appliedConfig!=null) break;
			}
			
			//Search  Match pfr chintf and tsource only
			for(NotificationConfig c:nc){
				appliedConfig=(appliedConfig==null &&
						c.getChintfid()==notificationCDR.getChargingInterfaceId() && 
						c.getTsourceid()==notificationCDR.getTrafficSourceId())?c:appliedConfig;
				if(appliedConfig!=null) break;
			}
			}catch(Exception e){
				logger.error(logHeader+" Exception "+e);
				
			}
			return appliedConfig;
		}

	 public TrafficStats getTrafficStats(Integer chintfId,Integer tsourceId,Integer campaignId,String publisherId){
			//logger.info("Cached URL Params "+cachedObjects.getChargingUrlParams());
			TrafficStats ts=null;
			publisherId=(publisherId==null)?"NA":publisherId.trim();
			String key="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_"+publisherId;
			String key1="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue()+"_NA";
			String key2="STATS_"+chintfId.intValue()+"_"+tsourceId.intValue()+"_"+campaignId.intValue();
			if(trafficStats.containsKey(key)){
				ts=trafficStats.get(key);
				ts.setConfigKey(key);
				return ts;
			}else if(trafficStats.containsKey(key1)){
				ts=trafficStats.get(key1);
				ts.setConfigKey(key1);
				return ts;
			}else if(trafficStats.containsKey(key2)){
				ts=trafficStats.get(key2);
				ts.setConfigKey(key2);
				return ts;
			}else
				return null;
		}
		
		
	 
	 
	
//	 @Scheduled(initialDelay=1, fixedRate=30000)
//	   public void schedularThread() {
//		 	String logHeader="S2SNotifySchedular :: ";	
//		 	 if(jocgCache.isNotifyS2SEnabled()){
//				//List<Subscriber> subList= subscriberDao.findFirst100ByStatusAndRenewalCategoryAndRenewalRequiredAndRenewalStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE,"SYSTEM",new Boolean(true),JocgStatusCodes.REN_NOT_INITIATED);
//			 	//PageRequest request = new PageRequest(0, 100, new Sort(Sort.Direction.ASC, "notifyTime"));
//			 	Calendar cal=Calendar.getInstance();
//			 	cal.setTimeInMillis(System.currentTimeMillis());
//			 	jocgCache.reloadStats();
//			 	
//			 	//logger.debug(logHeader+" ::schedularThread() :: Searching queued Notification on NotificationTime>="+cal.getTime()+", Status "+JocgStatusCodes.CHARGING_SUCCESS+", NotifyStatus "+JocgStatusCodes.NOTIFICATION_QUEUED+", page "+request);
//			 	List<ChargingCDR> subList= notifyCDRImpl.findQueuedNotifications(logHeader, JocgStatusCodes.CHARGING_SUCCESS, JocgStatusCodes.NOTIFICATION_QUEUED, 100);//chargingCDRDao.findByRequestDateAndStatus(cal.getTime(),JocgStatusCodes.CHARGING_SUCCESS,JocgStatusCodes.NOTIFICATION_QUEUED,request);
//				if(subList!=null){
//			 	logger.debug(logHeader+"schedularThread() :: Picked NUmber count "+subList.size());
//				
//				for(ChargingCDR cdr:subList){
//					cdr.setNotifyStatus(5);
//					chargingCDRDao.save(cdr);
//					jmsTemplate.convertAndSend("notify.adnetwork", cdr);
//					logger.debug(logHeader+" CDR Id "+cdr.getId()+", msisdn "+cdr.getMsisdn()+" sent to S2S JMS Queue");
//					
//					WAIT(10);
//				}
//				subList=null;
//				}
//		 	 }
//		 	 if(jocgCache.isNotifyS2SChurnEnabled()){	
//				//Run Notified for Churn
//		 		List<ChargingCDR> subList= notifyCDRImpl.findQueuedChurnNotifications(logHeader, JocgStatusCodes.CHARGING_SUCCESS, JocgStatusCodes.NOTIFICATION_QUEUED, 100);//chargingCDRDao.findByRequestDateAndStatus(cal.getTime(),JocgStatusCodes.CHARGING_SUCCESS,JocgStatusCodes.NOTIFICATION_QUEUED,request);
//				if(subList!=null){
//					logger.debug(logHeader+"churnSchedularThread() :: Picked NUmber count "+subList.size());
//					for(ChargingCDR cdr:subList){
//						jmsTemplate.convertAndSend("notify.adnetworkchurn", cdr);
//						logger.debug(logHeader+" Churn Notification for CDR Id "+cdr.getId()+", msisdn "+cdr.getMsisdn()+" sent to S2S Churn JMS Queue");
//						WAIT(10);
//					}
//					subList=null;
//				}
//		 	 }
//	
//				
//		 
//	 }
	 
	 public synchronized void WAIT(int ms){
		 try{
			 this.wait(ms);
		 }catch(Exception e){}
	 }

}
