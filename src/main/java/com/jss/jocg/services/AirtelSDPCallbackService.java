package com.jss.jocg.services;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jss.jocg.services.bo.airtel.ActivationCallback;
import com.jss.jocg.services.bo.airtel.AirtelApiResponseWraper;
//http://119.252.215.58:8082/tbms/services/NotificationToCPService?wsdl
@RestController
@RequestMapping("tbms/services")
public class AirtelSDPCallbackService {
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelSDPCallbackService.class);
	
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy|MM|dd|HH|mm|ss|");
	SimpleDateFormat sdffile=new SimpleDateFormat("yyyyMMdd");
	@Autowired JmsTemplate jmsTemplate;
	
	/**
	 * Method to simulate customer query , this method will fetch data from tbms server and write back the response
	 * */
	/*@RequestMapping("/SDPLWebService")
	public void checkStausFromTBMS(HttpServletRequest request,HttpServletResponse response){
		String outputString="",responseString="";
		String logHeader="STATUS::processStatusToSuntec() :: ";
		 String wsURL="";String queryData="";
		try{
			
			int modeVal=(int)(System.currentTimeMillis()%4);
			wsURL =(modeVal<=0)?"http://172.31.19.172:8082/tbms/services/SDPLWebService?wsdl"
					:(modeVal<=1)?"http://172.31.19.172:8083/tbms/services/SDPLWebService?wsdl"
					:(modeVal<=2)?"http://172.31.19.175:8082/tbms/services/SDPLWebService?wsdl"
					:"http://172.31.19.175:8083/tbms/services/SDPLWebService?wsdl";
			//2012-01-18T14:48:00Z
			queryData="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.sdpl.tbms.suntecgroup.com\">"+
					" <soapenv:Header/>"+
					" <soapenv:Body> "+
					" <ser:customerEnquiry>"+
					" <!--Optional:--> "+
					" <ser:msisdn>9999901505</ser:msisdn>"+
					" <!--Optional:--> "+
					" <ser:subscriberId></ser:subscriberId> "+
					" <!--Optional:-->  "+
					" <ser:serviceId>MOBTV</ser:serviceId> "+
					" <!--Optional:--> "+
					" <ser:operatorID>CM_OF</ser:operatorID> "+
					" <!--Optional:--> "+
					" <ser:systemId>1Y</ser:systemId> "+
					" <!--Optional:--> "+
					" <ser:password>W@P123</ser:password> "+
					" <!--Optional:--> "+
					" <ser:date>2012-01-18T14:48:00Z</ser:date> "+
					" <!--Optional:--> "+
					" <ser:UserField1></ser:UserField1> "+
					" <!--Optional:--> "+
					" <ser:UserField2></ser:UserField2> "+
					" <!--Optional:--> "+
					" <ser:UserField3></ser:UserField3> "+
					" <!--Optional:--> "+
					" <ser:UserField4></ser:UserField4> "+
					" <!--Optional:--> "+
					" <ser:UserField5></ser:UserField5> "+
					" <!--Optional:--> "+
					" <ser:UserField6>0</ser:UserField6> "+
					" <!--Optional:--> "+
					" <ser:UserField7>0</ser:UserField7> "+
					" <!--Optional:--> "+
					" <ser:UserField8>0</ser:UserField8> "+
					" <!--Optional:--> "+
					" <ser:UserField9>0</ser:UserField9> "+
					" <!--Optional:--> "+
					" <ser:UserField10>0</ser:UserField10> "+
					" </ser:customerEnquiry> "+
					" </soapenv:Body> "+
					"</soapenv:Envelope> ";

			
			System.out.println("Query Data "+queryData);
			URL url = new URL(wsURL);
			URLConnection connection = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection)connection;
			
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte[] buffer = new byte[queryData.length()];
			buffer = queryData.getBytes();
			bout.write(buffer);
			byte[] b = bout.toByteArray();
			System.out.println("Data Length "+b.length);
			// Set the appropriate HTTP parameters.
			httpConn.setRequestProperty("Content-Length",
			String.valueOf(b.length));
			httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
			httpConn.setRequestProperty("Cache-Control", "no-cache");
			httpConn.setRequestProperty("Pragma", "no-cache");
			httpConn.setRequestProperty("SOAPAction", "customerEnquiryGen");
			httpConn.setRequestMethod("POST");
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);
			//httpConn.connect();
			//int responseCode=httpConn.getResponseCode();
			//System.out.println("Response Code "+responseCode);
			OutputStream out = httpConn.getOutputStream();
			//Write the content of the request to the outputstream of the HTTP Connection.
			System.out.println("Out Stream "+out);
			out.write(b);
			out.flush();
			out.close();
			System.out.println("Out Stream Data flushed with  "+b.length+" bytes");
			BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
			System.out.println("In Stream "+in);
			System.out.println("Reading Input");
			//Write the SOAP message response to a String.
			while ((responseString = in.readLine()) != null) {
				System.out.println(">>received data "+responseString);
				outputString = outputString + responseString;
			}
			
			in.close();
			in=null;
			httpConn.disconnect();
			httpConn=null;
			url=null;
			
			
		}catch(Exception e){
			System.out.println("Exception "+e);
			e.printStackTrace();
		}
		
		try{
			PrintWriter pr=response.getWriter();
			pr.println(outputString);
			pr.flush();
			pr.close();
		}catch(Exception e){
			
		}
		
	}*/
	
	@RequestMapping("/SDPLWebService")
	public void checkStatusFromTBMS(HttpServletRequest request,HttpServletResponse response){
		String outputString="";
		String logHeader="STATUS::processStatusToSuntec() :: ";
		 StringBuilder sb = new StringBuilder();
		 String wsURL="";String queryData="";
		 StringBuilder sbb=new StringBuilder();
		    try {
		        java.io.BufferedReader bis = new java.io.BufferedReader(new java.io.InputStreamReader(request.getInputStream()));
		        String line = "";
		        while ((line = bis.readLine()) != null) {
		            sb.append(line);
		        }
		        bis.close();
		       bis=null;
		       queryData=sb.toString();
		       queryData=queryData.substring(queryData.indexOf("<soapenv:Body>"));
		       queryData="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.sdpl.tbms.suntecgroup.com\">"+queryData;
		       sbb.append("Received Data "+queryData);
		       
		       String responseString = "";
			
				int modeVal=(int)(System.currentTimeMillis()%4);
				wsURL =(modeVal<=0)?"http://172.31.19.172:8082/tbms/services/SDPLWebService?wsdl"
						:(modeVal<=1)?"http://172.31.19.172:8083/tbms/services/SDPLWebService?wsdl"
						:(modeVal<=2)?"http://172.31.19.175:8082/tbms/services/SDPLWebService?wsdl"
						:"http://172.31.19.175:8083/tbms/services/SDPLWebService?wsdl";
				 sbb.append("\nDest URL "+wsURL);
				 
				URL url = new URL(wsURL);
				URLConnection connection = url.openConnection();
				HttpURLConnection httpConn = (HttpURLConnection)connection;
				ByteArrayOutputStream bout = new ByteArrayOutputStream();
				byte[] buffer = new byte[queryData.length()];
				buffer = queryData.getBytes();
				bout.write(buffer);
				byte[] b = bout.toByteArray();
			
				// Set the appropriate HTTP parameters.
				httpConn.setRequestProperty("Content-Length",
				String.valueOf(b.length));
				httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
				//httpConn.setRequestProperty("Cache-Control", "no-cache");
				//httpConn.setRequestProperty("Pragma", "no-cache");
				//httpConn.setRequestProperty("SOAPAction", "customerEnquiryGen");
				httpConn.setRequestMethod("POST");
				httpConn.setDoOutput(true);
				httpConn.setDoInput(true);
				sbb.append("\nURL Connected ");
				OutputStream out = httpConn.getOutputStream();
				//Write the content of the request to the outputstream of the HTTP Connection.
				out.write(b);
				out.close();
				//Ready with sending the request.
				sbb.append("\nData Written...");
				//Read the response.
				BufferedReader in = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
				 
				//Write the SOAP message response to a String.
				while ((responseString = in.readLine()) != null) {
					outputString = outputString + responseString;
				}
				sbb.append("\nReceived Response "+outputString);
				in.close();
				in=null;
				httpConn.disconnect();
				httpConn=null;
				url=null;
				logger.debug("SUNTEC STATUS Query RESP "+outputString);
		       
		    } catch (Exception e) {
		    	logger.debug("SUNTEC STATUS Exception : " + e);
		    }finally{
		    	try{
		    		 SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
				        String fileName="/home/jocg/logs/querydata_"+sdf.format(new java.util.Date(System.currentTimeMillis()))+".txt";
				        FileWriter fw=new FileWriter(fileName,true);
				        fw.write("\n"+new java.sql.Timestamp(System.currentTimeMillis())+"-"+sbb.toString());
				        fw.flush();
				        fw.close();
				      fw=null;
		    	}catch(Exception ee){}
		    }
		    try{
				response.setContentType("text/xml; charset=utf-8");
				PrintWriter out=response.getWriter();
				out.println(outputString);
				out.flush();
				out.close();
		    }catch(Exception e){
		    	
		    }
		//return outputString;
	}
	
	@RequestMapping("/airtelsdpoffline")
	public @ResponseBody String processOfflineCallback(
			@RequestParam(value="cpid" ,required=false,defaultValue="NA") String cpId,
			@RequestParam(value="amount" ,required=false,defaultValue="NA") String amount,
			@RequestParam(value="chargingtime" ,required=false,defaultValue="NA") String chargingTime,
			@RequestParam(value="msisdn" ,required=false,defaultValue="NA") String msisdn,
			@RequestParam(value="productid" ,required=false,defaultValue="NA") String productId,
			@RequestParam(value="setransid" ,required=false,defaultValue="NA") String temp2,
			@RequestParam(value="refid" ,required=false,defaultValue="NA") String id,
			@RequestParam(value="chargingType" ,required=false,defaultValue="NA") String chargingType,
			@RequestParam(value="channel" ,required=false,defaultValue="WAP") String channelName,
			@RequestParam(value="xactionid" ,required=false,defaultValue="NA") String xactionId,
			@RequestParam(value="errorcode" ,required=false,defaultValue="1000") String errorCode,
			@RequestParam(value="errormsg" ,required=false,defaultValue="SUCCESS") String errorMsg,
			@RequestParam(value="cmpname" ,required=false,defaultValue="NA") String adNetworkName,
			@RequestParam(value="systemid" ,required=false,defaultValue="6WHMM") String dvSystemId
			){
		/**
		  private String cpId;
	    private String productId;
	    private String msisdn;
	    private String seTransactionId;
	    private String seTransactionTime;
	    private String xActionId;
	    private String lifecycleId;
		 */
				ActivationCallback callbackData=new ActivationCallback();
				try {
					if(msisdn.length()<=10) msisdn="91"+msisdn;
					
					callbackData.setAirtelCallbackId("NA");
					callbackData.setAmount(amount);
					SimpleDateFormat sdf =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
					java.sql.Timestamp ts=null;
					try{
					long l=sdf.parse(chargingTime).getTime();
					ts= new java.sql.Timestamp(l);
					}catch(Exception e){
						ts=new Timestamp(System.currentTimeMillis());
						e.printStackTrace();
					}
					callbackData.setChargigTime(ts);
					callbackData.setErrorCode(errorCode);
					callbackData.setErrorMsg("AOC : "+errorMsg);
					callbackData.setLowBalance("");
					callbackData.setMsisdn(msisdn);
					callbackData.setProductId(productId);
					callbackData.setTemp1("NA");
					callbackData.setTemp2(temp2);
					callbackData.setId(id);
					callbackData.setChargingType(chargingType);
					callbackData.setvCode("NA");
					callbackData.setPartyB(msisdn);
					callbackData.setChannelName(channelName);
					callbackData.setXactionId(xactionId);
					callbackData.setCallbackResp("NA");
					callbackData.setProcess(0);
					callbackData.setReferer("DV-CALLBACK");
					callbackData.setAdnetworkName(adNetworkName);
					callbackData.setSystemId(dvSystemId);
					logger.debug("Airtel-OFFLINE-CB ::  Parsed SDP Response"+ callbackData);
            
            //callback.airtelsdp
            jmsTemplate.convertAndSend("callback.airtelsdp", callbackData);
           
        } catch (Exception e) {
           
           // sb.append(logHeader + ":: Exception : " + e);
        } finally {
           // createDBLog(logHeader, wapReq, activationCallback);
        }
        try{
	        File f=new File("/home/jocg/data/callback/airtelin");
			if(!f.exists()) f.mkdirs();
			f=null;
			String fileName="AIRTEL_offlineCB";
			FileWriter fw=new FileWriter("/home/jocg/data/callback/airtelin/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
			fw.write("\nTime "+new java.util.Date()+"|"+callbackData.toString());
			fw.flush();
			fw.close();
			fw=null;
	        
	    } catch (Exception e1) {
	        
	    }
		
		return "success";
	
	}
	
	
	@RequestMapping("/NotificationToCPService")
	public @ResponseBody String processAirtelSDP(HttpServletRequest requestContext){
		String logHeader="CALLBACK::processAirtelSDP() :: ";
		 StringBuilder sb = new StringBuilder();
		    try {
		        java.io.BufferedReader bis = new java.io.BufferedReader(new java.io.InputStreamReader(requestContext.getInputStream()));
		        String line = "";
		        while ((line = bis.readLine()) != null) {
		            sb.append(line);
		        }
		        bis.close();
		        ActivationCallback activationCallback = null;
		        try {

		            AirtelApiResponseWraper airtelApiResponseWraper = new AirtelApiResponseWraper();
		            activationCallback = airtelApiResponseWraper.parseChargingActivationCallBack(sb.toString());
		            logger.debug(logHeader+" Parsed SDP Date "+ activationCallback);
		            
		            String msisdn=activationCallback.getMsisdn();
		            if(msisdn.length()<=10) msisdn="91"+msisdn;
		            activationCallback.setMsisdn(msisdn);
		            activationCallback.setReferer("SDP-CALLBACK");
		            //callback.airtelsdp
		            jmsTemplate.convertAndSend("callback.airtelsdp", activationCallback);
		            
		          //  jmsTemplate.convertAndSend("callback.tbmssdp", sb.toString());
		           
		        } catch (Exception e) {
		           
		           // sb.append(logHeader + ":: Exception : " + e);
		        } finally {
		           // createDBLog(logHeader, wapReq, activationCallback);
		        }
		        
		        File f=new File("/home/jocg/data/callback/airtelin");
				if(!f.exists()) f.mkdirs();
				f=null;
				String fileName="AIRTEL_CB";
				FileWriter fw=new FileWriter("/home/jocg/data/callback/airtelin/"+fileName+sdffile.format(new java.util.Date(System.currentTimeMillis()))+".cb",true);
				fw.write("\nTime "+new java.util.Date()+"|"+sb.toString());
				fw.flush();
				fw.close();
				fw=null;
		        
		    } catch (Exception e) {
		        sb.append("Airtel Exception : " + e);
		    }
		    
		  
		    
		    
		    String res = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""
		            + " xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
		            + "<soapenv:Body>"
		            + "<notificationToCPResponse xmlns=\"http://SubscriptionEngine.ibm.com\"/> "
		            + "</soapenv:Body>"
		            + "</soapenv:Envelope>";
	
		
		return res;
	}


}
