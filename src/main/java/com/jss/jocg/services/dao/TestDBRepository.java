package com.jss.jocg.services.dao;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.jss.jocg.services.data.TestDB;

public interface TestDBRepository  extends MongoRepository<TestDB, String>{

}
