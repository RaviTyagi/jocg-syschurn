package com.jss.jocg.services.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*; 
import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cache.entities.config.NotificationConfig;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;

/**
 * @author Nidhi
 *
 */
public class ChargingCDRRepositoryImpl implements ChargingCDRRepositoryCustom{
	private static final Logger logger = LoggerFactory
			.getLogger(ChargingCDRRepositoryImpl.class);
	 @Autowired
	    private MongoTemplate mongoTemplate;
	 @Autowired
	 	private JocgCacheManager jocgCache;
	 
	 public List<ChargingCDR> findQueuedReportCDR(String logHeader,int pageSize,java.util.Date lastUpdate,boolean fetchFromBegining){
		 logHeader +="findQueuedReportCDR() :: ";
		 Query query = new Query();
		 try{
			 logger.debug(logHeader+"REPORTING :: Running report query .....");
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			 String lastPickedDate=sdf.format(lastUpdate);
			 lastPickedDate =  lastPickedDate + "+05:30" ;
//			 if(fetchFromBegining){
//				 	query.addCriteria(Criteria.where("reportGenerated").is(new Boolean(false)));
//					query.limit(pageSize);
//			 }else{
//				 logger.debug("Last Picked Date "+lastPickedDate);
//				 query.addCriteria(Criteria.where("lastUpdate").gte(sdf.parse(lastPickedDate)).
//							and("reportGenerated").is(new Boolean(false)));
//				 query.with(new Sort(Sort.Direction.ASC, "lastUpdate"));
//				 query.limit(pageSize);
//			 }
			 query.addCriteria(Criteria.where("reportGenerated").is(new Boolean(false)));
			 query.with(new Sort(Sort.Direction.ASC, "lastUpdate"));
			 query.limit(pageSize);
			 
		 }catch(Exception e){
			 
		 }
		 List<ChargingCDR> cdrList=null;
		 try{
			 logger.debug(logHeader+" Report Fetch Query "+query);
			 cdrList=mongoTemplate.find(query, ChargingCDR.class);
			 logger.debug(logHeader+" Report Record Count  "+cdrList.size());
			 if(cdrList!=null && cdrList.size()>0){
				 for(ChargingCDR cdr:cdrList){
					 cdr.setReportGenerated(true);
					 mongoTemplate.save(cdr);
				 }
			 }
		 }catch(Exception e){}
		if(cdrList==null) cdrList=new ArrayList<>();
		 return cdrList;
		 
	 }
 
	 public List<ChargingCDR> findQueuedNotifications(String logHeader, Integer status,
			 Integer notifyStatus,
			 int limit){
		 logHeader +="findQueuedNotifications() :: ";
		 Query query = new Query();
		 try{
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			// SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			 String fcdate=sdf.format(cal.getTime());
			 cal.set(Calendar.HOUR_OF_DAY, 0);
			 cal.set(Calendar.MINUTE, 0);
			 cal.set(Calendar.SECOND, 0);
			 String fddaystart=sdf.format(cal.getTime());
			 java.util.Date d1=sdf.parse(fcdate);
			 java.util.Date d2=sdf.parse(fddaystart);
			System.out.println(d1);
			System.out.println(d2);
			logger.debug(logHeader+" Searching queued Notification on requestDate>="+fddaystart+" and  NotificationTime<="+fcdate+", Status "+status+", NotifyStatus "+notifyStatus+", limit "+limit);
			 
			query.addCriteria(Criteria.where("requestDate").gte(sdf.parse(fddaystart)).
						and("status").is(JocgStatusCodes.CHARGING_SUCCESS).
						and("notifyStatus").is(JocgStatusCodes.NOTIFICATION_QUEUED).
						and("notifyScheduleTime").lte(sdf.parse(fcdate)));
			 query.limit(limit);
		
		 }catch(Exception e){
			 logger.error(logHeader+" Exception "+e);
		 }
		 return mongoTemplate.find(query, ChargingCDR.class);
		
		 
	 }
	 
	 public List<ChargingCDR> findQueuedChurnNotifications(String logHeader, Integer status,
			 Integer notifyStatus,
			 int limit){
		 logHeader +="findQueuedNotifications() :: ";
		 Query query = new Query();
		 try{
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			 //SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			 String fcdate=sdf.format(cal.getTime());
			 cal.set(Calendar.HOUR_OF_DAY, 0);
			 cal.set(Calendar.MINUTE, 0);
			 cal.set(Calendar.SECOND, 0);
			 String fddaystart=sdf.format(cal.getTime());
			 java.util.Date d1=sdf.parse(fcdate);
			 java.util.Date d2=sdf.parse(fddaystart);
			System.out.println(d1);
			System.out.println(d2);
			logger.debug(logHeader+" Start Date "+d1+", End Date "+d2);
			logger.debug(logHeader+" Searching queued Churn Notification on NotificationTime<="+fcdate+", Status "+status+", NotifyStatus "+notifyStatus+", limit "+limit);
			 query.addCriteria(Criteria.where("requestDate").gte(sdf.parse(fddaystart)).
						and("status").is(JocgStatusCodes.CHARGING_SUCCESS).
						and("notifyStatus").is(JocgStatusCodes.NOTIFICATION_SENT).
						and("notifyChurnStatus").is(JocgStatusCodes.NOTIFICATION_QUEUED).
						and("churnSameDay").is(new Boolean(true)).
						and("notifyChurn").is(new Boolean(true)));
			 			//and("churn20Min").is(new Boolean(false)).
			 query.limit(limit);
		
		 }catch(Exception e){
			 logger.error(logHeader+" Exception "+e);
		 }
		 return mongoTemplate.find(query, ChargingCDR.class);
		
		 
	 }
	
	 public List<ChargingCDR> findQueuedChurnNotificationsNew(String logHeader, Integer status,
			 Integer notifyStatus,
			 int limit){
		 logHeader +="findQueuedNotifications() :: ";
		 Query query = new Query();
		 try{
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			 //SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			 java.util.Date notifyScheduleTimeEnd=cal.getTime();
			 java.util.Date requestDateStart=cal.getTime(); 
			 String fcdate=sdf.format(cal.getTime());
			 cal.set(Calendar.HOUR_OF_DAY, 0);
			 cal.set(Calendar.MINUTE, 0);
			 cal.set(Calendar.SECOND, 0);
			 String fddaystart=sdf.format(cal.getTime());
			 java.util.Date d1=sdf.parse(fcdate);
			 java.util.Date d2=sdf.parse(fddaystart);
			System.out.println(d1);
			System.out.println(d2);
			logger.debug(logHeader+" Start Date "+d1+", End Date "+d2);
			logger.debug(logHeader+" Searching queued Churn Notification on NotificationTime<="+fcdate+", Status "+status+", NotifyStatus "+notifyStatus+", limit "+limit);
			 query.addCriteria(Criteria.where("requestDate").gte(sdf.parse(fddaystart)).
						and("status").is(JocgStatusCodes.CHARGING_SUCCESS).
						and("notifyStatus").is(JocgStatusCodes.NOTIFICATION_SENT).
						and("notifyChurnStatus").is(JocgStatusCodes.NOTIFICATION_QUEUED).
						and("churnSameDay").is(new Boolean(true)).
						and("notifyChurn").is(new Boolean(true)));
			 			//and("churn20Min").is(new Boolean(false)).
			 query.limit(limit);
		
		 }catch(Exception e){
			 logger.error(logHeader+" Exception "+e);
		 }
		 return mongoTemplate.find(query, ChargingCDR.class);
		
		 
	 }
	 
	 public  AggregationResults<TrafficStats> getTrafficStats(List<String> publisherIds){

		// Iterator i=jocgCache.getNotificationConfig().values().iterator();
		 MatchOperation filterStates = match(new Criteria("status").in(JocgStatusCodes.SUB_SUCCESS_ACTIVE));
		 
		 Aggregation aggr=newAggregation(filterStates,
				 group("chargingInterfaceId", "trafficSourceId")
				 .count().as("count")
				 .sum("chargedAmount").as("totalPrice")
				 .avg("chargedAmount").as("avgChargedPrice"));
		 
		
//		 Aggregation aggr=newAggregation(filterStates,
//				 group("chargingInterfaceId", "trafficSourceId").count().as("count"));
//		 
//		 Aggregation aggr1=newAggregation(filterStates,
//		         group("chargingInterfaceId", "trafficSourceId").sum("chargedAmount").as("totalPrice"));
//		 
//		 Aggregation aggr2=newAggregation(filterStates,
//		         group("chargingInterfaceId", "trafficSourceId").avg("chargedAmount").as("avgChargedPrice"));
		
		 AggregationResults<TrafficStats> result = mongoTemplate.aggregate(aggr, Subscriber.class,TrafficStats.class);
//		 AggregationResults<TrafficStats> result1 = mongoTemplate.aggregate(aggr1, Subscriber.class,TrafficStats.class);
//		 AggregationResults<TrafficStats> result2 = mongoTemplate.aggregate(aggr2, Subscriber.class,TrafficStats.class);
//		 
//		 List l= result.getMappedResults();
//		 List l1= result1.getMappedResults();
//		 List l2= result2.getMappedResults();
//		 List<TrafficStats> finalList = new ArrayList();
//		 for(int i=0; i<l.size(); i++){
//			 TrafficStats ts= (TrafficStats)l.get(i);
//			 TrafficStats ts1= (TrafficStats)l1.get(i);
//			 TrafficStats ts2= (TrafficStats)l2.get(i);
//			 ts.setTotalPrice(ts1.getTotalPrice());
//			 ts.setAvgChargedPrice(ts2.getAvgChargedPrice());
//			 finalList.add(ts);
//		 }
		 
		 logger.debug("Query result "+result);
		 return result;
	 }
	
	 public  Map<String,TrafficStats> getCDRTrafficStats(){
		 return getCDRTrafficStats(null);
	 }
	 
	 public  Map<String,TrafficStats> getCDRTrafficStats(NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
			 
			 /*Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			 cal.set(Calendar.HOUR_OF_DAY, 0);
			 cal.set(Calendar.MINUTE, 0);
			 cal.set(Calendar.SECOND, 0);*/
			Calendar cal=Calendar.getInstance();
	        cal.setTimeInMillis(System.currentTimeMillis());
	        cal.set(Calendar.HOUR_OF_DAY, 0);
	        cal.set(Calendar.MINUTE, 0);
	        cal.set(Calendar.SECOND, 0);
//	        cal.set(Calendar.MILLISECOND, 0);
//	        cal.add(Calendar.HOUR_OF_DAY, -5);
//	        cal.add(Calendar.MINUTE, -30);
			 
			 java.util.Date cDate=cal.getTime();
			 //Get Stats Without Publisher id
			 stats.putAll(getTotalStats(cDate,nc));
			
			 //Add Success Count Stats
			 Map<String,TrafficStats> newStats=getSuccessStats(cDate,nc);
			 Iterator i=newStats.keySet().iterator();
			 String key="";TrafficStats oldObj=null,newObj=null;
			 while(i.hasNext()){
				 key=(String)i.next();
				 if(stats.containsKey(key)){
					 newObj=newStats.get(key); oldObj=stats.get(key); oldObj.setSuccessCount(newObj.getSuccessCount()); stats.put(key, oldObj); newObj=null;oldObj=null;
				 }else
					 stats.put(key, newStats.get(key));
			 }
			 newStats=null;key="";oldObj=null;newObj=null;i=null;
			 
			 //Add Sent Count Stats
			 newStats=getSentCount(cDate,nc);
			 i=newStats.keySet().iterator();
			 while(i.hasNext()){
				 key=(String)i.next();
				 if(stats.containsKey(key)){
					 newObj=newStats.get(key); oldObj=stats.get(key); oldObj.setSentCount(newObj.getSentCount()); stats.put(key, oldObj); newObj=null;oldObj=null;
				 }else
					 stats.put(key, newStats.get(key));
			 }
			 newStats=null;
			 newStats=null;key="";oldObj=null;newObj=null;i=null;
			 
			 
			 //Add Total Churn Stats
			newStats= getTotalChurn(cDate,nc);
			i=newStats.keySet().iterator();
			 while(i.hasNext()){
				 key=(String)i.next();
				 if(stats.containsKey(key)){
					 newObj=newStats.get(key); oldObj=stats.get(key); oldObj.setTotalChurn(newObj.getTotalChurn()); stats.put(key, oldObj); newObj=null;oldObj=null;
				 }else
					 stats.put(key, newStats.get(key));
			 }
			 newStats=null;
			 newStats=null;key="";oldObj=null;newObj=null;i=null;
			 
			 //Add Same Day Churn Stats
			 newStats=getSameDayChurn(cDate,nc);
			 i=newStats.keySet().iterator();
			 while(i.hasNext()){
				 key=(String)i.next();
				 if(stats.containsKey(key)){
					 newObj=newStats.get(key); oldObj=stats.get(key); oldObj.setSamedayChurn(newObj.getSamedayChurn()); stats.put(key, oldObj); newObj=null;oldObj=null;
				 }else
					 stats.put(key, newStats.get(key));
			 }
			 newStats=null;
			 newStats=null;key="";oldObj=null;newObj=null;i=null;
			 
			 //Add Same Day Churn Stats
			 newStats=get20MinChurn(cDate,nc);
			 i=newStats.keySet().iterator();
			 while(i.hasNext()){
				 key=(String)i.next();
				 if(stats.containsKey(key)){
					 newObj=newStats.get(key); oldObj=stats.get(key); oldObj.setChurn20Min(newObj.getChurn20Min()); stats.put(key, oldObj); newObj=null;oldObj=null;
				 }else
					 stats.put(key, newStats.get(key));
			 }
			 newStats=null;
			 newStats=null;key="";oldObj=null;newObj=null;i=null;
			 
		 }catch(Exception e){
			 logger.debug("Exception while generating Total Stats "+e);
		 }
		 return stats;
	 }
	 
	 	 
	 public Map<String,TrafficStats> getTotalStats(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
	
		 AggregationResults<TrafficStats> result=null;
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 MatchOperation filterStates = match(new Criteria("requestDate").gte(cDate));
			 Aggregation aggr=newAggregation(filterStates,
					 group("chargingInterfaceId", "trafficSourceId", "campaignId")
					 .count().as("totalCount"));
			 logger.debug("TrafficStatsBuilder::getTotalStats() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 }else{
			 MatchOperation filterStates = match(new Criteria("requestDate").gte(cDate).and("publisherId").in(pubId));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("totalCount"));
			 logger.debug("TrafficStatsBuilder::getTotalStats() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 String key="";
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 stats.put(key, ts);
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("Exception while generating Total Stats "+e);
		 }
		 return stats;
	 }
	 
	 public Map<String,TrafficStats> getSuccessStats(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
		 AggregationResults<TrafficStats> result=null;
		
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("campaignId").gt(0).and("requestType").is("ACT").and("status").is(JocgStatusCodes.CHARGING_SUCCESS).and("chargedAmount").gt(0));
			 Aggregation aggr=newAggregation(filterStates,
					 group("chargingInterfaceId", "trafficSourceId", "campaignId")
					 .count().as("successCount"));
			 logger.debug("TrafficStatsBuilder::getSuccessStats() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			
		 }else{
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("campaignId").gt(0).and("requestType").is("ACT").and("status").is(JocgStatusCodes.CHARGING_SUCCESS).and("chargedAmount").gt(0).and("publisherId").in(nc.getPubid()));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("successCount"));
			 logger.debug("TrafficStatsBuilder::getSuccessStats() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 String key="";
		 logger.debug("TrafficStatsBuilder::getSuccessStats() ::result "+result);
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 logger.debug("TrafficStatsBuilder::getSuccessStats() ::Adding Key "+key+", Stats: "+ts);
			 stats.put(key, ts);
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("Exception while generating Success Stats "+e);
		 }
		 return stats;
	 }
	 
	 public Map<String,TrafficStats> getTotalChurn(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
		 AggregationResults<TrafficStats> result=null;
			
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 
			 MatchOperation filterStates = match(new Criteria("requestDate").gte(cDate).and("requestType").in("DCT","UNSUB").and("status").in(JocgStatusCodes.UNSUB_SUCCESS));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("totalChurn"));
			 logger.debug("TrafficStatsBuilder::getTotalChurn() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 }else{
			 MatchOperation filterStates = match(new Criteria("requestDate").gte(cDate).and("requestType").in("DCT","UNSUB").and("publisherId").in(nc.getPubid()).and("status").in(JocgStatusCodes.UNSUB_SUCCESS));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("totalChurn"));
			 logger.debug("TrafficStatsBuilder::getTotalChurn() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 String key="";
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 stats.put(key, ts);
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("Exception while generating Total Churn Stats "+e);
		 }
		 return stats;
	 }
	 

	 public Map<String,TrafficStats> getSameDayChurn(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
		 AggregationResults<TrafficStats> result=null;
		
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("requestType").is("ACT").and("samedaychurn").is(true).and("status").is(4));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("samedayChurn"));
			 logger.debug("TrafficStatsBuilder::getSameDayChurn() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 }else{
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("requestType").is("ACT").and("status").is(4).and("samedaychurn").is(true).and("publisherId").is(pubId));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("samedayChurn"));
			 logger.debug("TrafficStatsBuilder::getSameDayChurn() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 logger.debug("TrafficCache::getSameDayChurn :: "+result);
		 String key="";
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 logger.debug("TrafficCache::getSameDayChurn :: Adding Key "+key);
			 stats.put(key, ts);
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("Exception while generating SameDay Churn Stats "+e);
		 }
		 return stats;
	 }

	 public Map<String,TrafficStats> get20MinChurn(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
		 AggregationResults<TrafficStats> result=null;
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("requestType").is("ACT").and("status").is(4).and("churn20min").is(true));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("churn20min"));
			 logger.debug("TrafficStatsBuilder::get20MinChurn() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 }else{
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("requestType").is("ACT").and("status").is(4).and("churn20min").is(true).and("publisherId").is(pubId));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("churn20min"));
			 logger.debug("TrafficStatsBuilder::get20MinChurn() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 logger.debug("TrafficCache::get20MinChurn :: "+result);
		 String key="";
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 logger.debug("TrafficCache::get20MinChurn :: Adding Key "+key);
			 stats.put(key, ts);
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("Exception while generating 20 Min Churn Stats "+e);
		 }
		 return stats;
	 }
	 
	 public Map<String,TrafficStats> getSentCount(java.util.Date cDate,NotificationConfig nc){
		 Map<String,TrafficStats> stats=new HashMap<>();
		 try{
		 String pubId=(nc==null)?"NA":(nc.getPubid()==null)?"NA":nc.getPubid().length()>0?("NA".equalsIgnoreCase(nc.getPubid())?"NA":nc.getPubid()):"NA";
		 String keySuffix="NA".equalsIgnoreCase(pubId)?"":"_"+pubId;
		 AggregationResults<TrafficStats> result=null;
		//chargingDate,notifyTime,notifyTimeChurn,requestDate
		 if("NA".equalsIgnoreCase(pubId)){
			 //Create stats without Publisher Id
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("campaignId").gt(0).and("requestType").is("ACT").and("status").is(JocgStatusCodes.CHARGING_SUCCESS).and("notifyStatus").is(JocgStatusCodes.NOTIFICATION_SENT));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("sentCount"));
			 logger.debug("TrafficStatsBuilder::getSentCount() ::Query "+aggr);
			result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 }else{
			 MatchOperation filterStates = match(new Criteria("chargingDate").gte(cDate).and("requestType").is("ACT").and("status").is(JocgStatusCodes.CHARGING_SUCCESS).and("notifyStatus").is(JocgStatusCodes.NOTIFICATION_SENT).and("publisherId").is(pubId));
			 Aggregation aggr=newAggregation(filterStates,
			         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("sentCount"));
			 logger.debug("TrafficStatsBuilder::getSentCount() ::Query "+aggr);
			 result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
			 
		 }
		 logger.debug("TrafficStatsBuilder::getSentCount :: "+result);
		 String key="";
		 for(TrafficStats ts:result){
			 key="STATS_"+ts.getChargingInterfaceId()+"_"+ts.getTrafficSourceId()+"_"+ts.getCampaignId()+keySuffix;
			 logger.debug("TrafficStatsBuilder::getSentCount :: Adding Key "+key+", Stats "+ts);
			 stats.put(key, ts);
			 
			 key="";
		 }
		 
		 
		 }catch(Exception e){
			 logger.debug("TrafficStatsBuilder::getSentCount::Exception while generating Sent Count Stats "+e);
		 }
		 return stats;
	 }
	 
	/* public  Map<String,TrafficStats> getCDRTrafficStats(){

		 Aggregation aggr=newAggregation(
				 group("chargingInterfaceId", "trafficSourceId", "campaignId")
				 .count().as("totalCount"));
		 

		 MatchOperation filterStates1 = match(new Criteria("status").in(5));
		 Aggregation aggr1=newAggregation(filterStates1,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("successCount"));
		 
		 MatchOperation filterStates2 = match(new Criteria("requestType").in("DCT","UNSUB"));
		 Aggregation aggr2=newAggregation(filterStates2,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("totalChurn"));
		 
		 MatchOperation filterStates3 = match(new Criteria("samedaychurn").is(true).and("status").is(5));
		 Aggregation aggr3=newAggregation(filterStates3,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("samedayChurn"));
		 
		 MatchOperation filterStates4 = match(new Criteria("churn20min").is(true).and("status").is(5));
		 Aggregation aggr4=newAggregation(filterStates4,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId").count().as("churn20min"));
		 
		 
		 AggregationResults<TrafficStats> result = mongoTemplate.aggregate(aggr, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result1 = mongoTemplate.aggregate(aggr1, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result2 = mongoTemplate.aggregate(aggr2, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result3 = mongoTemplate.aggregate(aggr3, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result4 = mongoTemplate.aggregate(aggr4, ChargingCDR.class,TrafficStats.class);
		 
		 
		 List<TrafficStats> l= result.getMappedResults();
		 Map<String,TrafficStats> finalStats = new HashMap();
		 for(int i=0; i<l.size(); i++){
			 TrafficStats ts= (TrafficStats)l.get(i);
			 String key = ts.getChargingInterfaceId() + "_" + ts.getTrafficSourceId() + "_" + ts.getCampaignId();
			 finalStats.put(key, ts);
		 }
		 
		 List<TrafficStats> l1= result1.getMappedResults();
		 List<TrafficStats> l2= result2.getMappedResults();
		 List<TrafficStats> l3= result3.getMappedResults();
		 List<TrafficStats> l4= result4.getMappedResults();
		 for(int i=0; i<l.size(); i++){
			 if(l1.size()>i){
				 TrafficStats ts1= (TrafficStats)l1.get(i);
				 String key1 = ts1.getChargingInterfaceId() + "_" + ts1.getTrafficSourceId() + "_" + ts1.getCampaignId();
				 finalStats.get(key1).setSuccessCount(ts1.getSuccessCount());
			 }
			 
			 if(l2.size()>i){
				 TrafficStats ts2= (TrafficStats)l2.get(i);
				 String key2 = ts2.getChargingInterfaceId() + "_" + ts2.getTrafficSourceId() + "_" + ts2.getCampaignId();
				 finalStats.get(key2).setTotalChurn(ts2.getTotalChurn());
			 }
			 
			 if(l3.size()>i){
				 TrafficStats ts3= (TrafficStats)l3.get(i);
				 String key3 = ts3.getChargingInterfaceId() + "_" + ts3.getTrafficSourceId() + "_" + ts3.getCampaignId();
				 finalStats.get(key3).setSamedayChurn(ts3.getSamedayChurn());
			 }
			 
			 if(l4.size()>i){
				 TrafficStats ts4= (TrafficStats)l4.get(i);
				 String key4 = ts4.getChargingInterfaceId() + "_" + ts4.getTrafficSourceId() + "_" + ts4.getCampaignId();
				 finalStats.get(key4).setChurn20Min(ts4.getChurn20Min());
			 }
		 }
		 
		 
		 ///with pubid
		 Aggregation aggr11=newAggregation(
				 group("chargingInterfaceId", "trafficSourceId", "campaignId", "publisherId")
				 .count().as("totalCount"));
		 
		 MatchOperation filterStates21 = match(new Criteria("status").in(5));
		 Aggregation aggr21=newAggregation(filterStates21,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId", "publisherId").count().as("successCount"));
		 
		 MatchOperation filterStates22 = match(new Criteria("requestType").in("DCT","UNSUB"));
		 Aggregation aggr22=newAggregation(filterStates22,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId", "publisherId").count().as("totalChurn"));
		 
		 MatchOperation filterStates23 = match(new Criteria("samedaychurn").is(true).and("status").is(5));
		 Aggregation aggr23=newAggregation(filterStates23,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId", "publisherId").count().as("samedayChurn"));
		 
		 MatchOperation filterStates24 = match(new Criteria("churn20min").is(true).and("status").is(5));
		 Aggregation aggr24=newAggregation(filterStates24,
		         group("chargingInterfaceId", "trafficSourceId", "campaignId", "publisherId").count().as("churn20min"));
		 
		 
		 AggregationResults<TrafficStats> result11 = mongoTemplate.aggregate(aggr11, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result21 = mongoTemplate.aggregate(aggr21, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result22 = mongoTemplate.aggregate(aggr22, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result23 = mongoTemplate.aggregate(aggr23, ChargingCDR.class,TrafficStats.class);
		 AggregationResults<TrafficStats> result24 = mongoTemplate.aggregate(aggr24, ChargingCDR.class,TrafficStats.class);
		 
		 
		 List<TrafficStats> l11= result11.getMappedResults();
//		 Map<String,TrafficStats> finalStats11 = new HashMap();
		 for(int i=0; i<l11.size(); i++){
			 TrafficStats ts11= (TrafficStats)l11.get(i);
			 if(ts11.getPublisherId()!=null && !ts11.getPublisherId().equals("NA")){
				 String key11 = ts11.getChargingInterfaceId() + "_" + ts11.getTrafficSourceId() + "_" + ts11.getCampaignId()+ "_" + ts11.getPublisherId();
				 finalStats.put(key11, ts11);
			 }
		 }
		 
		 List<TrafficStats> l21= result21.getMappedResults();
		 List<TrafficStats> l22= result22.getMappedResults();
		 List<TrafficStats> l23= result23.getMappedResults();
		 List<TrafficStats> l24= result24.getMappedResults();
		 for(int i=0; i<l11.size(); i++){
			 if(l21.size()>i){
				 TrafficStats ts21= (TrafficStats)l21.get(i);
				 String key21 = ts21.getChargingInterfaceId() + "_" + ts21.getTrafficSourceId() + "_" + ts21.getCampaignId();
				 finalStats.get(key21).setSuccessCount(ts21.getSuccessCount());
			 }
			 
			 if(l22.size()>i){
				 TrafficStats ts22= (TrafficStats)l22.get(i);
				 String key22 = ts22.getChargingInterfaceId() + "_" + ts22.getTrafficSourceId() + "_" + ts22.getCampaignId();
				 finalStats.get(key22).setTotalChurn(ts22.getTotalChurn());
			 }
			 
			 if(l23.size()>i){
				 TrafficStats ts23= (TrafficStats)l23.get(i);
				 String key23 = ts23.getChargingInterfaceId() + "_" + ts23.getTrafficSourceId() + "_" + ts23.getCampaignId();
				 finalStats.get(key23).setSamedayChurn(ts23.getSamedayChurn());
			 }
			 
			 if(l24.size()>i){
				 TrafficStats ts24= (TrafficStats)l24.get(i);
				 String key24 = ts24.getChargingInterfaceId() + "_" + ts24.getTrafficSourceId() + "_" + ts24.getCampaignId();
				 finalStats.get(key24).setChurn20Min(ts24.getChurn20Min());
			 }
		 }

		 
		 logger.debug("Query result "+finalStats);
		 return finalStats;
	 }*/
}
