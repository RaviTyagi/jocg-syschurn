package com.jss.jocg.services.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.jss.jocg.services.data.SubscriberTest;

public interface SubscriberTestRepository  extends MongoRepository<SubscriberTest, String> {
	public SubscriberTest getByName(String name);
	public SubscriberTest getById(String id);
	public List<SubscriberTest> getByAge(Integer age);
}
