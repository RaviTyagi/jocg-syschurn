package com.jss.jocg.services.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

import com.jss.jocg.services.data.Adnetwork;

@Repository
public interface AdnetworkRepository  extends MongoRepository<Adnetwork, String>,AdnetworkRepositoryCustom{
	public Adnetwork findById(String id);
	public Adnetwork findByClickId(String clickId);
}
