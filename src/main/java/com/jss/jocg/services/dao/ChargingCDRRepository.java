package com.jss.jocg.services.dao;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.jss.jocg.services.data.ChargingCDR;


public interface ChargingCDRRepository  extends MongoRepository<ChargingCDR, String>,ChargingCDRRepositoryCustom {
	public ChargingCDR findById(String id);
	public ChargingCDR findByjocgTransId(String jocgTransId);
	public ChargingCDR findByjocgTransIdAndStatus(String jocgTransId,Integer status);
	public ChargingCDR findByChargingInterfaceIdAndSdpTransactionId(Integer chargingInterfaceId,String sdpTransactionId);
	public List<ChargingCDR> findByMsisdn(Long msisdn);
	public List<ChargingCDR> findBySubscriberId(String subscriberId);
	@Query("{'msisdn' : ?0, 'requestDate' : {$gte : ?1}}")
	public List<ChargingCDR> findByMsisdnAndRequestDate(Long msisdn,java.util.Date requestDate);
	@Query("{'msisdn' : ?0, 'requestDate' : {$gte : ?1}, 'status' : ?2}")
	public List<ChargingCDR> findByMsisdnAndRequestDateAndStatus(Long msisdn,java.util.Date requestDate,Integer status);
	
}
