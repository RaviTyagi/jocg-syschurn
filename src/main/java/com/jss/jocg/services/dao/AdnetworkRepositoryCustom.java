package com.jss.jocg.services.dao;

import com.jss.jocg.services.data.Adnetwork;

public interface AdnetworkRepositoryCustom {

	public Adnetwork searchByClickId(String clickId);
	
}
