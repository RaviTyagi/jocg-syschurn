package com.jss.jocg.services.dao;

import java.util.List;

import com.jss.jocg.cdr.entities.SubscriberStats;
import com.jss.jocg.services.data.Subscriber;

public interface SubscriberRepositoryCustom {

	public List<Subscriber> getNextRenewalBatch();
	public void resetRenewalFlag();
	public Subscriber searchActiveSubscriber(String subscriberId,Long msisdn,Integer serviceId,String serviceName);
	public Subscriber searchSubscriberByChintfAndServiceId(Integer chintfId,String subscriberId,Long msisdn,Integer serviceId,String serviceName);
	public List<SubscriberStats> getSubscriberStatsForTheDay();
	public List<Subscriber> searchSystemChurnSubscribers(int ppid);
}
