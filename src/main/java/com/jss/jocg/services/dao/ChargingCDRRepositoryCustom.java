package com.jss.jocg.services.dao;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;

import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.services.data.ChargingCDR;

public interface ChargingCDRRepositoryCustom {

	public  Map<String,TrafficStats> getCDRTrafficStats();
	public List<ChargingCDR> findQueuedNotifications(String logHeader,Integer status,Integer notifyStatus,int limit);
	public List<ChargingCDR> findQueuedReportCDR(String logHeader,int pageSize,java.util.Date lastUpdate,boolean fetchFromBegining);
}
