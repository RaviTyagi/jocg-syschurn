package com.jss.jocg.services.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.services.data.Subscriber;

public interface SubscriberRepository extends MongoRepository<Subscriber, String>,SubscriberRepositoryCustom {
	
	//public Subscriber getBysubregId(Long subregId);
	public Subscriber findById(String id);
	public List<Subscriber> findBySubscriberId(String subscriberId);
	public Subscriber findByJocgCDRId(String jocgCDRId);
	public Subscriber findByCustomerId(String customerId);
	public Subscriber findByJocgTransId(String jocgTransId);
	public Subscriber findByJocgCDRIdAndJocgTransId(String jocgCDRId,String jocgTransId);
	public List<Subscriber> findBySubscriberIdAndServiceName(String subscriberId,String serviceName);

	public List<Subscriber> findByMsisdn(Long msisdn);
	//@Query(value = "select b from Subscriber b where b.status=:status and b.file like %:filePattern%")
	public Subscriber findByMsisdnAndOperatorServiceKey(Long msisdn,String operatorServiceKey);
	public Subscriber findBySubscriberIdAndOperatorServiceKey(String subscriberId,String operatorServiceKey);
	public Subscriber findByChargingInterfaceIdAndMsisdnAndServiceId(Integer chargingInterfaceId,Long msisdn,Integer serviceId);
	public List<Subscriber> findByMsisdnAndServiceId(Long msisdn,Integer serviceId);
	public List<Subscriber> findByMsisdnAndServiceName(Long msisdn,String serviceName);
	public Subscriber findByMsisdnAndPackageName(Long msisdn,String packageName);
	@Query("{ status : ?0 , renewalCategory : ?1 , renewalRequired : ?2}")
	public List<Subscriber> getByStatusAndRenewalCategoryAndRenewalRequired(int status,String renewalCategory,boolean renewalRequired);
	public List<Subscriber> findFirst100ByStatusAndRenewalCategoryAndRenewalRequiredAndRenewalStatus(Integer status,String renewalCategory,Boolean renewalRequired,Integer renewalStatus);
//	@Query("{$group : { _id : {chargingInterfaceId : '$chargingInterfaceId',trafficSourceId : '$trafficSourceId', publisherId :'$publisherId'}, totalPrice : { $sum : '$chargedAmount'}, avgChargedPrice : { $avg : '$chargedAmount'}, count : {$sum : 1} }}")
//	public List<TrafficStats> findAggregate();
	
	//public Subscriber findByMsisdnAndStatusCode(Long msisdn,Integer statusCode);
	
}
