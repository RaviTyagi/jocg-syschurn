package com.jss.jocg.services.dao;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.TrafficStats;
import com.jss.jocg.cdr.entities.SubscriberStats;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.services.data.SubscriberActiveBase;
import com.jss.jocg.util.JocgStatusCodes;

public class SubscriberRepositoryImpl implements SubscriberRepositoryCustom{
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriberRepositoryImpl.class);
	 @Autowired
	    private MongoTemplate mongoTemplate;
	 @Autowired
	 	private JocgCacheManager jocgCache;
	 
	 public void resetRenewalFlag(){
//		 Query query = new Query();
//		 try{
//			 query.addCriteria()
//		 }catch(Exception e){
//			 
//		 }
	 }
	 
	 public List<Subscriber> searchSystemChurnSubscribers(int ppid){
		 String logHead="searchProcessSystemChurnSubscribers ";
		 List<Subscriber> subList=null;		 
//		 List<Subscriber> subList=null;		
		 try{
			 boolean runQuery=false;
			 Query query = new Query();
			 if(ppid>0){
				 query.addCriteria(Criteria.where("pricepointId").is(ppid).and("reportGenerated").is(true).and("churnProcessed").is(false).and("status").ne(JocgStatusCodes.UNSUB_SUCCESS));
				 query.with(new Sort(Sort.Direction.DESC, "validityEndDate"));
				 runQuery=true;
			 }		 
			 if(runQuery==true){
				 query.limit(500);
				 logger.debug(logHead+"searchActiveSubscriber :: search By PricePointId : "+ppid+" :: "+query+"  "+query.getLimit());				 
				  subList=mongoTemplate.find(query, Subscriber.class);
			 }
		 }catch(Exception e){
			logger.error(logHead+"Exception "+e); 
		 }		 
		 return subList;
	 }
	 
	 public List<SubscriberStats> getSubscriberStatsForTheDay(){
		 List<SubscriberStats> subList=new ArrayList<>();
		 try{
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			 java.util.Date d=cal.getTime();
			 SimpleDateFormat sdfdate=new SimpleDateFormat("yyyy-MM-dd");
			 String cdate=sdfdate.format(d);
		 MatchOperation filterStates = match(new Criteria("status").is(5).and("validityEndDate").gte(d));
		 //.and("validityEndDate").gte(endDate)
		 //("chargingDate").lte(endDate).and
		
		 Aggregation aggr=newAggregation(filterStates,
				 group("sourceNetworkCode","operatorName", "circleName","serviceName","packageName")
				 .count().as("activeBase"));
		 logger.debug("getSubscriberStatsForTheDay():: Query "+aggr);
		 AggregationResults<SubscriberActiveBase> result = mongoTemplate.aggregate(aggr, Subscriber.class,SubscriberActiveBase.class);
		 logger.debug("getSubscriberStatsForTheDay():: result "+result);
		 Iterator<SubscriberActiveBase> i=result.iterator();
		 SubscriberActiveBase newObj=null;SubscriberStats newStats=null;
		 while(i.hasNext()){
			 newObj=i.next();
			 logger.debug("getSubscriberStatsForTheDay():: processing  "+newObj);
			 if(newObj!=null && newObj.getActiveBase()!=null){
				newStats=new SubscriberStats(null,newObj.getSourceNetworkCode(),newObj.getOperatorName(),newObj.getCircleName(),newObj.getServiceName(),newObj.getPackageName(),newObj.getActiveBase());
				newStats.setDatestr(sdfdate.parse(cdate));
				subList.add(newStats);
				 newStats=null;
			 }
			 
			 newObj=null;
		 }
		 }catch(Exception e){
			 logger.error("SUBSUMMARY :: getSubscriberStatsForTheDay():: Exception "+e);
			 e.printStackTrace();
		 }
		 return subList;
	 }
	
	 public Subscriber searchActiveSubscriber(String subscriberId,Long msisdn,Integer serviceId,String serviceName){
		 Subscriber sub=null;
		 try{
			 subscriberId=(subscriberId==null)?"":subscriberId.trim();
			 msisdn=(msisdn==null)?new Long(0L):msisdn;
			 boolean runQuery=false;
			 Query query = new Query();
			 if(msisdn.longValue()>0 && subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId)){
				 query.addCriteria(Criteria.where("serviceName").is(serviceName).
					 orOperator(Criteria.where("subscriberId").is(subscriberId),Criteria.where("msisdn").is(msisdn)));
				 runQuery=true;
			 }else if(msisdn.longValue()>0){
				 query.addCriteria(Criteria.where("serviceName").is(serviceName).and("msisdn").is(msisdn));
				 runQuery=true;
			 }else if(subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId) && subscriberId.length()>0){
				 query.addCriteria(Criteria.where("serviceName").is(serviceName).and("subscriberId").is(subscriberId));
				 runQuery=true;
			 }
			 
			 if(runQuery==true){
				 query.with(new Sort(Sort.Direction.DESC, "status"));
				 query.with(new Sort(Sort.Direction.DESC, "validityEndDate"));
				 logger.debug("searchActiveSubscriber :: search By service Name : "+msisdn+" :: "+query);
				 
				 List<Subscriber> subList=mongoTemplate.find(query, Subscriber.class);
				 if(subList==null || subList.isEmpty() && serviceId>0){
					 query = new Query();
					 runQuery=false;
					 if(msisdn.longValue()>0 && subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId)){
						 query.addCriteria(Criteria.where("serviceId").is(serviceId).
							 orOperator(Criteria.where("subscriberId").is(subscriberId),Criteria.where("msisdn").is(msisdn)));
						 runQuery=true;
					 }else if(msisdn.longValue()>0){
						 query.addCriteria(Criteria.where("serviceId").is(serviceId).and("msisdn").is(msisdn));
						 runQuery=true;
					 }else if(subscriberId!=null && !"NA".equalsIgnoreCase(subscriberId) && subscriberId.length()>0){
						 query.addCriteria(Criteria.where("serviceId").is(serviceId).and("subscriberId").is(subscriberId));
						 runQuery=true;
					 }
					 
					 if(runQuery==true){
					 	 query.addCriteria(Criteria.where("serviceId").is(serviceId).
								 orOperator(Criteria.where("subscriberId").is(subscriberId),Criteria.where("msisdn").is(msisdn)));
						
						 query.with(new Sort(Sort.Direction.DESC, "status"));
						 query.with(new Sort(Sort.Direction.DESC, "validityEndDate"));
						 logger.debug("searchActiveSubscriber ::search By service Id: "+msisdn+" :: "+query);
					 }
				 }
				 
				 java.util.Date d=new java.util.Date(System.currentTimeMillis());
				 for(Subscriber s:subList){
					 if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && ((s.getValidityEndDate()!=null && s.getValidityEndDate().after(d)) || (s.getValidityEndDate()==null && s.getNextRenewalDate()!=null && s.getNextRenewalDate().after(d)))){
						 sub=s;
						 break;
					 }
				 }
				 
				 for(Subscriber s:subList){
					 if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE && ((s.getValidityEndDate()!=null && s.getValidityEndDate().after(d)) || (s.getValidityEndDate()==null && s.getNextRenewalDate()!=null && s.getNextRenewalDate().after(d)))){
						 sub=s;
						 break;
					 }
				 }
				 for(Subscriber s:subList){
					 if(sub==null || sub.getId()==null){
						sub=s;
						break;
					 }else break;
				 }
			 }
		 }catch(Exception e){
			 
		 }
		 return sub;
	 }
	 
	 public Subscriber searchSubscriberByChintfAndServiceId(Integer chintfId,String subscriberId,Long msisdn,Integer serviceId,String serviceName){
		 Subscriber sub=null;
		 try{
			 subscriberId=(subscriberId==null)?"":subscriberId.trim();
			 msisdn=(msisdn==null)?new Long(0L):msisdn;
			 
			 Query query = new Query();
			 query.addCriteria(Criteria.where("serviceName").is(serviceName).and("chargingInterfaceId").is(chintfId.intValue()).
					 orOperator(Criteria.where("subscriberId").is(subscriberId),Criteria.where("msisdn").is(msisdn)));
			
			 query.with(new Sort(Sort.Direction.DESC, "status"));
			 query.with(new Sort(Sort.Direction.DESC, "validityEndDate"));
			 logger.debug("searchActiveSubscriber :: search By service Name : "+msisdn+" :: "+query);
			 
			 List<Subscriber> subList=mongoTemplate.find(query, Subscriber.class);
			 			 
			 java.util.Date d=new java.util.Date(System.currentTimeMillis());
			 for(Subscriber s:subList){
				 if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_ACTIVE && ((s.getValidityEndDate()!=null && s.getValidityEndDate().after(d)) || (s.getValidityEndDate()==null && s.getNextRenewalDate()!=null && s.getNextRenewalDate().after(d)))){
					 sub=s;
					 break;
				 }
			 }
			 
			 for(Subscriber s:subList){
				 if(s.getStatus()==JocgStatusCodes.SUB_SUCCESS_FREE && ((s.getValidityEndDate()!=null && s.getValidityEndDate().after(d)) || (s.getValidityEndDate()==null && s.getNextRenewalDate()!=null && s.getNextRenewalDate().after(d)))){
					 sub=s;
					 break;
				 }
			 }
			 for(Subscriber s:subList){
				 if(sub==null || sub.getId()==null){
					sub=s;
					break;
				 }else break;
			 }
		 }catch(Exception e){
			 
		 }
		 return sub;
	 }
	 
	public List<Subscriber> getNextRenewalBatch(){
		 Query query = new Query();
		 try{
			// Calendar cal=Calendar.getInstance();
			 //cal.setTimeInMillis(System.currentTimeMillis());
			 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			 Calendar cal=Calendar.getInstance();
			 cal.setTimeInMillis(System.currentTimeMillis());
			 String fcdate=sdf.format(cal.getTime());
			// cal.set(Calendar.DATE, 1);
			 //Updated after discussion with Nidhi Godha and Gajanand on 2nd Jan
			 cal.add(Calendar.DATE, -85);
			 String fstart=sdf.format(cal.getTime());
			 java.util.Date d1=sdf.parse(fcdate);
			
			 System.out.println(d1);
		
			
			 List<Integer> statusList=new ArrayList();
			 statusList.add(JocgStatusCodes.SUB_SUCCESS_ACTIVE);
			 statusList.add(JocgStatusCodes.SUB_SUCCESS_GRACE);
//			 query.addCriteria(Criteria.where("renewalCategory").is("SYSTEM").
//						and("renewalRequired").is(true).
//						and("status").in(statusList));
//			 query.addCriteria(Criteria.where("nextRenewalDate").lte(sdf.parse(fcdate)).orOperator(
//					 Criteria.where("nextRenewalDate").exists(false).and("validityEndDate").lte(sdf.parse(fcdate))));
					
			query.addCriteria(Criteria.where("renewalCategory").is("SYSTEM").
								and("renewalRequired").is(true).
								and("status").in(statusList).
				   orOperator(Criteria.where("nextRenewalDate").exists(false).and("validityEndDate").lte(sdf.parse(fcdate)).gte(sdf.parse(fstart)),
						   Criteria.where("validityEndDate").lte(sdf.parse(fcdate)).gte(sdf.parse(fstart)).
						   and("nextRenewalDate").lte(sdf.parse(fcdate)).gte(sdf.parse(fstart))));		 
			 query.limit(1000);
			
		 }catch(Exception e){
			 logger.error("Exception "+e);
			 System.out.println("Exception "+e);
		 }
		 System.out.println("Query "+query);
		 return mongoTemplate.find(query, Subscriber.class);
		
		
	}
}
