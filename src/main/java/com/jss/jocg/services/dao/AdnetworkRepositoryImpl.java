package com.jss.jocg.services.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.jss.jocg.services.data.Adnetwork;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JocgStatusCodes;

public class AdnetworkRepositoryImpl implements AdnetworkRepositoryCustom{
	private static final Logger logger = LoggerFactory
			.getLogger(AdnetworkRepositoryImpl.class);
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	public Adnetwork searchByClickId(String clickId){
		 Query query = new Query();
		 try{
			 query.addCriteria(Criteria.where("clickId").is(clickId));
			
		 }catch(Exception e){
			logger.error("Exception "+e); 
		 }
		return mongoTemplate.findOne(query, Adnetwork.class);
	}
}
