package com.jss.jocg.services.dao;

import java.util.List;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.jss.jocg.services.data.AirtelIndiaPendingTransactions;


public interface AirtelIndiaPendingTransactionsDao  extends MongoRepository<AirtelIndiaPendingTransactions, String>{
	public List<AirtelIndiaPendingTransactions> findByStatus(Integer status);
	
}
