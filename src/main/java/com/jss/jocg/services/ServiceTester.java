package com.jss.jocg.services;

import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
//import com.jss.jocg.cdr.dao.JocgcdrRepository;
//import com.jss.jocg.cdr.entities.JocgcdrEvent;
import com.jss.jocg.charging.model.VodaChargingRequest;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.model.QueryResponse;
import com.jss.jocg.services.dao.TestDBRepository;
import com.jss.jocg.services.data.TestDB;

@RestController
@RequestMapping("/test")
public class ServiceTester  extends JocgService {
	private static final Logger logger = LoggerFactory
			.getLogger(ServiceTester.class);
	//@Autowired JocgcdrRepository cdrRepo;
	
	@Autowired TestDBRepository testDb;
	
	@RequestMapping("/headers")
	public String testRouting(){
		StringBuilder sb=new StringBuilder();
		Enumeration<String> e=requestContext.getHeaderNames();
		while(e.hasMoreElements()){
			String key=e.nextElement();
			sb.append(key).append("=").append(requestContext.getHeader(key)).append("<br/>");
			
		}
		return sb.toString();
	}
	
	@RequestMapping("/time")
	public @ResponseBody TestDB testDateTimeSetting(@RequestParam(value="name",required=false,defaultValue="NA") String name,
			@RequestParam(value="dt",required=false,defaultValue="NA") String dateStamp){
		 TestDB t=new TestDB();
		try{
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyyMMddHHmmss'Z'");
		java.util.Date d=sdf1.parse(dateStamp);
		t.setName(name);
		t.setSubRequestDate(d);
	    
	     t=testDb.insert(t);
	     System.out.println("Time set "+t.getSubRequestDate());
		}catch(Exception e){}
	     return t;
	}
	
	
//	@RequestMapping("/cdr/{id}")
//	public @ResponseBody JocgcdrEvent searchCDREvent(Model model,@PathVariable(value="id") String cdrId){
//		return cdrRepo.findByCdrid(cdrId);
//	}
//	
	@RequestMapping("/packs/testvodapk430")
	public ModelAndView getTestPackage(Model model){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("vodacg430");
		return mv;
	}
	
	@RequestMapping("/packs/{operator}/{packName}")
	public ModelAndView getPackageListing(Model model, @PathVariable String operator,@PathVariable String packName){
		ModelAndView mv=new ModelAndView();
		Long msisdn=parseMsisdn();
		ChargingInterface chintfOp=jocgCache.getChargingInterfaceByName(operator);
		logger.info("JOCGREQ:"+msisdn+": chintfOp "+chintfOp);
		PackageDetail pack=jocgCache.getPackageDetailsByName(packName);
		logger.info("JOCGREQ:"+msisdn+": pack "+pack);
		Map<Integer,PackageChintfmap> pchintfmap=jocgCache.getPackageChargingInterfaceMap();
		Iterator<PackageChintfmap> i=pchintfmap.values().iterator();
		PackageChintfmap pchintfmap1=null;
		while(i.hasNext()){
			pchintfmap1=i.next();
			if(pchintfmap1.getChintfid()==chintfOp.getChintfid() && pchintfmap1.getPkgid()==pack.getPkgid()) break; 
			else pchintfmap1=null;
		}
		logger.info("JOCGREQ:"+msisdn+": pchintfmap1 "+pchintfmap1);
		if(pchintfmap1!=null || msisdn<=0){
			ChargingPricepoint pp=jocgCache.getPricePointListById(pchintfmap1.getOpPpid());
			
			mv.setViewName("vodain");
			VodaChargingRequest vcr=new VodaChargingRequest();
			vcr.setOperator(chintfOp.getName());
			vcr.setPack(pack.getPkgname());
			vcr.setSubid(""+msisdn);
			vcr.setVoucher("");
			mv.addObject("vcr",vcr);
			/*mv.addObject("operator", chintfOp.getName());
			mv.addObject("pack", pack.getPkgname());
			mv.addObject("subid", ""+msisdn);
			mv.addObject("voucher", "");*/
		}else{
			mv.addObject("message", "Invalid Package Request!");
			mv.setViewName("error");
		}
		
		return mv;
	}
	
	
	
}
