package com.jss.jocg.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.sms.data.JocgSMSMessage;

@RestController
@RequestMapping("/")
public class SmsPushRestService extends JocgService{
	private static final Logger logger = LoggerFactory
			.getLogger(SmsPushRestService.class);
	
	@Autowired JmsTemplate jmsTemplate;
	//http://172.31.22.121/jocg/sms/push?msisdn=&chintfid=2&smsinterface=airtelin&text=usetemplate&service=NexgTV&chintftype=operator
	@RequestMapping("jocg/sms/push")
	public @ResponseBody String sendSMS(@RequestParam(value="msisdn",required=true) Long msisdn,
			@RequestParam(value="chintfid",required=true) Integer chintfId,
			@RequestParam(value="smsinterface",required=true) String smsInterface,
			@RequestParam(value="text",required=true) String textMessage,
			@RequestParam(value="service",required=false,defaultValue="NA") String serviceName,
			@RequestParam(value="chintftype",required=true,defaultValue="NA") String chargingInterfaceType){
		String logHeader="smspushservice::"+msisdn+"::";
		String respStr="";
		JocgSMSMessage msg=new JocgSMSMessage();
		try{
			msg.setMsisdn(msisdn);
			msg.setChintfId(chintfId);
			msg.setSmsInterface(smsInterface);
			msg.setTextMessage(textMessage);
			msg.setServiceName(serviceName);
			msg.setChargingInterfaceType(chargingInterfaceType);
			jmsTemplate.convertAndSend("notify.sms", msg);
			logger.debug(logHeader+" New SMS Queued .......SMSData="+msg);
			respStr="OK";
		}catch(Exception e){
			respStr="NOK";
			logger.error(logHeader+" Message Failed for SMSData="+msg+"|Exception "+e);
		}
		return respStr;
	}
	
	
}
