package com.jss.jocg.services;

import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.services.bo.ChargingProcessor;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.dao.SubscriberRepository;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;

@Service
public class RenewalService extends JocgService{
	private static final Logger logger = LoggerFactory
			.getLogger(RenewalService.class);
	
	
	@Autowired SubscriberRepository subscriberDao;
	@Autowired JocgCacheManager jocgCache;
	@Autowired ChargingCDRRepository chargingCDRDao;
	@Autowired JmsTemplate jmsTemplate;
	
	//second, minute, hour, day of month, month, day(s) of week
	
	
//@Scheduled(cron = "0 0/1 * * * *")
//	@Scheduled(initialDelay=1, fixedRate=900000)
	public void startRenewal() {
		logger.debug("RENEWAL FLAG: "+jocgCache.isRenewalEnabled());
		 if(jocgCache.isRenewalEnabled()){
			 	logger.debug("RENEWAL Enabled. Starting Process .....");
				//List<Subscriber> subList= subscriberDao.findFirst100ByStatusAndRenewalCategoryAndRenewalRequiredAndRenewalStatus(JocgStatusCodes.SUB_SUCCESS_ACTIVE,"SYSTEM",new Boolean(true),JocgStatusCodes.REN_NOT_INITIATED);
				List<Subscriber> subList= subscriberDao.getNextRenewalBatch();//getByStatusAndRenewalCategoryAndRenewalRequired(JocgStatusCodes.SUB_SUCCESS_ACTIVE,"SYSTEM",true);
				logger.debug("RENEWAL :: Picked NUmber count "+subList.size());
				System.out.println("RENEWAL :: Picked NUmber count "+subList.size());
				for(Subscriber sub:subList){
					logger.debug("RENEWAL :: Processing Renewal for "+sub.getMsisdn());
					
					boolean processed=false;
					//Sent to JMS Queue if required to be processed
					if(sub.getChargingInterfaceId()==4){
						logger.info("RENEWAL:: ("+sub.getMsisdn()+") Request sent for JMS Queue for Renewal Processing");
						jmsTemplate.convertAndSend("renewal.paytm", sub);
						processed=true;
					}else if(sub.getChargingInterfaceId()==29){
						jmsTemplate.convertAndSend("renewal.mobikwik", sub);
					}else if(sub.getChargingInterfaceId()==34){
						//jmsTemplate.convertAndSend("renewal.connect", sub);
					}else
						logger.error("RENEWAL:: ("+sub.getMsisdn()+") Renewal queue not defined for chintfid "+sub.getChargingInterfaceId());
					
					if(processed){
						System.out.println("RENEWAL :: Processing Renewal for "+sub);
						logger.debug("RENEWAL :: Sent to Paytm Renewal Queue "+sub);
						sub.setRenewalStatus(JocgStatusCodes.REN_INPROCESS);
						Calendar cal=Calendar.getInstance();
						cal.setTimeInMillis(System.currentTimeMillis());
						cal.add(Calendar.DAY_OF_MONTH,1);
						java.util.Date nextRenewalDate=cal.getTime();
						sub.setNextRenewalDate(nextRenewalDate);
						this.updateSubscriber("RENEWAL::", sub, false);
					}else{
						logger.error("RENEWAL:: Skipped for subscriber id "+sub.getId()+", msisdn "+sub.getMsisdn());
					}
					
					logger.debug("RENEWAL :: Status Updated to  "+JocgStatusCodes.REN_INPROCESS+" for id "+sub.getId());
					WAIT(1000);
					//break;
				 }
		 }
		 
	 }
	 
	 public synchronized void WAIT(int ms){
		 try{
			 this.wait(ms);
		 }catch(Exception e){}
	 }
	
	
}
