package com.jss.jocg.services;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.handlers.JocgChargingHandler;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgRequestParameters;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.services.bo.JocgService;
import com.jss.jocg.services.bo.RequestValidator;
import com.jss.jocg.services.bo.ResponseBuilder;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.services.data.Subscriber;
import com.jss.jocg.util.JocgStatusCodes;

@RestController
@RequestMapping("jocg/dct")
public class DeactivationService extends JocgService {
	private static final Logger logger = LoggerFactory
			.getLogger(DeactivationService.class);
	
	@Autowired RequestValidator requestValidator;
	@Autowired ResponseBuilder responseBuilder;
	
	@RequestMapping("/{operator}")
	public @ResponseBody JocgDeactivationResponse processCharging(Model model, 
			@PathVariable(value="operator") String sourceOperator,
			@RequestParam(value="subid",required=false,defaultValue="NA") String subscriberId,
			@RequestParam(value="msisdn",required=false,defaultValue="0") Long requestMsisdn,
			@RequestParam(value="platform",required=false,defaultValue="NA") String platformName,
			@RequestParam(value="pkgname",required=false,defaultValue="NA") String packageName,
			@RequestParam(value="checksum",required=false,defaultValue="0") Long checkSum){
		String logHeader="DCT-SERVICE::{"+subscriberId+","+requestMsisdn+"} :: ";
		JocgDeactivationResponse dctResp=new JocgDeactivationResponse();
			JocgRequest newReq=new JocgRequest();
			newReq.setRequestType("DCT");
			dctResp.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
			String statusDescp="";
			//Set Request Parameters
			newReq.setRequestParams(new JocgRequestParameters("DCT",sourceOperator,subscriberId,parseMsisdn(),requestMsisdn,parseImsi(),platformName,packageName,0D,0,"M","NA",0D,"NA",checkSum,0,"NA",requestContext.getRemoteAddr(),requestContext.getHeader("referer"),requestContext.getHeader("user-agent")));
			
			newReq.setMsisdn(requestValidator.selectMsisdn("DCT", newReq.getRequestParams().getHeaderMsisdn(), newReq.getRequestParams().getRequestMsisdn()));
			
			newReq.setSubscriberId(requestValidator.prepareSubscriberId(newReq.getRequestParams().getSubscriberId(),newReq.getMsisdn()));
			
			//Rewriting LogHeader after selecting msisdn & subscriberId
			logHeader="DCT-SERVICE::{"+subscriberId+","+requestMsisdn+"} :: ";
			
			newReq.setCircle(circleIndentifier.identifyCircle(requestContext.getRemoteAddr(), parseImsi(), newReq.getMsisdn()));
			newReq.setPackageDetail(jocgCache.getPackageDetailsByName(newReq.getRequestParams().getPackageName()));
			if(newReq.getPackageDetail()!=null && newReq.getPackageDetail().getPkgid()>0){
				newReq.setService(jocgCache.getServicesById(newReq.getPackageDetail().getServiceid()));
				if(newReq.getService()==null || newReq.getService().getServiceid()<=0){
					dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
					dctResp.setStatusDescp("Service Details Not found");
				}
			}else{
				dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
				dctResp.setStatusDescp("Invalid Package Name");
			}
			logger.debug(logHeader+" Package Details "+newReq.getPackageDetail());
			//Check if subscribed for the service
			//Check if already subscribed
			Subscriber sub=null;boolean alreadySubscribed=false;
			
			//Search Subscriber & Validate existing subscription status
			sub=subMgr.searchSubscriber(newReq.getSubscriberId(),newReq.getMsisdn(), newReq.getService().getServiceid(), newReq.getService().getServiceName());
			logger.debug(logHeader+" Subscriber  "+sub);
			Integer subStatus=requestValidator.validateSubscription(sub, newReq);
			logger.debug(logHeader+" subStatus  "+subStatus.intValue());
			Integer usedChargingInterface=0;
			if(subStatus==1){
				dctResp.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
				if(sub.getChargingInterfaceId()>0){
					newReq.setChargingOperator(jocgCache.getChargingInterface(sub.getChargingInterfaceId()));
					logger.debug(logHeader+" Chintf  "+newReq.getChargingOperator());
					if(newReq.getChargingOperator()!=null && newReq.getChargingOperator().getChintfid()>0){
						if(newReq.getChargingOperator().getUseparenthandler()>0 && newReq.getChargingOperator().getPchintfid()>0){
							ChargingInterface pchintf=jocgCache.getChargingInterface(newReq.getChargingOperator().getPchintfid());
							logger.debug(logHeader+" Parent Chintf  "+pchintf);
							if(pchintf!=null && pchintf.getChintfid()>0){
								newReq.setChargingHandlerClass(pchintf.getHandlerClass());
								usedChargingInterface=pchintf.getChintfid();
							}else{
								usedChargingInterface=newReq.getChargingOperator().getChintfid();
								newReq.setChargingHandlerClass(newReq.getChargingOperator().getHandlerClass());
							}
						}else{
							usedChargingInterface=newReq.getChargingOperator().getChintfid();
							newReq.setChargingHandlerClass(newReq.getChargingOperator().getHandlerClass());
						}
					}else{
						dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
						dctResp.setStatusDescp("Invalid Charging Operator");
					}
					newReq.setSourceInterface(jocgCache.getChargingInterface(sub.getSourceNetworkId()));
					logger.debug(logHeader+" Source Interface  "+newReq.getSourceInterface());
				}
					
				statusDescp="Subscription found for service "+newReq.getService().getServiceName();
			}else{
				dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
				dctResp.setStatusDescp("Subscriber does not exists, Cant process deactivation!");
				sub=new Subscriber();
			}
			newReq.setSubscriber(sub);
			
			//Validate Checksum if applied
			if(dctResp.getStatusCode().intValue()==JocgStatusCodes.UNSUB_SUCCESS_PENDING ){
				//Prepare for Checksum Validation
				StringBuilder cs=new StringBuilder(platformName);
				cs.append("|").append(subscriberId).append("|").append(sourceOperator).append("|").append(packageName).append("|");
				
				/*if(!requestValidator.validateChecksum(cs.toString(),checkSum,checksumKey)){
					dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
					dctResp.setStatusDescp("Checksum Validation Failed");
				}*/
			}
			
			//Search Charging URL's
			if(dctResp.getStatusCode().intValue()==JocgStatusCodes.UNSUB_SUCCESS_PENDING ){
				List<ChargingUrl> urlList=jocgCache.getChargingUrlByChintfId(usedChargingInterface);
				ChargingUrl churl=null;
				for(ChargingUrl c:urlList){
					if("SDP-UNSUB".equalsIgnoreCase(c.getUrlCatg())){
						churl=c;break;
					}
				}
				logger.debug(logHeader+"unsub URL "+churl);
				newReq.setChargingURL(churl);
				logger.debug(logHeader+" Charging URL  "+newReq.getChargingURL());
				if(churl!=null && churl.getChurlId()>0){
					Map<Integer,List<ChargingUrlParam>> urlParams=new HashMap<>();
					urlParams.put(churl.getChurlId(), jocgCache.getChargingUrlParams(churl.getChurlId()));
					newReq.setUrlParams(urlParams);
				}
				
			}
			
			if(dctResp.getStatusCode().intValue()==JocgStatusCodes.UNSUB_SUCCESS_PENDING ){
				try{
					JocgChargingHandler chHand=(JocgChargingHandler)Class.forName(newReq.getChargingHandlerClass()).newInstance();
					logger.debug(logHeader+" chHand  "+chHand);
					dctResp=chHand.processDeactivation(newReq, requestContext);
					logger.debug(logHeader+" dctResp  "+dctResp);
				}catch(Exception e){
					dctResp.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
					dctResp.setStatusDescp("Exception "+e);
				}
			}
			
			//DB Transactions
			try{
				//Set JocgResponse Object for Status Update
				JocgResponse resp=new JocgResponse();
				resp.setResponseCode(dctResp.getStatusCode().intValue());
				resp.setResponseDescription(dctResp.getStatusDescp());
				resp.setRespType(JocgResponse.RESPTYPE_TEXT);
				newReq.setResponseObject(resp);
				if(dctResp.getStatusCode()==JocgStatusCodes.UNSUB_SUCCESS && sub!=null && sub.getId()!=null){
					sub.setStatus(dctResp.getStatusCode());
					sub.setStatusDescp(dctResp.getStatusDescp());
					
					Calendar cal=Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					java.util.Date cdate=cal.getTime();
					java.util.Date subDate=sub.getSubChargedDate();
					
					sub.setUnsubDate(cal.getTime());
					cal.add(Calendar.MINUTE, -20);
					if(cal.getTime().before(subDate)){
						newReq.setChurn20Min(true);
				
					}
					cal.setTimeInMillis(System.currentTimeMillis());
					if(cal.getTime().getDate()==subDate.getDate()){
						newReq.setChurnSameDay(true);;
					}
					this.updateSubscriber(logHeader, sub, false);
					logger.debug(logHeader+" Subscriber Unsubscribed Successfully  ");
				}else{
					logger.debug(logHeader+" Unsub request Status   "+resp.getResponseCode()+", Descp "+resp.getResponseDescription());
				}
				
				ChargingCDR newCDR=cdrBuilder.buildCDR(newReq,requestContext);
				logger.debug(logHeader+"CDR Object "+newCDR);
				
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
		return dctResp;
		
	}
	

	
	
	
	
}
