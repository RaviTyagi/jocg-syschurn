package com.jss.jocg;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.data.JocgCachedObjects;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
  entityManagerFactoryRef = "jocgcacheEntityManagerFactory",
  transactionManagerRef = "jocgcacheTransactionManager",
  basePackages = { "com.jss.jocg.cache.dao" }
)
public class JocgCacheConfigurations {

	 @Primary
	@Bean(name = "jocgcacheDataSource")
	  @ConfigurationProperties(prefix = "spring.datasource")
	  public DataSource dataSource() {
	    return DataSourceBuilder.create().build();
	  }

	 @Primary
	@Bean(name = "jocgcacheEntityManagerFactory")
	  public LocalContainerEntityManagerFactoryBean 
	  jocgcacheEntityManagerFactory(
	    EntityManagerFactoryBuilder builder,
	    @Qualifier("jocgcacheDataSource") DataSource dataSource
	  ) {
	    return
	      builder
	        .dataSource(dataSource)
	        .packages("com.jss.jocg.cache.entities.config")
	        .persistenceUnit("jocgcache")
	        .build();
	  }
	
	 @Primary
	@Bean(name = "jocgcacheTransactionManager")
	  public PlatformTransactionManager jocgcacheTransactionManager(
	    @Qualifier("jocgcacheEntityManagerFactory") EntityManagerFactory
	    jocgcdrEntityManagerFactory
	  ) {
	    return new JpaTransactionManager(jocgcdrEntityManagerFactory);
	  }
	
	/**
	 * Platform Bean configurations starts here
	 * @return
	 */
	@Bean
	public JocgCacheManager JocgCache(){
		return new JocgCacheManager();
	}
	
	@Bean
	public JocgCachedObjects JocgCachedObjects(){
		return new JocgCachedObjects();
	}
}
