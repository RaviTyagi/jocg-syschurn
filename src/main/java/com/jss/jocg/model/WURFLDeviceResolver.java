package com.jss.jocg.model;

import net.sourceforge.wurfl.core.Device;
import net.sourceforge.wurfl.core.WURFLManager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Makes use of the WURFL tools to resolve the device.
 */
@Service
public class WURFLDeviceResolver implements DeviceResolver {

   // @Resource
    //private WURFLManager manager;
	@Value("${jocg.wurfl.location}")
	String wurflResourceFile;
	
    @Override
    public DeviceInfo resolveDevice(HttpServletRequest request) {

        // Spring caters for the elementary information (is this a mobile?)
        /*boolean mobile = DeviceUtils.getCurrentDevice(request).isMobile();

        // Look for site preferences
        SitePreference sitePreference = SitePreferenceUtils.getCurrentSitePreference(request);

        // Decide the type of the device and how to display the device
        DeviceInfo deviceInfo = new DeviceInfo(mobile);
        deviceInfo.setDisplayNormal(SitePreference.NORMAL.equals(sitePreference));

        // Other properties are resolved using the WURFL capabilities
        Device device = manager.getDeviceForRequest(request);

        deviceInfo.setId(device.getId());

        Map<String,Object> capabilities=device.getCapabilities();
        deviceInfo.setMarkUp(device.getMarkUp().toString());
        deviceInfo.setUserAgent(device.getUserAgent());
        deviceInfo.setRowCount(getIntValue(capabilities.get(HandsetCapabilities.KEY_ROWS), 0));
        deviceInfo.setColumnCount(getIntValue(capabilities.get(HandsetCapabilities.KEY_COLUMNS), 0));
        deviceInfo.setBrand(getStringValue(capabilities.get(HandsetCapabilities.KEY_BRAND), "NA"));
        deviceInfo.setModel(getStringValue(capabilities.get(HandsetCapabilities.KEY_MODEL), "NA"));
        deviceInfo.setViewName(device.getMarkUp().toString());

        deviceInfo.setOsName(getStringValue(capabilities.get(HandsetCapabilities.KEY_DEVICE_OS), "NA"));
        deviceInfo.setOsVersion(getStringValue(capabilities.get(HandsetCapabilities.KEY_DEVICE_OS_VERSION), "NA"));
        deviceInfo.setMktName(getStringValue(capabilities.get(HandsetCapabilities.KEY_MARKETING_NAME), "NA"));
        deviceInfo.setWireless(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_IS_WIRELESS), false));
        deviceInfo.setTablet(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_IS_TABLET), false));
        deviceInfo.setWebSupport(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_WEB_SUPPORT), false));

        deviceInfo.setBrowserName(getStringValue(capabilities.get(HandsetCapabilities.KEY_MOBILE_BROWSER), "NA"));
        deviceInfo.setBrowserVersion(getStringValue(capabilities.get(HandsetCapabilities.KEY_MOBILE_BROWSER_VERSION), "NA"));
        deviceInfo.setXhtmlSupportLevel(getStringValue(capabilities.get(HandsetCapabilities.KEY_XHTML_SUPPORT_LEVEL), "NA"));
        deviceInfo.setNokiaSeries(getStringValue(capabilities.get(HandsetCapabilities.KEY_NOKIA_SERIES), "NA"));
        deviceInfo.setNokiaEdition(getStringValue(capabilities.get(HandsetCapabilities.KEY_NOKIA_EDITION), "NA"));

        deviceInfo.setNokiaFeaturePhone(getStringValue(capabilities.get(HandsetCapabilities.KEY_NOKIA_FEATURE_PACK), "NA"));
        deviceInfo.setSupportMIDI(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_MIDI_MONOPHONIC), false));
        deviceInfo.setSupportWAV(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_WAV), false));
        deviceInfo.setSupportMP3(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_MP3), false));
        deviceInfo.setSupportAMR(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_AMR), false));
        deviceInfo.setSupportMONO(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_MIDI_MONOPHONIC), false));
        deviceInfo.setSupportIMELODY(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_IMELODY), false));

        deviceInfo.setSupportJPG(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_JPG), false));
        deviceInfo.setSupportGIF(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_GIF), false));
        deviceInfo.setSupportPNG(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_PNG), false));
        deviceInfo.setSupportBMP(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_BMP), false));
        deviceInfo.setSupportWBMP(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_WBMP), false));
        deviceInfo.setSupportMP4Playback(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_PLAYBACK_MP4), false));
        deviceInfo.setSupport3GPPlayback(getBooleanValue(capabilities.get(HandsetCapabilities.KEY_PLAYBACK_3GPP), false));
        deviceInfo.setvXMLVersion(getStringValue(capabilities.get(HandsetCapabilities.KEY_SUPPORTS_VOICEXML), "NA"));

        return deviceInfo;*/
    	return new DeviceInfo(true);
    }

    public int getIntValue(Object value,int defa){
    	int retrVal=defa;
    	try{
    		String valStr=(String)value;
    		retrVal=Integer.parseInt(valStr);
    	}catch(Exception e){
    		retrVal=defa;
    	}
    	return retrVal;
    }

    public String getStringValue(Object value,String defa){
    	String retrVal=defa;
    	try{
    		String valStr=(String)value;
    		retrVal=(String)value;
    	}catch(Exception e){
    		retrVal=defa;
    	}
    	return retrVal;
    }
    public boolean getBooleanValue(Object value,boolean defa){
    	boolean retrVal=defa;
    	try{
    		String valStr=(String)value;
    		retrVal=Boolean.parseBoolean(valStr);
    	}catch(Exception e){
    		retrVal=defa;
    	}
    	return retrVal;
    }
}
