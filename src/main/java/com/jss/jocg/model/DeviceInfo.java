package com.jss.jocg.model;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Map;

/**
 * Provides a vendor-independent description of a device.
 */
public class DeviceInfo implements Device {

    private boolean mobile;

    private boolean displayNormal;

    private String id;

    private String userAgent;

    private String markUp;

    int width;
    int height;
    int rowCount;
    int columnCount;
    String viewName;
    String brand;
    String model;
    String osName;
    String osVersion;
    String mktName;
    boolean wireless;
    boolean tablet;
    boolean webSupport;
    String browserName;
    String browserVersion;
    String xhtmlSupportLevel;
    String nokiaSeries;
    String nokiaEdition;
    String nokiaFeaturePhone;
    String uaProff;
    boolean supportMIDI;
    boolean supportWAV;
    boolean supportMP3;
    boolean supportAMR;
    boolean supportMONO;
    boolean supportIMELODY;
    boolean supportJPG;
    boolean supportGIF;
    boolean supportPNG;
    boolean supportBMP;
    boolean supportWBMP;
    boolean supportMP4Playback;
    boolean support3GPPlayback;
    String vXMLVersion;

    @Override
    public String toString(){
        StringBuilder sb=new StringBuilder();
        Field[] fields = this.getClass().getDeclaredFields();
        sb.append(this.getClass().getName()).append("|");
        try {
            for (Field field : fields) {
                sb.append(field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        sb.append("|");
        return sb.toString();
    }


    public DeviceInfo(boolean mobile) {
        this.mobile = mobile;
    }

    @Override
    public boolean isMobile() {
        return mobile;
    }

    public boolean isDisplayNormal() {
        return displayNormal;
    }

    public void setDisplayNormal(boolean displayNormal) {
        this.displayNormal = displayNormal;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getMarkUp() {
        return markUp;
    }

    public void setMarkUp(String markUp) {
        this.markUp = markUp;
    }


	@Override
	public DevicePlatform getDevicePlatform() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isNormal() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isTablet() {
		// TODO Auto-generated method stub
		return false;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getRowCount() {
		return rowCount;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getColumnCount() {
		return columnCount;
	}

	public void setColumnCount(int columnCount) {
		this.columnCount = columnCount;
	}

	public String getViewName() {
		return viewName;
	}

	public void setViewName(String viewName) {
		this.viewName = viewName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public String getOsVersion() {
		return osVersion;
	}

	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	public String getMktName() {
		return mktName;
	}

	public void setMktName(String mktName) {
		this.mktName = mktName;
	}

	public boolean isWireless() {
		return wireless;
	}

	public void setWireless(boolean wireless) {
		this.wireless = wireless;
	}

	public boolean isWebSupport() {
		return webSupport;
	}

	public void setWebSupport(boolean webSupport) {
		this.webSupport = webSupport;
	}

	public String getBrowserName() {
		return browserName;
	}

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	public String getXhtmlSupportLevel() {
		return xhtmlSupportLevel;
	}

	public void setXhtmlSupportLevel(String xhtmlSupportLevel) {
		this.xhtmlSupportLevel = xhtmlSupportLevel;
	}

	public String getNokiaSeries() {
		return nokiaSeries;
	}

	public void setNokiaSeries(String nokiaSeries) {
		this.nokiaSeries = nokiaSeries;
	}

	public String getNokiaEdition() {
		return nokiaEdition;
	}

	public void setNokiaEdition(String nokiaEdition) {
		this.nokiaEdition = nokiaEdition;
	}

	public String getNokiaFeaturePhone() {
		return nokiaFeaturePhone;
	}

	public void setNokiaFeaturePhone(String nokiaFeaturePhone) {
		this.nokiaFeaturePhone = nokiaFeaturePhone;
	}

	public String getUaProff() {
		return uaProff;
	}

	public void setUaProff(String uaProff) {
		this.uaProff = uaProff;
	}

	public boolean isSupportMIDI() {
		return supportMIDI;
	}

	public void setSupportMIDI(boolean supportMIDI) {
		this.supportMIDI = supportMIDI;
	}

	public boolean isSupportWAV() {
		return supportWAV;
	}

	public void setSupportWAV(boolean supportWAV) {
		this.supportWAV = supportWAV;
	}

	public boolean isSupportMP3() {
		return supportMP3;
	}

	public void setSupportMP3(boolean supportMP3) {
		this.supportMP3 = supportMP3;
	}

	public boolean isSupportAMR() {
		return supportAMR;
	}

	public void setSupportAMR(boolean supportAMR) {
		this.supportAMR = supportAMR;
	}

	public boolean isSupportMONO() {
		return supportMONO;
	}

	public void setSupportMONO(boolean supportMONO) {
		this.supportMONO = supportMONO;
	}

	public boolean isSupportIMELODY() {
		return supportIMELODY;
	}

	public void setSupportIMELODY(boolean supportIMELODY) {
		this.supportIMELODY = supportIMELODY;
	}

	public boolean isSupportJPG() {
		return supportJPG;
	}

	public void setSupportJPG(boolean supportJPG) {
		this.supportJPG = supportJPG;
	}

	public boolean isSupportGIF() {
		return supportGIF;
	}

	public void setSupportGIF(boolean supportGIF) {
		this.supportGIF = supportGIF;
	}

	public boolean isSupportPNG() {
		return supportPNG;
	}

	public void setSupportPNG(boolean supportPNG) {
		this.supportPNG = supportPNG;
	}

	public boolean isSupportBMP() {
		return supportBMP;
	}

	public void setSupportBMP(boolean supportBMP) {
		this.supportBMP = supportBMP;
	}

	public boolean isSupportWBMP() {
		return supportWBMP;
	}

	public void setSupportWBMP(boolean supportWBMP) {
		this.supportWBMP = supportWBMP;
	}

	public boolean isSupportMP4Playback() {
		return supportMP4Playback;
	}

	public void setSupportMP4Playback(boolean supportMP4Playback) {
		this.supportMP4Playback = supportMP4Playback;
	}

	public boolean isSupport3GPPlayback() {
		return support3GPPlayback;
	}

	public void setSupport3GPPlayback(boolean support3gpPlayback) {
		support3GPPlayback = support3gpPlayback;
	}

	public String getvXMLVersion() {
		return vXMLVersion;
	}

	public void setvXMLVersion(String vXMLVersion) {
		this.vXMLVersion = vXMLVersion;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

	public void setTablet(boolean tablet) {
		this.tablet = tablet;
	}




}
