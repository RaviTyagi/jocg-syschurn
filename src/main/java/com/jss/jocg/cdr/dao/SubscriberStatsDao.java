package com.jss.jocg.cdr.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.jss.jocg.cdr.entities.SubscriberStats;


public interface SubscriberStatsDao  extends JpaRepository<SubscriberStats, Long> {
	
	@Query("select c from SubscriberStats c where date(c.datestr)=date(:dateStr)")
	public List<SubscriberStats> findByDatestr(@Param("dateStr") java.util.Date dateStr);
	
	@Transactional
	@Modifying
	@Query(value="delete from SubscriberStats c where c.datestr between ?1 and ?2")
	public void deleteByDatestrBetween(java.util.Date dateStr, java.util.Date dateStrEnd);
}
