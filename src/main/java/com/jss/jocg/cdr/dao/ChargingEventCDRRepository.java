package com.jss.jocg.cdr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jss.jocg.cdr.entities.ChargingEventCDR;


public interface ChargingEventCDRRepository extends JpaRepository<ChargingEventCDR, String> {

}
