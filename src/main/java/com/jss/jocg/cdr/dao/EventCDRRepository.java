package com.jss.jocg.cdr.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jss.jocg.cdr.entities.EventCDR;

public interface EventCDRRepository extends JpaRepository<EventCDR, String> {

}
