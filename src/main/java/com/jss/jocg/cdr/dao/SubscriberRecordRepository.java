package com.jss.jocg.cdr.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.jss.jocg.cdr.entities.SubscriberRecord;

public interface SubscriberRecordRepository extends JpaRepository<SubscriberRecord, String>{

}
