package com.jss.jocg.cdr.entities;

import java.io.Serializable;
import java.lang.reflect.Field;

import javax.persistence.*;
import java.util.Date;



/**
 * The persistent class for the tb_jocgevent_cdr database table.
 * 
 */
@Entity
@Table(name="tb_jocgevent_cdr")
@NamedQuery(name="EventCDR.findAll", query="SELECT e FROM EventCDR e")
public class EventCDR implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private byte activatedFromGrace;
	private byte activatedFromParking;
	private byte activatedFromZero;
	private int aggregatorId;
	private String aggregatorName;
	private int campaignId;
	private String campaignName;
	private Date cgResponseTime;
	private String cgStatusCode;
	private String cgStatusDescp;
	private String cgTransactionId;
	private double chargedAmount;
	private String chargedOperatorKey;
	private String chargedOperatorParams;
	private int chargedPpid;
	private double chargedPricePoint;
	private String chargedValidityPeriod;
	private String chargingCategory;
	private Date chargingDate;
	private int chargingInterfaceId;
	private String chargingInterfaceName;
	private String chargingInterfaceType;
	private int chImageId;
	private byte churn20Min;
	private byte churnSameDay;
	private byte churn24Hour;
	private String circleCode;
	private int circleId;
	private String circleName;
	private String contentType;
	private String countryName;
	private String currencyName;
	private String deviceDetails;
	private String deviceOS;
	private double discountGiven;
	private byte fallbackCharged;
	private String gmtDiff;
	private String jocgTransId;
	private int landingPageId;
	private int landingPageScheduleId;
	private Date lastUpdated;
	private String mppOperatorKey;
	private String mppOperatorParams;
	private Long msisdn;
	private byte notifyChurn;
	private Integer notifyChurnStatus;
	private String notifyDescp;
	private Date notifyScheduleTime;
	private int notifyStatus;
	private Date notifyTime;
	private Date notifyTimeChurn;
	private double operatorPricePoint;
	private String otpPin;
	private int otpStatus;
	private int packageChintfId;
	private int packageId;
	private String packageName;
	private double packagePrice;
	private Date parkingDate;
	private Date parkingToActivationDate;
	private String platformName;
	private int pricePointId;
	private String publisherId;
	private String referer;
	private String renewalCategory;
	private byte renewalRequired;
	private byte reportGenerated;
	private String requestCategory;
	private Date requestDate;
	private String requestType;
	private int routingScheduleId;
	private double rqAmountToBeCharged;
	private String rqNetworkType;
	private String rqPackageName;
	private double rqPackagePrice;
	private int rqPackageValidity;
	private String rqVoucherCode;
	private String sdpLifeCycleId;
	private Date sdpResponseTime;
	private String sdpStatusCode;
	private String sdpStatusDescp;
	private String sdpTransactionId;
	private String serviceCategory;
	private int serviceId;
	private String serviceName;
	private String sourceNetworkCode;
	private int sourceNetworkId;
	private String sourceNetworkName;
	private int status;
	private String statusDescp;
	private String subRegId;
	private String subscriberId;
	private String systemId;
	private int trafficSourceId;
	private String trafficSourceName;
	private String trafficSourceToken;
	private byte useParentHandler;
	private String userAgent;
	private String validityCategory;
	private Date validityEndDate;
	private int validityPeriod;
	private Date validityStartTime;
	private Date subRegDate;
	private String validityUnit;
	private double voucherDiscount;
	private String voucherDiscountType;
	private Long voucherId;
	private String voucherName;
	private String voucherType;
	private int voucherVendorId;
	private String voucherVendorName;
	private Integer cgImageId;

	public EventCDR() {
	}
	public EventCDR(String id,String subRegId,Integer landingPageScheduleId,Integer routingScheduleId,String jocgTransId,String requestCategory,String requestType,String subscriberId,Long msisdn,String platformName,String rqPackageName,Double rqPackagePrice,Integer rqPackageValidity,String rqNetworkType,String rqVoucherVendor,Double rqAmountToBeCharged,String rqVoucherCode,String systemId,Integer sourceNetworkId,String sourceNetworkName,String sourceNetworkCode,Integer aggregatorId,String aggregatorName,Boolean useParentHandler,Integer chargingInterfaceId,String chargingInterfaceName,String countryName,String currencyName,String gmtDiff,String chargingInterfaceType,Integer packageChintfId,Integer circleId,String circleName,String circleCode,Integer packageId,String packageName,Double packagePrice,Integer serviceId,String serviceName,String serviceCategory,Integer pricePointId,Double operatorPricePoint,String validityCategory,String validityUnit,Integer validityPeriod,String mppOperatorKey,String mppOperatorParams,Integer chargedPpid,Double chargedPricePoint,Boolean fallbackCharged,String chargedValidityPeriod,String chargedOperatorKey,String chargedOperatorParams,Long voucherId,String voucherName,Integer voucherVendorId,String voucherVendorName,String voucherType,Double voucherDiscount,String voucherDiscountType,String chargingCategory,Double discountGiven,Double chargedAmount,Boolean renewalRequired,String renewalCategory,java.util.Date chargingDate,java.util.Date validityStartDate,java.util.Date validityEndDate,String deviceDetails,String userAgent,String referer,Integer trafficSourceId,String trafficSourceName,String trafficSourceToken,Integer campaignId,String campaignName,String contentType,String publisherId,Integer notifyStatus,String notifyDescp,java.util.Date notifyTime,java.util.Date notifyScheduleTime,Boolean churn20Min,Boolean churnSameDay,Boolean notifyChurn,Integer notifyChurnStatus,java.util.Date notifyTimeChurn,Integer landingPageId,Integer cgImageId,Integer otpStatus,String otpPin,Integer status,String statusDescp,java.util.Date requestDate,String cgStatusCode,String cgStatusDescp,String cgTransactionId,java.util.Date cgResponseTime,String sdpStatusCode,String sdpStatusDescp,String sdpTransactionId,String sdpLifeCycleId,java.util.Date sdpResponseTime,java.util.Date lastUpdated,Boolean reportGenerated,Boolean activatedFromZero,Boolean activatedFromGrace,Boolean activatedFromParking,Date subRegDate,Boolean churn24Hour){
		this.subRegDate=subRegDate;
		this.subRegId=subRegId;
		this.landingPageScheduleId=landingPageScheduleId;
		this.routingScheduleId=routingScheduleId;
		this.sdpLifeCycleId=sdpLifeCycleId;
		this.jocgTransId=jocgTransId;
		this.requestCategory=requestCategory;
		this.requestType=requestType;
		this.subscriberId=subscriberId;
		this.msisdn=msisdn;
		this.platformName=platformName;
		this.rqPackageName=rqPackageName;
		this.rqPackagePrice=rqPackagePrice;
		this.rqPackageValidity=rqPackageValidity;
		this.rqNetworkType=rqNetworkType;
		//this.rqVoucherVendor=rqVoucherVendor;
		this.rqAmountToBeCharged=rqAmountToBeCharged;
		this.rqVoucherCode=rqVoucherCode;
		this.systemId=systemId;
		this.sourceNetworkId=sourceNetworkId;
		this.sourceNetworkName=sourceNetworkName;
		this.sourceNetworkCode=sourceNetworkCode;
		this.aggregatorId=aggregatorId;
		this.aggregatorName=aggregatorName;
		this.useParentHandler=(byte)((useParentHandler==true)?1:0);
		this.chargingInterfaceId=chargingInterfaceId;
		this.chargingInterfaceName=chargingInterfaceName;
		this.countryName=countryName;
		this.currencyName=currencyName;
		this.gmtDiff=gmtDiff;
		this.chargingInterfaceType=chargingInterfaceType;
		this.packageChintfId=packageChintfId;
		this.circleId=circleId;
		this.circleName=circleName;
		this.circleCode=circleCode;
		this.packageId=packageId;
		this.packageName=packageName;
		this.packagePrice=packagePrice;
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.serviceCategory=serviceCategory;
		this.pricePointId=pricePointId;
		this.operatorPricePoint=operatorPricePoint;
		this.validityCategory=validityCategory;
		this.validityUnit=validityUnit;
		this.validityPeriod=validityPeriod;
		this.mppOperatorKey=mppOperatorKey;
		this.mppOperatorParams=mppOperatorParams;
		this.chargedPpid=chargedPpid;
		this.chargedPricePoint=chargedPricePoint;
		this.fallbackCharged=(byte)(fallbackCharged==true?1:0);
		this.chargedValidityPeriod=chargedValidityPeriod;
		this.chargedOperatorKey=chargedOperatorKey;
		this.chargedOperatorParams=chargedOperatorParams;
		this.voucherId=voucherId;
		this.voucherName=voucherName;
		this.voucherVendorId=voucherVendorId;
		this.voucherVendorName=voucherVendorName;
		this.voucherType=voucherType;
		this.voucherDiscount=voucherDiscount;
		this.voucherDiscountType=voucherDiscountType;
		this.chargingCategory=chargingCategory;
		this.discountGiven=discountGiven;
		this.chargedAmount=chargedAmount;
		this.renewalRequired=(byte)(renewalRequired==true?1:0);
		this.renewalCategory=renewalCategory;
		this.chargingDate=chargingDate;
		this.validityStartTime=validityStartDate;
		this.validityEndDate=validityEndDate;
		this.deviceDetails=deviceDetails;
		this.userAgent=userAgent;
		this.referer=referer;
		this.trafficSourceId=trafficSourceId;
		this.trafficSourceName=trafficSourceName;
		this.trafficSourceToken=trafficSourceToken;
		this.campaignId=campaignId;
		this.campaignName=campaignName;
		this.contentType=contentType;
		this.publisherId=publisherId;
		this.notifyStatus=notifyStatus;
		this.notifyDescp=notifyDescp;
		this.notifyTime=notifyTime;
		this.notifyScheduleTime=notifyScheduleTime;
		this.churn20Min=(byte)(churn20Min==true?1:0);
		this.churnSameDay=(byte)(churnSameDay==true?1:0);
		this.notifyChurn=(byte)(notifyChurn==true?1:0);
		this.churn24Hour=(byte)(churn24Hour==true?1:0);
		this.notifyChurnStatus=notifyChurnStatus;
		this.notifyTimeChurn=notifyTimeChurn;
		this.landingPageId=landingPageId;
		this.cgImageId=cgImageId;
		this.otpStatus=otpStatus;
		this.otpPin=otpPin;
		this.status=status;
		this.statusDescp=statusDescp;
		this.requestDate=requestDate;
		this.cgStatusCode=cgStatusCode;
		this.cgStatusDescp=cgStatusDescp;
		this.cgTransactionId=cgTransactionId;
		this.cgResponseTime=cgResponseTime;
		this.sdpStatusCode=sdpStatusCode;
		this.sdpStatusDescp=sdpStatusDescp;
		this.sdpTransactionId=sdpTransactionId;
		this.sdpResponseTime=sdpResponseTime;
		this.lastUpdated=lastUpdated;
		this.reportGenerated=(byte)(reportGenerated==true?1:0);
		this.activatedFromZero=(byte)((activatedFromZero==null ||activatedFromZero==false)?0:1);
		this.activatedFromGrace=(byte)((activatedFromGrace==null ||activatedFromGrace==false)?0:1);
		this.activatedFromParking=(byte)((activatedFromParking==null ||activatedFromParking==false)?0:1);
	}

public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }

	@Id
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public byte getActivatedFromGrace() {
		return this.activatedFromGrace;
	}

	public void setActivatedFromGrace(byte activatedFromGrace) {
		this.activatedFromGrace = activatedFromGrace;
	}


	public byte getActivatedFromParking() {
		return this.activatedFromParking;
	}

	public void setActivatedFromParking(byte activatedFromParking) {
		this.activatedFromParking = activatedFromParking;
	}


	public byte getActivatedFromZero() {
		return this.activatedFromZero;
	}

	public void setActivatedFromZero(byte activatedFromZero) {
		this.activatedFromZero = activatedFromZero;
	}


	public int getAggregatorId() {
		return this.aggregatorId;
	}

	public void setAggregatorId(int aggregatorId) {
		this.aggregatorId = aggregatorId;
	}


	public String getAggregatorName() {
		return this.aggregatorName;
	}

	public void setAggregatorName(String aggregatorName) {
		this.aggregatorName = aggregatorName;
	}


	public int getCampaignId() {
		return this.campaignId;
	}

	public void setCampaignId(int campaignId) {
		this.campaignId = campaignId;
	}


	public String getCampaignName() {
		return this.campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getCgResponseTime() {
		return this.cgResponseTime;
	}

	public void setCgResponseTime(Date cgResponseTime) {
		this.cgResponseTime = cgResponseTime;
	}


	public String getCgStatusCode() {
		return this.cgStatusCode;
	}

	public void setCgStatusCode(String cgStatusCode) {
		this.cgStatusCode = cgStatusCode;
	}


	public String getCgStatusDescp() {
		return this.cgStatusDescp;
	}

	public void setCgStatusDescp(String cgStatusDescp) {
		this.cgStatusDescp = cgStatusDescp;
	}


	public String getCgTransactionId() {
		return this.cgTransactionId;
	}

	public void setCgTransactionId(String cgTransactionId) {
		this.cgTransactionId = cgTransactionId;
	}


	public double getChargedAmount() {
		return this.chargedAmount;
	}

	public void setChargedAmount(double chargedAmount) {
		this.chargedAmount = chargedAmount;
	}


	public String getChargedOperatorKey() {
		return this.chargedOperatorKey;
	}

	public void setChargedOperatorKey(String chargedOperatorKey) {
		this.chargedOperatorKey = chargedOperatorKey;
	}


	public String getChargedOperatorParams() {
		return this.chargedOperatorParams;
	}

	public void setChargedOperatorParams(String chargedOperatorParams) {
		this.chargedOperatorParams = chargedOperatorParams;
	}


	public int getChargedPpid() {
		return this.chargedPpid;
	}

	public void setChargedPpid(int chargedPpid) {
		this.chargedPpid = chargedPpid;
	}


	public double getChargedPricePoint() {
		return this.chargedPricePoint;
	}

	public void setChargedPricePoint(double chargedPricePoint) {
		this.chargedPricePoint = chargedPricePoint;
	}


	public String getChargedValidityPeriod() {
		return this.chargedValidityPeriod;
	}

	public void setChargedValidityPeriod(String chargedValidityPeriod) {
		this.chargedValidityPeriod = chargedValidityPeriod;
	}


	public String getChargingCategory() {
		return this.chargingCategory;
	}

	public void setChargingCategory(String chargingCategory) {
		this.chargingCategory = chargingCategory;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getChargingDate() {
		return this.chargingDate;
	}

	public void setChargingDate(Date chargingDate) {
		this.chargingDate = chargingDate;
	}


	public int getChargingInterfaceId() {
		return this.chargingInterfaceId;
	}

	public void setChargingInterfaceId(int chargingInterfaceId) {
		this.chargingInterfaceId = chargingInterfaceId;
	}


	public String getChargingInterfaceName() {
		return this.chargingInterfaceName;
	}

	public void setChargingInterfaceName(String chargingInterfaceName) {
		this.chargingInterfaceName = chargingInterfaceName;
	}


	public String getChargingInterfaceType() {
		return this.chargingInterfaceType;
	}

	public void setChargingInterfaceType(String chargingInterfaceType) {
		this.chargingInterfaceType = chargingInterfaceType;
	}


	public int getChImageId() {
		return this.chImageId;
	}

	public void setChImageId(int chImageId) {
		this.chImageId = chImageId;
	}


	public byte getChurn20Min() {
		return this.churn20Min;
	}

	public void setChurn20Min(byte churn20Min) {
		this.churn20Min = churn20Min;
	}


	public byte getChurnSameDay() {
		return this.churnSameDay;
	}

	public void setChurnSameDay(byte churnSameDay) {
		this.churnSameDay = churnSameDay;
	}


	public String getCircleCode() {
		return this.circleCode;
	}

	public void setCircleCode(String circleCode) {
		this.circleCode = circleCode;
	}


	public int getCircleId() {
		return this.circleId;
	}

	public void setCircleId(int circleId) {
		this.circleId = circleId;
	}


	public String getCircleName() {
		return this.circleName;
	}

	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}


	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}


	public String getCountryName() {
		return this.countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}


	public String getCurrencyName() {
		return this.currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}


	public String getDeviceDetails() {
		return this.deviceDetails;
	}

	public void setDeviceDetails(String deviceDetails) {
		this.deviceDetails = deviceDetails;
	}


	public String getDeviceOS() {
		return this.deviceOS;
	}

	public void setDeviceOS(String deviceOS) {
		this.deviceOS = deviceOS;
	}


	public double getDiscountGiven() {
		return this.discountGiven;
	}

	public void setDiscountGiven(double discountGiven) {
		this.discountGiven = discountGiven;
	}


	public byte getFallbackCharged() {
		return this.fallbackCharged;
	}

	public void setFallbackCharged(byte fallbackCharged) {
		this.fallbackCharged = fallbackCharged;
	}


	public String getGmtDiff() {
		return this.gmtDiff;
	}

	public void setGmtDiff(String gmtDiff) {
		this.gmtDiff = gmtDiff;
	}


	public String getJocgTransId() {
		return this.jocgTransId;
	}

	public void setJocgTransId(String jocgTransId) {
		this.jocgTransId = jocgTransId;
	}


	public int getLandingPageId() {
		return this.landingPageId;
	}

	public void setLandingPageId(int landingPageId) {
		this.landingPageId = landingPageId;
	}


	public int getLandingPageScheduleId() {
		return this.landingPageScheduleId;
	}

	public void setLandingPageScheduleId(int landingPageScheduleId) {
		this.landingPageScheduleId = landingPageScheduleId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdated() {
		return this.lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}


	public String getMppOperatorKey() {
		return this.mppOperatorKey;
	}

	public void setMppOperatorKey(String mppOperatorKey) {
		this.mppOperatorKey = mppOperatorKey;
	}


	public String getMppOperatorParams() {
		return this.mppOperatorParams;
	}

	public void setMppOperatorParams(String mppOperatorParams) {
		this.mppOperatorParams = mppOperatorParams;
	}


	public Long getMsisdn() {
		return this.msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}


	public byte getNotifyChurn() {
		return this.notifyChurn;
	}

	public void setNotifyChurn(byte notifyChurn) {
		this.notifyChurn = notifyChurn;
	}


	public Integer getNotifyChurnStatus() {
		return this.notifyChurnStatus;
	}

	public void setNotifyChurnStatus(Integer notifyChurnStatus) {
		this.notifyChurnStatus = notifyChurnStatus;
	}


	public String getNotifyDescp() {
		return this.notifyDescp;
	}

	public void setNotifyDescp(String notifyDescp) {
		this.notifyDescp = notifyDescp;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getNotifyScheduleTime() {
		return this.notifyScheduleTime;
	}

	public void setNotifyScheduleTime(Date notifyScheduleTime) {
		this.notifyScheduleTime = notifyScheduleTime;
	}


	public int getNotifyStatus() {
		return this.notifyStatus;
	}

	public void setNotifyStatus(int notifyStatus) {
		this.notifyStatus = notifyStatus;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getNotifyTime() {
		return this.notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getNotifyTimeChurn() {
		return this.notifyTimeChurn;
	}

	public void setNotifyTimeChurn(Date notifyTimeChurn) {
		this.notifyTimeChurn = notifyTimeChurn;
	}


	public double getOperatorPricePoint() {
		return this.operatorPricePoint;
	}

	public void setOperatorPricePoint(double operatorPricePoint) {
		this.operatorPricePoint = operatorPricePoint;
	}


	public String getOtpPin() {
		return this.otpPin;
	}

	public void setOtpPin(String otpPin) {
		this.otpPin = otpPin;
	}


	public int getOtpStatus() {
		return this.otpStatus;
	}

	public void setOtpStatus(int otpStatus) {
		this.otpStatus = otpStatus;
	}


	public int getPackageChintfId() {
		return this.packageChintfId;
	}

	public void setPackageChintfId(int packageChintfId) {
		this.packageChintfId = packageChintfId;
	}


	public int getPackageId() {
		return this.packageId;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}


	public String getPackageName() {
		return this.packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}


	public double getPackagePrice() {
		return this.packagePrice;
	}

	public void setPackagePrice(double packagePrice) {
		this.packagePrice = packagePrice;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getParkingDate() {
		return this.parkingDate;
	}

	public void setParkingDate(Date parkingDate) {
		this.parkingDate = parkingDate;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getParkingToActivationDate() {
		return this.parkingToActivationDate;
	}

	public void setParkingToActivationDate(Date parkingToActivationDate) {
		this.parkingToActivationDate = parkingToActivationDate;
	}


	public String getPlatformName() {
		return this.platformName;
	}

	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}


	public int getPricePointId() {
		return this.pricePointId;
	}

	public void setPricePointId(int pricePointId) {
		this.pricePointId = pricePointId;
	}


	public String getPublisherId() {
		return this.publisherId;
	}

	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}


	public String getReferer() {
		return this.referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}


	public String getRenewalCategory() {
		return this.renewalCategory;
	}

	public void setRenewalCategory(String renewalCategory) {
		this.renewalCategory = renewalCategory;
	}


	public byte getRenewalRequired() {
		return this.renewalRequired;
	}

	public void setRenewalRequired(byte renewalRequired) {
		this.renewalRequired = renewalRequired;
	}


	public byte getReportGenerated() {
		return this.reportGenerated;
	}

	public void setReportGenerated(byte reportGenerated) {
		this.reportGenerated = reportGenerated;
	}


	public String getRequestCategory() {
		return this.requestCategory;
	}

	public void setRequestCategory(String requestCategory) {
		this.requestCategory = requestCategory;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getRequestDate() {
		return this.requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}


	public String getRequestType() {
		return this.requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}


	public int getRoutingScheduleId() {
		return this.routingScheduleId;
	}

	public void setRoutingScheduleId(int routingScheduleId) {
		this.routingScheduleId = routingScheduleId;
	}


	public double getRqAmountToBeCharged() {
		return this.rqAmountToBeCharged;
	}

	public void setRqAmountToBeCharged(double rqAmountToBeCharged) {
		this.rqAmountToBeCharged = rqAmountToBeCharged;
	}


	public String getRqNetworkType() {
		return this.rqNetworkType;
	}

	public void setRqNetworkType(String rqNetworkType) {
		this.rqNetworkType = rqNetworkType;
	}


	public String getRqPackageName() {
		return this.rqPackageName;
	}

	public void setRqPackageName(String rqPackageName) {
		this.rqPackageName = rqPackageName;
	}


	public double getRqPackagePrice() {
		return this.rqPackagePrice;
	}

	public void setRqPackagePrice(double rqPackagePrice) {
		this.rqPackagePrice = rqPackagePrice;
	}


	public int getRqPackageValidity() {
		return this.rqPackageValidity;
	}

	public void setRqPackageValidity(int rqPackageValidity) {
		this.rqPackageValidity = rqPackageValidity;
	}


	public String getRqVoucherCode() {
		return this.rqVoucherCode;
	}

	public void setRqVoucherCode(String rqVoucherCode) {
		this.rqVoucherCode = rqVoucherCode;
	}


	public String getSdpLifeCycleId() {
		return this.sdpLifeCycleId;
	}

	public void setSdpLifeCycleId(String sdpLifeCycleId) {
		this.sdpLifeCycleId = sdpLifeCycleId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getSdpResponseTime() {
		return this.sdpResponseTime;
	}

	public void setSdpResponseTime(Date sdpResponseTime) {
		this.sdpResponseTime = sdpResponseTime;
	}


	public String getSdpStatusCode() {
		return this.sdpStatusCode;
	}

	public void setSdpStatusCode(String sdpStatusCode) {
		this.sdpStatusCode = sdpStatusCode;
	}


	public String getSdpStatusDescp() {
		return this.sdpStatusDescp;
	}

	public void setSdpStatusDescp(String sdpStatusDescp) {
		this.sdpStatusDescp = sdpStatusDescp;
	}


	public String getSdpTransactionId() {
		return this.sdpTransactionId;
	}

	public void setSdpTransactionId(String sdpTransactionId) {
		this.sdpTransactionId = sdpTransactionId;
	}


	public String getServiceCategory() {
		return this.serviceCategory;
	}

	public void setServiceCategory(String serviceCategory) {
		this.serviceCategory = serviceCategory;
	}


	public int getServiceId() {
		return this.serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}


	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getSourceNetworkCode() {
		return this.sourceNetworkCode;
	}

	public void setSourceNetworkCode(String sourceNetworkCode) {
		this.sourceNetworkCode = sourceNetworkCode;
	}


	public int getSourceNetworkId() {
		return this.sourceNetworkId;
	}

	public void setSourceNetworkId(int sourceNetworkId) {
		this.sourceNetworkId = sourceNetworkId;
	}


	public String getSourceNetworkName() {
		return this.sourceNetworkName;
	}

	public void setSourceNetworkName(String sourceNetworkName) {
		this.sourceNetworkName = sourceNetworkName;
	}


	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public String getStatusDescp() {
		return this.statusDescp;
	}

	public void setStatusDescp(String statusDescp) {
		this.statusDescp = statusDescp;
	}


	public String getSubRegId() {
		return this.subRegId;
	}

	public void setSubRegId(String subRegId) {
		this.subRegId = subRegId;
	}


	public String getSubscriberId() {
		return this.subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}


	public String getSystemId() {
		return this.systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}


	public int getTrafficSourceId() {
		return this.trafficSourceId;
	}

	public void setTrafficSourceId(int trafficSourceId) {
		this.trafficSourceId = trafficSourceId;
	}


	public String getTrafficSourceName() {
		return this.trafficSourceName;
	}

	public void setTrafficSourceName(String trafficSourceName) {
		this.trafficSourceName = trafficSourceName;
	}


	public String getTrafficSourceToken() {
		return this.trafficSourceToken;
	}

	public void setTrafficSourceToken(String trafficSourceToken) {
		this.trafficSourceToken = trafficSourceToken;
	}


	public byte getUseParentHandler() {
		return this.useParentHandler;
	}

	public void setUseParentHandler(byte useParentHandler) {
		this.useParentHandler = useParentHandler;
	}


	public String getUserAgent() {
		return this.userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}


	public String getValidityCategory() {
		return this.validityCategory;
	}

	public void setValidityCategory(String validityCategory) {
		this.validityCategory = validityCategory;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getValidityEndDate() {
		return this.validityEndDate;
	}

	public void setValidityEndDate(Date validityEndDate) {
		this.validityEndDate = validityEndDate;
	}


	public int getValidityPeriod() {
		return this.validityPeriod;
	}

	public void setValidityPeriod(int validityPeriod) {
		this.validityPeriod = validityPeriod;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getValidityStartTime() {
		return this.validityStartTime;
	}

	public void setValidityStartTime(Date validityStartTime) {
		this.validityStartTime = validityStartTime;
	}


	public String getValidityUnit() {
		return this.validityUnit;
	}

	public void setValidityUnit(String validityUnit) {
		this.validityUnit = validityUnit;
	}


	public double getVoucherDiscount() {
		return this.voucherDiscount;
	}

	public void setVoucherDiscount(double voucherDiscount) {
		this.voucherDiscount = voucherDiscount;
	}


	public String getVoucherDiscountType() {
		return this.voucherDiscountType;
	}

	public void setVoucherDiscountType(String voucherDiscountType) {
		this.voucherDiscountType = voucherDiscountType;
	}


	public Long getVoucherId() {
		return this.voucherId;
	}

	public void setVoucherId(Long voucherId) {
		this.voucherId = voucherId;
	}


	public String getVoucherName() {
		return this.voucherName;
	}

	public void setVoucherName(String voucherName) {
		this.voucherName = voucherName;
	}


	public String getVoucherType() {
		return this.voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}


	public int getVoucherVendorId() {
		return this.voucherVendorId;
	}

	public void setVoucherVendorId(int voucherVendorId) {
		this.voucherVendorId = voucherVendorId;
	}


	public String getVoucherVendorName() {
		return this.voucherVendorName;
	}

	public void setVoucherVendorName(String voucherVendorName) {
		this.voucherVendorName = voucherVendorName;
	}
	public byte getChurn24Hour() {
		return churn24Hour;
	}
	public void setChurn24Hour(byte churn24Hour) {
		this.churn24Hour = churn24Hour;
	}
	public Date getSubRegDate() {
		return subRegDate;
	}
	public void setSubRegDate(Date subRegDate) {
		this.subRegDate = subRegDate;
	}
	public Integer getCgImageId() {
		return cgImageId;
	}
	public void setCgImageId(Integer cgImageId) {
		this.cgImageId = cgImageId;
	}

}