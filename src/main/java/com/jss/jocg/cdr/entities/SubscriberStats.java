package com.jss.jocg.cdr.entities;

import java.lang.reflect.Field;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="tb_activebase_stats")
@NamedQuery(name="SubscriberStats.findAll", query="SELECT e FROM SubscriberStats e")
public class SubscriberStats {
	
	@Id
	@Column(name="id", unique=true, nullable=false)
	@GeneratedValue(strategy = GenerationType.IDENTITY) 
	private Long id;
	
	java.util.Date datestr;
	String sourceNetworkCode;
	String operator;
	String circle;
	String serviceName;
	String packageName;
	Integer activebase;
	
	public SubscriberStats(){
		
	}
	
	public SubscriberStats(Long id,String sourceNetworkCode,String operator,String circle,String serviceName,String packageName,Integer activeBase){
		this.id=id;
		
		this.sourceNetworkCode=sourceNetworkCode;
		this.operator=operator;
		this.circle=circle;
		this.serviceName=serviceName;
		this.packageName=packageName;
		this.activebase=activeBase;
	}

public String toString() {
		
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public java.util.Date getDatestr() {
		return datestr;
	}

	public void setDatestr(java.util.Date datestr) {
		this.datestr = datestr;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getCircle() {
		return circle;
	}

	public void setCircle(String circle) {
		this.circle = circle;
	}

	public Integer getActivebase() {
		return activebase;
	}

	public void setActivebase(Integer activebase) {
		this.activebase = activebase;
	}

	public String getSourceNetworkCode() {
		return sourceNetworkCode;
	}

	public void setSourceNetworkCode(String sourceNetworkCode) {
		this.sourceNetworkCode = sourceNetworkCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	
	
	
}
