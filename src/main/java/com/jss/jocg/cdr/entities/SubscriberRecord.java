package com.jss.jocg.cdr.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="tb_subscriber_reg")
@NamedQuery(name="SubscriberRecord.findAll", query="SELECT e FROM SubscriberRecord e")
public class SubscriberRecord implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String id;
		String subscriberId;
		String emailId;
		Long msisdn;
		String sourceNetworkName;
		String circleName;
		String circleCode;
		String operatorName;
		String countryName;
		String currency;
		String packageName;
		String serviceName;
		Integer packageChargingInterfaceMapId;
		Double operatorPricePoint;
		Integer validity;
		Double chargedAmount;
		Date subRequestDate;
		Date subChargedDate;
		Date validityStartDate;
		Date validityEndDate;
		Date lastRenewalDate;
		Date nextRenewalDate;
		Date parkingDate;
		Date graceDate;
		Date pendingDate;
		Date suspendDate;
		Date unsubDate;
		String jocgCDRId;
		String jocgTransId;
		Double subSessionRevenue;
		String operatorServiceKey;
		String chargedOperatorKey;
		Integer status;
		String statusDescp;
		String campaignName;
		String trafficSourceName;
		String voucherName;
		Double voucherDiscount;
		Double voucherChargedAmount;
		String systemId;
		String platformId;
		String customerCategory;
		String customerId;
		java.util.Date lastUpdated;
		
		public SubscriberRecord(){
			
		}
		
		public SubscriberRecord(String id,String subscriberId,String emailId,Long msisdn,String sourceNetworkName,String circleName,String circleCode,String operatorName,String countryName,String currency,String packageName,String serviceName,Integer packageChargingInterfaceMapId,Double operatorPricePoint,Integer validity,Double chargedAmount,Date subRequestDate,Date subChargedDate,Date validityStartDate,Date validityEndDate,Date lastRenewalDate,Date nextRenewalDate,Date parkingDate,Date graceDate,Date pendingDate,Date suspendDate,Date unsubDate,String jocgCDRId,String jocgTransId,Double subSessionRevenue,String operatorServiceKey,String chargedOperatorKey,Integer status,String statusDescp,String campaignName,String trafficSourceName,String voucherName,Double voucherDiscount,Double voucherChargedAmount,String systemId,String platformId,String customerCategory,String customerId,java.util.Date lastUpdated){
			this.id=id;
			this.subscriberId=subscriberId;
			this.emailId=emailId;
			this.msisdn=msisdn;
			this.sourceNetworkName=sourceNetworkName;
			this.circleName=circleName;
			this.circleCode=circleCode;
			this.operatorName=operatorName;
			this.countryName=countryName;
			this.currency=currency;
			this.packageName=packageName;
			this.serviceName=serviceName;
			this.packageChargingInterfaceMapId=packageChargingInterfaceMapId;
			this.operatorPricePoint=operatorPricePoint;
			this.validity=validity;
			this.chargedAmount=chargedAmount;
			this.subRequestDate=subRequestDate;
			this.subChargedDate=subChargedDate;
			this.validityStartDate=validityStartDate;
			this.validityEndDate=validityEndDate;
			this.lastRenewalDate=lastRenewalDate;
			this.nextRenewalDate=nextRenewalDate;
			this.parkingDate=parkingDate;
			this.graceDate=graceDate;
			this.pendingDate=pendingDate;
			this.suspendDate=suspendDate;
			this.unsubDate=unsubDate;
			this.jocgCDRId=jocgCDRId;
			this.jocgTransId=jocgTransId;
			this.subSessionRevenue=subSessionRevenue;
			this.operatorServiceKey=operatorServiceKey;
			this.chargedOperatorKey=chargedOperatorKey;
			this.status=status;
			this.statusDescp=statusDescp;
			this.campaignName=campaignName;
			this.trafficSourceName=trafficSourceName;
			this.voucherName=voucherName;
			this.voucherDiscount=voucherDiscount;
			this.voucherChargedAmount=voucherChargedAmount;
			this.systemId=systemId;
			this.platformId=platformId;
			this.customerCategory=customerCategory;
			this.customerId=customerId;
			this.lastUpdated=lastUpdated;
		}

		@Id
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getSubscriberId() {
			return subscriberId;
		}

		public void setSubscriberId(String subscriberId) {
			this.subscriberId = subscriberId;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public Long getMsisdn() {
			return msisdn;
		}

		public void setMsisdn(Long msisdn) {
			this.msisdn = msisdn;
		}

		public String getSourceNetworkName() {
			return sourceNetworkName;
		}

		public void setSourceNetworkName(String sourceNetworkName) {
			this.sourceNetworkName = sourceNetworkName;
		}

		public String getCircleName() {
			return circleName;
		}

		public void setCircleName(String circleName) {
			this.circleName = circleName;
		}

		public String getCircleCode() {
			return circleCode;
		}

		public void setCircleCode(String circleCode) {
			this.circleCode = circleCode;
		}

		public String getOperatorName() {
			return operatorName;
		}

		public void setOperatorName(String operatorName) {
			this.operatorName = operatorName;
		}

		public String getCountryName() {
			return countryName;
		}

		public void setCountryName(String countryName) {
			this.countryName = countryName;
		}

		public String getCurrency() {
			return currency;
		}

		public void setCurrency(String currency) {
			this.currency = currency;
		}

		public String getPackageName() {
			return packageName;
		}

		public void setPackageName(String packageName) {
			this.packageName = packageName;
		}

		public String getServiceName() {
			return serviceName;
		}

		public void setServiceName(String serviceName) {
			this.serviceName = serviceName;
		}

		public Integer getPackageChargingInterfaceMapId() {
			return packageChargingInterfaceMapId;
		}

		public void setPackageChargingInterfaceMapId(Integer packageChargingInterfaceMapId) {
			this.packageChargingInterfaceMapId = packageChargingInterfaceMapId;
		}

		public Double getOperatorPricePoint() {
			return operatorPricePoint;
		}

		public void setOperatorPricePoint(Double operatorPricePoint) {
			this.operatorPricePoint = operatorPricePoint;
		}

		public Integer getValidity() {
			return validity;
		}

		public void setValidity(Integer validity) {
			this.validity = validity;
		}

		public Double getChargedAmount() {
			return chargedAmount;
		}

		public void setChargedAmount(Double chargedAmount) {
			this.chargedAmount = chargedAmount;
		}

		public Date getSubRequestDate() {
			return subRequestDate;
		}

		public void setSubRequestDate(Date subRequestDate) {
			this.subRequestDate = subRequestDate;
		}

		public Date getSubChargedDate() {
			return subChargedDate;
		}

		public void setSubChargedDate(Date subChargedDate) {
			this.subChargedDate = subChargedDate;
		}

		public Date getValidityStartDate() {
			return validityStartDate;
		}

		public void setValidityStartDate(Date validityStartDate) {
			this.validityStartDate = validityStartDate;
		}

		public Date getValidityEndDate() {
			return validityEndDate;
		}

		public void setValidityEndDate(Date validityEndDate) {
			this.validityEndDate = validityEndDate;
		}

		public Date getLastRenewalDate() {
			return lastRenewalDate;
		}

		public void setLastRenewalDate(Date lastRenewalDate) {
			this.lastRenewalDate = lastRenewalDate;
		}

		public Date getNextRenewalDate() {
			return nextRenewalDate;
		}

		public void setNextRenewalDate(Date nextRenewalDate) {
			this.nextRenewalDate = nextRenewalDate;
		}

		public Date getParkingDate() {
			return parkingDate;
		}

		public void setParkingDate(Date parkingDate) {
			this.parkingDate = parkingDate;
		}

		public Date getGraceDate() {
			return graceDate;
		}

		public void setGraceDate(Date graceDate) {
			this.graceDate = graceDate;
		}

		public Date getPendingDate() {
			return pendingDate;
		}

		public void setPendingDate(Date pendingDate) {
			this.pendingDate = pendingDate;
		}

		public Date getSuspendDate() {
			return suspendDate;
		}

		public void setSuspendDate(Date suspendDate) {
			this.suspendDate = suspendDate;
		}

		public Date getUnsubDate() {
			return unsubDate;
		}

		public void setUnsubDate(Date unsubDate) {
			this.unsubDate = unsubDate;
		}

		public String getJocgCDRId() {
			return jocgCDRId;
		}

		public void setJocgCDRId(String jocgCDRId) {
			this.jocgCDRId = jocgCDRId;
		}

		public String getJocgTransId() {
			return jocgTransId;
		}

		public void setJocgTransId(String jocgTransId) {
			this.jocgTransId = jocgTransId;
		}

		public Double getSubSessionRevenue() {
			return subSessionRevenue;
		}

		public void setSubSessionRevenue(Double subSessionRevenue) {
			this.subSessionRevenue = subSessionRevenue;
		}

		public String getOperatorServiceKey() {
			return operatorServiceKey;
		}

		public void setOperatorServiceKey(String operatorServiceKey) {
			this.operatorServiceKey = operatorServiceKey;
		}

		public String getChargedOperatorKey() {
			return chargedOperatorKey;
		}

		public void setChargedOperatorKey(String chargedOperatorKey) {
			this.chargedOperatorKey = chargedOperatorKey;
		}

		public Integer getStatus() {
			return status;
		}

		public void setStatus(Integer status) {
			this.status = status;
		}

		public String getStatusDescp() {
			return statusDescp;
		}

		public void setStatusDescp(String statusDescp) {
			this.statusDescp = statusDescp;
		}

		public String getCampaignName() {
			return campaignName;
		}

		public void setCampaignName(String campaignName) {
			this.campaignName = campaignName;
		}

		public String getTrafficSourceName() {
			return trafficSourceName;
		}

		public void setTrafficSourceName(String trafficSourceName) {
			this.trafficSourceName = trafficSourceName;
		}

		public String getVoucherName() {
			return voucherName;
		}

		public void setVoucherName(String voucherName) {
			this.voucherName = voucherName;
		}

		public Double getVoucherDiscount() {
			return voucherDiscount;
		}

		public void setVoucherDiscount(Double voucherDiscount) {
			this.voucherDiscount = voucherDiscount;
		}

		public Double getVoucherChargedAmount() {
			return voucherChargedAmount;
		}

		public void setVoucherChargedAmount(Double voucherChargedAmount) {
			this.voucherChargedAmount = voucherChargedAmount;
		}

		public String getSystemId() {
			return systemId;
		}

		public void setSystemId(String systemId) {
			this.systemId = systemId;
		}

		public String getPlatformId() {
			return platformId;
		}

		public void setPlatformId(String platformId) {
			this.platformId = platformId;
		}

		public String getCustomerCategory() {
			return customerCategory;
		}

		public void setCustomerCategory(String customerCategory) {
			this.customerCategory = customerCategory;
		}

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public java.util.Date getLastUpdated() {
			return lastUpdated;
		}

		public void setLastUpdated(java.util.Date lastUpdated) {
			this.lastUpdated = lastUpdated;
		}
		
		
		
}
