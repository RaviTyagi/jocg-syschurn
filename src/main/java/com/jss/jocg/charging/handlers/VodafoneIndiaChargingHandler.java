package com.jss.jocg.charging.handlers;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.VodafoneChargingParams;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.ibm.vodafone.encrytion.EncryptionDecryptionUtil;

//com.jss.jocg.charging.handlers.VodafoneIndiaChargingHandler
public class VodafoneIndiaChargingHandler extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(VodafoneIndiaChargingHandler.class);
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" ::";
		ModelAndView mv=new ModelAndView();
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		try{
			
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"Charging URL Params "+cr.getChargingUrlParams());
			
			CgimageSchedule cgSch=cr.getCgimageSchedule();
			String cgImage="";
			if(cgSch!=null && cgSch.getImageUrl()!=null && cgSch.getImageUrl().startsWith("http://")){
				cgImage=cgSch.getImageUrl();
			}
			
			VodafoneChargingParams vcp=getEncryptedParams(true,""+cr.getMsisdn(),cr.getChargingUrlParams(),cr.getChargingPrice().getOpParams(),cr.getJocgTransactionId(),cgImage,((cr.getCircle()!=null)?cr.getCircle().getCirclecode():"05"));
			vcp.setCgURL(cr.getChargingURL().getUrl());
			if(cr.getLandingPage()!=null && cr.getLandingPage().getLandingPageId()>0)
				mv.setViewName(cr.getLandingPage().getLandingUrl());//mv.setViewName("vodacg");
			else{
				mv.setViewName("dvodacg");	
				
			}
			mv.addObject("vcp",vcp);

			cresp.setRespView(mv);
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Charging In Process");
			logger.info(loggerHead+"Handler Process Completed, Status "+cresp.getResponseCode());
			
		}catch(Exception e){
			logger.info(loggerHead+"Exception in Handler "+e);
			e.printStackTrace();
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception in handler "+e.getMessage());
			mv.setViewName("error");
			mv.addObject("message", "Error while processing Request");
			cresp.setRespView(mv);
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
		}
		
		cr.setResponseObject(cresp);
		return cr;
	}
	
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
		// TODO Auto-generated method stub
				String loggerHead=cr.getLoggerHead()+"VODAIN-CH :: ";
				ChargingResponse cresp=new ChargingResponse();
				ModelAndView mv=new ModelAndView();
				try{
					logger.info(loggerHead+"URL List inside Handler  "+cr.getOperatorURLList());
					ChargingUrl churl=null;
					for(ChargingUrl curl:cr.getOperatorURLList()){
						if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
							churl=curl; break;
						}
					}
					logger.info(loggerHead+"Processing for Charging URL "+churl+", url id "+churl.getChurlId());
					List<ChargingUrlParam> urlParams=cr.getOperatorUrlParams().get(churl.getChurlId());
					logger.info(loggerHead+"Charging URL Params "+urlParams);
				
					VodafoneChargingParams vcp=getEncryptedParams(true,""+cr.getMsisdn(),urlParams,cr.getSubscriber().getOperatorParams(),cr.getJocgTransId(),cr.getCgImageURL(),((cr.getCircleInfo()!=null)?cr.getCircleInfo().getCirclecode():"05"));
					vcp.setCgURL(churl.getUrl());
					mv.setViewName("vodacg");
					mv.addObject("vcp",vcp);
					
//					Map<String,String> encParams=getEncryptedParams(""+cr.getMsisdn(),urlParams,cr.getSubscriber().getOperatorParams(),cr.getJocgTransId(),cr.getCgImageURL(),((cr.getCircleInfo()!=null)?cr.getCircleInfo().getCirclecode():"05"));
//					logger.info(loggerHead+"Encoded Params "+encParams);
//					Iterator<String> i=encParams.keySet().iterator();
//					String key="";
//					while(i.hasNext()){
//						key=i.next();
//						mv.addObject(key, encParams.get(key));
//					}
//					mv.setViewName("vodacg");
//					mv.addObject("CGURL", churl.getUrl());
					cresp.setModelAndView(mv);
					cresp.setStatusCode(JocgStatusCodes.CHARGING_INPROCESS);
					cresp.setStatusDescription("Charging In Process");
					logger.info(loggerHead+"Handler Process Completed, Status "+cresp.getStatusCode());
					
				}catch(Exception e){
					logger.info(loggerHead+"Exception in Handler "+e);
					e.printStackTrace();
					cresp.setStatusCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
					cresp.setStatusDescription("Exception in handler "+e.getMessage());
					mv.setViewName("error");
					mv.addObject("message", "Error while processing Request");
				}
				return cresp;
	}
	
	
	public VodafoneChargingParams getEncryptedParams(boolean encrypt,String msisdn,List<ChargingUrlParam> urlParams,String operatorParams,String transactionId,String cgImage,String circleCode ){
		//Map<String,String> resp=new HashMap<>();
		 VodafoneChargingParams vcp=new VodafoneChargingParams();
		logger.info(msisdn+" getEncryptedParams() :: Encoding Process started .....opparams "+operatorParams);
		
		try{
		String loginId="NA",password="NA",privateKey="NA";
	
		for(ChargingUrlParam p1:urlParams){
			logger.info(msisdn+" getEncryptedParams() :: Checking Auth Params "+p1);
			if(p1.getParamKey().equalsIgnoreCase("Loginid"))
				loginId=p1.getDefaultValue();
			else if(p1.getParamKey().equalsIgnoreCase("password"))
				password=p1.getDefaultValue();
			else if(p1.getParamKey().equalsIgnoreCase("EncryptionKey"))
				privateKey=p1.getDefaultValue();
		}
		logger.info(msisdn+" getEncryptedParams() :: Auth Params loginId="+loginId+"|password="+password+"|privateKey="+privateKey); 
		if(!"NA".equalsIgnoreCase(loginId) || !"NA".equalsIgnoreCase(password) || !"NA".equalsIgnoreCase(privateKey)){
		
			String secretKey = genSecretKey(loginId, password);

	        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	        String transDate = sdf.format(new java.util.Date(System.currentTimeMillis()));
	        vcp.decodeParams(operatorParams);
	        String errorRedirectUrl="";
	        for(ChargingUrlParam p1:urlParams){
	        	logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Default Value "+p1.getDefaultValue()); 
	        	if(p1.getParamKey().equalsIgnoreCase("Loginid")){
	        		vcp.setLoginid(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		// resp.put(p1.getParamKey(),);
	        	}else if(p1.getParamKey().equalsIgnoreCase("password")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+secretKey); 
	        		vcp.setPassword(secretKey);
	        		//resp.put(p1.getParamKey(), secretKey);
	        	}else if(p1.getParamKey().equalsIgnoreCase("org_id")){
	        		vcp.setOrg_id(encryptParam(msisdn,p1.getDefaultValue(),false,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("from")){
	        		vcp.setFrom(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("consentStatus")){
	        		vcp.setConsentStatus(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("consentMsg")){
	        		vcp.setConsentMsg(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("ERROR_URL")){
	        		errorRedirectUrl=p1.getDefaultValue();
	        		//vcp.setParam1((encrypt)?java.net.URLEncoder.encode(EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue())):p1.getDefaultValue());
	        		//resp.put("param1", java.net.URLEncoder.encode(EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue())));
	        	}else if(p1.getParamKey().equalsIgnoreCase("CallBackURL")){
	        		vcp.setCallBackURL(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		//resp.put(p1.getParamKey(), java.net.URLEncoder.encode(EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue())));
	        	}else if(p1.getParamKey().equalsIgnoreCase("Service")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+vcp.getService()); 
	        		//No Need to encode Service Parameter so no need to encode
	        		vcp.setService(encryptParam(msisdn,vcp.getService(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(vcp.getService()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("Class")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+vcp.getCLASS()); 
	        		vcp.setCLASS(encryptParam(msisdn,vcp.getCLASS(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(vcp.getClassValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("MSISDN")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+msisdn); 
	        		vcp.setMSISDN(encryptParam(msisdn,msisdn,true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(msisdn));
	        	}else if(p1.getParamKey().equalsIgnoreCase("mode")){
	        		vcp.setMode(encryptParam(msisdn,p1.getDefaultValue(),true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(p1.getDefaultValue()));
	        	}else if(p1.getParamKey().equalsIgnoreCase("requestid")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+transactionId); 
	        		vcp.setRequestid(encryptParam(msisdn,transactionId,true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(transactionId));
	        	}else if(p1.getParamKey().equalsIgnoreCase("Requesttime")){
	        		logger.info(msisdn+" getEncryptedParams() :: Processing Param ="+p1.getParamKey()+", Overridden Value "+transDate); 
	        		vcp.setRequesttime(encryptParam(msisdn,transDate,true,encrypt));
	        		//resp.put(p1.getParamKey(), EncryptionDecryptionUtil.doEncrypt(transDate));
	        	}
	        	logger.info(msisdn+" getEncryptedParams() :: Param ="+p1.getParamKey()+" processed successfully"); 
	        	
	        }
	        
	        if(cgImage==null || "NA".equalsIgnoreCase(cgImage) || "cgimage".equalsIgnoreCase(cgImage)) cgImage="";
	        logger.info(msisdn+" getEncryptedParams() :: Adding CGImage  ="+cgImage); 
	        logger.info(msisdn+" getEncryptedParams() :: Processing Param =param3, Overridden Value "+errorRedirectUrl+"|offdeck|"+cgImage);
	        vcp.setParam3((cgImage!=null && cgImage.length()>0)?(java.net.URLEncoder.encode(errorRedirectUrl)+"|offdeck|"+java.net.URLEncoder.encode(cgImage)):errorRedirectUrl+"|offdeck|");
	        //vcp.setParam3(encryptParam(msisdn,((cgImage!=null && cgImage.length()>0)?(errorRedirectUrl+"|offdeck|"+cgImage):(errorRedirectUrl+"|offdeck|")),true,encrypt));
	        //	resp.put("param3", (cgImage!=null && cgImage.length()>0)?("|offdeck|"+java.net.URLEncoder.encode(cgImage)):"|offdeck|");
	        	logger.info(msisdn+" getEncryptedParams() :: Adding CircleCode  ="+circleCode); 
	        	vcp.setCircleId(encryptParam(msisdn,(circleCode==null)?"05":circleCode,true,encrypt));
	       // resp.put("CircleId", EncryptionDecryptionUtil.doEncrypt(circleCode));
	  }else{
		 
		  logger.error(msisdn+" :: encodedParams() :: Missing Auth Parameters ! loginid="+loginId+"|password="+password+"|privateKey="+privateKey);
		}
		}catch(Exception e){
			logger.error(msisdn+" :: encodedParams() :: "+e);
			
		}
	return vcp;
		
	}
	
	 public String genSecretKey(String loginId, String password) {
	        String privateKey = System.currentTimeMillis() + "|" + loginId + "|" + password;
	        String secretKey = EncryptionDecryptionUtil.doEncrypt(privateKey);
	        return secretKey;
	    }

	  public String encryptParam(String msisdn,String keyVal,boolean urlEncode,boolean encrypt){
		  String encValue="";
		  try{
		  if(urlEncode && encrypt){
			  //encValue= EncryptionDecryptionUtil.doEncrypt(java.net.URLEncoder.encode(keyVal,"utf8"));
			  encValue= java.net.URLEncoder.encode(EncryptionDecryptionUtil.doEncrypt(keyVal));
		  }else if(encrypt){
			  encValue= EncryptionDecryptionUtil.doEncrypt(keyVal);
		  }else
			  encValue= java.net.URLEncoder.encode(keyVal);
		  logger.info(msisdn+" encryptParam() :: Value="+keyVal+", Encoded Value "+encValue); 
		  }catch(Exception e){}
		  return encValue;
			  
	  }
	  
	  @Override
		public JocgDeactivationResponse processDeactivation(JocgRequest newReq,HttpServletRequest requestContext){
		  	String logHeader="processDeactivation("+newReq.getMsisdn()+") ::";
			JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
			jocgDeact.setStatusDescp("UNSUB IN PROCCESS");
			SimpleDateFormat sdf=new SimpleDateFormat("yyMMddHHmmss");
			VodafoneChargingParams vcp=new VodafoneChargingParams();
			if(vcp.decodeParams(newReq.getSubscriber().getOperatorParams())){
				String url="https://msg.vodafone.in:443/SGSM/sgsm?msisdn="+newReq.getSubscriber().getMsisdn()+"&service="+vcp.getService()+"&class="+vcp.getCLASS()+"&mode=WAP_D2C&requestid="+newReq.getSubscriber().getId()+"&action=CAN&org_id=DIGIVIVE&loginid=divive12&password=diG23Vive&timestamp="+sdf.format(new java.util.Date(System.currentTimeMillis()));
				logger.debug(logHeader+" Invoking URL "+url);
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setDestURL(url);
				sslClient.setAcceptDataFormat("text/html");
				sslClient.setHttpMethod("POST");
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore("VodaChurn:{"+newReq.getMsisdn()+"} :: ");
				int statusCode=getUnsubStatusCode(resp.getResponseData());
				
				if(statusCode==500){
					jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
					jocgDeact.setStatusDescp("UNSUB Request Sent "+resp.getResponseData());
				}else{
					jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED_LINKFAILURE);
					jocgDeact.setStatusDescp("UNSUB Request Sent "+resp.getResponseData());
				}
				logger.debug(logHeader+" URL Response "+resp.getResponseData()+", Parsed Status Code "+statusCode);
//				jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
//				jocgDeact.setStatusDescp("UNSUB Request Sent "+resp.getResponseData());
			}else{
				logger.error(logHeader+" Failed to decode Parameters ");
				jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
				jocgDeact.setStatusDescp("UNSUB Request Failed");
			}
			return jocgDeact;
		}
	  
	  
	  public int getUnsubStatusCode(String resp){
		  int statusCode=JocgStatusCodes.UNSUB_FAILED_LINKFAILURE;
			try{
			  if(resp.contains("STATUS CODE")){
					String temp=resp.substring(resp.indexOf("<TD>",resp.indexOf("STATUS CODE")+1));
					temp=temp.substring(temp.indexOf(">")+1);
					temp=temp.substring(0, temp.indexOf("<"));
					statusCode=JocgString.strToInt(temp, JocgStatusCodes.UNSUB_FAILED_LINKFAILURE);
					
				}
			}catch(Exception e){
				statusCode=JocgStatusCodes.UNSUB_FAILED_LINKFAILURE;
			}
			return statusCode;
	  }

}
