package com.jss.jocg.charging.handlers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.util.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.MobikwikChargingParams;
import com.jss.jocg.charging.model.parsers.MobikwikResponse;
import com.jss.jocg.charging.model.parsers.OxigenResponse;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class OxigenChargingHandler   extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(FreechargeChargingHandler.class);
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		
		if(cr.getMsisdn().longValue()<=0){
			ModelAndView mv=new ModelAndView();
			mv.setViewName("wregoxigen");
			mv.addObject("transid", cr.getJocgTransactionId());
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Customer Requested to Enter Msisdn");
			cr.setResponseObject(cresp);
		}else{
			//Send OTP for charging
			ChargingUrl churl=null;
			System.out.println("URL List "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("OTP-SEND".equalsIgnoreCase(curl.getUrlCatg())){ 
					churl=curl; break;
				}
			}
			
			List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());
			
			//Invoke Generate OTP Pin
			String msisdnStr=""+cr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			
		
			String dataTemplate=churl.getDataTemplate();
			
			for(ChargingUrlParam param:urlParams){
				if("MOBILE".equalsIgnoreCase(param.getParamKey())){
					dataTemplate=dataTemplate.replaceAll("<MOBILE>", msisdnStr);
				}else if("OTPTYPE".equalsIgnoreCase(param.getParamKey())){
					dataTemplate=dataTemplate.replaceAll("<OTPTYPE>", param.getDefaultValue());
				}
			}
			
			System.out.println(loggerHead+" DataTemplate "+dataTemplate);
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/url-encoded");
			sslClient.setHttpMethod("GET");
			int indx=churl.getAuthTemplate().indexOf(":");				
			sslClient.setAuthUser(churl.getAuthTemplate().substring(0,indx));
			sslClient.setAuthPwd(churl.getAuthTemplate().substring(indx+1));
			sslClient.setUseBasicAuthentication(true);
			sslClient.setContentType("application/x-www-form-urlencoded");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setDestURL(churl.getUrl()+"?"+dataTemplate);		
			sslClient.setRequestData(dataTemplate);
			
			sslClient.setConnectionTimeoutInSecond(10);
			sslClient.setReadTimeoutInSecond(20);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLOxigenPlain(loggerHead);		
			String respData=resp.getResponseData();
			System.out.println("Oxigen Response "+respData);
			
			OxigenResponse respObj=new OxigenResponse();
			int respCode=respObj.parseResponse("SEND-OTP", respData);
			logger.debug(loggerHead+" Oxigen SendOTP Api Parsed Response "+respObj+"| Raw data "+respData);
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if("SUCCESS".equalsIgnoreCase(respObj.getResponseMessage()) && respCode==0){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpoxigen");
				mv.addObject("transid", cr.getJocgTransactionId());
				mv.addObject("otpId", respObj.getReferenceId());
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin(respData);
				cr.setResponseObject(cresp);
			}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("OTP Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
				cr.setResponseObject(cresp);
			}
			
		}
		
		return cr;
	}
	
	public JocgResponse sendOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="OXIGEN::SENDOTP::"+cdr.getMsisdn().longValue()+"::";
		
		JocgResponse cresp=new JocgResponse();
		try{
			//Invoke Generate OTP Pin
			String msisdnStr=""+cdr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			String dataTemplate=url.getDataTemplate();
			
			for(ChargingUrlParam param:urlParams){
				if("EMAIL".equalsIgnoreCase(param.getParamKey())){
					dataTemplate=dataTemplate.replaceAll("<EMAIL>", cdr.getEmailId());
				}else if("MOBILE".equalsIgnoreCase(param.getParamKey())){
					dataTemplate=dataTemplate.replaceAll("<MOBILE>",msisdnStr );
				}else if("OTPTYPE".equalsIgnoreCase(param.getParamKey())){
					dataTemplate=dataTemplate.replaceAll("<OTPTYPE>", param.getDefaultValue());
				}
			}
			
			System.out.println(loggerHead+" DataTemplate "+dataTemplate);
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/url-encoded");
			sslClient.setHttpMethod("GET");
			int indx=url.getAuthTemplate().indexOf(":");				
			sslClient.setAuthUser(url.getAuthTemplate().substring(0,indx));
			sslClient.setAuthPwd(url.getAuthTemplate().substring(indx+1));
			sslClient.setUseBasicAuthentication(true);
			sslClient.setContentType("application/x-www-form-urlencoded");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setDestURL(url.getUrl()+"?"+dataTemplate);		
			sslClient.setRequestData(dataTemplate);
			
			sslClient.setConnectionTimeoutInSecond(10);
			sslClient.setReadTimeoutInSecond(20);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLOxigenPlain(loggerHead);		
			String respData=resp.getResponseData();
			System.out.println("Oxigen Response "+respData);
			
			OxigenResponse respObj=new OxigenResponse();
			int respCode=respObj.parseResponse("SEND-OTP", respData);
			logger.debug(loggerHead+" Oxigen SendOTP Api Parsed Response "+respObj+"| Raw data "+respData);
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if("SUCCESS".equalsIgnoreCase(respObj.getResponseMessage()) && respCode==0){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpoxigen");
				mv.addObject("transid", cdr.getJocgTransId());
				mv.addObject("otpId", respObj.getReferenceId());
				mv.addObject("msisdn",cdr.getMsisdn());
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin(respData);
			}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("OTP Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
				
			}
			
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cresp.setResponseDescription("System Error! OTP Failed. Please try again later.");
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		}
		return cresp;
	}
	
	
	public JocgResponse verifyOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="OXIGEN::VERIFYOTP::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
				//mobilenumber=<MOBILE>&otp=<OTP>
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+url.getDataTemplate());
				String dataTemplate=url.getDataTemplate();
				
				for(ChargingUrlParam param:urlParams){
					if("MOBILE".equalsIgnoreCase(param.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<MOBILE>", msisdnStr);
					}else if("OTP".equalsIgnoreCase(param.getParamKey())){
						String otp=cdr.getOtpPin();
						otp=(otp.indexOf(",")>0)?otp.substring(0,otp.indexOf(",")):otp;
						dataTemplate=dataTemplate.replaceAll("<OTP>", otp);
					}
				}
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/url-encoded");
				sslClient.setHttpMethod("POST");
				int indx=url.getAuthTemplate().indexOf(":");				
				sslClient.setAuthUser(url.getAuthTemplate().substring(0,indx));
				sslClient.setAuthPwd(url.getAuthTemplate().substring(indx+1));
				sslClient.setUseBasicAuthentication(true);
				sslClient.setContentType("application/x-www-form-urlencoded");
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setDestURL(url.getUrl());		//+"?"+dataTemplate
				sslClient.setRequestData(dataTemplate);
				
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLBasicAuth(loggerHead);		
				String respData=resp.getResponseData();
				OxigenResponse respObj=new OxigenResponse();
				int respCode=respObj.parseResponse("SEND-OTP", respData);
				logger.debug(loggerHead+" Oxigen VerifyOTP Api Parsed Response "+respObj+"| Raw data "+respData);
				
				if("SUCCESS".equalsIgnoreCase(respObj.getResponseMessage()) && respCode==0){
						cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
						cresp.setResponseDescription(respObj.getResponseMessage()+"-"+respObj.getReferenceId());
						cresp.setAccessToken(respObj.getAccessToken());
					}else{
						cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
						cresp.setResponseDescription("Authentication Failed!"+respObj.getResponseMessage());
					}
			}else{
				//INvalid URL
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("OTP Validation Failed! Invalid Verification URL.");
			}
			
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setResponseDescription("OTP Validation Failed!System Error.");
		}
		
		return cresp;
	}
	
	public JocgResponse chargeCustomer(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="OXIGEN::ChargedCustomer::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			
				String dataTemplate=url.getDataTemplate();
				//mobilenumber=<MOBILE>&Amount=<AMOUNT>&Keyword=<KEYWORD>&transID=<TRANSID>
				for(ChargingUrlParam param:urlParams){
					if("MOBILE".equalsIgnoreCase(param.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<MOBILE>", msisdnStr);
					}else if("AMOUNT".equalsIgnoreCase(param.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<AMOUNT>", ""+cdr.getOperatorPricePoint().intValue()+"00");
					}else if("KEYWORD".equalsIgnoreCase(param.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<KEYWORD>", param.getDefaultValue());
					}else if("TRANSID".equalsIgnoreCase(param.getParamKey())){
						//dataTemplate=dataTemplate.replaceAll("<TRANSID>", cdr.getJocgTransId());
						dataTemplate=dataTemplate.replaceAll("<TRANSID>", "DV"+System.currentTimeMillis());
					}
				}
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/url-encoded");
				sslClient.setHttpMethod("POST");
				int indx=url.getAuthTemplate().indexOf(":");
				sslClient.setAuthUser(url.getAuthTemplate().substring(0,indx));
				sslClient.setAuthPwd(url.getAuthTemplate().substring(indx+1));
				sslClient.setUseBasicAuthentication(true);
				sslClient.setAuthToken(cdr.getSubRegId());
				sslClient.setUseAuthTokenInBasicAuth(true);
				sslClient.setContentType("application/x-www-form-urlencoded");
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLBasicAuthOxigen(loggerHead);
				System.out.println("Oxigen Debit API Response "+resp.getResponseData());
				String respData=resp.getResponseData();
				OxigenResponse respObj=new OxigenResponse();
				int respCode=respObj.parseResponse("DEBIT", respData);
				logger.debug(loggerHead+" Oxigen DEBIT Api Parsed Response "+respObj+"| Raw data "+respData);
				
				if("SUCCESS".equalsIgnoreCase(respObj.getResponseMessage()) && respCode==0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_SUCCESS);
					cresp.setResponseDescription(respObj.getResponseMessage()+"-"+respObj.getReferenceId());
				}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					cresp.setResponseDescription("Charging Failed!"+respObj.getResponseMessage());
				}
			}else{
				//INvalid URL
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("Invalid DEBIT API URL!");
			}
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		}catch(Exception e){
			e.printStackTrace();
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setResponseDescription("System Error!");
		}
		
		return cresp;
	}
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("UNSUB Successful");
		return jocgDeact;
	}
}
