package com.jss.jocg.charging.handlers;

import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.AircelChargingParameters;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.util.JocgStatusCodes;

public class AircelIndiaChargingHandler  extends JocgChargingHandlerImpl{

	private static final Logger logger = LoggerFactory
			.getLogger(AircelIndiaChargingHandler.class);
	SimpleDateFormat timesdf=new SimpleDateFormat("yyyyMMddHHmmss");
	@Override
	public JocgRequest processCharging(JocgRequest cr, HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"  Charging URL Params "+cr.getChargingUrlParams()+" Charging Request : "+cr.getMsisdn()+" "+cr.getDeviceInfo());
			AircelChargingParameters acp=new AircelChargingParameters();
			acp.decodeParams(cr.getChargingPrice().getOpParams());
			String dataTemplate=cr.getChargingURL().getDataTemplate();
			for(ChargingUrlParam cparam:cr.getChargingUrlParams()){
				if("CONSTANT".equalsIgnoreCase(cparam.getParamCatg())){
					if("CPID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("USERID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("PASSWORD".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("BEARMODE".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("SEID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("REDIRECTURL".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", URLEncoder.encode(cparam.getDefaultValue().trim()));
					}else if("WAP_MDATA".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("CONTENTID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}
				}else{
					if("PRODUCTID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", acp.getProductid()).trim();
					}else if("MPNAME".equalsIgnoreCase(cparam.getParamKey())){						
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", acp.getMpname()).trim();
					}else if("MSISDN".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", ""+(cr.getMsisdn()%10000000000L));
					}else if("TRANSID".equalsIgnoreCase(cparam.getParamKey())){
						int msisdnlastDigit=(int)(cr.getMsisdn()%10000);						
						cr.setJocgTransactionId("DIGIV"+System.currentTimeMillis()+msisdnlastDigit);
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cr.getJocgTransactionId().trim());
					}else if("PRICE".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">" ,Integer.toString(acp.getPrice()*100)).trim();
					}else if("PDESC".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">",acp.getPdesc()).trim();
					}else if("VALIDITY".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">",Integer.toString(acp.getValidity())).trim();
					}
				}
			}
			
			String cgURL=(cr.getChargingURL().getUrl().endsWith("?"))?cr.getChargingURL().getUrl()+""+dataTemplate:cr.getChargingURL().getUrl()+"?"+dataTemplate;
			logger.info(loggerHead+"CG URL Template "+cgURL);
			
			cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
			cresp.setRedirectUrl(cgURL);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Forward to CG URL.....");
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e);
		}
		cr.setResponseObject(cresp);
		return cr;
	}
}
	
	