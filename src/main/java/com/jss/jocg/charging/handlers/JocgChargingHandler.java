package com.jss.jocg.charging.handlers;

import javax.servlet.http.HttpServletRequest;

import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;

public interface JocgChargingHandler {
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext);
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext);
	
	public ChargingResponse processCharging(ChargingRequest cr,HttpServletRequest requestContext);
	
	
}
