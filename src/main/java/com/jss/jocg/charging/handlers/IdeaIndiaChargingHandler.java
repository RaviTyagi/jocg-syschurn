package com.jss.jocg.charging.handlers;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.IdeaChargingParameters;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.VodafoneChargingParams;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;

public class IdeaIndiaChargingHandler  extends JocgChargingHandlerImpl{

	private static final Logger logger = LoggerFactory
			.getLogger(IdeaIndiaChargingHandler.class);
	SimpleDateFormat timesdf=new SimpleDateFormat("yyyyMMddHHmmss");
	@Override
	public JocgRequest processCharging(JocgRequest cr, HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"Charging URL Params "+cr.getChargingUrlParams());
			IdeaChargingParameters icp=new IdeaChargingParameters();
			icp.decodeParams(cr.getChargingPrice().getOpParams());
			String dataTemplate=cr.getChargingURL().getDataTemplate();
			for(ChargingUrlParam cparam:cr.getChargingUrlParams()){
				if("CONSTANT".equalsIgnoreCase(cparam.getParamCatg())){
					if("CPID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("USER_ID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", icp.getEncryptedParameter(cparam.getDefaultValue()).trim());
					}else if("PWD".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", icp.getEncryptedParameter(cparam.getDefaultValue()).trim());
					}
				}else{
					if("SERVICE_KEY".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", icp.getEncryptedParameter(icp.getServiceKey()).trim());
					}else if("IMGURL".equalsIgnoreCase(cparam.getParamKey())){
						String imgUrl=cr.getCgimageSchedule().getImageUrl();
						imgUrl=(imgUrl!=null)?imgUrl:"";
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", URLEncoder.encode(imgUrl, "utf8").trim());
					}else if("TRANS_TIME".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">",timesdf.format(new java.util.Date(System.currentTimeMillis())));
					}else if("TRANSID".equalsIgnoreCase(cparam.getParamKey())){
						int msisdnlastDigit=(int)(cr.getMsisdn()%10000);
						if(cr.getChargingOperator().getChintfid()==6)
							cr.setJocgTransactionId("O4"+System.currentTimeMillis());
						else
						cr.setJocgTransactionId("Digivive"+System.currentTimeMillis()+msisdnlastDigit);
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cr.getJocgTransactionId().trim());
					}
				}
			}
			
			String cgURL=(cr.getChargingURL().getUrl().endsWith("?"))?cr.getChargingURL().getUrl()+""+dataTemplate:cr.getChargingURL().getUrl()+"?"+dataTemplate;
			logger.info(loggerHead+"CG URL Template "+cgURL);
			
			cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
			cresp.setRedirectUrl(cgURL);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Forward to CG URL.....");
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e);
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	 @Override
		public JocgDeactivationResponse processDeactivation(JocgRequest newReq,HttpServletRequest requestContext){
		  	String logHeader="processDeactivation("+newReq.getMsisdn()+") ::";
			JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
			jocgDeact.setStatusDescp("UNSUB IN PROCCESS");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdftime=new SimpleDateFormat("hhmmss");
			IdeaChargingParameters icp=new IdeaChargingParameters();
			if(icp.decodeParams(newReq.getSubscriber().getOperatorParams())){
				String url="https://112.110.33.101:7005/subscription/DeactivateSubscription?msisdn="+newReq.getSubscriber().getMsisdn()+"&srvkey="+icp.getServiceKey()+"&User=Digivive&Pass=Ekzsi42h&reqType=Unsub&mode=WAP&originator=digivive&Refid=Digivive"+System.currentTimeMillis();
				//https://112.110.33.101:7005/subscription/DeactivateSubscription?User=Digivive&Pass=Ekzsi42h&Msisdn=911111269708&Srvkey=MYTVD&mode=WAP&reqType=Unsub&precharge=N&Refid=1z5d7w&reqdate=20171006&reqtime=145532&originator=digivive&info=null
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setDestURL(url);
				sslClient.setCertificateKeystorePath("/home/jocg/conf/jocgvodaidea.keystore");
				sslClient.setTrustStorePassword("changeit");
				sslClient.setAcceptDataFormat("text/html");
				sslClient.setHttpMethod("POST");
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURL();//connectURLViaSSLIgnore("IdeaChurn:{"+newReq.getMsisdn()+"} :: ");
				if(resp.getResponseData()!=null && resp.getResponseData().contains("200") && resp.getResponseData().contains("SUCCESS")){
					jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
					jocgDeact.setStatusDescp("UNSUB Request Sent "+resp.getResponseData());
					logger.debug(logHeader+" URL Response "+resp.getResponseData()+", Error "+resp.getErrorMessage());
				}else{
					logger.error(logHeader+" Request Failed at SDP "+resp.getResponseData());
					jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
					jocgDeact.setStatusDescp("UNSUB Request Failed at SDP, SDP response "+resp.getResponseData());
				}
			}else{
				logger.error(logHeader+" Failed to decode Parameters ");
				jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
				jocgDeact.setStatusDescp("UNSUB Request Failed");
			}
			
		
			return jocgDeact;
		}
	  
	
}
	
	