package com.jss.jocg.charging.handlers.model;

import com.jss.jocg.util.JocgString;

public class MPurseResponse {

	int statusCode;
	String responseStatus;
	String responseMessage;
	String mpurseTransactionId;
	String nexgtvTransId;
	String externalId;
	String timeStamp;
	
	public MPurseResponse(){
		
	}
	public boolean parseMPurseResponse(String mpurseResponse){
		boolean resp=true;
		try{
			mpurseResponse=mpurseResponse.trim();
			if(mpurseResponse.contains("<ResponseCode>")){
				//ParseResponseCode
				this.setStatusCode(JocgString.strToInt(JocgString.parseXmlField(mpurseResponse, "ResponseCode"), -1));
				//parseResponseStatus
				this.setResponseStatus(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "ResponseStatus"),"NA"));
				//parse Response message
				this.setResponseMessage(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "ResponseMessage"),"NA"));
				//parse mpurse trx id
				this.setMpurseTransactionId(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "MPURSETRXID"),"NA"));
				this.setExternalId(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "EXTRXID"),"NA"));
				this.setNexgtvTransId(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "NEXGTVXID"),"NA"));
				this.setTimeStamp(JocgString.formatString(JocgString.parseXmlField(mpurseResponse, "TS"),"NA"));
			}else{
				resp=false;
			}
		}catch(Exception e){
			resp=false;
		}
		return resp;
	}
	
	
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getMpurseTransactionId() {
		return mpurseTransactionId;
	}
	public void setMpurseTransactionId(String mpurseTransactionId) {
		this.mpurseTransactionId = mpurseTransactionId;
	}
	public String getNexgtvTransId() {
		return nexgtvTransId;
	}
	public void setNexgtvTransId(String nexgtvTransId) {
		this.nexgtvTransId = nexgtvTransId;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
}
