package com.jss.jocg.charging.handlers;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.FreechargeChargingParams;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.MobikwikChargingParams;
import com.jss.jocg.charging.model.parsers.FreechargeResponse;
import com.jss.jocg.charging.model.parsers.FreechargeResponseWrapper;
import com.jss.jocg.charging.model.parsers.MobikwikResponse;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class FreechargeChargingHandler  extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(FreechargeChargingHandler.class);
	FreechargeResponseWrapper responseWraper=new FreechargeResponseWrapper();
	String freechargeSecretKey="a395405a-711e-47d4-95d0-c1c4669b3a53";
/*
 	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		ChargingUrl churl=null;
		System.out.println("URL List "+cr.getUrlList());
		for(ChargingUrl curl:cr.getUrlList()){
			if("OTP-SEND".equalsIgnoreCase(curl.getUrlCatg())){ 
				churl=curl; break;
			}
		}

		List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());						
		
		//Invoke Generate OTP Pin
		String msisdnStr=""+cr.getMsisdn();
		msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
		FreechargeChargingParams fcParams=new FreechargeChargingParams();
		fcParams.setMobileNumber(msisdnStr);
		fcParams.setSecretKey(freechargeSecretKey);
				
		for(ChargingUrlParam up:urlParams) if("MERCHANTID".equalsIgnoreCase(up.getParamKey())) fcParams.setMerchantId(up.getDefaultValue());	
		String dataTemplate=fcParams.getJson("OTP", fcParams.getChecksum(loggerHead, "OTP"), true);
		
		JcmsSSLClient sslClient=new JcmsSSLClient();
		sslClient.setAcceptDataFormat("text/plain");
		sslClient.setContentType("application/json");
		sslClient.setHttpMethod("GET");
		sslClient.setContentType("application/json");
		sslClient.setAcceptDataFormat("application/json");
		sslClient.setDestURL(churl.getUrl());
		sslClient.setRequestData(dataTemplate);
		sslClient.setConnectionTimeoutInSecond(10);
		sslClient.setReadTimeoutInSecond(20);
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
		String respData=resp.getResponseData();
		System.out.println("Freecharge OTP API Response "+resp.getResponseData());
		
		int statusCode=resp.getResponseCode();
		int respTimeInSecond=resp.getResponseTime();
		
		if(statusCode==200  && respData.toUpperCase().contains("VERIFY")){
			ModelAndView mv=new ModelAndView();
			mv.setViewName("getotp");
			//System.out.println("---------1---------"+mv.getViewName());
			mv.addObject("transid", cr.getJocgTransactionId());
			//System.out.println("---------2---------"+cr.getJocgTransactionId());
			mv.addObject("otpId", otpJsonParser(respData));
//			System.out.println("---------3---------"+otpJsonParser(respData));
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setOtpPin(respData);
		}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
				cr.setResponseObject(cresp);
			
		}
	
		return cr;
	}
 
 */
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		
		if(cr.getMsisdn().longValue()<=0){
			ModelAndView mv=new ModelAndView();
			mv.setViewName("wregfreecharge");
			mv.addObject("transid", cr.getJocgTransactionId());
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Customer Requested to Enter Msisdn");
			cr.setResponseObject(cresp);
		}else{
			
			ChargingUrl churl=null;
			System.out.println("URL List "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("OTP-SEND".equalsIgnoreCase(curl.getUrlCatg())){ 
					churl=curl; break;
				}
			}

			List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());						
			
			//Invoke Generate OTP Pin
			String msisdnStr=""+cr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			FreechargeChargingParams fcParams=new FreechargeChargingParams();
			fcParams.setMobileNumber(msisdnStr);
			fcParams.setSecretKey(freechargeSecretKey);
					
			for(ChargingUrlParam up:urlParams) if("MERCHANTID".equalsIgnoreCase(up.getParamKey())) fcParams.setMerchantId(up.getDefaultValue());	
			String dataTemplate=fcParams.getJson("OTP", fcParams.getChecksum(loggerHead, "OTP"), true);
			
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/json");
			sslClient.setHttpMethod("GET");
			sslClient.setContentType("application/json");
			sslClient.setAcceptDataFormat("application/json");
			sslClient.setDestURL(churl.getUrl());
			sslClient.setRequestData(dataTemplate);
			sslClient.setConnectionTimeoutInSecond(10);
			sslClient.setReadTimeoutInSecond(20);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
			String respData=resp.getResponseData();
			System.out.println("Freecharge OTP API Response "+resp.getResponseData());
			
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if(statusCode==200  && respData.toUpperCase().contains("VERIFY")){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpfreecharge");
				//System.out.println("---------1---------"+mv.getViewName());
				mv.addObject("transid", cr.getJocgTransactionId());
				//System.out.println("---------2---------"+cr.getJocgTransactionId());
				mv.addObject("otpId", otpJsonParser(respData));
//				System.out.println("---------3---------"+otpJsonParser(respData));
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin(respData);
				cr.setResponseObject(cresp);
			}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
					cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
					cr.setResponseObject(cresp);
				
			}

		
		
		}
		
		
			
		return cr;
	}
	
	public JocgResponse sendOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="FREECHARGE::SENDOTP::"+cdr.getSubscriberId()+","+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			//Invoke Generate OTP Pin
			String msisdnStr=""+cdr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
		
			
			FreechargeChargingParams fcParams=new FreechargeChargingParams();
			fcParams.setMobileNumber(msisdnStr);
			fcParams.setSecretKey(freechargeSecretKey);
					
			for(ChargingUrlParam up:urlParams) 
				if("MERCHANTID".equalsIgnoreCase(up.getParamKey())) fcParams.setMerchantId(up.getDefaultValue());
			
			String dataTemplate=fcParams.getJson("OTP", fcParams.getChecksum(loggerHead, "OTP"), true);
			dataTemplate=dataTemplate.replaceAll("<EMAIL>", cdr.getEmailId());
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/json");
			sslClient.setHttpMethod("GET");
			sslClient.setContentType("application/json");
			sslClient.setAcceptDataFormat("application/json");
			sslClient.setDestURL(url.getUrl());
			sslClient.setRequestData(dataTemplate);
			sslClient.setConnectionTimeoutInSecond(10);
			sslClient.setReadTimeoutInSecond(20);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
			String respData=resp.getResponseData();
			System.out.println("Freecharge OTP API Response "+resp.getResponseData());
			
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if(statusCode==200  && respData.toUpperCase().contains("VERIFY")){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpfreecharge");
				mv.addObject("transid", cdr.getJocgTransId());
				mv.addObject("otpId", otpJsonParser(respData));
				mv.addObject("msisdn",cdr.getMsisdn());
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin(respData);
			}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
			}
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cresp.setResponseDescription("System Error! OTP Failed. Please try again later.");
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		}
		return cresp;
	}
	
	
	
	public String otpJsonParser(String json){	
		String otpId="";
	        String as[] = json.split(",");
	        for (String a : as) {
	            if (a.contains("otpId")) {
	                String such[] = a.split(":");
	                //System.out.println("------>" + such[1].replace("\"", ""));
	                otpId=such[1].replace("\"", "");
	                break;
	            }            
	        }  
	        return otpId;
	}
	public JocgResponse verifyOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="FREECHARGE::VERIFYOTP::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{	
			System.out.println(loggerHead+"URL "+url);			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){				
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;				
				FreechargeChargingParams fcParams=new FreechargeChargingParams();
				fcParams.setMobileNumber(msisdnStr);
				String otpIdEnterOtp[]=cdr.getOtpPin().split(",");
				fcParams.setOtp(otpIdEnterOtp[0]);
				fcParams.setOtpId(otpIdEnterOtp[1]);//Need to be checked while testing
				fcParams.setSecretKey(freechargeSecretKey);
				for(ChargingUrlParam up:urlParams) if("MERCHANTID".equalsIgnoreCase(up.getParamKey())) fcParams.setMerchantId(up.getDefaultValue());
				String dataTemplate=fcParams.getJson("VERIFY-OTP", fcParams.getChecksum(loggerHead, "VERIFY-OTP"), true);				
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/json");
				sslClient.setHttpMethod("POST");
				sslClient.setContentType("application/json");
				sslClient.setAcceptDataFormat("application/json");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				System.out.println(loggerHead+"Invoking URL....... ");				
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
				System.out.println(loggerHead+"URL resp "+resp);
				System.out.println("Freecharge Response "+resp.getResponseData());
				//Validate Response and set status accordingly
				String verfyRespData="NA";
				if(resp!=null)
				verfyRespData=parseverfyOTPResp(resp.getResponseData());	
				if(!verfyRespData.equals("NA")){
//				verifyOTP(cdr, url, chintf, urlParams);
					//CHECK-BALANCE
				
				balanceCheck(url,chintf,verfyRespData);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);				
				cresp.setResponseDescription("OTP Verification Failed! Please try again later.");
				}
				cresp.setAccessToken(resp.getResponseData());
				
				
			}else{
				//INvalid URL
				System.out.println(loggerHead+"Invalid URL");
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("OTP Verification Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return cresp;
	}
	
	public String balanceCheck(ChargingUrl url,ChargingInterface chintf,String accessToken)	
	{
//		String dataTemplate="?"
		JcmsSSLClient sslClient=new JcmsSSLClient();
		sslClient.setConnectionTimeoutInSecond(10);
		sslClient.setReadTimeoutInSecond(20);	
		sslClient.setHttpMethod("GET");
		sslClient.setAcceptDataFormat("application/json");
		url.setUrl("https://checkout.freecharge.in/api/v1/co/oauth/wallet/balance");
		sslClient.setContentType("application/x-www-form-urlencoded");		//
		sslClient.setDestURL(url.getUrl()+"?merchantId=CXQ2jtX6XBsZSC&accessToken="+accessToken);
//		sslClient.setRequestData(dataTemplate);
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLOxigenPlain("Freecharge BalanceCheck API :: ");			
		System.out.println("URL resp "+resp);
		System.out.println("Freecharge Response "+resp.getResponseData());
		return resp.getResponseData();
	}
	
	public JocgResponse chargeCustomer(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="FREECHARGE::VERIFYOTP::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
				
				FreechargeChargingParams fcParams=new FreechargeChargingParams();
				fcParams.setMobileNumber(msisdnStr);
				fcParams.setAmount(cdr.getOperatorPricePoint().intValue());
//				fcParams.setMerchantTxnId(cdr.getJocgTransId());
				fcParams.setMerchantTxnId(Long.toString(System.currentTimeMillis()));
				fcParams.setAccessToken(parseverfyOTPResp(cdr.getSubRegId()));
				fcParams.setSecretKey(freechargeSecretKey);
				for(ChargingUrlParam up:urlParams) {
					if("MERCHANTID".equalsIgnoreCase(up.getParamKey())) fcParams.setMerchantId(up.getDefaultValue());
					else if("CURRENCY".equalsIgnoreCase(up.getParamKey())) fcParams.setCurrency(up.getDefaultValue());
					else if("CHANNEL".equalsIgnoreCase(up.getParamKey())) fcParams.setChannel(up.getDefaultValue());
				}

				String dataTemplate=fcParams.getJson("CHARGE", fcParams.getChecksum(loggerHead, "CHARGE"), true);
				
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("application/json");
				sslClient.setContentType("application/json");
				sslClient.setHttpMethod("GET");
				sslClient.setContentType("application/json");
				sslClient.setAcceptDataFormat("application/json");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				System.out.println(loggerHead+"Invoking URL....... ");
				
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
				System.out.println(loggerHead+"URL resp "+resp);
				FreechargeResponse freechargeResp=responseWraper.parseResponse(resp.getResponseData());
				System.out.println("Freecharge Debit Response, Parse Object "+freechargeResp+"|Raw Data "+resp.getResponseData());
				
				//Validate response and set status
				//SuccessResponse:{"amount":"5.00","checksum":"67ee13987102cd6e118d688fe19c04d94ae239898a0b609d2342cc8006a88ebd","errorCode":null,"errorMessage":null,"merchantTxnId":"1515153632781","metadata":null,"runningBalance":null,"status":"COMPLETED","txnId":"CXQ2jtX6XBsZSC_1515153632781_1"}
			
				if(freechargeResp.getStatus()!=null && freechargeResp.getStatus().contains("COMPLETED")){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_SUCCESS);
					cresp.setResponseDescription("Charging Successful! Wallet response ("+freechargeResp.getStatus()+"-"+freechargeResp.getTxnId()+").");
				}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					cresp.setResponseDescription(freechargeResp.getStatus()+"-"+freechargeResp.getTxnId()+"|"+freechargeResp.getErrorMessage());
				}
				
				
			}else{
				//INvalid URL
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("Charging Failed! Please try again later.");
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return cresp;
	}
	
	public String parseverfyOTPResp(String Resp){
		String data="NA";		
        String as[] = Resp.split(",");
        for (String a : as) {
            if (a.contains("accessToken")) {
                String such[] = a.split(":");
                data=such[1].replace("\"", "");
                break;
            }            
        }        
		return data;
	}
	public String parseverfyOTPRespBalnaceCheck(String Resp){
		String data="NA";		
        String as[] = Resp.split(",");
        for (String a : as) {
            if (a.contains("walletBalance")) {
                String such[] = a.split(":");
                data=such[1].replace("\"", "").replace("}", "");
                break;
            }            
        }        
		return data;
	}
	//System.out.println("------>" + such[1].replace("\"", "").replace("}", ""));
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("UNSUB Successful");
		return jocgDeact;
	}
}
