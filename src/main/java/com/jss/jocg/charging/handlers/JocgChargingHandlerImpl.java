package com.jss.jocg.charging.handlers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.util.JocgStatusCodes;


public class JocgChargingHandlerImpl implements JocgChargingHandler {
	private static final Logger logger = LoggerFactory
			.getLogger(JocgChargingHandlerImpl.class);

	@Autowired JocgCacheManager jocgCache;
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		return cr;
	}
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
		jocgDeact.setStatusDescp("UNSUB Not Processed");
		return jocgDeact;
	}
	
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String invokeURL(String msisdn, String url1) {
        String urlResp = "";
        String loggerHead = "invokeURL() :: "+msisdn+" ::";
        long initTime = System.currentTimeMillis();
        try {
        	logger.info(loggerHead + " Invoking URL " + url1);
            java.net.URL url = new java.net.URL(url1);
            java.net.HttpURLConnection urlConn = (java.net.HttpURLConnection) url.openConnection();
            urlConn.setUseCaches(false);
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.connect();
            int statusCode = urlConn.getResponseCode();
            logger.info(loggerHead  + " URL Connected, Status Code : " + statusCode);
            java.io.DataInputStream dis = new java.io.DataInputStream(urlConn.getInputStream());
            logger.info(loggerHead  + " URL Connected, Status Code : " + statusCode);
            int x = 0;
            while ((x = dis.read()) != -1) {
                urlResp = urlResp + (char) x;
            }
            logger.info(loggerHead  + "  Data from URL : " + urlResp);
            dis.close();
            dis = null;
            urlConn.disconnect();
            urlConn = null;
            url = null;

        } catch (Exception e) {
        	logger.info(loggerHead  + " Exception " + e.getMessage());
            urlResp = urlResp + "-101";
        }
        long urlResponseTime = System.currentTimeMillis() - initTime;
        logger.info(loggerHead  + "  URL Response " + urlResp + " | Response Time (In Millis=" + urlResponseTime + ", In Seconds=" + (urlResponseTime / 1000) + ") ");
        return urlResp;

    }
}
