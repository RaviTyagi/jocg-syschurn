package com.jss.jocg.charging.handlers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.util.JocgStatusCodes;

public class CommonOffdeckChargingHandler extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(CommonOffdeckChargingHandler.class);

	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		return cr;
	}
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("UNSUB Successful");
		return jocgDeact;
	}
}
