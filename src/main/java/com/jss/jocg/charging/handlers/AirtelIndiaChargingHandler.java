package com.jss.jocg.charging.handlers;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.AirtelIndiaChargingParams;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.VodafoneChargingParams;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;

public class AirtelIndiaChargingHandler extends JocgChargingHandlerImpl{
	
	private static final Logger logger = LoggerFactory
			.getLogger(AirtelIndiaChargingHandler.class);
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-REDIRECT-APP".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			
			if("C".equalsIgnoreCase(cr.getRequestParams().getRequestCategory()) || cr.getChargingURL()==null || cr.getChargingURL().getChurlId()<=0){
				for(ChargingUrl curl:cr.getUrlList()){
					if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
						cr.setChargingURL(curl);break;
					}
				}
			}
			
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"Charging URL Params "+cr.getChargingUrlParams());
			
			String cgURL=cr.getChargingURL().getUrl()+"?"+cr.getChargingURL().getDataTemplate();
			logger.info(loggerHead+"CG URL Template "+cgURL);
			for(ChargingUrlParam up:cr.getChargingUrlParams()){
				if("CONSTANT".equalsIgnoreCase(up.getParamCatg())){
					String keyToReplace="<"+up.getParamKey()+">";
					cgURL=cgURL.replaceAll(keyToReplace, up.getDefaultValue());
				}
			}
			
			logger.info(loggerHead+"CG URL (After Adding Constant Params) "+cgURL+", Found charging Parameters "+cr.getChargingPrice().getOpParams());
			AirtelIndiaChargingParams chargingParams=new AirtelIndiaChargingParams();
			chargingParams.decodeParams(cr.getChargingPrice().getOpParams());
			logger.info(loggerHead+"Charging Params "+chargingParams);
			cgURL=addCGParameter(cgURL,cr.getJocgTransactionId(),cr.getChargingUrlParams(),chargingParams);
			
			cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
			cresp.setRedirectUrl(cgURL);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Forward to CG URL.....");
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e);
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
		String loggerHead=cr.getLoggerHead()+"AIRTELIN-CH :: ";
		ChargingResponse cresp=new ChargingResponse();
		ModelAndView mv=new ModelAndView();
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getOperatorURLList());
			ChargingUrl churl=null;
			for(ChargingUrl curl:cr.getOperatorURLList()){
				if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
					churl=curl; break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+churl+", url id "+churl.getChurlId());
			List<ChargingUrlParam> urlParams=cr.getOperatorUrlParams().get(churl.getChurlId());
			logger.info(loggerHead+"Charging URL Params "+urlParams);
			
			String cgURL=churl.getUrl()+"?"+churl.getDataTemplate();
			logger.info(loggerHead+"CG URL Template "+cgURL);
			for(ChargingUrlParam up:urlParams){
				if("CONSTANT".equalsIgnoreCase(up.getParamCatg())){
					String keyToReplace="<"+up.getParamKey()+">";
					cgURL=cgURL.replaceAll(keyToReplace, up.getDefaultValue());
				}
			}
			
			logger.info(loggerHead+"CG URL (After Adding Constant Params) "+cgURL+", Found charging Parameters "+cr.getSubscriber().getOperatorParams());
			AirtelIndiaChargingParams chargingParams=new AirtelIndiaChargingParams();
			chargingParams.decodeParams(cr.getSubscriber().getOperatorParams());
			logger.info(loggerHead+"Charging Params "+chargingParams);
			cgURL=addCGParameter(cgURL,cr.getJocgTransId(),urlParams,chargingParams);
			mv=new ModelAndView("redirect:"+cgURL);
			cresp.setRedirectURL(cgURL);
			cresp.setReturnType(ChargingResponse.RETR_TYPE_URL);
			cresp.setModelAndView(mv);
		}catch(Exception e){
			
		}
		
		return cresp;
	}
	
	public String addCGParameter(String cgURL,String jocgTransId,List<ChargingUrlParam> urlParams,AirtelIndiaChargingParams chargingParams){
		try{
			String valueToReplace="";
			for(ChargingUrlParam urlParam:urlParams){
				if(!urlParam.getParamCatg().equalsIgnoreCase("CONSTANT")){
					
					valueToReplace=("JOCG_TRANS_ID".equalsIgnoreCase(urlParam.getParamKey()) || "TRANS_ID".equalsIgnoreCase(urlParam.getParamKey()))?jocgTransId:
						("PRODUCT_ID".equalsIgnoreCase(urlParam.getParamKey()))?chargingParams.getProductId():
						("PRODUCT_NAME".equalsIgnoreCase(urlParam.getParamKey()))?chargingParams.getProductName():
						("PRODUCT_DESCP".equalsIgnoreCase(urlParam.getParamKey()))?chargingParams.getProductDescp():
						("CONTENT_TYPE".equalsIgnoreCase(urlParam.getParamKey()))?chargingParams.getContentType():
						("VALIDITY".equalsIgnoreCase(urlParam.getParamKey()))?""+chargingParams.getValidity():
						("AMOUNT".equalsIgnoreCase(urlParam.getParamKey()))?""+chargingParams.getAmount():
						("CLIENT_ID".equalsIgnoreCase(urlParam.getParamKey()))?chargingParams.getClientId():
						"NA";
					logger.info("Replacing <"+urlParam.getParamKey()+"> with "+valueToReplace);
						cgURL=cgURL.replaceAll("<"+urlParam.getParamKey()+">",valueToReplace);
				}
				
			}
		}catch(Exception e){
			logger.error("Exception while replacing parameter "+e);
		}
		
		return cgURL;
	}
	
	
	 @Override
		public JocgDeactivationResponse processDeactivation(JocgRequest newReq,HttpServletRequest requestContext){
			JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
			jocgDeact.setStatusDescp("UNSUB IN PROCCESS");
			SimpleDateFormat sdf=new SimpleDateFormat("yyMMddHHmmss");
			AirtelIndiaChargingParams aich=new AirtelIndiaChargingParams();
			if(aich.decodeParams(newReq.getSubscriber().getOperatorParams())){
				String url=newReq.getChargingURL().getUrl();
				String dataTemplate=newReq.getChargingURL().getDataTemplate();
				dataTemplate=dataTemplate.replaceAll("<PRODUCT_ID>", aich.getProductId());
				dataTemplate=dataTemplate.replaceAll("<MSISDN>",""+newReq.getMsisdn());
				
				logger.debug("Invoking URL "+url+", Data "+dataTemplate);
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setDestURL(url);
				sslClient.setContentType("application/json");
				sslClient.setAcceptDataFormat("application/json");
				sslClient.setHttpMethod("POST");
				sslClient.setUseBasicAuthentication(true);
				sslClient.setAuthUser(aich.getClientId());
				sslClient.setAuthPwd(aich.getSecretKey());
				sslClient.setRequestData(dataTemplate);
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectInsecureURLBasicAuth("VodaChurn:{"+newReq.getMsisdn()+"} :: ");
				logger.debug("URL Response "+resp.getResponseData());
				jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS_PENDING);
				jocgDeact.setStatusDescp("UNSUB Request Sent "+resp.getResponseData());
			}
			
			return jocgDeact;
		}

}
