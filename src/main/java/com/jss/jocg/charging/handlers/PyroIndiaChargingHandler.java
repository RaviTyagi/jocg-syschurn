package com.jss.jocg.charging.handlers;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.AirtelIndiaChargingParams;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.PyroChargingParams;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class PyroIndiaChargingHandler  extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(PyroIndiaChargingHandler.class);
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" :: ";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-SESSION".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"Charging URL Params "+cr.getChargingUrlParams());
			
			PyroChargingParams pcp=new PyroChargingParams();
			pcp.decodeParams(cr.getChargingPrice().getOpParams());
			
			String urlData=cr.getChargingURL().getDataTemplate();
			for(ChargingUrlParam up:cr.getChargingUrlParams()){
				String keyToReplace="<"+up.getParamKey()+">";
				if("CONSTANT".equalsIgnoreCase(up.getParamCatg())){
					if("REDIRECT_URL".equalsIgnoreCase(up.getParamKey()))
						urlData=urlData.replaceAll(keyToReplace,JocgString.getBase64EncodedString(up.getDefaultValue()));
					else
						urlData=urlData.replaceAll(keyToReplace, up.getDefaultValue());
				}else{
					
					if("TRANSID".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, cr.getChargingPrice().getCpid()+cr.getJocgTransactionId().substring(cr.getJocgTransactionId().lastIndexOf("_")+1).replaceAll("<SERVERID>", "120"));
					}else if("CPID".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, ""+cr.getChargingPrice().getCpid());
					}else if("SERVICE_KEY".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, pcp.getServiceKey());
					}else if("MSISDN".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, ""+(cr.getMsisdn()%10000000000L));
					}else if("SERVICE_ID".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, pcp.getServiceId());
					}else if("SERVICE_NAME".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, pcp.getServiceName());
					}else if("AMOUNT".equalsIgnoreCase(up.getParamKey())){
						urlData=urlData.replaceAll(keyToReplace, ""+pcp.getAmount());
					}
				}
			}
			
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setHttpMethod("GET");
			sslClient.setDestURL(cr.getChargingURL().getUrl()+urlData);
			logger.debug(loggerHead+"Invoking URL "+sslClient.getDestURL());
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.invokeURL(loggerHead,cr.getChargingURL().getUrl()+urlData);
			if(resp.getResponseCode()==200){
				logger.debug(loggerHead+"Session Id "+resp.getResponseData());
				//Form CG Redirect URL
				logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
				ChargingUrl cgUrl=null;
				for(ChargingUrl curl:cr.getUrlList()){
					if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
						cgUrl=curl;break;
					}
				}
				cr.setChargingUrlParams(cr.getUrlParams().get(cgUrl.getChurlId()));
				String sessionId=resp.getResponseData().substring(resp.getResponseData().lastIndexOf(":")+1);
				String finalCGRedirectUrl=cgUrl.getUrl()+cgUrl.getDataTemplate().replaceAll("<SESSION_ID>", sessionId);
				cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
				cresp.setRedirectUrl(finalCGRedirectUrl);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setResponseDescription("Forward to CG URL.....");
			}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("Failed to generate session Id, resp"+resp.getErrorMessage());
			}
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e);
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		if(cr.getPackageDetail()!=null && "PK413".equalsIgnoreCase(cr.getPackageDetail().getPkgname())){
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
			jocgDeact.setStatusDescp("UNSUB Successful");
		}else{
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
			jocgDeact.setStatusDescp("UNSUB Failed");
		}
		return jocgDeact;
	}
}
