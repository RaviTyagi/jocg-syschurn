package com.jss.jocg.charging.handlers;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.IdeaChargingParameters;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.NetExcelChargingParams;
import com.jss.jocg.util.JocgStatusCodes;

public class NetExcelIndiaChargingHandler  extends JocgChargingHandlerImpl{

	private static final Logger logger = LoggerFactory.getLogger(NetExcelIndiaChargingHandler.class);
	SimpleDateFormat timesdf=new SimpleDateFormat("yyyyMMddHHmmss");
	SimpleDateFormat sdf=new SimpleDateFormat("YYMMDDHHmmSS");
	@Override	
	public JocgRequest processCharging(JocgRequest cr, HttpServletRequest requestContext){
		String loggerHead=""+cr.getSubscriberId()+"-"+cr.getMsisdn()+" ::";
		JocgResponse cresp=new JocgResponse();
		cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		
		try{
			logger.info(loggerHead+"URL List inside Handler  "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
					cr.setChargingURL(curl);break;
				}
			}
			logger.info(loggerHead+"Processing for Charging URL "+cr.getChargingURL());
			cr.setChargingUrlParams(cr.getUrlParams().get(cr.getChargingURL().getChurlId()));
			logger.info(loggerHead+"Charging URL Params "+cr.getChargingUrlParams());
			NetExcelChargingParams nxlcp=new NetExcelChargingParams();
			nxlcp.decodeParams(cr.getChargingPrice().getOpParams());
			String dataTemplate=cr.getChargingURL().getDataTemplate();
			for(ChargingUrlParam cparam:cr.getChargingUrlParams()){
				if("CONSTANT".equalsIgnoreCase(cparam.getParamCatg())){
					if("CPID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cparam.getDefaultValue().trim());
					}else if("REDIRECTURL".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", ""+URLEncoder.encode(cparam.getDefaultValue()));
					}
				}
					else{
					if("TRANSID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", cr.getChargingPrice().getCpid()+cr.getJocgTransactionId().substring(cr.getJocgTransactionId().lastIndexOf("_")+1).replaceAll("<SERVERID>", "120"));
					}else if("SERVICEID".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", nxlcp.getServiceId());
					}else if("MSISDN".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", ""+(cr.getMsisdn()%10000000000L));
					}else if("TIMESTAMP".equalsIgnoreCase(cparam.getParamKey())){
						dataTemplate=dataTemplate.replaceAll("<"+cparam.getParamKey()+">", ""+sdf.format(new java.util.Date()));					
				}
			}
		}	
			String cgURL=(cr.getChargingURL().getUrl().endsWith("?"))?cr.getChargingURL().getUrl()+""+dataTemplate:cr.getChargingURL().getUrl()+"?"+dataTemplate;
			logger.info(loggerHead+"CG URL Template "+cgURL);
			
			cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
			cresp.setRedirectUrl(cgURL);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Forward to CG URL.....");				
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_EXCEPTION);
			cresp.setResponseDescription("Exception "+e);
		}
		cr.setResponseObject(cresp);
		return cr;
	}
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		if(cr.getPackageDetail()!=null && "PK413".equalsIgnoreCase(cr.getPackageDetail().getPkgname())){
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
			jocgDeact.setStatusDescp("UNSUB Successful");
		}else{
			jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_FAILED);
			jocgDeact.setStatusDescp("UNSUB Failed");
		}
		return jocgDeact;
	}
	
}
	
	