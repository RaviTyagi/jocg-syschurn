package com.jss.jocg.charging.handlers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.handlers.model.MPurseResponse;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.util.JocgStatusCodes;

public class NexGMpurseChargingHandler extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(NexGMpurseChargingHandler.class);
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		return cr;
	}
	
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
		// TODO Auto-generated method stub
		ChargingResponse cresp=new ChargingResponse();
		ChargingUrl churl=null;
		for(ChargingUrl curl:cr.getOperatorURLList()){
			if("SDP-CHARGE".equalsIgnoreCase(curl.getUrlCatg())){ 
				churl=curl; break;
			}
		}
		List<ChargingUrlParam> urlParams=cr.getOperatorUrlParams().get(churl.getChurlId());
		String mpurseParams=cr.getSubscriber().getOperatorParams();
		String mpurseKey=cr.getSubscriber().getOperatorServiceKey();
		String url=buildChargingURL(churl,urlParams,cr.getSubscriber().getOperatorPricePoint(),cr.getJocgTransId());
		String urlResp=invokeURL(""+cr.getMsisdn(),url);
		MPurseResponse mpurseResp=new MPurseResponse();
		boolean parseFlag=mpurseResp.parseMPurseResponse(urlResp);
		if(parseFlag){
			if(mpurseResp.getStatusCode()==2000){
				//successful charging
				cresp.setStatusCode(JocgStatusCodes.CHARGING_SUCCESS);
				cresp.setCgStatusCode("NA");
				cresp.setCgTransId("NA");
				cresp.setSdpChargedAmount(cr.getSubscriber().getOperatorPricePoint());
				cresp.setSdpStatus(mpurseResp.getResponseStatus());
				cresp.setSdpStatusCode(""+mpurseResp.getStatusCode());
				cresp.setSdpTransId(mpurseResp.getMpurseTransactionId());
			}else{
				cresp.setStatusCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setCgStatusCode("NA");
				cresp.setCgTransId("NA");
				cresp.setSdpChargedAmount(cr.getSubscriber().getOperatorPricePoint());
				cresp.setSdpStatus(mpurseResp.getResponseStatus());
				cresp.setSdpStatusCode(""+mpurseResp.getStatusCode());
				cresp.setSdpTransId(mpurseResp.getMpurseTransactionId());
			}
		}else{
			cresp.setCgStatus("NA");
			cresp.setCgStatusCode("NA");
			cresp.setCgTransId("NA");
			cresp.setSdpChargedAmount(0D);
			cresp.setStatusCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setSdpStatus("RESP_PARSE_FAILED");
			cresp.setStatusDescription("CHINTF URL Resp : "+urlResp);
			cresp.setSdpStatusCode("RESP_PARSE_FAILED");
			cresp.setSdpTransId("NA");
		}
		ModelAndView modelAndView=new ModelAndView();
		modelAndView.setViewName("mpurse");
		modelAndView.addObject("status",""+cresp.getSdpStatus());
		modelAndView.addObject("message",""+cresp.getSdpStatusCode()+"|"+cresp.getSdpTransId()+"|"+cresp.getSdpChargedAmount());
		cresp.setReturnType(ChargingResponse.RETR_TYPE_MODELANDVIEW);
		cresp.setModelAndView(modelAndView);
		return cresp;
	}
	
	private String buildChargingURL(ChargingUrl churl,List<ChargingUrlParam> urlParams,Double amount,String transactionId){
		String url=churl.getUrl()+churl.getDataTemplate();
		String keyToReplace="";
		for(ChargingUrlParam newParam:urlParams){
			try{
				keyToReplace="<"+newParam.getParamKey()+">";
				if(newParam.getParamCatg().equalsIgnoreCase("CONSTANT")){
					url=url.replaceAll(keyToReplace, newParam.getDefaultValue());
				}else{
					//Dynamic Params
					if("nexgtrxid".equalsIgnoreCase(newParam.getParamKey())){
						//Insert TransactionId
						url=url.replaceAll(keyToReplace, newParam.getDefaultValue());
					}else if("amount".equalsIgnoreCase(newParam.getParamKey()) || "debit".equalsIgnoreCase(newParam.getParamKey())){
						url=url.replaceAll(keyToReplace, ""+amount.doubleValue());
					}
				}
			}catch(Exception e){
				
			}
		}
		return url;
	}
	
	
	
	
}
