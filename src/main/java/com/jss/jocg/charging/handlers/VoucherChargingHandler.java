package com.jss.jocg.charging.handlers;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.VodafoneChargingParams;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;

public class VoucherChargingHandler extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(VoucherChargingHandler.class);
	
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest newReq,HttpServletRequest requestContext){
	  	String logHeader="processDeactivation("+newReq.getMsisdn()+") ::";
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("Customer Deactivated");
	
		return jocgDeact;
	}

}
