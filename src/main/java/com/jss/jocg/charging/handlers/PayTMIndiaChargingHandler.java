package com.jss.jocg.charging.handlers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.handlers.model.MPurseResponse;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.PaytmIndiaChargingParams;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;
import com.paytm.pg.merchant.CheckSumServiceHelper;

public class PayTMIndiaChargingHandler  extends JocgChargingHandlerImpl{
	static int customerIdCounter=20101;
	private static final Logger logger = LoggerFactory
			.getLogger(PayTMIndiaChargingHandler.class);
	
	SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		ChargingUrl churl=null;
		for(ChargingUrl curl:cr.getUrlList()){
			if("CG-REDIRECT".equalsIgnoreCase(curl.getUrlCatg())){ 
				churl=curl; break;
			}
		}
		logger.debug(loggerHead+" Step 2 "+churl);
		logger.debug(loggerHead+" Step 2.1 "+cr.getChargingPrice());
		logger.debug(loggerHead+" Step 2.2 "+cr.getJocgTransactionId());
		List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());
		logger.debug(loggerHead+" Step 3 "+urlParams);
		PaytmIndiaChargingParams payTmParams=buildChargingURL(cr,churl,urlParams,
				((cr.getChargedAmount()!=null && cr.getChargedAmount()>0)?cr.getChargedAmount():cr.getChargingPrice().getPricePoint()),
				cr.getJocgTransactionId());
		
		if(payTmParams!=null){
			ModelAndView mv=new ModelAndView();
			payTmParams.setCgURL(churl.getUrl());
			mv.setViewName("paytmcg");
			mv.addObject("ptmparams",payTmParams);
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
		}else{
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cresp.setResponseDescription("Failed to prepare CG URL");
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
			cr.setResponseObject(cresp);
		}
		/*String url=buildChargingURL(cr,churl,urlParams,
				((cr.getChargedAmount()!=null && cr.getChargedAmount()>0)?cr.getChargedAmount():cr.getChargingPrice().getPricePoint()),
				cr.getJocgTransactionId());
		logger.debug(loggerHead+" Step 4 "+url);
		if(url!=null && url.startsWith("http")){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("FWD To CG");
			cresp.setRedirectUrl(url);
			cresp.setRespType(JocgResponse.RESPTYPE_REDIRECTURL);
			cr.setResponseObject(cresp);
		}else{
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cresp.setResponseDescription("Failed to prepare CG URL");
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
			cr.setResponseObject(cresp);
		}*/
		logger.debug(loggerHead+" Step 5");
		
		return cr;
	}
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
		
		
		return null;
	}
	
	/**
	
		
	 * @param paytmChecksumKey
	 * @param dataTemplate
	 * @return
	 * @throws Exception
	 */
	private PaytmIndiaChargingParams buildChargingURL(JocgRequest newReq,ChargingUrl churl,List<ChargingUrlParam> urlParams,Double amount,String transactionId){
		//String url=churl.getUrl()+churl.getDataTemplate();
		String loggerHead="buildChargingURL():: {"+newReq.getSubscriberId()+","+newReq.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 3.1 : Charging URL  "+churl);
		String url=churl.getDataTemplate();
		String keyToReplace="";
		String paytmCheckSumKey="";
		PaytmIndiaChargingParams payTmParams=null;
		logger.debug(loggerHead+" Step 3.1 : Data Template "+url);
				
		for(ChargingUrlParam newParam:urlParams){
			
			try{
				if(newParam.getStatus()>0){
					
					keyToReplace="<"+newParam.getParamKey()+">";
					logger.debug(loggerHead+" Step 3.1.1 >>> Processing Param  "+keyToReplace+", Type "+newParam.getParamCatg()+", Default Value "+newParam.getDefaultValue());
					if(newParam.getParamCatg().equalsIgnoreCase("CONSTANT")){
						if("PAYTM_CHECKSUM_KEY".equalsIgnoreCase(newParam.getParamKey()))
							paytmCheckSumKey=newParam.getDefaultValue();
						else
							url=url.replaceAll(keyToReplace, newParam.getDefaultValue());
					}else{
						//Dynamic Params
						if("JOCG_TRANS_ID".equalsIgnoreCase(newParam.getParamKey())){
							//Insert TransactionId
							//transactionId=transactionId.substring(transactionId.lastIndexOf("_")+1);
							url=url.replaceAll(keyToReplace,transactionId);
						}else if("CUSTOMER_ID".equalsIgnoreCase(newParam.getParamKey())){
							customerIdCounter++;
							//url=url.replaceAll(keyToReplace, newReq.getSubscriberId());
							url=url.replaceAll(keyToReplace, ""+customerIdCounter);
						}else if("AMOUNT".equalsIgnoreCase(newParam.getParamKey()) || 
								"PACKAGE_PP".equalsIgnoreCase(newParam.getParamKey())){
							url=url.replaceAll(keyToReplace, ""+amount.intValue());
						}else if("VALIDITY".equalsIgnoreCase(newParam.getParamKey())){
							url=url.replaceAll(keyToReplace, ""+ (newReq.getChargingPrice().getValidity()));
						}else if("VALIDITY_END".equalsIgnoreCase(newParam.getParamKey())){
							Calendar cal=Calendar.getInstance();
							cal.setTimeInMillis(System.currentTimeMillis());
							cal.add(Calendar.DATE, newReq.getChargingPrice().getValidity()+1);
							url=url.replaceAll(keyToReplace, sdf.format(cal.getTime()));
						}
				
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		logger.debug(loggerHead+" Step 3.1.2 >>> Final Data   "+url);
		try{
			payTmParams=calculateCheckSum(paytmCheckSumKey,url);
			
		}catch(Exception e){}
		if(payTmParams!=null){
			return payTmParams;
		}else return null;
		
//		
//		logger.debug(loggerHead+" Step 3.1.2 >>> Final URL   "+url);
//		return url;
	}
	
	/*private String buildChargingURL(JocgRequest newReq,ChargingUrl churl,List<ChargingUrlParam> urlParams,Double amount,String transactionId){
		//String url=churl.getUrl()+churl.getDataTemplate();
		String loggerHead="buildChargingURL():: {"+newReq.getSubscriberId()+","+newReq.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 3.1 : Charging URL  "+churl);
		String url=churl.getDataTemplate();
		String keyToReplace="";
		String paytmCheckSumKey="";
		logger.debug(loggerHead+" Step 3.1 : Data Template "+url);
				
		for(ChargingUrlParam newParam:urlParams){
			
			try{
				if(newParam.getStatus()>0){
					
					keyToReplace="<"+newParam.getParamKey()+">";
					logger.debug(loggerHead+" Step 3.1.1 >>> Processing Param  "+keyToReplace+", Type "+newParam.getParamCatg()+", Default Value "+newParam.getDefaultValue());
					if(newParam.getParamCatg().equalsIgnoreCase("CONSTANT")){
						if("PAYTM_CHECKSUM_KEY".equalsIgnoreCase(newParam.getParamKey()))
							paytmCheckSumKey=newParam.getDefaultValue();
						else
							url=url.replaceAll(keyToReplace, newParam.getDefaultValue());
					}else{
						//Dynamic Params
						if("JOCG_TRANS_ID".equalsIgnoreCase(newParam.getParamKey())){
							//Insert TransactionId
							//transactionId=transactionId.substring(transactionId.lastIndexOf("_")+1);
							url=url.replaceAll(keyToReplace, transactionId);
						}else if("CUSTOMER_ID".equalsIgnoreCase(newParam.getParamKey())){
							customerIdCounter++;
							//url=url.replaceAll(keyToReplace, newReq.getSubscriberId());
							url=url.replaceAll(keyToReplace, ""+customerIdCounter);
						}else if("AMOUNT".equalsIgnoreCase(newParam.getParamKey()) || 
								"PACKAGE_PP".equalsIgnoreCase(newParam.getParamKey())){
							url=url.replaceAll(keyToReplace, ""+amount.intValue());
						}else if("VALIDITY".equalsIgnoreCase(newParam.getParamKey())){
							url=url.replaceAll(keyToReplace, ""+ newReq.getChargingPrice().getValidity());
						}else if("VALIDITY_END".equalsIgnoreCase(newParam.getParamKey())){
							Calendar cal=Calendar.getInstance();
							cal.setTimeInMillis(System.currentTimeMillis());
							
							cal.add(Calendar.DAY_OF_MONTH, newReq.getChargingPrice().getValidity());
							
							url=url.replaceAll(keyToReplace, sdf.format(cal.getTime()));
						}
				
					}
				}
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		logger.debug(loggerHead+" Step 3.1.2 >>> Final Data   "+url);
		try{
			
			String checkSum=calculateCheckSum(paytmCheckSumKey,url);
			logger.debug(loggerHead+" Step 3.1.3 >>> checkSum  "+checkSum);
			url=churl.getUrl()+url.replaceAll("<CHECKSUM>", checkSum);
		}catch(Exception e){}
		logger.debug(loggerHead+" Step 3.1.2 >>> Final URL   "+url);
		return url;
	}*/
	private PaytmIndiaChargingParams calculateCheckSum(String paytmChecksumKey,String dataTemplate) throws Exception{
		
		PaytmIndiaChargingParams payTmChargingParams=new PaytmIndiaChargingParams();
		dataTemplate=dataTemplate.substring(0,dataTemplate.lastIndexOf("&"));
		List<String> strList=JocgString.convertStrToList(dataTemplate, "&");
		TreeMap t=new TreeMap();
		for(String str:strList){
			try{
				
				String key=str.substring(0, str.indexOf("="));
				String value=str.substring(str.indexOf("=")+1);
				logger.debug(key+"="+value);
			t.put(key, value);
			}catch(Exception e){
				
			}
		}
		logger.debug("Paytm TreeMap "+t);
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		String checkSumHash=checksumHelper.genrateCheckSum(paytmChecksumKey, t);
		if(payTmChargingParams.extractParams(t, checkSumHash))
			return payTmChargingParams;
		else return null;
		
	}
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("UNSUB Successful");
		return jocgDeact;
	}
	
	/*private String calculateCheckSum(String paytmChecksumKey,String dataTemplate) throws Exception{
	
		dataTemplate=dataTemplate.substring(0,dataTemplate.lastIndexOf("&"));
		List<String> strList=JocgString.convertStrToList(dataTemplate, "&");
		TreeMap t=new TreeMap();
		for(String str:strList){
			try{
				
				String key=str.substring(0, str.indexOf("="));
				String value=str.substring(str.indexOf("=")+1);
				logger.debug(key+"="+value);
			t.put(key, value);
			}catch(Exception e){
				
			}
		}
		logger.debug("Paytm TreeMap "+t);
		CheckSumServiceHelper checksumHelper = CheckSumServiceHelper.getCheckSumServiceHelper();
		return checksumHelper.genrateCheckSum(paytmChecksumKey, t);
		
	}*/
	
}
