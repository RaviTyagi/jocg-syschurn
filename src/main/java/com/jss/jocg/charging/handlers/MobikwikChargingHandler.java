package com.jss.jocg.charging.handlers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import com.jss.jocg.cache.bo.JocgCacheManager;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.charging.model.ChargingRequest;
import com.jss.jocg.charging.model.ChargingResponse;
import com.jss.jocg.charging.model.FreechargeChargingParams;
import com.jss.jocg.charging.model.JocgDeactivationResponse;
import com.jss.jocg.charging.model.JocgRequest;
import com.jss.jocg.charging.model.JocgResponse;
import com.jss.jocg.charging.model.MobikwikChargingParams;
import com.jss.jocg.charging.model.PaytmIndiaChargingParams;
import com.jss.jocg.charging.model.parsers.MobikwikResponse;
import com.jss.jocg.services.dao.ChargingCDRRepository;
import com.jss.jocg.services.data.ChargingCDR;
import com.jss.jocg.util.JcmsSSLClient;
import com.jss.jocg.util.JocgStatusCodes;
import com.jss.jocg.util.JocgString;

public class MobikwikChargingHandler  extends JocgChargingHandlerImpl{
	private static final Logger logger = LoggerFactory
			.getLogger(MobikwikChargingHandler.class);
	
	public MobikwikChargingHandler(){}
	
	/*@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		ChargingUrl churl=null;
		System.out.println("URL List "+cr.getUrlList());
		for(ChargingUrl curl:cr.getUrlList()){
			if("OTP-SEND".equalsIgnoreCase(curl.getUrlCatg())){ 
				churl=curl; break;
			}
		}
		logger.debug(loggerHead+" Step 2 "+churl);
		logger.debug(loggerHead+" Step 2.1 "+cr.getChargingPrice());
		logger.debug(loggerHead+" Step 2.2 "+cr.getJocgTransactionId());
		List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());
		logger.debug(loggerHead+" Step 3 "+urlParams);
		
		//Invoke Generate OTP Pin
		MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
		String msisdnStr=""+cr.getMsisdn();
		msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
		
		mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
		//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
		mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
		String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_GENOTP, churl.getDataTemplate());
		
		JcmsSSLClient sslClient=new JcmsSSLClient();
		sslClient.setAcceptDataFormat("text/plain");
		sslClient.setContentType("application/url-encoded");
		sslClient.setHttpMethod("POST");
		sslClient.setContentType("application/x-www-form-urlencoded");
		sslClient.setAcceptDataFormat("text/plain");
		sslClient.setDestURL(churl.getUrl());
		sslClient.setRequestData(dataTemplate);
		JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
		MobikwikResponse mp=new MobikwikResponse();
		mp.parseXML(resp.getResponseData());
		System.out.println("Mobikwik Response "+mp);
		String respData=resp.getResponseData();
		int statusCode=resp.getResponseCode();
		int respTimeInSecond=resp.getResponseTime();
		
		if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
			ModelAndView mv=new ModelAndView();
			mv.setViewName("getotp");
			mv.addObject("transid", cr.getJocgTransactionId());
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setOtpPin("SUCCESS");
		}else{
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
				cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
				cr.setResponseObject(cresp);
		}

		
		logger.debug(loggerHead+" Step 5");
		
		return cr;
	}
	*/
	
	@Override
	public JocgRequest processCharging(JocgRequest cr,HttpServletRequest requestContext){
		String loggerHead=" {"+cr.getSubscriberId()+","+cr.getMsisdn()+"} :: ";
		logger.debug(loggerHead+" Step 1");
		JocgResponse cresp=cr.getResponseObject();
		if(cr.getMsisdn().longValue()<=0){
			ModelAndView mv=new ModelAndView();
			mv.setViewName("wregmobikwick");
			mv.addObject("transid", cr.getJocgTransactionId());
			cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
			cresp.setRespView(mv);
			cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
			cresp.setResponseDescription("Customer Requested to Enter Msisdn");
			cr.setResponseObject(cresp);
		}else{
			ChargingUrl churl=null;
			System.out.println("URL List "+cr.getUrlList());
			for(ChargingUrl curl:cr.getUrlList()){
				if("OTP-SEND".equalsIgnoreCase(curl.getUrlCatg())){ 
					churl=curl; break;
				}
			}
			logger.debug(loggerHead+" Step 2 "+churl);
			logger.debug(loggerHead+" Step 2.1 "+cr.getChargingPrice());
			logger.debug(loggerHead+" Step 2.2 "+cr.getJocgTransactionId());
			List<ChargingUrlParam> urlParams=cr.getUrlParams().get(churl.getChurlId());
			logger.debug(loggerHead+" Step 3 "+urlParams);
			
			//Invoke Generate OTP Pin
			MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
			String msisdnStr=""+cr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			
			mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
			//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
			mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
			String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_GENOTP, churl.getDataTemplate());
			
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/url-encoded");
			sslClient.setHttpMethod("POST");
			sslClient.setContentType("application/x-www-form-urlencoded");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setDestURL(churl.getUrl());
			sslClient.setRequestData(dataTemplate);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
			MobikwikResponse mp=new MobikwikResponse();
			mp.parseXML(resp.getResponseData());
			System.out.println("Mobikwik Response "+mp);
			String respData=resp.getResponseData();
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpmobikwik");
				mv.addObject("msisdn",""+cr.getMsisdn().longValue());
				mv.addObject("transid", cr.getJocgTransactionId());
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin("SUCCESS");
				cr.setResponseObject(cresp);
			}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
					cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
					cr.setResponseObject(cresp);
			}
		}
		

		
		logger.debug(loggerHead+" Step 5");
		
		return cr;
	}
	
	public JocgResponse sendOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="FREECHARGE::SENDOTP::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			//Invoke Generate OTP Pin
			//Invoke Generate OTP Pin
			MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
			String msisdnStr=""+cdr.getMsisdn();
			msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
			
			mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
			//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
			mobikwikParams.extractParams(logger, loggerHead, cdr.getOperatorPricePoint().intValue(), "", cdr.getJocgTransId(), "",  cdr.getJocgTransId(), urlParams);
			String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_GENOTP, url.getDataTemplate());
			
					
			JcmsSSLClient sslClient=new JcmsSSLClient();
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setContentType("application/url-encoded");
			sslClient.setHttpMethod("POST");
			sslClient.setContentType("application/x-www-form-urlencoded");
			sslClient.setAcceptDataFormat("text/plain");
			sslClient.setDestURL(url.getUrl());
			sslClient.setRequestData(dataTemplate);
			JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
			MobikwikResponse mp=new MobikwikResponse();
			mp.parseXML(resp.getResponseData());
			System.out.println("Mobikwik Response "+mp);
			String respData=resp.getResponseData();
			int statusCode=resp.getResponseCode();
			int respTimeInSecond=resp.getResponseTime();
			
			if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
				ModelAndView mv=new ModelAndView();
				mv.setViewName("otpmobikwik");
				mv.addObject("transid", cdr.getJocgTransId());
				mv.addObject("msisdn",""+cdr.getMsisdn().longValue());
				
				cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
				cresp.setRespView(mv);
				cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
				cresp.setOtpPin("SUCCESS");
			}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cresp.setResponseDescription("OTP Generation Failed! Please try again later.");
					cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
					
			}
			
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
			cresp.setResponseDescription("System Error! OTP Failed. Please try again later.");
			cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		}
		return cresp;
	}
	
	
	public JocgResponse verifyOTP(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="MOBIKWIK::VERIFYOTP::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				
				MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
				
				mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
				//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
				mobikwikParams.extractParams(logger, loggerHead, cdr.getOperatorPricePoint(), cdr.getOtpPin(), cdr.getJocgTransId(), "",cdr.getJocgTransId(), urlParams);
				String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_VALIDATEOTP, url.getDataTemplate());
				
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/url-encoded");
				sslClient.setHttpMethod("POST");
				sslClient.setContentType("application/x-www-form-urlencoded");
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				System.out.println(loggerHead+"Invoking URL....... ");
				
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
				MobikwikResponse mp=new MobikwikResponse();
				mp.parseXML(resp.getResponseData());
				System.out.println(loggerHead+"Mobikwik Response "+mp);
				String respData=resp.getResponseData();
				int statusCode=resp.getResponseCode();
				int respTimeInSecond=resp.getResponseTime();
				if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
					cresp.setResponseDescription(mp.getStatusDescription());
					cresp.setAccessToken(mp.getAccessToken());
				}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
					cresp.setResponseDescription("Token Request Failed!"+mp.getStatusDescription());
				}
				System.out.println(loggerHead+"URL resp "+resp);
				System.out.println(loggerHead+"Mobikwik Response "+resp.getResponseData());
				
			}else{
				//Invalid URL
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED_CGCONSENT);
				cresp.setResponseDescription("Token Request Failed!");
				System.out.println(loggerHead+"Invalid Token URL  "+url);
			}
		}catch(Exception e){
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setResponseDescription("System Error!");
			e.printStackTrace();
		}
		logger.debug(loggerHead+" verify OTP process resp "+cresp);
		return cresp;
	}
	
	public JocgResponse chargeCustomer(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams){
		String loggerHead="MOBIKWIK::chargeCustomer::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				
				MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
				mobikwikParams.setAmount(cdr.getOperatorPricePoint().intValue());
				mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
				//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
				mobikwikParams.extractParams(logger, loggerHead, cdr.getOperatorPricePoint(), "", cdr.getJocgTransId(), cdr.getSubRegId(),cdr.getJocgTransId(), urlParams);
				String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_CHARGE, url.getDataTemplate());
				
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/url-encoded");
				sslClient.setHttpMethod("POST");
				sslClient.setContentType("application/x-www-form-urlencoded");
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				sslClient.setConnectionTimeoutInSecond(10);
				sslClient.setReadTimeoutInSecond(20);
				System.out.println(loggerHead+"Invoking URL....... ");
				
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
				MobikwikResponse mp=new MobikwikResponse();
				mp.parseXML(resp.getResponseData());
				System.out.println(loggerHead+"Mobikwik Response "+mp);
				String respData=resp.getResponseData();
				int statusCode=resp.getResponseCode();
				int respTimeInSecond=resp.getResponseTime();
				if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_SUCCESS);
					cresp.setResponseDescription("Charging Successful! Wallet response ("+mp.getStatusDescription()+")");
					cresp.setAccessToken(mp.getAccessToken());
				}else if(mp.getStatusCode()==159){
					//Create new Wallet
					cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
					cresp.setResponseDescription("Wallet Account does not exist! Create new wallet.");
					cresp.setAccessToken(cdr.getSubRegId());
					cresp.setRespType(JocgResponse.RESPTYPE_MODELANDVIEW);
					ModelAndView mv=new ModelAndView();
					mv.setViewName("cwmkwik");
					mv.addObject("transid", cdr.getJocgTransId());
					cresp.setRespView(mv);
				}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					if(resp!=null)
					cresp.setResponseDescription("Charge Request Failed! Error code "+mp.getStatusCode()+"|"+mp.getStatusDescription());
					else
						cresp.setResponseDescription("Charge Request Failed! Debit API Response not received.");
				}
				System.out.println(loggerHead+"URL resp "+resp);
				System.out.println(loggerHead+"Mobikwik Response "+resp.getResponseData());
				
			}else{
				//INvalid URL
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("Charge Request Failed! Failed to connect wallet API.");
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setResponseDescription("Charge Request Failed! Error occured while connecting wallet API.");
		}
		logger.debug(loggerHead+" charged wallet process resp "+cresp);
		return cresp;
	}
	
	public JocgResponse createNewWallet(ChargingCDR cdr,ChargingUrl url,ChargingInterface chintf, List<ChargingUrlParam> urlParams,String emailId){
		String loggerHead="MOBIKWIK::CREATEWALLET::"+cdr.getMsisdn().longValue()+"::";
		JocgResponse cresp=new JocgResponse();
		cresp.setRespType(JocgResponse.RESPTYPE_TEXT);
		try{
			
			System.out.println(loggerHead+"URL "+url);
			
			if(url!=null && url.getChurlId()>0 && urlParams!=null && urlParams.size()>0){
				
				MobikwikChargingParams mobikwikParams=new MobikwikChargingParams();
				String msisdnStr=""+cdr.getMsisdn().longValue();
				msisdnStr=(msisdnStr.length()>10)?msisdnStr.substring(msisdnStr.length()-10):msisdnStr;
				
				mobikwikParams.setMsisdn(JocgString.strToLong(msisdnStr, 0L));
				//mobikwikParams.extractParams(logger, loggerHead, cr.getChargingPrice().getPricePoint(), "", cr.getJocgTransactionId(), "", cr.getJocgTransactionId(), urlParams);
				mobikwikParams.extractParams(logger, loggerHead, cdr.getOperatorPricePoint(), "", cdr.getJocgTransId(), cdr.getSubRegId(),cdr.getJocgTransId(), urlParams);
				mobikwikParams.setEmailId(emailId);
				String dataTemplate=mobikwikParams.generateChecksum(logger, loggerHead, MobikwikChargingParams.RQTYPE_CREATEWALLET, url.getDataTemplate());
				
				System.out.println(loggerHead+"URL To Invoke "+url.getUrl());
				System.out.println(loggerHead+"URL Data "+dataTemplate);
				
				JcmsSSLClient sslClient=new JcmsSSLClient();
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setContentType("application/url-encoded");
				sslClient.setHttpMethod("POST");
				sslClient.setContentType("application/x-www-form-urlencoded");
				sslClient.setAcceptDataFormat("text/plain");
				sslClient.setDestURL(url.getUrl());
				sslClient.setRequestData(dataTemplate);
				System.out.println(loggerHead+"Invoking URL....... ");
				
				JcmsSSLClient.JcmsSSLClientResponse resp=sslClient.connectURLViaSSLIgnore(loggerHead);
				MobikwikResponse mp=new MobikwikResponse();
				mp.parseXML(resp.getResponseData());
				System.out.println(loggerHead+"Mobikwik Response "+mp);
				String respData=resp.getResponseData();
				int statusCode=resp.getResponseCode();
				int respTimeInSecond=resp.getResponseTime();
				if("SUCCESS".equalsIgnoreCase(mp.getStatus()) && mp.getStatusCode()==0){
					cresp.setResponseCode(JocgStatusCodes.CHARGING_INPROCESS);
					cresp.setResponseDescription(mp.getStatusDescription());
					cresp.setAccessToken(mp.getAccessToken());
				}else{
					cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
					cresp.setResponseDescription("Create Wallet Request Failed! Error code "+mp.getStatusCode());
				}
				System.out.println(loggerHead+"URL resp "+resp);
				System.out.println(loggerHead+"Mobikwik Response "+resp.getResponseData());
				
			}else{
				//INvalid URL
				System.out.println(loggerHead+"Invalid URL");
				cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
				cresp.setResponseDescription("Create Wallet Request Failed!");
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			cresp.setResponseCode(JocgStatusCodes.CHARGING_FAILED);
			cresp.setResponseDescription("Create Wallet Request Failed! Error occured while connecting Wallet API.");
		}
		
		return cresp;
	}
	
	
	
	
	
	
	
	@Override
	public ChargingResponse processCharging(ChargingRequest cr, HttpServletRequest requestContext) {
	return null;
	}
	
	
	
	
	
	
	@Override
	public JocgDeactivationResponse processDeactivation(JocgRequest cr,HttpServletRequest requestContext){
		JocgDeactivationResponse jocgDeact=new JocgDeactivationResponse();
		jocgDeact.setStatusCode(JocgStatusCodes.UNSUB_SUCCESS);
		jocgDeact.setStatusDescp("UNSUB Successful");
		return jocgDeact;
	}

}
