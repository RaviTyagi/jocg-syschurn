package com.jss.jocg.charging.model;

import java.util.List;

import com.jss.jocg.util.JocgString;

public class JocgChargingParams {
	
	public final String KEY_PARKING="PARKING:";
	public final String KEY_GRACE="GRACE:";
	public final String KEY_SUSPENSION="SUSPENSION:";
	Integer parking;
	Integer grace;
	Integer suspension;
	
	
	
	public JocgChargingParams(){
		
	}
	
	
	public boolean decodeCommonParams(List<String> paramList){
		boolean flag=true;
		try{
			
			for(String p:paramList){
				if(p.startsWith(KEY_PARKING)){
					this.setParking(Integer.parseInt(p.substring(p.indexOf(":")+1)));
				}else if(p.startsWith(KEY_GRACE)){
					this.setGrace(Integer.parseInt(p.substring(p.indexOf(":")+1)));
				}else if(p.startsWith(KEY_SUSPENSION)){
					this.setSuspension(Integer.parseInt(p.substring(p.indexOf(":")+1)));
				}
			}
		}catch(Exception e){
			
		}
		
		return flag;
	}

	
	public Integer getParking() {
		return parking;
	}


	public void setParking(Integer parking) {
		this.parking = parking;
	}


	public Integer getGrace() {
		return grace;
	}


	public void setGrace(Integer grace) {
		this.grace = grace;
	}


	public Integer getSuspension() {
		return suspension;
	}


	public void setSuspension(Integer suspension) {
		this.suspension = suspension;
	}


	
	
}
