package com.jss.jocg.charging.model.parsers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.jackson.map.ObjectMapper;

import com.jss.jocg.cache.entities.config.json.NotifyConfig;

public class FreechargeResponseWrapper {
	private final static ObjectMapper objectMapper = new ObjectMapper();
	static Log logger = LogFactory.getLog(FreechargeResponseWrapper.class.getName());
	
	public FreechargeResponseWrapper(){
		
	}
	
	public FreechargeResponse parseResponse(String respdata){
		 try {
		      return objectMapper.readValue(respdata, FreechargeResponse.class);
	    } catch (Exception ex) {
	       logger.error("convertToEntityAttribute::" + respdata+", exception: "+ex);
	      return null;
	    }
	}
	
}
