package com.jss.jocg.charging.model.parsers;

import java.lang.reflect.Field;

public class FreechargeResponse {
/**
 *  {"amount":"5.00","checksum":"157cf8791c4428fb519622eb938b85b40db2f3d647fcfee801e4a5aeeafd13a9",
 "errorCode":null,"errorMessage":null,"merchantTxnId":"1515397602094","metadata":null,"runningBalance":null,
 "status":"COMPLETED","txnId":"CXQ2jtX6XBsZSC_1515397602094_1"}.
 */
Double amount;
String checksum;
String errorCode;
String errorMessage;
String merchantTxnId;
String metadata;
Double runningBalance; 
String status;
String txnId;

public FreechargeResponse(){
	
}

@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}

public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}
public String getChecksum() {
	return checksum;
}
public void setChecksum(String checksum) {
	this.checksum = checksum;
}
public String getErrorCode() {
	return errorCode;
}
public void setErrorCode(String errorCode) {
	this.errorCode = errorCode;
}
public String getErrorMessage() {
	return errorMessage;
}
public void setErrorMessage(String errorMessage) {
	this.errorMessage = errorMessage;
}
public String getMerchantTxnId() {
	return merchantTxnId;
}
public void setMerchantTxnId(String merchantTxnId) {
	this.merchantTxnId = merchantTxnId;
}
public String getMetadata() {
	return metadata;
}
public void setMetadata(String metadata) {
	this.metadata = metadata;
}
public Double getRunningBalance() {
	return runningBalance;
}
public void setRunningBalance(Double runningBalance) {
	this.runningBalance = runningBalance;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getTxnId() {
	return txnId;
}
public void setTxnId(String txnId) {
	this.txnId = txnId;
}




	
}
