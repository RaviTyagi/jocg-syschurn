package com.jss.jocg.charging.model;

import java.util.List;

import com.ibm.wcg.utility.decryption.agc.AESEncryptionDecryptionUtil;
import com.jss.jocg.util.JocgString;

public class IdeaChargingParameters  extends JocgChargingParams{

String serviceKey;


public IdeaChargingParameters(){
	
}

public boolean decodeParams(String opParams){
	boolean flag=true;
	try{
		List<String> paramList=JocgString.convertStrToList(opParams, "|");
		decodeCommonParams(paramList);
		for(String p:paramList){
			if(p.startsWith("SERVICE_KEY")){
				this.setServiceKey(p.substring(p.indexOf(":")+1));
			}
		}
	}catch(Exception e){
		System.out.println("Exception while parsing opparams "+e);
		flag=false;
	}
	
	return flag;
}
public String getEncryptedParameter(String paramVal){
	return AESEncryptionDecryptionUtil.encrypt(paramVal);
}

public String getServiceKey() {
	return serviceKey;
}
public void setServiceKey(String serviceKey) {
	this.serviceKey = serviceKey;
}

	
}
