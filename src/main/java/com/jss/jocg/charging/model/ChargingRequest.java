package com.jss.jocg.charging.model;

import java.util.List;
import java.util.Map;

import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.services.data.Subscriber;

public class ChargingRequest {
	
	
	
	List<ChargingUrl> operatorURLList;
	Map<Integer,List<ChargingUrlParam>> operatorUrlParams;
	Subscriber subscriber;
	Long msisdn;
	String subscriberId;
	String remoteIP;
	String loggerHead="";
	Circleinfo circleInfo;
	String trafficSourceToken;
	String device;
	String jocgTransId;
	String cgImageURL;
	
	public ChargingRequest(){
	
		
	}

	public Map<Integer, List<ChargingUrlParam>> getOperatorUrlParams() {
		return operatorUrlParams;
	}

	public void setOperatorUrlParams(Map<Integer, List<ChargingUrlParam>> operatorUrlParams) {
		this.operatorUrlParams = operatorUrlParams;
	}

	public Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public Long getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public String getRemoteIP() {
		return remoteIP;
	}

	public void setRemoteIP(String remoteIP) {
		this.remoteIP = remoteIP;
	}

	public List<ChargingUrl> getOperatorURLList() {
		return operatorURLList;
	}

	public void setOperatorURLList(List<ChargingUrl> operatorURLList) {
		this.operatorURLList = operatorURLList;
	}

	public String getLoggerHead() {
		return loggerHead;
	}

	public void setLoggerHead(String loggerHead) {
		this.loggerHead = loggerHead;
	}

	public Circleinfo getCircleInfo() {
		return circleInfo;
	}

	public void setCircleInfo(Circleinfo circleInfo) {
		this.circleInfo = circleInfo;
	}

	public String getTrafficSourceToken() {
		return trafficSourceToken;
	}

	public void setTrafficSourceToken(String trafficSourceToken) {
		this.trafficSourceToken = trafficSourceToken;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getJocgTransId() {
		return jocgTransId;
	}

	public void setJocgTransId(String jocgTransId) {
		this.jocgTransId = jocgTransId;
	}

	public String getCgImageURL() {
		return cgImageURL;
	}

	public void setCgImageURL(String cgImageURL) {
		this.cgImageURL = cgImageURL;
	}
	
	
	
	
	
}
