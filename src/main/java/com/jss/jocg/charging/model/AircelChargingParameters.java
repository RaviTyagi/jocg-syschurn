package com.jss.jocg.charging.model;

import java.lang.reflect.Field;
import java.util.List;

import com.ibm.wcg.utility.decryption.agc.AESEncryptionDecryptionUtil;
import com.jss.jocg.util.JocgString;

public class AircelChargingParameters  extends JocgChargingParams{
//PRODUCTID:DIGTVD|PDESC:Aircel%20NexGTV|MPNAME:Aircel%20NexGTV|PRICE:2|VALIDITY:1
	String productid;
	String pdesc;
	String mpname;
	Integer price;
	Integer validity;
	public AircelChargingParameters(){}
	
	public boolean decodeParams(String opParams){
		boolean flag=true;
		try{
			List<String> paramList=JocgString.convertStrToList(opParams, "|");
			decodeCommonParams(paramList);
			for(String p:paramList){
				if(p.startsWith("PRODUCTID")){
					this.setProductid(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("MPNAME")){
					this.setMpname(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("PDESC")){
					this.setPdesc(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("PRICE")){
					this.setPrice(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("VALIDITY")){
					this.setValidity(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}
				
				
			}
		}catch(Exception e){
			System.out.println("Exception while parsing opparams "+e);
			flag=false;
		}
		
		return flag;
	}
	
	
	
	@Override
	public String toString() {
			
	        Field[] fields = this.getClass().getDeclaredFields();
	        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	        try {
	            for (Field field : fields) {
	                str.append( field.getName()).append("=").append(field.get(this)).append(",");
	            }
	        } catch (IllegalArgumentException ex) {
	            System.out.println(ex);
	        } catch (IllegalAccessException ex) {
	            System.out.println(ex);
	        }
	        str.append("}");
	        return str.toString();
	    }

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getPdesc() {
		return pdesc;
	}

	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}

	public String getMpname() {
		return mpname;
	}

	public void setMpname(String mpname) {
		this.mpname = mpname;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}
	
	
	
}
