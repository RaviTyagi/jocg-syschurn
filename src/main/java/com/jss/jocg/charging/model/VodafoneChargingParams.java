package com.jss.jocg.charging.model;

import java.util.List;

import com.jss.jocg.util.JocgString;

public class VodafoneChargingParams extends JocgChargingParams{
	//PARKING:|GRACE:|SUSPENSION:|SERVICE:|CLASS:
	public final String KEY_SERVICE="SERVICE:";
	public final String KEY_CLASS="CLASS:";
	String Service;
	String Loginid;
	String password;
	String org_id;
	String EncryptionKey;
	String from;
	String consentStatus;
	String consentMsg;
	String CallBackURL;
	String CLASS;
	String MSISDN;
	String mode;
	String requestid;
	String Requesttime;
	String param1;
	String param3;
	String CircleId;
	String cgURL;
	
	
	public boolean decodeParams(String opParams){
		boolean flag=true;
		try{
			List<String> paramList=JocgString.convertStrToList(opParams, "|");
			decodeCommonParams(paramList);
			for(String p:paramList){
				if(p.startsWith(KEY_SERVICE)){
					this.setService(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith(KEY_CLASS)){
					this.setCLASS(p.substring(p.indexOf(":")+1));
				}
			}
		}catch(Exception e){
			System.out.println("Exception while parsing opparams "+e);
			flag=false;
		}
		
		return flag;
	}


	public String getService() {
		return Service;
	}


	public void setService(String service) {
		Service = service;
	}


	public String getLoginid() {
		return Loginid;
	}


	public void setLoginid(String loginid) {
		Loginid = loginid;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getOrg_id() {
		return org_id;
	}


	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}


	public String getEncryptionKey() {
		return EncryptionKey;
	}


	public void setEncryptionKey(String encryptionKey) {
		EncryptionKey = encryptionKey;
	}


	public String getFrom() {
		return from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getConsentStatus() {
		return consentStatus;
	}


	public void setConsentStatus(String consentStatus) {
		this.consentStatus = consentStatus;
	}


	public String getConsentMsg() {
		return consentMsg;
	}


	public void setConsentMsg(String consentMsg) {
		this.consentMsg = consentMsg;
	}


	public String getCallBackURL() {
		return CallBackURL;
	}


	public void setCallBackURL(String callBackURL) {
		CallBackURL = callBackURL;
	}


	

	public String getCLASS() {
		return CLASS;
	}


	public void setCLASS(String classValue) {
		this.CLASS = classValue;
	}


	public String getMSISDN() {
		return MSISDN;
	}


	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}


	public String getMode() {
		return mode;
	}


	public void setMode(String mode) {
		this.mode = mode;
	}


	public String getRequestid() {
		return requestid;
	}


	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}


	public String getRequesttime() {
		return Requesttime;
	}


	public void setRequesttime(String requesttime) {
		Requesttime = requesttime;
	}


	public String getParam1() {
		return param1;
	}


	public void setParam1(String param1) {
		this.param1 = param1;
	}


	public String getParam3() {
		return param3;
	}


	public void setParam3(String param3) {
		this.param3 = param3;
	}


	public String getCircleId() {
		return CircleId;
	}


	public void setCircleId(String circleId) {
		CircleId = circleId;
	}


	public String getCgURL() {
		return cgURL;
	}


	public void setCgURL(String cgURL) {
		this.cgURL = cgURL;
	}


	

	
	
	

}
