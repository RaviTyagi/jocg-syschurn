package com.jss.jocg.charging.model;

import org.springframework.web.servlet.ModelAndView;

public class ChargingResponse {
	public static final int RETR_TYPE_MODELANDVIEW=0;
	public static final int RETR_TYPE_URL=1;
	int statusCode;
	String statusDescription;
	String cgTransId;
	String cgStatus;
	String cgStatusCode;
	String sdpTransId;
	String sdpStatus;
	String sdpStatusCode;
	Integer otpStatus;
	String otpPin;
	Double sdpChargedAmount;
	int returnType;
	String redirectURL;
	ModelAndView modelAndView;
	
	
	
	public ChargingResponse(){}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public String getCgTransId() {
		return cgTransId;
	}

	public void setCgTransId(String cgTransId) {
		this.cgTransId = cgTransId;
	}

	public String getCgStatus() {
		return cgStatus;
	}

	public void setCgStatus(String cgStatus) {
		this.cgStatus = cgStatus;
	}

	public String getCgStatusCode() {
		return cgStatusCode;
	}

	public void setCgStatusCode(String cgStatusCode) {
		this.cgStatusCode = cgStatusCode;
	}

	public String getSdpTransId() {
		return sdpTransId;
	}

	public void setSdpTransId(String sdpTransId) {
		this.sdpTransId = sdpTransId;
	}

	public String getSdpStatus() {
		return sdpStatus;
	}

	public void setSdpStatus(String sdpStatus) {
		this.sdpStatus = sdpStatus;
	}

	public String getSdpStatusCode() {
		return sdpStatusCode;
	}

	public void setSdpStatusCode(String sdpStatusCode) {
		this.sdpStatusCode = sdpStatusCode;
	}

	public Integer getOtpStatus() {
		return otpStatus;
	}

	public void setOtpStatus(Integer otpStatus) {
		this.otpStatus = otpStatus;
	}

	public String getOtpPin() {
		return otpPin;
	}

	public void setOtpPin(String otpPin) {
		this.otpPin = otpPin;
	}

	public Double getSdpChargedAmount() {
		return sdpChargedAmount;
	}

	public void setSdpChargedAmount(Double sdpChargedAmount) {
		this.sdpChargedAmount = sdpChargedAmount;
	}

	public String getRedirectURL() {
		return redirectURL;
	}

	public void setRedirectURL(String redirectURL) {
		this.redirectURL = redirectURL;
	}

	public ModelAndView getModelAndView() {
		return modelAndView;
	}

	public void setModelAndView(ModelAndView modelAndView) {
		this.modelAndView = modelAndView;
	}

	public int getReturnType() {
		return returnType;
	}

	public void setReturnType(int returnType) {
		this.returnType = returnType;
	}
	
	
	
	
}
