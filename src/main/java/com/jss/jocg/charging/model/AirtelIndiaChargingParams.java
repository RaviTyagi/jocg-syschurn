package com.jss.jocg.charging.model;

import java.lang.reflect.Field;
import java.util.List;

import com.jss.jocg.util.JocgString;

public class AirtelIndiaChargingParams  extends JocgChargingParams{
	
	//PRODUCT_ID=546322|PRODUCT_NAME=nexGTv|PRODUCT_DESCP=nexGTv|CONTENT_TYPE=VIDEOS
	//|VALIDITY=1|AMOUNT=5|CLIENT_ID=4026122647c3c3a94e259a7bc26f74e2
	String productId;
	String productName;
	String productDescp;
	String contentType;
	Integer validity;
	Integer amount;
	String clientId;
	String secretKey;
	
	public AirtelIndiaChargingParams(){}
	
	@Override
	public String toString() {
			
	        Field[] fields = this.getClass().getDeclaredFields();
	        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	        try {
	            for (Field field : fields) {
	                str.append( field.getName()).append("=").append(field.get(this)).append(",");
	            }
	        } catch (IllegalArgumentException ex) {
	            System.out.println(ex);
	        } catch (IllegalAccessException ex) {
	            System.out.println(ex);
	        }
	        str.append("}");
	        return str.toString();
	    }

	public boolean decodeParams(String opParams){
		boolean flag=true;
		try{
			List<String> paramList=JocgString.convertStrToList(opParams, "|");
			decodeCommonParams(paramList);
			for(String p:paramList){
				if(p.startsWith("PRODUCT_ID")){
					this.setProductId(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("PRODUCT_NAME")){
					this.setProductName(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("PRODUCT_DESCP")){
					this.setProductDescp(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("CONTENT_TYPE")){
					this.setContentType(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("VALIDITY")){
					this.setValidity(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("AMOUNT")){
					this.setAmount(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("CLIENT_ID")){
					this.setClientId(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("SECRET_KEY")){
					this.setSecretKey(p.substring(p.indexOf(":")+1));
				}
				
				
			}
		}catch(Exception e){
			System.out.println("Exception while parsing opparams "+e);
			flag=false;
		}
		
		return flag;
	}
	
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescp() {
		return productDescp;
	}
	public void setProductDescp(String productDescp) {
		this.productDescp = productDescp;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Integer getValidity() {
		return validity;
	}
	public void setValidity(Integer validity) {
		this.validity = validity;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	
	
}
