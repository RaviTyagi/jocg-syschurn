package com.jss.jocg.charging.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jss.jocg.cache.entities.config.CgimageSchedule;
import com.jss.jocg.cache.entities.config.ChargingInterface;
import com.jss.jocg.cache.entities.config.ChargingPricepoint;
import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import com.jss.jocg.cache.entities.config.Circleinfo;
import com.jss.jocg.cache.entities.config.IPSeries;
import com.jss.jocg.cache.entities.config.ImsiSeries;
import com.jss.jocg.cache.entities.config.LandingPageSchedule;
import com.jss.jocg.cache.entities.config.MsisdnSeries;
import com.jss.jocg.cache.entities.config.PackageChintfmap;
import com.jss.jocg.cache.entities.config.PackageDetail;
import com.jss.jocg.cache.entities.config.Platform;
import com.jss.jocg.cache.entities.config.Routing;
import com.jss.jocg.cache.entities.config.Service;
import com.jss.jocg.cache.entities.config.TrafficCampaign;
import com.jss.jocg.cache.entities.config.TrafficSource;
import com.jss.jocg.cache.entities.config.VoucherDetail;
import com.jss.jocg.services.data.Subscriber;

public class JocgRequest {
	JocgRequestParameters requestParams;
	String subscriberId;
	Long msisdn;
	String requestType;
	Circleinfo circle;
	String deviceInfo;
	PackageDetail packageDetail;
	ChargingInterface sourceInterface;
	PackageChintfmap pkgChintfMap;
	ChargingInterface chargingOperator;
	ChargingPricepoint chargingPrice;
	ChargingPricepoint chargedPrice;//Applicable in case of Voucher only
	Boolean useParentHandler;
	Integer parentChintfId;
	String chargingHandlerClass;
	ChargingUrl chargingURL;
	List<ChargingUrlParam> chargingUrlParams;
	Integer checkSumStatus;
	TrafficCampaign campaign;
	TrafficSource trafficSource;
	VoucherDetail voucher;
	Platform platfrom;
	ImsiSeries imsiSeries;
	IPSeries ipSeries;
	MsisdnSeries msisdnSeries;
	LandingPageSchedule landingPage;
	CgimageSchedule cgimageSchedule;
	Service service;
	Subscriber subscriber;
	JocgResponse responseObject;
	String jocgTransactionId;
	List<ChargingUrl> urlList;
	Map<Integer,List<ChargingUrlParam>> urlParams;
	
	String sslKeyStoreFile;
	String sslKeystorePwd;
	

	String adNetworkToken;
	String chargingCategory;
	Double discountGiven;
	Double chargedAmount;
	
	Boolean churn20Min;
	Boolean churnSameDay;
	java.util.Date  unsubTime;
	int clickCount;
	Boolean routeToWasteURL;
	Routing routingInfo;
	
	public JocgRequest(){
		routingInfo=null;
		clickCount=0;
		urlList=new ArrayList<>();
		urlParams=new HashMap<>();
		requestParams=new JocgRequestParameters();
		circle=new Circleinfo();
		packageDetail=new PackageDetail();
		sourceInterface=new ChargingInterface();
		pkgChintfMap=new PackageChintfmap();
		chargingOperator=new ChargingInterface();
		chargingPrice=new ChargingPricepoint();
		chargingHandlerClass=new String();
		chargingURL=new ChargingUrl();
		chargingUrlParams=new ArrayList<>();
		checkSumStatus=new Integer(JocgRequestParameters.CSVALIDATION_REQUIRED);
		campaign=new TrafficCampaign();
		trafficSource=new TrafficSource();
		voucher=new VoucherDetail();
		platfrom=new Platform();
		imsiSeries=new ImsiSeries();
		ipSeries=new IPSeries();
		msisdnSeries=new MsisdnSeries();
		landingPage=new LandingPageSchedule();
		cgimageSchedule=new CgimageSchedule();
		service=new Service();
		this.subscriber=null;
		this.responseObject=new JocgResponse();
		this.useParentHandler=false;
		this.parentChintfId=0;
		this.churn20Min=false;
		this.churnSameDay=false;
		this.unsubTime=null;

	}
	
	public JocgRequestParameters getRequestParams() {
		return requestParams;
	}
	public void setRequestParams(JocgRequestParameters requestParams) {
		this.requestParams = requestParams;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public Long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(Long msisdn) {
		this.msisdn = msisdn;
	}
	public Circleinfo getCircle() {
		return circle;
	}
	public void setCircle(Circleinfo circle) {
		this.circle = circle;
	}
	public String getDeviceInfo() {
		return deviceInfo;
	}
	public void setDeviceInfo(String deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	public PackageDetail getPackageDetail() {
		return packageDetail;
	}
	public void setPackageDetail(PackageDetail packageDetail) {
		this.packageDetail = packageDetail;
	}
	public ChargingInterface getSourceInterface() {
		return sourceInterface;
	}
	public void setSourceInterface(ChargingInterface sourceInterface) {
		this.sourceInterface = sourceInterface;
	}
	public PackageChintfmap getPkgChintfMap() {
		return pkgChintfMap;
	}
	public void setPkgChintfMap(PackageChintfmap pkgChintfMap) {
		this.pkgChintfMap = pkgChintfMap;
	}
	public ChargingInterface getChargingOperator() {
		return chargingOperator;
	}
	public void setChargingOperator(ChargingInterface chargingOperator) {
		this.chargingOperator = chargingOperator;
	}
	public ChargingPricepoint getChargingPrice() {
		return chargingPrice;
	}
	public void setChargingPrice(ChargingPricepoint chargingPrice) {
		this.chargingPrice = chargingPrice;
	}
	public String getChargingHandlerClass() {
		return chargingHandlerClass;
	}
	public void setChargingHandlerClass(String chargingHandlerClass) {
		this.chargingHandlerClass = chargingHandlerClass;
	}
	public ChargingUrl getChargingURL() {
		return chargingURL;
	}
	public void setChargingURL(ChargingUrl chargingURL) {
		this.chargingURL = chargingURL;
	}
	public List<ChargingUrlParam> getChargingUrlParams() {
		return chargingUrlParams;
	}
	public void setChargingUrlParams(List<ChargingUrlParam> chargingUrlParams) {
		this.chargingUrlParams = chargingUrlParams;
	}
	public Integer getCheckSumStatus() {
		return checkSumStatus;
	}
	public void setCheckSumStatus(Integer checkSumStatus) {
		this.checkSumStatus = checkSumStatus;
	}
	public TrafficCampaign getCampaign() {
		return campaign;
	}
	public void setCampaign(TrafficCampaign campaign) {
		this.campaign = campaign;
	}
	public TrafficSource getTrafficSource() {
		return trafficSource;
	}
	public void setTrafficSource(TrafficSource trafficSource) {
		this.trafficSource = trafficSource;
	}
	public VoucherDetail getVoucher() {
		return voucher;
	}
	public void setVoucher(VoucherDetail voucher) {
		this.voucher = voucher;
	}
	public Platform getPlatfrom() {
		return platfrom;
	}
	public void setPlatfrom(Platform platfrom) {
		this.platfrom = platfrom;
	}
	public ImsiSeries getImsiSeries() {
		return imsiSeries;
	}
	public void setImsiSeries(ImsiSeries imsiSeries) {
		this.imsiSeries = imsiSeries;
	}
	public IPSeries getIpSeries() {
		return ipSeries;
	}
	public void setIpSeries(IPSeries ipSeries) {
		this.ipSeries = ipSeries;
	}
	public MsisdnSeries getMsisdnSeries() {
		return msisdnSeries;
	}
	public void setMsisdnSeries(MsisdnSeries msisdnSeries) {
		this.msisdnSeries = msisdnSeries;
	}
	public LandingPageSchedule getLandingPage() {
		return landingPage;
	}
	public void setLandingPage(LandingPageSchedule landingPage) {
		this.landingPage = landingPage;
	}
	public CgimageSchedule getCgimageSchedule() {
		return cgimageSchedule;
	}
	public void setCgimageSchedule(CgimageSchedule cgimageSchedule) {
		this.cgimageSchedule = cgimageSchedule;
	}
	public Service getService() {
		return service;
	}
	public void setService(Service service) {
		this.service = service;
	}

	public Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public JocgResponse getResponseObject() {
		return responseObject;
	}

	public void setResponseObject(JocgResponse responseObject) {
		this.responseObject = responseObject;
	}

	public String getJocgTransactionId() {
		return jocgTransactionId;
	}

	public void setJocgTransactionId(String jocgTransactionId) {
		this.jocgTransactionId = jocgTransactionId;
	}

	public Boolean getUseParentHandler() {
		return useParentHandler;
	}

	public void setUseParentHandler(Boolean useParentHandler) {
		this.useParentHandler = useParentHandler;
	}

	public Integer getParentChintfId() {
		return parentChintfId;
	}

	public void setParentChintfId(Integer parentChintfId) {
		this.parentChintfId = parentChintfId;
	}

	public List<ChargingUrl> getUrlList() {
		return urlList;
	}

	public void setUrlList(List<ChargingUrl> urlList) {
		this.urlList = urlList;
	}

	public Map<Integer, List<ChargingUrlParam>> getUrlParams() {
		return urlParams;
	}

	public void setUrlParams(Map<Integer, List<ChargingUrlParam>> urlParams) {
		this.urlParams = urlParams;
	}



	public String getAdNetworkToken() {
		return adNetworkToken;
	}

	public void setAdNetworkToken(String adNetworkToken) {
		this.adNetworkToken = adNetworkToken;
	}

	public String getChargingCategory() {
		return chargingCategory;
	}

	public void setChargingCategory(String chargingCategory) {
		this.chargingCategory = chargingCategory;
	}

	public Double getDiscountGiven() {
		return discountGiven;
	}

	public void setDiscountGiven(Double discountGiven) {
		this.discountGiven = discountGiven;
	}

	public Double getChargedAmount() {
		return chargedAmount;
	}

	public void setChargedAmount(Double chargedAmount) {
		this.chargedAmount = chargedAmount;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Boolean getChurn20Min() {
		return churn20Min;
	}

	public void setChurn20Min(Boolean churn20Min) {
		this.churn20Min = churn20Min;
	}

	public Boolean getChurnSameDay() {
		return churnSameDay;
	}

	public void setChurnSameDay(Boolean churnSameDay) {
		this.churnSameDay = churnSameDay;
	}

	public java.util.Date getUnsubTime() {
		return unsubTime;
	}

	public void setUnsubTime(java.util.Date unsubTime) {
		this.unsubTime = unsubTime;
	}

	public String getSslKeyStoreFile() {
		return sslKeyStoreFile;
	}

	public void setSslKeyStoreFile(String sslKeyStoreFile) {
		this.sslKeyStoreFile = sslKeyStoreFile;
	}

	public String getSslKeystorePwd() {
		return sslKeystorePwd;
	}

	public void setSslKeystorePwd(String sslKeystorePwd) {
		this.sslKeystorePwd = sslKeystorePwd;
	}

	public ChargingPricepoint getChargedPrice() {
		return chargedPrice;
	}

	public void setChargedPrice(ChargingPricepoint chargedPrice) {
		this.chargedPrice = chargedPrice;
	}

	public int getClickCount() {
		return clickCount;
	}

	public void setClickCount(int clickCount) {
		this.clickCount = clickCount;
	}

	public Boolean getRouteToWasteURL() {
		return routeToWasteURL;
	}

	public void setRouteToWasteURL(Boolean routeToWasteURL) {
		this.routeToWasteURL = routeToWasteURL;
	}

	public Routing getRoutingInfo() {
		return routingInfo;
	}

	public void setRoutingInfo(Routing routingInfo) {
		this.routingInfo = routingInfo;
	}
	
	
}
