package com.jss.jocg.charging.model;

import com.jss.jocg.util.JocgStatusCodes;

public class JocgDeactivationResponse {

Integer statusCode;
String statusDescp;
String subscriberId;
Long msisdn;
String serviceName;
String packageName;

public JocgDeactivationResponse(){
	statusCode=JocgStatusCodes.UNSUB_SUCCESS_PENDING;
	statusDescp="UNSUB Failed";
	subscriberId="NA";
	msisdn=0L;
	serviceName="NA";
	packageName="NA";
	
}

public Integer getStatusCode() {
	return statusCode;
}

public void setStatusCode(Integer statusCode) {
	this.statusCode = statusCode;
}

public String getStatusDescp() {
	return statusDescp;
}

public void setStatusDescp(String statusDescp) {
	this.statusDescp = statusDescp;
}

public String getSubscriberId() {
	return subscriberId;
}

public void setSubscriberId(String subscriberId) {
	this.subscriberId = subscriberId;
}

public Long getMsisdn() {
	return msisdn;
}

public void setMsisdn(Long msisdn) {
	this.msisdn = msisdn;
}

public String getServiceName() {
	return serviceName;
}

public void setServiceName(String serviceName) {
	this.serviceName = serviceName;
}

public String getPackageName() {
	return packageName;
}

public void setPackageName(String packageName) {
	this.packageName = packageName;
}


	
}
