package com.jss.jocg.charging.model;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.jss.jocg.util.JocgString;

public class FreechargeChargingParams {

	String secretKey;
	String checksum;
	String mobileNumber;
	String merchantId;
	String otpId;
	String otp;
	String accessToken;
	String merchantTxnId;
	String currency;
	String channel;
	int amount;
	public FreechargeChargingParams(){
		this.secretKey="";
		this.mobileNumber="NA";
		this.merchantId="NA";
		this.otpId="NA";
		this.otp="NA";
		this.accessToken="NA";
		this.merchantTxnId="NA";
		this.currency="NA";
		this.channel="NA";
		this.amount=0;
	}
	
	public String getJson(String requestType,String checkSum,boolean addChecksum){
		StringBuilder sb=new StringBuilder();
		if("OTP".equalsIgnoreCase(requestType)){
			//{ "merchantTxnId": "iTEST1_***sdsds%1233", "amount": "200.65", "currency": "INR", "furl": "http://domain:8080//failure.html?ji=ered&sdee=3423", "surl": "http://domain:8080//success.html?hi=ho&abc=abr", "productInfo": "auth", "email": "test2323@test.com", "mobile": "9090909090", "customerName": "testCustomer", "customNote": "please make it fast...; !", "channel": "WEB", "os": "ubuntu-14.04", "merchantId": "abcXXX123"}
			sb.append("{");			
			sb.append('"').append("merchantId").append('"').append(":").append('"').append(this.getMerchantId()).append('"').append(",");
			sb.append('"').append("mobileNumber").append('"').append(":").append('"').append(this.getMobileNumber()).append('"');
			if(addChecksum) sb.append(",").append('"').append("checksum").append('"').append(":").append('"').append(checkSum).append('"');
			sb.append("}");
		}else if ("VERIFY-OTP".equalsIgnoreCase(requestType)){
			//https://login.freecharge.in/api/v2/co/oauth/user/login?merchantId=&otpId=&otp=&checksum=
			
			sb.append("{");
			sb.append('"').append("merchantId").append('"').append(":").append('"').append(this.getMerchantId()).append('"').append(",");			
			sb.append('"').append("otp").append('"').append(":").append('"').append(this.getOtp()).append('"').append(",");
			sb.append('"').append("otpId").append('"').append(":").append('"').append(this.getOtpId()).append('"');
			if(addChecksum) sb.append(",").append('"').append("checksum").append('"').append(":").append('"').append(checkSum).append('"');
			sb.append("}");
		}else if("CHARGE".equalsIgnoreCase(requestType)){
			//https://checkout.freecharge.in/api/v1/co/oauth/wallet/debit?amount=&merchantId=&merchantTxnId=&currency=&channel=&accessToken=&checksum=
			sb.append("{");
			sb.append('"').append("accessToken").append('"').append(":").append('"').append(this.getAccessToken()).append('"').append(",");
			sb.append('"').append("amount").append('"').append(":").append('"').append(this.getAmount()).append('"').append(",");
			sb.append('"').append("channel").append('"').append(":").append('"').append(this.getChannel()).append('"').append(",");
			sb.append('"').append("currency").append('"').append(":").append('"').append(this.getCurrency()).append('"').append(",");
			sb.append('"').append("merchantId").append('"').append(":").append('"').append(this.getMerchantId()).append('"').append(",");
			sb.append('"').append("merchantTxnId").append('"').append(":").append('"').append(this.getMerchantTxnId()).append('"');								
			if(addChecksum) sb.append(",").append('"').append("checksum").append('"').append(":").append('"').append(checkSum).append('"');
			
			sb.append("}");
		}else if("GET-BALANCE".equalsIgnoreCase(requestType)){
			//https://checkout.freecharge.in/api/v1/co/oauth/wallet/balance?merchantId=&accessToken=
			sb.append("{");
			sb.append('"').append("merchantId").append('"').append(":").append('"').append(this.getMerchantId()).append('"').append(",");
			sb.append('"').append("accessToken").append('"').append(":").append('"').append(this.getAccessToken()).append('"');
			if(addChecksum) sb.append(",").append('"').append("checksum").append('"').append(":").append('"').append(checkSum).append('"');
			sb.append("}");
		}
		
		
		return sb.toString();
	}
	
	public String getChecksum(String logHeader,String requestType){
		String checksum="";
		try{
			
			System.out.println(logHeader+" Calculating checksum for requestType "+requestType);
			String jsonString=getJson(requestType,"",false);
			System.out.println("Json For CheckSum for "+requestType+" : "+jsonString);
			MessageDigest md=null; 
			String plainText = jsonString.concat(this.getSecretKey());
			try { md = MessageDigest.getInstance("SHA-256"); } catch (NoSuchAlgorithmException e) { }
			md.update(plainText.getBytes(StandardCharsets.UTF_8));
			byte[] mdbytes = md.digest();
			// convert the byte to hex format method 1 
			StringBuffer checksum1 = new StringBuffer(); 
			for (int i = 0; i < mdbytes.length; i++) { 
				checksum1.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));			
			}
			checksum=checksum1.toString();
			System.out.println(logHeader+" checksum "+checksum);
		}catch(Exception e){
			System.out.println(logHeader+" Error "+e);
		}
		return checksum;
	}
	
	public String getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getOtpId() {
		return otpId;
	}
	public void setOtpId(String otpId) {
		this.otpId = otpId;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getMerchantTxnId() {
		return merchantTxnId;
	}
	public void setMerchantTxnId(String merchantTxnId) {
		this.merchantTxnId = merchantTxnId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
}
