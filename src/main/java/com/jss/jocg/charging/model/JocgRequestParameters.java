package com.jss.jocg.charging.model;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

/**
 * JocgRequestParameters.class holds all request parameters on Jocg Platform
 * @author Rishi Tyagi
 *
 */

public class JocgRequestParameters {
	public static final int CSVALIDATION_NOTREQUIRED=-2;
	public static final int CSVALIDATION_FAILED=-1;
	public static final int CSVALIDATION_REQUIRED=0;
	public static final int CSVALIDATION_SUCCESS=1;
	String requestCategory;
	
	String sourceOperator;
	String subscriberId;
	Long headerMsisdn;
	Long requestMsisdn;
	String imsi;
	String platformName;
	String packageName;
	Double packagePrice;
	Integer packageValidity;
	String networkType;
	String voucherVendor;
	Double amountToBeCharged;
	String voucherCode;
	Long checkSum;
	Integer campaignId;
	String publisherId;
	Integer checkSumStatus;//-2: Not required,-1=ChecksumValidation Failed, 0=To be validated, 1=Validated,
	String sourceIP;
	String referer;
	String userAgent;
	HttpServletRequest requestContext;
	
	
	public JocgRequestParameters(){
		
	}
	
	public JocgRequestParameters(String requestCategory, String sourceOperator, String subscriberId, Long headerMsisdn, Long requestMsisdn, String imsi, String platformName, String packageName, Double packagePrice, Integer packageValidity, String networkType, String voucherVendor, Double amountToBeCharged, String voucherCode, Long checkSum, Integer campaignId, String publisherId,String sourceIP,String referer,String userAgent){
		this.requestCategory=requestCategory;
		this.sourceOperator=sourceOperator;
		this.subscriberId=subscriberId;
		this.headerMsisdn=headerMsisdn;
		this.requestMsisdn=requestMsisdn;
		this.imsi=imsi;
		this.platformName=platformName;
		this.packageName=packageName;
		this.packagePrice=packagePrice;
		this.packageValidity=packageValidity;
		this.networkType=networkType;
		this.voucherVendor=voucherVendor;
		this.amountToBeCharged=amountToBeCharged;
		this.voucherCode=voucherCode;
		this.checkSum=checkSum;
		this.campaignId=campaignId;
		this.publisherId=publisherId;
		this.sourceIP=sourceIP;
		this.referer=referer;
		this.userAgent=userAgent;
		this.checkSumStatus=0;
		}
	
	public JocgRequestParameters(String requestCategory, String sourceOperator, String subscriberId, Long headerMsisdn, Long requestMsisdn, String imsi, String platformName, String packageName, Double packagePrice, Integer packageValidity, String networkType, String voucherVendor, Double amountToBeCharged, String voucherCode, Long checkSum, Integer campaignId, String publisherId,String sourceIP,String referer,String userAgent,HttpServletRequest requestContext){
		this.requestCategory=requestCategory;
		this.sourceOperator=sourceOperator;
		this.subscriberId=subscriberId;
		this.headerMsisdn=headerMsisdn;
		this.requestMsisdn=requestMsisdn;
		this.imsi=imsi;
		this.platformName=platformName;
		this.packageName=packageName;
		this.packagePrice=packagePrice;
		this.packageValidity=packageValidity;
		this.networkType=networkType;
		this.voucherVendor=voucherVendor;
		this.amountToBeCharged=amountToBeCharged;
		this.voucherCode=voucherCode;
		this.checkSum=checkSum;
		this.campaignId=campaignId;
		this.publisherId=publisherId;
		this.sourceIP=sourceIP;
		this.referer=referer;
		this.userAgent=userAgent;
		this.checkSumStatus=0;
		this.requestContext=requestContext;
		if((headerMsisdn==null || headerMsisdn<=0) && ("OP".equalsIgnoreCase(requestCategory) || "C".equalsIgnoreCase(requestCategory)) && ("ID_OF".equalsIgnoreCase(sourceOperator) || "AIRHD".equalsIgnoreCase(sourceOperator) || "OAIRT".equalsIgnoreCase(sourceOperator)) && (subscriberId==null || "NA".equalsIgnoreCase(subscriberId) || subscriberId.length()<=0)){
			this.subscriberId=sourceOperator+""+System.currentTimeMillis();
			if(requestMsisdn==null || requestMsisdn<=0){
				this.requestMsisdn=System.currentTimeMillis();
			}
		}
		
		}
	
	  public String toString(){
          StringBuilder sb=new StringBuilder();
         Field[] fields = this.getClass().getDeclaredFields();
         try {
             for (Field field : fields) {
                 sb.append(field.getName()).append("=").append(field.get(this)).append("|");
             }
         } catch (IllegalArgumentException ex) {
             System.out.println(ex);
         } catch (IllegalAccessException ex) {
             System.out.println(ex);
         }
          return sb.toString();
         
     }
	  
	public String getRequestCategory() {
		return requestCategory;
	}
	public void setRequestCategory(String requestCategory) {
		this.requestCategory = requestCategory;
	}
	public String getSourceOperator() {
		return sourceOperator;
	}
	public void setSourceOperator(String sourceOperator) {
		this.sourceOperator = sourceOperator;
	}
	public String getSubscriberId() {
		return subscriberId;
	}
	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}
	public Long getRequestMsisdn() {
		return requestMsisdn;
	}
	public void setRequestMsisdn(Long requestMsisdn) {
		this.requestMsisdn = requestMsisdn;
	}
	public String getPlatformName() {
		return platformName;
	}
	public void setPlatformName(String platformName) {
		this.platformName = platformName;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public Double getPackagePrice() {
		return packagePrice;
	}
	public void setPackagePrice(Double packagePrice) {
		this.packagePrice = packagePrice;
	}
	public Integer getPackageValidity() {
		return packageValidity;
	}
	public void setPackageValidity(Integer packageValidity) {
		this.packageValidity = packageValidity;
	}
	public String getNetworkType() {
		return networkType;
	}
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	public String getVoucherVendor() {
		return voucherVendor;
	}
	public void setVoucherVendor(String voucherVendor) {
		this.voucherVendor = voucherVendor;
	}
	public Double getAmountToBeCharged() {
		return amountToBeCharged;
	}
	public void setAmountToBeCharged(Double amountToBeCharged) {
		this.amountToBeCharged = amountToBeCharged;
	}
	public String getVoucherCode() {
		return voucherCode;
	}
	public void setVoucherCode(String voucherCode) {
		this.voucherCode = voucherCode;
	}
	public Long getCheckSum() {
		return checkSum;
	}
	public void setCheckSum(Long checkSum) {
		this.checkSum = checkSum;
	}
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public String getPublisherId() {
		return publisherId;
	}
	public void setPublisherId(String publisherId) {
		this.publisherId = publisherId;
	}

	public Long getHeaderMsisdn() {
		return headerMsisdn;
	}

	public void setHeaderMsisdn(Long headerMsisdn) {
		this.headerMsisdn = headerMsisdn;
	}

	public Integer getCheckSumStatus() {
		return checkSumStatus;
	}

	public void setCheckSumStatus(Integer checkSumStatus) {
		this.checkSumStatus = checkSumStatus;
	}

	public String getSourceIP() {
		return sourceIP;
	}

	public void setSourceIP(String sourceIP) {
		this.sourceIP = sourceIP;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	
	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public HttpServletRequest getRequestContext() {
		return requestContext;
	}

	public void setRequestContext(HttpServletRequest requestContext) {
		this.requestContext = requestContext;
	}

	
	
	
}
