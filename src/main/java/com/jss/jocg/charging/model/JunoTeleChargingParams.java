package com.jss.jocg.charging.model;

import java.lang.reflect.Field;
import java.util.List;

import com.jss.jocg.util.JocgString;

public class JunoTeleChargingParams  extends JocgChargingParams{
//PARKING:8|GRACE:2|SUSPENSION:8|VALIDITY:7|AMOUNT:25|SERVICE_KEY:JTS_NGTV_TAM_W25
	Integer validity;
	Integer amount;
	String junoProductId;
	String productName;
	String productDescp;
	String cgImage;
	String productType;
	String currency;
	String cpId;
	String cpName;
	String contentSize;
	String action;
	String transId;
	String operatorName;

	
	
	
	public boolean decodeParams(String opParams){
		boolean flag=true;
		try{
			List<String> paramList=JocgString.convertStrToList(opParams, "|");
			decodeCommonParams(paramList);
			for(String p:paramList){
				if(p.startsWith("VALIDITY")){
					this.setValidity(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("AMOUNT")){
					this.setAmount(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("SERVICE_KEY")){
					this.setJunoProductId(p.substring(p.indexOf(":")+1));
				}
				
			}
		}catch(Exception e){
			System.out.println("Exception while parsing opparams "+e);
			flag=false;
		}
		
		return flag;
	}
	
	
	
	@Override
	public String toString() {
			
	        Field[] fields = this.getClass().getDeclaredFields();
	        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	        try {
	            for (Field field : fields) {
	                str.append( field.getName()).append("=").append(field.get(this)).append(",");
	            }
	        } catch (IllegalArgumentException ex) {
	            System.out.println(ex);
	        } catch (IllegalAccessException ex) {
	            System.out.println(ex);
	        }
	        str.append("}");
	        return str.toString();
	    }



	public Integer getValidity() {
		return validity;
	}



	public void setValidity(Integer validity) {
		this.validity = validity;
	}



	public Integer getAmount() {
		return amount;
	}



	public void setAmount(Integer amount) {
		this.amount = amount;
	}



	public String getJunoProductId() {
		return junoProductId;
	}



	public void setJunoProductId(String junoProductId) {
		this.junoProductId = junoProductId;
	}



	public String getProductName() {
		return productName;
	}



	public void setProductName(String productName) {
		this.productName = productName;
	}



	public String getProductDescp() {
		return productDescp;
	}



	public void setProductDescp(String productDescp) {
		this.productDescp = productDescp;
	}



	public String getCgImage() {
		return cgImage;
	}



	public void setCgImage(String cgImage) {
		this.cgImage = cgImage;
	}



	public String getProductType() {
		return productType;
	}



	public void setProductType(String productType) {
		this.productType = productType;
	}



	public String getCurrency() {
		return currency;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}



	public String getCpId() {
		return cpId;
	}



	public void setCpId(String cpId) {
		this.cpId = cpId;
	}



	public String getCpName() {
		return cpName;
	}



	public void setCpName(String cpName) {
		this.cpName = cpName;
	}



	public String getContentSize() {
		return contentSize;
	}



	public void setContentSize(String contentSize) {
		this.contentSize = contentSize;
	}



	public String getAction() {
		return action;
	}



	public void setAction(String action) {
		this.action = action;
	}



	public String getTransId() {
		return transId;
	}



	public void setTransId(String transId) {
		this.transId = transId;
	}



	public String getOperatorName() {
		return operatorName;
	}



	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	
	
	
}
