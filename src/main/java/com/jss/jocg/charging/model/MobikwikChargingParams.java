package com.jss.jocg.charging.model;

import java.lang.reflect.Field;
import java.security.Key;
import java.security.MessageDigest;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;

import com.jss.jocg.cache.entities.config.ChargingUrl;
import com.jss.jocg.cache.entities.config.ChargingUrlParam;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class MobikwikChargingParams {
	public static final String RQTYPE_GENOTP="GENERATE_OTP";
	public static final String RQTYPE_VALIDATEOTP="VALIDATE_OTP";
	public static final String RQTYPE_GETBALANCE="GET_BALANCE";
	public static final String RQTYPE_CHARGE="CHARGE";
	public static final String RQTYPE_CREATEWALLET="CREATENEW";
	String mobikwickRequestType;
	int amount;
	long msisdn;
	String tokenType;
	String msgCode;
	String merchantId;
	String transId;
	String merchantName;
	String otpPin;
	String token;
	String orderId;
	String comment;
	String txnType;
	String checkSumKey;
	String checksum;
	String emailId;
	
	public MobikwikChargingParams(){
		
		this.checkSumKey="c9dNoS77cebgq9Oex4zBWCCMBfob";
	}
	
	private String toHex(byte[] bytes)
	  {
	    StringBuilder buffer = new StringBuilder(bytes.length * 2);
	    
	    byte[] arrayOfByte = bytes;
	    int j = bytes.length; 
	    for (int i = 0; i < j; i++) { 
	    	 Byte b = Byte.valueOf(arrayOfByte[i]);
	    	 String str = Integer.toHexString(b.byteValue());
	    	 int len = str.length();
		      if (len == 8){
		        buffer.append(str.substring(6));
		      }else if (str.length() == 2) {
		        buffer.append(str);
		      } else {
		        buffer.append("0" + str);
		      }
	    }
	    return buffer.toString();
	  }
	  
	  private String calculateChecksum(String secretKey, String allParamValue) throws Exception { 
		 // allParamValue=allParamValue+secretKey;
		  byte[] dataToEncryptByte = allParamValue.getBytes();
	    byte[] keyBytes = secretKey.getBytes();
	    SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
	    Mac mac = Mac.getInstance("HmacSHA256");
	    mac.init(secretKeySpec);
	    byte[] checksumByte = mac.doFinal(dataToEncryptByte);
	    String checksum = toHex(checksumByte);
	    return checksum;
	  }
	
	public String generateChecksum(Logger logger,String logHeader,String requestType,String requestDataTemplate){
		
		try{
		
			String checksumStrTemplate=requestDataTemplate.substring(requestDataTemplate.indexOf("checksum=")-1);
			requestDataTemplate=requestDataTemplate.substring(0, (requestDataTemplate.length()-checksumStrTemplate.length()));
			if(RQTYPE_GENOTP.equalsIgnoreCase(requestType)){
				//https://walletapi.mobikwik.com/otpgenerate?amount=&cell=&tokentype=&msgcode=&mid=&merchantname=&checksum=
				//amount=<AMOUNT>&cell=<MOBILE>&tokentype=<TOKENTYPE>&msgcode=<MSGCODE>&mid=<TRANSID>&merchantname=<MERCHANTID>&checksum=<CHECKSUM>
				System.out.println("Step 1: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<AMOUNT>", ""+this.getAmount());
				System.out.println("Step 2: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<MOBILE>", ""+this.getMsisdn());
				System.out.println("Step 3: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<TOKENTYPE>", this.getTokenType());
				System.out.println("Step 4: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<MSGCODE>", this.getMsgCode());
				System.out.println("Step 5: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<TRANSID>", this.getTransId());
				System.out.println("Step 6: data Template "+requestDataTemplate);
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTID>", this.getMerchantId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTNAME>", this.getMerchantName());
				//Params:amount=<AMOUNT>&cell=<MOBILE>&tokentype=<TOKENTYPE>&msgcode=<MSGCODE>&mid=<MERCHANTID>&merchantname=<MERCHANTNAME>&checksum=<CHECKSUM>
				//checksum-sequence=‘amount’’cell’’merchantname’’mid’’msgcode’’tokentype’
				checksumStrTemplate="'"+this.getAmount()+"''"+this.getMsisdn()+"''"+this.getMerchantName()+"''"+this.getMerchantId()+"''"+this.getMsgCode()+"''"+this.getTokenType()+"'";
				//checksumStrTemplate="'1''"+this.getMsisdn()+"''"+this.getMerchantName()+"''"+this.getMerchantId()+"''"+this.getMsgCode()+"''"+this.getTokenType()+"'";
				System.out.println("Step 7: checksumStrTemplate "+checksumStrTemplate);
				System.out.println("Step 7: data Template "+requestDataTemplate);
			}else if(RQTYPE_VALIDATEOTP.equalsIgnoreCase(requestType)){//Token generation api
				//https://walletapi.mobikwik.com/tokengenerate?amount=&cell=&otp=&tokentype=&msgcode=&mid=&merchantname=&checksum=
				//amount=<AMOUNT>&cell=<MOBILE>&otp=<OTPPIN>&tokentype=<TOKENTYPE>&msgcode=<MSGCODE>&mid=<TRANSID>&merchantname=<MERCHANTID>&checksum=<CHECKSUM>
				requestDataTemplate=requestDataTemplate.replaceAll("<AMOUNT>", ""+this.getAmount());
				requestDataTemplate=requestDataTemplate.replaceAll("<MOBILE>", ""+this.getMsisdn());
				requestDataTemplate=requestDataTemplate.replaceAll("<OTPPIN>", this.getOtpPin());
				requestDataTemplate=requestDataTemplate.replaceAll("<TOKENTYPE>", this.getTokenType());
				requestDataTemplate=requestDataTemplate.replaceAll("<MSGCODE>", this.getMsgCode());
				requestDataTemplate=requestDataTemplate.replaceAll("<TRANSID>", this.getTransId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTID>", this.getMerchantId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTNAME>", this.getMerchantName());
				//amount=<AMOUNT>&cell=<MOBILE>&otp=<OTPPIN>&tokentype=<TOKENTYPE>&msgcode=<MSGCODE>&mid=<MERCHANTID>&merchantname=<MERCHANTNAME>&checksum=<CHECKSUM>
				//"'" + amount + "''" + cell + "''" + merchantname + "''" + mid + "''" + msgcode + "''" + otp + "''" + tokentype + "'";
				checksumStrTemplate="'"+this.getAmount()+"''"+this.getMsisdn()+"''"+this.getMerchantName()+"''"+this.getMerchantId()+"''"+this.getMsgCode()+"''"+this.getOtpPin()+"''"+this.getTokenType()+"'";
				System.out.println("Mobikwik::ValidateOTP: checksumStrTemplate "+checksumStrTemplate);
			
			}else if(RQTYPE_GETBALANCE.equalsIgnoreCase(requestType)){
				//https://walletapi.mobikwik.com/userbalance?cell=&token=&msgcode=&mid=&merchantname=&checksum=
				//cell=<MOBILE>&token=<TOKEN>&msgcode=<MSGCODE>&mid=<TRANSID>&merchantname=<MERCHANTID>&checksum=<CHECKSUM>
				requestDataTemplate=requestDataTemplate.replaceAll("<MOBILE>", ""+this.getMsisdn());
				requestDataTemplate=requestDataTemplate.replaceAll("<TOKEN>", this.getToken());
				requestDataTemplate=requestDataTemplate.replaceAll("<MSGCODE>", this.getMsgCode());
				requestDataTemplate=requestDataTemplate.replaceAll("<TRANSID>", this.getTransId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTID>", this.getMerchantId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTNAME>", this.getMerchantName());
				//cell=<MOBILE>&token=<TOKEN>&msgcode=<MSGCODE>&mid=<MERCHANTID>&merchantname=<MERCHANTNAME>&checksum=<CHECKSUM>
				checksumStrTemplate=""+this.getMsisdn()+""+this.getToken()+""+this.getMsgCode()+""+this.getMerchantId()+""+this.getMerchantName();
				System.out.println("Mobikwik::GetBalance: checksumStrTemplate "+checksumStrTemplate);
			}else if(RQTYPE_CREATEWALLET.equalsIgnoreCase(requestType)){
				requestDataTemplate=requestDataTemplate.replaceAll("<EMAIL>", ""+this.getEmailId());
				requestDataTemplate=requestDataTemplate.replaceAll("<TOKEN>", this.getToken());
				requestDataTemplate=requestDataTemplate.replaceAll("<MOBILE>", ""+this.getMsisdn());
				requestDataTemplate=requestDataTemplate.replaceAll("<MSGCODE>", this.getMsgCode());
				//requestDataTemplate=requestDataTemplate.replaceAll("<TRANSID>", this.getTransId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTID>", this.getMerchantId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTNAME>", this.getMerchantName());
				//"'" + cell + "''" + email + "''" + merchantname + "''" + mid + "''" + msgcode + "''" + token + "'";
				checksumStrTemplate= "'" + this.getMsisdn() + "''"+this.getEmailId()+"''" + this.getMerchantName() + "''" + this.getMerchantId() +
						"''" + this.getMsgCode() + "''" + this.getToken() +"'";
			
				System.out.println("Mobikwik::CreateWalletRequest: checksumStrTemplate "+checksumStrTemplate);
			}else{
				//Charge Request
				//https://walletapi.mobikwik.com/debitwallet?amount=&token=&cell=&orderid=&comment=&txntype=&msgcode=&mid=&merchantname=&checksum=
				//amount=<AMOUNT>&token=<TOKEN>&cell=<MOBILE>&orderid=<ORDERID>&comment=<COMMENT>&txntype=<TXNTYPE>&msgcode=<MSGCODE>&mid=<TRANSID>&merchantname=<MERCHANTID>&checksum=<CHECKSUM>
				requestDataTemplate=requestDataTemplate.replaceAll("<AMOUNT>", ""+this.getAmount());
				requestDataTemplate=requestDataTemplate.replaceAll("<TOKEN>", this.getToken());
				requestDataTemplate=requestDataTemplate.replaceAll("<MOBILE>", ""+this.getMsisdn());
				requestDataTemplate=requestDataTemplate.replaceAll("<ORDERID>", ""+this.getOrderId());
				requestDataTemplate=requestDataTemplate.replaceAll("<COMMENT>", ""+this.getComment());
				requestDataTemplate=requestDataTemplate.replaceAll("<TRANSTYPE>", ""+this.getTxnType());
				requestDataTemplate=requestDataTemplate.replaceAll("<MSGCODE>", this.getMsgCode());
				requestDataTemplate=requestDataTemplate.replaceAll("<TRANSID>", this.getTransId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTID>", this.getMerchantId());
				requestDataTemplate=requestDataTemplate.replaceAll("<MERCHANTNAME>", this.getMerchantName());
				//"'" + amount + "''" + cell + "''" + comment + "''" + merchantname + "''" + mid + "''" + msgcode + "''" + orderid + "''" + token + "''" + txntype + "'"
				checksumStrTemplate= "'" + this.getAmount() + "''" + this.getMsisdn() + "''"+this.getComment()+"''" + this.getMerchantName() + "''" + this.getMerchantId() +
						"''" + this.getMsgCode() + "''" + this.getTransId() + "''" + this.getToken() + "''" + this.getTxnType() + "'";
			
				System.out.println("Mobikwik::ChargeRequest: checksumStrTemplate "+checksumStrTemplate);
			}
			
			logger.debug(logHeader+" Calculating Checksum for "+checksumStrTemplate);
			this.setChecksum(calculateChecksum(this.getCheckSumKey(), checksumStrTemplate));
			
			requestDataTemplate +="&checksum="+this.getChecksum();
			logger.debug(logHeader+"Request Params after checksum "+requestDataTemplate);
		}catch(Exception e){
			
		}
		return requestDataTemplate;
	}
	public boolean extractParams(Logger logger,String logHeader,double chargingAmount,String otpPin,String orderId,String token,String jocgTransId,List<ChargingUrlParam> urlParams){
		boolean status=true;
		try{
			int amt=(int)(chargingAmount);
			this.setAmount(amt);
			this.setMsisdn(msisdn);
			this.setTransId(jocgTransId);
			this.setOrderId(jocgTransId);
			this.setOtpPin(otpPin);
			this.setToken(token);
			System.out.println("URLParams "+urlParams);
			for(ChargingUrlParam p:urlParams){
				if("CONSTANT".equalsIgnoreCase(p.getParamCatg())){
					if("MERCHANTID".equalsIgnoreCase(p.getParamKey())){
						this.setMerchantId(p.getDefaultValue());
					}else if("MERCHANTNAME".equalsIgnoreCase(p.getParamKey())){
						this.setMerchantName(p.getDefaultValue());
					}else if("MSGCODE".equalsIgnoreCase(p.getParamKey())){
						this.setMsgCode(p.getDefaultValue());
					}else if("TOKENTYPE".equalsIgnoreCase(p.getParamKey())){
						this.setTokenType(p.getDefaultValue());
					}else if("TXNTYPE".equalsIgnoreCase(p.getParamKey())){
						this.setTokenType(p.getDefaultValue());
					}else if("TRANSTYPE".equalsIgnoreCase(p.getParamKey())){
						this.setTxnType(p.getDefaultValue());
					}else if("COMMENT".equalsIgnoreCase(p.getParamKey())){
						this.setComment(p.getDefaultValue());
					}else if("CHECKSUM_KEY".equalsIgnoreCase(p.getParamKey())){
						this.setCheckSumKey(p.getDefaultValue());
					}
					
				}
			}
		}catch(Exception e){
			status=false;
			logger.debug(logHeader+"extractParams :: "+e.getMessage());
		}
		return status;
	}
	
	@Override
	public String toString() {
			
        Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
        try {
            for (Field field : fields) {
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
    }
	
	public String getMobikwickRequestType() {
		return mobikwickRequestType;
	}
	public void setMobikwickRequestType(String mobikwickRequestType) {
		this.mobikwickRequestType = mobikwickRequestType;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public long getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(long msisdn) {
		this.msisdn = msisdn;
	}
	public String getTokenType() {
		return tokenType;
	}
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}
	public String getMsgCode() {
		return msgCode;
	}
	public void setMsgCode(String msgCode) {
		this.msgCode = msgCode;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getMerchantId() {
		return merchantId;
	}
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	public String getOtpPin() {
		return otpPin;
	}
	public void setOtpPin(String otpPin) {
		this.otpPin = otpPin;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getTxnType() {
		return txnType;
	}
	public void setTxnType(String txnType) {
		this.txnType = txnType;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	public String getCheckSumKey() {
		return checkSumKey;
	}

	public void setCheckSumKey(String checkSumKey) {
		this.checkSumKey = checkSumKey;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	

}
