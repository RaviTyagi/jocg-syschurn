package com.jss.jocg.charging.model.parsers;

import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class OxigenResponse {
//OTPResponse : <Wallet><ResponseCode>0</ResponseCode><ResponseMessage>Success</ResponseMessage><ReferenceId>54994741</ReferenceId></Wallet>
int responseCode;
String responseMessage;
String referenceId;
String accessToken;

public int parseResponse(String respType,String respData){
	
	this.responseCode=-1;
	try{
		if("SEND-OTP".equalsIgnoreCase(respType)){
			this.responseCode=JocgString.strToInt(extractValue(respData,"ResponseCode","-1"),-1);
			this.responseMessage=extractValue(respData,"ResponseMessage","NA");
			this.referenceId=extractValue(respData,"ReferenceId","NA");
			this.accessToken=extractValue(respData,"AuthToken","NA");
		}else if("DEBIT".equalsIgnoreCase(respType)){
			this.responseCode=JocgString.strToInt(extractValue(respData,"ResponseCode","-1"),-1);
			this.responseMessage=extractValue(respData,"ResponseMessage","NA");
			this.referenceId=extractValue(respData,"ReferenceId","NA");
			this.accessToken=extractValue(respData,"AuthToken","NA");
		}
		
	}catch(Exception e){
		this.responseCode=-2;
	}
	
	
	
	return this.responseCode;
}

@Override
public String toString(){
	Field[] fields = this.getClass().getDeclaredFields();
    StringBuilder str= new StringBuilder("OxigenResponse{");;
    try {
        for (Field field : fields) {
        	if(!field.getName().startsWith("KEY_"))
            str.append( field.getName()).append("=").append(field.get(this)).append(",");
        }
    } catch (IllegalArgumentException ex) {
        System.out.println(ex);
    } catch (IllegalAccessException ex) {
        System.out.println(ex);
    }
    str.append("}");
    return str.toString();
}
	
	public String extractValue(String data,String tagName,String defaultValue){
		String extractedValue="";
		try{
			String keyToSearch="<"+tagName+">";
			if(data.contains(keyToSearch)){
				data=data.substring(data.indexOf(keyToSearch)+keyToSearch.length());
				extractedValue=data.substring(0, data.indexOf("<"));
			}else{
				extractedValue=defaultValue;
			}
		}catch(Exception e){
			extractedValue=defaultValue;
		}
		
		return extractedValue;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
	

}
