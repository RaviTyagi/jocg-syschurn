package com.jss.jocg.charging.model;

import java.lang.reflect.Field;
import java.util.List;

import com.jss.jocg.util.JocgString;

public class NetExcelChargingParams   extends JocgChargingParams{
//:204000020077776|:204000020077776|:MobileTV|:10|VALIDITY:3
//SERVICE_KEY:00748577|SERVICE_ID:00748577|SERVICE_NAME:Mobile%20TV|AMOUNT:99|VALIDITY:30	
	
	String serviceKey;
	String serviceId;
	String serviceName;
	Integer amount;
	Integer validity;
	public NetExcelChargingParams(){}
	
	public boolean decodeParams(String opParams){
		boolean flag=true;
		try{
			List<String> paramList=JocgString.convertStrToList(opParams, "|");
			decodeCommonParams(paramList);
			for(String p:paramList){
				if(p.startsWith("SERVICE_KEY")){
					this.setServiceKey(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("SERVICE_ID")){
					this.setServiceId(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("SERVICE_NAME")){
					this.setServiceName(p.substring(p.indexOf(":")+1));
				}else if(p.startsWith("AMOUNT")){
					this.setAmount(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}else if(p.startsWith("VALIDITY")){
					this.setValidity(JocgString.strToInt(p.substring(p.indexOf(":")+1),0));
				}
				
				
			}
		}catch(Exception e){
			System.out.println("Exception while parsing opparams "+e);
			flag=false;
		}
		
		return flag;
	}
	
	
	
	@Override
	public String toString() {
			
	        Field[] fields = this.getClass().getDeclaredFields();
	        StringBuilder str= new StringBuilder(this.getClass().getName()+"{");;
	        try {
	            for (Field field : fields) {
	                str.append( field.getName()).append("=").append(field.get(this)).append(",");
	            }
	        } catch (IllegalArgumentException ex) {
	            System.out.println(ex);
	        } catch (IllegalAccessException ex) {
	            System.out.println(ex);
	        }
	        str.append("}");
	        return str.toString();
	    }
	public String getServiceKey() {
		return serviceKey;
	}
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getValidity() {
		return validity;
	}
	public void setValidity(Integer validity) {
		this.validity = validity;
	}
	
	
	
	
}
