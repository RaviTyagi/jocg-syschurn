package com.jss.jocg.charging.model.parsers;

import java.lang.reflect.Field;

import com.jss.jocg.util.JocgString;

public class MobikwikResponse {

	int msgCode;
	String status;
	String statusDescription;
	int statusCode;
	String accessToken;
	public MobikwikResponse(){}
	
	public void parseXML(String xmlData){
		//Generate OTP Pin
		//<?xml version="1.0" encoding="UTF-8" standalone="yes"?><response>  <messagecode>504</messagecode>  <status>FAILURE</status>  <statuscode>99</statuscode>  <statusdescription>Unexpected Error</statusdescription></response>
		//Validate OTP/Generate Token
		//<?xml version="1.0" encoding="UTF-8" standalone="yes"?><response>  <messagecode>507</messagecode>  <status>SUCCESS</status>  <statuscode>0</statuscode>  <statusdescription>Token Generated</statusdescription>  <token>57ef5d9b56d74c74bc45d776509ce0b9</token>  <checksum>ad3849e14eb1e302b348af33253e3ccde503ed093bce53cc16cd3ad9be60a810</checksum></response>
		
		this.msgCode=JocgString.strToInt(extractValue(xmlData,"messagecode","0"),-1);
		this.status=extractValue(xmlData,"status","NA");
		this.statusCode=JocgString.strToInt(extractValue(xmlData,"statuscode","0"),-1);
		this.statusDescription=extractValue(xmlData,"statusdescription","NA");
		this.accessToken=extractValue(xmlData,"token","NA");
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
        StringBuilder str= new StringBuilder("MobikwikResponse{");;
        try {
            for (Field field : fields) {
            	if(!field.getName().startsWith("KEY_"))
                str.append( field.getName()).append("=").append(field.get(this)).append(",");
            }
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalAccessException ex) {
            System.out.println(ex);
        }
        str.append("}");
        return str.toString();
	}
	public String extractValue(String data,String tagName,String defaultValue){
		String extractedValue="";
		try{
			String keyToSearch="<"+tagName+">";
			if(data.contains(keyToSearch)){
				data=data.substring(data.indexOf(keyToSearch)+keyToSearch.length());
				extractedValue=data.substring(0, data.indexOf("<"));
			}else{
				extractedValue=defaultValue;
			}
		}catch(Exception e){
			extractedValue=defaultValue;
		}
		
		return extractedValue;
	}

	public int getMsgCode() {
		return msgCode;
	}

	public void setMsgCode(int msgCode) {
		this.msgCode = msgCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
}
