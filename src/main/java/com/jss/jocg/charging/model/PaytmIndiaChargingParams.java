package com.jss.jocg.charging.model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeMap;

public class PaytmIndiaChargingParams implements Serializable{

	//REQUEST_TYPE=SUBSCRIBE&MID=<COMPANYID>&ORDER_ID=<JOCG_TRANS_ID>&CUST_ID=<CUSTOMER_ID>
	//&TXN_AMOUNT=<AMOUNT>&CHANNEL_ID=WEB&INDUSTRY_TYPE_ID=RETAIL&WEBSITE=Digivivewalletwap
	//&SUBS_SERVICE_ID=1A&SUBS_AMOUNT_TYPE=VARIABLE&SUBS_FREQUENCY=365&SUBS_FREQUENCY_UNIT=DAY
	//&SUBS_ENABLE_RETRY=1&SUBS_EXPIRY_DATE=<VALIDITY_END>&SUBS_MAX_AMOUNT=<PACKAGE_PP>
	//&SUBS_PPI_ONLY=Y&CALLBACK_URL=<CALLBACK_URL>&CHECKSUMHASH=<CHECKSUM>
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String REQUEST_TYPE;
	String MID;
	String ORDER_ID;
	String CUST_ID;
	String TXN_AMOUNT;
	String CHANNEL_ID;
	String INDUSTRY_TYPE_ID;
	String WEBSITE;
	String SUBS_SERVICE_ID;
	String SUBS_AMOUNT_TYPE;
	String SUBS_FREQUENCY;
	String SUBS_FREQUENCY_UNIT;
	String SUBS_ENABLE_RETRY;
	String SUBS_EXPIRY_DATE;
	String SUBS_MAX_AMOUNT;
	String SUBS_PPI_ONLY;
	String CALLBACK_URL;
	String CHECKSUMHASH;
	String cgURL;
	
	public PaytmIndiaChargingParams(){}
	public boolean extractParams(TreeMap t,String checkSumHash){
		boolean retr=false;
		try{
			Iterator<String> i=t.keySet().iterator();
			String key="",value="";
			while(i.hasNext()){
				key=i.next();
				value=(String)t.get(key);
				if("REQUEST_TYPE".equalsIgnoreCase(key)){this.REQUEST_TYPE=value;}
				else if("MID".equalsIgnoreCase(key)){this.MID=value;}
				else if("ORDER_ID".equalsIgnoreCase(key)){this.ORDER_ID=value;}
				else if("CUST_ID".equalsIgnoreCase(key)){this.CUST_ID=value;}
				else if("TXN_AMOUNT".equalsIgnoreCase(key)){this.TXN_AMOUNT=value;}
				else if("CHANNEL_ID".equalsIgnoreCase(key)){this.CHANNEL_ID=value;}
				else if("INDUSTRY_TYPE_ID".equalsIgnoreCase(key)){this.INDUSTRY_TYPE_ID=value;}
				else if("WEBSITE".equalsIgnoreCase(key)){this.WEBSITE=value;}
				else if("SUBS_SERVICE_ID".equalsIgnoreCase(key)){this.SUBS_SERVICE_ID=value;}
				else if("SUBS_AMOUNT_TYPE".equalsIgnoreCase(key)){this.SUBS_AMOUNT_TYPE=value;}
				else if("SUBS_FREQUENCY".equalsIgnoreCase(key)){this.SUBS_FREQUENCY=value;}
				else if("SUBS_FREQUENCY_UNIT".equalsIgnoreCase(key)){this.SUBS_FREQUENCY_UNIT=value;}
				else if("SUBS_ENABLE_RETRY".equalsIgnoreCase(key)){this.SUBS_ENABLE_RETRY=value;}
				else if("SUBS_EXPIRY_DATE".equalsIgnoreCase(key)){this.SUBS_EXPIRY_DATE=value;}
				else if("SUBS_MAX_AMOUNT".equalsIgnoreCase(key)){this.SUBS_MAX_AMOUNT=value;}
				else if("SUBS_PPI_ONLY".equalsIgnoreCase(key)){this.SUBS_PPI_ONLY=value;}
				else if("CALLBACK_URL".equalsIgnoreCase(key)){this.CALLBACK_URL=value;}

			}
			retr=true;
		}catch(Exception e){
			retr=false;
		}
		if(retr) this.CHECKSUMHASH=checkSumHash;
		return retr;
	}
	public String getREQUEST_TYPE() {
		return REQUEST_TYPE;
	}
	public void setREQUEST_TYPE(String rEQUEST_TYPE) {
		REQUEST_TYPE = rEQUEST_TYPE;
	}
	public String getMID() {
		return MID;
	}
	public void setMID(String mID) {
		MID = mID;
	}
	public String getORDER_ID() {
		return ORDER_ID;
	}
	public void setORDER_ID(String oRDER_ID) {
		ORDER_ID = oRDER_ID;
	}
	public String getCUST_ID() {
		return CUST_ID;
	}
	public void setCUST_ID(String cUST_ID) {
		CUST_ID = cUST_ID;
	}
	public String getTXN_AMOUNT() {
		return TXN_AMOUNT;
	}
	public void setTXN_AMOUNT(String tXN_AMOUNT) {
		TXN_AMOUNT = tXN_AMOUNT;
	}
	public String getCHANNEL_ID() {
		return CHANNEL_ID;
	}
	public void setCHANNEL_ID(String cHANNEL_ID) {
		CHANNEL_ID = cHANNEL_ID;
	}
	public String getINDUSTRY_TYPE_ID() {
		return INDUSTRY_TYPE_ID;
	}
	public void setINDUSTRY_TYPE_ID(String iNDUSTRY_TYPE_ID) {
		INDUSTRY_TYPE_ID = iNDUSTRY_TYPE_ID;
	}
	public String getWEBSITE() {
		return WEBSITE;
	}
	public void setWEBSITE(String wEBSITE) {
		WEBSITE = wEBSITE;
	}
	public String getSUBS_SERVICE_ID() {
		return SUBS_SERVICE_ID;
	}
	public void setSUBS_SERVICE_ID(String sUBS_SERVICE_ID) {
		SUBS_SERVICE_ID = sUBS_SERVICE_ID;
	}
	public String getSUBS_AMOUNT_TYPE() {
		return SUBS_AMOUNT_TYPE;
	}
	public void setSUBS_AMOUNT_TYPE(String sUBS_AMOUNT_TYPE) {
		SUBS_AMOUNT_TYPE = sUBS_AMOUNT_TYPE;
	}
	public String getSUBS_FREQUENCY() {
		return SUBS_FREQUENCY;
	}
	public void setSUBS_FREQUENCY(String sUBS_FREQUENCY) {
		SUBS_FREQUENCY = sUBS_FREQUENCY;
	}
	public String getSUBS_FREQUENCY_UNIT() {
		return SUBS_FREQUENCY_UNIT;
	}
	public void setSUBS_FREQUENCY_UNIT(String sUBS_FREQUENCY_UNIT) {
		SUBS_FREQUENCY_UNIT = sUBS_FREQUENCY_UNIT;
	}
	public String getSUBS_ENABLE_RETRY() {
		return SUBS_ENABLE_RETRY;
	}
	public void setSUBS_ENABLE_RETRY(String sUBS_ENABLE_RETRY) {
		SUBS_ENABLE_RETRY = sUBS_ENABLE_RETRY;
	}
	public String getSUBS_EXPIRY_DATE() {
		return SUBS_EXPIRY_DATE;
	}
	public void setSUBS_EXPIRY_DATE(String sUBS_EXPIRY_DATE) {
		SUBS_EXPIRY_DATE = sUBS_EXPIRY_DATE;
	}
	public String getSUBS_MAX_AMOUNT() {
		return SUBS_MAX_AMOUNT;
	}
	public void setSUBS_MAX_AMOUNT(String sUBS_MAX_AMOUNT) {
		SUBS_MAX_AMOUNT = sUBS_MAX_AMOUNT;
	}
	public String getSUBS_PPI_ONLY() {
		return SUBS_PPI_ONLY;
	}
	public void setSUBS_PPI_ONLY(String sUBS_PPI_ONLY) {
		SUBS_PPI_ONLY = sUBS_PPI_ONLY;
	}
	public String getCALLBACK_URL() {
		return CALLBACK_URL;
	}
	public void setCALLBACK_URL(String cALLBACK_URL) {
		CALLBACK_URL = cALLBACK_URL;
	}
	public String getCHECKSUMHASH() {
		return CHECKSUMHASH;
	}
	public void setCHECKSUMHASH(String cHECKSUMHASH) {
		CHECKSUMHASH = cHECKSUMHASH;
	}
	public String getCgURL() {
		return cgURL;
	}
	public void setCgURL(String cgURL) {
		this.cgURL = cgURL;
	}
	
	
	
}
