package com.jss.jocg.charging.model;

import java.lang.reflect.Field;

import org.springframework.web.servlet.ModelAndView;

public class JocgResponse {
	public static int RESPTYPE_REDIRECTURL=0;
	public static int RESPTYPE_MODELANDVIEW=1;
	public static int RESPTYPE_TEXT=2;
	Integer responseCode;
	String responseDescription;
	Integer trafficValidatorStatus;
	String trafficValidatorMessage;
	Integer validity;
	ModelAndView respView;
	Integer respType;
	String redirectUrl;
	String otpPin="";
	String accessToken;
	
	public JocgResponse(){
		
	}
	
	@Override
	public String toString(){
		Field[] fields = this.getClass().getDeclaredFields();
	    StringBuilder str= new StringBuilder("JocgResponse{");;
	    try {
	        for (Field field : fields) {
	        	if(!field.getName().startsWith("KEY_"))
	            str.append( field.getName()).append("=").append(field.get(this)).append(",");
	        }
	    } catch (IllegalArgumentException ex) {
	        System.out.println(ex);
	    } catch (IllegalAccessException ex) {
	        System.out.println(ex);
	    }
	    str.append("}");
	    return str.toString();
	}
	
	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public ModelAndView getRespView() {
		return respView;
	}
	public void setRespView(ModelAndView respView) {
		this.respView = respView;
	}
	public Integer getRespType() {
		return respType;
	}
	public void setRespType(Integer respType) {
		this.respType = respType;
	}

	public Integer getTrafficValidatorStatus() {
		return trafficValidatorStatus;
	}

	public void setTrafficValidatorStatus(Integer trafficValidatorStatus) {
		this.trafficValidatorStatus = trafficValidatorStatus;
	}

	public String getTrafficValidatorMessage() {
		return trafficValidatorMessage;
	}

	public void setTrafficValidatorMessage(String trafficValidatorMessage) {
		this.trafficValidatorMessage = trafficValidatorMessage;
	}

	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	public String getOtpPin() {
		return otpPin;
	}

	public void setOtpPin(String otpPin) {
		this.otpPin = otpPin;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	
	
}
