package com.jss.jocg.web.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	@Value("${home.message:test}")
	public String message="Test Message";
	
	@RequestMapping("/")
	public String home(Map<String,Object> model){
		model.put("message", message);
		
		return "home";
		
	}
	
	@RequestMapping("/test")
	public ModelAndView home(){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("vodacgresp");
		mv.addObject("message", "Hello Test message!");
	
		
		return mv;
		
	}
	
}
